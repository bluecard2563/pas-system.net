<?php

/*
  Author:Esteban Angulo
  Modified date:19/02/2010
  Last modified:
 * Nicolas Flores 04/05/2010
 */

class Sales {

	var $db;
	var $serial_sal;
    var $serial_con;
	var $serial_cus;
	var $sal_serial_sal;
	var $serial_inv;
	var $serial_cnt;
	var $serial_pbd;
	var $serial_cur;
	var $serial_cit;
	var $card_number_sal;
	var $emission_date_sal;
	var $begin_date_sal;
	var $end_date_sal;
	var $in_date_sal;
	var $days_sal;
	var $fee_sal;
	var $observations_sal;
	var $cost_sal;
	var $free_sal;
	var $id_erp_sal;
	var $status_sal;
	var $type_sal;
	var $stock_type_sal;
	var $total_sal;
	var $direct_sale_sal;
	var $total_cost_sal;
	var $change_fee_sal;
	var $international_fee_status;
	var $international_fee_used;
	var $international_fee_amount;
	var $aux;
     var $deliveredKit;
     var $transation_id;

	function __construct($db, $serial_sal = NULL, $serial_cus = NULL, $serial_pbd = NULL, $serial_cnt = NULL, $serial_inv = NULL, $serial_cur = NULL, $serial_cit = NULL, $card_number_sal= NULL, $emission_date_sal= NULL, $begin_date_sal= NULL, $end_date_sal= NULL, $in_date_sal= NULL, $days_sal= NULL, $fee_sal= NULL, $observations_sal= NULL, $cost_sal= NULL, $free_sal= NULL, $id_erp_sal= NULL, $status_sal=NULL, $type_sal=NULL, $stock_type_sal=NULL, $total_sal=NULL, $direct_sale_sal=NULL, $total_cost_sal=NULL, $change_fee_sal=NULL, $sal_serial_sal=NULL, $international_fee_status=NULL, $international_fee_used=NULL, $international_fee_amount=NULL, $serial_con = NULL, $transaction_id = NULL) {
		$this->db = $db;
		$this->serial_sal = $serial_sal;
        $this->serial_con = $serial_con;
		$this->serial_cus = $serial_cus;
		$this->serial_pbd = $serial_pbd;
		$this->serial_cnt = $serial_cnt;
		$this->serial_inv = $serial_inv;
		$this->serial_cur = $serial_cur;
		$this->serial_cit = $serial_cit;
		$this->card_number_sal = $card_number_sal;
		$this->emission_date_sal = $emission_date_sal;
		$this->begin_date_sal = $begin_date_sal;
		$this->end_date_sal = $end_date_sal;
		$this->in_date_sal = $in_date_sal;
		$this->days_sal = $days_sal;
		$this->fee_sal = $fee_sal;
		$this->observations_sal = $observations_sal;
		$this->cost_sal = $cost_sal;
		$this->free_sal = $free_sal;
		$this->id_erp_sal = $id_erp_sal;
		$this->status_sal = $status_sal;
		$this->type_sal = $type_sal;
		$this->stock_type_sal = $stock_type_sal;
		$this->total_sal = $total_sal;
		$this->direct_sale_sal = $direct_sale_sal;
		$this->total_cost_sal = $total_cost_sal;
		$this->change_fee_sal = $change_fee_sal;
		$this->sal_serial_sal = $sal_serial_sal;
		$this->international_fee_status = $international_fee_status;
		$this->international_fee_used = $international_fee_used;
		$this->international_fee_amount = $international_fee_amount;
                $this->deliveredKit = $deliveredKit;
                $this->transation_id = $transaction_id;
	}

	/*	 * *********************************************
	  @Name: getData
	  @Description: Returns all data
	  @Params: N/A
	  @Returns: true/false
	 * ********************************************* */

	function getData() {
		if ($this->serial_sal != NULL) {
			$sql = "SELECT s.serial_sal,
                        s.serial_cus,
                        s.serial_pbd,
                        s.serial_cnt,
                        s.serial_inv,
                        s.serial_cur,
                        s.serial_cit,
                        s.card_number_sal,
                        DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y %T'),
                        DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y'),
                        DATE_FORMAT(s.end_date_sal,'%d/%m/%Y'),
                        DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),
                        s.days_sal,
                        s.fee_sal,
                        s.observations_sal,
                        s.cost_sal,
                        s.free_sal,
                        s.id_erp_sal,
                        s.status_sal,
                        s.type_sal,
                        s.stock_type_sal,
                        s.total_sal,
                        s.direct_sale_sal,
                        s.total_cost_sal,
                        s.change_fee_sal,
                        s.sal_serial_sal,
                        s.international_fee_status,
                        s.international_fee_used,
                        s.international_fee_amount,
                        s.deliveredKit,
                        s.serial_con,
                        s.transaction_id
                    FROM sales s
                    WHERE s.serial_sal ='" . $this->serial_sal . "'";
			$result = $this->db->Execute($sql);
			if ($result === false)
				return false;

			if ($result->fields[0]) {
				$this->serial_sal = $result->fields[0];
				$this->serial_cus = $result->fields[1];
				$this->serial_pbd = $result->fields[2];
				$this->serial_cnt = $result->fields[3];
				$this->serial_inv = $result->fields[4];
				$this->serial_cur = $result->fields[5];
				$this->serial_cit = $result->fields[6];
				$this->card_number_sal = $result->fields[7];
				$this->emission_date_sal = $result->fields[8];
				$this->begin_date_sal = $result->fields[9];
				$this->end_date_sal = $result->fields[10];
				$this->in_date_sal = $result->fields[11];
				$this->days_sal = $result->fields[12];
				$this->fee_sal = $result->fields[13];
				$this->observations_sal = $result->fields[14];
				$this->cost_sal = $result->fields[15];
				$this->free_sal = $result->fields[16];
				$this->id_erp_sal = $result->fields[17];
				$this->status_sal = $result->fields[18];
				$this->type_sal = $result->fields[19];
				$this->stock_type_sal = $result->fields[20];
				$this->total_sal = $result->fields[21];
				$this->direct_sale_sal = $result->fields[22];
				$this->total_cost_sal = $result->fields[23];
				$this->change_fee_sal = $result->fields[24];
				$this->sal_serial_sal = $result->fields[25];
				$this->international_fee_status = $result->fields[26];
				$this->international_fee_used = $result->fields[27];
				$this->international_fee_amount = $result->fields[28];
                $this->deliveredKit = $result->fields[29];
                $this->serial_con = $result->fields[30];
                 $this->transaction_id = $result->fields[31];
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * function insert
	  @Name: insert
	  @Description: Inserts a new register in data base
	  @Params:
	 *       N/A
	  @Returns: insertedID
	 *          or
	 *        false
	 * ********************************************* */

	function insert() {
		$sql = "INSERT INTO sales (
                                serial_cus,
                                serial_con,
                                serial_pbd,
                                serial_cnt,
                                serial_inv,
								serial_cur,
                                serial_cit,
                                card_number_sal,
                                emission_date_sal,
                                begin_date_sal,
                                end_date_sal,
                                in_date_sal,
                                days_sal,
                                fee_sal,
                                observations_sal,
                                cost_sal,
                                free_sal,
                                id_erp_sal,
                                status_sal,
                                type_sal,
                                stock_type_sal,
                                total_sal,
                                direct_sale_sal,
                                total_cost_sal,
                                change_fee_sal,
                                sal_serial_sal,
                                international_fee_status,
                                international_fee_used,
                                international_fee_amount,
                                deliveredKit, 
                                transaction_id)
                VALUES( '" . $this->serial_cus . "',";
                
                if ($this->serial_con != '') {
			$sql.= "'" . $this->serial_con . "',";
		} else {
			$sql.= "NULL,";
		}
                
                $sql.="
                          '" . $this->serial_pbd . "',
                          '" . $this->serial_cnt . "',";
                
		if ($this->serial_inv != '') {
			$sql.= "'" . $this->serial_inv . "',";
		} else {
			$sql.= "NULL,";
		}
		if ($this->serial_cur != '') {
			$sql.= "'" . $this->serial_cur . "',";
		} else {
			$sql.= "1,";
		}
		if ($this->serial_cit != 'NULL') {
			$sql.="'" . $this->serial_cit . "',";
		} else {
			$sql.= "NULL,";
		}
		if ($this->card_number_sal == 'N/A') {
			$sql.="NULL,";
		} else {
			$sql.="'" . $this->card_number_sal . "',";
		}
		$sql.="STR_TO_DATE(CONCAT('" . $this->emission_date_sal . "',' ',(SELECT curtime())),'%d/%m/%Y %T'),
                        STR_TO_DATE('" . $this->begin_date_sal . "','%d/%m/%Y'),
                        STR_TO_DATE('" . $this->end_date_sal . "','%d/%m/%Y'),
                        CURRENT_TIMESTAMP(),
                        '" . $this->days_sal . "',
                        '" . ($this->fee_sal ? $this->fee_sal : "0.00") . "',
                        '" . $this->observations_sal . "',
                        '" . ($this->cost_sal ? $this->cost_sal : "0") . "',
                        '" . $this->free_sal . "',
                        " . ($this->id_erp_sal ? "'" . $this->id_erp_sal . "'" : "NULL") . ",
                        '" . $this->status_sal . "',
                        '" . $this->type_sal . "',
                        '" . $this->stock_type_sal . "',
                        '" . ($this->total_sal ? $this->total_sal : "0.00") . "',
                        " . ($this->direct_sale_sal ? "'" . $this->direct_sale_sal . "'" : "NULL") . ",
                        '" . ($this->total_cost_sal ? $this->total_cost_sal : "0") . "',
                        '" . $this->change_fee_sal . "',
                        " . ($this->sal_serial_sal ? "'" . $this->sal_serial_sal . "'" : "NULL") . ",
                        '" . $this->international_fee_status . "',
                        '" . $this->international_fee_used . "',
                        '" . ($this->international_fee_amount ? $this->international_fee_amount : "0.00") . "',
                        '" . $this->deliveredKit . "'"
                        . ",'" . $this->transation_id . "')";
              
		$result = $this->db->Execute($sql);

		if ($result) {
			return $this->db->insert_ID();
		} else {
			ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
			return false;
		}
	}

	/*	 * *********************************************
	 * function insert
	  @Name: insert
	  @Description: Inserts a new register in data base
	  @Params:
	 *       N/A
	  @Returns: insertedID
	 *          or
	 *        false
	 * ********************************************* */

	function update() {
		$sql = "UPDATE sales SET
			        serial_cus=" . $this->serial_cus . ",
                    serial_con=" . $this->serial_con . ",
					serial_pbd=" . $this->serial_pbd . ",
					serial_cnt=" . $this->serial_cnt . ",";
		if ($this->serial_inv != '') {
			$sql.=" serial_inv=" . $this->serial_inv . ",";
		} else {
			$sql.=" serial_inv = NULL,";
		}

		if ($this->serial_cit != '') {
			$sql.=" serial_cit=" . $this->serial_cit . ",";
		} else {
			$sql.=" serial_cit = NULL,";
		}
		$sql.="	card_number_sal='" . $this->card_number_sal . "',
                        emission_date_sal=STR_TO_DATE('" . $this->emission_date_sal . "','%d/%m/%Y %T'),
                        begin_date_sal=STR_TO_DATE('" . $this->begin_date_sal . "','%d/%m/%Y'),
                        end_date_sal=STR_TO_DATE('" . $this->end_date_sal . "','%d/%m/%Y'),
                        in_date_sal=STR_TO_DATE('" . $this->in_date_sal . "','%d/%m/%Y'),
                        days_sal='" . $this->days_sal . "',
                        fee_sal='" . $this->fee_sal . "',
                        observations_sal='" . $this->observations_sal . "',
                        cost_sal='" . $this->cost_sal . "',
                        free_sal='" . $this->free_sal . "',";
		if ($this->id_erp_sal != '') {
			$sql.=" id_erp_sal=" . $this->id_erp_sal . ",";
		} else {
			$sql.=" id_erp_sal = NULL,";
		}
		$sql.="	status_sal='" . $this->status_sal . "',
                        type_sal='" . $this->type_sal . "',
                        total_sal='" . $this->total_sal . "',
                        stock_type_sal='" . $this->stock_type_sal . "',
                        direct_sale_sal='" . $this->direct_sale_sal . "',
                        total_cost_sal='" . $this->total_cost_sal . "',
                        change_fee_sal='" . $this->change_fee_sal . "',
                        international_fee_status='" . $this->international_fee_status . "',
                        international_fee_used='" . $this->international_fee_used . "',
                        international_fee_amount='" . $this->international_fee_amount . "'
                WHERE serial_sal=" . $this->serial_sal;
		$result = $this->db->Execute($sql);

		if ($result) {
			return true;
		} else {
			ErrorLog::log($this->db, 'UPDATE FAILED - ' . get_class($this), $sql);
			return false;
		}
	}

	/*	 * *********************************************
	 * function updateInvoice
	  @Name: updateInvoice
	  @Description: updates serial_inv
	  @Params:
	 *       $serial_inv
	  @Returns: true if updated succesfully
	 *          or
	 *        false if not
	 * ********************************************* */

	function updateInvoice($serial_inv) {
		$sql = "UPDATE sales SET serial_inv ='" . $serial_inv . "'
				WHERE serial_sal = '" . $this->serial_sal . "'";

		$result = $this->db->Execute($sql);
		if ($result === false

			)return false;

		if ($result == true)
			return true;
		else {
			ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
			return false;
		}
	}

	/*	 * *********************************************
	 * function updateStatus
	  @Name: updateStatus
	  @Description: updates status_sal and free_sal
	  @Params:
	 *       none
	  @Returns: true if updated succesfully
	 *          or
	 *        false if not
	 * ********************************************* */

	function updateStatus() {
		$sql = "UPDATE sales SET status_sal ='$this->status_sal',
                                  free_sal='$this->free_sal',
								  begin_date_sal= STR_TO_DATE('" . $this->begin_date_sal . "','%d/%m/%Y'),
								  end_date_sal= STR_TO_DATE('" . $this->end_date_sal . "','%d/%m/%Y')
				WHERE serial_sal = '" . $this->serial_sal . "'";
		//die($sql);
		$result = $this->db->Execute($sql);
		if ($result === false

			)return false;

		if ($result == true)
			return true;
		else
			return false;
	}

	/*	 * *********************************************
	 * function voidSerial_inv
	  @Name: voidSerial_inv
	  @Description: sets serial_inv null
	  @Params:
	 *       serial_inv
	  @Returns: true if updated succesfully
	 *          or
	 *        false if not
	 * ********************************************* */

	function voidSerial_inv($serial_inv) {
		$sql = "UPDATE sales SET serial_inv =NULL
            WHERE serial_inv=" . $serial_inv;
		//die($sql);
		$result = $this->db->Execute($sql);
		if ($result === false)
			return false;

		if ($result == true)
			return true;
		else
			return false;
	}

	/*	 * *********************************************
	 * function updateStatusByInvoice
	  @Name: updateStatusByInvoice
	  @Description: updates status_sal of a given serial_inv
	  @Params:
	 *       none
	  @Returns: true if updated succesfully
	 *          or
	 *        false if not
	 * ********************************************* */

	function updateStatusByInvoice() {
		$sql = "UPDATE sales SET status_sal ='" . $this->status_sal . "'
				 WHERE serial_inv = '" . $this->serial_inv . "'
				 AND status_sal NOT IN ('VOID','BLOCKED')";

		$result = $this->db->Execute($sql);
		if ($result === false

			)return false;

		if ($result == true)
			return true;
		else
			return false;
	}

	/*	 * *********************************************
	 * function insertServices
	  @Name: insertServices
	  @Description: Inserts a new register in data base
	  @Params:
	 *       serial_sal , serial_sbc
	  @Returns: true
	 *          or
	 *        false
	 * ********************************************* */

	function insertServices($serial_sal, $serial_sbd) {
		$sbcbd = new ServicesByCountryBySale($this->db);
		$sbcbd->setSerial_sal($serial_sal);
		$sbcbd->setSerial_sbd($serial_sbd);
		if ($sbcbd->insert()) {
			return true;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * function getSalesTotal
	  @Name: getSalesTotal
	  @Description: Get the total sales using the sales ids recived.
	  @Params:
	 *       $sales: string with sales serial separated by ,
	  @Returns: double with sales total.
	 * ************************************************ */

	function getSalesTotal($sales) {
		if (!$sales)
			return NULL;
		$sql = "
		  SELECT SUM(s.total_sal)
		  	FROM sales s
		  WHERE s.serial_sal IN (" . $sales . ")
		";
		//echo $sql." ";
		$result = $this->db->getOne($sql);
		if ($result === false)
			die("failed getSalesTotal");
		if ($result)
			return $result;
		else
			return NULL;
	}

	/*	 * *********************************************
	 * function getSalesTotal
	  @Name: getSalesTotal
	  @Description: Get the total sales using the sales ids recived.
	  @Params:
	 *       $sales: string with sales serial separated by ,
	  @Returns: double with sales total.
	 * ************************************************ */

	function getCostTotal($sales) {
		if (!$sales)
			return NULL;
		$sql = "
		  SELECT SUM(s.total_cost_sal)
		  	FROM sales s
		  WHERE s.serial_sal IN (" . $sales . ")
		";
		//echo $sql." ";
		$result = $this->db->getOne($sql);
		if ($result === false)
			die("failed getCostTotal");
		if ($result)
			return $result;
		else
			return NULL;
	}

	/**
	 * function getMasiveSales
	  @Name: getMasiveSales
	  @Description: Get the sales that corresponds to masive products
	  @Params:
	  @Returns: array with masive sales
	 * */
	function getMasiveSales($code_lang) {
		$sql = "SELECT pbl.name_pbl, sal.serial_sal, sal.card_number_sal
				FROM sales sal
				JOIN product_by_dealer pbd ON sal.serial_pbd=pbd.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product pro ON pxc.serial_pro=pro.serial_pro
				JOIN product_by_language pbl ON pbl.serial_pro=pro.serial_pro
				JOIN language lang ON pbl.serial_lang = lang.serial_lang AND lang.code_lang = '$code_lang'
				WHERE (pro.third_party_register_pro = 'YES' OR pro.masive_pro = 'YES')
				AND (sal.status_sal IN ('ACTIVE','REGISTERED'))
				AND (sal.end_date_sal > CURDATE())";
		//die($sql);

		$result = $this->db->Execute($sql);
		if ($result === false)
			die("failed getMasiveSales");
		$num = $result->RecordCount();
		if ($num > 0) {
			$arr = array();
			$cont = 0;

			do {
				$arr[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arr;
	}

	/**
	  @Name: getMasiveSales
	  @Description: Returns an array of masive sales.
	  @Params: class atribute serial_dea
	  @Returns: Sales array
	 * */
	function getSubMasiveSales() {
		$sql = "SELECT 	s.serial_pbd,
							s.serial_sal,
							s.card_number_sal,
							pbl.name_pbl,
							cus.first_name_cus as legal_entity_name,
 							CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name,
							DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y')as emission_date,
							FORMAT(s.total_sal * s.change_fee_sal,2) as total_sal,
							cur.symbol_cur,
							s.status_sal,
							pxc.serial_pxc,
							IF(COUNT(tl.serial_trl) > 0, COUNT(tl.serial_trl), 'NO') AS 'registered_people'
                    FROM sales s
                    JOIN counter c ON s.serial_cnt=c.serial_cnt
                    JOIN dealer d ON d.serial_dea=c.serial_dea 
                    JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
                    JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
                    JOIN product pr ON pr.serial_pro=pxc.serial_pro AND pr.masive_pro='YES' AND s.sal_serial_sal=" . $this->serial_sal . "
                    JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
                    JOIN customer cus ON cus.serial_cus=s.serial_cus
					JOIN currency cur ON cur.serial_cur = s.serial_cur
					LEFT JOIN traveler_log tl ON tl.serial_sal=s.serial_sal
					GROUP BY s.serial_sal
                    ORDER BY s.emission_date_sal DESC,s.card_number_sal
                    ";
		//die($sql);
		$result = $this->db->Execute($sql);
		if ($result) {
			$num = $result->RecordCount();
			if ($num > 0) {
				$list = array();
				$cont = 0;

				do {
					$list[$cont] = $result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				} while ($cont < $num);
			}
		}
		return $list;
	}

	/**
	  @Name: getSalesByInvoice
	  @Description: Returns an array of sales availables for invoice.
	  @Params: serial invoice
	  @Returns: Sales array
	 * */
	function getSalesByInvoice($serial_inv) {
		$sql = "SELECT s.serial_pbd,s.serial_sal,s.card_number_sal,pbl.name_pbl,cus.first_name_cus as legal_entity_name,
					   CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name, DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y')as emission_date,
					   s.total_sal,s.status_sal, s.total_cost_sal, s.serial_cnt,
					   inv.discount_prcg_inv, inv.other_dscnt_inv, inv.applied_taxes_inv, id_erp_sal
				FROM sales s
				JOIN counter c ON s.serial_cnt=c.serial_cnt
				JOIN dealer d ON d.serial_dea=c.serial_dea
				JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
				JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
				JOIN product pr ON pr.serial_pro=pxc.serial_pro
				JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				JOIN customer cus ON cus.serial_cus=s.serial_cus
				JOIN invoice inv ON s.serial_inv=inv.serial_inv
				WHERE s.serial_inv=" . $serial_inv . "
				ORDER BY s.emission_date_sal DESC,s.card_number_sal";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$list = array();
			$cont = 0;

			do {
				$list[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $list;
	}

	/**
	  @Name: getSalesByInvoice
	  @Description: Returns an array of sales availables for invoice.
	  @Params: serial invoice
	  @Returns: Sales array
	 * */
	function getSalesByInvoiceLog($serial_inv) {
		$sql = "SELECT s.serial_pbd, s.serial_sal, s.card_number_sal, pbl.name_pbl, cus.first_name_cus as legal_entity_name,
					   CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name,
					   DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y')as emission_date, s.total_sal, s.status_sal,
					   s.total_cost_sal, IF(s.begin_date_sal<=NOW(), 'YES', 'NO') AS 'international_fee_validation'
				FROM sales s
				JOIN invoice_log inl ON s.serial_sal=inl.serial_sal AND inl.void_sales_inl='YES' AND inl.status_inl='PENDING'
				JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
				JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
				JOIN product pr ON pr.serial_pro=pxc.serial_pro
				JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				JOIN customer cus ON cus.serial_cus=s.serial_cus
				WHERE inl.serial_inv=" . $serial_inv . "
				ORDER BY s.emission_date_sal DESC,s.card_number_sal";
		//die($sql);
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$list = array();
			$cont = 0;

			do {
				$list[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $list;
	}

	/**
	  @Name: getSaleProduct
	  @Description: Returns an array with the information of a specific sale.
	  @Params: none
	  @Returns: Sale array or False
	 * */
	function getSaleProduct($serial_lang) {
		$sql = "SELECT pbl.name_pbl, d.name_dea, CONCAT(u.first_name_usr,' ', u.last_name_usr) as 'counter', cit.name_cit,
						   CONCAT(cou.code_cou,'-',cit.code_cit,'-',dea.code_dea,'-',d.code_dea) as 'code', cits.name_cit city_sal,
						   cous.name_cou country_sal, s.card_number_sal, DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y %T') AS 'emission_date_sal',
						   DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal', DATE_FORMAT(s.end_date_sal, '%d/%m/%Y') AS 'end_date_sal',
						   FORMAT(s.total_sal * change_fee_sal, 2) AS 'total_sal', 
						   FORMAT(s.fee_sal * change_fee_sal, 2) AS 'fee_sal', 
						   s.days_sal, p.flights_pro, p.masive_pro, pbl.serial_pro
                    FROM sales s
                    JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
                    JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
                    JOIN product p ON p.serial_pro=pxc.serial_pro
                    JOIN benefits_product bp ON bp.serial_pro=p.serial_pro
                    JOIN benefits b ON b.serial_ben=bp.serial_ben
                    JOIN benefits_by_language bbl ON bbl.serial_ben=b.serial_ben
                    JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang = " . $serial_lang . "
                    JOIN counter c ON c.serial_cnt=s.serial_cnt
                    JOIN dealer d ON c.serial_dea=d.serial_dea
                    JOIN dealer dea ON dea.serial_dea=d.dea_serial_dea
                    JOIN sector sec ON sec.serial_sec=d.serial_sec
                    JOIN city cit ON cit.serial_cit=sec.serial_cit
                    JOIN country cou ON cou.serial_cou=cit.serial_cou
                    JOIN user u ON u.serial_usr=c.serial_usr
					LEFT JOIN city cits ON cits.serial_cit =  s.serial_cit
					LEFT JOIN country cous ON cous.serial_cou = cits.serial_cou
                    WHERE s.serial_sal='" . $this->serial_sal . "'";
		$result = $this->db->Execute($sql);
		if ($result) {
			return $result->GetRowAssoc(false);
		} else {
			return false;
		}
	}

	/**
	  @Name: getSaleServices
	  @Description: Returns an array with the services in a specific sale.
	  @Params: none
	  @Returns: Two-dimensional array or False
	 * */
	function getSaleServices($serial_lang, $active_service_only = FALSE) {
		$sql = "SELECT s.serial_sal, sbd.serial_sbd, sbc.serial_sbc, sbc.status_sbc, sbc.price_sbc, sbc.cost_sbc, sbc.coverage_sbc, sbl.name_sbl, ser.fee_type_ser
				FROM sales s
						JOIN services_by_country_by_sale sbcbs ON s.serial_sal = sbcbs.serial_sal
						JOIN services_by_dealer sbd ON sbcbs.serial_sbd = sbd.serial_sbd
						JOIN services_by_country sbc ON sbd.serial_sbc = sbc.serial_sbc
						JOIN services ser ON sbc.serial_ser = ser.serial_ser
						JOIN services_by_language sbl ON ser.serial_ser = sbl.serial_ser
				WHERE s.serial_sal = '" . $this->serial_sal . "' AND sbl.serial_lang=" . $serial_lang . "";
		
		if($active_service_only){
			$sql .= " AND ser.status_ser = 'ACTIVE'";
		}
		
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arr = array();
			$cont = 0;
			do {
				$arr[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
			return $arr;
		} else {
			return false;
		}
	}

	/**
	  @Name: getSaleBenefits
	  @Description: Returns an array with the benefits in a specific sale.
	  @Params: none
	  @Returns: Two-dimensional array or False
	 * */
	function getSaleBenefits($serial_lang) {
		$sql = "SELECT bbl.description_bbl, 
						FORMAT(bp.price_bxp * change_fee_sal, 2) AS 'price_bxp', 
						FORMAT(bp.restriction_price_bxp * change_fee_sal, 2) AS 'restriction_price_bxp', 
						rbl.description_rbl,
						IF(c.serial_con = 4, FORMAT(bp.price_bxp * change_fee_sal, 2), cbl.description_cbl) as alias_con, 
						bbl.serial_lang
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd = pbd.serial_pbd
			JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
			JOIN benefits_product bp ON bp.serial_pro = pxc.serial_pro AND bp.status_bxp = 'ACTIVE'
			JOIN conditions c ON c.serial_con = bp.serial_con
			JOIN conditions_by_language cbl ON cbl.serial_con = c.serial_con AND cbl.serial_lang = $serial_lang
			JOIN product_by_language pbl ON pxc.serial_pro = pbl.serial_pro AND pbl.serial_lang = $serial_lang
			LEFT JOIN restriction_type rt ON rt.serial_rst = bp.serial_rst AND rt.status_rst = 'ACTIVE'
			LEFT JOIN restriction_by_language rbl ON rbl.serial_rst=rt.serial_rst AND rbl.serial_lang = pbl.serial_lang
			JOIN benefits b ON b.serial_ben=bp.serial_ben AND b.status_ben = 'ACTIVE'
			JOIN benefits_by_language bbl ON bbl.serial_ben=b.serial_ben AND bbl.serial_lang = $serial_lang
			WHERE s.serial_sal='" . $this->serial_sal . "'
			ORDER BY b.weight_ben";

		//Debug::print_r($sql);
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arr = array();
			$cont = 0;
			do {
				$arr[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
			return $arr;
		}
		return false;
	}

	/*
	 * @Name: getFreeSales
	 * @Description: Returns an array of all the free sales pending to be authorized.
	 * @Params: N/A
	 * @Returns: An array of free sales.
	 */

	public static function getFreeSales($db, $serial_cou, $serial_cit=NULL, $serial_usr=NULL, $serial_dea=NULL, $dea_serial_dea=NULL) {
		$f1 = '';
		$f2 = '';
		$f3 = '';

		if ($serial_cit) {
			$f1 = "AND cit.serial_cit = $serial_cit";
		}
		if ($serial_usr) {
			$f2 = "AND ubd.serial_usr = $serial_usr";
		}
		if ($serial_dea) {
			$f3 = "AND dea.dea_serial_dea = $serial_dea";
		}
		if ($dea_serial_dea) {
			$f3 = "AND dea.serial_dea = $dea_serial_dea";
		}

		$sql = "SELECT s.*, pbl.name_pbl
                    FROM sales s
                    JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
                    JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
                    JOIN dealer dea ON dea.serial_dea=cnt.serial_dea $f3
                    JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea $f2
                    JOIN sector sec ON sec.serial_sec = dea.serial_sec
                    JOIN city cit ON cit.serial_cit = sec.serial_cit $f1
                    JOIN product_by_country pbc ON pbc.serial_pxc=pbd.serial_pxc
                    JOIN country cou ON cou.serial_cou = cit.serial_cou AND cou.serial_cou = $serial_cou
                    JOIN product_by_language pbl ON pbc.serial_pro=pbl.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
                    WHERE s.free_sal = 'YES'
                    AND s.status_sal = 'REQUESTED'
                    ORDER BY s.in_date_sal";

		$result = $db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arr = array();
			$cont = 0;

			do {
				$arr[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arr;
	}

	/*
	 * @Name: getSalesStatus
	 * @Description: Returns an array of all the sale satatus in DB.
	 * @Params: $db: DB connection
	 * @Returns: An array of free sales.
	 */

	public static function getSalesStatus($db) {
		$sql = "SHOW COLUMNS FROM sales LIKE 'status_sal'";
		$result = $db->Execute($sql);

		$type = $result->fields[1];
		$type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
		return $type;
	}

	public static function checkSaleHaveOtherDiscount($db, $sId) {
		$sql = "SELECT 1 FROM cupon WHERE consuming_sal='$sId'";

		$result = $db->getOne($sql);
		if ($result == 1)
			return 1;
		else
			return 0;
	}

	/*
	 * @Name: getSalesTypes
	 * @Description: Returns an array of all the sale types in DB.
	 * @Params: $db: DB connection
	 * @Returns: An array of free sales.
	 */

	public static function getSalesTypes($db) {
		$sql = "SHOW COLUMNS FROM sales LIKE 'type_sal'";
		$result = $db->Execute($sql);

		$type = $result->fields[1];
		$type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
		return $type;
	}

	/*
	 * @Name: cardNumberExists
	 * @Description: Returns true or false if the card exists or not
	 * @Params: -
	 * @Returns: True/False
	 */

	function getDataByCardNumber() {
		if ($this->card_number_sal != NULL) {
			$sql = "SELECT DISTINCT s.serial_sal,
                                    s.serial_cus,
                                    s.serial_pbd,
                                    s.serial_cit,
                                    s.serial_cnt,
                                    s.serial_inv,
                                    IFNULL(s.card_number_sal,trl.card_number_trl) as card_number_sal,
                                    DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y %T'),
                                    DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y'),
                                    DATE_FORMAT(s.end_date_sal, '%d/%m/%Y'),
                                    DATE_FORMAT(s.in_date_sal, '%d/%m/%Y %T'),
                                    s.days_sal,
                                    s.fee_sal,
                                    s.observations_sal,
                                    s.cost_sal,
                                    s.free_sal,
                                    s.id_erp_sal,
                                    s.status_sal,
                                    s.type_sal,
                                    s.stock_type_sal,
                                    s.total_sal,
                                    s.direct_sale_sal,
                                    s.total_cost_sal,
                                    s.change_fee_sal,
                                    cit.name_cit,
                                    cou.name_cou,
                                    cit2.name_cit,
                                    cou2.name_cou,
                                    pbl.name_pbl,
                                    pbl.serial_pbl,
                                    d.name_dea,
									s.international_fee_status,
									s.international_fee_used,
									s.international_fee_amount,
									MAX(trl.serial_trl) AS 'serial_trl'
                            FROM sales s
                            JOIN customer c ON c.serial_cus=s.serial_cus
                            LEFT JOIN city cit ON cit.serial_cit=s.serial_cit
                            JOIN city cit2 ON cit2.serial_cit=c.serial_cit
                            LEFT JOIN country cou ON cou.serial_cou=cit.serial_cou
                            JOIN country cou2 ON cou2.serial_cou=cit2.serial_cou
                            JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
                            JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
                            JOIN product p ON p.serial_pro=pxc.serial_pro
                            JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang = '" . $_SESSION['serial_lang'] . "'
                            LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal
                            JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
                            JOIN dealer d ON cnt.serial_dea=d.serial_dea
                            WHERE s.card_number_sal ='" . $this->card_number_sal . "'
                            OR trl.card_number_trl ='" . $this->card_number_sal . "'";


			$result = $this->db->Execute($sql);
			if ($result === false)
				return false;

			if ($result->fields[0]) {
				$this->serial_sal = $result->fields[0];
				$this->serial_cus = $result->fields[1];
				$this->serial_pbd = $result->fields[2];
				$this->serial_cit = $result->fields[3];
				$this->serial_cnt = $result->fields[4];
				$this->serial_inv = $result->fields[5];
				$this->card_number_sal = $result->fields[6];
				$this->emission_date_sal = $result->fields[7];
				$this->begin_date_sal = $result->fields[8];
				$this->end_date_sal = $result->fields[9];
				$this->in_date_sal = $result->fields[10];
				$this->days_sal = $result->fields[11];
				$this->fee_sal = $result->fields[12];
				$this->observations_sal = $result->fields[13];
				$this->cost_sal = $result->fields[14];
				$this->free_sal = $result->fields[15];
				$this->id_erp_sal = $result->fields[16];
				$this->status_sal = $result->fields[17];
				$this->type_sal = $result->fields[18];
				$this->stock_type_sal = $result->fields[19];
				$this->total_sal = $result->fields[20];
				$this->direct_sale_sal = $result->fields[21];
				$this->total_cost_sal = $result->fields[22];
				$this->change_fee_sal = $result->fields[23];
				$this->aux['destinationCity'] = $result->fields[24];
				$this->aux['destinationCountry'] = $result->fields[25];
				$this->aux['customerCity'] = $result->fields[26];
				$this->aux['customerCountry'] = $result->fields[27];
				$this->aux['product'] = $result->fields[28];
				$this->aux['serial_pbl'] = $result->fields[29];
				$this->aux['name_dea'] = $result->fields[30];
				$this->international_fee_status = $result->fields[31];
				$this->international_fee_used = $result->fields[32];
				$this->international_fee_amount = $result->fields[33];
				$this->aux['serial_trl'] = $result->fields[34];

				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static function getAssistanceDataByCardNumber($db, $card_number) {
		if (TravelerLog::cardNumberExistInTravelerLog($db, $card_number)) {
			$join_sales = "JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd";
			$join_traveler_log = "JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal 
									AND trl.card_number_trl LIKE '$card_number'";
		} else {
			$join_sales = "JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd AND sal.card_number_sal LIKE '$card_number'";
			$join_traveler_log = "LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal";
		}

		$sql = "SELECT sal.serial_sal, trl.serial_trl, HOUR(TIMEDIFF(NOW(),sal.emission_date_sal)) as days_since_emition,
					   sal.status_sal, inv.status_inv, DATEDIFF(inv.due_date_inv,NOW()) as days_to_dueDate, pbl.name_pbl,
					   CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as name_cnt, dea.name_dea, dea.dea_serial_dea, cou.name_cou,
					   man.name_man
				FROM sales sal
				$join_traveler_log
				LEFT JOIN invoice inv ON inv.serial_inv = sal.serial_inv
				$join_sales
                JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
                JOIN product_by_language pbl ON pbl.serial_pro=pxc.serial_pro AND pbl.serial_lang = '" . $_SESSION['serial_lang'] . "'
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN user usr ON usr.serial_usr = cnt.serial_usr
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
				JOIN manager man ON man.serial_man = mbc.serial_man
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou";
		//die('<pre>'.$sql.'</pre>');
		$result = $db->getAll($sql);

		if ($result[0]) {
			return $result[0];
		} else {
			return FALSE;
		}
	}

	/*
	 * @Name: cardNumberExists
	 * @Description: Returns true or false if the card exists or not
	 * @Params: -
	 * @Returns: True/False
	 */

	public static function cardNumberExists($db, $card_number) {
		$sql = "SELECT IFNULL(s.card_number_sal,trl.card_number_trl) as card_number_sal
                 FROM sales s
                 LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal
                 WHERE s.card_number_sal = '" . $card_number . "'
                 OR trl.card_number_trl = '" . $card_number . "'";
		//die($sql);
		$result = $db->Execute($sql);

		if ($result->fields[0]) {
			return $result->fields[0];
		} else {
			return false;
		}
	}

	/*
	 * @Name: getCardBenefits
	 * @Description: Returns all the benefits that covers a card
	 * @Params: -
	 * @Returns: array
	 */

	public static function getCardBenefits($db, $card_number) {
		$sql = "(
				SELECT DISTINCT bbp.*, cbl.description_cbl as alias_con, bbl.description_bbl, cur.symbol_cur,
						con.alias_con
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN benefits_product bbp ON bbp.serial_pro = pxc.serial_pro AND bbp.serial_cur='1'
				JOIN conditions con ON con.serial_con = bbp.serial_con
				JOIN benefits_by_language bbl ON bbl.serial_ben = bbp.serial_ben AND bbl.serial_lang = {$_SESSION['serial_lang']}
				JOIN conditions_by_language cbl ON cbl.serial_con = bbp.serial_con AND cbl.serial_lang = {$_SESSION['serial_lang']}
				JOIN currency cur ON cur.serial_cur = bbp.serial_cur
				WHERE s.card_number_sal = $card_number
			) UNION (
				SELECT DISTINCT bbp.*, cbl.description_cbl as alias_con, bbl.description_bbl, cur.symbol_cur,
						con.alias_con
				FROM sales s
				JOIN traveler_log trl ON trl.serial_sal = s.serial_sal
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN benefits_product bbp ON bbp.serial_pro = pxc.serial_pro AND bbp.serial_cur='1'
				JOIN conditions con ON con.serial_con = bbp.serial_con
				JOIN benefits_by_language bbl ON bbl.serial_ben = bbp.serial_ben AND bbl.serial_lang = {$_SESSION['serial_lang']}
				JOIN conditions_by_language cbl ON cbl.serial_con = bbp.serial_con AND cbl.serial_lang = {$_SESSION['serial_lang']}
				JOIN currency cur ON cur.serial_cur = bbp.serial_cur
				WHERE trl.card_number_trl = $card_number
			)";
		
		//die(Debug::print_r($sql));
		
		$result = $db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/*
	 * @Name: getCardBeneficiaries
	 * @Description: Returns all the beneficiaries covered by a card
	 * @Params: -
	 * @Returns: array
	 */

	public static function getCardBeneficiaries($db, $card_number) {

		$pos = strpos($card_number, '_');

		if ($pos === false) {
			$sql = "SELECT c.*, DATE_FORMAT(c.birthdate_cus,'%d/%m/%Y') as b_date, cou.name_cou, cit.name_cit,
                       IF(tl.start_trl ,DATE_FORMAT(MAX(tl.start_trl) ,'%d/%m/%Y'),DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y')) as start_date, IF(tl.end_trl,DATE_FORMAT(tl.end_trl,'%d/%m/%Y'),DATE_FORMAT(s.end_date_sal,'%d/%m/%Y')) as end_date,
                       IFNULL(cit_des.name_cit,cit_trl.name_cit) as city_des, IFNULL(cou_des.name_cou,cou_trl.name_cou) as country_des, tl.serial_trl
                FROM sales s
                JOIN customer c ON c.serial_cus = s.serial_cus
                JOIN city cit ON cit.serial_cit = c.serial_cit
                JOIN country cou ON cou.serial_cou = cit.serial_cou
                LEFT JOIN city cit_des ON cit_des.serial_cit = s.serial_cit
                LEFT JOIN country cou_des ON cou_des.serial_cou = cit_des.serial_cou
                LEFT JOIN traveler_log tl ON tl.serial_sal= s.serial_sal
                LEFT JOIN city cit_trl ON cit_trl.serial_cit = tl.serial_cit
                LEFT JOIN country cou_trl ON cou_trl.serial_cou = cit_trl.serial_cou
                WHERE s.card_number_sal LIKE '$card_number'
                OR tl.card_number_trl LIKE '$card_number'
                GROUP BY c.serial_cus

                UNION";
		} else {
			$sql = "";
		}
		$sql.=" SELECT c.*, DATE_FORMAT(c.birthdate_cus,'%d/%m/%Y') as b_date, cou.name_cou, cit.name_cit,
                       DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as start_date, DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date,
                       cit_des.name_cit as city_des, cou_des.name_cou as country_des, e.serial_trl
                FROM extras e
                JOIN customer c ON c.serial_cus = e.serial_cus
                JOIN city cit ON cit.serial_cit = c.serial_cit
                JOIN country cou ON cou.serial_cou = cit.serial_cou
                JOIN sales s ON s.serial_sal = e.serial_sal
                LEFT JOIN city cit_des ON cit_des.serial_cit = s.serial_cit
                LEFT JOIN country cou_des ON cou_des.serial_cou = cit_des.serial_cou
                WHERE s.card_number_sal LIKE '$card_number'

				UNION

				SELECT c.*, DATE_FORMAT(c.birthdate_cus,'%d/%m/%Y') as b_date, cou.name_cou, cit.name_cit,
					   DATE_FORMAT(MAX(tl.start_trl) ,'%d/%m/%Y') as start_date, DATE_FORMAT(tl.end_trl,'%d/%m/%Y') as end_date,
					   cit_trl.name_cit as city_des, cou_trl.name_cou as country_des, tl.serial_trl
				FROM sales s
				JOIN traveler_log tl ON tl.serial_sal= s.serial_sal
				JOIN customer c ON c.serial_cus = tl.serial_cus
				JOIN city cit ON cit.serial_cit = c.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou
				JOIN city cit_trl ON cit_trl.serial_cit = tl.serial_cit
				JOIN country cou_trl ON cou_trl.serial_cou = cit_trl.serial_cou
				WHERE tl.card_number_trl LIKE '$card_number'
				GROUP BY c.serial_cus";
		//die(Debug::print_r($sql));
		$result = $db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/*
	 * @Name: daysSinceEmission
	 * @Description: Checks how many hours have pased since card emission
	 * @Params: -
	 * @Returns: boolean
	 */

	function daysSinceEmission() {
		$sql = "SELECT HOUR(TIMEDIFF(NOW(),s.emission_date_sal))
                FROM sales s
                LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal
                WHERE s.card_number_sal = '" . $this->card_number_sal . "'
                OR trl.card_number_trl = '" . $this->card_number_sal . "'";
		//die($sql);
		$result = $this->db->getOne($sql);
		if ($result === false)
			die("failed daysSinceEmission");
		if ($result)
			return $result;
		else
			return NULL;
	}

	/*	 * *********************************************
	 * function validateRegisterCard
	  @Name: validateRegisterCard
	  @Description: Checks if a card available for travels register
	  @Params:
	 *   $this->card_number_sal: object atribute for card_number_sal
	  @Returns: true: when is executive
	 *    false: when is not executive
	 * ********************************************* */

	function validateRegisterCard() {
		$sql = "SELECT 1
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			JOIN product p ON p.serial_pro = pxc.serial_pro
			LEFT JOIN traveler_log trl ON trl.serial_sal=s.serial_sal
			WHERE (s.card_number_sal={$this->card_number_sal}
				   OR trl.card_number_trl={$this->card_number_sal})
			AND p.flights_pro=0";
                //die($sql);
		$result = $this->db->getOne($sql);
		if ($result)
			return true;
		else
			return false;
	}

	/*	 * *********************************************
	 * function getCardInfo
	  @Name: getCardInfo
	  @Description: Returns an array with the info of a sale , based on card_number_sal
	  @Params:
	 *   object attribute: card_number_sal
	  @Returns: array: with all info
	 *    false: when there is no info for the card_number given
	 * ********************************************* */

	function getCardInfo($register = true) {
		if(!$register){
			$report_filter = "AND s.end_date_sal >= CURDATE()";
		}

		$sql = "SELECT s.serial_sal, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name,
					pbl.name_pbl, s.card_number_sal, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as begin_date_sal,
					DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal, s.days_sal, 
					IF(p.limit_pro='YES',(s.days_sal-IFNULL(SUM(tl.days_trl),0)),s.days_sal) as available_days,
					s.status_sal
				FROM sales s
				JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro = pxc.serial_pro AND p.flights_pro=0
				JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro
				JOIN customer cus ON cus.serial_cus=s.serial_cus
				LEFT JOIN traveler_log tl ON tl.serial_sal=s.serial_sal AND tl.status_trl <> 'DELETED'
				WHERE (s.card_number_sal={$this->card_number_sal}
					OR tl.card_number_trl={$this->card_number_sal})
			$report_filter 
			GROUP BY s.serial_sal
			ORDER BY s.begin_date_sal";
		//echo $sql;
		$result = $this->db->getRow($sql);
		if ($result)
			return $result;
		else
			return false;
	}

	/*	 * *********************************************
	 * function getAvailableDays
	  @Name: getAvailableDays
	  @Description: returns the available days for a card by serial_sal
	  @Params:
	 *   object attribute: serial_sal
	  @Returns: int: number of days available
	 * ********************************************* */

	function getAvailableDays() {
		$sql = "SELECT IF(p.limit_pro='YES',(s.days_sal-IFNULL(SUM(tl.days_trl),0)),s.days_sal)as available_days
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			JOIN product p ON p.serial_pro = pxc.serial_pro AND p.flights_pro=0
			LEFT JOIN traveler_log tl ON tl.serial_sal=s.serial_sal AND status_trl <> 'DELETED'
			WHERE s.serial_sal={$this->serial_sal}
			GROUP BY s.serial_sal";
		//echo $sql;
		$result = $this->db->getOne($sql);
		if ($result >= 0)
			return $result;
		else
			return false;
	}

	/*	 * *********************************************
	 * function getSaleDates
	  @Name: getSaleDates
	  @Description: Returns an array with all sales of a customer and the begin and end dates
	  @Params:
	 *   $serial_cus: object atribute for card_number_sal
	  @Returns: true: when is executive
	 *    false: when is not executive
	 * ********************************************* */

	function getSaleDates($serial_cus, $start_dt, $end_dt) {
		$sql = "SELECT 1
			FROM sales s
			WHERE s.serial_cus='$serial_cus'
			AND((STR_TO_DATE('$start_dt','%d/%m/%Y') BETWEEN  DATE_ADD(s.begin_date_sal,INTERVAL 1 DAY) AND SUBDATE(s.end_date_sal,INTERVAL 1 DAY))
		     OR (STR_TO_DATE('$end_dt','%d/%m/%Y') BETWEEN  DATE_ADD(s.begin_date_sal,INTERVAL 1 DAY) AND   SUBDATE(s.end_date_sal,INTERVAL 1 DAY))
			 OR (STR_TO_DATE('$start_dt','%d/%m/%Y')=s.begin_date_sal AND STR_TO_DATE('$end_dt','%d/%m/%Y')=s.end_date_sal)
		     OR (s.begin_date_sal BETWEEN DATE_ADD(STR_TO_DATE('$start_dt','%d/%m/%Y'),INTERVAL 1 DAY) AND SUBDATE(STR_TO_DATE('$end_dt','%d/%m/%Y'),INTERVAL 1 DAY))
			 OR (s.end_date_sal BETWEEN DATE_ADD(STR_TO_DATE('$start_dt','%d/%m/%Y'),INTERVAL 1 DAY) AND SUBDATE(STR_TO_DATE('$end_dt','%d/%m/%Y'),INTERVAL 1 DAY)))
			AND (s.status_sal='ACTIVE' OR s.status_sal='REGISTERED' OR s.status_sal='REQUESTED' OR s.status_sal='STANDBY')";

		//echo $sql;
		$result = $this->db->getOne($sql);
		if ($result == 1)
			return true;
		else
			return false;
	}

	/*	 * *********************************************
	 * @Name: isDirectSale
	 * @Description: Verifies if the sale was made by a Planet Assist Official Dealer.
	 * @Params: $serial_cnt
	 * @Returns: true: when is Direct Sale
	 *          false: otherwise.
	 * ********************************************* */

	function isDirectSale($serial_cnt) {
		$sql = "SELECT mbc.serial_mbc
			  FROM counter c 
			  JOIN dealer b ON b.serial_dea=c.serial_dea
			  JOIN dealer d ON d.serial_dea=b.dea_serial_dea AND d.official_seller_dea='YES'
			  JOIN manager_by_country mbc ON mbc.serial_mbc=d.serial_mbc AND mbc.official_mbc='YES'
			  WHERE c.serial_cnt='" . $serial_cnt . "'";
		//echo $sql;
		$result = $this->db->Execute($sql);

		if ($result->fields[0]) {
			return 'YES';
		} else {
			return 'NO';
		}
	}

	/*	 * *********************************************
	 * @Name: isRefundable
	 * @Description: Verifies if a card is refundable. The conditions for a card to be
	 *               refundable is that the sale is completly paid.
	 * @Params: $serial_sal: object atribute for serial_sal
	 * @Returns: true: when is executive
	 * 			false: when is not executive
	 * ********************************************* */

	public static function isRefundable($db, $serial_sal) {
		$sql = "SELECT serial_sal
			FROM sales s
			WHERE s.serial_sal='" . $serial_sal . "'
			AND s.status_sal='STANDBY'";

		$result = $db->Execute($sql);

		if ($result->fields['0']) {
			return true;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * @Name: hasTravelsRegistered
	 * @Description: Verifies if the card permits has any travel registered in the traveler´s log.
	 * @Params: $serial_sal: object atribute for serial_sal
	 * @Returns: true: if ther's any travel log.
	 * 			false: otherwise.
	 * ********************************************* */

	public static function hasTravelsRegistered($db, $serial_sal) {
		$sql = "SELECT s.serial_sal
				FROM sales s
				JOIN traveler_log trl ON trl.serial_sal=s.serial_sal
				WHERE s.serial_sal=" . $serial_sal."
				AND trl.status_trl <> 'DELETED'";
		
		$result = $db->Execute($sql);

		if ($result->fields['0']) {
			return true;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * function changeStatus
	 * change the status of an specific invoice
	 * ********************************************* */

	function changeStatus($status) {
		$sql = "UPDATE sales SET status_sal='" . $status . "'
                  WHERE serial_sal=" . $this->serial_sal;
		$result = $this->db->Execute($sql);
		if ($result === false)
			return false;

		if ($result == true)
			return true;
		else
			return false;
	}

	/*	 * *********************************************
	 * function getGeneralConditionsbySale
	  @Name: getGeneralConditionsbySale
	  @Description: Returns the url where is the General Conditions file for the product sold
	  @Params:
	 *   $serial_pbd: serial product by dealer
	 * 	 $serial_sal: serial of the sale
	  @Returns: true: url
	 *    false: when is not executive
	 * ********************************************* */

	function getGeneralConditionsbySale($serial_pbd, $serial_sal, $serial_cou, $serial_lang) {
		$sql = "	SELECT DISTINCT cbp.serial_gcn, cbp.serial_pro, s.serial_pbd, glc.url_glc
					FROM conditions_by_product cbp
					JOIN general_conditions gcn ON gcn.serial_gcn = cbp.serial_gcn AND gcn.status_gcn = 'ACTIVE'
					JOIN product_by_country pxc ON pxc.serial_pro = cbp.serial_pro
					JOIN product_by_dealer pbd ON pbd.serial_pxc = pxc.serial_pxc
					JOIN sales s ON s.serial_pbd = pbd.serial_pbd AND s.serial_pbd = $serial_pbd
						AND s.serial_sal = $serial_sal
					JOIN gconditions_by_language_by_country glc ON glc.serial_gcn = cbp.serial_gcn
						AND glc.serial_cou = $serial_cou
						AND glc.serial_lang = $serial_lang";
		//die($sql);
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$list = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$list[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $list;
	}

	/**
	 * */
	function getSaleUserMail($serial_sal) {
		$sql = "
                    SELECT usr.email_usr
                    FROM sales sal
                    JOIN counter cnt ON sal.serial_cnt = cnt.serial_cnt
                    JOIN dealer dea ON cnt.serial_dea = dea.serial_dea
                    JOIN sector sec ON dea.serial_sec = sec.serial_sec
                    JOIN city cit ON sec.serial_cit = cit.serial_cit
                    JOIN country cou ON cit.serial_cou = cou.serial_cou
                    JOIN comissionist_by_country cbc ON cou.serial_cou = cbc.serial_cou
                    JOIN user usr ON cbc.serial_usr = usr.serial_usr
                    WHERE sal.serial_sal ='" . $serial_sal . "'
                    LIMIT 0,1
                    ";

		$result = $this->db->getOne($sql);
		if ($result) {
			$this->aux = $result;
			return true;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * function validateCardForModify
	  @Name: validateCardForModify
	  @Description: Return if a card number is valid to register travels, or is a registered travel card
	  @Params:
	 *   $card_number: the card number to be checked
	 *   $for_validation: when true returns true or false, otherwise returns,'sale', 'travel_log' OR false
	  @Returns:
	 *    for_validation = true:	true:when found
	 * 										false:when not found
	 * 							   false:	'sale': when found on sales table
	 * 										'travel_log' when found on traveler_log table
	 * 										false: when not found
	 *
	 * ********************************************* */

	function validateCardForModify($card_number, $for_validation=true) {
		$sql = "SELECT 1
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			JOIN product p ON p.serial_pro = pxc.serial_pro
			WHERE s.card_number_sal=$card_number
			AND p.flights_pro=0
			AND p.masive_pro='NO'";
		$result = $this->db->getOne($sql);
		if ($result) {
			if ($for_validation) {
				return true;
			} else {
				return 'sale';
			}
		} else {
			$sql = "SELECT 1
			FROM traveler_log t
			WHERE t.card_number_trl=$card_number
			AND t.status_trl='ACTIVE'
			";
			$result = $this->db->getOne($sql);
			if ($result) {
				if ($for_validation) {
					return true;
				} else {
					return 'travel_log';
				}
			} else {
				return false;
			}
		}
	}

	/*	 * *********************************************
	 * function getCardDaysAvailability
	  @Name: getCardDaysAvailability
	  @Description: Get card availability data.
	  @Params:
	 *       $this->serial_sal: object atribute for serial_sal
	  @Returns:     array of results
	 *        false when no results
	 * ********************************************* */

	function getCardDaysAvailability() {
		$sql = "SELECT s.serial_sal,pbl.name_pbl,s.card_number_sal,DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as begin_date_sal,DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as end_date_sal,s.days_sal,IF(p.limit_pro='YES',(s.days_sal-IFNULL(SUM(tl.days_trl),0)),s.days_sal)as available_days
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			JOIN product p ON p.serial_pro = pxc.serial_pro AND p.flights_pro=0
			JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro
			LEFT JOIN traveler_log tl ON tl.serial_sal=s.serial_sal
			WHERE s.serial_sal={$this->serial_sal}
			AND s.end_date_sal >= CURDATE()
			GROUP BY s.serial_sal";
		$result = $this->db->getRow($sql);
		if ($result)
			return $result;
		else
			return false;
	}

	/*
	 * getCardsByCustomerReport
	 * @param: $db - DB connection
	 * @param: $serial_cus - Customer ID
	 * @return: An array of all customers that bought any Planet Assist card.
	 */

	public static function getCardsByCustomerReport($db, $serial_cus, $own_branch_cards_only = FALSE, $exclude_free = FALSE) {
		if ($own_branch_cards_only) {
			$restrict_own_cards = " JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND cnt.serial_dea = " . $_SESSION['serial_dea'];
		}
		
		if($exclude_free):
			$exclude_free_statement = " AND s.status_sal NOT IN ('REQUESTED','DENIED','REFUNDED','BLOCKED')";
		endif;

		$sql = "SELECT DISTINCT * FROM 
				(
					(SELECT DISTINCT s.card_number_sal, s.sal_serial_sal, pbl.name_pbl, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') AS 'begin_date_sal',
						 DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') AS 'end_date_sal' , s.status_sal, s.serial_sal, 
						 s.serial_cnt, s.serial_inv, trl.serial_trl, s.free_sal, trl.card_number_trl, s.serial_con
				  FROM customer c
				  JOIN sales s ON s.serial_cus=c.serial_cus	
				  $restrict_own_cards
				  $exclude_free_statement
				  LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal AND trl.serial_cus = '" . $serial_cus . "'
				  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				  JOIN product_by_language pbl ON pbl.serial_pro=pxc.serial_pro AND pbl.serial_lang='" . $_SESSION['serial_lang'] . "'
				  WHERE c.serial_cus = '" . $serial_cus . "') 


					UNION

					(SELECT DISTINCT s.card_number_sal, s.sal_serial_sal, pbl.name_pbl, DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') AS 'begin_date_sal',
						 DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') AS 'end_date_sal' , s.status_sal, s.serial_sal,
						 s.serial_cnt, s.serial_inv, trl.serial_trl, s.free_sal, trl.card_number_trl, s.serial_con
				  FROM customer c
				  LEFT JOIN traveler_log trl ON trl.serial_cus=c.serial_cus AND trl.serial_cus = '" . $serial_cus . "'
				  LEFT JOIN sales s ON trl.serial_sal = s.serial_sal
				  $restrict_own_cards
				  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				  JOIN product_by_language pbl ON pbl.serial_pro=pxc.serial_pro AND pbl.serial_lang='" . $_SESSION['serial_lang'] . "'
				  WHERE c.serial_cus = '" . $serial_cus . "')

				) as tbl";
//die('<pre>'.$sql.'</pre>');
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	public static function getMasiveCardsByCardNumber($db, $cardNumber, $own_branch_cards_only = FALSE) {
		if ($own_branch_cards_only) {
			$restrict_own_cards = " JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND cnt.serial_dea = " . $_SESSION['serial_dea'];
		}
		$sql = "SELECT s.serial_sal, s.card_number_sal, pbl.name_pbl, s.sal_serial_sal, s.free_sal, s.status_sal
			    FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro=pxc.serial_pro
				JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang = '" . $_SESSION['serial_lang'] . "'
				$restrict_own_cards
				WHERE s.card_number_sal LIKE '$cardNumber'";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	public static function getNormalCardsByCardNumber($db, $cardNumber, $own_branch_cards_only = FALSE){
		if ($own_branch_cards_only) {
			$restrict_own_cards = " JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND cnt.serial_dea = " . $_SESSION['serial_dea'];
		}
		
		$sql = "SELECT	s.serial_sal, s.card_number_sal, pbl.name_pbl, s.free_sal, s.status_sal, 
						s.serial_cnt, MAX(trl.serial_trl) AS 'serial_trl', s.serial_con
			    FROM sales s
				LEFT JOIN traveler_log trl ON trl.serial_sal = s.serial_sal AND status_trl <> 'DELETED'
				JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd AND s.status_sal NOT IN ('REQUESTED','DENIED','REFUNDED','BLOCKED')
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro=pxc.serial_pro
				JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro AND pbl.serial_lang = '" . $_SESSION['serial_lang'] . "'
				$restrict_own_cards
				WHERE s.card_number_sal LIKE '$cardNumber' OR trl.card_number_trl LIKE '$cardNumber'";
		
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	/*
	 * getCardReport
	 * @param: $db - DB connection
	 * @param: $serial_cus - Customer ID
	 * @return: An array of all customers that bought any Planet Assist card.
	 */

	public static function getCardReport($db, $serial_cou, $date_type, $begin_date, $end_date, $serial_cit=NULL, $serial_mbc=NULL, $serial_usr=NULL, $dea_serial_dea=NULL, $serial_dea=NULL, $serial_pro=NULL, $type_sal=NULL, $status_sal=NULL, $operator=NULL, $total_sal=NULL, $stock_type=NULL, $order_by=NULL) {

		if ($order_by == NULL || $order_by == "card_number") {
			$order_by = "col1";
		} else {
			$order_by = "col2";
		}

        $sql = "SELECT  DISTINCT s.serial_sal,
						IFNULL(card_number_sal, 'N/A') as col1,
        				DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y') as col2,
        				CONCAT(first_name_cus,' ',last_name_cus) as col3,
                     	pbl.name_pbl as col4,
                     	DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as col5, 
                     	DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as col6,
                     	DATEDIFF(s.end_date_sal,s.begin_date_sal)+1 as col7,
                     	s.total_sal as col8,
                     	IF(s.free_sal = 'YES', 'FREE', s.status_sal) as col9,
                     	dea.name_dea as col10,
                     	CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as col11,
                     	s.stock_type_sal as col12,
						cus.document_cus,
						DATE_FORMAT(cus.birthdate_cus, '%d/%m/%Y') AS 'birthdate_cus',
						cou.name_cou AS 'name_country'
				FROM sales s
				LEFT JOIN city cit_dest ON cit_dest.serial_cit = s.serial_cit
				LEFT JOIN country cou ON cit_dest.serial_cou = cou.serial_cou
				JOIN customer cus ON cus.serial_cus = s.serial_cus AND s.status_sal <> 'DENIED'
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN user usr ON usr.serial_usr = cnt.serial_usr
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				$refund_join
				WHERE cit.serial_cou='$serial_cou'";

		if ($date_type == "insystem") {
			$sql .= " AND (STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y'))";
		} else if ($date_type == "coverage") {
			$sql .= " AND STR_TO_DATE(DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')
                      ";
		}
		//AND STR_TO_DATE(DATE_FORMAT(s.end_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')) Se 
		if ($serial_cit != NULL) {
			$sql.=" AND cit.serial_cit = '$serial_cit'";
		}
		if ($serial_mbc != NULL) {
			$sql.=" AND dea.serial_mbc = '$serial_mbc'";
		}
		if ($serial_usr != NULL) {
			$sql.=" AND ubd.serial_usr = '$serial_usr' AND ubd.status_ubd = 'ACTIVE'";
		}
		if ($dea_serial_dea != NULL) {
			$sql.=" AND dea.dea_serial_dea = '$dea_serial_dea'";
		}
		if ($serial_dea != NULL) {
			$sql.=" AND dea.serial_dea = '$serial_dea'";
		}
		if ($serial_pro != NULL) {
			$sql.=" AND pxc.serial_pro = '$serial_pro'";
		}
		if ($type_sal != NULL) {
			$sql.=" AND s.type_sal = '$type_sal'";
		}
		if ($total_sal != NULL) {
			$sql.=" AND s.total_sal " . $operator . $total_sal;
		}
		if ($stock_type != NULL) {
			$sql.=" AND s.stock_type_sal = '$stock_type'";
		}
		if ($status_sal != NULL) {
			$sql .= " AND s.status_sal IN ($status_sal)";
		}
		$sql.=" ORDER BY $order_by";

//		echo $sql;
//		die('<pre>'.$sql.'</pre>');
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}
        
        public static function getKitsReport($db,$manager=NULL,$responsible=NULL,$dealer=null,$branch=null,$counter=NULL,$begin_date=null,$end_date=null) {
            $sql = "SELECT IFNULL(s.card_number_sal, 'N/A') AS 'card_number', CONCAT(first_name_cus,' ',last_name_cus) as 'customer', 
                concat(u.first_name_usr, ' ', u.last_name_usr) as 'responsible', d.name_dea as 'dealer', b.name_dea as 'branch', 
                concat(uc.first_name_usr, ' ', uc.last_name_usr) as 'counter',
                pbl.name_pbl as 'product', IF(s.deliveredKit, 'Si','No') as 'deliveredKit'
            FROM sales s
            JOIN customer cus ON cus.serial_cus = s.serial_cus AND s.status_sal <> 'DENIED'
            JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt"; $counter? $sql .= " AND cnt.serial_cnt = '{$counter}' " : " ";
            $sql .= " JOIN user uc ON uc.serial_usr = cnt.serial_usr
            JOIN dealer b ON b.serial_dea = cnt.serial_dea AND b.status_dea = 'ACTIVE'"; $manager? $sql .= " AND b.serial_mbc = '{$manager}' " : " "; 
            $branch? $sql .= " AND b.serial_dea = '{$branch}' " : " ";
            $sql .= " JOIN dealer d ON d.serial_dea = b.dea_serial_dea AND d.status_dea = 'ACTIVE'"; $dealer? $sql .= " AND d.serial_dea = '{$dealer}' " : " ";
            $sql .= "JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE'
            JOIN user u ON u.serial_usr = ubd.serial_usr "; $responsible? $sql .= " AND u.serial_usr = '{$responsible}' " : " ";
            $sql .= " JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
            JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
            JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = '{$_SESSION['serial_lang']}'
            WHERE (STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')) ORDER BY s.emission_date_sal";
            
          //echo $sql;
//          die('<pre>'.$sql.'</pre>');
            $result = $db->getAll($sql);

            if ($result) {
                    return $result;
            } else {
                    return false;
            }
        }

	/*	 * *********************************************
	 * function getFreeSAlesByZone
	 * gets information about free sales by zone and country
	 * ********************************************* */

	function getFreeSalesByZone($serial_zon, $serial_cou, $serial_mbc_user, $dea_serial_dea_user, $serial_mbc=NULL, $serial_cit=NULL, $serial_dea=NULL, $serial_bra=NULL, $date_from=NULL, $date_to=NULL, $status=NULL) {
		$managerSQL = '';
		$citySQL = '';
		$dealerSQL = '';
		$branchSQL = '';
		$datesSQL = '';
		$statusSQL = '';

		if ($serial_mbc) {
			$managerSQL = "AND mbc.serial_mbc =" . $serial_mbc;
		}
		if ($serial_cit) {
			$citySQL = "AND cit.serial_cit = '" . $serial_cit . "'";
		}
		if ($serial_dea) {
			$dealerSQL = " AND dea.serial_dea = '" . $serial_dea . "'";
		}
		if ($serial_bra) {
			$branchSQL = "AND br.serial_dea = '" . $serial_bra . "'";
		}
		if ($date_from && $date_to) {
			$datesSQL = " AND DATE_FORMAT(sal.in_date_sal,'%Y/%m/%d') BETWEEN STR_TO_DATE('$date_from', '%d/%m/%Y') AND STR_TO_DATE('$date_to', '%d/%m/%Y')";
		}
		if ($status) {
			$statusSQL = "AND sal.status_sal = '" . $status . "'";
		}
		$sql = "SELECT sal.serial_sal, br.serial_dea AS serial_bra, zon.name_zon, cou.name_cou, cit.name_cit, man.name_man, dea.name_dea, br.name_dea AS name_bra,
						CONCAT(usr.first_name_usr,' ',usr.last_name_usr) AS name_usr, DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y') AS date_sal,
						IFNULL(sal.card_number_sal,'N/A') AS card_number_sal, sal.days_sal, sal.fee_sal,
						sal.total_cost_sal, IFNULL(co.name_cou,'N/A') AS destination, pbl.name_pbl
				 FROM sales sal
					   JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
					   JOIN dealer br ON cnt.serial_dea=br.serial_dea $branchSQL
					   JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea $dealerSQL AND dea.status_dea = 'ACTIVE'
					   JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea 
					   JOIN user usr ON usr.serial_usr = ubd.serial_usr AND ubd.status_ubd='ACTIVE'
					   JOIN manager_by_country AS mbc ON mbc.serial_mbc = br.serial_mbc  $managerSQL
					   JOIN manager AS man ON man.serial_man = mbc.serial_man
					   JOIN sector sec ON br.serial_sec=sec.serial_sec
					   JOIN city cit ON sec.serial_cit=cit.serial_cit $citySQL
					   LEFT JOIN city ci ON ci.serial_cit = sal.serial_cit
					   JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou=" . $serial_cou . "
					   LEFT JOIN country co ON co.serial_cou = ci.serial_cou
					   JOIN zone zon ON zon.serial_zon = cou.serial_zon AND zon.serial_zon=" . $serial_zon . "
					   JOIN product_by_dealer pbd ON sal.serial_pbd=pbd.serial_pbd
					   JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
					   JOIN product pro ON pxc.serial_pro=pro.serial_pro
					   JOIN product_by_language pbl ON pbl.serial_pro=pro.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				   WHERE sal.free_sal = 'YES' $datesSQL $statusSQL
				   ";
		if ($serial_mbc_user != '1') {
			$sql.=" AND mbc.serial_mbc=" . $serial_mbc_user;
		}
		if ($dea_serial_dea_user) {
			$sql.=" AND dea.serial_dea=" . $dea_serial_dea_user;
		}
		$sql.=" ORDER BY name_bra, serial_bra";
		//die('<pre>'.$sql.'</pre>');
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arr = array();
			$cont = 0;

			do {
				$arr[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arr;
	}

	/*
	 * @name: expireCards
	 * @param: $db - DB connection
	 * @returns: TRUE - if all the active paied cards where updated to EXPIRED after their lifetime.
	 *           FALSE- otherwise
	 */

	public static function expireCards($db) {
		$sql = "UPDATE sales SET
				status_sal='EXPIRED'
			  WHERE end_date_sal < NOW()
			  AND (status_sal = 'ACTIVE'
			  OR status_sal = 'REGISTERED')
			  AND serial_inv IS NOT NULL";
		
		$sql_free = "UPDATE sales SET
						status_sal='EXPIRED'
					WHERE STR_TO_DATE(DATE_FORMAT(end_date_sal,'%Y-%m-%d'), '%Y-%m-%d') < CURDATE()
					AND status_sal = 'ACTIVE'
					AND free_sal = 'YES'";

		$result = $db->Execute($sql);
		$result_free = $db->Execute($sql_free);

		if ($result && $result_free) {
			return true;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	  funcion getSalesInRange
	  @Name: getSalesInRange
	  @Description: get all sales in a stock range
	  @Params:
	 *    $db: db object
	 *    $from: range start
	 *    $to: range end
	  @Returns: array of card numbers or false
	 * ********************************************* */

	public static function getSalesInRange($db, $from, $to) {
		if ($db && $from && $to) {
			$sql = "SELECT s.card_number_sal
				  FROM sales s
				  WHERE s.card_number_sal BETWEEN $from AND $to";
			//echo $sql;
			$result = $db->getCol($sql);
			if ($result) {
				return $result;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/*
	 * @name: getSalesStock
	 * @param: $db - DB connection
	 * @returns: An array of all the stock types values in DB
	 */

	public static function getSalesStock($db) {
		$sql = "SHOW COLUMNS FROM sales LIKE 'stock_type_sal'";
		$result = $db->Execute($sql);

		$type = $result->fields[1];
		$type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
		return $type;
	}

	/*
	 * @name: getDealerSalesForChartReport
	 * @param: $db - DB connection
	 * @param: $serial_dea - Dealer ID
	 * @param: $begin_date - The begin of the analisys time as an array in the d/m/Y format
	 * @param: $end_date - The end of the analisys time as an array in the d/m/Y format
	 */

	public static function getDealerSalesForChartReport($db, $serial_dea, $begin_date, $end_date) {
		$sql = "SELECT s.serial_sal, s.total_sal, DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y') AS 'emission_date_sal',
					 (i.discount_prcg_inv + i.other_dscnt_inv) AS 'discounts', i.applied_taxes_inv, pbl.name_pbl,
					 IF(r.serial_ref IS NULL, 'NO', 'YES') AS 'has_refunds', p.serial_pro,
					 IF(STR_TO_DATE(DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y'), '%d/%m/%Y')
					 BETWEEN STR_TO_DATE('" . $begin_date['0'] . "/" . $begin_date['1'] . "/" . $begin_date['2'] . "', '%d/%m/%Y')
					 AND STR_TO_DATE('" . $end_date['0'] . "/" . $end_date['1'] . "/" . $end_date['2'] . "', '%d/%m/%Y'), 'CURRENT', 'PREVIOUS') AS 'type',
					 IFNULL(SUM(spf.estimated_amount_spf),'0') AS 'reserved',
					 IFNULL(SUM(prq.amount_authorized_prq),'0') AS 'total_paid'
			  FROM sales s
			  JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
			  JOIN dealer b ON b.serial_dea=cnt.serial_dea AND b.serial_dea='$serial_dea'
			  JOIN invoice i ON i.serial_inv=s.serial_inv
			  JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
			  JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
			  JOIN product p ON p.serial_pro=pxc.serial_pro
			  JOIN product_by_language pbl ON pbl.serial_pro=p.serial_pro
			  LEFT JOIN refund r ON r.serial_sal=s.serial_sal AND r.status_ref='APROVED'
			  LEFT JOIN file f ON f.serial_sal=s.serial_sal
			  LEFT JOIN payment_request prq ON prq.serial_fle=f.serial_fle AND prq.status_prq='APROVED' AND prq.dispatched_prq='YES'
			  LEFT JOIN service_provider_by_file spf ON spf.serial_fle=f.serial_fle
			  WHERE STR_TO_DATE(DATE_FORMAT(s.in_date_sal , '%d/%m/%Y'), '%d/%m/%Y')
					BETWEEN STR_TO_DATE('" . $begin_date['0'] . "/" . $begin_date['1'] . "/" . $begin_date['2'] . "', '%d/%m/%Y')
					AND STR_TO_DATE('" . $end_date['0'] . "/" . $end_date['1'] . "/" . $end_date['2'] . "', '%d/%m/%Y')
			  OR STR_TO_DATE(DATE_FORMAT(s.in_date_sal , '%d/%m/%Y'), '%d/%m/%Y')
					BETWEEN STR_TO_DATE('" . $begin_date['0'] . "/" . $begin_date['1'] . "/" . ($begin_date['2'] - 1) . "', '%d/%m/%Y')
					AND STR_TO_DATE('" . $end_date['0'] . "/" . $end_date['1'] . "/" . ($end_date['2'] - 1) . "', '%d/%m/%Y')
			GROUP BY s.serial_sal
			ORDER BY type, emission_date_sal";
		//die($sql);
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/*	 * *********************************************
	 * function getSalesAnalysis
	 * gets information for the sale's analysis
	 * ********************************************* */

	function getSalesAnalysis($serial_zon, $serial_cou, $date_from, $date_to, $user_serial_mbc, $user_serial_dea, $serial_mbc=NULL, $serial_cit=NULL, $serial_usr=NULL, $serial_dea=NULL, $serial_bra=NULL, $flag=null) {

		if ($serial_mbc)
			$managerSQL = "AND mbc.serial_mbc =" . $serial_mbc;
		if ($serial_cit)
			$citySQL = "AND cit.serial_cit = '" . $serial_cit . "'";
		if ($serial_dea)
			$dealerSQL = " AND dea.serial_dea = '" . $serial_dea . "'";
		if ($serial_bra)
			$branchSQL = "AND br.serial_dea = '" . $serial_bra . "'";
		if ($serial_usr)
			$userSQL = "AND ubd.serial_usr = '" . $serial_usr . "'";

		$sql = "SELECT DISTINCT(sal.serial_sal), zon.name_zon, cou.name_cou, man.name_man, cit.name_cit,
						CONCAT(usr.first_name_usr,' ',usr.last_name_usr) AS 'name_usr', dea.name_dea,
						br.name_dea AS name_bra, br.percentage_dea, br.type_percentage_dea, sal.total_sal,
						dea.serial_dea, br.serial_dea AS serial_bra, cit.serial_cit, inv.applied_taxes_inv,
						(inv.discount_prcg_inv + inv.other_dscnt_inv) AS 'discounts'
					FROM sales sal
					     JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
					     JOIN dealer br ON cnt.serial_dea=br.serial_dea $branchSQL
					     JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea $dealerSQL
					     JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc $managerSQL
					     JOIN manager man ON man.serial_man=mbc.serial_man
                         JOIN sector sec ON br.serial_sec=sec.serial_sec
      			       	 JOIN city cit ON sec.serial_cit=cit.serial_cit $citySQL
      			       	 JOIN user_by_dealer ubd ON ubd.serial_dea=br.serial_dea AND ubd.status_ubd='ACTIVE' $userSQL
                         JOIN user usr ON usr.serial_usr=ubd.serial_usr
				         JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou=" . $serial_cou . "
					     JOIN zone zon ON zon.serial_zon=cou.serial_zon AND zon.serial_zon=" . $serial_zon . "
					     JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv
					     JOIN payments pay ON pay.serial_pay=inv.serial_pay
                    WHERE sal.free_sal = 'NO' AND sal.status_sal NOT IN ('VOID','DENIED','REFUNDED','BLOCKED')
					        AND sal.serial_inv IS NOT NULL AND inv.serial_pay IS NOT NULL AND pay.status_pay = 'PAID'
							AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$date_from', '%d/%m/%Y') AND STR_TO_DATE('$date_to', '%d/%m/%Y')";

		if ($user_serial_mbc != '1')
			$sql.=" AND mbc.serial_mbc=" . $user_serial_mbc;
		if ($user_serial_dea)
			$sql.=" AND dea.serial_dea=" . $user_serial_dea;

		$sql.=" ORDER BY man.serial_man, dea.serial_dea, br.serial_dea, cit.serial_cit";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arr = array();
			$cont = 0;

			do {
				$arr[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arr;
	}

	/**
	  @Name: getMasiveSalesReport
	  @Description: Get the sales that corresponds to masive products
	  @Params:
	  @Returns: array with masive sales
	 * */
	function getMasiveSalesReport($code_lang, $serial_cus, $sal_serial_sal = NULL) {
		$sql = "SELECT pbl.name_pbl, sal.serial_sal, sal.card_number_sal, DATE_FORMAT(sal.emission_date_sal,'%d/%m/%Y') as emission_date_sal,
					   DATE_FORMAT(sal.begin_date_sal,'%d/%m/%Y') as begin_date_sal,
					   DATE_FORMAT(sal.end_date_sal,'%d/%m/%Y') as end_date_sal, sal.status_sal
				FROM sales sal
				JOIN product_by_dealer pbd ON sal.serial_pbd=pbd.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product pro ON pxc.serial_pro=pro.serial_pro
				JOIN product_by_language pbl ON pbl.serial_pro=pro.serial_pro
				JOIN language lang ON pbl.serial_lang = lang.serial_lang AND lang.code_lang = '$code_lang'
				WHERE (pro.third_party_register_pro = 'YES' OR pro.masive_pro = 'YES')
				AND (sal.status_sal IN ('ACTIVE','REGISTERED'))
				AND (sal.end_date_sal > CURDATE())
				AND sal.serial_cus = '$serial_cus'";
		if ($sal_serial_sal != NULL) {
			$sql .= " AND sal.sal_serial_sal = '$sal_serial_sal'";
		} else {
			$sql .= " AND sal.sal_serial_sal IS NULL";
		}
		//die($sql);

		$result = $this->db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
	  @Name: getMasiveSalesCustomerReport
	  @Description: Get the customers that corresponds to a masive sale
	  @Params:
	  @Returns: array with masive sales
	 * */
	public static function getMasiveSalesCustomerReport($db, $serial_sal) {
		$sql = "SELECT CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus, trl.card_number_trl, trl.serial_trl, trl.serial_sal, cus.document_cus, IFNULL(cus.email_cus,'-') as email_cus,
				IFNULL(cus.address_cus,'-') as address_cus, IFNULL(DATE_FORMAT(cus.birthdate_cus,'%d/%m/%Y'),'-') as birthdate_cus,
				IFNULL(cus.phone1_cus,'-') as phone1_cus, IFNULL(cus.cellphone_cus,'-') as cellphone_cus,
				IFNULL(cou.name_cou,'-') as name_cou, IFNULL(cit.name_cit,'-') as name_cit
				FROM customer cus
				JOIN traveler_log trl ON trl.serial_cus = cus.serial_cus
				LEFT JOIN city cit ON cit.serial_cit = cus.serial_cit
				LEFT JOIN country cou ON cou.serial_cou = cit.serial_cou
				WHERE trl.serial_sal = $serial_sal";
		//die($sql);

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
	  @Name: getPhoneSalesReport
	  @Description: Get the sales that corresponds to a phone counter
	  @Params:
	  @Returns: array with masive sales
	 * */
	public static function getPhoneSalesReport($db, $serial_usr, $beginDate, $endDate) {
		$sql = "SELECT pbl.name_pbl, IFNULL(sal.card_number_sal,trl.card_number_trl) as card_number, IFNULL(inv.number_inv,'-') as number_inv, DATE_FORMAT(sal.emission_date_sal,'%d/%m/%Y') as emission_date_sal,
                               sal.total_sal, IFNULL(inv.total_inv,'-') as total_inv
                    FROM sales sal
                    JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
                    JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.phone_sales_dea = 'YES'
                    JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
                    JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
                    JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = " . $_SESSION['serial_lang'] . "
                    LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal
                    LEFT JOIN invoice inv ON inv.serial_inv = sal.serial_inv
                    WHERE cnt.serial_usr = $serial_usr
                    AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'), '%d/%m/%Y') BETWEEN STR_TO_DATE('$beginDate', '%d/%m/%Y') AND STR_TO_DATE('$endDate', '%d/%m/%Y')";
		//die(Debug::print_r($sql));

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
	  @Name: getDealerSalesReport
	  @Description: Get the sales that corresponds to a phone counter
	  @Params:
	  @Returns: array with masive sales
	 * */
	public static function getDealerSalesReport($db, $params, $serial_dea, $serial_cus=NULL, $beginDate=NULL, 
												$endDate=NULL, $status_sal=NULL, $serial_pbd=NULL, 
												$date_type=NULL, $serial_cnt = NULL) {
		$sql = "SELECT IFNULL(sal.card_number_sal,trl.card_number_trl) as card_number, DATE_FORMAT(sal.emission_date_sal,'%d/%m/%Y') as emission_date_sal,
					   CONCAT(cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'')) as name_cus, pbl.name_pbl, DATE_FORMAT(sal.begin_date_sal,'%d/%m/%Y') as begin_date_sal,
					   DATE_FORMAT(sal.end_date_sal,'%d/%m/%Y') as end_date_sal, sal.days_sal, sal.status_sal, CONCAT(usr.first_name_usr, ' ',
					   usr.last_name_usr) as name_usr, sal.total_sal
				FROM sales sal
				LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal
				JOIN customer cus ON cus.serial_cus = sal.serial_cus
				JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN user usr ON usr.serial_usr = cnt.serial_usr
				WHERE cnt.serial_dea = '$serial_dea'";
		if ($params == "CUSTOMER") {
			$sql .= " AND sal.serial_cus = '$serial_cus'";
		} elseif ($params == "PARAMETER") {
			if ($date_type == "emission") {
				$sql .= " AND (STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('" . $beginDate . "','%d/%m/%Y') AND STR_TO_DATE('" . $endDate . "','%d/%m/%Y'))";
			} else if ($date_type == "coverage") {
				$sql .= " AND (STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('" . $beginDate . "','%d/%m/%Y') AND STR_TO_DATE('" . $endDate . "','%d/%m/%Y'))";
			}

			if ($status_sal != NULL) {
				$sql .= " AND sal.status_sal = '$status_sal'";
			}
			if ($serial_pbd != NULL) {
				$sql .= " AND sal.serial_pbd = '$serial_pbd'";
			}
		}
		
		if($serial_cnt){
			$sql .= " AND sal.serial_cnt = '$serial_cnt'";
		}
		
		$sql.=" ORDER BY sal.emission_date_sal";
		//die(Debug::print_r($sql));
		$result = $db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arr = array();
			$cont = 0;

			do {
				$arr[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arr;
	}

	/**
	  @Name: getSalesWithStandByChanges
	  @Description: Get the sales that have registers in sales-log table with STAND-BY type
	  @Params: $db
	  @Returns: array with sales
	 * */
	public static function getSalesWithStandByChanges($db) {
		$parameter = new Parameter($db, 9);
		$parameter->getData();
		$limit = $parameter->getValue_par();

		$sql = "SELECT sal.serial_sal, IF(DATEDIFF(CURDATE(),MIN(slg.date_slg)) < ($limit*30),'NO','YES') as expire
                FROM sales sal
                JOIN sales_log slg ON slg.serial_sal = sal.serial_sal
                WHERE slg.type_slg = 'STAND_BY'
                GROUP BY sal.serial_sal";
		//die($sql);
		$result = $db->getAll($sql);

		if ($result)
			return $result;
		else
			return NULL;
	}

	/**
	  @Name: hasBeenReactivated
	  @Description: Verifies if a card has been reactivated at some point.
	  @Params: $db
	  @Returns: TRUE if has been reactivated
	  FALSE otherwise
	 * */
	public static function hasBeenReactivated($db, $serial_sal) {
		$sql = "SELECT sal.serial_sal
                FROM sales sal
                JOIN sales_log slg ON slg.serial_sal = sal.serial_sal
                WHERE slg.type_slg = 'REACTIVATED'
				AND slg.status_slg IN ('PENDING', 'AUTHORIZED')
				AND sal.serial_sal = '$serial_sal'";
		//die($sql);
		$result = $db->getOne($sql);

		if ($result)
			return TRUE;
		else
			return FALSE;
	}

	/**
	  @Name: hasBeenReactivated
	  @Description: Verifies if a card has been reactivated at some point.
	  @Params: $db
	  @Returns: TRUE if has been reactivated
	  FALSE otherwise
	 * */
	public static function getPendingReactivatedCards($db) {
		$sql = "SELECT slg.serial_slg, sal.card_number_sal, CONCAT(cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'')) as name_cus
                FROM sales sal
                JOIN sales_log slg ON slg.serial_sal = sal.serial_sal
				JOIN customer cus ON cus.serial_cus = sal.serial_cus
                WHERE slg.type_slg = 'REACTIVATED'
				AND slg.status_slg IN ('PENDING')";
		//die($sql);
		$result = $db->getAll($sql);

		if ($result)
			return $result;
		else
			return FALSE;
	}

	/*
	 * @name: isPlanetAssistSale
	 * @description: Verifies if a sale is from any country other than EC or PE
	 * @param: serial_sal - Sale's ID
	 * @returns: TRUE if not EC/PU
	 *           FALSE otherwise.
	 */

	public static function isPlanetAssistSale($db, $serial_sal) {
		global $globalBlueCardCountryCodes;
		$sql = "SELECT s.serial_sal
			  FROM sales s
			  JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
			  JOIN dealer d ON d.serial_dea=cnt.serial_dea
			  JOIN sector sec ON sec.serial_sec=d.serial_sec
			  JOIN city c ON c.serial_cit=sec.serial_cit
			  JOIN country cou ON cou.serial_cou = c.serial_cou AND cou.serial_cou NOT IN($globalBlueCardCountryCodes)
			  WHERE s.serial_sal=" . $serial_sal;
		$result = $db->getOne($sql);

		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: getCountryofSale
	 * @param: $db - DB connection
	 * @param: serial_sal - Sale's ID
	 * @return: $serial_cou - The Country's ID where the sale took place.
	 */

	public static function getCountryofSale($db, $serial_sal) {
		$sql = "SELECT c.serial_cou
			  FROM sales s
			  JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
			  JOIN dealer d ON d.serial_dea=cnt.serial_dea
			  JOIN sector sec ON sec.serial_sec=d.serial_sec
			  JOIN city c ON c.serial_cit=sec.serial_cit			  
			  WHERE s.serial_sal=" . $serial_sal;

		$result = $db->getOne($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
	 * @Name: getInternationalFeeStatus
	 * @Description: Returns an array of all the sale international fee satatus in DB.
	 * @Params: $db: DB connection
	 * @Returns: An array of free sales.
	 */
	public static function getInternationalFeeStatus($db) {
		$sql = "SHOW COLUMNS FROM sales LIKE 'international_fee_status'";
		$result = $db->Execute($sql);

		$type = $result->fields[1];
		$type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
		return $type;
	}

	/**
	 * @Name: getInternationalFeeUsed
	 * @Description: Returns an array of all the sale international fee used in DB.
	 * @Params: $db: DB connection
	 * @Returns: An array of free sales.
	 */
	public static function getInternationalFeeUsed($db) {
		$sql = "SHOW COLUMNS FROM sales LIKE 'international_fee_used'";
		$result = $db->Execute($sql);

		$type = $result->fields[1];
		$type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
		return $type;
	}

	/**
	 * @Name: getDaylyOnlySaleManagerSales
	 * @Description: Returns an array of all the sales from a only sales managers
	 * @Params: $db: DB connection
	 * @Returns: An array of sales.
	 */
	public static function getDaylyOnlySaleManagerSales($db) {
		$sql = "SELECT s.serial_sal, dea.serial_dea, man.serial_man, s.serial_pbd, dea.serial_cdd
				FROM sales s
				JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
				JOIN dealer dea ON dea.serial_dea=cnt.serial_dea
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
				JOIN manager man ON man.serial_man = mbc.serial_man AND man.sales_only_man = 'YES'
				WHERE s.serial_inv IS NULL
				AND s.free_sal = 'NO'
				AND s.status_sal = 'REGISTERED'";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
	  @Name onCoverageTime
	  @Description Checks if a card available for travels register
	  @Params
	 *   $this->card_number_sal: object atribute for card_number_sal
	  @Returns true: when is executive
	 *    false: when is not executive
	 * */
	function onCoverageTime() {
		$sql = "SELECT 1
			FROM sales s
			WHERE s.serial_sal={$this->serial_sal}
			AND begin_date_sal <= NOW()";
		$result = $this->db->getOne($sql);
		if ($result)
			return true;
		else
			return false;
	}

	/**
	 * @name hasApplicationsPending
	 * @param $db Database Connection
	 * @param $serial_sal Sales ID
	 * @return TRUE If there's any applicantios to modify the sales in any way.
	 * @return FALSE If no application was found
	 */
	public static function hasApplicationsPending($db, $serial_sal) {
		$sql = "SELECT s.serial_sal
			FROM sales s
			JOIN sales_log slg ON slg.serial_sal=s.serial_sal
			WHERE slg.serial_sal=" . $serial_sal . "
			AND slg.status_slg='PENDING'";

		$result = $db->getOne($sql);

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	function payInternationaFee($serial_man, $serial_btm) {
		/*//Gets date
		$billingToManager = new BillingToManager($this->db, $serial_btm);
		$date = $billingToManager->getLastBillToManager($serial_man, $serial_btm);

		if ($billingToManager->getData()) {
			$date_btm = $billingToManager->getDate_btm();

			if ($date) {
				$extra_conditions = " AND (sal.emission_date_sal > STR_TO_DATE('$date','%d/%m/%Y %H:%i:%s')
										AND sal.emission_date_sal < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY))";
			} else {
				$extra_conditions = " AND sal.emission_date_sal < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)";
			}
		} else {
			if ($date) {
				$extra_conditions = " AND sal.emission_date_sal > STR_TO_DATE('$date','%d/%m/%Y %H:%i:%s')";
			}
		}*/
		
		//Gets date
		$billingToManager=new BillingToManager($this->db);
		$date = $billingToManager->getLastBillToManager($serial_man, $serial_btm);

		if($serial_btm) {
			$billingToManager = new BillingToManager($this->db, $serial_btm);
			$billingToManager->getData();

			$date_btm = $billingToManager->getDate_btm();

			if($date) {
				$dates_restriction = " AND	(
												(	
													sal.in_date_sal > STR_TO_DATE('$date','%d/%m/%Y')
													AND sal.in_date_sal < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)
												) OR (
													 sl.date_slg >  STR_TO_DATE('$date','%d/%m/%Y')
													AND sl.date_slg < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)
												)
											)";
			} else {
				$dates_restriction = " AND (
												sal.in_date_sal < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)
											OR 
												sl.date_slg < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)
											)";
			}
		} else {
			if($date) {
				$dates_restriction = " AND (
												sal.in_date_sal > STR_TO_DATE('$date','%d/%m/%Y')
											 OR
												sl.date_slg > STR_TO_DATE('$date','%d/%m/%Y')
											)";
			}
		}

		if($date_to){
			$dates_restriction .= " AND (
											sal.in_date_sal < DATE_ADD(STR_TO_DATE('$date_to','%d/%m/%Y'), INTERVAL 1 DAY)
										 OR
											sl.date_slg < DATE_ADD(STR_TO_DATE('$date_to','%d/%m/%Y'), INTERVAL 1 DAY)
										)";
		}

		$sql = "SELECT DISTINCT sal.serial_sal
					FROM manager man
					JOIN manager_by_country mbc ON mbc.serial_man = man.serial_man
					JOIN dealer dea ON dea.serial_mbc = mbc.serial_mbc
					JOIN counter cnt ON cnt.serial_dea = dea.serial_dea
					JOIN user usr ON usr.serial_usr = cnt.serial_usr
					JOIN sales sal ON sal.serial_cnt = cnt.serial_cnt
					LEFT JOIN sales_log sl ON sl.serial_sal=sal.serial_sal AND type_slg = 'REFUNDED' AND status_slg = 'AUTHORIZED'
					JOIN customer cus ON cus.serial_cus = sal.serial_cus
					JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = " . $_SESSION['serial_lang'] . "
					WHERE man.serial_man = $serial_man
					AND sal.international_fee_used = 'NO'
					AND (sal.international_fee_status = 'TO_COLLECT' OR sal.international_fee_status = 'TO_REFUND')
					$dates_restriction";
		
		$result = $this->db->getAll($sql);
		$connector = '';

		if (count($result) > 0) {
			foreach ($result as $r) {
				$sales_ids.=$connector . $r['0'];
				$connector = ',';
			}
		} else {
			ErrorLog::log($this->db, 'SALES payInternationaFee - getSales ', $sql);
			return FALSE;
		}

		$sql = "UPDATE sales SET
				international_fee_used='YES',
				international_fee_status='PAID'
				WHERE serial_sal IN ($sales_ids)";

		$result = $this->db->Execute($sql);

		if ($result) {
			return TRUE;
		} else {
			ErrorLog::log($this->db, 'SALES payInternationaFee - updateSales', $sql);
			return FALSE;
		}
	}

	public static function isMassiveSale($db, $serial_sal) {
		$sql = " SELECT s.serial_sal
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro = pxc.serial_pro AND p.masive_pro = 'YES'
			  	WHERE s.serial_sal = $serial_sal";

		$result = $db->getOne($sql);

		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public static function flightRegisterGeneratesCardNumber($db, $serial_sal) {
		$sql = "SELECT s.serial_sal
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro = pxc.serial_pro AND p.generate_number_pro = 'NO'
			  	WHERE s.serial_sal = $serial_sal";

		$result = $db->getOne($sql);

		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public static function getAssistanceSaleInfoByCardNumber($db, $card_number, $masive) {

		$sql = "SELECT sal.serial_sal, sal.status_sal, sal.free_sal, sal.serial_inv
				FROM sales sal
				LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal";
		if ($masive == 0) {
			$sql .= " WHERE sal.card_number_sal LIKE '$card_number'
				OR trl.card_number_trl LIKE '$card_number'";
		} else {
			$sql .= " WHERE trl.card_number_trl LIKE '$card_number'";
		}
		//die($sql);
		$result = $db->getAll($sql);

		if ($result[0]) {
			return $result[0];
		} else {
			return FALSE;
		}
	}

	public static function getSalesInfoForCustomerPromo($db, $serial_sal) {
		$sql = "SELECT s.total_sal, pbd.serial_pxc, s.free_sal, s.type_sal
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				WHERE s.serial_sal = $serial_sal";

		return $db->getRow($sql);
	}

	public static function updateSaleTotalForCupon($db, $total_sal, $serial_sal) {
		$sql = "UPDATE sales SET
				total_sal = $total_sal
			  WHERE serial_sal = $serial_sal";

		$result = $db->Execute($sql);

		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public static function getSoldProductInformation($db, $serial_sal) {
		$sql = "SELECT s.serial_pbd, pxc.serial_pxc, pxc.serial_pro
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				WHERE serial_sal = $serial_sal";

		$result = $db->getRow($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public static function getGrossSalesForReport($db, $serial_cou, $date_field, $date_of_analisys, $order_field, $serial_cit = NULL, $serial_mbc = NULL, $serial_ubd = NULL, $serial_dea = NULL, $serial_bra = NULL, $stock_type = NULL, $sale_type = NULL, $product_list = NULL, $status_sal = NULL, $amount_clause = NULL, $effective_sales = false) {

		$date_of_analisys_arr = explode("/", $date_of_analisys);
		$lastDay = strftime("%d", mktime(0, 0, 0, $date_of_analisys_arr[0] + 1, 0, $date_of_analisys_arr[1]));

		($date_field == 'insystem') ? $date_field = 's.in_date_sal' : $date_field = 's.begin_date_sal';
		($order_field == 'card_number') ? $order_field = '2' : $order_field = '3';
		($serial_cit) ? $city_clause = " AND cit.serial_cit = $serial_cit" : NULL;
		($serial_mbc) ? $mbc_clause = " AND br.serial_mbc = $serial_mbc" : NULL;
		($serial_ubd) ? $ubd_clause = " AND ubd.serial_usr = $serial_ubd" : NULL;
		($serial_dea) ? $dea_clause = " AND br.dea_serial_dea = $serial_dea" : NULL;
		($serial_bra) ? $bra_clause = " AND br.serial_dea = $serial_bra" : NULL;
		($stock_type) ? $stock_clause = " AND s.stock_type_sal = '$stock_type'" : NULL;
		($sale_type) ? $sale_type_clause = " AND s.type_sal  IN ($sale_type)" : NULL;
		($product_list) ? $product_clause = " AND pxc.serial_pro IN ($product_list)" : NULL;
		($amount_clause) ? $amount_clause = " AND s.total_sal $amount_clause" : NULL;
		$effective_clause = "WHERE $date_field BETWEEN STR_TO_DATE( '01/" . $date_of_analisys . "' ,'%d/%m/%Y') AND STR_TO_DATE('" . $lastDay . "/" . $date_of_analisys . "','%d/%m/%Y')"; //" WHERE STR_TO_DATE(DATE_FORMAT($date_field, '%m/%Y'), '%m/%Y') = STR_TO_DATE('$date_of_analisys', '%m/%Y')";

		$sql = "(
					SELECT  'PROCESSED' AS 'register_type',
							IF(s.card_number_sal IS NULL OR s.card_number_sal = 0, 'N/A', s.card_number_sal) AS 'card_number_sal',
							DATE_FORMAT(s.in_date_sal, '%d/%m/%Y') AS 'in_date_sal',
							DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y') AS 'emission_date_sal',
							CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus',
							DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal',
							DATE_FORMAT(s.end_date_sal, '%d/%m/%Y') AS 'end_date_sal',
							s.days_sal, s.total_sal, br.name_dea,
							CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'name_cnt', s.stock_type_sal,
							s.status_sal, 'YES' AS 'on_date', pbl.name_pbl, s.serial_sal
					FROM sales s
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd AND s.status_sal <> 'DENIED'
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc $product_clause
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
					JOIN customer cus ON cus.serial_cus = s.serial_cus
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN user u ON u.serial_usr = cnt.serial_usr
					JOIN dealer br ON br.serial_dea = cnt.serial_dea $mbc_clause $dea_clause $bra_clause  AND br.status_dea= 'ACTIVE'
					JOIN user_by_dealer ubd ON ubd.serial_dea = br.serial_dea $ubd_clause 
						AND s.in_date_sal 
							BETWEEN ubd.begin_date_ubd AND 
							DATE_SUB(IFNULL(ubd.end_date_ubd, DATE_ADD(CURRENT_DATE(), INTERVAL 2 DAY)), INTERVAL 1 DAY)
					JOIN sector sec ON sec.serial_sec = br.serial_sec
					JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou $city_clause
					$effective_clause
					$stock_clause
					$sale_type_clause				
					$amount_clause
				) UNION (
					SELECT  IF(slg.type_slg = 'REFUNDED', 'REFUND', IF(s.status_sal = 'REFUNDED', 'REFUND', 'VOID')) AS 'register_type',
							IF(s.card_number_sal IS NULL OR s.card_number_sal = 0, 'N/A', s.card_number_sal) AS 'card_number_sal',
							DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y') AS 'emission_date_sal',
							DATE_FORMAT(s.in_date_sal, '%d/%m/%Y') AS 'in_date_sal',
							CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus',
							DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal',
							DATE_FORMAT(s.end_date_sal, '%d/%m/%Y') AS 'end_date_sal',
							s.days_sal, s.total_sal, br.name_dea,
							CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'name_cnt', s.stock_type_sal,
							s.status_sal,
							IF( STR_TO_DATE(DATE_FORMAT($date_field, '%m/%Y'), '%m/%Y') = STR_TO_DATE('$date_of_analisys', '%m/%Y'),
								'YES', 'NO') AS 'on_date', pbl.name_pbl, s.serial_sal
					FROM sales s
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc $product_clause
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
					JOIN customer cus ON cus.serial_cus = s.serial_cus
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN user u ON u.serial_usr = cnt.serial_usr
					JOIN dealer br ON br.serial_dea = cnt.serial_dea $mbc_clause $dea_clause $bra_clause AND br.status_dea= 'ACTIVE'
					JOIN user_by_dealer ubd ON ubd.serial_dea = br.serial_dea $ubd_clause
						AND s.in_date_sal 
							BETWEEN ubd.begin_date_ubd AND 
							DATE_SUB(IFNULL(ubd.end_date_ubd, DATE_ADD(CURRENT_DATE(), INTERVAL 2 DAY)), INTERVAL 1 DAY)
					JOIN sector sec ON sec.serial_sec = br.serial_sec
					JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou $city_clause
					JOIN sales_log slg ON slg.serial_sal = s.serial_sal
						AND slg.type_slg IN ('VOID_INVOICE','REFUNDED', 'VOID_SALE')
						AND slg.status_slg = 'AUTHORIZED'
						AND s.status_sal IN ('VOID','REFUNDED')
						AND s.status_sal <> 'DENIED'";
		if ($effective_sales) {
			$sql.=$effective_clause;
		} else {
			$sql.="WHERE slg.date_slg BETWEEN STR_TO_DATE( '01/" . $date_of_analisys . "' ,'%d/%m/%Y') AND STR_TO_DATE('" . $lastDay . "/" . $date_of_analisys . "','%d/%m/%Y')";
		}

		$sql.="	$stock_clause
					$sale_type_clause
					$amount_clause
			GROUP BY s.serial_sal) ORDER BY 1, $order_field";

		//die(Debug::print_r($sql));
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public static function getInfoForManagerSalesAnalysisReport($db, $serial_cou, $serial_mbc, $begin_date, $end_date, $active_only = TRUE) {
		if($active_only):
			$active_only_sql = " AND d.status_dea = 'ACTIVE'";
		endif;
			
		$sql = "SELECT	DISTINCT u.serial_usr, 
						CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'name_usr',
						SUM(sal.total_sal) as sales, 
						void_table.void, free_table.free, refunded_table.refunded
				FROM user u " .
				self::getInnerJoinSQL($serial_mbc, $serial_cou, $begin_date, $end_date, 'VOID', $active_only) .
				self::getInnerJoinSQL($serial_mbc, $serial_cou, $begin_date, $end_date, 'FREE', $active_only) .
				self::getInnerJoinSQL($serial_mbc, $serial_cou, $begin_date, $end_date, 'REFUNDED', $active_only) . "
				JOIN user_by_dealer ubd ON ubd.serial_usr = u.serial_usr
				JOIN dealer d ON d.serial_dea=ubd.serial_dea $active_only_sql
				JOIN dealer parentd ON parentd.serial_dea = d.dea_serial_dea
				JOIN counter cnt ON cnt.serial_dea = d.serial_dea
				JOIN sales sal ON sal.serial_cnt = cnt.serial_cnt 
					AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') 
						BETWEEN ubd.begin_date_ubd AND 
						IFNULL(ubd.end_date_ubd, CURRENT_DATE())
				JOIN manager_by_country mbc ON mbc.serial_mbc = d.serial_mbc
				WHERE d.serial_mbc = $serial_mbc
				AND mbc.serial_cou = $serial_cou
				AND sal.status_sal <> 'DENIED'
				AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('" . $begin_date . "','%d/%m/%Y') AND STR_TO_DATE('" . $end_date . "','%d/%m/%Y')
				GROUP BY u.serial_usr";
	//	die('<pre>'.$sql.'</pre>');
		$result = $db->getAll($sql);
		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public static function getInnerJoinSQL($serial_mbc, $serial_cou, $begin_date, $end_date, $card_status = 'ALL', $active_only = TRUE) {
		if($active_only):
			$active_only_sql = " AND d2.status_dea= 'ACTIVE'";
		endif;
		
		switch ($card_status) {
			case 'VOID':
				$sql_header = "LEFT JOIN ( SELECT serial_usr, SUM(void) as void FROM
											(SELECT DISTINCT sal2.card_number_sal,u2.serial_usr,sal2.total_sal as void";
				$and_statement = "sal2.status_sal = 'VOID'";
				$slg_type_statement = "AND slg.type_slg IN ('VOID_INVOICE', 'VOID_SALE')";
				$sql_end = " as void_table ON u.serial_usr = void_table.serial_usr";
				break;
			case 'FREE':
				$sql_header = "LEFT JOIN ( SELECT serial_usr, SUM(free) as free FROM
											(SELECT DISTINCT sal2.card_number_sal,u2.serial_usr,sal2.total_sal as free";
				$and_statement = "(sal2.free_sal = 'YES' AND sal2.status_sal = 'ACTIVE')";
				$slg_type_statement = "AND slg.type_slg = 'FREE'";
				$sql_end = " as free_table ON u.serial_usr = free_table.serial_usr";
				break;
			case 'REFUNDED':
				$sql_header = "LEFT JOIN ( SELECT serial_usr, SUM(refunded) as refunded FROM
											(SELECT DISTINCT sal2.card_number_sal,u2.serial_usr,sal2.total_sal as refunded";
				$and_statement = "sal2.status_sal = 'REFUNDED'";
				$slg_type_statement = "AND slg.type_slg = 'REFUNDED'";
				$sql_end = " as refunded_table ON u.serial_usr = refunded_table.serial_usr";
				break;
		}

		$sql = " $sql_header
						FROM user u2
						JOIN user_by_dealer ubd2 ON ubd2.serial_usr = u2.serial_usr
						JOIN dealer d2 ON d2.serial_dea=ubd2.serial_dea
						JOIN counter cnt2 ON cnt2.serial_dea = d2.serial_dea $active_only_sql
						JOIN sales sal2 ON sal2.serial_cnt = cnt2.serial_cnt 
							AND STR_TO_DATE(DATE_FORMAT(sal2.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') 
								BETWEEN ubd2.begin_date_ubd AND 
								IFNULL(ubd2.end_date_ubd, CURRENT_DATE())
						JOIN manager_by_country mbc2 ON mbc2.serial_mbc = d2.serial_mbc
						JOIN sales_log slg ON slg.serial_sal = sal2.serial_sal
						$slg_type_statement
						AND slg.status_slg = 'AUTHORIZED'
						WHERE d2.serial_mbc = $serial_mbc
						AND mbc2.serial_cou = $serial_cou
						AND $and_statement
						AND (STR_TO_DATE(DATE_FORMAT(slg.date_slg,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('" . $begin_date . "','%d/%m/%Y') AND STR_TO_DATE('" . $end_date . "','%d/%m/%Y'))
						)as t1
						GROUP BY serial_usr
				) $sql_end ";

		return $sql;
	}

	/*
	 * @name: getTotalCardsSoldResume
	 * @param: $db - DB connection
	 * @return: Resume of cards sold by stock type
	 */

	public static function getTotalCardsSoldResume($db, $serial_cou, $serial_cit, $serial_mbc, $serial_usr, $begin_date, $end_date) {
		$sql = "SELECT stock_type_sal, COUNT(sal.card_number_sal) AS total_cards
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.status_dea = 'ACTIVE'
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				WHERE STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
		";
		$sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";
		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";
		$sql .= " GROUP BY stock_type_sal";

		$result = $db->getAssoc($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: getCardsSoldByCountry
	 * @param: $db - DB connection
	 * @return: Resume of cards sold by stock type, filtered mandatory by country and beginning and ending date
	 */

	public static function getCardsSoldByCountry($db, $serial_cou, $begin_date, $end_date, $serial_cit=NULL, $serial_mbc=NULL, $serial_usr=NULL) {

		$sql = "SELECT DISTINCT dea.serial_dea, stock_type_sal, COUNT(sal.card_number_sal) AS total_cards,
						CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as name_usr, cit.name_cit, man.name_man,
						CONCAT(dea.name_dea, ' - ', dea.code_dea) as name_dea, usr.serial_usr, man.serial_man, cit.serial_cit
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.status_dea = 'ACTIVE'
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc 
				JOIN manager man ON man.serial_man = mbc.serial_man
				WHERE STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
				";
		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";
		$sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";

		$sql .= " GROUP BY dea.serial_dea, stock_type_sal
				  ";
		if ($serial_usr) {
			$sql .= " ORDER BY name_dea, serial_dea";
		} else {
			if ($serial_mbc && $serial_cit) {
				$sql .= " ORDER BY  name_usr, serial_usr, name_dea, serial_dea";
			} else {
				$sql .= " ORDER BY name_cit, serial_cit, name_man, serial_man, name_usr, serial_usr, name_dea, serial_dea";
			}
		}

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: getDealersSellingVirtualCards
	 * @param: $db, $serial_cou, $begin_date, $end_date, $serial_cit, $serial_mbc, $serial_usr
	 * @return: Returns the number of dealers selling cards in a period of time
	 */

	public static function getDealersSellingCards($db, $serial_cou, $begin_date, $end_date, $serial_cit=NULL, $serial_mbc=NULL, $serial_usr=NULL) {
		$sql = "SELECT COUNT(DISTINCT dea.serial_dea) as sold
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.status_dea = 'ACTIVE'
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				WHERE dea.status_dea = 'ACTIVE'
					AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
		";

		$sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";
		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";

		$result = $db->getOne($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: getCardsSoldByDealer (VIRTUAL CARDS)
	 * @param: $db, $serial_cou, $begin_date, $end_date, $serial_cit, $serial_mbc, $serial_usr
	 * @return: Returns a list of dealers who sold in a period of time by country
	 */

	public static function getCardsSoldByDealer($db, $serial_cou, $begin_date, $end_date, $serial_cit=NULL, $serial_mbc=NULL, $serial_usr=NULL) {
		$sql = "SELECT COUNT(sal.card_number_sal) AS total_cards, 
						CONCAT(dea.code_dea, ' - ', dea.name_dea) AS 'name_dea'
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.status_dea = 'ACTIVE'
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				WHERE dea.status_dea = 'ACTIVE' AND sal.stock_type_sal = 'VIRTUAL'
					AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
		";

		$sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";
		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";

		$sql .= " GROUP BY dea.serial_dea
					ORDER BY total_cards DESC
				";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: getVirtualCardsSoldInfoByDealer
	 * @param: $db, $serial_cou, $begin_date, $end_date, $serial_cit, $serial_mbc, $serial_usr
	 * @return: Returns the number of virtual cards sold by dealer by responsible
	 */

	public static function getVirtualCardsSoldInfoByDealer($db, $serial_cou, $begin_date, $end_date, $serial_cit=NULL, $serial_mbc=NULL, $serial_usr=NULL) {
		$sql = "SELECT DISTINCT dea.serial_dea, IFNULL(COUNT(sal.card_number_sal) ,0) AS total_cards,
						CONCAT(dea.code_dea, ' - ', dea.name_dea) AS 'name_dea',
						cit.serial_cit, cit.name_cit, mbc.serial_mbc, man.name_man, usr.serial_usr,
						CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as name_usr,
						IFNULL(SUM(sal.total_sal) ,0) AS 'sales_amount'
				FROM dealer dea
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				JOIN counter cnt ON cnt.serial_dea = dea.serial_dea
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
				JOIN manager man ON man.serial_man = mbc.serial_man
				LEFT JOIN sales sal ON sal.serial_cnt = cnt.serial_cnt
					AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') and STR_TO_DATE('$end_date', '%d/%m/%Y')
				WHERE dea.status_dea = 'ACTIVE'";

		$sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";
		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";

		$sql .= " GROUP BY dea.serial_dea
				 ORDER BY dea.serial_mbc, ubd.serial_usr, sales_amount DESC";
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: getCardsSoldByDealer (VIRTUAL CARDS)
	 * @param: $db, $serial_cou, $begin_date, $end_date, $serial_cit, $serial_mbc, $serial_usr
	 * @return: Returns a list of dealers who sold in a period of time by country
	 */

	public static function getTotalAmountsSoldByDealer($db, $serial_cou, $begin_date, $end_date, $serial_cit=NULL, $serial_mbc=NULL, $serial_usr=NULL) {
		$sql = "SELECT SUM(sal.total_sal) AS total_sales, CONCAT(dea.code_dea, ' - ', dea.name_dea) AS 'name_dea'
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.status_dea = 'ACTIVE'
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				WHERE dea.status_dea = 'ACTIVE'
				AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')";

		$sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";
		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";

		$sql .= " GROUP BY dea.serial_dea
				ORDER BY total_sales DESC";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: getRefundVoidSalesByManager
	 * @param: $db, $serial_mbc,$dateFrom , $dateTo
	 * @return: Returns a list of sales by manager available for international cost refund. 
	 */

	public static function getVoidAndRefundedSalesByManager($db, $serial_mbc, $dateFrom, $dateTo) {
		$sql = "	SELECT	s.serial_sal, s.card_number_sal,dea.name_dea,s.status_sal, slg.description_slg,
							DATE_FORMAT(MAX(slg.date_slg),'%d/%m/%Y') as log_date, usr.first_name_usr,
							usr.last_name_usr,s.begin_date_sal, s.international_fee_amount,
							s.total_sal
					FROM sales s 
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
					JOIN sales_log slg ON slg.serial_sal = s.serial_sal
					JOIN user usr ON usr.serial_usr = slg.usr_serial_usr
					WHERE dea.serial_mbc = {$serial_mbc}
					AND s.status_sal = 'VOID'
					AND s.international_fee_status IN ('TO_REFUND', 'TO_COLLECT')
					AND s.international_fee_amount > 0
					AND (slg.type_slg = 'VOID_INVOICE' OR slg.type_slg = 'VOID_SALE')
					AND slg.status_slg = 'AUTHORIZED'
					AND STR_TO_DATE(DATE_FORMAT(slg.date_slg, '%d/%m/%Y'), '%d/%m/%Y') 
						BETWEEN STR_TO_DATE('{$dateFrom}','%d/%m/%Y') 
						AND STR_TO_DATE('{$dateTo}','%d/%m/%Y')
					GROUP BY slg.serial_sal
					
				UNION

					SELECT s.serial_sal, s.card_number_sal, dea.name_dea, s.status_sal, slg.description_slg,
							DATE_FORMAT(r.auth_date_ref,'%Y-%m-%d') AS 'log_date', usr.first_name_usr,
							usr.last_name_usr,s.begin_date_sal, s.international_fee_amount,
							s.total_sal
					FROM sales s 
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
					JOIN refund r ON r.serial_sal = s.serial_sal
					JOIN sales_log slg ON slg.serial_sal = s.serial_sal AND slg.type_slg = 'REFUNDED' AND slg.status_slg = 'AUTHORIZED'
					JOIN user usr ON usr.serial_usr = r.serial_usr
					WHERE dea.serial_mbc = {$serial_mbc}
					AND s.status_sal = 'REFUNDED'
					AND s.international_fee_amount > 0
					AND s.international_fee_status IN ('TO_REFUND', 'TO_COLLECT')
					AND STR_TO_DATE(DATE_FORMAT(r.auth_date_ref, '%d/%m/%Y'), '%d/%m/%Y') 
						BETWEEN STR_TO_DATE('{$dateFrom}','%d/%m/%Y') 
						AND STR_TO_DATE('{$dateTo}','%d/%m/%Y')";
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: getAuthorizedFreeSalesByManager
	 * @param: $db, $serial_mbc,$dateFrom , $dateTo
	 * @return: Returns a list of free authorized sales by manager available for international cost refund. 
	 */

	public static function getAuthorizedFreeSalesByManager($db, $serial_mbc, $dateFrom, $dateTo) {
		$sql = "SELECT s.serial_sal, s.card_number_sal,dea.name_dea,s.status_sal, 
						slg.description_slg,usr.first_name_usr,usr.last_name_usr, 
						DATE_FORMAT(MAX(slg.date_slg),'%d/%m/%Y') as log_date,
						observations_sal, s.international_fee_amount, s.total_sal
				FROM sales s 
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
				JOIN sales_log slg ON slg.serial_sal = s.serial_sal
				JOIN user usr ON usr.serial_usr = slg.usr_serial_usr
				WHERE dea.serial_mbc = {$serial_mbc}
				AND (s.status_sal = 'STANDBY' OR s.status_sal = 'ACTIVE' OR s.status_sal = 'EXPIRED')
				AND s.international_fee_status = 'TO_COLLECT'
				AND s.free_sal = 'YES'
				AND s.international_fee_used = 'NO'
				AND slg.type_slg = 'FREE'
				AND slg.status_slg = 'AUTHORIZED'
				AND s.international_fee_amount > 0
				AND STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y')  BETWEEN STR_TO_DATE('{$dateFrom}','%d/%m/%Y') AND STR_TO_DATE('{$dateTo}','%d/%m/%Y')
				GROUP BY slg.serial_sal ";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: updateInternationalFeeToRefund
	 * @param: $db, $salesToUpdate
	 * @return: Update sales changing international_fee_status to 
	 */

	public static function updateInternationalFeeToRefund($db, $salesToUpdate, $resetValue = FALSE) {
		$sql = "UPDATE sales 
				SET international_fee_status = 'TO_REFUND' ";
		$sql.= ( $resetValue) ? " , international_fee_amount = 0 " : "";
		$sql.="	WHERE serial_sal IN({$salesToUpdate})";
		$result = $db->Execute($sql);
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: checkIfSaleIsMasiveByCardNumber
	 * @param: $db, $cardNumber
	 * @return: true or false
	 */

	public static function checkIfSaleIsMasiveByCardNumber($db, $card_number) {
		$sql = "SELECT s.serial_sal
			FROM sales s
			JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
			JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
			JOIN product p ON pxc.serial_pro=p.serial_pro AND p.masive_pro='YES'
			WHERE s.card_number_sal LIKE '$card_number'";

		$result = $db->getOne($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}
	
	/**
	 * @name totalSoldByProductByDates
	 * @param type $db
	 * @param type $begin_date
	 * @param type $end_date
	 * @param type $date_field
	 * @param type $serial_cou
	 * @param type $serial_mbc
	 * @param type $serial_dea
	 * @param type $serial_bra
	 * @param type $serial_cit
	 * @return array Data 
	 * @
	 */
	public static function totalSoldByProductByDates($db, $begin_date, $end_date, $date_field, $serial_cou = NULL, $serial_mbc = NULL,
														$serial_dea = NULL,	$serial_bra = NULL, $serial_cit = NULL){
		$extra_params = "";
		if($serial_cou) $extra_params .= " AND pxc.serial_cou = $serial_cou";
		if($serial_mbc) $extra_params .= " AND bra.serial_mbc = $serial_mbc";
		if($serial_dea) $extra_params .= " AND pbd.serial_dea = $serial_dea";
		if($serial_bra) $extra_params .= " AND bra.serial_dea = $serial_bra";
		if($serial_cit) $extra_params .= " AND sec.serial_cit = $serial_cit";
		
		if ($date_field == 'in_date_sal'){
			$date_field = 's.in_date_sal';
		}else{
			$date_field = 'f.creation_date_fle';
		}
		
			
		$sql = "SELECT i.serial_pro, i.name_pbl, SUM(i.total_sal) AS 'total_sold',
						SUM(i.estimated_amount) AS 'estimated_amount',
						SUM(i.cost_sold) AS 'cost_sold',
						SUM(IFNULL(p_requests.unpaid_fees,0)) AS 'unpaid_fees',
						SUM(IFNULL(p_requests.paid_fees,0)) AS 'paid_fees',
						SUM(IFNULL(p_requests.unpaid_management,0)) AS 'unpaid_management',
						SUM(IFNULL(p_requests.paid_management,0)) AS 'paid_management',
						SUM(IFNULL(p_requests.unpaid_auditory,0)) AS 'unpaid_auditory',
						SUM(IFNULL(p_requests.paid_auditory,0)) AS 'paid_auditory',
						SUM(IFNULL(p_requests.unpaid_repricing,0)) AS 'unpaid_repricing',
						SUM(IFNULL(p_requests.paid_repricing,0)) AS 'paid_repricing',
						SUM(IFNULL(p_requests.unpaid_credit_note,0)) AS 'unpaid_credit_note',
						SUM(IFNULL(p_requests.paid_credit_note,0)) AS 'paid_credit_note'
				FROM (SELECT s.serial_sal,f.serial_fle, pxc.serial_pro, pbl.name_pbl, 
							s.total_sal,
							SUM(IF((f.status_fle = 'closed'),0,IFNULL(spf.estimated_amount_spf, 0))) AS 'estimated_amount',	
							s.total_cost_sal AS 'cost_sold'
					FROM sales s
					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt AND s.status_sal <> 'DENIED'
					JOIN dealer bra ON bra.serial_dea = cnt.serial_dea AND bra.status_dea = 'ACTIVE'
					JOIN sector sec ON sec.serial_sec = bra.serial_sec
					JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
					JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
					LEFT JOIN file f ON f.serial_sal = s.serial_sal
					LEFT JOIN service_provider_by_file spf ON spf.serial_fle = f.serial_fle	
					WHERE STR_TO_DATE(DATE_FORMAT($date_field,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN 
							STR_TO_DATE('$begin_date','%d/%m/%Y') AND 
							STR_TO_DATE('$end_date', '%d/%m/%Y')
					$extra_params
					GROUP BY pxc.serial_pro, s.serial_sal) AS i
					
				LEFT JOIN(SELECT s.serial_sal,f.serial_fle, SUM(IF((prq.type_prq = 'FEE' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_fees,
							SUM(IF((prq.type_prq = 'FEE' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_fees,
							SUM(IF((prq.type_prq = 'MANAGEMENT' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_management,
							SUM(IF((prq.type_prq = 'MANAGEMENT' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_management,
							SUM(IF((prq.type_prq = 'AUDITORY' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_auditory,
							SUM(IF((prq.type_prq = 'AUDITORY' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_auditory,
							SUM(IF((prq.type_prq = 'REPRICING' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_repricing,
							SUM(IF((prq.type_prq = 'REPRICING' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_repricing,
							SUM(IF((prq.type_prq = 'CREDIT_NOTE' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'NO' ),prq.amount_authorized_prq,0)) as unpaid_credit_note,
							SUM(IF((prq.type_prq = 'CREDIT_NOTE' AND prq.status_prq = 'APROVED' AND prq.dispatched_prq = 'YES' ),prq.amount_authorized_prq,0)) as paid_credit_note
                FROM sales s
                JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
                JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
                JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = 2
                LEFT JOIN file f ON f.serial_sal = s.serial_sal
                LEFT JOIN payment_request prq ON prq.serial_fle = f.serial_fle
               WHERE STR_TO_DATE(DATE_FORMAT($date_field,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN 
							STR_TO_DATE('$begin_date','%d/%m/%Y') AND 
							STR_TO_DATE('$end_date', '%d/%m/%Y')
                    $extra_params
                GROUP BY s.serial_sal)  as p_requests  ON i.serial_fle = p_requests.serial_fle AND i.serial_sal = p_requests.serial_sal
				GROUP BY i.serial_pro
				ORDER BY i.name_pbl";
		//die(Debug::print_r($sql));
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
		
	}
	
	public static function updateErpId($db, $serial_sal, $erp_serial){
		$sql= "UPDATE sales
			   SET id_erp_sal = $erp_serial
			   WHERE serial_sal = $serial_sal";
		
		$result = $db -> Execute($sql);
		
		if (!$result)
			ErrorLog::log($db, 'ERP PROCESS FAILED - UPDATE ERPID SALES', $sql);
	}
	
	public static function getSalesBasicInfo($db, $serial_sal){
		$sql = "SELECT	DATE_FORMAT(s.in_date_sal, '%d/%m/%Y') AS 'in_date_sal',
						DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal',
						DATE_FORMAT(s.end_date_sal, '%d/%m/%Y') AS 'end_date_sal',
						CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus',
						s.days_sal, pbl.name_pbl, s.total_sal
				FROM sales s 
				JOIN customer cus ON cus.serial_cus = s.serial_cus
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
				WHERE s.serial_sal = $serial_sal";
				
		$result = $db->getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getDestinationsValues($db, $serial_cou, $begin_date, $end_date, $date_type,
												$serial_cit = NULL, $serial_mbc = NULL, $serial_usr = NULL,
												$serial_dea = NULL, $serial_bra = NULL){
		$city_filter = $serial_cit ? ' AND cit.serial_cit = '.$serial_cit : '';
		$mbc_filter = $serial_mbc ? ' AND dea.serial_mbc = '.$serial_mbc : '';
		$usr_filter = $serial_usr ? ' AND ubd.serial_usr = '.$serial_usr : '';
		$dea_filter = $serial_dea ? ' AND dea.serial_dea = '.$serial_dea : '';
		$bra_filter = $serial_bra ? ' AND br.serial_dea = '.$serial_bra : '';
		
		
		$sql = "SELECT dest_cou.serial_cou, dest_cou.name_cou, dest_cit.name_cit, count(serial_sal) AS 'travels'
				FROm sales s
				JOIN city dest_cit On dest_cit.serial_cit = s.serial_cit
				JOIN country dest_cou ON dest_cou.serial_cou = dest_cit.serial_cou
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer br ON br.serial_dea = cnt.serial_dea $bra_filter
				JOIN dealer dea ON dea.serial_dea = br.dea_serial_dea $mbc_filter $dea_filter
				JOIN sector sec ON br.serial_sec = sec.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit $city_filter
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc AND mbc.serial_cou = $serial_cou
				JOIN user_by_dealer ubd ON ubd.serial_dea = br.serial_dea $usr_filter
					";
		
		if ($date_type == "insystem") {
			$sql .= " WHERE (STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y'))";
		} else if ($date_type == "coverage") {
			$sql .= " WHERE STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')";
		}
		
		$sql .= "	GROUP BY dest_cit.serial_cit
					ORDER BY travels DESC";
		
		//Debug::print_r($sql);
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function internationalFlight($db, $serial_sal, $serial_trl = NULL){
		$sql = "SELECT pxc.serial_cou AS 'origin', c.serial_cou AS 'destination'
				FROM sales s 
				JOIN prodcut_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc";
		
		if($serial_trl){
			$sql .= " JOIN travel_register tr On tr.serial_sal = s.serial_sal AND tr.serial_trl = $serial_trl
					  JOIN city c ON c.serial_cit = tr.serial_cit";
		}else{
			$sql .= " JOIN city c ON c.serial_cit = s.serial_cit";
		}
		
		$result = $db -> getRow($sql);
		
		if($result){
			if($result['origin'] == $result['destination']){
				return FALSE;
			}else{
				return TRUE;
			}
		}else{
			return TRUE;
		}
	}

        public static function getDestinationsByProduct($db, $serial_cou, $begin_date, $end_date, $date_type, $serial_mbc = NULL, 
                $serial_pro = NULL, $serial_usr = NULL ,$serial_dea = NULL, $serial_bra = NULL, $serial_type = NULL){
		
                $product_filter = $serial_pro ? ' AND pbl.serial_pro = '.$serial_pro : '';
		$mbc_filter = $serial_mbc ? ' AND dea.serial_mbc = '.$serial_mbc : '';
		$usr_filter = $serial_usr ? ' AND ubd.serial_usr = '.$serial_usr : '';
		$dea_filter = $serial_dea ? ' AND dea.serial_dea = '.$serial_dea : '';
		$bra_filter = $serial_bra ? ' AND br.serial_dea = '.$serial_bra : '';	
                $type_filter = $serial_type ? ' AND br.serial_dlt = '.$serial_type : '';	
		
		$sql = "SELECT pbl.name_pbl, dest_cou.name_cou, count(s.serial_sal) AS 'travels'
                        FROM sales s 
                        JOIN city dest_cit ON dest_cit.serial_cit = s.serial_cit
                        JOIN country dest_cou ON dest_cou.serial_cou = dest_cit.serial_cou
                        JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
                        JOIN dealer br ON br.serial_dea = cnt.serial_dea $bra_filter
			JOIN dealer dea ON dea.serial_dea = br.dea_serial_dea $mbc_filter $dea_filter $type_filter
                        JOIN user_by_dealer ubd ON ubd.serial_dea = br.serial_dea $usr_filter
					    JOIN sector sec ON br.serial_sec = sec.serial_sec
                        JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
                        JOIN product_by_dealer pbd ON s.serial_pbd = pbd.serial_pbd
                        JOIN product_by_country pxc ON pbd.serial_pxc = pxc.serial_pxc 
                        JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro $product_filter AND pbl.serial_lang = 2";
               
		if ($date_type == "insystem") {
			$sql .= " WHERE (STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y'))";
		} else if ($date_type == "coverage") {
			$sql .= " WHERE STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')";
		}
		
		$sql .= "	GROUP BY pbl.name_pbl, dest_cou.name_cou
				ORDER BY pbl.name_pbl, travels DESC";
		
		//Debug::print_r($sql);
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	/*
	 * getCardManagerReport
	 * Report with cost informations
	 */

	public static function getCardManagerReport($db, $serial_cou, $date_type, $begin_date, $end_date, $serial_cit=NULL, $serial_mbc=NULL, $serial_usr=NULL, $dea_serial_dea=NULL, $serial_dea=NULL, $serial_pro=NULL, $type_sal=NULL, $status_sal=NULL, $operator=NULL, $total_sal=NULL, $stock_type=NULL, $order_by=NULL) {

		if ($order_by == NULL || $order_by == "card_number") {
			$order_by = "col1";
		} else {
			$order_by = "col2";
		}
		if ($serial_usr != NULL) {
			$responsable=" JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.serial_usr = '$serial_usr' AND ubd.status_ubd = 'ACTIVE'";
		}
        $sql = "SELECT  s.serial_sal,
						IFNULL(card_number_sal, 'N/A') as col1,
        				DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y') as col2,
        				CONCAT(first_name_cus,' ',last_name_cus) as col3,
						(IFNULL(COUNT(e.serial_ext),0)+1) as total_pas,
                     	pbl.name_pbl as col4,
                     	DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as col5, 
                     	DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as col6,
                     	DATEDIFF(s.end_date_sal,s.begin_date_sal)+1 as col7,
                     	s.total_sal as col8,
                     	IF(s.free_sal = 'YES', 'FREE', s.status_sal) as col9,
                     	dea.name_dea as col10,
                     	CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as col11,
                     	s.total_cost_sal as total_cost,
						cus.document_cus,
						DATE_FORMAT(cus.birthdate_cus, '%d/%m/%Y') AS 'birthdate_cus',
						cou.name_cou AS 'name_country'
				FROM sales s
				LEFT JOIN city cit_dest ON cit_dest.serial_cit = s.serial_cit
				LEFT JOIN country cou ON cit_dest.serial_cou = cou.serial_cou
				LEFT JOIN extras e ON s.serial_sal = e.serial_sal
				JOIN customer cus ON cus.serial_cus = s.serial_cus AND s.status_sal <> 'DENIED'
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN user usr ON usr.serial_usr = cnt.serial_usr
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
				$responsable
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				$refund_join
				WHERE cit.serial_cou='$serial_cou'";

		if ($date_type == "insystem") {
			$sql .= " AND (STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y'))";
		} else if ($date_type == "coverage") {
			$sql .= " AND STR_TO_DATE(DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')
                      ";
		}
		//AND STR_TO_DATE(DATE_FORMAT(s.end_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND STR_TO_DATE('$end_date','%d/%m/%Y')) Se 
		if ($serial_cit != NULL) {
			$sql.=" AND cit.serial_cit = '$serial_cit'";
		}
		if ($serial_mbc != NULL) {
			$sql.=" AND dea.serial_mbc = '$serial_mbc'";
		}
		
		if ($dea_serial_dea != NULL) {
			$sql.=" AND dea.dea_serial_dea = '$dea_serial_dea'";
		}
		if ($serial_dea != NULL) {
			$sql.=" AND dea.serial_dea = '$serial_dea'";
		}
		if ($serial_pro != NULL) {
			$sql.=" AND pxc.serial_pro = '$serial_pro'";
		}
		if ($type_sal != NULL) {
			$sql.=" AND s.type_sal = '$type_sal'";
		}
		if ($total_sal != NULL) {
			$sql.=" AND s.total_sal " . $operator . $total_sal;
		}
		if ($stock_type != NULL) {
			$sql.=" AND s.stock_type_sal = '$stock_type'";
		}
		if ($status_sal != NULL) {
			$sql .= " AND s.status_sal IN ($status_sal)";
		}
		$sql.=" group by s.serial_sal ORDER BY $order_by";

//		echo $sql;
		//die('<pre>'.$sql.'</pre>');
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}
	
	/*New Report for manager*/
	
	public static function getInfoForManagerAnalysisReport($db, $serial_cou, $serial_mbc, $begin_date, $end_date) {
		if($active_only):
			$active_only_sql = " AND d.status_dea = 'ACTIVE'";
		endif;
			
		$sql = "SELECT	DISTINCT u.serial_usr, 
						CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'name_usr',
						SUM(sal.total_sal) as sales, 
						SUM(sal.total_cost_sal) as cost,
						comissions_table.effective_comission AS 'effective_comission'
				FROM user u " .
				self::SQLComissions($serial_cou, $begin_date, $end_date, $serial_mbc) .
				"
				JOIN user_by_dealer ubd ON ubd.serial_usr = u.serial_usr
				JOIN dealer d ON d.serial_dea=ubd.serial_dea $active_only_sql
				JOIN dealer parentd ON parentd.serial_dea = d.dea_serial_dea
				JOIN counter cnt ON cnt.serial_dea = d.serial_dea
				JOIN sales sal ON sal.serial_cnt = cnt.serial_cnt 
					AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') 
						BETWEEN ubd.begin_date_ubd AND 
						IFNULL(ubd.end_date_ubd, CURRENT_DATE())
				JOIN manager_by_country mbc ON mbc.serial_mbc = d.serial_mbc
				WHERE d.serial_mbc = $serial_mbc
				AND mbc.serial_cou = $serial_cou
				AND sal.status_sal <> 'DENIED'
				AND STR_TO_DATE(DATE_FORMAT(sal.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') BETWEEN STR_TO_DATE('" . $begin_date . "','%d/%m/%Y') AND STR_TO_DATE('" . $end_date . "','%d/%m/%Y')
				GROUP BY u.serial_usr";
		//die('<pre>'.$sql.'</pre>');
		$result = $db->getAll($sql);
		if ($result) {
			return $result;
			
		} else {
			return FALSE;
		}
	}

	
	public static function SQLComissions($serial_cou, $begin_date, $end_date, $serial_mbc = NULL) {
		if ($serial_mbc)
			$mbc_clause = " AND va.serial_mbc = $serial_mbc";
		

		$sql = "LEFT JOIN (SELECT va.serial_usr,
							ROUND(AVG(global_comission), 2) AS 'theoric_comission',
							ROUND((SUM(comission_value)/SUM(total_sal)) * 100, 2) AS 'effective_comission'
							FROM view_average_comission va 
							JOIN user u ON u.serial_usr = va.serial_usr
							WHERE STR_TO_DATE(DATE_FORMAT(emission_date_sal, '%d/%m/%Y'), '%d/%m/%Y') 
								BETWEEN STR_TO_DATE('$begin_date', '%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
				AND serial_cou = $serial_cou
				$mbc_clause
				GROUP BY va.serial_usr
				 )  AS comissions_table ON comissions_table.serial_usr = u.serial_usr";

		//Debug::print_r($sql);
		//$result = $db->getAll($sql);
	
			return $sql;
	
	}
	
    public static function GetSalesByCustomer($db, $document_cus) {
        $sql = "SELECT sal.card_number_sal, sal.in_date_sal,  pbl.name_pbl,cou.name_cou, sal.begin_date_sal, sal.end_date_sal, sal.status_sal 
                FROM sales sal 
                JOIN customer cus ON cus.serial_cus = sal.serial_cus AND cus.document_cus = '$document_cus'
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2
                JOIN city cit ON cit.serial_cit= sal.serial_cit
                JOIN country cou  ON cou.serial_cou = cit.serial_cou";
        //echo $sql; die();
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function GetDataPdfBySale($db, $serial_sal) {
        $sql = "SELECT  sal.in_date_sal, sal.card_number_sal,cus.last_name_cus, cus.first_name_cus,cus.document_cus, cus.birthdate_cus,
                cus.phone1_cus, cus.email_cus, cus.address_cus, cus.relative_cus, cus.relative_phone_cus, pbl.name_pbl, sal.fee_sal, sal.total_sal
                FROM sales sal 
                JOIN customer cus ON cus.serial_cus = sal.serial_cus AND sal.serial_sal = $serial_sal
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2
                JOIN city cit ON cit.serial_cit= sal.serial_cit
                JOIN country cou  ON cou.serial_cou = cit.serial_cou";
        //echo $sql; die();
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }    
    
    public static function GetRegisterTravelByCustomer($db, $document_cus) {
        $sql = "SELECT sal.serial_sal, sal.card_number_sal, pbl.name_pbl, sal.in_date_sal, cou.serial_cou, cou.name_cou, sal.begin_date_sal, sal.end_date_sal, sal.days_sal, (sal.days_sal-SUM(tvl.days_trl)) as days
                FROM sales sal
                JOIN customer cus ON cus.serial_cus = sal.serial_cus  AND cus.document_cus = '$document_cus' AND sal.status_sal = 'ACTIVE'
                JOIN traveler_log tvl ON sal.serial_sal = tvl.serial_sal 
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
                                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                                JOIN product pro ON pro.serial_pro = pbc.serial_pro
                                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2                                
                JOIN city cit ON cit.serial_cit= tvl.serial_cit
                JOIN country cou  ON cou.serial_cou = cit.serial_cou
                GROUP BY sal.serial_sal";
       //echo $sql; die();
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }    
    
     public static function GetTravelsBySale($db, $serial_sal) {
        $sql = "SELECT  sal.card_number_sal, cus.first_name_cus, cus.last_name_cus, cou.name_cou, tvl.start_trl, tvl.end_trl, tvl.days_trl
                FROM sales sal 
                JOIN traveler_log tvl ON sal.serial_sal = tvl.serial_sal AND sal.serial_sal = $serial_sal
                JOIN customer cus ON cus.serial_cus = sal.serial_cus                   
                JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
                JOIN product_by_country pbc ON pbc.serial_pxc = pbd.serial_pxc
                JOIN product pro ON pro.serial_pro = pbc.serial_pro
                JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang = 2
                JOIN city cit ON cit.serial_cit= tvl.serial_cit
                JOIN country cou  ON cou.serial_cou = cit.serial_cou";
        //echo $sql; die();
        $result = $db->getAll($sql);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }    
     public  function getTransactionByCustomer($db, $serial_Cus){
		$sql = "SELECT transaction_id FROM sales WHERE serial_cus = $serial_Cus";
		//echo $sql;
                $result = $db -> getAll($sql);
		if ($result){
                return $result;}
                else {
                    return false;
                }
	}
    
     public  function getSalesByTransaction($db, $transaction_id){
		$sql = "SELECT transaction_id, serial_sal, serial_cus FROM sales WHERE transaction_id = $transaction_id";
		//echo $sql;
                $result = $db -> getAll($sql);
		if ($result){
                return $result;}
                else {
                    return false;
                }
	}
    //GETTERS
	function getSerial_sal() {
		return $this->serial_sal;
	}
    
    function getSerial_con() {
        return $this->serial_con;
    }

	function getSerial_cus() {
		return $this->serial_cus;
	}

	function getSal_serial_sal() {
		return $this->sal_serial_sal;
	}

	function getSerial_pbd() {
		return $this->serial_pbd;
	}

	function getSerial_cur() {
		return $this->serial_cur;
	}

	function getSerial_cit() {
		return $this->serial_cit;
	}

	function getSerial_cnt() {
		return $this->serial_cnt;
	}

	function getSerial_inv() {
		return $this->serial_inv;
	}

	function getCardNumber_sal() {
		return $this->card_number_sal;
	}

	function getEmissionDate_sal() {
		return $this->emission_date_sal;
	}

	function getBeginDate_sal() {
		return $this->begin_date_sal;
	}

	function getEndDate_sal() {
		return $this->end_date_sal;
	}

	function getInDate_sal() {
		return $this->in_date_sal;
	}

	function getDays_sal() {
		return $this->days_sal;
	}

	function getFee_sal() {
		return $this->fee_sal;
	}

	function getObservations_sal() {
		return $this->observations_sal;
	}

	function getCost_sal() {
		return $this->cost_sal;
	}

	function getFree_sal() {
		return $this->free_sal;
	}

	function getIdErp_sal() {
		return $this->id_erp_sal;
	}

	function getStatus_sal() {
		return $this->status_sal;
	}

	function getType_sal() {
		return $this->type_sal;
	}

	function getTotal_sal() {
		return $this->total_sal;
	}

	function getStockType_sal() {
		return $this->stock_type_sal;
	}

	function getDirectSale_sal() {
		return $this->direct_sale_sal;
	}

	function getTotalCost_sal() {
		return $this->total_cost_sal;
	}

	function getChange_fee_sal() {
		return $this->change_fee_sal;
	}

	function getAux() {
		return $this->aux;
	}

	function getInternationalFeeStatus_sal() {
		return $this->international_fee_status;
	}

	function getInternationalFeeUsed_sal() {
		return $this->international_fee_used;
	}

	function getInternational_fee_amount() {
		return $this->international_fee_amount;
	}
        
        function getDeliveredKit() {
            return $this->deliveredKit;
        }
        
     function getTransaction_Id($transactionId){
            $this->transation_id = $transactionId;
        }    

	//SETTERS
	function setSerial_sal($serial_sal) {
		$this->serial_sal = $serial_sal;
	}
    
    function setSerial_con($serial_con) {
		$this->serial_con = $serial_con;
	}

	function setSerial_cus($serial_cus) {
		$this->serial_cus = $serial_cus;
	}

	function setSal_serial_sal($sal_serial_sal) {
		$this->sal_serial_sal = $sal_serial_sal;
	}

	function setSerial_pbd($serial_pbd) {
		$this->serial_pbd = $serial_pbd;
	}

	function setSerial_cur($serial_cur) {
		$this->serial_cur = $serial_cur;
	}

	function setSerial_cit($serial_cit) {
		$this->serial_cit = $serial_cit;
	}

	function setSerial_cnt($serial_cnt) {
		$this->serial_cnt = $serial_cnt;
	}

	function setSerial_inv($serial_inv) {
		$this->serial_inv = $serial_inv;
	}

	function setCardNumber_sal($card_number_sal) {
		$this->card_number_sal = $card_number_sal;
	}

	function setEmissionDate_sal($emission_date_sal) {
		$this->emission_date_sal = $emission_date_sal;
	}

	function setBeginDate_sal($begin_date_sal) {
		$this->begin_date_sal = $begin_date_sal;
	}

	function setEndDate_sal($end_date_sal) {
		$this->end_date_sal = $end_date_sal;
	}

	function setInDate_sal($in_date_sal) {
		$this->in_date_sal = $in_date_sal;
	}

	function setDays_sal($days_sal) {
		$this->days_sal = $days_sal;
	}

	function setFee_sal($fee_sal) {
		$this->fee_sal = $fee_sal;
	}

	function setObservations_sal($observations_sal) {
		$this->observations_sal = $observations_sal;
	}

	function setCost_sal($cost_sal) {
		$this->cost_sal = $cost_sal;
	}

	function setFree_sal($free_sal) {
		$this->free_sal = $free_sal;
	}

	function setIdErpSal_sal($id_erp_sal) {
		$this->id_erp_sal = $id_erp_sal;
	}

	function setStatus_sal($status_sal) {
		$this->status_sal = $status_sal;
	}

	function setType_sal($type_sal) {
		$this->type_sal = $type_sal;
	}

	function setTotal_sal($total_sal) {
		$this->total_sal = $total_sal;
	}

	function setStockType_sal($stock_type_sal) {
		$this->stock_type_sal = $stock_type_sal;
	}

	function setDirectSale_sal($direct_sale_sal) {
		$this->direct_sale_sal = $direct_sale_sal;
	}

	function setTotalCost_sal($total_cost_sal) {
		$this->total_cost_sal = $total_cost_sal;
	}

	function setChange_fee_sal($change_fee_sal) {
		$this->change_fee_sal = $change_fee_sal;
	}

	function setInternationalFeeStatus_sal($international_fee_status) {
		$this->international_fee_status = $international_fee_status;
	}

	function setInternationalFeeUsed_sal($international_fee_used) {
		$this->international_fee_used = $international_fee_used;
	}

	function setInternationalFeeAmount_sal($international_fee_amount) {
		$this->international_fee_amount = $international_fee_amount;
	}
        
        function setDeliveredKit($deliveredKit) {
            $this->deliveredKit = $deliveredKit;
        }
        function setTransaction_Id($transactionId){
            $this->transation_id = $transactionId;
        }
}

?>