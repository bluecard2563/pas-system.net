<?php
/*
File: PaymentRequest.class.php
Author: Patricio Astudillo
Creation Date: 19/05/2010
Modified By: David Bergmann
Last Modified: 03/06/2010
*/

class PaymentRequest{
    var $db;
    var $serial_prq;
    var $serial_usr;
    var $usr_serial_usr;
    var $serial_fle;
    var $serial_dbf;
    var $usr_serial_usr2;
    var $status_prq;
    var $amount_reclaimed_prq;
    var $amount_authorized_prq;
    var $request_date_prq;
    var $audited_date_prq;
    var $dispatched_prq;
    var $auditor_comments;
    var $observations_prq;
    var $file_url_prq;
    var $dispatched_date_prq;
	var $type_prq;
	var $process_as;

    function __construct($db, $serial_prq = NULL, $serial_usr = NULL, $usr_serial_usr = NULL, $serial_fle=NULL,
                         $serial_dbf = NULL, $usr_serial_usr2 = NULL, $status_prq=NULL, $amount_reclaimed_prq=NULL,
                         $amount_authorized_prq=NULL, $request_date_prq=NULL, $audited_date_prq=NULL, $dispatched_prq=NULL,
                         $auditor_comments=NULL,$observations_prq=NULL,$file_url_prq=NULL,$dispatched_date_prq=NULL){
        $this -> db = $db;
        $this -> serial_prq = $serial_prq;
        $this -> serial_usr = $serial_usr;
        $this -> usr_serial_usr = $usr_serial_usr;
        $this -> serial_fle = $serial_fle;
        $this -> serial_dbf = $serial_dbf;
        $this -> usr_serial_usr2 = $usr_serial_usr2;
        $this -> status_prq=$status_prq;
        $this -> amount_reclaimed_prq=$amount_reclaimed_prq;
        $this -> amount_authorized_prq=$amount_authorized_prq;
        $this -> request_date_prq=$request_date_prq;
        $this -> audited_date_prq=$audited_date_prq;
        $this -> dispatched_prq=$dispatched_prq;
        $this -> auditor_comments=$auditor_comments;
        $this -> observations_prq=$observations_prq;
        $this -> file_url_prq=$file_url_prq;
        $this -> dispatched_date_prq=$dispatched_date_prq;
    }

    /*
     * getData
     * @return: True if OK / False on error.
     */
    function getData(){
        if($this->serial_prq!=''){
            $sql = "SELECT prq.serial_prq, prq.serial_usr, prq.usr_serial_usr, prq.serial_fle,
                           prq.usr_serial_usr2, prq.status_prq, prq.amount_reclaimed_prq, prq.amount_authorized_prq,
                           DATE_FORMAT(prq.request_date_prq, '%d/%m/%Y') AS 'request_date_prq',
						   DATE_FORMAT(prq.audited_date_prq, '%d/%m/%Y') AS 'audited_date_prq', prq.dispatched_prq, prq.auditor_comments,
                           prq.observations_prq, prq.file_url_prq, 
						   DATE_FORMAT(prq.dispatched_date_prq, '%d/%m/%Y') AS 'dispatched_date_prq', f.serial_cus,
						   prq.type_prq, prq.serial_dbf, prq.process_as
                    FROM payment_request prq
                    JOIN file f ON f.serial_fle=prq.serial_fle
                    WHERE serial_prq=".$this->serial_prq;

            $result=$this->db->Execute($sql);

            if($result->fields['0']){
                $this -> serial_usr = $result->fields['1'];
				$this -> serial_dbf = $result->fields['17'];
                $this -> usr_serial_usr = $result->fields['2'];
                $this -> serial_fle = $result->fields['3'];
                $this -> usr_serial_usr2 = $result->fields['4'];
                $this -> status_prq=$result->fields['5'];
                $this -> amount_reclaimed_prq=$result->fields['6'];
                $this -> amount_authorized_prq=$result->fields['7'];
                $this -> request_date_prq=$result->fields['8'];
                $this -> audited_date_prq=$result->fields['9'];
                $this -> dispatched_prq=$result->fields['10'];
                $this -> auditor_comments=$result->fields['11'];
                $this -> observations_prq=$result->fields['12'];
                $this -> file_url_prq=$result->fields['13'];
                $this -> dispatched_date_prq=$result->fields['14'];
                $this -> aux['serial_cus']=$result->fields['15'];
				$this -> type_prq = $result->fields['16'];
				$this -> process_as = $result->fields['17'];

                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /*
    * function insert
    * @returns: TRUE if OK, FALSE otherwise
    */
    function insert(){
        $sql = "INSERT INTO payment_request(
					serial_prq,
					serial_usr,
					usr_serial_usr,
					serial_fle,
					serial_dbf,
					usr_serial_usr2,
					status_prq,
					amount_reclaimed_prq,
					amount_authorized_prq,
					request_date_prq,
					audited_date_prq,
					dispatched_prq,
					auditor_comments,
					observations_prq,
					file_url_prq,
					dispatched_date_prq,
					type_prq,
					process_as
				)VALUES(
					NULL,
                    '".$this->serial_usr."',";
					($this->usr_serial_usr != NULL)?$sql.= "'".$this->usr_serial_usr."',":$sql.="NULL,";
                    $sql .= "'".$this->serial_fle."',";
					($this->serial_dbf != NULL)?$sql.="'".$this->serial_dbf."',":$sql.="NULL,";
			$sql .= "NULL,
					'".$this->status_prq."',
					'".$this->amount_reclaimed_prq."',
					'".$this->amount_authorized_prq."',
					NOW(),
					NULL,
					'NO',
					NULL,
					'".$this->observations_prq."',
					NULL,
					NULL,";
					($this->type_prq != NULL)?$sql.="'".$this->type_prq."',":$sql.="NULL";
					
					  ($this->process_as != NULL)?$sql.="'".$this->process_as."')":$sql.="'MIXED')";
        $result = $this->db->Execute($sql);
//        Debug::print_r($sql);die();
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /***********************************************
    * function update
    * update a row in the DB
    ***********************************************/
    function update(){
        $sql = "UPDATE payment_request SET
                       serial_usr = $this->serial_usr,";
        if($this->usr_serial_usr){
            $sql .= " usr_serial_usr = '$this->usr_serial_usr',";
        } else {
            $sql.=" usr_serial_usr = NULL,";
        }
        if($this->usr_serial_usr2){
            $sql .= " usr_serial_usr2 = $this->usr_serial_usr2,";
        } else {
            $sql.=" usr_serial_usr2 = NULL,";
        }
        $sql.="	serial_fle = $this->serial_fle,
                status_prq = '$this->status_prq',
                amount_reclaimed_prq = '$this->amount_reclaimed_prq',
                amount_authorized_prq = '$this->amount_authorized_prq',";
        if($this->request_date_prq){
            $sql .= " request_date_prq = STR_TO_DATE('$this->request_date_prq','%d/%m/%Y'),";
        } else {
            $sql.=" request_date_prq = NULL,";
        }
        if($this->audited_date_prq){
            $sql .= " audited_date_prq = STR_TO_DATE('$this->audited_date_prq','%d/%m/%Y'),";
        } else {
            $sql.=" audited_date_prq = NULL,";
        }
        $sql.="	dispatched_prq = '$this->dispatched_prq',
                auditor_comments = '$this->auditor_comments',
                observations_prq = '$this->observations_prq',
                file_url_prq = '$this->file_url_prq',";
        if($this->dispatched_date_prq!=NULL){
            $sql .= " dispatched_date_prq = STR_TO_DATE('$this->dispatched_date_prq','%d/%m/%Y'),";
        } else {
            $sql.=" dispatched_date_prq = NULL,";
        }		
		($this->type_prq!=NULL)?$sql.=" type_prq = '$this->type_prq'":$sql.=" type_prq = NULL";
		
		$sql .= " WHERE serial_prq = '$this->serial_prq'";

        $result = $this->db->Execute($sql);

        if($result == true){
            return true;
        }else{
			ErrorLog::log($this -> db, 'UPDATE FAILED - '.get_class($this), $sql);
            return false;
        }
    }

    /*
     * getPendingMedicalApplications
     * @param: $db - DB Connection
     * @param: $serial_usr - Owner of the applications.
     * @return: An array of pending applications data.
     */
    public static function getPendingMedicalApplications($db, $serial_usr){
            $sql = "SELECT prq.serial_prq, CONCAT(c.first_name_cus,' ',c.last_name_cus) AS 'name_cus', f.diagnosis_fle,
                                       CONCAT(u.first_name_usr,' ',u.last_name_usr)	AS 'name_usr', f.serial_fle, 
										DATE_FORMAT(prq.request_date_prq, '%d/%m/%Y') AS 'request_date_prq'
                            FROM payment_request prq
                            JOIN file f ON f.serial_fle=prq.serial_fle
                            JOIN customer c ON c.serial_cus=f.serial_cus
                            JOIN user u ON u.serial_usr=prq.serial_usr
                            WHERE prq.usr_serial_usr='$serial_usr'
                            AND prq.status_prq='AUDIT_REQUEST'";

            $result=$db->getAll($sql);

            if($result){
                    return $result;
            }else{
                    return false;
            }
    }


    /*
     * getPaymentRequestStates
     * @returns: an array of all the payment_request status
     */
    function getPaymentRequestStates(){
        $sql = "SHOW COLUMNS FROM payment_request LIKE 'status_prq'";
        $result = $this->db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }
	
	/*
     * getPaymentRequestStates
     * @returns: an array of all the payment_request status
     */
    public static function getPaymentRequestTypes($db){
		global $global_payment_request_type;
        $sql = "SHOW COLUMNS FROM payment_request LIKE 'type_prq'";
        $result = $db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		
		$types_list = array();
		foreach($type as $item){
			$types_list[$item] = $global_payment_request_type[$item];
		}
        return $types_list;
    }

    /*
     * getPrintedValues
     * @returns: an array of all the values in the printed_prq field.
     */
    function getDispatchedValues(){
        $sql = "SHOW COLUMNS FROM payment_request LIKE 'dispatched_prq'";
        $result = $this->db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
        return $type;
    }

    /*
     * getAmountOfRequestsByUser
     * @returns: the number of pendin requests by user
     */
    function getAmountOfRequestsByUser($serial_usr){
        $sql = "SELECT count(*)
                FROM payment_request
                WHERE usr_serial_usr=".$serial_usr."
                AND status_prq='AUDIT_REQUEST'";
        //die($sql);
        $result = $this->db -> Execute($sql);

        if($result->fields[0]){
                return $result->fields[0];
        }else{
                return 0;
        }
    }

    /*
     * getAmountOfRequestsByUser
     * @returns: the number of pendin requests by user
     */
    public static function fileHasPendingRequests($db,$serial_fle){
        $sql = "SELECT serial_fle
                FROM payment_request
                WHERE serial_fle=".$serial_fle."
                AND status_prq='AUDIT_REQUEST'";
        //die($sql);
        $result = $db -> Execute($sql);

        if($result->fields[0]){
                return $result->fields[0];
        }else{
                return false;
        }
    }

    /*
     * getMedicalCheckupsByFile
     * @param: $db - DB Connection
     * @param: $serial_usr - Owner of the applications.
     * @return: An array of medical checkups by file data.
     */
    public static function getMedicalCheckupsByFile($db, $serial_fle){
        $sql = "SELECT auditor_comments, DATE_FORMAT(request_date_prq,'%d/%m/%Y') as request_date_prq,
                       DATE_FORMAT(audited_date_prq,'%d/%m/%Y') as audited_date_prq, file_url_prq
                FROM payment_request
                WHERE serial_fle='$serial_fle'
                AND status_prq='AUDITED'";

        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

	/*
     * getMedicalCheckupsByFile
     * @param: $db - DB Connection
     * @param: $serial_usr - Owner of the applications.
     * @return: An array of medical checkups by file data.
     */
    public static function getPendingMedicalCheckupsByFile($db, $serial_fle){
        $sql = "SELECT serial_prq
                FROM payment_request
                WHERE serial_fle='$serial_fle'
                AND status_prq='AUDIT_REQUEST'";

        $result=$db->getAll($sql);

        if($result){
            return $result[0];
        }else{
            return false;
        }
    }

    /*
     * getPendingClaimsByCountry
     * @param: $db - DB Connection
     * @param: $serial_cou, $serial_spv
     * @return: An array of pending claims data.
     */
    public static function getPendingClaimsByCountry($db, $document_to_dbf, $invoiceTo = 'MIXED', $serial_spv=NULL, $serial_fle=NULL){
        $sql = "SELECT DISTINCT prq.*, DATE_FORMAT(prq.request_date_prq,'%d/%m/%Y') as request_date, dbf.*, CONCAT(cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'')) as name_cus,
                       cus.serial_cus, IFNULL(prq.type_prq, 'N/A') AS 'type_prq',cou.name_cou,
                       DATE_FORMAT(dbf.document_date_dbf,'%d/%m/%Y') AS 'document_date_dbf'
                FROM payment_request prq
                JOIN benefits_by_document bbd
                JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf AND bbd.serial_dbf = dbf.serial_dbf
                JOIN service_provider_by_file spf ON spf.serial_spf = bbd.serial_spf AND spf.serial_fle = dbf.serial_fle
                JOIN file fle ON fle.serial_fle = prq.serial_fle AND fle.serial_fle = dbf.serial_fle AND fle.serial_fle = spf.serial_fle
                JOIN sales sal ON sal.serial_sal = fle.serial_sal
                JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
                JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
                JOIN sector sec ON sec.serial_sec = dea.serial_sec
                JOIN city cit ON cit.serial_cit = sec.serial_cit
                JOIN country cou ON cou.serial_cou = cit.serial_cou
                JOIN customer cus ON cus.serial_cus = fle.serial_cus
                WHERE dbf.document_to_dbf='$document_to_dbf'
                AND prq.status_prq ='APROVED'
                AND prq.dispatched_prq ='NO'";

        $sql.=" AND prq.process_as IN ('$invoiceTo', 'MIXED')";

        if($serial_spv != NULL){
            $sql.=" AND spf.serial_spv='$serial_spv'";
        }

        if($serial_fle != NULL){
            $sql.=" AND fle.serial_fle=$serial_fle";
        }
        $sql.= " ORDER BY request_date";

//        Debug::print_r($sql); die;
        //echo $sql;
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /*
     * getPayOffDocument
     * @param: $db - DB Connection
     * @param: $serial_usr - Owner of the applications.
     * @return: An array of pending applications data.
     */
    public static function getPayOffDocument($db,$claims) {
        $sql=   "SELECT DISTINCT cit.serial_cou, cus.document_cus, CONCAT(cus.first_name_cus,' ', IFNULL(cus.last_name_cus,'')) as name_cus,
						own.document_cus, CONCAT(own.first_name_cus,' ', own.last_name_cus) as name_owner,
						day(sal.emission_date_sal) as emission_day, month(sal.emission_date_sal) as emission_month,
						year(sal.emission_date_sal) as emission_year, IF( sal.card_number_sal = '0', trl.card_number_trl, IFNULL(sal.card_number_sal, trl.card_number_trl)) as card_number,
						DATE_FORMAT(fle.incident_date_fle,'%d/%m/%Y') as incident_date_fle, fle.type_fle,
                        DATE_FORMAT(sal.begin_date_sal,'%d/%m/%Y') as begin_date_sal, DATE_FORMAT(sal.end_date_sal,'%d/%m/%Y') as end_date_sal,
                        spf.medical_type_spf, spv.type_spv, sal.serial_sal
                 FROM payment_request prq
                 JOIN `file` fle ON fle.serial_fle = prq.serial_fle
                 JOIN customer cus ON cus.serial_cus = fle.serial_cus
                 JOIN sales sal ON sal.serial_sal = fle.serial_sal
				 JOIN customer own ON own.serial_cus = sal.serial_cus
				 JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
				 JOIN user usr ON usr.serial_usr = cnt.serial_usr
				 JOIN city cit ON cit.serial_cit = usr.serial_cit
                 LEFT JOIN traveler_log trl ON trl.serial_trl = fle.serial_trl
				 LEFT JOIN service_provider_by_file spf ON spf.serial_fle = fle.serial_fle
				 LEFT JOIN service_provider spv ON spv.serial_spv = spf.serial_spv
                 WHERE prq.serial_prq IN (".$claims.")";
        //die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result[0];
        }else{
            return false;
        }
    }

    /***********************************************
    * funcion getZonesWithPendingClaims
    * gets all the Zones with pending claims
    ***********************************************/
    public static function getZonesWithPendingClaims($db){
        $sql =  "SELECT DISTINCT zon.serial_zon, zon.name_zon
                 FROM zone zon
                 JOIN country cou ON cou.serial_zon = zon.serial_zon
                 JOIN city cit ON cit.serial_cou = cou.serial_cou
                 JOIN customer cus ON cus.serial_cit = cit.serial_cit
                 JOIN sales sal ON sal.serial_cus = cus.serial_cus
                 JOIN `file` fle ON fle.serial_sal = sal.serial_sal
                 JOIN payment_request prq ON prq.serial_fle = fle.serial_fle
				 JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                 WHERE prq.dispatched_prq = 'NO'
                 AND prq.status_prq = 'APROVED'
				 AND dbf.document_to_dbf = 'CLIENT'
                 ORDER BY zon.name_zon";
                 //die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }
	
    /***********************************************
    * funcion getCountriesWithPendingClaims
    * gets all the Countries with pending claims
    ***********************************************/
    public static function getCountriesWithPendingClaims($db,$serial_zon){
        $sql =  "SELECT DISTINCT cou.serial_cou, cou.name_cou
                 FROM country cou 
                 JOIN city cit ON cit.serial_cou = cou.serial_cou
                 JOIN customer cus ON cus.serial_cit = cit.serial_cit
                 JOIN sales sal ON sal.serial_cus = cus.serial_cus
                 JOIN `file` fle ON fle.serial_sal = sal.serial_sal
                 JOIN payment_request prq ON prq.serial_fle = fle.serial_fle
				 JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                 WHERE prq.dispatched_prq = 'NO'
                 AND prq.status_prq = 'APROVED'
				 AND dbf.document_to_dbf = 'CLIENT'
                 AND cou.serial_zon = $serial_zon
                 ORDER BY cou.name_cou";
                 //die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /***********************************************
    * funcion getCitiesWithPendingClaims
    * gets all the Cities with pending claims
    ***********************************************/
    public static function getCitiesWithPendingClaims($db,$serial_cou){
        $sql =  "SELECT DISTINCT cit.serial_cit, cit.name_cit
                 FROM city cit
                 JOIN customer cus ON cus.serial_cit = cit.serial_cit
                 JOIN sales sal ON sal.serial_cus = cus.serial_cus
                 JOIN `file` fle ON fle.serial_sal = sal.serial_sal
                 JOIN payment_request prq ON prq.serial_fle = fle.serial_fle
				 JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                 WHERE prq.dispatched_prq = 'NO'
                 AND prq.status_prq = 'APROVED'
				 AND dbf.document_to_dbf = 'CLIENT'
                 AND cit.serial_cou = $serial_cou
                 ORDER BY cit.name_cit";
                 //die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /***********************************************
    * funcion getCustomersWithPendingClaims
    * gets all the Customers with pending claims
    ***********************************************/
    public static function getCustomersWithPendingClaims($db,$serial_cit){
        $sql =  "SELECT DISTINCT cus.serial_cus, cus.document_cus, CONCAT(cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'')) as name_cus
                 FROM customer cus
                 JOIN sales sal ON sal.serial_cus = cus.serial_cus
                 JOIN `file` fle ON fle.serial_sal = sal.serial_sal
                 JOIN payment_request prq ON prq.serial_fle = fle.serial_fle
				 JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                 WHERE prq.dispatched_prq = 'NO'
                 AND prq.status_prq = 'APROVED'
				 AND dbf.document_to_dbf = 'CLIENT'
                 AND cus.serial_cit = $serial_cit
                 ORDER BY name_cus";
                 //die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /***********************************************
    * funcion getCardsWithPendingClaims
    * gets all the Cards with pending claims
    ***********************************************/
    public static function getCardsWithPendingClaims($db,$serial_cus){
        $sql =  "SELECT DISTINCT IFNULL(sal.card_number_sal, trl.card_number_trl) as card_number
                 FROM sales sal
                 LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal
                 JOIN `file` fle ON fle.serial_sal = sal.serial_sal
                 JOIN payment_request prq ON prq.serial_fle = fle.serial_fle
				 JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                 WHERE prq.dispatched_prq = 'NO'
                 AND prq.status_prq = 'APROVED'
				 AND dbf.document_to_dbf = 'CLIENT'
                 AND sal.serial_cus = $serial_cus
                 ORDER BY card_number";
                 //die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /***********************************************
    * funcion getFilesByCard
    * gets all the Files belonging to a Card
    ***********************************************/
    public static function getFilesByCardWithPendingClaims($db, $card_number){
        $sql = "SELECT DISTINCT f.serial_fle, cou.name_cou, cit.name_cit, f.serial_trl, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
                       f.serial_sal, f.diagnosis_fle, f.cause_fle, f.status_fle, DATE_FORMAT(f.creation_date_fle, '%d/%m/%Y') as creation_date_fle,
                       f.currency_fle, f.exchange_rate_fle, f.total_cost_fle, f.authorized_fle,
                       f.observations_fle, f.end_date_fle, f.type_fle
                FROM `file` f
                JOIN sales s ON s.serial_sal = f.serial_sal
                JOIN customer cus ON cus.serial_cus = s.serial_cus
                JOIN city cit ON cit.serial_cit = f.serial_cit
                JOIN country cou ON cou.serial_cou = cit.serial_cou
                JOIN payment_request prq ON prq.serial_fle = f.serial_fle
				JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl
                WHERE (s.card_number_sal='".$card_number."'
                OR trl.card_number_trl='".$card_number."')
                AND prq.dispatched_prq = 'NO'
                AND prq.status_prq = 'APROVED'
				AND dbf.document_to_dbf = 'CLIENT'
                ORDER BY f.serial_fle";
        //die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

	/***********************************************
    * funcion getFilesAndClaimsByCard
    * gets all the Files belonging to a Card
    ***********************************************/
    public static function getFilesAndClaimsByCard($db, $card_number){
        $sql = "SELECT DISTINCT f.serial_fle, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus
                FROM `file` f
                JOIN sales s ON s.serial_sal = f.serial_sal
                JOIN customer cus ON cus.serial_cus = f.serial_cus
                JOIN payment_request prq ON prq.serial_fle = f.serial_fle 
					AND dispatched_prq = 'NO' 
					AND (file_url_prq IS NULL OR file_url_prq = '')
				JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
				JOIN benefits_by_document bbd ON bbd.serial_dbf = dbf.serial_dbf
                LEFT JOIN traveler_log trl ON trl.serial_trl = f.serial_trl
                WHERE (s.card_number_sal='".$card_number."'
                OR trl.card_number_trl='".$card_number."')
				AND f.status_fle <> 'closed'
                ORDER BY f.serial_fle";
        //Debug::print_r($sql); die;
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    /***********************************************
    * funcion getFilesByCard
    * gets all the Files belonging to a Card
    ***********************************************/
    public static function getLiquidationDocument($db, $serial_prq){
		//*************** VALIDATIONS FOR TRAVELER LOG
		$trl_cards = self::getAffectedCardByAssistancesPayments($db, $serial_prq);
		if($trl_cards){
			$trl_card_numbers = array();
			foreach($trl_cards as $c){
				array_push($trl_card_numbers, "'".$c['card_number_trl']."'");
			}
			
			$trl_card_numbers = implode(',', $trl_card_numbers);
			$trl_card_numbers = " AND card_number_trl IN ($trl_card_numbers)";
		}
		
		
        $sql = "SELECT IFNULL(trl.serial_cus, cus.serial_cus) AS 'serial_cus', fle.serial_fle,
					   prq.amount_reclaimed_prq, prq.amount_authorized_prq, IFNULL(sal.card_number_sal, trl.card_number_trl) as card_number, fle.creation_date_fle,
					   fle.diagnosis_fle, dbf.comment_dbf, prq.serial_dbf, prq.type_prq,
					   IF(fle.serial_trl IS NULL, 
							CONCAT(cus.first_name_cus, ' ',IFNULL(cus.last_name_cus,'')),  
							CONCAT(tc.first_name_cus, ' ',IFNULL(tc.last_name_cus,''))) AS name_cus
                FROM payment_request prq
                JOIN `file` fle ON fle.serial_fle = prq.serial_fle
                JOIN sales sal ON sal.serial_sal = fle.serial_sal
                JOIN customer cus ON cus.serial_cus = fle.serial_cus
                LEFT JOIN traveler_log trl ON trl.serial_trl = fle.serial_trl $trl_card_numbers
				LEFT JOIN customer tc ON tc.serial_cus = trl.serial_cus
                JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                WHERE prq.serial_prq IN ($serial_prq)";
        //Debug::print_r($sql); die;
        $result = $db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                        $arreglo[$cont]=$result->GetRowAssoc(false);
                        $result->MoveNext();
                        $cont++;
                }while($cont<$num);
        }
        return $arreglo;
    }

	/*generate name of upload file*/
	public static function generateFileName($db,$exts){
		
		do{
			$name='medicalcheckup_'.rand(1, 1000).'.'.$exts;
			
			$sql="SELECT file_url_prq
			      FROM payment_request
				  WHERE file_url_prq='".DOCUMENT_ROOT."assistanceFiles/file_IDFILE/".$name."'";

			$id=$db->getOne($sql);

		}while($id);

		return $name;
	}
	
	/*
     * getPendingPayments
     * @param: $selPayTo - Pay to
     * @param: $selType - Document Type .
	 * @param: $txtBeginDate - begin date limit.
	 * @param: $txtEndDate - end date limit.
     * @return: An array of pending payment requests data.
     */
	function getPendingPayments($selPayTo,$selType,$txtBeginDate,$txtEndDate){
		$sql = "SELECT *
				FROM payment_request prq
				JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
				WHERE prq.request_date_prq BETWEEN 
											STR_TO_DATE('$txtBeginDate','%d/%m/%Y') AND 
											STR_TO_DATE('$txtEndDate', '%d/%m/%Y')
				AND dbf.document_to_dbf = '$selPayTo'
				AND prq.type_prq = '$selType'
				AND prq.status_prq = 'APROVED'
				AND prq.dispatched_prq = 'NO' 
			";
		$result = $this->db->getAll($sql);
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/***********************************************
    * funcion getZonesWithPendingClaims
    * gets all the Zones with pending claims
    ***********************************************/
    public static function getZonesWithClaims($db, $document_to = NULL, $status_prq = NULL){
        $sql =  "SELECT DISTINCT zon.serial_zon, zon.name_zon
                 FROM zone zon
                 JOIN country cou ON cou.serial_zon = zon.serial_zon
                 JOIN product_by_country pxc ON pxc.serial_cou = cou.serial_cou
				 JOIN product_by_dealer pbd ON pbd.serial_pxc = pxc.serial_pxc
                 JOIN sales sal ON sal.serial_pbd = pbd.serial_pbd
                 JOIN `file` fle ON fle.serial_sal = sal.serial_sal
                 JOIN payment_request prq ON prq.serial_fle = fle.serial_fle
				 JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
				WHERE dbf.document_to_dbf = '$document_to'";

		if($status_prq)	$sql .= " AND prq.status_prq = '$status_prq'";

		$sql .="  ORDER BY zon.name_zon";

		//die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }
	 /***********************************************
    * funcion getCountriesClaims
    * gets all the Countries with pending claims
    ***********************************************/
    public static function getCountriesWithClaims($db,$serial_zon,$document_to=NULL){
        $sql =  "SELECT DISTINCT cou.serial_cou, cou.name_cou
                 FROM country cou
                 JOIN product_by_country pxc ON pxc.serial_cou = cou.serial_cou
				 JOIN product_by_dealer pbd ON pbd.serial_pxc = pxc.serial_pxc
                 JOIN sales sal ON sal.serial_pbd = pbd.serial_pbd
                 JOIN `file` fle ON fle.serial_sal = sal.serial_sal
                 JOIN payment_request prq ON prq.serial_fle = fle.serial_fle
				 JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                 WHERE prq.status_prq = 'APROVED'
				 AND dbf.document_to_dbf = '$document_to'";
          $sql .=  " AND cou.serial_zon = $serial_zon
                 ORDER BY cou.name_cou";
                 //die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

	 /***********************************************
    * funcion getCitiesWithPendingClaims
    * gets all the Cities with pending claims
    ***********************************************/
    public static function getCitiesWithClaims($db,$serial_cou,$document_to=NULL){
        $sql =  "SELECT DISTINCT cit.serial_cit, cit.name_cit
					FROM city cit
					JOIN sector sec ON cit.serial_cit = sec.serial_cit
					JOIN dealer dea ON sec.serial_sec = dea.serial_sec
					JOIN counter cnt ON dea.serial_dea = cnt.serial_dea
					JOIN sales sal ON cnt.serial_cnt = sal.serial_cnt
					JOIN `file` fle ON sal.serial_sal = fle.serial_sal
					JOIN payment_request prq ON prq.serial_fle = fle.serial_fle
					JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                 WHERE prq.status_prq = 'APROVED'
				 AND dbf.document_to_dbf = '$document_to'
                 AND cit.serial_cou = $serial_cou
                 ORDER BY cit.name_cit";
                 //die($sql);
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

	/*
     * getClaimsForReport
     * @param: $db - DB Connection
     * @param: $selPayTo, $selType, $txtBeginDate, $txtEndDate, $selProvider, $selCity, $selectedBoxes
	 * Columns Documento,Pais,N. Documento,Concepto,Valor aprobado,	Fecha,Cliente
     * @return: An array of pending claims data.
     */
    public static function getClaimsForReport($db, $document_to_dbf, $date_from, $date_end, $processAs,
                                                $serial_spv = NULL,$serial_cit = NULL, $claims_serials = NULL){
        switch ($processAs):
            case 'PAYABLE': $processAss = 'PAYABLE';
                break;
            case 'COLLECTABLE': $processAss = 'COLLECTABLE';
                break;  
            case 'NOPAYABLE': $processAss = 'PAYABLE';
                break;
            case 'NOCOLLECTABLE': $processAss = 'COLLECTABLE';
                break;          
        endswitch;
        
        $sql = "SELECT DISTINCT sal.card_number_sal, fle.serial_fle, fle.diagnosis_fle, fle.cause_fle, dbf.name_dbf, cou.name_cou, prq.serial_prq, prq.type_prq,prq.amount_reclaimed_prq, prq.amount_authorized_prq, prq.status_prq,
                                DATE_FORMAT(prq.dispatched_date_prq,'%d/%m/%Y') as request_date, fle.incident_date_fle,
                                CONCAT(cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'')) as name_cus, spv.name_spv,        
                                IF(prq.dispatched_prq ='NO', 'PENDING', 'PAID') AS 'status', dbf.comment_dbf as comment_dbf, cou2.name_cou AS 'destination_cou', prq.request_date_prq, prq.dispatched_date_prq, datediff(prq.dispatched_date_prq,prq.request_date_prq) AS 'days'
                FROM payment_request prq
                JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                JOIN benefits_by_document bbd ON bbd.serial_dbf = dbf.serial_dbf AND bbd.serial_dbf = prq.serial_dbf
                JOIN service_provider_by_file spf ON spf.serial_spf = bbd.serial_spf AND prq.serial_fle = spf.serial_fle
                JOIN service_provider spv ON spf.serial_spv = spv.serial_spv
                JOIN file fle ON fle.serial_fle = prq.serial_fle JOIN city cit2 ON cit2.serial_cit = fle.serial_cit JOIN country cou2 ON cou2.serial_cou = cit2.serial_cou 
                JOIN sales sal ON sal.serial_sal = fle.serial_sal
                JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
                JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
                JOIN sector sec ON sec.serial_sec = dea.serial_sec
                JOIN city cit ON cit.serial_cit = sec.serial_cit
                JOIN country cou ON cou.serial_cou = cit.serial_cou
                JOIN customer cus ON cus.serial_cus = fle.serial_cus
                WHERE STR_TO_DATE(DATE_FORMAT(prq.dispatched_date_prq,'%d/%m/%Y'), '%d/%m/%Y') 
                        BETWEEN STR_TO_DATE('{$date_from}','%d/%m/%Y') 
                        AND DATE_ADD(STR_TO_DATE('{$date_end}','%d/%m/%Y'), INTERVAL 1 DAY)
                AND prq.status_prq ='APROVED'
				 AND prq.process_as IN ('$processAss', 'MIXED')
				 ";
        
        if ($processAs == 'PAYABLE' || $processAs == 'COLLECTABLE' ){
            $sql.= " AND prq.dispatched_prq ='YES'";
        }
        else{    
            $sql.= " AND prq.dispatched_prq ='NO'";
        }
        
        
        if ($document_to_dbf <> 'ALL') $sql.= "AND dbf.document_to_dbf = '$document_to_dbf'";
        
        //case for provider
        if($serial_spv != NULL) $sql.= " AND spf.serial_spv = $serial_spv";
        if($serial_cit != NULL) $sql.= " AND cit.serial_cit = '$serial_cit'";
        if($claims_serials != '') $sql.= " AND prq.serial_prq IN ($claims_serials)";

        $sql.= " ORDER BY name_cus, request_date";

        //die(Debug::print_r($sql));
        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }

	/***********************************************
    * funcion getClaimsInformationForReport
    * gets all the information
    ***********************************************/
    public static function getClaimsInformationForReport(   $db, $document_to_dbf, $date_from, $date_end, $processAs,
                                                            $serial_cou = NULL, $serial_spv = NULL, $serial_cit= NULL){
        switch ($processAs):
            case 'PAYABLE': $processAss = 'PAYABLE';
                break;
            case 'NOPAYABLE': $processAss = 'PAYABLE';
                break;
        endswitch;
        
        $sql = "SELECT DISTINCT prq.*, DATE_FORMAT(prq.dispatched_date_prq,'%d/%m/%Y') as request_date, dbf.*, 
                        CONCAT(cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'')) as name_cus,
                        cus.serial_cus, IFNULL(prq.type_prq, 'N/A') AS 'type_prq',cou.name_cou, fle.incident_date_fle
                FROM payment_request prq
                JOIN documents_by_file dbf ON dbf.serial_dbf = prq.serial_dbf
                JOIN benefits_by_document bbd ON bbd.serial_dbf = dbf.serial_dbf AND bbd.serial_dbf = prq.serial_dbf
                JOIN service_provider_by_file spf ON spf.serial_spf = bbd.serial_spf AND prq.serial_fle = spf.serial_fle
                JOIN file fle ON fle.serial_fle = prq.serial_fle
                JOIN sales sal ON sal.serial_sal = fle.serial_sal
                JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
                JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
                JOIN sector sec ON sec.serial_sec = dea.serial_sec
                JOIN city cit ON cit.serial_cit = sec.serial_cit
                JOIN country cou ON cou.serial_cou = cit.serial_cou 
                JOIN customer cus ON cus.serial_cus = fle.serial_cus
                WHERE STR_TO_DATE(DATE_FORMAT(prq.dispatched_date_prq,'%d/%m/%Y'), '%d/%m/%Y') 
                        BETWEEN STR_TO_DATE('{$date_from}','%d/%m/%Y') 
                        AND DATE_ADD(STR_TO_DATE('{$date_end}','%d/%m/%Y'), INTERVAL 1 DAY)
                AND prq.status_prq ='APROVED'
				AND prq.process_as IN ('$processAss', 'MIXED')
				";
                        
        if ($processAs == 'PAYABLE'){
            $sql.= " AND prq.dispatched_prq ='YES'";
        }else{    
            $sql.= " AND prq.dispatched_prq ='NO'";
        }
        
        if ($document_to_dbf <> 'ALL') $sql.= "AND dbf.document_to_dbf = '$document_to_dbf'";

        
        if($serial_cit) $sql.= " AND cit.serial_cit = $serial_cit";
        if($serial_spv) $sql.= " AND spf.serial_spv = $serial_spv";
        if($serial_cou) $sql.= " AND cou.serial_cou = $serial_cou";
        
        $sql.= " ORDER BY name_cus, request_date";
        //die(Debug::print_r($sql));

        $result=$db->getAll($sql);

        if($result){
            return $result;
        }else{
            return false;
        }
    }
	
	public static function getAffectedCardByAssistancesPayments($db, $serial_prq){
		$sql = "SELECT trl.card_number_trl
				FROM traveler_log trl
				JOIN file f ON f.serial_trl = trl.serial_trl
				JOIN payment_request prq ON prq.serial_fle = f.serial_fle
				WHERE trl.card_number_trl IS NOT NULL
				AND prq.serial_prq IN ($serial_prq)";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	
    /*
     * getPaymentRequestStates
     * @returns: an array of all the payment_request status
     */
    public static function getPaymentProcessAs($db){
        global $global_process_as;
        $sql = "SHOW COLUMNS FROM payment_request LIKE 'process_as'";
        $result = $db -> Execute($sql);

        $type=$result->fields[1];
        $type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));

        $types_list = array();
        foreach($type as $item){
            $types_list[$item] = $global_process_as[$item];
        }
        return $types_list;
    }

    //GETTERS
    function getSerial_prq(){
        return $this->serial_prq;
    }
    function getSerial_usr(){
        return  $this->serial_usr;
    }
    function getSerial_fle(){
        return $this->serial_fle;
    }
    function getSerial_dbf(){
        return $this->serial_dbf;
    }
    function getSerialMedicalUser(){
        return $this->usr_serial_usr;
    }
    function getAprovationUser(){
        return $this->usr_serial_usr2;
    }
    function getStatus_prq(){
        return $this->status_prq;
    }
    function getAmount_reclaimed_prq(){
        return $this->amount_reclaimed_prq;
    }
    function getAmount_authorized_prq(){
        return $this->amount_authorized_prq;
    }
    function getRequest_date_prq(){
        return $this->request_date_prq;
    }
    function getAudited_date_prq(){
        return $this->audited_date_prq;
    }
    function getDispatched_prq(){
        return $this->dispatched_prq;
    }
    function getAuditor_comments_prq(){
        return $this->auditor_comments;
    }
    function getObservations_prq(){
        return $this->observations_prq;
    }
    function getFile_url_prq(){
        return $this->file_url_prq;
    }
    function getDispatched_date_prq(){
        return $this->dispatched_date_prq;
    }
    function getAux_prq(){
        return $this->aux;
    }
	public function getType_prq() {
		return $this->type_prq;
	}
	 public function getProcessAS() {
        return $this->process_as;
    }
	
    //SETTERS
    function setSerial_prq($serial_prq){
        $this->serial_prq = $serial_prq;
    }
    function setSerial_usr($serial_usr){
        $this->serial_usr = $serial_usr;
    }
    function setSerial_fle($serial_fle){
        $this->serial_fle = $serial_fle;
    }
    function setSerial_dbf($serial_dbf){
        $this->serial_dbf = $serial_dbf;
    }
    function setSerialMedicalUser($usr_serial_usr){
        $this->usr_serial_usr = $usr_serial_usr;
    }
    function setAprovationUser($usr_serial_usr2){
        $this->usr_serial_usr2 = $usr_serial_usr2;
    }
    function setStatus_prq($status_prq){
        $this->status_prq = $status_prq;
    }
    function setAmount_reclaimed_prq($amount_reclaimed_prq){
        $this->amount_reclaimed_prq=$amount_reclaimed_prq;
    }
    function setAmount_authorized_prq($amount_authorized_prq){
        $this->amount_authorized_prq=$amount_authorized_prq;
    }
    function setRequest_date_prq($request_date_prq){
        $this->request_date_prq=$request_date_prq;
    }
    function setAudited_date_prq($audited_date_prq){
        $this->audited_date_prq=$audited_date_prq;
    }
    function setDispatched_prq($dispatched_prq){
        $this->dispatched_prq=$dispatched_prq;
    }
    function setAuditor_comments_prq($auditor_comments){
        $this->auditor_comments=$auditor_comments;
    }
    function setObservations_prq($observations_prq){
        $this->observations_prq=$observations_prq;
    }
    function setFile_url_prq($file_url_prq){
        $this->file_url_prq=$file_url_prq;
    }
    function setDispatched_date_prq($dispatched_date_prq){
        $this->dispatched_date_prq=$dispatched_date_prq;
    }
	public function setType_prq($type_prq) {
		$this->type_prq = $type_prq;
	}
	 public function setProcessAS($process_as) {
        $this->process_as = $process_as;
    }
}
?>