<?php
/*
File: Tax.class.php
Author: David Bergmann
Creation Date: 11/01/2010 08:17
Last Modified:
Modified By:
*/
class Tax {				
	var $db;
	var $serial_tax;
	var $name_tax;
	var $percentage_tax;
	var $serial_cou;
	var $status_tax;
	
	function __construct($db, $serial_tax = NULL, $name_tax = NULL, $percentage_tax = NULL,$serial_cou = NULL,$status_tax = NULL){
		$this -> db = $db;
		$this -> serial_tax = $serial_tax;
		$this -> name_tax = $name_tax;
		$this -> percentage_tax = $percentage_tax;
		$this -> status_tax = $status_tax;
	}
	
	/***********************************************
    * function getData
    * gets data by serial_tax
    ***********************************************/
	function getData(){
		if($this->serial_tax!=NULL){
			$sql = 	"SELECT serial_tax, name_tax, percentage_tax, serial_cou, status_tax
					FROM tax_by_country
					WHERE serial_tax='".$this->serial_tax."'";	
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_tax=$result->fields[0];
				$this->name_tax=$result->fields[1];	
				$this->percentage_tax=$result->fields[2];	
				$this->serial_cou=$result->fields[3];
				$this->status_tax=$result->fields[4];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}

	/***********************************************
    * function insert
    * inserts a new register in DB 
    ***********************************************/
	function insert(){
		$sql="INSERT INTO tax_by_country (
					serial_tax,
					name_tax,
					percentage_tax,
					serial_cou,
					status_tax
					)
			  VALUES(NULL,
					'".$this->name_tax."',
					'".$this->percentage_tax."',
					'".$this->serial_cou."',
					'".$this->status_tax."'	
					);";
					
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}

	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
	function update(){
		$sql="UPDATE tax_by_country
		SET name_tax='".$this->name_tax."',
			percentage_tax='".$this->percentage_tax."',
			status_tax='".$this->status_tax."'
		WHERE serial_tax='".$this->serial_tax."'";

		$result = $this->db->Execute($sql);
		//echo $sql." ";
		if ($result==true) 
			return true;
		else
			return false;
	}
	
	/***********************************************
    * funcion getTaxes
    * gets all the Taxes of the system
    ***********************************************/
	function getTaxes($serial_cou=NULL){
		$sql = 	"SELECT serial_tax, name_tax, percentage_tax, serial_cou, status_tax
				 FROM tax_by_country
                         WHERE status_tax = 'ACTIVE'";
		if($serial_cou!=NULL){
			$sql.=" AND serial_cou = '".$serial_cou."'";
		}
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}

        
	
	/** 
	@Name: getStatusList
	@Description: Gets all credit days status in DB.
	@Params: N/A
	@Returns: Status array
	**/
	function getStatusList(){
		$sql = "SHOW COLUMNS FROM tax_by_country LIKE 'status_tax'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	/** 
	@Name: getTaxesAutocompleter
	@Description: Returns an array of taxes.
	@Params: Tax name or pattern.
	@Returns: Tax array
	**/
	function getTaxesAutocompleter($name_tax){
		$sql = "SELECT t.serial_tax, t.name_tax, t.percentage_tax, co.name_cou
				FROM tax_by_country t
				JOIN country co ON co.serial_cou=t.serial_cou
				WHERE LOWER(t.name_tax) LIKE _utf8'%".utf8_encode($name_tax)."%' collate utf8_bin
				LIMIT 10";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
	/***********************************************
    * funcion taxExists
    * verifies if a City exists or not
    ***********************************************/
	function taxExists($name_tax, $serial_tax=NULL,$serial_cou){
		$sql = 	"SELECT t.serial_tax
				 FROM tax_by_country t
				 WHERE LOWER(t.name_tax)= _utf8'".utf8_encode($name_tax)."' collate utf8_bin
				 AND t.serial_cou = ".$serial_cou;
		if($serial_tax!=NULL){
			$sql.=" AND serial_tax <> '".$serial_tax."'";
		}
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}

	///GETTERS
	function getSerial_tax(){
		return $this->serial_tax;
	}
	function getName_tax(){
		return $this->name_tax;
	}
	function getPercentage_tax(){
		return $this->percentage_tax;
	}
	function getSerial_cou(){
		return $this->serial_cou;
	}
	function getStatus_tax(){
		return $this->status_tax;
	}

	///SETTERS	
	function setSerial_tax($serial_tax){
		$this->serial_tax=$serial_tax;
	}
	function setName_tax($name_tax){
		$this->name_tax=$name_tax;
	}
	function setPercentage_tax($percentage_tax){
		$this->percentage_tax=$percentage_tax;
	}
	function setSerial_cou($serial_cou){
		$this->serial_cou=$serial_cou;
	}
	function setStatus_tax($status_tax){
		$this->status_tax=$status_tax;
	}
	
}
?>