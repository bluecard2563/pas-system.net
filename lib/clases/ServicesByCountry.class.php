<?php
/*
File:ServicesByCountry.class.php
Author: Miguel Ponce
Creation Date: 09/03/2010
*/
class ServicesByCountry{
    var $db;
    var $serial_sbc;
    var $serial_cou;
    var $serial_ser;
    var $status_sbc;
    var $price_sbc;
    var $cost_sbc;
    var $coverage_sbc;

    function    __construct($db, $serial_sbc=NULL, $serial_cou=NULL, $serial_ser = NULL, $status_sbc = NULL, $price_sbc = NULL, $cost_sbc = NULL, $coverage_sbc = NULL ){
            $this -> db = $db;
            $this -> serial_sbc = $serial_sbc;
            $this -> serial_cou = $serial_cou;
            $this -> serial_ser = $serial_ser;
            $this -> status_sbc = $status_sbc;
            $this -> price_sbc = $price_sbc;
            $this -> cost_sbc = $cost_sbc;
            $this -> coverage_sbc = $coverage_sbc;
    }

    /***********************************************
    * function insert
    * inserts a new register en DB
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO services_by_country(serial_sbc,serial_cou,serial_ser,status_sbc,price_sbc,cost_sbc,coverage_sbc)
            VALUES(NULL,
                '".$this->serial_cou."',
                '".$this->serial_ser."',
                '".$this->status_sbc."',
                '".$this->price_sbc."',
                '".$this->cost_sbc."',
                '".$this->coverage_sbc."')";
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

        function update() {
        $sql = "
            UPDATE services_by_country
            set serial_cou='".$this->serial_cou."',
            serial_ser='".$this->serial_ser."',
            status_sbc='".$this->status_sbc."',
            price_sbc='".$this->price_sbc."',
            cost_sbc='".$this->cost_sbc."',
            coverage_sbc='".$this->coverage_sbc."'    
            WHERE serial_sbc = '".$this->serial_sbc."'";
        $result = $this->db->Execute($sql);
        if ($result === false)return false;
        if($result==true)
            return true;
        else
            return false;
    }

        /***********************************************
    * function getData
    * sets data by serial_ser
    ***********************************************/
    function getDataBySerial_ser(){
        if($this->serial_ser!=NULL){
            $sql = "SELECT  serial_sbc,
                            serial_cou,
                            serial_ser,
                            status_sbc,
                            price_sbc,
                            cost_sbc,
                            coverage_sbc
                    FROM services_by_country
                    WHERE serial_ser='".$this->serial_ser."' and serial_cou='".$this->serial_cou."'";
            $result = $this->db->Execute($sql);
            if ($result === false) return false;

            if($result->fields[0]){
                $this -> serial_sbc = $result->fields[0];
                $this -> serial_cou = $result->fields[1];
                $this -> serial_ser = $result->fields[2];
                $this -> status_sbc = $result->fields[3];
                $this -> price_sbc = $result->fields[4];
                $this -> cost_sbc = $result->fields[5];
                $this -> coverage_sbc = $result->fields[6];
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /***********************************************
    * funcion getAssignedCountries
    * gets all the Countries assigened to a service
    ***********************************************/
    function getAssignedCountries(){
        
        $sql = 	"SELECT sbc.serial_sbc,
                        sbc.serial_ser,
                        cou.serial_zon,
                        sbc.serial_cou,
                        sbc.status_sbc,
                        sbc.price_sbc,
                        sbc.cost_sbc,
                        sbc.coverage_sbc,
                        GROUP_CONCAT(  DISTINCT CAST(tbs.serial_tax AS CHAR) ORDER BY tbs.serial_tax ASC SEPARATOR ',' ) assigned_taxes,
                        GROUP_CONCAT(  DISTINCT CAST(sbd.serial_sbd AS CHAR) ORDER BY sbd.serial_sbd ASC SEPARATOR ',' ) assigned_dealers
                FROM services_by_country sbc
                LEFT JOIN taxes_by_service tbs ON sbc.serial_sbc = tbs.serial_sbc
                LEFT JOIN services_by_dealer sbd ON sbd.serial_sbc = sbc.serial_sbc
                AND sbd.status_sbd = 'ACTIVE'
                JOIN country cou ON cou.serial_cou = sbc.serial_cou
                WHERE serial_ser = '".$this->serial_ser."'
                AND sbc.serial_cou <> '1'
                GROUP BY sbc.serial_cou
                ORDER BY cou.serial_zon";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arreglo=array();
            $cont=0;

            do{
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arreglo;
    }

     function serviceByCountryExists(){
        if($this->serial_ser != NULL && $this->serial_cou != NULL){
            $sql = "SELECT sbc.serial_sbc
                    FROM services_by_country sbc
                    WHERE sbc.serial_ser = '".$this->serial_ser."'
                    AND sbc.serial_cou = '".$this->serial_cou."'";
            //echo $sql;
            $result = $this->db->Execute($sql);

            if ($result === false)return false;

            if($result->fields[0]){
                return $result->fields[0];
            }else{
                return 0;
            }
        }else{
            return false;
        }
    }



//SETTERS
    function setSerial_sbc($serial_sbc){
            $this->serial_sbc = $serial_sbc;
    }
    function setSerial_cou($serial_cou){
            $this->serial_cou = $serial_cou;
    }
    function setSerial_ser($serial_ser){
            $this->serial_ser = $serial_ser;
    }
    function setStatus_sbc($status_sbc){
            $this->status_sbc = $status_sbc;
    }
    function setPrice_sbc($price_sbc){
            $this->price_sbc = $price_sbc;
    }
    function setCost_sbc($cost_sbc){
            $this->cost_sbc = $cost_sbc;
    }
    function setCoverage_sbc($coverage_sbc){
            $this->coverage_sbc = $coverage_sbc;
    }
//GETTERS
    function getSerial_sbc(){
            return $this->serial_sbc;
    }
    function getSerial_cou(){
            return $this->serial_cou;
    }
    function getSerial_ser(){
            return $this->serial_ser;
    }
    function getStatus_sbc(){
            return $this->status_sbc;
    }
    function getPrice_sbc(){
            return $this->price_sbc;
    }
    function getCost_sbc(){
            return $this->cost_sbc;
    }
    function getCoverage_sbc(){
            return $this->coverage_sbc;
    }

}
?>
