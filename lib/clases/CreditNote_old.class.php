<?php
/*
File: CreditNote.class.php
Author: Santiago Benitez
Creation Date: 18/02/2010
Last Modified:
Modified By:
*/

class CreditNote {
    var $db;
    var $serial_cn;
    var $serial_inv;
	var $serial_sal;
	var $serial_pay;
    var $serial_ref;
    var $serial_dbm;
    var $number_cn;
    var $amount_cn;
    var $status_cn;
    var $date_cn;
    var $printed_cn;
    var $payment_status_cn;
	var $penalty_fee_cn;

    function __construct($db, $serial_cn = NULL, $serial_dbm=NULL, $serial_inv=NULL, $serial_ref = NULL, $number_cn = NULL, $amount_cn = NULL, $status_cn = NULL,
						 $date_cn = NULL, $printed_cn=NULL, $payment_status_cn=NULL, $serial_sal=NULL, $penalty_fee_cn=NULL) {
        $this -> db = $db;
        $this -> serial_cn = $serial_cn;
		$this -> serial_dbm = $serial_dbm;
        $this -> serial_inv = $serial_inv;
		$this -> serial_sal = $serial_sal;
        $this -> serial_ref = $serial_ref;
        $this -> number_cn = $number_cn;
        $this -> amount_cn = $amount_cn;
        $this -> status_cn = $status_cn;
        $this -> date_cn = $date_cn;
        $this -> printed_cn = $printed_cn;
        $this -> payment_status_cn = $payment_status_cn;
		$this -> penalty_fee_cn = $penalty_fee_cn;
    }

    /***********************************************
	* function getData
	* gets data by serial_cn
	***********************************************/
    function getData() {
        if($this->serial_cn!=NULL) {
            $sql = 	"SELECT serial_cn, serial_inv, serial_ref, number_cn, amount_cn, status_cn,
							DATE_FORMAT(date_cn,'%d/%m/%Y'), printed_cn, payment_status_cn, serial_sal, penalty_fee_cn,
							serial_dbm, serial_pay
					FROM credit_note
					WHERE serial_cn='".$this->serial_cn."'";

            $result = $this->db->Execute($sql);
            if ($result === false) return false;
            
            if($result->fields[0]) {
                $this->serial_cn=$result->fields[0];
                $this->serial_inv=$result->fields[1];
                $this->serial_ref=$result->fields[2];
                $this->number_cn=$result->fields[3];
                $this->amount_cn=$result->fields[4];
                $this->status_cn=$result->fields[5];
                $this->date_cn=$result->fields[6];
                $this->printed_cn=$result->fields[7];
                $this->payment_status_cn=$result->fields[8];
				$this->serial_sal=$result->fields[9];
				$this->penalty_fee_cn=$result->fields[10];
				$this->serial_dbm=$result->fields[11];
				$this->serial_pay=$result->fields[12];
                return true;
            }
        }
        return false;
    }

    /***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
    function insert() {
        $sql="INSERT INTO credit_note (
					serial_cn,
					serial_dbm,
                    serial_inv,
					serial_sal,
					serial_pay,
					serial_ref,
					number_cn,
					amount_cn,
					status_cn,
					date_cn,
					printed_cn,
                    payment_status_cn,
					penalty_fee_cn
					)
			  VALUES(NULL,
					 '".$this->serial_dbm."',";
        if($this->serial_inv != '') {
            $sql.="'".$this->serial_inv."',";
        }else {
            $sql.="NULL,";
        }

		if($this->serial_sal != '') {
            $sql.="'".$this->serial_sal."',";
        }else {
            $sql.="NULL,";
        }
		
		if($this->serial_pay != '') {
            $sql.="'".$this->serial_pay."',";
        }else {
            $sql.="NULL,";
        }
		
        if($this->serial_ref != '') {
            $sql.="'".$this->serial_ref."',";
        }else {
            $sql.="NULL,";
        }
             $sql.="'".$this->number_cn."',
					'".$this->amount_cn."',
					'".$this->status_cn."',
					NOW(),
					'NO',
        '".$this->payment_status_cn."',";

		if($this->penalty_fee_cn != '') {
            $sql.="'".$this->penalty_fee_cn."');";
        }else {
            $sql.="NULL);";
        }
		
        //die(Debug::print_r($sql));

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

    /***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
    function update() {
        $sql="UPDATE credit_note
		SET serial_dbm='".$this->serial_dbm."',
			serial_inv=";
        if($this->serial_inv != '') {
            $sql.="'".$this->serial_inv."',";
        }else {
            $sql.="NULL,";
        }

		$sql.=" serial_sal=";
        if($this->serial_sal != '') {
            $sql.="'".$this->serial_sal."',";
        }else {
            $sql.="NULL,";
        }
		
		$sql.=" serial_pay=";
        if($this->serial_pay != '') {
            $sql.="'".$this->serial_pay."',";
        }else {
            $sql.="NULL,";
        }
		
		$sql.=" serial_ref=";
        if($this->serial_ref != '') {
            $sql.="'".$this->serial_ref."',";
        }else {
            $sql.="NULL,";
        }
		
        $sql.=" number_cn='".$this->number_cn."',
			amount_cn='".$this->amount_cn."',
			status_cn='".$this->status_cn."',
			date_cn=STR_TO_DATE('".$this->date_cn."','%d/%m/%Y'),
			printed_cn='".$this->printed_cn."',
            payment_status_cn='".$this->payment_status_cn."',";

		$sql.=" penalty_fee_cn=";
        if($this->penalty_fee_cn != '') {
            $sql.="'".$this->penalty_fee_cn."'";
        }else {
            $sql.="NULL";
        }

        $sql.=" WHERE serial_cn='".$this->serial_cn."'";
        //die($sql);
        $result = $this->db->Execute($sql);
        if ($result==true)
            return true;
        else
            return false;
    }

    /***********************************************
        * funcion updatePayment_status_cn
        * updates a register in DB
        ***********************************************/
    function updatePayment_status_cn() {
        $sql="UPDATE credit_note
				SET payment_status_cn='".$this->payment_status_cn."'
                WHERE serial_cn='".$this->serial_cn."'";

        $result = $this->db->Execute($sql);
        if ($result==true)
            return true;
        else
            return false;
    }

    /***********************************************
    * funcion getCreditNotesForPayment
    * Description: Returns a credit note array
    * Returns: true if the customer or dealer has a payment with excess
      *        false if not
    ***********************************************/
    function getCreditNotesForPayment($serial_dea=null,$serial_cus=null){
            $sql = "SELECT cn.serial_cn, cn.serial_dbm, cn.serial_inv, cn.serial_ref, cn.number_cn, cn.amount_cn, cn.serial_sal
                     FROM credit_note cn
                     JOIN refund r ON r.serial_ref=cn.serial_ref AND cn.serial_ref IS NOT NULL AND cn.status_cn='ACTIVE' AND cn.payment_status_cn='PENDING'
                     JOIN sales sal ON sal.serial_sal=r.serial_sal
                     JOIN invoice i ON i.serial_inv=sal.serial_inv ";
            if($serial_cus){
                $sql.="JOIN customer c ON c.serial_cus = i.serial_cus
                    WHERE c.serial_cus = ".$serial_cus;
            }else if($serial_dea){
                $sql.="JOIN dealer d ON d.serial_dea = i.serial_dea
                       WHERE d.serial_dea = ".$serial_dea;
            }
            else{
                return false;
            }
	            $result = $this->db -> Execute($sql);
            $num = $result -> RecordCount();
            if($num > 0){
                $arreglo=array();
                $cont=0;

                do{
                    $arreglo[$cont]=$result->GetRowAssoc(false);
                    $result->MoveNext();
                    $cont++;
                }while($cont<$num);
            }

            return $arreglo;
    }

    /***********************************************
        * funcion getCreditNote
        * gets all the CreditNotes of the system
        ***********************************************
    function getCreditNote() {
        $sql = 	"SELECT serial_cn, serial_inv, serial_ref, number_cn, amount_cn, status_cn, date_cn, banner_cou, phone_cou, printed_cn, payment_status_cn
				 FROM credit_note
				 ORDER BY amount_cn";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0) {
            $arreglo=array();
            $cont=0;

            do {
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }*/
    
    /*******************************************************************
        * funcion getCreditNotesByOwner
        * gets all the CreditNotes of the system from an specific owner
        ***************************************************************/
    function getCreditNotesByOwner($serial_dea,$serial_cus) {
        $sql = 	"SELECT cn.serial_cn, cn.serial_dbm, cn.serial_inv, cn.serial_ref, cn.number_cn, cn.amount_cn, DATE_FORMAT(cn.date_cn,'%d/%m/%Y'), dea.name_dea, CONCAT(CONCAT(cus.first_name_cus,' '),IFNULL(cus.last_name_cus,''))as name_cus
				 FROM credit_note cn
				 JOIN refund r ON r.serial_ref=cn.serial_ref AND cn.serial_ref IS NOT NULL AND cn.status_cn='ACTIVE' AND cn.payment_status_cn='PENDING'
				 JOIN sales sal ON sal.serial_sal=r.serial_sal
				 JOIN invoice inv ON inv.serial_inv=sal.serial_inv
				 LEFT JOIN customer cus ON cus.serial_cus=inv.serial_cus AND inv.serial_cus IS NOT NULL AND cus.serial_cus='".$serial_cus."'
				 LEFT JOIN dealer dea ON dea.serial_dea=inv.serial_dea AND inv.serial_dea IS NOT NULL AND dea.serial_dea='".$serial_dea."'
				 ORDER BY cn.date_cn";
        //die($sql);
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0) {
            $arreglo=array();
            $cont=0;

            do {
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }
    

	/******************************************************************
        * funcion getCreditNotesByParameters
        * gets all the CreditNotes of the system from an specific owner
        ***************************************************************/
    function getCreditNotesByParameters($serialCus = NULL, $serialInv = NULL, $dateFrom = NULL, $dateTo = NULL, $selManager = NULL,$selCity = NULL,$selDealer = NULL,$selType = NULL, $countryID = NULL) {
    	
    	//echo $serialCus." - i ".$serialInv." - f ".$dateFrom." - t ".$dateTo." - m ".$selManager." - c ".$selCity." - d ".$selDealer." - t ".$selType;
    	
    	if($serialCus){
			$appliedFilters = "	AND cus.serial_cus = ".$serialCus;
		}
    	if($serialInv){
			$appliedFilters = "	AND inv.serial_inv = ".$serialInv;
		}
    	if($selManager){
			$appliedFilters = "	AND dea2.serial_mbc = ".$selManager;
		}
    	if($selCity){
			$appliedFilters = "	AND sec.serial_cit = ".$selCity;
		}
    	if($selDealer){
			$appliedFilters = "	AND dea2.serial_dea = ".$selDealer;
		}
		if($countryID){
			$appliedFilters = " AND mbc.serial_cou = $countryID";
		}
    	if($selType){
	    	switch($selType){
	    		case 1: //Reembolso
					$appliedFilters .= " AND cn.serial_ref IS NOT NULL";
	    			break;
	    		case 2: //Anulacion de Factura:
	    			$appliedFilters .= " AND cn.serial_inv IS NOT NULL";
	    			break;
	    		case 3: //Penalidad:
	    			$appliedFilters .= " AND cn.serial_sal IS NOT NULL";
					break;
				case 4: //Incobrable
	    			$appliedFilters .= " AND cn.serial_pay IS NOT NULL";
	    			break;
	    	}
		}
    	if($dateFrom && $dateTo){
    		$appliedFilters .= " AND STR_TO_DATE(DATE_FORMAT(cn.date_cn , '%d/%m/%Y') ,'%d/%m/%Y')
    							 BETWEEN STR_TO_DATE('".$dateFrom."','%d/%m/%Y') AND STR_TO_DATE('".$dateTo."','%d/%m/%Y')";
    	}
		//die($appliedFilters);
        $sql .= "	SELECT 	DISTINCT cn.serial_cn, 
							cn.serial_dbm, 
							DATE_FORMAT(cn.date_cn,'%d/%m/%Y') date_cn, 
							cn.amount_cn,
							'Anulacion' as 'concept',
							inv.number_inv, inv.serial_inv,  inv.serial_cus,
							DATE_FORMAT(inv.date_inv,'%d/%m/%Y') date_inv, 
							inv.total_inv,
							IF(cn.serial_pay IS NULL, inv.total_inv - cn.amount_cn, 0) as saldo, 
							cn.number_cn,
							IF(inv.serial_dea IS NULL, CONCAT(IFNULL(cus.first_name_cus, ''), ' ', IFNULL(cus.last_name_cus, '')), holder.name_dea) AS 'cus_name',
							observation_inl AS 'observations', 
							mbc.serial_mbc, m.name_man
					FROM credit_note cn
					JOIN invoice inv ON inv.serial_inv = cn.serial_inv
					JOIN invoice_log il ON il.serial_inv = inv.serial_inv
					JOIN sales s ON s.serial_sal = il.serial_sal

					JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
					JOIN dealer b ON b.serial_dea = cnt.serial_dea
					JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc
					JOIN manager m ON m.serial_man = mbc.serial_man
					JOIN dealer dea2 ON dea2.serial_dea = b.dea_serial_dea
					JOIN sector sec ON sec.serial_sec = b.serial_sec

					LEFT JOIN customer cus ON cus.serial_cus = inv.serial_cus
					LEFT JOIN dealer holder ON holder.serial_dea = inv.serial_dea
					
					WHERE cn.serial_inv IS NOT NULL
					$appliedFilters
					GROUP BY cn.serial_cn
					
				UNION
				
				SELECT 	DISTINCT cn.serial_cn, 
						cn.serial_dbm, 
						DATE_FORMAT(cn.date_cn,'%d/%m/%Y') date_cn, 
						cn.amount_cn,
						'Penalidad' as 'concept',
						inv.number_inv, inv.serial_inv,  inv.serial_cus,
						DATE_FORMAT(inv.date_inv,'%d/%m/%Y') date_inv, 
						inv.total_inv,
						IF(cn.serial_pay IS NULL, inv.total_inv - cn.amount_cn, 0) as saldo, 
						cn.number_cn,
						IF(inv.serial_dea IS NULL, CONCAT(IFNULL(cus.first_name_cus, ''), ' ', IFNULL(cus.last_name_cus, '')), holder.name_dea) AS 'cus_name',
						'Penalidad' AS 'observations', 
						mbc.serial_mbc, m.name_man
				FROM credit_note cn
				JOIN sales s ON s.serial_sal = cn.serial_sal
				JOIN invoice inv ON inv.serial_inv = s.serial_inv
				JOIN sales_log sl ON sl.serial_sal = s.serial_sal

				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer b ON b.serial_dea = cnt.serial_dea
				JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc
				JOIN manager m ON m.serial_man = mbc.serial_man
				JOIN dealer dea2 ON dea2.serial_dea = b.dea_serial_dea
				JOIN sector sec ON sec.serial_sec = b.serial_sec

				LEFT JOIN customer cus ON cus.serial_cus = inv.serial_cus
				LEFT JOIN dealer holder ON holder.serial_dea = inv.serial_dea

				WHERE cn.serial_sal IS NOT NULL
				".$appliedFilters."
					
				UNION
				
				SELECT 	DISTINCT cn.serial_cn, 
						cn.serial_dbm, 
						DATE_FORMAT(cn.date_cn,'%d/%m/%Y') date_cn, 
						cn.amount_cn,
						'Reembolso' as 'concept',
						inv.number_inv, inv.serial_inv,  inv.serial_cus,
						DATE_FORMAT(inv.date_inv,'%d/%m/%Y') date_inv, 
						inv.total_inv,
						IF(cn.serial_pay IS NULL, inv.total_inv - cn.amount_cn, 0) as saldo, 
						cn.number_cn,
						IF(inv.serial_dea IS NULL, CONCAT(IFNULL(cus.first_name_cus, ''), ' ', IFNULL(cus.last_name_cus, '')), holder.name_dea) AS 'cus_name',
						r.comments_ref AS 'observations', 
						mbc.serial_mbc, m.name_man
				FROM credit_note cn
				JOIN refund r ON r.serial_ref = cn.serial_ref
				JOIN sales s ON s.serial_sal = r.serial_sal

				JOIN invoice inv ON inv.serial_inv = s.serial_inv

				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer b ON b.serial_dea = cnt.serial_dea
				JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc
				JOIN manager m ON m.serial_man = mbc.serial_man
				JOIN dealer dea2 ON dea2.serial_dea = b.dea_serial_dea
				JOIN sector sec ON sec.serial_sec = b.serial_sec

				LEFT JOIN customer cus ON cus.serial_cus = inv.serial_cus
				LEFT JOIN dealer holder ON holder.serial_dea = inv.serial_dea

				WHERE cn.serial_ref IS NOT NULL
				".$appliedFilters."
				GROUP BY cn.serial_cn
					
				UNION
				
				SELECT 	DISTINCT cn.serial_cn, 
						cn.serial_dbm, 
						DATE_FORMAT(cn.date_cn,'%d/%m/%Y') date_cn, 
						cn.amount_cn,
						'Incobrable' as 'concept',
						inv.number_inv, inv.serial_inv,  inv.serial_cus,
						DATE_FORMAT(inv.date_inv,'%d/%m/%Y') date_inv, 
						inv.total_inv,
						IF(cn.serial_pay IS NULL, inv.total_inv - cn.amount_cn, 0) as saldo, 
						cn.number_cn,
						IF(inv.serial_dea IS NULL, CONCAT(IFNULL(cus.first_name_cus, ''), ' ', IFNULL(cus.last_name_cus, '')), holder.name_dea) AS 'cus_name',
						'Incobrable' AS 'observations', 
						mbc.serial_mbc, m.name_man
				FROM credit_note cn
				JOIN invoice inv ON inv.serial_pay = cn.serial_pay
				JOIN sales s ON s.serial_inv = inv.serial_inv

				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer b ON b.serial_dea = cnt.serial_dea
				JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc
				JOIN manager m ON m.serial_man = mbc.serial_man
				JOIN dealer dea2 ON dea2.serial_dea = b.dea_serial_dea
				JOIN sector sec ON sec.serial_sec = b.serial_sec

				LEFT JOIN customer cus ON cus.serial_cus = inv.serial_cus
				LEFT JOIN dealer holder ON holder.serial_dea = inv.serial_dea

				WHERE cn.serial_pay IS NOT NULL
					".$appliedFilters."
				GROUP BY cn.serial_cn
				
				ORDER BY serial_cn";
    	
        //Debug::print_r($sql); die;
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0) {
            $arreglo=array();
            $cont=0;
            do {
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }
        return $arreglo;
    }
    
    /***********************************************
        * funcion getAllCreditNote
        * gets all the CreditNotes of the system
        ***********************************************/
    function getAllCreditNote() {
        $sql = 	"SELECT serial_cn, cn.serial_dbm, serial_inv, serial_ref, number_cn, amount_cn, status_cn, date_cn, banner_cou, phone_cou, printed_cn, payment_status_cn
				 FROM credit_note";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0) {
            $arreglo=array();
            $cont=0;

            do {
                $arreglo[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arreglo;
    }

	/*
	 * @Name: getCreditNoteRegister
	 * @Description: Get the info of a credit note according to it's number, type and manager by country.
	 * @Params: $type, $Manager_by_Country ID
	 * @Returns: TRUE if OK / FALSE otherwise.
	 */
    function getCreditNoteRegister($serial_dbm, $type) {
        $sql = 	"SELECT DISTINCT cn.serial_cn
                 FROM credit_note cn";

		if($type == 'INVOICE'){
			$sql.=" JOIN invoice i ON i.serial_inv=cn.serial_inv";
		}else if($type=='REFUND'){
			$sql.=" JOIN refund r ON r.serial_ref = cn.serial_ref
				    JOIN sales s ON s.serial_sal = r.serial_sal";
		}else if($type=='PENALTY'){
			$sql.=" JOIN sales s ON s.serial_sal = cn.serial_sal";
		}else{
			$sql.=" JOIN payments p ON p.serial_pay = cn.serial_pay";
		}
		
		$sql.="	WHERE cn.number_cn = '".$this->number_cn."'
				AND cn.status_cn = 'ACTIVE'
				AND cn.serial_dbm = '".$serial_dbm."'";

        $result = $this->db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
    }

	public static function getCreditNoteNumber($db, $serial_cn){
		$sql="SELECT number_cn
			  FROM credit_note
			  WHERE serial_cn=".$serial_cn;

		$result=$db->getOne($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	public static function getCreditNotesByNumberAndManagerByCountry($db, $serial_mbc, $number_cn, $type_dbm){
		$sql="SELECT cn.serial_cn, IFNULL(i.number_inv, IFNULL(i2.number_inv, i3.number_inv)) AS 'number_inv',
					 IFNULL(i.serial_inv, IFNULL(i2.serial_inv, i3.serial_inv)) AS 'serial_inv', cn.number_cn
			  FROM credit_note cn
			  JOIN document_by_manager dbm ON dbm.serial_dbm = cn.serial_dbm
					AND dbm.purpose_dbm='CREDIT_NOTE'
					AND (dbm.type_dbm='BOTH' OR dbm.type_dbm='$type_dbm')

			  LEFT JOIN sales s ON s.serial_sal=cn.serial_sal
			  LEFT JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
			  LEFT JOIN dealer b ON b.serial_dea=cnt.serial_dea AND b.serial_mbc='$serial_mbc'
			  LEFT JOIN invoice i ON i.serial_inv=s.serial_inv

			  LEFT JOIN invoice i2 ON i2.serial_inv=cn.serial_inv
			  LEFT JOIN sales s2 ON s2.serial_inv=i2.serial_inv
			  LEFT JOIN counter cnt2 ON cnt2.serial_cnt=s2.serial_cnt
			  LEFT JOIN dealer b2 ON b2.serial_dea=cnt2.serial_dea AND b2.serial_mbc='$serial_mbc'

			  LEFT JOIN refund r ON r.serial_ref=cn.serial_ref
			  LEFT JOIN sales s3 ON s3.serial_sal=r.serial_sal
			  LEFT JOIN counter cnt3 ON cnt3.serial_cnt=s3.serial_cnt
			  LEFT JOIN dealer b3 ON b3.serial_dea=cnt3.serial_dea AND b3.serial_mbc='$serial_mbc'
			  LEFT JOIN invoice i3 ON i3.serial_inv=s3.serial_inv
			  
			  WHERE cn.number_cn=".$number_cn;
			  //echo '<pre>'.$sql.'</pre>';

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	public static function hasPenaltyCreditNote($db, $serial_sal){
		$sql = "SELECT serial_cn
				FROM credit_note
				WHERE serial_sal = $serial_sal
				AND status_cn = 'ACTIVE'";
		
		//Debug::print_r($sql);
		
		$result = $db -> getOne($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	///GETTERS
    function getSerial_cn() {
        return $this->serial_cn;
    }
	function getSerial_dbm() {
        return $this->serial_dbm;
    }
    function getSerial_inv() {
        return $this->serial_inv;
    }
	function getSerial_sal() {
        return $this->serial_sal;
    }
    function getSerial_ref() {
        return $this->serial_ref;
    }
    function getNumber_cn() {
        return $this->number_cn;
    }
    function getAmount_cn() {
        return $this->amount_cn;
    }
    function getStatus_cn() {
        return $this->status_cn;
    }
    function getDate_cn() {
        return $this->date_cn;
    }
    function getPrinted_cn(){
        return $this->printed_cn;
    }
    function getPayment_status_cn(){
        return $this->payment_status_cn;
    }
	function getPenalty_fee_cn(){
        return $this->penalty_fee_cn;
    }
	public function getSerial_pay() {
		return $this->serial_pay;
	}

    ///SETTERS
    function setSerial_cn($serial_cn) {
        $this->serial_cn = $serial_cn;
    }
	function setSerial_dbm($serial_dbm) {
        $this->serial_dbm = $serial_dbm;
    }
    function setSerial_inv($serial_inv) {
        $this->serial_inv = $serial_inv;
    }
	function setSerial_sal($serial_sal) {
        $this->serial_sal = $serial_sal;
    }
    function setSerial_ref($serial_ref) {
        $this->serial_ref = $serial_ref;
    }
    function setNumber_cn($number_cn) {
        $this->number_cn = $number_cn;
    }
    function setAmount_cn($amount_cn) {
        $this->amount_cn = $amount_cn;
    }
    function setStatus_cn($status_cn) {
        $this->status_cn = $status_cn;
    }
    function setDate_cn($date_cn) {
        $this->date_cn = $date_cn;
    }
    function setPrinted_cn($printed_cn){
            $this->printed_cn=$printed_cn;
    }
    function setPayment_status_cn($payment_status_cn){
            $this->payment_status_cn=$payment_status_cn;
    }
	function setPenalty_fee_cn($penalty_fee_cn){
		$this->penalty_fee_cn=$penalty_fee_cn;
    }
	public function setSerial_pay($serial_pay) {
		$this->serial_pay = $serial_pay;
	}
}
?>
