<?php
/**
 * Created by PhpStorm.
 * User: leninpadilla
 * Date: 25/7/19
 * Time: 12:02
 */

class KioskPayment
{
    var $db;
    var $idPago;
    var $codigoRespuesta;
    var $lote;
    var $referencia;
    var $autorizacion;
    var $codigoAdquirente;
    var $nombreAdquiriente;
    var $tarjetaHabiente;
    var $numeroTarjeta;
    var $montoFijo;
    var $interes;
    var $AID;
    var $ARQC;
    var $aplicacionEMV;
    var $codigoRespuestaAut;
    var $criptograma;
    var $fecha;
    var $fechaVencimiento;
    var $hora;
    var $MID;
    var $modoLectura;
    var $numerTarjetaEncriptado;
    var $PIN;
    var $publicidad;
    var $TID;
    var $TSI;
    var $TVR;
    var $tipoMensaje;
    var $mensajeRespuestaAut;
    var $monto;
    var $IVA;
    var $propina;
    var $servicio;
    var $ICE;
    var $otrosImp;
    var $base0;
    var $base12;
    var $cashOver;
    var $trama;

    function __construct($db, $idPago = NULL, $codigoRespuesta = NULL, $lote = NULL, $referencia = NULL, $autorizacion = NULL, $codigoAdquirente = NULL, $nombreAdquiriente = NULL, $tarjetaHabiente = NULL,
                        $numeroTarjeta = NULL, $montoFijo = NULL, $interes = NULL, $AID = NULL, $ARQC = NULL, $aplicacionEMV = NULL, $codigoRespuestaAut = NULL, $criptograma = NULL, $fecha = NULL, $fechaVencimiento = NULL,
                        $hora = NULL, $MID = NULL, $modoLectura = NULL, $numerTarjetaEncriptado = NULL, $PIN = NULL, $publicidad = NULL, $TID = NULL, $TSI = NULL, $TVR = NULL, $tipoMensaje = NULL, $mensajeRespuestaAut = NULL,
                        $monto = NULL, $IVA = NULL, $propina = NULL, $servicio = NULL, $ICE = NULL, $otrosImp = NULL, $base0 = NULL, $base12 = NULL, $cashOver = NULL, $trama = NULL)
    {
        $this->db = $db;
        $this->idPago = $idPago;
        $this->codigoRespuesta = $codigoRespuesta;
        $this->lote = $lote;
        $this->referencia = $referencia;
        $this->autorizacion = $autorizacion;
        $this->codigoAdquirente = $codigoAdquirente;
        $this->nombreAdquiriente = $nombreAdquiriente;
        $this->tarjetaHabiente = $tarjetaHabiente;
        $this->numeroTarjeta = $numeroTarjeta;
        $this->montoFijo = $montoFijo;
        $this->interes = $interes;
        $this->AID = $AID;
        $this->ARQC = $ARQC;
        $this->aplicacionEMV = $aplicacionEMV;
        $this->codigoRespuestaAut = $codigoRespuestaAut;
        $this->criptograma = $criptograma;
        $this->fecha = $fecha;
        $this->fechaVencimiento = $fechaVencimiento;
        $this->hora = $hora;
        $this->MID = $MID;
        $this->modoLectura = $modoLectura;
        $this->numerTarjetaEncriptado = $numerTarjetaEncriptado;
        $this->PIN = $PIN;
        $this->publicidad = $publicidad;
        $this->TID = $TID;
        $this->TSI = $TSI;
        $this->TVR = $TVR;
        $this->tipoMensaje = $tipoMensaje;
        $this->mensajeRespuestaAut = $mensajeRespuestaAut;
        $this->monto = $monto;
        $this->IVA = $IVA;
        $this->propina = $propina;
        $this->servicio = $servicio;
        $this->ICE = $ICE;
        $this->otrosImp = $otrosImp;
        $this->base0 = $base0;
        $this->base12 = $base12;
        $this->cashOver = $cashOver;
        $this->trama = $trama;
    }


    function insert()
    {

        $sql = "INSERT INTO kiosk_payment (
              codigoRespuesta,
              lote,
              referencia,
              autorizacion,
              codigoAdquirente,
              nombreAdquiriente,
              tarjetaHabiente,
              numeroTarjeta,
              montoFijo,
              interes,
              AID,
              ARQC,
              aplicacionEMV,
              codigoRespuestaAut,
              criptograma,
              fecha,
              fechaVencimiento,
              hora,
              MID,
              modoLectura,
              numerTarjetaEncriptado, 
              PIN,
              publicidad,
              TID,
              TSI,
              TVR,
              tipoMensaje,
              mensajeRespuestaAut,
              monto,
              IVA,
              propina,
              servicio,
              ICE,
              otrosImp,
              base0,
              base12,
              cashOver,
			  trama
              ) VALUES (
               '" . $this->codigoRespuesta . "',
               '" . $this->lote . "',
               '" . $this->referencia . "',
               '" . $this->autorizacion . "',
               '" . $this->codigoAdquirente . "',
               '" . $this->nombreAdquiriente . "',
               '" . $this->tarjetaHabiente . "',
               '" . $this->numeroTarjeta . "',
               '" . $this->montoFijo . "',
               '" . $this->interes . "',
               '" . $this->AID . "',
               '" . $this->ARQC . "',
               '" . $this->aplicacionEMV . "',
               '" . $this->codigoRespuestaAut . "',
               '" . $this->criptograma . "',
               '" . $this->fecha . "',
               '" . $this->fechaVencimiento . "',
               '" . $this->hora . "',
               '" . $this->MID . "',
               '" . $this->modoLectura . "',
               '" . $this->numerTarjetaEncriptado . "',
               '" . $this->PIN . "',
               '" . $this->publicidad . "',
               '" . $this->TID . "',
               '" . $this->TSI . "',
               '" . $this->TVR . "',
               '" . $this->tipoMensaje . "',
               '" . $this->mensajeRespuestaAut . "',
               '" . $this->monto . "',
               '" . $this->IVA . "',
               '" . $this->propina . "',
               '" . $this->servicio . "',
               '" . $this->ICE . "',
               '" . $this->otrosImp . "',
               '" . $this->base0 . "',
               '" . $this->base12 . "',
               '" . $this->cashOver . "',
			   '" . $this->trama . "')";

        //Debug::print_r($sql);die();

        $result = $this->db->Execute($sql);

        if ($result) {
            return $this->db->insert_ID();
        } else {
            ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    function getAllData()
    {
        $sql = "SELECT * FROM kiosk_payment";
        $result = $this->db->getAll($sql);

        //Debug::print_r($result);die();
        if ($result) {
            $resultado = array();
            foreach ($result as $valor) {
                $array = array();
                $array['idPago'] = $valor['idPago'];
                $array['codigoRespuesta'] = $valor['codigoRespuesta'];
                $array['lote'] = $valor['lote'];
                $array['referencia'] = $valor['referencia'];
                $array['autorizacion'] = $valor['autorizacion'];
                $array['codigoAdquirente'] = $valor['codigoAdquirente'];
                $array['nombreAdquiriente'] = $valor['nombreAdquiriente'];
                $array['tarjetaHabiente'] = $valor['tarjetaHabiente'];
                $array['numeroTarjeta'] = $valor['numeroTarjeta'];
                $array['montoFijo'] = $valor['montoFijo'];
                $array['interes'] = $valor['interes'];
                $array['AID'] = $valor['AID'];
                $array['ARQC'] = $valor['ARQC'];
                $array['aplicacionEMV'] = $valor['aplicacionEMV'];
                $array['codigoRespuestaAut'] = $valor['codigoRespuestaAut'];
                $array['criptograma'] = $valor['criptograma'];
                $array['fecha'] = $valor['fecha'];
                $array['fechaVencimiento'] = $valor['fechaVencimiento'];
                $array['hora'] = $valor['hora'];
                $array['MID'] = $valor['MID'];
                $array['modoLectura'] = $valor['modoLectura'];
                $array['numerTarjetaEncriptado'] = $valor['numerTarjetaEncriptado'];
                $array['PIN'] = $valor['PIN'];
                $array['publicidad'] = $valor['publicidad'];
                $array['TID'] = $valor['TID'];
                $array['TSI'] = $valor['TSI'];
                $array['TVR'] = $valor['TVR'];
                $array['tipoMensaje'] = $valor['tipoMensaje'];
                $array['mensajeRespuestaAut'] = $valor['mensajeRespuestaAut'];
                $array['monto'] = $valor['monto'];
                $array['IVA'] = $valor['IVA'];
                $array['propina'] = $valor['propina'];
                $array['servicio'] = $valor['servicio'];
                $array['ICE'] = $valor['ICE'];
                $array['otrosImp'] = $valor['otrosImp'];
                $array['base0'] = $valor['base0'];
                $array['base12'] = $valor['base12'];
                $array['cashOver'] = $valor['cashOver'];
                $array['trama'] = $valor['trama'];
                array_push($resultado, $array);
            }
            //Debug::print_r($resultado);die();
            return $resultado;
        }


    }

    function getData()
    {
        //Debug::print_r()
        if ($this->idPago != NULL) {
            $sql = "SELECT idPago,
                        codigoRespuesta,
              lote,
              referencia,
              autorizacion,
              codigoAdquirente,
              nombreAdquiriente,
              tarjetaHabiente,
              numeroTarjeta,
              montoFijo,
              interes,
              AID,
              ARQC,
              aplicacionEMV,
              codigoRespuestaAut,
              criptograma,
              fecha,
              fechaVencimiento,
              hora,
              MID,
              modoLectura,
              numerTarjetaEncriptado, 
              PIN,
              publicidad,
              TID,
              TSI,
              TVR,
              tipoMensaje,
              mensajeRespuestaAut,
              monto,
              IVA,
              propina,
              servicio,
              ICE,
              otrosImp,
              base0,
              base12,
              cashOver,
			  trama
                    FROM kiosk_payment
                    WHERE idPago ='" . $this->idPago . "'";

            $result = $this->db->Execute($sql);
            if ($result === false)
                return false;

            if ($result->fields[0]) {
                $this->idPago = $result->fields[0];
                $this->codigoRespuesta = $result->fields[1];
                $this->lote = $result->fields[2];
                $this->referencia = $result->fields[3];
                $this->autorizacion = $result->fields[4];
                $this->codigoAdquirente = $result->fields[5];
                $this->nombreAdquiriente = $result->fields[6];
                $this->tarjetaHabiente = $result->fields[7];
                $this->numeroTarjeta = $result->fields[8];
                $this->montoFijo = $result->fields[9];
                $this->interes = $result->fields[10];
                $this->AID = $result->fields[11];
                $this->ARQC = $result->fields[12];
                $this->aplicacionEMV = $result->fields[13];
                $this->codigoRespuestaAut = $result->fields[14];
                $this->criptograma = $result->fields[15];
                $this->fecha = $result->fields[16];
                $this->fechaVencimiento = $result->fields[17];
                $this->hora = $result->fields[18];
                $this->MID = $result->fields[19];
                $this->modoLectura = $result->fields[20];
                $this->numerTarjetaEncriptado = $result->fields[21];
                $this->PIN = $result->fields[22];
                $this->publicidad = $result->fields[23];
                $this->TID = $result->fields[24];
                $this->TSI = $result->fields[25];
                $this->TVR = $result->fields[26];
                $this->tipoMensaje = $result->fields[27];
                $this->mensajeRespuestaAut = $result->fields[28];
                $this->monto = $result->fields[29];
                $this->IVA = $result->fields[30];
                $this->propina = $result->fields[31];
                $this->servicio = $result->fields[32];
                $this->ICE = $result->fields[33];
                $this->otrosImp = $result->fields[34];
                $this->base0 = $result->fields[35];
                $this->base12 = $result->fields[36];
                $this->cashOver = $result->fields[37];
                $this->trama = $result->fields[38];
                return true;
            }
        }
        return false;



    }

    function update() {
        $sql = "UPDATE kiosk_payment SET
					codigoRespuesta		=	 '" . $this->codigoRespuesta . "',
					lote		=	 '" . $this->lote . "',
					referencia=		 '" . $this->referencia . "',
					autorizacion=		 '" . $this->autorizacion . "',
					codigoAdquirente	=	 '" . $this->codigoAdquirente . "',
					nombreAdquiriente=		 '" . $this->nombreAdquiriente . "',
					tarjetaHabiente	=		 '" . $this->tarjetaHabiente . "',
					numeroTarjeta	=		 '" . $this->numeroTarjeta . "',
					montoFijo=		 '" . $this->montoFijo . "',
					interes=			 '" . $this->interes . "',
					AID=		 '" . $this->AID . "',				
					ARQC=		 '" . $this->ARQC . "',
					aplicacionEMV=	 '" . $this->aplicacionEMV . "',
					codigoRespuestaAut =			 '" . $this->codigoRespuestaAut . "',
					criptograma=          '" . $this->criptograma . "',
					fecha=        '" . $this->fecha . "',
                    fechaVencimiento= '" . $this->fechaVencimiento . "',
                    hora=        '" . $this->hora . "',
                    MID =        '" . $this->MID . "',
                    modoLectura =        '" . $this->modoLectura . "',
                    numerTarjetaEncriptado =        '" . $this->numerTarjetaEncriptado . "',
                    PIN =        '" . $this->PIN . "',
                    publicidad =        '" . $this->publicidad . "',
                    TID =        '" . $this->TID . "',
                    TSI =        '" . $this->TSI . "',
                    TVR =        '" . $this->TVR . "',
                    tipoMensaje =        '" . $this->tipoMensaje . "',
                    mensajeRespuestaAut=        '" . $this->mensajeRespuestaAut . "',
                    monto =        '" . $this->monto . "',
                    IVA =        '" . $this->IVA . "',
                    propina =        '" . $this->propina . "',
                    servicio =        '" . $this->servicio . "',
                    ICE =        '" . $this->ICE . "',
                    otrosImp =        '" . $this->otrosImp . "',
                    base0 =        '" . $this->base0. "',
                    base12 =        '" . $this->base12 . "',
                    cashOver =        '" . $this->cashOver . "',
                    trama =        '".$this->trama ."'
                WHERE idPago = '" . $this->idPago . "'";

        $result = $this->db->Execute($sql);

        if ($result) {
            return true;
        } else {
            ErrorLog::log($this->db, 'UPDATE FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    //GETTERS
    function getIdPago()
    {
        return $this->idPago;
    }

    function getCodigoRespuesta()
    {
        return $this->codigoRespuesta;
    }

    function getLote()
    {
        return $this->lote;
    }

    function getReferencia()
    {
        return $this->referencia;
    }

    function getAutorizacion()
    {
        return $this->autorizacion;
    }

    function getCodigoAdquirente()
    {
        return $this->codigoAdquirente;
    }

    function getNombreAdquiriente()
    {
        //Debug::print_r($this->nombreAdquiriente);die();
        return $this->nombreAdquiriente;
    }

    function getTarjetaHabiente()
    {
        return $this->tarjetaHabiente;
    }

    function getNumeroTarjeta()
    {
        return $this->numeroTarjeta;
    }

    function getMontoFijo()
    {
        return $this->montoFijo;
    }

    function getInteres()
    {
        return $this->interes;
    }

    function getAID()
    {
        return $this->AID;
    }

    function getARQC()
    {
        return $this->ARQC;
    }

    function getAplicacionEMV()
    {
        return $this->aplicacionEMV;
    }

    function getCodigoRespuestaAut()
    {
        return $this->codigoRespuestaAut;
    }

    function getCriptograma()
    {
        return $this->criptograma;
    }

    function getFecha()
    {
        return $this->fecha;
    }

    function getFechaVencimiento()
    {
        return $this->fechaVencimiento;
    }

    function getHora()
    {
        return $this->hora;
    }

    function getMID()
    {
        return $this->MID;
    }

    function getModoLectura()
    {
        return $this->modoLectura;
    }

    function getNumerTarjetaEncriptado()
    {
        return $this->numerTarjetaEncriptado;
    }

    function getPIN()
    {
        return $this->PIN;
    }

    function getPublicidad()
    {
        return $this->publicidad;
    }

    function getTID()
    {
        return $this->TID;
    }

    function getTSI()
    {
        return $this->TSI;
    }

    function getTVR()
    {
        return $this->TVR;
    }

    function getTipoMensaje()
    {
        return $this->tipoMensaje;
    }

    function getMensajeRespuestaAut()
    {
        return $this->mensajeRespuestaAut;
    }

    function getMonto()
    {
        return $this->monto;
    }

    function getIVA()
    {
        return $this->IVA;
    }

    function getPropina()
    {
        return $this->propina;
    }

    function getServicio()
    {
        return $this->servicio;
    }

    function getICE()
    {
        return $this->ICE;
    }

    function getOtrosImp()
    {
        return $this->otrosImp;
    }

    function getBase0()
    {
        return $this->base0;
    }

    function getBase12()
    {
        return $this->base12;
    }

    function getCashOver()
    {
        return $this->cashOver;
    }

    function getTrama()
    {
        return $this->trama;
    }

    //SETTERS
    function setIdPago($idPago)
    {
        $this->idPago = $idPago;
    }

    function setCodigoRespuesta($codigoRespuesta)
    {
        //Debug::print_r($codigoRespuesta);
        $this->codigoRespuesta = $codigoRespuesta;
    }

    function setLote($lote)
    {
        $this->lote = $lote;
    }

    function setReferencia($referencia)
    {
        $this->referencia = $referencia;
    }

    function setAutorizacion($autorizacion)
    {
        $this->autorizacion = $autorizacion;
    }

    function setCodigoAdquirente($codigoAdquirente)
    {
        $this->codigoAdquirente = $codigoAdquirente;
    }

    function setNombreAdquiriente($nombreAdquiriente)
    {
        $this->nombreAdquiriente = $nombreAdquiriente;
    }

    function setTarjetaHabiente($tarjetaHabiente)
    {
        $this->tarjetaHabiente = $tarjetaHabiente;
    }

    function setNumeroTarjeta($numeroTarjeta)
    {
        $this->numeroTarjeta = $numeroTarjeta;
    }

    function setMontoFijo($montoFijo)
    {
        $this->montoFijo = $montoFijo;
    }

    function setInteres($interes)
    {
        $this->interes = $interes;
    }

    function setAID($AID)
    {
        $this->AID = $AID;
    }

    function setARQC($ARQC)
    {
        $this->ARQC = $ARQC;
    }

    function setAplicacionEMV($aplicacionEMV)
    {
        $this->aplicacionEMV = $aplicacionEMV;
    }

    function setCodigoRespuestaAut($codigoRespuestaAut)
    {
        $this->codigoRespuestaAut = $codigoRespuestaAut;
    }

    function setCriptograma($criptograma)
    {
        $this->criptograma = $criptograma;
    }

    function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    function setFechaVencimiento($fechaVencimiento)
    {
        $this->fechaVencimiento = $fechaVencimiento;
    }

    function setHora($hora)
    {
        $this->hora = $hora;
    }

    function setMID($MID)
    {
        $this->MID = $MID;
    }

    function setModoLectura($modoLectura)
    {
        $this->modoLectura = $modoLectura;
    }

    function setNumerTarjetaEncriptado($numerTarjetaEncriptado)
    {
        $this->numerTarjetaEncriptado = $numerTarjetaEncriptado;
    }

    function setPIN($PIN)
    {
        $this->PIN = $PIN;
    }

    function setPublicidad($publicidad)
    {
        $this->publicidad = $publicidad;
    }

    function setTID($TID)
    {
        $this->TID = $TID;
    }

    function setTSI($TSI)
    {
        $this->TSI = $TSI;
    }

    function setTVR($TVR)
    {
        $this->TVR = $TVR;
    }

    function setTipoMensaje($tipoMensaje)
    {
        $this->tipoMensaje = $tipoMensaje;
    }

    function setMensajeRespuestaAut($mensajeRespuestaAut)
    {
        $this->mensajeRespuestaAut = $mensajeRespuestaAut;
    }

    function setMonto($monto)
    {
        $this->monto = $monto;
    }

    function setIVA($IVA)
    {
        $this->IVA = $IVA;
    }

    function setPropina($propina)
    {
        $this->propina = $propina;
    }

    function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    function setICE($ICE)
    {
        $this->ICE = $ICE;
    }

    function setOtrosImp($otrosImp)
    {
        $this->otrosImp = $otrosImp;
    }

    function setBase0($base0)
    {
        $this->base0 = $base0;
    }

    function setBase12($base12)
    {
        $this->base12 = $base12;
    }

    function setCashOver($cashOver)
    {
        $this->cashOver = $cashOver;
    }

    function setTrama($trama)
    {
        $this->trama = $trama;
    }


}