<?php
/*
File: CreditDay.class.php
Author: Patricio Astudillo M.
Creation Date: 06/01/2010 16:01
Last Modified: 06/01/2010
Modified By: Patricio Astudillo
*/

class CreditDay{				
	var $db;
	var $serial_cdd;
	var $serial_cou;
	var $days_cdd;
	var $status_cdd;

	function __construct($db, $serial_cdd=NULL, $serial_cou=NULL, $days_cdd=NULL, $status_cdd=NULL ){
		$this -> db = $db;
		$this -> serial_cdd = $serial_cdd;
		$this -> serial_cou = $serial_cou;
		$this -> days_cdd = $days_cdd;
		$this -> status_cdd = $status_cdd;
	}
	
	/** 
	@Name: getData
	@Description: Retrieves the data of a creditDay object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_cdd!=NULL){
			$sql = 	"SELECT serial_cdd, serial_cou, days_cdd, status_cdd
					FROM credit_day
					WHERE serial_cdd='".$this->serial_cdd."'";
                                        //die($sql);
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_cdd =$result->fields[0];
				$this -> serial_cou = $result->fields[1];
				$this -> days_cdd = $result->fields[2];
				$this -> status_cdd = $result->fields[3];
			
				return true;
			}else
				return false;
		}else
			return false;		
	}
	
	/** 
	@Name: creditDayExists
	@Description: Verifies if a creditDay exist for a specific country.
	@Params: $days_cdd(creditDay), $serial_cou(Country ID), $serial_cdd(creditDay ID)
	@Returns: TRUE if exist. / FALSE on no match
	**/
	function creditDayExists($days_cdd, $serial_cou, $serial_cdd){
		$sql = 	"SELECT serial_cdd
				 FROM credit_day 
				 WHERE days_cdd='".$days_cdd."'
				 AND serial_cou='".$serial_cou."'";
				 
		if($serial_cdd!=NULL){
			$sql.=" AND serial_cdd <> ".$serial_cdd;
		}

		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	/** 
	@Name: creditDayStatus
	@Description: Verifies if a Dealer is assosiated to a creditDay.
	@Params: $serial_cdd
	@Returns: TRUE if exist. / FALSE on no match
	**/
	function creditDayStatus($serial_cdd){
		$sql = 	"SELECT dea.serial_dea
				 FROM dealer dea
				 WHERE dea.serial_cdd='".$serial_cdd."'";
				 
		//echo $sql;
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}
	
	
	/** 
	@Name: getCreditDays
	@Description: Gets all credit days for a specific country.
	@Params: $serial_cou(Country ID)
	@Returns: Credit days array
	**/
	function getCreditDays($serial_cou){
		$lista=NULL;
		$sql = 	"SELECT cd.serial_cdd as 'serial', cd.days_cdd, cd.status_cdd as 'status', cd.serial_cdd as 'update'
				 FROM credit_day cd
				 WHERE cd.serial_cou=".$serial_cou."
				 AND cd.status_cdd ='ACTIVE'
				 ORDER BY cd.days_cdd";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}
		
		return $arr;
	}

        /**
	@Name: getCreditDays
	@Description: Gets all credit days for a specific country.
	@Params: $serial_cou(Country ID)
	@Returns: Credit days array
	**/
	function getAllCreditDays($serial_cou){
		$lista=NULL;
		$sql = 	"SELECT cd.serial_cdd as 'serial', cd.days_cdd, cd.status_cdd as 'status', cd.serial_cdd as 'update'
				 FROM credit_day cd
				 WHERE cd.serial_cou=".$serial_cou."
				 ORDER BY cd.days_cdd";
                
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;

			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;

			}while($cont<$num);
		}

		return $arr;
	}
	
	/** 
	@Name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
    function insert() {
        $sql = "
            INSERT INTO credit_day (
				serial_cdd,
				serial_cou,
				days_cdd,
				status_cdd)
            VALUES (
                NULL,
				'".$this->serial_cou."',
				'".$this->days_cdd."',
				'".$this->status_cdd."')";

        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
 	/** 
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
    function update() {
        $sql = "
                UPDATE credit_day SET
					serial_cou ='".$this->serial_cou."',
					days_cdd ='".$this->days_cdd."',
					status_cdd ='".$this->status_cdd."'
				WHERE serial_cdd = '".$this->serial_cdd."'";

        $result = $this->db->Execute($sql);
        if ($result === false)return false;
		
        if($result==true)
            return true;
        else
            return false;
    }
	
	/** 
	@Name: getStatusList
	@Description: Gets all credit days status in DB.
	@Params: N/A
	@Returns: Status array
	**/
	function getStatusList(){
		$sql = "SHOW COLUMNS FROM credit_day LIKE 'status_cdd'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}
	
	///GETTERS
	function getSerial_cdd(){
		return $this->serial_cdd;
	}
	function getSerial_cou(){
		return $this->serial_cou;
	}
	function getDays_cdd(){
		return $this->days_cdd;
	}
	function getStatus_cdd(){
		return $this->status_cdd;
	}

	///SETTERS	
	function setSerial_cdd($serial_cdd){
		$this->serial_cdd = $serial_cdd;
	}
	function setSerial_cou($serial_cou){
		$this->serial_cou = $serial_cou;
	}
	function setDays_cdd($days_cdd){
		$this->days_cdd = $days_cdd;
	}
	function setStatus_cdd($status_cdd){
		$this->status_cdd = $status_cdd;
	}
}
?>