<?php
/*
Author: Patricio Astudillo 
Modified date: 30/08/2010
Last modified: 30/08/2010
*/

class GConditionByLanguageByCountry {
	var $db;
	var $serial_glc;
	var $serial_gcn;
	var $serial_cou;
	var $serial_lang;
	var $url_glc;
	var $name_glc;
	
	function __construct($db, $serial_glc=NULL, $serial_gcn = NULL, $serial_cou=NULL,
						 $serial_lang = NULL, $name_glc = NULL, $url_glc = NULL){
		$this -> db = $db;
		$this -> serial_glc = $serial_glc;
		$this -> serial_gcn = $serial_gcn;
		$this -> serial_cou = $serial_cou;
		$this -> serial_lang = $serial_lang;
		$this -> name_glc = $name_glc;
		$this -> url_glc = $url_glc;
	}
	
	/* function getData() 		*/
	/* set class parameters		*/
	function getData(){
		if($this->serial_glc!=NULL){
			$sql = 	"SELECT serial_glc, serial_gcn, serial_cou, serial_lang, name_glc, url_glc
					FROM gconditions_by_language_by_country
					WHERE serial_glc='".$this->serial_glc."'";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_gcn=$result->fields[1];
				$this->serial_cou=$result->fields[2];
				$this->serial_lang=$result->fields[3];
				$this->name_glc=$result->fields[4];
				$this->url_glc=$result->fields[5];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}
	
	function insert(){
		$sql="INSERT INTO gconditions_by_language_by_country (
					serial_glc,
					serial_gcn,
					serial_cou,
					serial_lang,
					name_glc,
					url_glc)
			  VALUES(NULL,
				    '".$this->serial_gcn."',
					'".$this->serial_cou."',
					'".$this->serial_lang."',
					'".$this->name_glc."',
					'".$this->url_glc."');";
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/* function update() 						*/
	/* update a General Conditions				*/
	function update(){
		if($this->serial_glc!=NULL){
			$sql="UPDATE gconditions_by_language_by_country SET
				serial_gcn='".$this->serial_gcn."',
				serial_lang='".$this->serial_lang."',
				serial_cou='".$this->serial_cou."',
				name_glc='".$this->name_glc."',
				url_glc='".$this->url_glc."'
				WHERE serial_glc='".$this->serial_glc."'";
					
			$result = $this->db->Execute($sql);

			if ($result==true) 
				return true;
			else
				return false;
		}else
			return false;		
	}
	
	
	function getAvailableConditionsByCountry($serial_gcn,$serial_cou,$serial_lang=NULL){
		$sql = "SELECT serial_glc, serial_lang, name_glc, url_glc
				FROM gconditions_by_language_by_country
				WHERE serial_gcn=".$serial_gcn."
				AND serial_cou=".$serial_cou;

		if($serial_lang!=''){
			$sql.=" AND serial_lang='".$serial_lang."'";
		}

		$result = $this->db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}	
	
	/* function existGeneralCondition() 			*/
	/* returns true if name_glc already exists	*/
	function existGeneralConditionByLanguage($name_gcn,$serial_lang,$serial_gcn=NULL){
		$sql = 	"SELECT serial_glc
				 FROM gconditions_by_language_by_country
				 WHERE name_glc='".utf8_encode($name_gcn)."'
				 AND serial_lang=".$serial_lang;
				 
		if($serial_gcn != NULL && $serial_gcn != ''){
			$sql.= " AND serial_gcn <> ".$serial_gcn;
		}		 
		
		$result = $this->db -> Execute($sql);
		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	public static function getTranslationsInfo($db, $serial_gcn, $serial_lang){
		$sql="SELECT glc.serial_glc, glc.name_glc, c.name_cou, l.name_lang, glc.url_glc
			  FROM gconditions_by_language_by_country glc
			  JOIN language l ON l.serial_lang=glc.serial_lang
			  JOIN country c ON c.serial_cou=glc.serial_cou
			  WHERE glc.serial_gcn=$serial_gcn
			  AND glc.serial_lang=$serial_lang";

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function getRemainingLanguagesToTranslate($db, $serial_cou, $serial_gcn){
		$sql="SELECT l.serial_lang, l.name_lang
			  FROM language l
			  WHERE l.serial_lang NOT IN (SELECT glc.serial_lang
										  FROM gconditions_by_language_by_country glc
										  WHERE glc.serial_gcn=$serial_gcn
										  AND glc.serial_cou=$serial_cou)";

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function generateFileName($db){
		$name='general_condition_'.rand(1, 1000).'.pdf';

		do{
			$sql="SELECT serial_glc
			      FROM gconditions_by_language_by_country
				  WHERE url_glc='$name'";

			$id=$db->getOne($sql);
		}while($id);

		return $name;
	}

	public static function updateNameByCondition_Language($db, $serial_gcn, $serial_lang, $name_glc){
		$sql="UPDATE gconditions_by_language_by_country SET
				name_glc='$name_glc'
			 WHERE serial_gcn=$serial_gcn
			 AND serial_lang=$serial_lang";

		$result=$db->Execute($sql);

		if($result)
			return TRUE;
		else
			return FALSE;
	}
	
	///GETTERS
	function getSerial_glc(){
		return $this->serial_glc;
	}
	function getSerial_cou(){
		return $this->serial_cou;
	}
	function getSerial_gcn(){
		return $this->serial_gcn;
	}
	function getSerial_lang(){
		return $this->serial_lang;
	}
	function getName_glc(){
		return $this->name_glc;
	}
	function getUrl_glc(){
		return $this->url_glc;
	}

	///SETTERS	
	function setSerial_glc($serial_glc){
		$this->serial_glc = $serial_glc;
	}
	function setSerial_gcn($serial_gcn){
		$this->serial_gcn = $serial_gcn;
	}
	function setSerial_cou($serial_cou){
		$this->serial_cou = $serial_cou;
	}
	function setSerial_lang($serial_lang){
		$this->serial_lang = $serial_lang;
	}
	function setName_glc($name_glc){
		$this->name_glc = $name_glc;
	}
	function setUrl_glc($url_glc){
		$this->url_glc = $url_glc;
	}
}
?>
