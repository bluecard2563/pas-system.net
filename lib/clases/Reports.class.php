<?php
/**
 * File: Reports.class.php
 * Author: Patricio Astudillo
 * Creation Date: 27/10/2016
 * Modified By:
 * Last Modified:
*/

class Reports {
	public static function getInvoicesForERPMatch($db, $countryID, $beginDate, $endDate, $MBCId = NULL){
		if($MBCId){
			$mbc_clause = " AND mbc.serial_mbc = $MBCId";
		}
		
		$sql = "SELECT * 
				FROM 
				(
					SELECT i.serial_inv, i.number_inv, mbc.serial_mbc, 
							DATE_FORMAT(i.date_inv, '%d/%m/%Y') as date_inv, 
							CONCAT( cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'') ) name_cus, 
							i.serial_cus,
							IF(i.serial_cus IS NULL,'Comercializador','Cliente') facturado_a,
							i.serial_dea,
							i.total_inv,
							i.status_inv,
							man.name_man,
							bra.name_dea as name_bra,
							d.name_doc,
							i.subtotal_inv
					FROM 	invoice i
					JOIN invoice_log ilog ON  ilog.serial_inv = i.serial_inv
					JOIN sales s ON  s.serial_sal = ilog.serial_sal
					JOIN document_by_manager dbm ON dbm.serial_dbm = i.serial_dbm
					JOIN document_type d ON d.serial_doc = dbm.serial_doc
					JOIN customer cus ON cus.serial_cus = s.serial_cus
					JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
					JOIN user cnt_user ON cnt.serial_usr = cnt_user.serial_usr
					JOIN dealer bra ON cnt.serial_dea = bra.serial_dea
					JOIN user_by_dealer ubd ON bra.serial_dea = ubd.serial_dea AND ubd.status_ubd = 'ACTIVE'
					JOIN user usr ON ubd.serial_usr = usr.serial_usr
					JOIN manager_by_country mbc ON bra.serial_mbc = mbc.serial_mbc
					JOIN manager man ON mbc.serial_man = man.serial_man 
					AND mbc.serial_cou = $countryID
					$MBCId
					AND STR_TO_DATE(DATE_FORMAT(i.date_inv,'%d/%m/%Y'), '%d/%m/%Y') 
						BETWEEN STR_TO_DATE('$beginDate' , '%d/%m/%Y') 
						AND STR_TO_DATE('$endDate' , '%d/%m/%Y')  
					GROUP BY dbm.type_dbm, i.number_inv  

					UNION 

					SELECT i.serial_inv, i.number_inv, mbc.serial_mbc, 
							DATE_FORMAT(i.date_inv, '%d/%m/%Y') as date_inv, 
							CONCAT( cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'') ) name_cus, 
							i.serial_cus,
							IF(i.serial_cus IS NULL,'Comercializador','Cliente') facturado_a,
							i.serial_dea,
							i.total_inv,
							i.status_inv,
							man.name_man,
							bra.name_dea as name_bra,
							d.name_doc,
							i.subtotal_inv
					FROM invoice i
					JOIN sales s ON  s.serial_inv = i.serial_inv
					JOIN document_by_manager dbm ON dbm.serial_dbm = i.serial_dbm
					JOIN document_type d ON d.serial_doc = dbm.serial_doc
					JOIN customer cus ON cus.serial_cus = s.serial_cus
					JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
					JOIN user cnt_user ON cnt.serial_usr = cnt_user.serial_usr
					JOIN dealer bra ON cnt.serial_dea = bra.serial_dea
					JOIN user_by_dealer ubd ON bra.serial_dea = ubd.serial_dea AND ubd.status_ubd = 'ACTIVE'
					JOIN user usr ON ubd.serial_usr = usr.serial_usr
					JOIN manager_by_country mbc ON bra.serial_mbc = mbc.serial_mbc
					JOIN manager man ON mbc.serial_man = man.serial_man 
					AND mbc.serial_cou = $countryID 
					$MBCId
					AND STR_TO_DATE(DATE_FORMAT(i.date_inv,'%d/%m/%Y'), '%d/%m/%Y') 
						BETWEEN STR_TO_DATE('$beginDate' , '%d/%m/%Y') 
						AND STR_TO_DATE('$endDate' , '%d/%m/%Y')  
					GROUP BY dbm.type_dbm, i.number_inv 
				)as results
				ORDER BY number_inv";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getSummarizedSalesForInvoice($db, $invoiceID){
		$sql = "SELECT	SUM(total_sal) AS 'sold', 
						ROUND(((IFNULL(discount_prcg_inv, 0) + IFNULL(other_dscnt_inv, 0))/100) * SUM(total_sal), 2) AS 'discount',
						ROUND((IFNULL(comision_prcg_inv, 0) / 100) * SUM(total_sal), 2) AS 'comission',
						i.applied_taxes_inv
				FROM sales s
				JOIN invoice i ON i.serial_inv = s.serial_inv
					AND i.serial_inv = $invoiceID";
		
		//Debug::print_r($sql); die;
		$result = $db->getRow($sql);
		
		if($result['sold']){
			return $result;
		}else{
			$sql = "SELECT SUM(total_sal) AS 'sold', 
							ROUND(((IFNULL(discount_prcg_inv, 0) + IFNULL(other_dscnt_inv, 0))/100) * SUM(total_sal), 2) AS 'discount',
							ROUND((IFNULL(comision_prcg_inv, 0) / 100) * SUM(total_sal), 2) AS 'comission',
							i.applied_taxes_inv
					FROM invoice_log il 
					JOIN invoice i ON i.serial_inv = il.serial_inv
					JOIN sales s ON s.serial_sal = il.serial_sal
						AND il.serial_inv = $invoiceID";
			
			//Debug::print_r($sql); die;
			$result = $db->getRow($sql);

			if($result){
				return $result;
			}else{
				return FALSE;
			}
		}
	}
	
	public static function getComissionableSalesList($db, $dateFrom, $dateTo, $mbcID = NULL, $branchID = NULL){
		global $mbcAllowedToComissionate;
		if($mbcID){
			$mbcClause = " AND b.serial_mbc = $mbcID";
		}
		
		if($branchID){
			$deaClause = " AND b.serial_dea = $branchID";
		}
		
		$sql = "SELECT s.card_number_sal, 
						DATE_FORMAT(s.in_date_sal, '%d/%m/%Y') AS 'in_date_sal',
						s.total_sal, 
						s.status_sal, i.number_inv, i.status_inv, i.total_inv, 
						ROUND(((IFNULL(discount_prcg_inv, 0) + IFNULL(other_dscnt_inv, 0))/100) * total_sal, 2) AS 'discount',
						CONCAT(u.first_name_usr, ' ' ,u.last_name_usr) AS 'counter',
						p.erp_id, DATE_FORMAT(p.date_pay, '%d/%m/%Y') AS 'date_pay',
						cnt.serial_cnt, IFNULL(s.card_number_sal, 'N/A') AS 'card_number',
                                                i.applied_taxes_inv as 'applied_taxes_inv'
				FROM sales s
				JOIN invoice i ON s.serial_inv = i.serial_inv 
					AND status_inv = 'PAID'
					AND s.status_sal in ('REGISTERED', 'ACTIVE','EXPIRED', 'STANDBY')
					AND s.in_date_sal BETWEEN STR_TO_DATE('$dateFrom', '%d/%m/%Y') AND STR_TO_DATE('$dateTo', '%d/%m/%Y')
				JOIN payments p ON p.serial_pay = i.serial_pay
					
				JOIN counter cnt on cnt.serial_cnt = s.serial_cnt
				JOIN dealer b ON b.serial_dea = cnt.serial_dea 
					AND b.serial_mbc IN ($mbcAllowedToComissionate)
					$mbcClause
					$deaClause
				JOIN user u on u.serial_usr = cnt.serial_usr";
		
		//AND p.date_pay BETWEEN STR_TO_DATE('$dateFrom', '%d/%m/%Y') AND STR_TO_DATE('$dateTo', '%d/%m/%Y')
		
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
    
    public static function getPaidDataForManagementAnalysis($db, $dateFrom, $dateTo, $serial_cou, $serial_mbc = ""){
        if($serial_mbc != ""){
            $mbc_clause = " AND mbc.serial_mbc = $serial_mbc";
        }
        
        $sql = "SELECT u.serial_usr, CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'responsible', 
                        ROUND(s.total_sal - (s.total_sal * (IFNULL(i.discount_prcg_inv, 0) / 100) + (IFNULL(other_dscnt_inv, 0) / 100 )), 2) AS 'calculatedInvoice',
                        s.total_sal, i.total_inv, 
                        IF(s.status_sal = 'REFUNDED', s.total_sal, 0) AS 'total_refunded',
                        ROUND((other_dscnt_inv / 100) * i.total_inv, 2) AS 'amount_discount', mbc.serial_mbc, m.name_man,
                         s.serial_sal, s.card_number_sal, i.number_inv
                FROM sales s
                JOIN invoice i on i.serial_inv = s.serial_inv
                    AND i.status_inv in ('PAID', 'VOID')
                    AND s.status_sal IN ('STAND-BY', 'ACTIVE', 'EXPIRED', 'REFUNDED')
                JOIN payments p ON p.serial_pay = i.serial_pay
                    AND p.full_date_pay BETWEEN STR_TO_DATE('$dateFrom', '%d/%m/%Y') AND STR_TO_DATE('$dateTo', '%d/%m/%Y')
                JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
                JOIN dealer b ON b.serial_dea = cnt.serial_dea
                JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea
                    AND STR_TO_DATE(DATE_FORMAT(s.in_date_sal,'%d/%m/%Y'),'%d/%m/%Y') 
						BETWEEN ubd.begin_date_ubd AND 
						IFNULL(ubd.end_date_ubd, CURRENT_DATE())
                JOIN user u ON u.serial_usr = ubd.serial_usr
                JOIN manager_by_country mbc ON mbc.serial_mbc = b.serial_mbc
                    AND mbc.serial_cou = $serial_cou
                    $mbc_clause
                JOIN manager m ON m.serial_man = mbc.serial_man
                ORDER BY m.serial_man, u.serial_usr";
        
        //Debug::print_r($sql); die;
        $result = $db -> getAll($sql);
        
        if($result){
            return $result;
        }else{
            return FALSE;
        }
    }
    
    public static function acessMonthlyList($db, $beginDate, $endDate){
        $sql = "SELECT DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date', s.serial_sal,
                        DATE_FORMAT(s.end_date_sal, '%d/%m/%Y') AS 'end_date',  
                        IFNULL(ih.document_cus, d.id_dea) AS 'invoiced_document',
                        ins.document_cus AS 'holder_document', 
                        CONCAT(ins.last_name_cus, ' ',ins.first_name_cus) AS 'holder_names',
                        IF(p.flights_pro = 0 AND p.third_party_register_pro = 'YES', 'CORPORATIVE', 'INDIVIDUAL') AS 'product_type'
                FROM sales s
                JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
                JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
                    AND pxc.serial_cou = 62
                JOIN product p ON p.serial_pro = pxc.serial_pro
                JOIN invoice i ON i.serial_inv = s.serial_inv
                    AND date_inv between STR_TO_DATE('$beginDate', '%d/%m/%Y') AND STR_TO_DATE('$endDate', '%d/%m/%Y')
                JOIN customer ins ON ins.serial_cus = s.serial_cus
                LEFT JOIN customer ih ON ih.serial_cus = i.serial_cus
                LEFT JOIN dealer d ON d.serial_dea = i.serial_dea
                ORDER BY i.date_inv";
        
        $result = $db->getAll($sql);
        
        if($result){
            $finalArray = array();
            
            foreach($result as $sale){
                $lineArray = array();
                //DEPENDENT SQL SECTION
                $productInfoSQL = "SELECT c.document_cus, CONCAT(c.last_name_cus, ' ',c.first_name_cus) AS 'dependent_names'
                                   FROM sales s
                                   JOIN extras e ON e.serial_sal = s.serial_sal
                                   JOIN customer c ON c.serial_cus = e.serial_cus
                                   AND s.serial_sal = {$sale['serial_sal']}";
                
                $dependentsArray = $db->getAll($productInfoSQL);
                
                switch(count($dependentsArray)){
                    case 0: $policy_type = 'T';
                        break;
                    case 1: $policy_type = 'T+1';
                        break;
                    default: $policy_type = 'T+F';
                        break;
                }
                
                switch(ERPConnectionFunctions::retrieveDocumentType($sale['invoiced_document'])){
                    case '1': $idtype_holder = 'R';
                        break;
                    case '2': $idtype_holder = 'C';
                        break;
                    default: $idtype_holder = 'P';
                        break;
                }
                
                
                if($sale['product_type'] == 'CORPORATIVE'){
                   $lineArray['type'] = 'C'; 
                }else{
                   $lineArray['type'] = 'I'; 
                }
                
                $lineArray['begin'] = $sale['begin_date'];
                $lineArray['end'] = $sale['end_date'];
                $lineArray['id_holder'] = $sale['invoiced_document'];
                $lineArray['idtype_holder'] = $idtype_holder;
                $lineArray['policy_type'] = $policy_type;
                $lineArray['id_insured'] = $sale['holder_document'];
                $lineArray['names_insured'] = $sale['holder_names'];
                $lineArray['insured_type'] = 'XXX';
                $lineArray['company_name'] = 'Blue Card Ecuador S.A.';
                $lineArray['company_id'] = '1391722907001';
                
                array_push($finalArray, $lineArray);
                
                //DEPENDENT ARRAY SECTION
                if($dependentsArray){
                    $depLineArray = array();
                    
                    
                    foreach($dependentsArray as $dependent){
                        $depLineArray['type'] = $lineArray['type'];
                        $depLineArray['begin'] = $lineArray['begin'];
                        $depLineArray['end'] = $lineArray['end'];
                        $depLineArray['id_holder'] = $lineArray['id_holder'];
                        $depLineArray['idtype_holder'] = $lineArray['idtype_holder'];
                        $depLineArray['policy_type'] = $policy_type;
                        $depLineArray['id_insured'] = $dependent['document_cus'];
                        $depLineArray['names_insured'] = $dependent['dependent_names'];
                        $depLineArray['insured_type'] = 'XXXX';
                        $depLineArray['company_name'] = 'Blue Card Ecuador S.A.';
                        $depLineArray['company_id'] = '1391722907001';
                        
                        array_push($finalArray, $depLineArray);
                    }
                }
                
                //CORPORATIVE SECTION
                $corpSQL = "SELECT c.document_cus, CONCAT(c.last_name_cus, ' ',c.first_name_cus) AS 'dependent_names'
                            FROM sales s
                            JOIN travelers_log tl ON tl.serial_sal = tl.serial_sal
                            JOIN customer c On c.serial_cus = tl.serial_cus
                            AND s.serial_sal = {$sale['serial_sal']}";
                            
                $corpArray = $db->getAll($corpSQL);
                
                if($corpArray){
                    $corpLineArray = array();
                    
                    foreach($corpArray as $registry){
                        $corpLineArray['type'] = $lineArray['type'];
                        $corpLineArray['begin'] = $lineArray['begin'];
                        $corpLineArray['end'] = $lineArray['end'];
                        $corpLineArray['id_holder'] = $lineArray['id_holder'];
                        $corpLineArray['idtype_holder'] = $lineArray['idtype_holder'];
                        $corpLineArray['policy_type'] = $policy_type;
                        $corpLineArray['id_insured'] = $registry['document_cus'];
                        $corpLineArray['names_insured'] = $registry['dependent_names'];
                        $corpLineArray['insured_type'] = 'XXX';
                        $corpLineArray['company_name'] = 'Blue Card Ecuador S.A.';
                        $corpLineArray['company_id'] = '1391722907001';
                        
                        array_push($finalArray, $corpLineArray);
                    }
                }
            }
            
            return $finalArray;
        }else{
            return FALSE;
        }
    }
}
