<?php
/*
File: User_web.php
Author: Santiago Borja Lopez
Creation Date: 11/02/2010 10:00
LastModified: 11/02/2010 10:00
Modified By: Santiago Borja Lopez
*/
class User_web{				
	var $db;
	var $serial_usw;
	var $username_usw;
	var $first_name_usw;
	var $last_name_usw;
	var $email_usw;
	var $password_usw;
	var $first_access_usw;
	var $status_usw;
	var $admin_usw;
		
	function __construct($db, $serial_usw=NULL, $username_usw = NULL, $first_name_usw=NULL, $last_name_usw=NULL, $email_usw=NULL, $password_usw=NULL, $first_access_usw=NULL, $status_usw=NULL, $admin_usw=NULL){
		$this -> db = $db;
		$this -> serial_usw = $serial_usw;
		$this -> username_usw = $username_usw;
		$this -> first_name_usw = $first_name_usw;
		$this -> last_name_usw = $last_name_usw;
		$this -> email_usw = $email_usw; 
		$this -> password_usw = $password_usw;
		$this -> first_access_usw = $first_access_usw;
		$this -> status_usw = $status_usw;
		$this -> admin_usw = $admin_usw;
	}
	
	/***********************************************
    * funcion getData
    * sets data by username
    ***********************************************/
	function getData(){
		if($this->serial_usw!=NULL){
				$sql = 	"SELECT serial_usw, username_usw, first_name_usw, last_name_usw, email_usw, password_usw, first_access_usw, status_usw, admin_usw
						FROM user_web
						WHERE serial_usw ='".$this->serial_usw."'";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_usw =$result->fields[0];
				$this -> username_usw = $result->fields[1];
				$this -> first_name_usw = $result->fields[2];
				$this -> last_name_usw = $result->fields[3];
				$this -> email_usw = $result->fields[4];
				$this -> password_usw = $result->fields[5];
				$this -> first_access_usw = $result->fields[6];
				$this -> status_usw = $result->fields[7];
				$this -> admin_usw = $result->fields[8];
				return true;
			}	
		}
		return false;		
	}
	
	/***********************************************
    * funcion getAllWebUsers
    * gets information from All Web Users
    ***********************************************/
    function getAllWebUsers(){
        $lista=NULL;
        $sql = 	"SELECT u.serial_usw, u.username_usw, u.first_name_usw,u.last_name_usw, u.email_usw FROM user_web u ";
        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arr=array();
            $cont=0;

            do{
                $arr[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arr;
    }
    
	/***********************************************
    * funcion removeUser
    * removes a Web Users
    ***********************************************/
    public static function removeUser($db, $serial_user){
        $sql = 	"DELETE FROM user_web WHERE serial_usw = ".$serial_user;
        return $db -> Execute($sql);
    }
	
	function getDataLogin(){
		if($this->username_usw!=NULL){
			$sql = 	"SELECT serial_usw, password_usw, first_name_usw, last_name_usw, admin_usw
					FROM user_web
					WHERE username_usw='".$this->username_usw."'";
			$result = $this->db->Execute($sql);
			
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_usw =$result->fields[0];
				$this -> password_usw = $result->fields[1];
				$this -> first_name_usw = $result->fields[2];
				$this -> last_name_usw = $result->fields[3];
				$this -> admin_usw = $result->fields[4];
				return true;
			}	
		}
		return false;		
	}
	
	/***********************************************
    * function insert
    * inserts a new register en DB 
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO user_web (
        		username_usw,
                first_name_usw,
                last_name_usw,
                email_usw,
                password_usw,
                first_access_usw,
                status_usw,
                admin_usw)
            VALUES(	'".$this->username_usw."',
	                '".$this->first_name_usw."',
	                '".$this->last_name_usw."',
	                '".$this->email_usw."',
	                '".md5($this->password_usw)."',
	                1,
	                'ACTIVE',
	                '".$this->admin_usw."')";
				
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
    
	/***********************************************
    * funcion updatePassword
    * Sets the new password for the user
    ***********************************************/
    function updatePassword($serial_usw) {
        $sql = "UPDATE user_web SET
                password_usw = '".$this->password_usw."'
                WHERE serial_usw = '".$serial_usw."'";
        //echo $sql;
        $result = $this->db->Execute($sql);
        return $result;
    }
	
 	/***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
    function update() {
        $sql = "UPDATE user_web SET 
        		first_name_usw	='".$this->first_name_usw."',
				last_name_usw	='".$this->last_name_usw."',
				email_usw		='".$this->email_usw."',
				admin_usw		='".$this->admin_usw."'";
        if($this->password_usw!=NULL)
            $sql .= ",password_usw	='".$this->password_usw."'";
        if($this->first_access_usw!=NULL)
            $sql .=",first_access_usw ='".$this->first_access_usw."'";
        $sql .=" WHERE serial_usw = '".$this->serial_usw."'";
        //echo $sql;
        $result = $this->db->Execute($sql);
        return $result;
    }
	
	/*
	* Generates a username based on its data
	*/
	function generateUsername($names,$last_names){
		setlocale(LC_CTYPE, 'es_ES');//Enables strtolower and strtoupper functions to convert acute characters;
		$existe_user = 0;
		if($names != '' && $last_names != ''){
			$a = array('&#225;', '&#233;', '&#237;', '&#243;', '&#250;', '&#241;', ' ');
			$b = array('a', 'e', 'i', 'o', 'u', 'n', '');
			
			$names = str_replace($a, $b,strtolower($names));
			$last_names = str_replace($a, $b,strtolower($last_names));
			do{
				$username = substr($names,0,2).substr($last_names,0,6);
				if($existe_user != 0){// If user already exists adds a number to the end
					$username .= $existe_user;
				}
				
				$sql = "SELECT username_usw FROM user_web where username_usw = '$username'";

				$result = $this->db->Execute($sql);

				if($result->fields[0]){
					$existe_user ++;//The user exists
				}else{
					return $username;
				}
			}while($existe_user != 0);
		}else{
			return false;
		}
	}
	
	function generatePassword($fSize,$linSize,$type=true){
		srand($this -> create_sequence());	
		$password="";
		$max_chars = round(rand($fSize,$linSize));
		$chars = array();
		for ($i="a"; $i<"z"; $i++) $chars[] = $i; 
		$chars[] = "z";
		for ($i=0; $i<$max_chars; $i++) {
		  $letra = round(rand(0, 1)); 
		  if ($type == false)
			$password .= $chars[round(rand(0, count($chars)-1))];
		  else
			$password .= round(rand(0, 9));
		}
		
		$password=md5($password);
		$password=substr($password,0,7);
		return $password;
	}
	
	function create_sequence() {
	   list($usec, $sec) = explode(' ', microtime());
	   return (float) $sec + ((float) $usec * 100000);
	}
	
	///GETTERS
	function getSerial_usw(){
		return $this->serial_usw;
	}
	function getUsername_usw(){
		return $this->username_usw;
	}
	function getPassword_usw(){
		return $this->password_usw;
	}
	function getFirstname_usw(){
		return $this->first_name_usw;
	}
	function getLastname_usw(){
		return $this->last_name_usw;
	}
	function getEmail_usw(){
		return $this->email_usw;
	}
	function getAdmin_usw(){
		return $this->admin_usw;
	}

	
	
	///SETTERS	
	function setSerial_usw($serial_usw){
		$this->serial_usw = $serial_usw;
	}
	function setUsername_usw($username_usw){
		$this->username_usw = $username_usw;
	}
	function setPassword_usw($password_usw){
		$this->password_usw = $password_usw;
	}
	function setFirstname_usw($first_name_usw){
		$this->first_name_usw = $first_name_usw;
	}
	function setLastname_usw($last_name_usw){
		$this->last_name_usw = $last_name_usw;
	}
	function setEmail_usw($email_usw){
		$this->email_usw = $email_usw;
	}
	function setAdmin_usw($admin_usw){
		$this->admin_usw = $admin_usw;
	}
}
?>