<?php
/*
    Document   : AssistanceGroup.class.php
    Created on : Apr 23, 2010, 10:40:33 AM
    Author     : H3Dur4k
    Description:
        manage assistanceGroup table.
*/
class AssistanceGroup{
	var $db;
	var $serial_asg;
	var $serial_cou;
	var $name_asg;

	function __construct($db, $serial_asg=NULL, $serial_cou = NULL, $name_asg=NULL) {
		$this -> db = $db;
		$this -> serial_asg = $serial_asg;
		$this -> serial_cou = $serial_cou;
		$this -> name_asg = $name_asg;
	}

	/***********************************************
    * funcion getData
    * sets data by serial_asg
    ***********************************************/
	function getData(){
		if($this->serial_asg!=NULL){
				$sql = 	"SELECT serial_asg,serial_cou, name_asg
						FROM assistance_group
						WHERE serial_asg ='".$this->serial_asg."'";
			//echo $sql;
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this -> serial_asg =$result->fields[0];
				$this -> serial_cou = $result->fields[1];
				$this -> name_asg = $result->fields[2];
				return true;
			}
		}
		return false;
	}

		/***********************************************
    * function insert
    * inserts a new register in the DB
    ***********************************************/
    function insert() {
        $sql = "
            INSERT INTO assistance_group (
        		serial_cou,
                name_asg)
            VALUES(	'".$this->serial_cou."',
            		'".$this->name_asg."')";
       	//echo $sql;
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }

 	/***********************************************
    * funcion update
    * updates a register in the DB
    ***********************************************/
    function update() {
        $sql = "
                UPDATE assistance_group SET";
        $sql.=" serial_cou			='".$this->serial_cou."',
        		name_asg				='".$this->name_asg."'";
        $sql .=" WHERE serial_asg 	='".$this->serial_asg."'";
        //echo $sql;
        $result = $this->db->Execute($sql);
        return $result;
    }

	/***********************************************
	* function validateName
	@Name: validateName
	@Description: Check if a group name already exists
	@Params:
         *       $serial_cou: country id
         *       $name: group name
	 *			 $groupId: serial of a group that should not be analized
	@Returns: true/false
	***********************************************/
	function validateName($serial_cou,$name,$groupId=false){
		$sql="SELECT 1 FROM assistance_group
				WHERE serial_cou=$serial_cou
				AND UPPER(name_asg) =UPPER('$name')";
		if($groupId)
			$sql.=" AND serial_asg != $groupId";
		$result = $this->db->getOne($sql);
		//echo $sql;
		//echo $result;
		if($result==1)
			return true;
		else
			return false;
	}
	/***********************************************
	* function getGroupsByCountry
	@Name: getGroupsByCountry
	@Description: gets a list of groups by country
	@Params:
         *       object atribute $this->serial_cou
	@Returns: Groups array/false
	***********************************************/
	function getGroupsByCountry(){
		$sql="SELECT asg.serial_asg,asg.name_asg
			FROM assistance_group asg
			WHERE asg.serial_cou={$this->serial_cou}";
		$result=$this->db->getAll($sql);
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	/***********************************************
	* function getUsersInGroup
	@Name: getUsersInGroup
	@Description: gets a list of users in a group
	@Params:
         *       object atribute $this->serial_asg
	@Returns: users array/false
	***********************************************/
	function getUsersInGroup(){
		$sql="SELECT agu.serial_usr,agu.status_agu,agu.type_agu
			  FROM assistance_groups_by_users agu 
			WHERE agu.serial_asg={$this->serial_asg}";
			//echo $sql;
		$result=$this->db->getAll($sql);
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	/***********************************************
	* function getActiveUsersInGroup
	@Name: getActiveUsersInGroup
	@Description: gets a list of active users in a group
	@Params:
         *       object atribute $this->serial_asg
	@Returns: users array/false
	***********************************************/
	function getActiveUsersInGroup(){
		$sql="SELECT u.serial_usr,IF((u.serial_mbc IS NULL),u.serial_dea,'planet') as serial_type,u.first_name_usr,u.last_name_usr
              FROM user u
			  JOIN assistance_groups_by_users agu ON agu.serial_usr=u.serial_usr
			WHERE agu.serial_asg={$this->serial_asg}
			AND agu.status_agu = 'ACTIVE'";
		$result=$this->db->getAll($sql);
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	/***********************************************
	* function getGroupsWithAlerts
	@Name: getGroupsWithAlerts
	@Description: gets a list of groups with pending alerts.
	@Params:
         *       none
	@Returns: users array/false
	***********************************************/
	function getGroupsWithAlerts(){
		$sql="SELECT DISTINCT asg.serial_asg,cou.code_cou,asg.name_asg
			FROM assistance_group asg
			JOIN country cou ON asg.serial_cou=cou.serial_cou
			JOIN alerts al ON al.remote_id=asg.serial_asg
			WHERE al.type_alt='FILE' AND al.status_alt='PENDING'";
		$result=$this->db->getAll($sql);
		if($result)
			return $result;
		else
			return false;
	}
	/***********************************************
	* function getGroupsToAssign
	@Name: getGroupsToAssign
	@Description: gets a list of groups available to be assigned alerts, except the same group.
	@Params:
         *       none
	@Returns: users array/false
	***********************************************/
	function getGroupsToAssign(){
		$sql="SELECT DISTINCT asg.serial_asg,cou.code_cou,asg.name_asg
			FROM assistance_group asg
			JOIN country cou ON asg.serial_cou=cou.serial_cou
			WHERE serial_asg !={$this->serial_asg}";
		$result=$this->db->getAll($sql);
		if($result)
			return $result;
		else
			return false;
	}
	/***********************************************
	* function getGroupPendingAlerts
	@Name: getGroupPendingAlerts
	@Description: Retrieves pending alerts by group.
	@Params:
         *       none
	@Returns: users array/false
	***********************************************/
	function getGroupPendingAlerts(){
		$sql="SELECT alt.serial_alt,u.first_name_usr, u.last_name_usr,alt.message_alt,in_date_alt,alert_time_alt
			FROM alerts alt
			JOIN user u ON u.serial_usr=alt.serial_usr
			WHERE alt.remote_id ={$this->serial_asg}
			AND alt.type_alt='FILE'
			AND alt.status_alt='PENDING'";
		$result=$this->db->getAll($sql);
		//echo $sql;
		if($result)
			return $result;
		else
			return false;
	}
	
	public static function getFilteredGroups($db, $serial_cou){
		$sql = "SELECT serial_asg, name_asg, serial_cou
				FROM assistance_group";
		
		if($serial_cou <> 1){
			$sql .= " WHERE serial_cou = $serial_cou";
		}
		
		$sql .= " ORDER BY name_asg";
		
		$result = $db -> getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	///GETTERS
	function getSerial_asg(){
		return $this->serial_asg;
	}
	function getSerial_cou(){
		return $this->serial_cou;
	}
	function getName_asg(){
		return $this->name_asg;
	}

	///SETTERS
	function setSerial_asg($serial_asg){
		$this->serial_asg = $serial_asg;
	}
	function setSerial_cou($serial_cou){
		$this->serial_cou = $serial_cou;
	}

	function setName_asg($name_asg){
		$this->name_asg = $name_asg;
	}
}
?>
