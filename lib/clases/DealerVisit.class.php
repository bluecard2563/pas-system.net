<?php
/*
File: DealerVisit.class.php
Author: David Bergmann
Creation Date: 09/03/2010
Last Modified: 26/03/2010
Modified By: David Bergmann
*/

class DealerVisit {
	var $db;
	var $serial_vis;
	var $serial_dea;
	var $serial_usr;
	var $status_vis;
	var $title_vis;
	var $start_vis;
	var $end_vis;
	var $allday_vis;
	var $type_vis;

	function __construct($db, $serial_vis = NULL, $serial_dea = NULL, $serial_usr = NULL,
                             $status_vis = NULL,$title_vis = NULL, $start_vis = NULL,
                             $end_vis = NULL,$allday_vis = NULL, $type_vis = NULL){
		$this -> db = $db;
		$this -> serial_vis = $serial_vis;
		$this -> serial_dea = $serial_dea;
		$this -> serial_usr = $serial_usr;
		$this -> status_vis = $status_vis;
		$this -> title_vis = $title_vis;
		$this -> start_vis = $start_vis;
		$this -> end_vis = $end_vis;
		$this -> allday_vis = $allday_vis;
		$this -> type_vis = $type_vis;
	}

	/***********************************************
	* function getData
	* gets data by serial_vis
	***********************************************/
	function getData(){
		if($this->serial_vis!=NULL){
			$sql = 	"SELECT serial_vis, serial_dea, serial_usr, status_vis, title_vis
							start_vis, end_vis, allday_vis, type_vis
					 FROM visits
					 WHERE serial_vis='".$this->serial_vis."'";
			//echo $sql." ";
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
				$this->serial_vis=$result->fields[0];
				$this->serial_dea=$result->fields[1];
				$this->serial_usr=$result->fields[2];
				$this->status_vis=$result->fields[3];
				$this->title_vis=$result->fields[4];
				$this->start_vis=$result->fields[5];
				$this->end_vis=$result->fields[6];
				$this->allday_vis=$result->fields[7];
				$this->type_vis=$result->fields[8];
				return true;
			}
			else
				return false;

		}else
			return false;
	}

	/***********************************************
	* function insert
	* inserts a new register in DB
	***********************************************/
	function insert(){
		$sql="INSERT INTO visits (
						  serial_vis,
						  serial_dea,
						  serial_usr,
						  status_vis,
						  title_vis,
						  start_vis,
						  end_vis,
						  allday_vis,
						  type_vis
				  )
				  VALUES('".$this->serial_vis."',
						 '".$this->serial_dea."',
						 '".$this->serial_usr."',
						 '".$this->status_vis."',
						 '".$this->title_vis."',
						 STR_TO_DATE('".$this->start_vis."','%Y-%m-%d  %H:%i'),
						 STR_TO_DATE('".$this->end_vis."','%Y-%m-%d  %H:%i'),";
			  if($this->allday_vis=="true"){
				  $sql.="'YES',";
			  }else{
				  $sql.="'NO',";
			  }
				  $sql.="'".$this->type_vis."');";

		$result = $this->db->Execute($sql);

		if($result){
            return TRUE;
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}

    /***********************************************
    * funcion update
    * updates a register in DB
    ***********************************************/
	function update(){
		$sql="UPDATE visits
			  SET status_vis='".$this->status_vis."',
				  start_vis='".$this->start_vis."',
				  end_vis='".$this->end_vis."',";
		if($this->allday_vis=="true"){
		   $sql.="allday_vis='YES',";
		}else{
		   $sql.="allday_vis='NO',";
		}
	   $sql.="type_vis='".$this->type_vis."'
			  WHERE serial_vis='".$this->serial_vis."'";
		//echo $sql;
		$result = $this->db->Execute($sql);
		if ($result==true)
			return true;
		else
			return false;
	}

	/***********************************************
	* funcion delete
	* deletes a register in DB
	***********************************************/
	function delete(){
		$sql="DELETE FROM visits
			  WHERE serial_vis='".$this->serial_vis."'";

		$result = $this->db->Execute($sql);
		//echo $sql." ";
		if ($result==true)
			return true;
		else
			return false;
	}

	/*************************************************************
	* funcion deleteEventsByUser
	* deletes al register in DB acording to a specific serial_usr
	*************************************************************/
	function getVisitsByMonth($month,$year){
		$sql="SELECT serial_vis as id, serial_dea as serial, serial_usr as user, title_vis as title,
					 DATE_FORMAT(start_vis,'%Y-%m-%d %H:%i') as start,DATE_FORMAT(end_vis,'%Y-%m-%d %H:%i') as end,
					 status_vis as status, allday_vis as allDay, type_vis as type
			  FROM visits
			  WHERE serial_usr='".$this->serial_usr."'
			  AND allday_vis <> 'YES'
			  AND MONTH(start_vis) = $month
			  AND YEAR(start_vis) = $year";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;

		if($result){
				return $result;
		}else{
				return NULL;
		}
	}

	/*************************************************************
	* funcion deleteEventsByUser
	* deletes al register in DB acording to a specific serial_usr
	*************************************************************/
	function deleteEventsByUser($month=NULL,$year=NULL){
		$sql="DELETE FROM visits
			  WHERE serial_usr='".$this->serial_usr."'
			  AND allday_vis<>'YES'";
		if($month!=NULL && $year!=NULL) {
			$sql .= " AND MONTH(start_vis) = $month
					  AND YEAR(start_vis) = $year";
		}
		//die($sql);
		$result = $this->db->Execute($sql);
		if ($result==true)
			return true;
		else
			return false;
	}

	/***********************************************
	* funcion getCalendar
	* gets all the Events for a specific User
	***********************************************/
	function getCalendar($serial_usr){
		$sql = 	"SELECT serial_vis as id, serial_dea as serial, serial_usr as user, title_vis as title,
						DATE_FORMAT(start_vis,'%Y-%m-%d %H:%i') as start,DATE_FORMAT(end_vis,'%Y-%m-%d %H:%i') as end,
						status_vis as status, allday_vis as allDay, type_vis as type
				 FROM visits
				 WHERE serial_usr= '".$serial_usr."'
				 ORDER BY start_vis";
				 //echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

	/***********************************************
	* funcion eventExists
	* verifies if an Event exists or not
	***********************************************/
	function eventExists($serial_vis){
		$sql = 	"SELECT serial_vis
						 FROM visits
						 WHERE serial_vis='".$serial_vis."'";

		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return false;
		}
	}

	/***********************************************
	* funcion getStatusList
	* gets all the Visits status in DB.
	***********************************************/
	function getStatusList(){
		$sql = "SHOW COLUMNS FROM visits LIKE 'status_vis'";
		$result = $this->db -> Execute($sql);

		$status=$result->fields[1];
		$status = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$status));
		return $status;
	}

	/***********************************************
	* funcion getTypeList
	* gets all the Types status in DB.
	***********************************************/
	function getTypeList(){
		$sql = "SHOW COLUMNS FROM visits LIKE 'type_vis'";
		$result = $this->db -> Execute($sql);

		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

	/*******************************************************************
	* funcion getMonthVisitsPerDealer
	* gets the number of visits from a dealer in a specific month in DB.
	*******************************************************************/
	function getMonthVisitsPerDealer(){
		$sql = "SELECT COUNT(*)
				FROM visits
				WHERE serial_dea = ".$this->serial_dea."
				AND MONTH(start_vis) = MONTH(STR_TO_DATE('".$this->start_vis."','%Y-%m-%d  %H:%i'))
				AND YEAR(start_vis) = YEAR(STR_TO_DATE('".$this->start_vis."','%Y-%m-%d  %H:%i'))";
			//echo $sql." ";
		$result = $this->db -> Execute($sql);
		return $result->fields[0];
	}

	/***********************************************************
	* funcion getDealerVisitsReport
	* returns a list of visits acording to a specific parameter.
	***********************************************************/
	function getDealerVisitsReport($params){
		$extraParams=0;
		$sql = "SELECT CONCAT(CONCAT(u.first_name_usr, ' '),u.last_name_usr) as name_usr,
							  d.name_dea, DATE_FORMAT(v.start_vis,'%Y-%m-%d') as date_vis, v.status_vis,
							  v.type_vis, obv.host_obv, obv.comments_obv, obv.strategy_obv
				FROM visits v
				LEFT JOIN observations_by_visit obv ON obv.serial_vis = v.serial_vis
				JOIN user u ON u.serial_usr = v.serial_usr
				JOIN dealer d ON d.serial_dea = v.serial_dea";
		if($params) {
			foreach($params as $k=>$p){
				if($extraParams==0) {
					$sql .= " WHERE $k='$p'";
					$extraParams++;
				}
				else {
					if($k=="v.start_vis" and $params['v.end_vis']) {
						$sql .= " AND $k BETWEEN STR_TO_DATE('$p','%d/%m/%Y') AND STR_TO_DATE('".$params['v.end_vis']."','%d/%m/%Y')";
					} elseif ($k=="v.start_vis") {
						$sql .= " AND $k >= STR_TO_DATE('$p','%d/%m/%Y')";
					} elseif($k!="v.end_vis") {
						$sql .= " AND $k='$p'";
					}
				}
			}
		}
		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
				$arreglo=array();
				$cont=0;

				do{
						$arreglo[$cont]=$result->GetRowAssoc(false);
						$result->MoveNext();
						$cont++;
				}while($cont<$num);
		}

		return $arreglo;
	}

	public static function getVisitsForDealerChartReport($db, $serial_dea){
		$sql="SELECT DATE_FORMAT(v.start_vis, '%d/%m/%Y') AS 'start_vis', 
					 v.type_vis, obv.comments_obv, CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'name_usr'
			  FROM visits v
			  JOIN user u ON u.serial_usr=v.serial_usr
			  JOIN observations_by_visit obv ON obv.serial_vis=v.serial_vis
			  WHERE v.serial_dea=".$serial_dea;

		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
				$arreglo=array();
				$cont=0;

				do{
						$arreglo[$cont]=$result->GetRowAssoc(false);
						$result->MoveNext();
						$cont++;
				}while($cont<$num);
		}

		return $arreglo;
	}

        
	///GETTERS
	function getSerial_vis(){
		return $this->serial_vis;
	}
	function getSerial_dea(){
		return $this->serial_dea;
	}
	function getSerial_usr(){
		return $this->serial_usr;
	}
	function getStatus_vis(){
		return $this->status_vis;
	}
	function getTitle_vis(){
		return $this->title_vis;
	}
	function getStart_vis(){
		return $this->start_vis;
	}
	function getEnd_vis(){
		return $this->end_vis;
	}
	function getAllday_vis(){
		return $this->allday_vis;
	}
	function getType_vis(){
		return $this->type_vis;
	}

	///SETTERS	
	function setSerial_vis($serial_vis){
		$this->serial_vis = $serial_vis;
	}
	function setSerial_dea($serial_dea){
		$this->serial_dea = $serial_dea;
	}
	function setSerial_usr($serial_usr){
		$this->serial_usr = $serial_usr;
	}
	function setStatus_vis($status_vis){
		$this->status_vis = $status_vis;
	}
	function setTitle_vis($serial_vis){
		$this->title_vis = $serial_vis;
	}
	function setStart_vis($start_vis){
		$this->start_vis = $start_vis;
	}
	function setEnd_vis($end_vis){
		$this->end_vis = $end_vis;
	}
	function setAllday_vis($allday_vis){
		$this->allday_vis = $allday_vis;
	}
	function setType_vis($type_vis){
		$this->type_vis = $type_vis;
	}
}
?>
