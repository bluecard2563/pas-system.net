<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MyPdf
 *
 * @author luiseduardosalvador
 */
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        if ($this->header_xobjid === false) {
			// start a new XObject Template
			$this->header_xobjid = $this->startTemplate($this->w, $this->tMargin);
			$headerfont = $this->getHeaderFont();
			$headerdata = $this->getHeaderData();
			$this->y = $this->header_margin;
			if ($this->rtl) {
				$this->x = $this->w - $this->original_rMargin;
			} else {
				$this->x = $this->original_lMargin;
			}
			if (($headerdata['logo']) AND ($headerdata['logo'] != K_BLANK_IMAGE)) {
                $this->SetAlpha(0.5);
				$imgtype = TCPDF_IMAGES::getImageFileType(LOCAL_PATH_IMAGES.$headerdata['logo']);
                $img_x = A4_PAGE_WIDTH - $this->original_rMargin - ($headerdata['logo_width']);
				if (($imgtype == 'eps') OR ($imgtype == 'ai')) {
					$this->ImageEps(LOCAL_PATH_IMAGES.$headerdata['logo'], $img_x, '', $headerdata['logo_width']);
				} elseif ($imgtype == 'svg') {
					$this->ImageSVG(LOCAL_PATH_IMAGES.$headerdata['logo'], $img_x, '', $headerdata['logo_width']);
				} else {
					$this->Image(LOCAL_PATH_IMAGES.$headerdata['logo'], $img_x, '', $headerdata['logo_width']);
				}
				$imgy = $this->getImageRBY();
			} else {
				$imgy = $this->y;
			}
			$cell_height = $this->getCellHeight($headerfont[2] / $this->k);
			// set starting margin for text data cell
			if ($this->getRTL()) {
				//$header_x = $this->original_rMargin + ($headerdata['logo_width'] * 1.1);
                $header_x = $this->original_rMargin;
			} else {
				//$header_x = $this->original_lMargin + ($headerdata['logo_width'] * 1.1);
                $header_x = $this->original_lMargin;
			}
			$cw = $this->w - $this->original_lMargin - $this->original_rMargin - ($headerdata['logo_width'] * 1.1);
			$this->SetTextColorArray($this->header_text_color);
			// header title
			$this->SetFont($headerfont[0], 'B', $headerfont[2] + 1);
			$this->SetX($header_x);
			$this->Cell($cw, $cell_height, $headerdata['title'], 0, 1, '', 0, '', 0);
			// header string
			//$this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);
			$this->SetX($header_x);
			$this->MultiCell($cw, $cell_height, $headerdata['string'], 0, '', 0, 1, '', '', true, 0, false, true, 0, 'T', false);
			// print an ending header line
//			$this->SetLineStyle(array('width' => 0.85 / $this->k, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $headerdata['line_color']));
//			$this->SetY((2.835 / $this->k) + max($imgy, $this->y));
//			if ($this->rtl) {
//				$this->SetX($this->original_rMargin);
//			} else {
//				$this->SetX($this->original_lMargin);
//			}
//			$this->Cell(($this->w - $this->original_lMargin - $this->original_rMargin), 0, '', 'T', 0, 'C');
			$this->endTemplate();
		}
		// print header template
		$x = 0;
		$dx = 0;
		if (!$this->header_xobj_autoreset AND $this->booklet AND (($this->page % 2) == 0)) {
			// adjust margins for booklet mode
			$dx = ($this->original_lMargin - $this->original_rMargin);
		}
		if ($this->rtl) {
			$x = $this->w + $dx;
		} else {
			$x = 0 + $dx;
		}
		$this->printTemplate($this->header_xobjid, $x, 0, 0, 0, '', '', false);
		if ($this->header_xobj_autoreset) {
			// reset header xobject template at each page
			$this->header_xobjid = false;
		}
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-20);
        
        $this->SetAlpha(0.5);
        $img_x = A4_PAGE_WIDTH - $this->original_rMargin - 15;
        $this->Image(LOCAL_PATH_IMAGES.'logoblue_thumb.jpg', $img_x, '', '15');
        
        $cw = $this->w - $this->original_lMargin - $this->original_rMargin - (15 * 1.1);
        $this->Cell($cw, 15, 'Aprobado según Resolución No. 0000000338 del 21 de Septiembre del 2012', 0, 0, '', 0, '', 0);
        
        // Page number
        $this->Cell(0, 15, $this->getAliasNumPage().' de '.$this->getAliasNbPages().'     ', 0, 0, 'R', 0, '', 0, false, 'T', 'M');
    }
}
