<?php
/*
File: Manager.class.php
Author: Santiago Benítez
Creation Date: 11/02/2010 10:02
Last Modified: 04/10/2010 10:02
Modified By: David Bergmann
*/
class Manager{				
	var $db;
	var $serial_man; 
	var $serial_cit;
	var $man_serial_man;
	var $name_man; 
	var $document_man; 
	var $address_man; 
	var $phone_man; 
	var $contract_date_man; 
	var $contact_man; 
	var $contact_phone_man; 
	var $contact_email_man;
	var $type_man;
	var $sales_only_man;
	var $aux_data;
	
	function __construct($db, $serial_man=NULL, $serial_cit=NULL, $man_serial_man=NULL, $name_man=NULL,
						 $document_man=NULL, $address_man=NULL, $phone_man=NULL, $contract_date_man=NULL,
						 $contact_man=NULL, $contact_phone_man=NULL, $contact_email_man=NULL, $type_man=NULL,
						 $sales_only_man=NULL){
		
		$this -> db = $db;
		$this -> serial_man = $serial_man;
		$this -> serial_cit = $serial_cit;
		$this -> man_serial_man = $man_serial_man;
		$this -> name_man = $name_man;
		$this -> document_man = $document_man;
		$this -> address_man = $address_man;
		$this -> phone_man = $phone_man;
		$this -> contract_date_man = $contract_date_man;
		$this -> contact_man = $contact_man;
		$this -> contact_phone_man = $contact_phone_man;
		$this -> contact_email_man = $contact_email_man;
		$this -> type_man = $type_man;
		$this -> sales_only_man = $sales_only_man;
	}
	
	/** 
	@Name: getData
	@Description: Retrieves the data of a Manager object.
	@Params: The ID of the object
	@Returns: TRUE if O.K. / FALSE on errors
	**/
	function getData(){
		if($this->serial_man!=NULL){
			$sql = 	"SELECT d.serial_man, d.serial_cit, d.man_serial_man, d.name_man, d.document_man, d.address_man,
                            d.phone_man, DATE_FORMAT(d.contract_date_man,'%d/%m/%Y'), d.contact_man, d.contact_phone_man,
                            d.contact_email_man, d.type_man, d.sales_only_man, cou.serial_cou, z.serial_zon, 
							CONCAT(c.name_cit, ', ', cou.name_cou) AS 'geographic_info'
					FROM manager d
					JOIN city c ON c.serial_cit=d.serial_cit
					JOIN country cou ON cou.serial_cou=c.serial_cou
					JOIN zone z ON z.serial_zon=cou.serial_zon
					WHERE serial_man='".$this->serial_man."'";
                        //die($sql);
			$result = $this->db->Execute($sql);
			if ($result === false) return false;

			if($result->fields[0]){
		
				$this -> serial_man=$result->fields[0]; 
				$this -> serial_cit=$result->fields[1]; 
				$this -> man_serial_man=$result->fields[2]; 
				$this -> name_man=$result->fields[3]; 
				$this -> document_man=$result->fields[4]; 
				$this -> address_man=$result->fields[5]; 
				$this -> phone_man=$result->fields[6]; 
				$this -> contract_date_man=$result->fields[7]; 
				$this -> contact_man=$result->fields[8]; 
				$this -> contact_phone_man=$result->fields[9]; 
				$this -> contact_email_man=$result->fields[10]; 
				$this -> type_man=$result->fields[11];
				$this -> sales_only_man=$result->fields[12];
				$this -> aux_data['serial_cou']=$result->fields[13];
				$this -> aux_data['serial_zon']=$result->fields[14];
				$this -> aux_data['geographic_info']=$result->fields[15];
			
				return true;
			}else
				return false;
		}else
			return false;		
	}

        
	
	/** 
	@Name: getManagers
	@Description: Gets all managers with a specific criteria
	@Returns: Managers array
	**/
	function getManagers($params=NULL){
		$sql = "SELECT m.serial_man, m.serial_cit, m.man_serial_man, m.name_man, m.document_man,
					   m.address_man, m.phone_man, m.contract_date_man, m.contact_man, m.contact_phone_man,
					   m.contact_email_man, m.type_man, m.sales_only_man
				FROM manager m
				JOIN city c ON c.serial_cit = m.serial_cit
				JOIN country cou ON cou.serial_cou = c.serial_cou";
		if($params!=NULL){
			$iteration = 0;
			foreach ($params as $k=>$p) {
				if($iteration == 0) {
					$sql .= " WHERE $k = '$p'";
					$iteration++;
				} else {
					$sql .= " AND $k = '$p'";
				}
			}
		}
		//die($sql);
		$result = $this->db->getAll($sql);
		
		if($result){
            return $result;
        }else{
            return false;
        }
	}	
	
	/** 
	@Name: insert
	@Description: Inserts a register in DB
	@Params: N/A
	@Returns: serial_man if OK. / FALSE on error
	**/
    function insert() {
        $sql = "INSERT INTO manager (
						serial_man,
						serial_cit,
						man_serial_man,
						name_man,
						document_man,
						address_man,
						phone_man,
						contract_date_man,
						contact_man,
						contact_phone_man,
						contact_email_man,
						type_man,
						sales_only_man)
				VALUES (
						NULL,
						'$this->serial_cit',";
				if($this->man_serial_man=='NULL'){
					$sql.="NULL,";
				}else{
					$sql.="'$this->man_serial_man',";
				}
				 $sql.="'$this->name_man',
						'$this->document_man',
						'$this->address_man',
						'$this->phone_man',
						STR_TO_DATE('$this->contract_date_man','%d/%m/%Y'),
						'$this->contact_man',
						'$this->contact_phone_man',
						'$this->contact_email_man',
						'$this->type_man',
						'$this->sales_only_man')";
        $result = $this->db->Execute($sql);
        
    	if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
    }
	
	/** 
	@Name: update
	@Description: Updates a register in DB
	@Params: N/A
	@Returns: TRUE if OK. / FALSE on error
	**/
    function update() {
        $sql = "UPDATE manager SET
					serial_cit='$this->serial_cit',";
			$sql.= "man_serial_man=";
		if($this->man_serial_man=='NULL' || $this->man_serial_man==''){
			$sql.= "NULL,";
		}else{
			$sql.="'$this->man_serial_man',";
		}
			$sql.= "name_man='$this->name_man',
					document_man='$this->document_man',
					address_man='$this->address_man',
					phone_man='$this->phone_man',
					contract_date_man=STR_TO_DATE('$this->contract_date_man','%d/%m/%Y'),
					contact_man='$this->contact_man',
					contact_phone_man='$this->contact_phone_man',
					contact_email_man='$this->contact_email_man',
					type_man='$this->type_man',
					sales_only_man='$this->sales_only_man'
				WHERE serial_man = '$this->serial_man'";
                                //die($sql);
        $result = $this->db->Execute($sql);
        if ($result === false)return false;
		
        if($result==true)
            return true;
        else
            return false;
    }

    /***********************************************
    * funcion managerNameExists
    * verifies if a manager's name exists or not
    ***********************************************/
	function managerNameExists($name_man=NULL, $serial_man=NULL){
		$sql = 	"SELECT serial_man
				 FROM manager
				 WHERE LOWER(name_man) = _utf8'".utf8_encode($name_man)."' collate utf8_bin";
		if($serial_man!=NULL){
			$sql.=" AND serial_man <> ".$serial_man;
		}
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/***********************************************
    * funcion managerDocumentExists
    * verifies if a manager's document exists or not
    ***********************************************/
	function managerDocumentExists($document_man, $serial_man=NULL){
		$sql = 	"SELECT serial_man
				 FROM manager
				 WHERE LOWER(document_man)= _utf8'".utf8_encode($document_man)."' collate utf8_bin";
		if($serial_man!=NULL){
			$sql.=" AND serial_man <> ".$serial_man;
		}
                //die($sql);
		$result = $this->db -> Execute($sql);

		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

		/***********************************************
    * function managerDocumentByCountryExists
    * verifies if a manager's document exists or not, depending on a country
    ***********************************************/
	function managerDocumentByCountryExists($serial_cou, $document, $serial_man=NULL){
		$sql = 	" SELECT man.serial_man
                  FROM manager man
				  JOIN city cit ON cit.serial_cit=man.serial_cit
				  JOIN country cou ON cou.serial_cou=cit.serial_cou AND cou.serial_cou=".$serial_cou."
                  WHERE man.document_man = '".$document."'";

		if($serial_man!=NULL && $serial_man!=''){
			$sql.=" AND man.serial_man <> ".$serial_man;
		}
		//die($sql);
		$result = $this->db -> Execute($sql);
		if($result->fields[0]){
			return true;
		}else{
			return false;
		}
	}

	/** 
	@Name: getManagersAutocompleter
	@Description: Returns an array of managers.
	@Params: Manager name or pattern.
	@Returns: Manager array
	**/
	function getManagersAutocompleter($name_man){
		$sql = "SELECT serial_man, name_man, document_man
			FROM manager
			WHERE (LOWER(name_man) LIKE _utf8'%".utf8_encode($name_man)."%' collate utf8_bin
			OR LOWER(document_man) LIKE _utf8'%".utf8_encode($name_man)."%' collate utf8_bin)
                        AND man_serial_man <> 'NULL'
			LIMIT 10";
                //die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}

	/**
	@Name: getCountriesWithManager
	@Description: Returns an array of countries that have at least 1 manager
	@Params: $db
	@Returns: Country array
	**/
	public static function getCountriesWithManager($db){
		$sql =" SELECT DISTINCT cou.serial_cou, cou.name_cou
				FROM manager_by_country mbc
				JOIN country cou ON mbc.serial_cou=cou.serial_cou
				WHERE mbc.serial_cou <> 1
				AND mbc.status_mbc = 'ACTIVE'
				ORDER BY cou.name_cou";
		//die($sql);
		$result = $db->getAll($sql);

		if($result){
            return $result;
        }else{
            return false;
        }
	}

	/**
	@Name: getBillingToManagerReport
	@Description: Returns an array of countries that have at least 1 manager
	@Params: $db
	@Returns: Country array
	**/
	public static function getBillingToManagerReport($db, $serial_man, $order_by, $serial_btm = NULL, $date_to = NULL){
		//Gets date
		$billingToManager=new BillingToManager($db);
		$date = $billingToManager->getLastBillToManager($serial_man,$serial_btm);

		if($serial_btm) {
			$billingToManager = new BillingToManager($db, $serial_btm);
			$billingToManager->getData();

			$date_btm = $billingToManager->getDate_btm();

			if($date) {
				$dates_restriction = " AND	(
												(	
													sal.in_date_sal > STR_TO_DATE('$date','%d/%m/%Y')
													AND sal.in_date_sal < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)
												) OR (
													 sl.date_slg >  STR_TO_DATE('$date','%d/%m/%Y')
													AND sl.date_slg < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)
												)
											)";
			} else {
				$dates_restriction = " AND (
												sal.in_date_sal < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)
											OR 
												sl.date_slg < DATE_ADD(STR_TO_DATE('$date_btm','%d/%m/%Y'), INTERVAL 1 DAY)
											)";
			}
		} else {
			if($date) {
				$dates_restriction = " AND (
												sal.in_date_sal > STR_TO_DATE('$date','%d/%m/%Y')
											 OR
												sl.date_slg > STR_TO_DATE('$date','%d/%m/%Y')
											)";
			}
		}

		if($date_to){
			$dates_restriction .= " AND (
											sal.in_date_sal < DATE_ADD(STR_TO_DATE('$date_to','%d/%m/%Y'), INTERVAL 1 DAY)
										 OR
											sl.date_slg < DATE_ADD(STR_TO_DATE('$date_to','%d/%m/%Y'), INTERVAL 1 DAY)
										)";
		}

		//Gets to-collect sales
		$sql = "SELECT sal.serial_sal, sal.card_number_sal, DATE_FORMAT(sal.emission_date_sal,'%d/%m/%Y') as in_date_sal, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as name_cus,
					   pbl.name_pbl, DATE_FORMAT(sal.begin_date_sal,'%d/%m/%Y') as begin_date_sal, DATE_FORMAT(sal.end_date_sal,'%d/%m/%Y') as end_date_sal,
					   sal.days_sal, sal.total_sal, dea.name_dea, CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as name_usr,
					   sal.international_fee_amount, sal.international_fee_status, sal.status_sal
				FROM manager man
				JOIN manager_by_country mbc ON mbc.serial_man = man.serial_man
				JOIN dealer dea ON dea.serial_mbc = mbc.serial_mbc
				JOIN counter cnt ON cnt.serial_dea = dea.serial_dea
				JOIN user usr ON usr.serial_usr = cnt.serial_usr
				JOIN sales sal ON sal.serial_cnt = cnt.serial_cnt
				LEFT JOIN sales_log sl ON sl.serial_sal=sal.serial_sal AND type_slg = 'REFUNDED' AND status_slg = 'AUTHORIZED'
				JOIN customer cus ON cus.serial_cus = sal.serial_cus
				JOIN product_by_dealer pbd ON pbd.serial_pbd = sal.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = ".$_SESSION['serial_lang']."
				WHERE man.serial_man = $serial_man
				AND sal.international_fee_used = 'NO'
				AND (sal.international_fee_status = 'TO_COLLECT' OR sal.international_fee_status = 'TO_REFUND')
				$dates_restriction
				ORDER BY $order_by";
		//echo '<pre>'.$sql.'</pre>';	die;

		$result = $db->getAll($sql);

		if($result){
			return $result;
		} else {
			return false;
		}
	}
	
	/** 
	@Name: getAllTypes
	@Description: Gets all manager types in DB.
	@Params: N/A
	@Returns: Status array
	**/
	function getAllTypes(){
		$sql = "SHOW COLUMNS FROM manager LIKE 'type_man'";
		$result = $this->db -> Execute($sql);
		
		$type=$result->fields[1];
		$type = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2",$type));
		return $type;
	}

    public function getManagerDataBySerial($db,$serial_man){
        $sql =" SELECT *
				FROM manager
				WHERE serial_man = $serial_man";
        $result = $db->getAll($sql);

        if($result){
            return $result[0];
        }else{
            return false;
        }
    }

    public function getManagerByManagerByCountryId($managerByCountryId)
    {
        $sql = "
            SELECT man.serial_man, mbc.serial_mbc, man.serial_cit, man.name_man
            FROM manager man
            INNER JOIN manager_by_country mbc on mbc.serial_man = man.serial_man
            WHERE mbc.serial_mbc = $managerByCountryId
        ";

        $result = $this->db->getAll($sql);
        if ($result) {
            return $result[0];
        } else {
            return false;
        }
    }

	//Getters
	function getSerial_man(){ 
		return $this->serial_man; 
	} 
	function getSerial_cit(){ 
		return $this->serial_cit; 
	} 
	function getMan_serial_man(){ 
		return $this->man_serial_man; 
	} 
	function getDocument_man(){ 
		return $this->document_man; 
	} 
	function getName_man(){ 
		return $this->name_man; 
	} 
	function getAddress_man(){ 
		return $this->address_man; 
	} 
	function getPhone_man(){ 
		return $this->phone_man; 
	} 
	function getContract_date_man(){ 
		return $this->contract_date_man; 
	} 
	function getContact_man(){ 
		return $this->contact_man; 
	} 
	function getContact_phone_man(){ 
		return $this->contact_phone_man; 
	} 
	function getContact_email_man(){ 
		return $this->contact_email_man; 
	}
	function getType_man(){ 
		return $this->type_man; 
	}
	function getSales_only_man(){
		return $this->sales_only_man;
	}
	function getAuxData_man(){ 
		return $this->aux_data; 
	}

	
	//Setters
	function setSerial_man($serial_man){ 
		$this->serial_man=$serial_man; 
	} 
	function setSerial_cit($serial_cit){ 
		$this->serial_cit=$serial_cit; 
	} 
	function setDocument_man($document_man){ 
		$this->document_man=$document_man; 
	} 
	function setName_man($name_man){ 
		$this->name_man=$name_man; 
	} 
	function setAddress_man($address_man){ 
		$this->address_man=$address_man; 
	} 
	function setPhone_man($phone_man){ 
		$this->phone_man=$phone_man; 
	} 
	function setContract_date_man($contract_date_man){ 
		$this->contract_date_man=$contract_date_man; 
	} 
	function setContact_man($contact_man){ 
		$this->contact_man=$contact_man; 
	} 
	function setContact_phone_man($contact_phone_man){ 
		$this->contact_phone_man=$contact_phone_man; 
	} 
	function setContact_email_man($contact_email_man){ 
		$this->contact_email_man=$contact_email_man; 
	}  
	function setType_man($type_man){ 
		$this->type_man=$type_man; 
	}
	function setSales_only_man($sales_only_man){
		$this->sales_only_man=$sales_only_man;
	}
	function setMan_serial_man($man_serial_man){ 
		$this->man_serial_man=$man_serial_man; 
	} 
}
?>