<?php
/*
 * File: AppliedProductsForCustomerPromo
 * Author: Patricio Astudillo
 * Creation Date: Feb 14, 2011, 4:22:45 PM
 * Description:
 * Modified By:
 * Last Modified:
 */
class AppliedProductsForCustomerPromo{
	var $db;
	var $serial_pcp;
	var $serial_pxc;
	var $serial_ccp;
	var $serial_cou;

	function __construct($db, $serial_pcp=NULL, $serial_pxc=NULL, $serial_ccp=NULL, $serial_cou=NULL){
		$this -> db = $db;
		$this -> serial_pcp = $serial_pcp;
		$this -> serial_pxc = $serial_pxc;
		$this -> serial_ccp = $serial_ccp;
		$this -> status_cou = $serial_cou;
	}

	function insert(){
		$sql = "INSERT INTO applied_products_by_customer_promo (
					`serial_pcp`,
					`serial_pxc`,
					`serial_ccp`,
					`serial_cou`
				) values (
					NULL,
					{$this -> serial_pxc},
					{$this -> serial_ccp},
					{$this -> serial_cou})";

		$result = $this -> db -> Execute($sql);

		if($result){
			return $this->db->insert_ID();
		}else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
			return false;
		}
	}
	
	public static function deleteAllMyProducts($db, $customerPromoSerial){
		$sql = "DELETE FROM applied_products_by_customer_promo
				WHERE serial_ccp = $customerPromoSerial";

		$result = $db -> Execute($sql);
		return true;
	}

	public static function getAllMyProducts($db, $serial_ccp){
		$sql = "SELECT serial_pcp, serial_pxc, serial_ccp, serial_cou
				FROM applied_products_by_customer_promo
				WHERE serial_ccp = $serial_ccp";

		$result = $db -> getAll($sql);

		if($result)
			return $result;
		else
			return FALSE;
	}

	public static function getSerialAllMyProducts($db, $serial_ccp){
		$sql = "SELECT serial_pxc
				FROM applied_products_by_customer_promo
				WHERE serial_ccp = $serial_ccp";

		$result = $db -> getAll($sql);

		if($result)
			return $result;
		else
			return FALSE;
	}

	public function getSerial_pcp() {
	 return $this->serial_pcp;
	}

	public function setSerial_pcp($serial_pcp) {
	 $this->serial_pcp = $serial_pcp;
	}

	public function getSerial_pxc() {
	 return $this->serial_pxc;
	}

	public function setSerial_pxc($serial_pxc) {
	 $this->serial_pxc = $serial_pxc;
	}

	public function getSerial_ccp() {
	 return $this->serial_ccp;
	}

	public function setSerial_ccp($serial_ccp) {
	 $this->serial_ccp = $serial_ccp;
	}

	public function getSerial_cou() {
	 return $this->serial_cou;
	}

	public function setSerial_cou($serial_cou) {
	 $this->serial_cou = $serial_cou;
	}
}
?>
