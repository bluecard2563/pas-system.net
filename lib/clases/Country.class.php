<?php
/*
File: Country.class.php
Author: Pablo Puente
Creation Date: 28/12/2009 11:48
Last Modified: 06/01/2010 11:36
Modified By: David Bergmann
*/
class Country {				
	var $db;
	var $serial_cou;
    var $serial_lang;
	var $serial_zon;
	var $code_cou;
	var $name_cou;
	var $flag_cou;
	var $card_cou;
	var $banner_cou;
        var $phone_cou;
	
	
	function __construct($db, $serial_cou = NULL, $serial_lang=NULL, $serial_zon = NULL, $code_cou = NULL, $name_cou = NULL, $flag_cou = NULL, $card_cou = NULL, $banner_cou = NULL,$phone_cou=NULL){
		$this -> db = $db;
		$this -> serial_cou = $serial_cou;
		$this -> serial_lang = $serial_lang;
		$this -> serial_zon = $serial_zon;
		$this -> code_cou = $code_cou;
		$this -> name_cou = $name_cou;
		$this -> flag_cou = $flag_cou;
		$this -> card_cou = $card_cou;
		$this -> banner_cou = $banner_cou;
		$this -> phone_cou = $phone_cou;
	}
	
	/***********************************************
        * function getData
        * gets data by serial_cou
        ***********************************************/
	function getData(){
		if($this->serial_cou!=NULL){
			$sql = 	"SELECT serial_cou, serial_lang, serial_zon, code_cou, name_cou, flag_cou, card_cou, banner_cou, phone_cou
					FROM country
					WHERE serial_cou='".$this->serial_cou."'";

			$result = $this->db->Execute($sql);
			if ($result === false) return false;
			if($result->fields[0]){
				$this->serial_cou=$result->fields[0];
                $this->serial_lang=$result->fields[1];
				$this->serial_zon=$result->fields[2];
				$this->code_cou=$result->fields[3];
				$this->name_cou=$result->fields[4];
				$this->flag_cou=$result->fields[5];
				$this->card_cou=$result->fields[6];
				$this->banner_cou=$result->fields[7];
                $this->phone_cou=$result->fields[8];
				return true;
			}	
			else
				return false;

		}else
			return false;		
	}

        /***********************************************
        * function getGeoData
        * gets geoData by serial_cou
        ***********************************************/
	function getGeoData($serial_cou){
		if($serial_cou){
			$sql = 	"SELECT serial_cou, serial_zon, code_cou 
					FROM country
					WHERE serial_cou='".$serial_cou."'";

			$result = $this->db -> Execute($sql);
                        $num = $result -> RecordCount();
                        return $result->GetRowAssoc(false);
                }
        }


	/***********************************************
        * function insert
        * inserts a new register in DB
        ***********************************************/
	function insert(){
		$sql="INSERT INTO country (
					serial_cou,
                                        serial_lang,
					serial_zon,
					code_cou,
					name_cou,
					flag_cou,
					card_cou,
					banner_cou,
                                        phone_cou
					)
			  VALUES(NULL,";
                            if($this->serial_lang != '') {
                                $sql.="'".$this->serial_lang."',";
                            }
                            else {
                                $sql.="NULL,";
                            }
                                 $sql.="'".$this->serial_zon."',
					'".$this->code_cou."',	
					'".$this->name_cou."',					
					'".$this->flag_cou."',					
					'".$this->card_cou."',					
					'".$this->banner_cou."',";
                            if($this->phone_cou != '') {
                                $sql.="'".$this->phone_cou."')";
                            }
                            else {
                                $sql.="NULL)";
                            }
		$result = $this->db->Execute($sql);
		
		if($result){
            return $this->db->insert_ID();
        }else{
			ErrorLog::log($this -> db, 'INSERT FAILED - '.get_class($this), $sql);
        	return false;
        }
	}
	
	/***********************************************
        * funcion update
        * updates a register in DB
        ***********************************************/
	function update(){
		$sql="UPDATE country
		SET serial_lang=";
                if($this->serial_lang != '') {
                    $sql.="'".$this->serial_lang."',";
                }
                else {
                    $sql.="NULL,";
                }
                 $sql.="serial_zon='".$this->serial_zon."',
			code_cou='".$this->code_cou."',
			name_cou='".$this->name_cou."',
			flag_cou='".$this->flag_cou."',
			card_cou='".$this->card_cou."',
			banner_cou='".$this->banner_cou."',
                        phone_cou=";
                if($this->phone_cou != '') {
                    $sql.="'".$this->phone_cou."'";
                }
                else {
                    $sql.="NULL";
                }
		$sql.=" WHERE serial_cou='".$this->serial_cou."'";
		
		//die($sql);
		$result = $this->db->Execute($sql);
		if ($result==true) 
			return true;
		else
			return false;
	}
	
	/***********************************************
        * funcion getCountry
        * gets all the Countries of the system
        ***********************************************/
	public static function getAllAvailableCountries($db){
		$sql = 	"SELECT serial_cou, serial_lang, serial_zon, code_cou, name_cou, flag_cou, card_cou, banner_cou, phone_cou
				 FROM country
				 WHERE serial_cou != 1 AND serial_cou != 236
				 ORDER BY name_cou";
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont = 0;
			do{
				$arreglo[$cont++] = $result -> GetRowAssoc(false);
				$result->MoveNext();
			}while($cont < $num);
		}
		return $arreglo;
	}

	/***********************************************
        * funcion getCountry
        * gets all the Countries of the system
        ***********************************************/
	function getCountriesWithDealer(){
		$sql = 	"SELECT DISTINCT cou.serial_cou, cou.serial_lang, cou.serial_zon, cou.code_cou,
						cou.name_cou, cou.flag_cou, cou.card_cou, cou.banner_cou, cou.phone_cou
				 FROM country cou
				 JOIN city cit ON cit.serial_cou = cou.serial_cou
				 JOIN sector sec ON sec.serial_cit = cit.serial_cit
				 JOIN dealer dea ON dea.serial_sec = sec.serial_sec
				 WHERE cou.serial_cou!=1
				 ORDER BY cou.name_cou";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}
	
/***********************************************
        * funcion getAllCountries
        * gets all the Countries of the system in a static way
        ***********************************************/
	public static function getAllCountries($db,$all=true){
		$sql = 	"SELECT serial_cou, serial_lang, c.serial_zon, code_cou, name_cou, flag_cou, 
						card_cou, banner_cou, phone_cou, z.name_zon
                 FROM country c
				 JOIN zone z ON z.serial_zon = c.serial_zon";
		if(!$all){
			$sql.= " WHERE name_cou != 'Todos'";
		}
		$sql.= " ORDER BY name_cou ASC";
		
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arr=array();
			$cont=0;
			
			do{
				$arr[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arr;
	}
	
	/***********************************************
        * funcion getAllCountry
        * gets all the Countries of the system
        ***********************************************/
	function getAllCountry(){
		$sql = 	"SELECT serial_cou, serial_lang, serial_zon, code_cou, name_cou, flag_cou, card_cou, banner_cou, phone_cou
                         FROM country
                         ORDER BY name_cou";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
        /**
        @Name: getOwnCountries
        @Description: Retrieves the information of a specific list of country.
        @Params: The list of country IDs
        @Returns: Country array
        **/
        function getOwnCountries($countriesID){
            if($countriesID) {
                $sql =   "SELECT serial_cou, code_cou, name_cou
                         FROM country
                         WHERE serial_cou IN (".$countriesID.")
                         ORDER BY name_cou";

                $result = $this->db -> Execute($sql);
                $num = $result -> RecordCount();
                if($num > 0){
                    $arreglo=array();
                    $cont=0;

                    do{
                        $arreglo[$cont]=$result->GetRowAssoc(false);
                        $result->MoveNext();
                        $cont++;
                    }while($cont<$num);
                }
            }
            return $arreglo;
        }

			  /**
        @Name: getOwnCountriesWithBonus
        @Description: Get a list og countries with Bonus to be paid, based on who is going to be paid:
		   *			DEALER or COUNTER
        @Params: $countriesID:  list of countries which the user has access to.
		   *	 $bonus_to:DEALER or COUNTER
        @Returns: Country array
        **/
        function getOwnCountriesWithBonus($countriesID,$bonus_to, $serial_mbc){
			if(!$countriesID || !$bonus_to){
				return false;
			}else{
				switch($bonus_to){
					case 'COUNTER':
							 $sql =   "SELECT DISTINCT cou.serial_cou,cou.name_cou
									   FROM applied_bonus apb
									   JOIN sales s ON s.serial_sal=apb.serial_sal
									   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
									   JOIN dealer dea on cnt.serial_dea=dea.serial_dea
									   JOIN sector sec ON dea.serial_sec=sec.serial_sec
									   JOIN city cit ON cit.serial_cit=sec.serial_cit
									   JOIN country cou ON cou.serial_cou=cit.serial_cou
									   WHERE apb.bonus_to_apb='COUNTER'
									   AND apb.status_apb='ACTIVE'
									   AND cou.serial_cou IN (".$countriesID.")";
						break;
					case 'DEALER':
							 $sql =   "SELECT DISTINCT cou.serial_cou,cou.name_cou
									   FROM applied_bonus apb
									   JOIN sales s ON s.serial_sal=apb.serial_sal
									   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
									   JOIN dealer dea on cnt.serial_dea=dea.serial_dea
									   JOIN sector sec ON dea.serial_sec=sec.serial_sec
									   JOIN city cit ON cit.serial_cit=sec.serial_cit
									   JOIN country cou ON cou.serial_cou=cit.serial_cou
									   WHERE apb.bonus_to_apb='DEALER'
									   AND apb.status_apb='ACTIVE'
									   AND cou.serial_cou IN (".$countriesID.")";
						break;
				}

				if($serial_mbc!='1'){
					$sql.=" AND dea.serial_mbc=".$serial_mbc;
				}
				
				$sql.=" ORDER BY cou.name_cou";

				//echo $sql;
                $result = $this->db ->getAll($sql);
				if($result)
					return $result;
				else
					return false;
			}
		}

			  /**
        @Name: getOwnCountriesWithBonusToAuthorize
        @Description: Get a list og countries with out of date Bonus which could be authorize, based on who is going to be paid:
		   *			DEALER or COUNTER
        @Params: $countriesID:  list of countries which the user has access to.
		   *	 $bonus_to:DEALER or COUNTER
        @Returns: Country array
        **/
        function getOwnCountriesWithBonusToAuthorize($countriesID,$bonus_to){
			if(!$countriesID || !$bonus_to){
				return false;
			}else{
				switch($bonus_to){
					case 'COUNTER':
							 $sql =   "SELECT DISTINCT cou.serial_cou,cou.name_cou
									   FROM applied_bonus apb
									   JOIN sales s ON s.serial_sal=apb.serial_sal
									   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
									   JOIN dealer dea on cnt.serial_dea=dea.serial_dea
									   JOIN sector sec ON dea.serial_sec=sec.serial_sec
									   JOIN city cit ON cit.serial_cit=sec.serial_cit
									   JOIN country cou ON cou.serial_cou=cit.serial_cou
									   WHERE apb.bonus_to_apb='COUNTER'
									   AND apb.status_apb='ACTIVE'
									   AND apb.ondate_apb='NO'
									   AND apb.authorized_apb IS NULL
									   AND cou.serial_cou IN (".$countriesID.")
									ORDER BY cou.name_cou";
						break;
					case 'DEALER':
							 $sql =   "SELECT DISTINCT cou.serial_cou,cou.name_cou
									   FROM applied_bonus apb
									   JOIN sales s ON s.serial_sal=apb.serial_sal
									   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
									   JOIN dealer dea on cnt.serial_dea=dea.serial_dea
									   JOIN sector sec ON dea.serial_sec=sec.serial_sec
									   JOIN city cit ON cit.serial_cit=sec.serial_cit
									   JOIN country cou ON cou.serial_cou=cit.serial_cou
									   WHERE apb.bonus_to_apb='DEALER'
									   AND apb.status_apb='ACTIVE'
									   AND apb.ondate_apb='NO'
									   AND apb.authorized_apb IS NULL
									   AND cou.serial_cou IN (".$countriesID.")
									ORDER BY cou.name_cou";
						break;
				}
				//echo $sql;
                $result = $this->db ->getAll($sql);
				if($result)
					return $result;
				else
					return false;
			}
		}


		  /**
        @Name: getOwnCountriesWithComission
        @Description: Get a list og countries with comission to be paid, based on who is going to be paid:
		   *			MANAGER,RESPONSIBLE OR DEALER
        @Params: $countriesID:  list of countries which the user has access to.
		   *	 $comissions_to: MANAGER,RESPONSIBLE OR DEALER
		   *	 $for_report:if true it will exclude the validation to get only pending comission
        @Returns: Country array
        **/
        function getOwnCountriesWithComission($countriesID,$comissions_to,$serial_mbc,$for_report=false){
			if(!$countriesID || !$comissions_to){
				return false;
			}else{
				switch($comissions_to){
					case 'MANAGER':
							 $sql =   "SELECT DISTINCT cou.serial_cou,cou.name_cou
									   FROM country cou
									   JOIN manager_by_country mbc ON mbc.serial_cou=cou.serial_cou
									   JOIN applied_comissions com ON com.serial_mbc=mbc.serial_mbc AND com.value_mbc_com>0";
								if(!$for_report){
										$sql.=" AND com.creation_date_com>mbc.last_commission_paid_mbc";
								}
							$sql.=" WHERE cou.serial_cou IN (".$countriesID.")";
								if($serial_mbc!='1'){
										$sql.=" AND mbc.serial_mbc=".$serial_mbc;
								}
							$sql.="	ORDER BY cou.name_cou";
						break;
					case 'RESPONSIBLE':
							 $sql =   "SELECT DISTINCT cou.serial_cou,cou.name_cou
									   FROM applied_comissions com
									   JOIN user_by_dealer ubd ON com.serial_ubd=ubd.serial_ubd
									   JOIN dealer dea ON dea.serial_dea=ubd.serial_dea
									   JOIN sector sec ON sec.serial_sec=dea.serial_sec
									   JOIN city cit ON cit.serial_cit=sec.serial_cit
									   JOIN country cou ON cou.serial_cou=cit.serial_cou
									   WHERE cou.serial_cou IN (".$countriesID.")
									   AND com.value_ubd_com>0";
							  if(!$for_report){
									$sql.=" AND com.creation_date_com>ubd.last_commission_paid_ubd";
							  }
							  if($serial_mbc!='1'){
									$sql.=" AND dea.serial_mbc=".$serial_mbc;
							  }
							 $sql.=" ORDER BY cou.name_cou";
						break;
					case 'DEALER':
							 $sql =   "SELECT DISTINCT cou.serial_cou,cou.name_cou
										   FROM applied_comissions com
										   JOIN dealer dea ON dea.serial_dea=com.serial_dea
										   JOIN sector sec ON sec.serial_sec=dea.serial_sec
										   JOIN city cit ON cit.serial_cit=sec.serial_cit
										   JOIN country cou ON cou.serial_cou=cit.serial_cou
										   WHERE cou.serial_cou IN (".$countriesID.")
										   AND com.value_dea_com>0";
								if(!$for_report){
									$sql.=" AND com.creation_date_com>dea.last_commission_paid_dea";
								}
								if($serial_mbc!='1'){
									$sql.=" AND dea.serial_mbc=".$serial_mbc;
							    }
							 $sql.=" ORDER BY cou.name_cou";
						break;
					case 'SUBMANAGER':
						$sql = "SELECT DISTINCT cou.serial_cou,cou.name_cou
								FROM applied_comissions com
								JOIN dealer dea ON dea.serial_dea=com.serial_dea AND manager_rights_dea = 'YES'
								JOIN sector sec ON sec.serial_sec=dea.serial_sec
								JOIN city cit ON cit.serial_cit=sec.serial_cit
								JOIN country cou ON cou.serial_cou=cit.serial_cou
								WHERE cou.serial_cou IN (".$countriesID.")
								AND com.value_dea_com > 0";
						if(!$for_report){
							$sql.=" AND com.creation_date_com > dea.last_commission_paid_dea";
						}
						
						if($serial_mbc!='1'){
							$sql.=" AND dea.serial_mbc=".$serial_mbc;
						}
						
						$sql.=" ORDER BY cou.name_cou";
						break;
				}
				//echo $sql;
                $result = $this->db ->getAll($sql);
				if($result)
					return $result;
				else
					return false;
			}
		}

        /**
        @Name: getInvoiceCountries
        @Description: Retrieves the information of a specific list of country.
        @Params: The list of country IDs
        @Returns: Country array
        **/
        function getInvoiceCountries($countriesID, $serial_mbc){
            if($countriesID) {
                $sql = "SELECT DISTINCT cou.serial_cou, cou.code_cou, cou.name_cou
						FROM country cou
						JOIN manager_by_country mbc ON mbc.serial_cou = cou.serial_cou
						JOIN dealer dea ON dea.serial_mbc=mbc.serial_mbc
						JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
						JOIN sales s ON cnt.serial_cnt=s.serial_cnt AND s.status_sal='REGISTERED' AND s.free_sal ='NO'
						JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
						JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
						JOIN product p ON p.serial_pro=pxc.serial_pro AND ((p.masive_pro='NO') OR (p.masive_pro='YES' AND s.sal_serial_sal IS NOT NULL))
						AND s.serial_inv IS NULL";

				if($serial_mbc!='1'){
					$sql.=" WHERE dea.serial_mbc=".$serial_mbc;
				}
				$sql.=" ORDER BY cou.name_cou";
				//Debug::print_r($sql);
				
                $result = $this->db -> Execute($sql);
                $num = $result -> RecordCount();
                if($num > 0){
                    $arreglo=array();
                    $cont=0;

                    do{
                        $arreglo[$cont]=$result->GetRowAssoc(false);
                        $result->MoveNext();
                        $cont++;
                    }while($cont<$num);
                }
            }
            return $arreglo;
        }

        /**
        @Name: getPaymentCountries
        @Description: Retrieves the information of a specific list of country.
        @Params: The list of country IDs
        @Returns: Country array
        **/
        function getPaymentCountries($countriesID, $serial_mbc){
            if($countriesID) {
                $sql =   "SELECT DISTINCT cou.serial_cou, cou.code_cou, cou.name_cou
                         FROM country cou
                         JOIN city cit ON cou.serial_cou=cit.serial_cou
                         JOIN sector sec ON sec.serial_cit=cit.serial_cit
                         JOIN dealer dea ON dea.serial_sec=sec.serial_sec
                         JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
                         JOIN sales s ON cnt.serial_cnt=s.serial_cnt
                         JOIN invoice i ON i.serial_inv=s.serial_inv AND (i.status_inv='STAND-BY')
                         WHERE cou.serial_cou IN (".$countriesID.")";

				if($serial_mbc!='1'){
					$sql.=" AND dea.serial_mbc=".$serial_mbc;
				}
				
				$sql.=" ORDER BY cou.name_cou";

                $result = $this->db -> Execute($sql);
                $num = $result -> RecordCount();
                if($num > 0){
                    $arreglo=array();
                    $cont=0;

                    do{
                        $arreglo[$cont]=$result->GetRowAssoc(false);
                        $result->MoveNext();
                        $cont++;
                    }while($cont<$num);
                }
            }
            return $arreglo;
        }
        
	/** 
	@Name: getPhoneCountries
	@Description: Retrieves the information of the countries with a phone number
	@Returns: Country array
	**/
	function getPhoneCountries($show_all_country = true){
		if($show_all_country){
			$sql = 	"(SELECT serial_cou, name_cou, phone_cou
					 FROM country
					 WHERE serial_cou = 1
					 ) UNION";
		}
		
		$sql .= "(SELECT serial_cou, name_cou, phone_cou
                 FROM country
			 	 WHERE phone_cou IS NOT NULL AND serial_cou != 1
                 ORDER BY name_cou)";
		
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arrayPhones=array();
			$cont=0;
			
			do{
				$arrayPhones[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arrayPhones;
	}

        /***********************************************
        * funcion getCountryByZone
        * gets all the Countries in a specific Zone of the system
        ***********************************************/
	function getCountriesByZone($serial_zon,$countryList=NULL){
		$sql = 	"SELECT serial_cou, serial_lang, serial_zon, code_cou, name_cou, flag_cou, card_cou, banner_cou, phone_cou
				 FROM country
				 WHERE serial_zon=".$serial_zon;
                if($countryList!=NULL) {
                    $sql.=" AND serial_cou IN (".$countryList.")";
                }
                                 
                    $sql.=" ORDER BY name_cou";
                //die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}
        
         /***********************************************
        * funcion getInvoiceLogCountriesByZone
        * gets all the Countries in a specific Zone of the system where are pending invoice logs
        ***********************************************/
	function getInvoiceLogCountriesByZone($serial_zon,$countryList=NULL){
		$sql = 	"SELECT DISTINCT cou.serial_cou, cou.code_cou, cou.name_cou
                         FROM country cou
                         JOIN city cit ON cou.serial_cou=cit.serial_cou
                         JOIN sector sec ON sec.serial_cit=cit.serial_cit
                         JOIN dealer dea ON dea.serial_sec=sec.serial_sec
                         JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
                         JOIN sales s ON cnt.serial_cnt=s.serial_cnt
                         JOIN invoice inv ON inv.serial_inv=s.serial_inv
                         JOIN invoice_log inl ON inl.serial_inv=inv.serial_inv AND inl.status_inl='PENDING'";

                if($countryList!=NULL) {
                    $sql.=" WHERE cou.serial_cou IN (".$countryList.")";
                }
                                 
                    $sql.=" ORDER BY cou.name_cou";
                //die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}

        /***********************************************
        * funcion getInvoiceLogCountriesByZone
        * gets all the Countries in a specific Zone of the system where are pending invoice logs
        ***********************************************/
	function getCreditNoteCountriesByZone($serial_zon,$serial_mbc,$countryList=NULL){
		$sql = 	"SELECT DISTINCT cou.serial_cou, cou.code_cou, cou.name_cou
				 FROM country cou
				 JOIN city cit ON cou.serial_cou=cit.serial_cou
				 JOIN sector sec ON sec.serial_cit=cit.serial_cit
				 JOIN dealer dea ON dea.serial_sec=sec.serial_sec
				 JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
				 JOIN sales s ON cnt.serial_cnt=s.serial_cnt
				 JOIN refund r ON r.serial_sal=s.serial_sal
				 JOIN credit_note cn ON r.serial_ref=cn.serial_ref AND cn.serial_ref IS NOT NULL AND cn.status_cn='ACTIVE' AND cn.payment_status_cn='PENDING'";

		if($countryList!=NULL) {
			$sql.=" WHERE cou.serial_cou IN (".$countryList.")";
		}
		
		if($countryList!=NULL && $serial_mbc!= 1) {
			$sql.=" AND dea.serial_mbc = ".$serial_mbc;
		}

		$sql.=" ORDER BY cou.name_cou";
                //die($sql);
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}
	
	/***********************************************
    * funcion getWebCountries
    * gets all the Countries that will be used at the website, excludes the "ALL" country
    ***********************************************/
	public static function getWebCountries($db){
		$sql = 	"SELECT serial_cou, serial_zon, name_cou, flag_cou, card_cou, banner_cou
				 FROM country
				 WHERE serial_lang IS NOT NULL AND code_cou IS NOT NULL
                                 ORDER BY name_cou";
		$result = $db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$countries=array();
			$cont=0;
			
			do{
				$countries[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $countries;
	}
	
	/***********************************************
    * funcion getCountryLanguage
    * gets the code of this countrie's language
    ***********************************************/
	public static function getCountryLanguageCode($db,$country){
		$sql = 	"SELECT code_lang
				 FROM country c, language a
				 WHERE c. serial_lang = a. serial_lang AND c.serial_cou = ".$country;
		$result = $db -> Execute($sql);
		if($result->fields[0]){
			return $result->fields[0];
		}
		return false;
	}

        /***********************************************
        * funcion getCountryByZone
        * gets all the Countries in a specific Zone of the system
        ***********************************************/
	function getUnselectedCountriesByZone($serial_zon,$countryList,$exceptionList=NULL){
		$sql = 	"SELECT serial_cou, serial_lang, serial_zon, code_cou, name_cou, flag_cou, card_cou, banner_cou, phone_cou
				 FROM country
				 WHERE serial_zon=".$serial_zon."";
                                 if ($countryList != NULL){
                                    $sql .= ' AND serial_cou IN ("'.$countryList.'")';
                                }

                if($exceptionList){
                    $sql.=" AND serial_cou NOT IN (".$exceptionList.")";
                }
                $sql .= " ORDER BY name_cou";
                //echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}
        
        /***********************************************
        * funcion getCountryByCity
        * gets the country of a selected city
        ***********************************************/
	public static function getCountryByCity($db, $serial_cit){
		$sql = 	"SELECT c.serial_cit, cou.serial_cou
				FROM city c
				JOIN country cou ON c.serial_cou=cou.serial_cou
                                WHERE c.serial_cit='".$serial_cit."'";
		$result = $db -> Execute($sql);

		if($result->fields[0]){
			return $result->fields[1];
		}else{
			return false;
		}
	}

        /**
	* @Name: getPromoCountries
	* @Description: Returns an array of countries where a list of
        *             products are sold
	* @Params: serial_zon: Specific Zone
        *          countryList: List of my countries
        *          productList: List of products that are sold in that country
	* @Returns: Country array
	**/
	function getPromoCountries($serial_zon, $countryList, $productList){
		$list=NULL;
		$sql = 	"SELECT c.serial_cou, c.serial_zon, c.code_cou, c.name_cou, c.flag_cou, c.card_cou, c.banner_cou
			 FROM country c
			 WHERE serial_zon=".$serial_zon."
                         AND serial_cou IN (".$countryList.")
                         AND serial_cou IN (SELECT DISTINCT pbc.serial_cou
                                            FROM product_by_country pbc
                                            WHERE pbc.serial_pro IN (".$productList."))
                         ORDER BY name_cou";

		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;

			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}

		return $arreglo;
	}
        
	/** 
	@Name: getCountriesAutocompleter
	@Description: Returns an array of countries.
	@Params: Country name or pattern.
	@Returns: Country array
	**/
	function getCountriesAutocompleter($name_cou){
		$sql = "SELECT c.serial_cou, c.code_cou, c.name_cou, z.name_zon, z.serial_zon
				FROM country c
				JOIN zone z ON z.serial_zon=c.serial_zon
				WHERE LOWER(c.name_cou) LIKE _utf8'%".utf8_encode($name_cou)."%' collate utf8_bin
				OR LOWER(c.code_cou) LIKE _utf8'%".utf8_encode($name_cou)."%' collate utf8_bin
				LIMIT 10";
		
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}
	
	/***********************************************
        * funcion countryExists
        * verifies if a Country exists or not
        ***********************************************/
	function countryExists($txtNameCountry, $serial_cou=NULL){
		if($txtNameCountry){
			$sql = 	"SELECT serial_cou
					 FROM country
					 WHERE LOWER(name_cou)= _utf8'".utf8_encode($txtNameCountry)."' collate utf8_bin";
			if($serial_cou!=NULL){
				$sql.=" AND serial_cou <> ".$serial_cou;
			}
			$result = $this->db -> Execute($sql);
			if($result->fields[0]){
				return $result->fields[0];
			}else{
				return false;
			}
		}
	}
	
	/***********************************************
        * funcion countryCodeExists
        * verifies if a Country Code exists or not
        ***********************************************/
	function countryCodeExists($txtCodeCountry, $serial_cou=NULL){
		if($txtCodeCountry){
			$sql = 	"SELECT serial_cou
					 FROM country
					 WHERE LOWER(code_cou)= _utf8'".utf8_encode($txtCodeCountry)."' collate utf8_bin";
			if($serial_cou!=NULL){
				$sql.=" AND serial_cou <> ".$serial_cou;
			}
			$result = $this->db -> Execute($sql);
			if($result->fields[0]){
				return $result->fields[0];
			}else{
				return false;
			}
		}
	}
	
	/***********************************************
        * funcion getLastCountry
        * gets the last Serial from the table Country
        ***********************************************/
	function getLastCountry(){
		$sql = 	"SELECT max(serial_cou)
				 FROM country";

		$result = $this->db -> Execute($sql);
		if($result->fields[0]){
			return $result->fields[0];
		}else{
			return 0;
		}
	}
	
	/***********************************************
        * funcion getCountryAccessList
        * gets the last Serial from the table Country
        ***********************************************/
	function getCountryAccessList(){
		$sql = 	"SELECT cou.serial_cou, cou.serial_zon, cou.name_cou
                         FROM dealer dea
                         JOIN sector sec ON dea.serial_sec=sec.serial_sec
                         JOIN city cit ON cit.serial_cit=sec.serial_cit
                         JOIN country cou ON cou.serial_cou=cit.serial_cou
                         ORDER BY cou.name_cou";
		//echo $sql;
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();
		if($num > 0){
			$arreglo=array();
			$cont=0;
			
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		
		return $arreglo;
	}


	/***********************************************
	* function getCountriesWithAssistanceGroups
	@Name: getCountriesWithAssistanceGroups
	@Description: Check if a group name already exists
	@Params:
			None
	@Returns: list of countries/false
	***********************************************/
	function getCountriesWithAssistanceGroups(){
		$sql="SELECT DISTINCT c.serial_cou,c.name_cou
				FROM assistance_group ass
				JOIN country c ON c.serial_cou=ass.serial_cou
				ORDER BY c.name_cou";
		$result=$this->db->getAll($sql);
		//echo $sql;
		if($result)
			return $result;
		else
			return false;
	}

	/***********************************************
	 * @Name: getCountriesWithExcessPayments
	 * @Description: Retrieves a list of countries with excess payments to dispatch.
	 * @Params: DB connection
	 * @Returns: an array of countries.
    ***********************************************/
	public static function getCountriesWithExcessPayments($db){
		$sql = 	"SELECT DISTINCT cou.serial_cou, cou.name_cou
				 FROM country cou
				 JOIN city cit ON cit.serial_cou=cou.serial_cou
				 JOIN sector sec ON sec.serial_cit=cit.serial_cit
				 JOIN dealer b ON b.serial_sec=sec.serial_sec
				 JOIN counter c ON c.serial_dea=b.serial_dea
				 JOIN sales s ON s.serial_cnt=c.serial_cnt
				 JOIN invoice i ON i.serial_inv=s.serial_inv AND i.status_inv='PAID'
				 JOIN payments p ON p.serial_pay=i.serial_pay AND p.status_pay='EXCESS' AND p.excess_amount_available_pay > 0
                 ORDER BY name_cou";
		
		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/**
	@Name: getOwnCountries
	@Description: Retrieves the information of a specific list of country.
	@Params: The list of country IDs
	@Returns: Country array
	**/
	function getOwnCountriesWithPendingFreeSales($countriesID){
		if($countriesID) {
			$sql =  "SELECT distinct(cou.serial_cou), cou.code_cou, cou.name_cou
					 FROM country cou
					 JOIN city c ON c.serial_cou=cou.serial_cou
					 JOIN sector s ON s.serial_cit=c.serial_cit
					 JOIN dealer d ON d.serial_sec=s.serial_sec
					 JOIN counter cnt ON cnt.serial_dea=d.serial_dea
					 JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt AND sal.status_sal='REQUESTED'
					 WHERE cou.serial_cou IN (".$countriesID.")
					 ORDER BY name_cou";

			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
			if($num > 0){
				$arreglo=array();
				$cont=0;

				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
		}
		return $arreglo;
	}

	/**
	@Name: getCountriesByInvoice
	@Description: Retrieves the information of a specific country list
	@Params:
	@Returns: Country array
	**/
	function getCountriesByInvoice($countriesID){
		$arreglo=array();
		if($countriesID){
			$sql =  "SELECT distinct(cou.serial_cou), cou.name_cou
					 FROM sales s
					   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
					   JOIN dealer dea ON cnt.serial_dea=dea.serial_dea
					   JOIN sector sec ON dea.serial_sec=sec.serial_sec
					   JOIN city cit ON sec.serial_cit=cit.serial_cit
					   JOIN country cou ON cit.serial_cou=cou.serial_cou
					   LEFT JOIN invoice i ON i.serial_inv=s.serial_inv
					   WHERE cou.serial_cou IN (".$countriesID.")
					   ORDER BY name_cou";

			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();

			if($num > 0){
				$cont=0;
				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
		}
		return $arreglo;
	}

		/**
	@Name: getCountriesByZoneWithInvoices
	@Description: Retrieves the information of a specific country list
	@Params:
	@Returns: Country array
	**/
  function getCountriesByZoneWithInvoices($serial_zon, $countriesID, $serial_mbc, $dea_serial_dea) {
		$arreglo = array();
		if ($countriesID) {
			$sql = "SELECT distinct(cou.serial_cou), cou.name_cou
					FROM sales s
					JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
					JOIN dealer br ON cnt.serial_dea=br.serial_dea
					JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
					JOIN sector sec ON br.serial_sec=sec.serial_sec
					JOIN city cit ON sec.serial_cit=cit.serial_cit
					JOIN country cou ON cit.serial_cou=cou.serial_cou
					JOIN invoice i ON i.serial_inv = s.serial_inv AND i.status_inv='STAND-BY'
					WHERE serial_zon=" . $serial_zon . "
					AND cou.serial_cou IN (" . $countriesID . ")";

			if ($serial_mbc != '1') {
				$sql.=" AND mbc.serial_mbc=" . $serial_mbc;
			}
			if ($dea_serial_dea) {
				$sql.=" AND br.serial_dea=" . $dea_serial_dea;
			}
			$sql.=" ORDER BY name_cou";

			$result = $this->db->Execute($sql);
			$num = $result->RecordCount();

			if ($num > 0) {
				$cont = 0;
				do {
					$arreglo[$cont] = $result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				} while ($cont < $num);
			}
		}
		return $arreglo;
	}

	/**
	@Name: getCountriesByExcessPayments
	@Description: Retrieves the information of a specific country list
	@Params:
	@Returns: Country array
	**/
	function getCountriesByExcessPayments($countriesID, $serial_mbc, $dea_serial_dea){
		$sql = "	SELECT distinct(cou.serial_cou), cou.name_cou
					FROM sales s
					JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
					JOIN dealer br ON cnt.serial_dea=br.serial_dea
					JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
					JOIN country cou ON mbc.serial_cou=cou.serial_cou
					JOIN invoice inv ON inv.serial_inv=s.serial_inv
					JOIN payments pay ON pay.serial_pay=inv.serial_pay AND pay.status_pay='EXCESS'
					WHERE cou.serial_cou IN (".$countriesID.")
					GROUP BY cou.serial_cou";

		if($serial_mbc!='1'){
			$sql.=" AND mbc.serial_mbc=".$serial_mbc;
		}

		if($dea_serial_dea){
			$sql.=" AND br.dea_serial_dea=".$dea_serial_dea;
		}

		$sql.=" ORDER BY cou.name_cou";

		$result = $this->db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/**
	@Name: getCountriesWithPayments
	@Description: Retrieves the information of a specific country list
	@Params:
	@Returns: Country array
	**/
	function getCountriesWithPayments($countriesID, $serial_mbc, $dea_serial_dea){
		$arreglo=array();
		if($countriesID){
			$sql =  "SELECT distinct(cou.serial_cou), cou.name_cou
					 FROM sales s
					   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
					   JOIN dealer br ON cnt.serial_dea=br.serial_dea
						JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
						JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
					   JOIN sector sec ON br.serial_sec=sec.serial_sec
					   JOIN city cit ON sec.serial_cit=cit.serial_cit
					   JOIN country cou ON cit.serial_cou=cou.serial_cou
					   LEFT JOIN invoice inv ON inv.serial_inv=s.serial_inv
					   JOIN payments pay ON pay.serial_pay=inv.serial_pay
					   WHERE cou.serial_cou IN (".$countriesID.")
					   ";
			if($serial_mbc!='1'){
				$sql.=" AND mbc.serial_mbc=".$serial_mbc;
			}
			if($dea_serial_dea){
				$sql.=" AND dea.serial_dea=".$dea_serial_dea;
			}
			$sql.=" ORDER BY cou.name_cou";
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();

			if($num > 0){
				$cont=0;
				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
		}
		return $arreglo;
	}

			/**
	@Name: getCountriesWithFreeCard
	@Description: Retrieves the information of a specific country list
	@Params:
	@Returns: Country array
	**/
	function getCountriesWithFreeCard($serial_zon, $countriesID, $serial_mbc, $dea_serial_dea){
		$arreglo=array();
		if($countriesID){
			$sql =  "SELECT distinct(cou.serial_cou), cou.name_cou
					 FROM sales sal
					   JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
					   JOIN dealer br ON cnt.serial_dea=br.serial_dea
					   JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
					   JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
					   JOIN sector sec ON br.serial_sec=sec.serial_sec
					   JOIN city cit ON sec.serial_cit=cit.serial_cit
					   JOIN country cou ON cit.serial_cou=cou.serial_cou
					   WHERE cou.serial_zon=".$serial_zon." AND sal.free_sal = 'YES' AND cou.serial_cou IN (".$countriesID.")
					   ";
			if($serial_mbc!='1'){
				$sql.=" AND mbc.serial_mbc=".$serial_mbc;
			}
			if($dea_serial_dea){
				$sql.=" AND dea.serial_dea=".$dea_serial_dea;
			}
			$sql.=" ORDER BY cou.name_cou";
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();

			if($num > 0){
				$cont=0;
				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
		}
		return $arreglo;
	}

		/**
	@Name: getPaymentDetailCountries
	@Description: Retrieves the information of a specific country list
	@Params:
	@Returns: Country array
	**/
	function getPaymentDetailCountries($countriesID){
		$arreglo=array();
		if($countriesID){
			$sql =  "SELECT distinct(cou.serial_cou), cou.name_cou
					 FROM sales s
					   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
					   JOIN dealer dea ON cnt.serial_dea=dea.serial_dea
					   JOIN sector sec ON dea.serial_sec=sec.serial_sec
					   JOIN city cit ON sec.serial_cit=cit.serial_cit
					   JOIN country cou ON cit.serial_cou=cou.serial_cou
					   LEFT JOIN invoice inv ON inv.serial_inv=s.serial_inv
					   JOIN payments pay ON pay.serial_pay=inv.serial_pay
					   JOIN payment_detail pyf ON pyf.serial_pay=pay.serial_pay AND checked_pyf='NO'
					   WHERE cou.serial_cou IN (".$countriesID.")
					   ORDER BY name_cou";
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();

			if($num > 0){
				$cont=0;
				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
		}
		return $arreglo;
	}

	/*
	 * @name: getOwnCountriesWithDealers
	 * @param: $db - DB connection
	 * @param: $serial_zon - Zone ID
	 * @param: $countries_IDs - A set of IDs of the countries I have access.
	 */
	public static function getOwnCountriesWithDealers($db, $serial_zon, $countries_IDs, $serial_mbc=NULL){
		$sql="SELECT DISTINCT(cou.serial_cou) , cou.name_cou
			  FROM country cou
			  JOIN city c ON c.serial_cou=cou.serial_cou
			  JOIN sector s ON s.serial_cit=c.serial_cit
			  JOIN dealer d ON d.serial_sec=s.serial_sec
			  WHERE cou.serial_cou IN ($countries_IDs)
			  AND cou.serial_zon=".$serial_zon;

		if($serial_mbc){
			$sql.=" AND d.serial_mbc=".$serial_mbc;
			$sql.=" AND cou.serial_cou <> 1";
		}else{
			$sql .= " UNION 
					(SELECT DISTINCT(cou.serial_cou) , cou.name_cou
					FROM country cou
					WHERE serial_cou = 1
					AND cou.serial_zon=".$serial_zon.")
					";
		}
		//Debug::print_r($sql);
		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}
	/**
	@Name: getCountriesWithRefunds
	@Description: Retrieves the information of a specific country list
	@Params:
	@Returns: Country array
	**/
	function getCountriesWithRefunds($countriesID, $serial_mbc, $dea_serial_dea){
		$arreglo=array();
		if($countriesID){
			$sql =  "SELECT distinct(cou.serial_cou), cou.name_cou
					 FROM sales sal
					   JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
					   JOIN dealer br ON cnt.serial_dea=br.serial_dea
						JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
						JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
					   JOIN sector sec ON dea.serial_sec=sec.serial_sec
					   JOIN city cit ON sec.serial_cit=cit.serial_cit
					   JOIN country cou ON cit.serial_cou=cou.serial_cou
					   JOIN refund ref ON ref.serial_sal=sal.serial_sal
					   WHERE cou.serial_cou IN (".$countriesID.")
					   ";
			if($serial_mbc!='1'){
				$sql.=" AND mbc.serial_mbc=".$serial_mbc;
			}
			if($dea_serial_dea){
				$sql.=" AND dea.serial_dea=".$dea_serial_dea;
			}
			$sql.=" ORDER BY cou.name_cou";
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();

			if($num > 0){
				$cont=0;
				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
		}
		return $arreglo;
	}
	
	
	/**
        @Name: getInvoiceCountries
        @Description: Retrieves the information of countries with Masive Sales.
        @Params: The list of country IDs
        @Returns: Country array
    **/
    function getMasiveCountries($countriesID){
		
	   	if($countriesID) {
			$sql =  "(SELECT DISTINCT cou.serial_cou, cou.code_cou, cou.name_cou
					 FROM country cou
					 JOIN city cit ON cou.serial_cou=cit.serial_cou
					 JOIN sector sec ON sec.serial_cit=cit.serial_cit
					 JOIN dealer dea ON dea.serial_sec=sec.serial_sec
					 JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
					 JOIN sales s ON cnt.serial_cnt=s.serial_cnt AND s.status_sal='REGISTERED'
					 JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
					 JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
					 JOIN product p ON p.serial_pro=pxc.serial_pro 
						AND p.masive_pro='YES' 
						AND s.sal_serial_sal IS NULL
					 WHERE cou.serial_cou IN (".$countriesID.") 
 					 AND NOW() < s.end_date_sal

					 )
					 ORDER BY cou.name_cou";
			//die($sql);
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();
			if($num > 0){
				$arreglo=array();
				$cont=0;

				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
		}
		return $arreglo;
	}

	/**
	@Name: getCountriesSalesAnalysis
	@Description: Retrieves the information of a specific country list
	@Params:
	@Returns: Country array
	**/
	function getCountriesSalesAnalysis($serial_zon, $countriesID, $serial_mbc, $dea_serial_dea){
		$arreglo=array();
		if($countriesID){
			$sql =  "SELECT distinct(cou.serial_cou), cou.name_cou
						FROM sales sal
							JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
							JOIN dealer br ON cnt.serial_dea=br.serial_dea
							JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
							JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc
							JOIN sector sec ON br.serial_sec=sec.serial_sec
      			       	    JOIN city cit ON sec.serial_cit=cit.serial_cit
							JOIN country cou ON cit.serial_cou=cou.serial_cou AND cou.serial_cou IN (".$countriesID.")
							JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv
							JOIN payments pay ON pay.serial_pay=inv.serial_pay
                        WHERE sal.free_sal = 'NO' AND sal.status_sal NOT IN ('VOID','DENIED','REFUNDED','BLOCKED')  AND sal.serial_inv IS NOT NULL
                              AND inv.serial_pay IS NOT NULL AND pay.status_pay = 'PAID' AND cou.serial_zon=".$serial_zon."
                       ";
			if($serial_mbc!='1'){
				$sql.=" AND mbc.serial_mbc=".$serial_mbc;
			}
			if($dea_serial_dea){
				$sql.=" AND dea.serial_dea=".$dea_serial_dea;
			}
			$sql.="  ORDER BY cou.name_cou";
			$result = $this->db -> Execute($sql);
			$num = $result -> RecordCount();

			if($num > 0){
				$cont=0;
				do{
					$arreglo[$cont]=$result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				}while($cont<$num);
			}
		}
		return $arreglo;
	}

	/**
	@Name: getCountriesPricesByProduct
	@Description: Retrieves the information of a specific country list
	@Params:
	@Returns: Country array
	**/
	function getCountriesPricesByProduct($serial_zon, $countriesID){
		$arreglo=array();
		$sql =  "SELECT DISTINCT (cou.serial_cou), cou.name_cou
						FROM product pro
						JOIN product_by_country pbc ON pbc.serial_pro = pro.serial_pro
						JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro
						JOIN country cou ON cou.serial_cou = pbc.serial_cou
						JOIN zone zon ON zon.serial_zon = cou.serial_zon AND zon.serial_zon=".$serial_zon."
					WHERE cou.serial_cou IN (".$countriesID.")
					ORDER BY cou.name_cou
					  ";
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();

		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/**
	@Name: getCountriesBenefitsByProduct
	@Description: Retrieves the information of a specific country list
	@Params:
	@Returns: Country array
	**/
	function getCountriesBenefitsByProduct($serial_zon, $countriesID, $serial_lang){
		$arreglo=array();
		$sql =  "SELECT DISTINCT (cou.serial_cou), cou.name_cou
						FROM product pro
						JOIN product_by_country pbc ON pro.serial_pro=pbc.serial_pro AND pro.status_pro = 'ACTIVE'
						JOIN product_by_language pbl ON pbl.serial_pro = pro.serial_pro AND pbl.serial_lang= ".$serial_lang."
						JOIN benefits_product bp ON bp.serial_pro=pro.serial_pro AND bp.status_bxp = 'ACTIVE'
						JOIN conditions c ON c.serial_con=bp.serial_con
						LEFT JOIN restriction_type rt ON rt.serial_rst=bp.serial_rst AND rt.status_rst = 'ACTIVE'
						LEFT JOIN restriction_by_language rbl ON rbl.serial_rst=rt.serial_rst AND rbl.serial_lang = ".$serial_lang."
						JOIN benefits b ON b.serial_ben=bp.serial_ben AND b.status_ben = 'ACTIVE'
						JOIN benefits_by_language bbl ON bbl.serial_ben=b.serial_ben AND bbl.serial_lang= ".$serial_lang."
						JOIN country cou ON cou.serial_cou = pbc.serial_cou
						JOIN zone zon ON zon.serial_zon = cou.serial_zon
						JOIN manager_by_country mbc ON mbc.serial_cou = cou.serial_cou
						JOIN dealer dea ON dea.serial_mbc = mbc.serial_mbc
					WHERE pbl.serial_lang = ".$serial_lang." AND cou.serial_cou IN (".$countriesID.") AND zon.serial_zon = ".$serial_zon."
					 ORDER BY cou.name_cou ";
		
		$result = $this->db -> Execute($sql);
		$num = $result -> RecordCount();

		if($num > 0){
			$cont=0;
			do{
				$arreglo[$cont]=$result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			}while($cont<$num);
		}
		return $arreglo;
	}

	/*
	 * @name: getCountryNameByCityCode
	 * @param: $db - DB connection
	 * @param: $serial_cit - City ID
	 * @return: City Name
	 */
	public static function getCountryNameByCityCode($db, $serial_cit){
		$sql = "SELECT c.name_cou
				FROM country c
				JOIN city cit ON cit.serial_cou=c.serial_cou AND cit.serial_cit=".$serial_cit;


		$result=$db->getOne($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}

	/*
	 * @name: hasPhoneSales
	 * @param: $db - DB connection
	 * @param: $serial_cit - City ID
	 * @return: TRUE/FALSE
	 */
	public static function hasPhoneSales($db, $serial_cou){
		$sql = "SELECT *
				FROM country cou
				JOIN city cit ON cit.serial_cou = cou.serial_cou
				JOIN sector sec ON sec.serial_cit = cit.serial_cit
				JOIN dealer dea ON dea.serial_sec = sec.serial_sec
				WHERE cou.serial_cou = $serial_cou
				AND dea.phone_sales_dea = 'YES'
				AND dea.status_dea = 'ACTIVE'";
		$result=$db->getOne($sql);

		if($result){
			return true;
		}else{
			return false;
		}
	}

	/*
	 * @name: getPhoneSalesCountries
	 * @param: $db - DB connection
	 * @return: Array with countries
	 */
	public static function getPhoneSalesCountries($db, $all = false){
		global $phoneSalesDealer;
		$sql = "SELECT DISTINCT(cou.serial_cou), cou.name_cou, dea.serial_dea
				FROM country cou
				JOIN city cit ON cit.serial_cou = cou.serial_cou
				JOIN sector sec ON sec.serial_cit = cit.serial_cit
				JOIN dealer dea ON dea.serial_sec = sec.serial_sec
				WHERE dea.phone_sales_dea = 'YES'
				AND dea.status_dea = 'ACTIVE'
				AND dea.dea_serial_dea IS NOT NULL";
		if(!$all) {
			$sql .= " AND dea.serial_dea <> $phoneSalesDealer";
		}
		$sql.= " GROUP BY cou.serial_cou";
		$result=$db->getAll($sql);
		if($result){
				return $result;
		}else{
				return NULL;
		}
	}

	/*
	 * @name: getCountriesWithBonus
	 * @param: $db - DB connection
	 * @return: Array with countries
	 */
	public static function getCountriesWithBonus($db, $country_list, $serial_zon = NULL, $bonus_to = NULL, $at_least_one_paid = NULL, $authorized = NULL){
		if($at_least_one_paid) $one_paid = " JOIN applied_bonus_liquidation abl ON abl.serial_apb = apb.serial_apb";

		$sql = "SELECT DISTINCT c.serial_cou, c.name_cou
				  FROM country c
				  JOIN city cit ON cit.serial_cou=c.serial_cou
				  JOIN sector s ON s.serial_cit=cit.serial_cit
				  JOIN dealer d ON d.serial_sec=s.serial_sec
				  JOIN dealer b ON b.dea_serial_dea=d.serial_dea
				  JOIN counter cnt ON cnt.serial_dea=b.serial_dea
				  JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
				  JOIN applied_bonus apb ON apb.serial_sal=sal.serial_sal
				  $one_paid
				  WHERE c.serial_cou IN ($country_list)";

		if($serial_zon) $sql.= " AND c.serial_zon=$serial_zon";
		if($bonus_to) $sql.= " AND apb.bonus_to_apb = '$bonus_to'";
		if($authorized) $sql.=" AND apb.ondate_apb = 'NO'
								AND apb.authorized_apb = 'YES'";

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return NULL;
		}
	}

	public static function getOwnCountriesWithProducts($db, $countryList, $hide_web_country = true){
		$sql = "SELECT DISTINCT c.serial_cou, c.code_cou, c.name_cou
				FROM country c
				JOIN product_by_country pxc ON pxc.serial_cou = c.serial_cou AND pxc.status_pxc = 'ACTIVE'
				WHERE c.serial_cou IN (".$countryList.")";
		if($hide_web_country) $sql .= " AND c.serial_cou <> 1";
		$sql .=" ORDER BY name_cou";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}


	/*
	 * @name: getCountriesWithSales
	 * @param: $db - DB connection
	 * @return: Array with countries
	 */
	public static function getCountriesWithSales($db, $countryList, $serial_mbc, $serial_dea){

		$sql = "SELECT DISTINCT cou.serial_cou, cou.name_cou
				FROM sales sal
				JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt AND cnt.status_cnt = 'ACTIVE'
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.status_dea = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou
				WHERE cou.serial_cou IN (".$countryList.") AND cou.serial_cou != 1
				";

		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_dea ? " AND dea.serial_dea = $serial_dea" : "";
		$sql .= " ORDER BY name_cou";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	/*
	 * @name: getCountriesWithAssignedDealers
	 * @param: $db - DB connection
	 * @return: Array with countries
	 */
	public static function getCountriesWithAssignedDealers($db, $countryList, $serial_mbc, $serial_dea){

		$sql = "SELECT DISTINCT cou.serial_cou, cou.name_cou
				FROM dealer dea
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou 
				WHERE cou.serial_cou IN (".$countryList.") AND cou.serial_cou != 1
				";

		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_dea ? " AND dea.serial_dea = $serial_dea" : "";
		$sql .= " ORDER BY name_cou";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

	public static function getCountriesWithManager($db){
		$sql="SELECT DISTINCT cou.serial_cou, cou.name_cou
				FROM country cou
				JOIN manager_by_country mbc ON cou.serial_cou=mbc.serial_cou";

		$result = $db -> getAll($sql);

		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getCountriesWithFiles($db){
		$sql = "SELECT DISTINCT cou.serial_cou, name_cou
				FROM file f
				JOIN city cit ON cit.serial_cit = f.serial_cit
				JOIN country cou ON cou.serial_cou = cit.serial_cou AND cou.serial_cou <> 1
				ORDER BY name_cou";
		
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}

    function getPhoneCountriesForPlannetAssist(){
        $sql = "SELECT serial_cou, name_cou, phone_cou
                 FROM country
			 	 WHERE phone_cou IS NOT NULL 
			 	 AND serial_cou IN (67,218)
                 ORDER BY name_cou";

        $result = $this->db -> Execute($sql);
        $num = $result -> RecordCount();
        if($num > 0){
            $arrayPhones=array();
            $cont=0;

            do{
                $arrayPhones[$cont]=$result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            }while($cont<$num);
        }

        return $arrayPhones;
    }

	///GETTERS
	function getSerial_cou(){
		return $this->serial_cou;
	}
        function getSerial_lang(){
		return $this->serial_lang;
	}
	function getSerial_zon(){
		return $this->serial_zon;
	}
	function getCode_cou(){
		return $this->code_cou;
	}
	function getName_cou(){
		return $this->name_cou;
	}
	function getFlag_cou(){
		return $this->flag_cou;
	}
	function getCard_cou(){
		return $this->card_cou;
	}
	function getBanner_cou(){
		return $this->banner_cou;
	}
	function getPhone_cou(){
		return $this->phone_cou;
	}

	///SETTERS	
	function setSerial_cou($serial_cou){
		$this->serial_cou = $serial_cou;
	}
        function setSerial_lang($serial_lang){
		$this->serial_lang = $serial_lang;
	}
	function setSerial_zon($serial_zon){
		$this->serial_zon = $serial_zon;
	}
	function setCode_cou($code_cou){
		$this->code_cou = $code_cou;
	}
	function setName_cou($name_cou){
		$this->name_cou = $name_cou;
	}
	function setFlag_cou($flag_cou){
		$this->flag_cou = $flag_cou;
	}
	function setCard_cou($card_cou){
		$this->card_cou = $card_cou;
	}
	function setBanner_cou($banner_cou){
		$this->banner_cou = $banner_cou;
	}
        function setPhone_cou($phone_cou){
		$this->phone_cou = $phone_cou;
	}
}
?>
