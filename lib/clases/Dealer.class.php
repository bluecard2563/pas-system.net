<?php

/*
  File: Dealer.class.php
  Author: Patricio Astudillo
  Creation Date: 06/01/2009 15:57
  Last Modified: 22/11/2017
  Modified By: Carlos Oberto
 */

class Dealer {

	var $db;
	var $serial_dea;
	var $serial_sec;
	var $serial_mbc;
	var $serial_cdd;
	var $serial_dlt;
	var $dea_serial_dea;
	var $serial_usr;
	var $id_dea;
	var $code_dea;
	var $name_dea;
	var $category_dea;
	var $address_dea;
	var $phone1_dea;
	var $phone2_dea;
	var $contract_date_dea;
	var $fax_dea;
	var $email1_dea;
	var $email2_dea;
	var $contact_dea;
	var $phone_contact_dea;
	var $email_contact_dea;
	var $percentage_dea;
	var $type_percentage_dea;
	var $status_dea;
	var $payment_deadline_dea;
	var $bill_to_dea;
	var $pay_day_dea;
	var $pay_contact_dea;
	var $assistance_contact_dea;
	var $email_assistance_dea;
	var $branch_number_dea;
	var $visits_number_dea;
	var $official_seller_dea;
	var $last_commission_paid_dea;
	var $bonus_to_dea;
	var $manager_name_dea;
	var $manager_phone_dea;
	var $manager_email_dea;
	var $manager_birthday_dea;
	var $phone_sales_dea;
	var $created_on_dea;
	var $manager_rights_dea;
	var $bank_dea;
	var $account_type_dea;
	var $account_number_dea;
	var $aux_data;
	var $minimum_kits_dea;
	var $notifications_dea;
	var $logo_dea;

	function __construct($db, $serial_dea = NULL, $serial_sec = NULL, $serial_mbc = NULL, $serial_cdd = NULL, $serial_dlt = NULL, $dea_serial_dea = NULL, $id_dea = NULL, $name_dea = NULL, $category_dea = NULL, $address_dea = NULL, $phone1_dea = NULL, $phone2_dea = NULL, $contract_date_dea = NULL, $fax_dea = NULL, $email1_dea = NULL, $email2_dea = NULL, $contact_dea = NULL, $phone_contact_dea = NULL, $email_contact_dea = NULL, $percentage_dea = NULL, $type_percentage_dea = NULL, $status_dea = NULL, $payment_deadline_dea = NULL, $bill_to_dea = NULL, $pay_day_dea = NULL, $pay_contact_dea = NULL, $assistance_contact_dea = NULL, $email_assistance_dea = NULL, $code_dea = NULL, $branch_number_dea = NULL, $visits_number_dea = NULL, $official_seller_dea = NULL, $last_commission_paid_dea = NULL, $bonus_to_dea = NULL, $manager_name_dea = NULL, $manager_phone_dea = NULL, $manager_email_dea = NULL, $manager_birthday_dea = NULL, $phone_sales_dea = NULL, $serial_usr = NULL, $created_on_dea = NULL, $manager_rights_dea = NULL, $logo_dea = NULL) {

		$this->db = $db;
		$this->serial_dea = $serial_dea;
		$this->serial_sec = $serial_sec;
		$this->serial_mbc = $serial_mbc;
		$this->serial_cdd = $serial_cdd;
		$this->serial_dlt = $serial_dlt;
		$this->dea_serial_dea = $dea_serial_dea;
		$this->id_dea = $id_dea;
		$this->name_dea = $name_dea;
		$this->category_dea = $category_dea;
		$this->address_dea = $address_dea;
		$this->phone1_dea = $phone1_dea;
		$this->phone2_dea = $phone2_dea;
		$this->contract_date_dea = $contract_date_dea;
		$this->fax_dea = $fax_dea;
		$this->email1_dea = $email1_dea;
		$this->email2_dea = $email2_dea;
		$this->contact_dea = $contact_dea;
		$this->phone_contact_dea = $phone_contact_dea;
		$this->email_contact_dea = $email_contact_dea;
		$this->percentage_dea = $percentage_dea;
		$this->type_percentage_dea = $type_percentage_dea;
		$this->status_dea = $status_dea;
		$this->payment_deadline_dea = $payment_deadline_dea;
		$this->bill_to_dea = $bill_to_dea;
		$this->pay_day_dea = $pay_day_dea;
		$this->pay_contact_dea = $pay_contact_dea;
		$this->assistance_contact_dea = $assistance_contact_dea;
		$this->email_assistance_dea = $email_assistance_dea;
		$this->code_dea = $code_dea;
		$this->branch_number_dea = $branch_number_dea;
		$this->visits_number_dea = $visits_number_dea;
		$this->official_seller_dea = $official_seller_dea;
		$this->last_commission_paid_dea = $last_commission_paid_dea;
		$this->bonus_to_dea = $bonus_to_dea;
		$this->manager_name_dea = $manager_name_dea;
		$this->manager_phone_dea = $manager_phone_dea;
		$this->manager_email_dea = $manager_email_dea;
		$this->manager_birthday_dea = $manager_birthday_dea;
		$this->phone_sales_dea = $phone_sales_dea;
		$this->serial_usr = $serial_usr;
		$this->created_on_dea = $created_on_dea;
		$this->manager_rights_dea = $manager_rights_dea;
		$this->logo_dea =$logo_dea;
	}

	/**
	  @Name: getData
	  @Description: Retrieves the data of a Dealer object.
	  @Params: The ID of the object
	  @Returns: TRUE if O.K. / FALSE on errors
	 * */
	function getData() {
		if ($this->serial_dea != NULL) {
			$sql = "SELECT d.serial_dea, 
            				d.serial_sec, 
            				d.serial_mbc, 
            				d.serial_cdd, 
            				d.serial_dlt, 
            				ubd.serial_usr AS 'responsible',
            				d.id_dea, 
            				d.name_dea,
                            d.category_dea, 
                            d.address_dea,	
                            d.phone1_dea, 
                            d.phone2_dea, 
                            DATE_FORMAT(d.contract_date_dea,'%d/%m/%Y'), 
                            d.fax_dea,
                            d.email1_dea, 
                            d.email2_dea, 
                            d.contact_dea, 
                            d.phone_contact_dea, 
                            d.email_contact_dea, 
                            IFNULL(d.percentage_dea,0),
                            d.type_percentage_dea, 
                            d.status_dea, 
                            d.payment_deadline_dea, 
                            d.bill_to_dea, 
                            d.pay_contact_dea,
                            d.assistance_contact_dea, 
                            d.email_assistance_dea, 
                            d.dea_serial_dea, 
                            d.code_dea, 
                            d.branch_number_dea, 
                            d.visits_number_dea,
                            d.official_seller_dea, 
                            DATE_FORMAT(d.last_commission_paid_dea,'%d/%m/%Y %T'), 
                            d.bonus_to_dea, 
                            c.serial_cit,
                            cou.serial_cou, 
                            z.serial_zon,
							d.manager_name_dea, 
							d.manager_phone_dea, 
							d.manager_email_dea, 
							IFNULL(DATE_FORMAT(d.manager_birthday_dea,'%d/%m/%Y'),'') AS manager_birthday_dea,
							d.phone_sales_dea,
							d.serial_usr,
							DATE_FORMAT(d.created_on_dea, '%d/%m/%Y') AS 'created_on_dea',
							CONCAT(u.first_name_usr,' ',u.last_name_usr) AS 'creator_name',
							d.manager_rights_dea,
							d.bank_dea, 
							d.account_type_dea, 
							d.account_number_dea,
							d.minimum_kits_dea,
							d.notifications_dea,
							d.logo_dea 
					FROM dealer d
					LEFT JOIN user_by_dealer ubd ON ubd.serial_dea=d.serial_dea AND ubd.status_ubd = 'ACTIVE'
					LEFT JOIN user u ON u.serial_usr=d.serial_usr
					JOIN sector s ON s.serial_sec=d.serial_sec
					JOIN city c ON c.serial_cit=s.serial_cit
					JOIN country cou ON cou.serial_cou=c.serial_cou
					JOIN zone z ON z.serial_zon=cou.serial_zon
					WHERE d.serial_dea='" . $this->serial_dea . "'";
			
			$result = $this->db->Execute($sql);
			if ($result === false)
				return false;

			if ($result->fields[0]) {
				$this->serial_dea = $result->fields[0];
				$this->serial_sec = $result->fields[1];
				$this->serial_mbc = $result->fields[2];
				$this->serial_cdd = $result->fields[3];
				$this->serial_dlt = $result->fields[4];
				$this->aux_data['serial_usr'] = $result->fields[5];
				$this->id_dea = $result->fields[6];
				$this->name_dea = $result->fields[7];
				$this->category_dea = $result->fields[8];
				$this->address_dea = $result->fields[9];
				$this->phone1_dea = $result->fields[10];
				$this->phone2_dea = $result->fields[11];
				$this->contract_date_dea = $result->fields[12];
				$this->fax_dea = $result->fields[13];
				$this->email1_dea = $result->fields[14];
				$this->email2_dea = $result->fields[15];
				$this->contact_dea = $result->fields[16];
				$this->phone_contact_dea = $result->fields[17];
				$this->email_contact_dea = $result->fields[18];
				$this->percentage_dea = $result->fields[19];
				$this->type_percentage_dea = $result->fields[20];
				$this->status_dea = $result->fields[21];
				$this->payment_deadline_dea = $result->fields[22];
				$this->bill_to_dea = $result->fields[23];
				$this->pay_contact_dea = $result->fields[24];
				$this->assistance_contact_dea = $result->fields[25];
				$this->email_assistance_dea = $result->fields[26];
				$this->dea_serial_dea = $result->fields[27];
				$this->code_dea = $result->fields[28];
				$this->branch_number_dea = $result->fields[29];
				$this->visits_number_dea = $result->fields[30];
				$this->official_seller_dea = $result->fields[31];
				$this->last_commission_paid_dea = $result->fields[32];
				$this->bonus_to_dea = $result->fields[33];
				$this->aux_data['serial_cit'] = $result->fields[34];
				$this->aux_data['serial_cou'] = $result->fields[35];
				$this->aux_data['serial_zon'] = $result->fields[36];
				$this->manager_name_dea = $result->fields[37];
				$this->manager_phone_dea = $result->fields[38];
				$this->manager_email_dea = $result->fields[39];
				$this->manager_birthday_dea = $result->fields[40];
				$this->phone_sales_dea = $result->fields[41];
				$this->serial_usr = $result->fields[42];
				$this->created_on_dea = $result->fields[43];
				$this->aux_data['creator_name'] = $result->fields[44];
				$this->manager_rights_dea = $result->fields[45];
				$this->bank_dea = $result->fields[46];
				$this->account_type_dea = $result->fields[47];
				$this->account_number_dea = $result->fields[48];
				$this->minimum_kits_dea = $result->fields[49];
				$this->notifications_dea = $result->fields[50];
				$this->logo_dea = $result->fields [51]; 
				return true;
			}
		}
		return false;
	}

	/**
	  @Name: insert
	  @Description: Inserts a register in DB
	  @Params: N/A
	  @Returns: TRUE if OK. / FALSE on error
	 * */
	function insert() {
		$sql = "INSERT INTO dealer (
                        serial_dea,
                        serial_sec,
                        serial_mbc,
                        serial_cdd,
                        serial_dlt,
                        dea_serial_dea,
                        id_dea,
                        code_dea,
                        name_dea,
                        category_dea,
                        address_dea,
                        phone1_dea,
                        phone2_dea,
                        contract_date_dea,
                        fax_dea,
                        email1_dea,
                        email2_dea,
                        contact_dea,
                        phone_contact_dea,
                        email_contact_dea,
                        percentage_dea,
                        type_percentage_dea,
                        status_dea,
                        payment_deadline_dea,
                        bill_to_dea,
                        pay_contact_dea,
                        assistance_contact_dea,
                        email_assistance_dea,
                        branch_number_dea,
                        visits_number_dea,
                        official_seller_dea,
                        last_commission_paid_dea,
                        bonus_to_dea,
						manager_name_dea,
						manager_phone_dea,
						manager_email_dea,
						manager_birthday_dea,
						phone_sales_dea,
						serial_usr,
						created_on_dea,
						manager_rights_dea,
						bank_dea, 
						account_type_dea, 
						account_number_dea,
						minimum_kits_dea,
						notifications_dea, 
						logo_dea 
		) VALUES (NULL,
                    '$this->serial_sec',
                    '$this->serial_mbc',
                    '$this->serial_cdd',
                    '$this->serial_dlt',";
		($this->dea_serial_dea == 'NULL' || $this->dea_serial_dea == '') ? $sql.="NULL," : $sql.="'$this->dea_serial_dea',";
			$sql .= "'$this->id_dea',
                    '$this->code_dea',
                    '$this->name_dea',
                    '$this->category_dea',
                    '$this->address_dea',
                    '$this->phone1_dea',
                    '$this->phone2_dea',
                    STR_TO_DATE('$this->contract_date_dea','%d/%m/%Y'),
                    '$this->fax_dea',
                    '$this->email1_dea',
                    '$this->email2_dea',
                    '$this->contact_dea',
                    '$this->phone_contact_dea',
                    '$this->email_contact_dea',
                    '$this->percentage_dea',
                    '$this->type_percentage_dea',
                    '$this->status_dea',
                    '$this->payment_deadline_dea',
                    '$this->bill_to_dea',
                    '$this->pay_contact_dea',
                    '$this->assistance_contact_dea',
                    '$this->email_assistance_dea',";
		($this->branch_number_dea == 'NULL' || $this->branch_number_dea == '') ? $sql.="NULL," : $sql.="'$this->branch_number_dea',";
		($this->visits_number_dea == 'NULL' || $this->visits_number_dea == '') ? $sql.="NULL," : $sql.="'$this->visits_number_dea',";
		$sql.="'$this->official_seller_dea',
                    NOW(),";
		($this->bonus_to_dea == 'NULL' || $this->bonus_to_dea == '') ? $sql.="NULL," : $sql.="'$this->bonus_to_dea',";
		$sql.="	'$this->manager_name_dea',
					'$this->manager_phone_dea',
					'$this->manager_email_dea',";
		($this->manager_birthday_dea == 'NULL' || $this->manager_birthday_dea == '') ? $sql.="NULL," : $sql.="STR_TO_DATE('$this->manager_birthday_dea','%d/%m/%Y'),";
		($this->phone_sales_dea == 'NULL' || $this->phone_sales_dea == '') ? $sql.="'NO'," : $sql.="'$this->phone_sales_dea',";
		($this->serial_usr == 'NULL' || $this->serial_usr == '') ? $sql.="NULL," : $sql.="'{$this->serial_usr}', ";
		$sql.= " NOW(),
					'{$this->manager_rights_dea}',";
		$sql .= ($this->bank_dea!='')?"'".$this->bank_dea."',":'NULL,';
		$sql .= ($this->account_type_dea!='')?"'".$this->account_type_dea."',":'NULL,';
		$sql .=	($this->account_number_dea!='')?"'".$this->account_number_dea."',":'NULL,';
		$sql .=	($this->minimum_kits_dea!='')?"'".$this->minimum_kits_dea."',":'3,';
		$sql .=	($this->notifications_dea!='')?"'".$this->notifications_dea."',":'N0,';
		$sql .= "'$this->logo_dea'";
		$sql .= ")";

		//Debug::print_r($sql); die;

		$result = $this->db->Execute($sql);

		if ($this->db->insert_ID()) {
			return $this->db->insert_ID();
		} else {
			ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
			return false;
		}
	}

	/**
	  @Name: update
	  @Description: Updates a register in DB
	  @Params: N/A
	  @Returns: TRUE if OK. / FALSE on error
	 * */
	function update() {
		$sql = "UPDATE dealer SET
						serial_sec='" . $this->serial_sec . "',
						serial_mbc='" . $this->serial_mbc . "',
						serial_cdd='" . $this->serial_cdd . "',
						serial_dlt='" . $this->serial_dlt . "',
						dea_serial_dea=";
		if ($this->dea_serial_dea == 'NULL' || $this->dea_serial_dea == '') {
			$sql.="NULL,";
		} else {
			$sql.="'" . $this->dea_serial_dea . "',";
		}
		$sql.= "id_dea='" . $this->id_dea . "',
                        code_dea='" . $this->code_dea . "',
                        name_dea='" . $this->name_dea . "',
                        category_dea='" . $this->category_dea . "',
                        address_dea='" . $this->address_dea . "',
                        phone1_dea='" . $this->phone1_dea . "',
                        phone2_dea='" . $this->phone2_dea . "',
                        contract_date_dea=STR_TO_DATE('" . $this->contract_date_dea . "','%d/%m/%Y'),
                        fax_dea='" . $this->fax_dea . "',
                        email1_dea='" . $this->email1_dea . "',
                        email2_dea='" . $this->email2_dea . "',
                        contact_dea='" . $this->contact_dea . "',
                        phone_contact_dea='" . $this->phone_contact_dea . "',
                        email_contact_dea='" . $this->email_contact_dea . "',
                        percentage_dea='" . $this->percentage_dea . "',
                        type_percentage_dea='" . $this->type_percentage_dea . "',
                        status_dea='" . $this->status_dea . "',
                        payment_deadline_dea='" . $this->payment_deadline_dea . "',
                        bill_to_dea='" . $this->bill_to_dea . "',
                        pay_contact_dea='" . $this->pay_contact_dea . "',
                        assistance_contact_dea='" . $this->assistance_contact_dea . "',
                        email_assistance_dea='" . $this->email_assistance_dea . "',
						minimum_kits_dea='" . $this->minimum_kits_dea . "',
						notifications_dea='" . $this->notifications_dea . "',
						logo_dea='".$this->logo_dea."',
						branch_number_dea=";

		if ($this->branch_number_dea == '') {
			$sql.="NULL,";
		} else {
			$sql.="'" . $this->branch_number_dea . "',";
		}

		$sql.=" visits_number_dea='" . $this->visits_number_dea . "',
                        bonus_to_dea='" . $this->bonus_to_dea . "',
						manager_name_dea='" . $this->manager_name_dea . "',
						manager_phone_dea='" . $this->manager_phone_dea . "',
						manager_email_dea='" . $this->manager_email_dea . "',";
		if ($this->manager_birthday_dea == 'NULL' || $this->manager_birthday_dea == '') {
			$sql.="manager_birthday_dea= NULL,";
		} else {
			$sql.="manager_birthday_dea=STR_TO_DATE('" . $this->manager_birthday_dea . "','%d/%m/%Y'),";
		}
			$sql .= "	manager_rights_dea = '{$this->manager_rights_dea}'";
		
		$sql .= ", bank_dea = ";
		$sql .= ($this->bank_dea)?"'".$this->bank_dea."'":'NULL';
		
		$sql .= ", account_type_dea = ";
		$sql .=($this->account_type_dea)?"'".$this->account_type_dea."'":'NULL';
		
		$sql .=	", account_number_dea = ";
		$sql .= ($this->account_number_dea)?"'".$this->account_number_dea."'":'NULL';
		
		$sql .= " WHERE serial_dea = '" . $this->serial_dea . "'";
		//Debug::print_r($sql); die;

		$result = $this->db->Execute($sql);
		if ($result) {
			return true;
		} else {
			ErrorLog::log($this->db, 'UPDATE FAILED - ' . get_class($this), $sql);
			return false;
		}
	}

	/*	 * *********************************************
	 * function getGeoData
	 * gets geoData by serial_dea
	 * ********************************************* */

	function getGeoData($serial_dea) {
		if ($serial_dea) {
			$sql = "SELECT d.serial_dea, c.serial_cit,cou.serial_cou, z.serial_zon
					FROM dealer d
					JOIN sector s ON s.serial_sec=d.serial_sec
					JOIN city c ON c.serial_cit=s.serial_cit
					JOIN country cou ON cou.serial_cou=c.serial_cou
					JOIN zone z ON z.serial_zon=cou.serial_zon
					WHERE d.serial_dea='" . $serial_dea . "'";

			$result = $this->db->Execute($sql);
			$num = $result->RecordCount();
			return $result->GetRowAssoc(false);
		}
	}

	/**
	  @Name: getDealers
	  @Description: Gets all dealers with a specific criteria
	  @Params: An array of parameters for making the query more precise.
	  Array's sintaxis:
	  $params=array('0'=>array('field'=> field_name,
	  'value'=> ID,
	  'like'=> 0/1),
	  '1'=>array('field'=> field_name,
	  'value'=> ID,
	  'like'=> 0/1)..
	  )
	  'field': field name for the query.
	  'value': field ID for matching.
	  'like': '0' for different to, '1' for equal to
	  @Returns: Dealers array
	 * */
	function getDealers($params = NULL) {
		$extraParams = 0;
		$sql = "SELECT DISTINCT d.*, ubd.serial_usr, s.name_sec, mbc.serial_man, dtl.description_dtl, d.code_dea,c.serial_cit,
                            dd.visits_number_dea as parent_visits_number_dea, cdd.days_cdd
					 FROM dealer d
					 JOIN user_by_dealer ubd ON ubd.serial_dea=d.serial_dea AND ubd.status_ubd='ACTIVE'
					 LEFT JOIN dealer dd ON d.dea_serial_dea = dd.serial_dea
					 JOIN sector s ON d.serial_sec=s.serial_sec
					 JOIN city c ON c.serial_cit=s.serial_cit
					 JOIN country cou ON cou.serial_cou=c.serial_cou
					 JOIN manager_by_country mbc ON mbc.serial_mbc=d.serial_mbc
					 JOIN dealer_type dt ON dt.serial_dlt=d.serial_dlt
					 JOIN dealer_ty_by_language dtl ON dtl.serial_dlt=dt.serial_dlt AND dtl.serial_lang = '" . $_SESSION['serial_lang'] . "'
					 JOIN credit_day cdd ON cdd.serial_cdd=d.serial_cdd";
		if ($params) {
			foreach ($params as $p) {
				if ($extraParams == 0) {
					$sql.=" WHERE " . $p['field'];
					if (is_array($p['value'])) {
						if ($p['like'] == 1) {
							$sql.=" IN (" . implode(',', $p['value']) . ")";
						} else {
							$sql.=" NOT IN (" . implode(',', $p['value']) . ")";
						}
					} else {
						if ($p['like'] == 1) {
							if ($p['value'] == 'NULL') {
								$sql.=" IS NULL";
							} else {
								$sql.=" = '" . $p['value'] . "'";
							}
						} else {
							if ($p['value'] == 'NULL') {
								$sql.=" IS NOT NULL";
							} else {
								$sql.=" <> '" . $p['value'] . "'";
							}
						}
					}
					$extraParams = 1;
				} else {
					$sql.=" AND " . $p['field'];
					if (is_array($p['value'])) {
						if ($p['like'] == 1) {
							$sql.=" IN (" . implode(',', $p['value']) . ")";
						} else {
							$sql.=" NOT IN (" . implode(',', $p['value']) . ")";
						}
					} else {
						if ($p['like'] == 1) {
							if ($p['value'] == 'NULL') {
								$sql.=" IS NULL";
							} else {
								$sql.=" = '" . $p['value'] . "'";
							}
						} else {
							if ($p['value'] == 'NULL') {
								$sql.=" IS NOT NULL";
							} else {
								$sql.=" <> '" . $p['value'] . "'";
							}
						}
					}
				}
			}
		}

		$sql.=" ORDER BY d.name_dea ASC";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arr = array();
			$cont = 0;

			do {
				$arr[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arr;
	}

	/**
	  @Name: dealerIDExists
	  @Description: Verifies if another dealer exist with the same ID.
	  @Params: $id_dea(dealerID), $serial_dea(dealer serialID)
	  @Returns: TRUE if exist. / FALSE no match
	 * */
	function dealerIDExists($id_dea, $serial_dea) {
		$sql = "SELECT serial_dea
				 FROM dealer
				 WHERE id_dea='" . $id_dea . "'";

		if ($serial_dea != NULL) {
			$sql.=" AND serial_dea <> " . $serial_dea;
		}

		$result = $this->db->Execute($sql);

		if ($result->fields[0]) {
			return true;
		} else {
			return false;
		}
	}

	/**
	  @Name: getDealersByCityWithComissions
	  @Description: Gets all dealers by city with pending comissions
	  @Params: $serial_cit city's serial
	  $is_report:
	  @Returns: Dealers array
	 * */
	function getDealersByCityWithComissions($serial_cit, $serial_dea = NULL, $comission_to = NULL, $is_report = false) {
		$sql = "SELECT DISTINCT dea.serial_dea,dea.code_dea,dea.name_dea
				FROM applied_comissions com
				JOIN dealer brc ON brc.serial_dea=com.serial_dea
				JOIN dealer dea ON dea.serial_dea=brc.dea_serial_dea
				JOIN sector sec ON sec.serial_sec=dea.serial_sec
				WHERE sec.serial_cit=$serial_cit
				AND com.value_dea_com > 0
				AND dea.phone_sales_dea = 'NO'";
		
		
		if (!$is_report) {
			$sql.=" AND com.dea_payment_date_com IS NULL";
		}

		if ($serial_dea != NULL) {
			$sql.=" AND dea.serial_dea = $serial_dea";
		}

		$sql.=" ORDER BY dea.name_dea ASC";

		//Debug::print_r($sql); die; echo $sql;
		$result = $this->db->getAll($sql);
		if ($result)
			return $result;
		else
			return false;
	}

	/**
	  @Name: getDealersByCityWithBonus
	  @Description: Gets all dealers by city with pending comissions
	  @Params: $serial_cit city's serial
	  $bonus_to: bonus type, COUNTER or DEALER
	  @Returns: Dealers array
	 * */
	function getDealersByCityWithBonus($serial_cit, $bonus_to, $serial_dea = NULL) {
		if (!$serial_cit || !$bonus_to) {
			return false;
		} else {
			switch ($bonus_to) {
				case 'COUNTER':
					$sql = "SELECT DISTINCT dea.serial_dea,dea.code_dea,dea.name_dea
							   FROM applied_bonus apb
							   JOIN sales s ON s.serial_sal=apb.serial_sal
							   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
							   JOIN dealer brc ON brc.serial_dea=cnt.serial_dea
							   JOIN dealer dea ON dea.serial_dea=brc.dea_serial_dea
							   JOIN sector sec ON sec.serial_sec=dea.serial_sec
							   WHERE sec.serial_cit=$serial_cit
							   AND apb.bonus_to_apb='COUNTER'
							   AND apb.status_apb='ACTIVE'
							   AND dea.phone_sales_dea = 'NO'";
					break;
				case 'DEALER':
					$sql = "SELECT DISTINCT dea.serial_dea,dea.code_dea,dea.name_dea
							   FROM applied_bonus apb
							   JOIN sales s ON s.serial_sal=apb.serial_sal
							   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
							   JOIN dealer brc ON brc.serial_dea=cnt.serial_dea
							   JOIN dealer dea ON dea.serial_dea=brc.dea_serial_dea
							   JOIN sector sec ON sec.serial_sec=dea.serial_sec
							   WHERE sec.serial_cit=$serial_cit
							   AND apb.bonus_to_apb='DEALER'
							   AND apb.status_apb='ACTIVE'
							   AND dea.phone_sales_dea = 'NO'";
					break;
			}
			if ($serial_dea != NULL) {
				$sql .= " AND dea.serial_dea = $serial_dea";
			}

			$sql.= " ORDER BY dea.name_dea";
		}
		//echo $sql;
		$result = $this->db->getAll($sql);
		if ($result)
			return $result;
		else
			return false;
	}

	/**
	  @Name: getDealersByCityWithBonusToAuthorize
	  @Description: Gets all dealers by city with pending comissions
	  @Params: $serial_cit city's serial
	  $bonus_to: bonus type, COUNTER or DEALER
	  @Returns: Dealers array
	 * */
	function getDealersByCityWithBonusToAuthorize($serial_cit, $bonus_to, $serial_dea = NULL) {
		if (!$serial_cit || !$bonus_to) {
			return false;
		} else {
			switch ($bonus_to) {
				case 'COUNTER':
					$sql = "SELECT DISTINCT dea.serial_dea,dea.code_dea,dea.name_dea
							   FROM applied_bonus apb
							   JOIN sales s ON s.serial_sal=apb.serial_sal
							   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
							   JOIN dealer brc ON brc.serial_dea=cnt.serial_dea
							   JOIN dealer dea ON dea.serial_dea=brc.dea_serial_dea
							   JOIN sector sec ON sec.serial_sec=dea.serial_sec
							   WHERE sec.serial_cit=$serial_cit
							   AND apb.bonus_to_apb='COUNTER'
							   AND apb.status_apb='ACTIVE'
							   AND apb.ondate_apb='NO'
							   AND apb.authorized_apb IS NULL
							   AND dea.phone_sales_dea = 'NO'
								ORDER BY dea.name_dea";
					break;
				case 'DEALER':
					$sql = "SELECT DISTINCT dea.serial_dea,dea.code_dea,dea.name_dea
							   FROM applied_bonus apb
							   JOIN sales s ON s.serial_sal=apb.serial_sal
							   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
							   JOIN dealer brc ON brc.serial_dea=cnt.serial_dea
							   JOIN dealer dea ON dea.serial_dea=brc.dea_serial_dea
							   JOIN sector sec ON sec.serial_sec=dea.serial_sec
							   WHERE sec.serial_cit=$serial_cit
							   AND apb.bonus_to_apb='DEALER'
							   AND apb.status_apb='ACTIVE'
							   AND apb.ondate_apb='NO'
							   AND apb.authorized_apb IS NULL
							   AND dea.phone_sales_dea = 'NO'
								ORDER BY dea.name_dea";
					break;
			}
			if ($serial_dea != NULL) {
				$sql .= " AND dea.serial_dea = $serial_dea";
			}
		}
		//echo $sql;
		$result = $this->db->getAll($sql);
		if ($result)
			return $result;
		else
			return false;
	}

	/**
	  @Name: getBranchesByDealerWithBonus
	  @Description: Gets all dealers by city with pending comissions
	  @Params: $serial_cit city's serial
	  $bonus_to: bonus type, COUNTER or DEALER
	  @Returns: Dealers array
	 * */
	function getBranchesByDealerWithBonus($serial_dea, $bonus_to, $serial_dea_session = NULL) {
		if (!$serial_dea || !$bonus_to) {
			return false;
		} else {
			switch ($bonus_to) {
				case 'COUNTER':
					$sql = "SELECT DISTINCT brc.serial_dea,brc.code_dea,brc.name_dea
							   FROM applied_bonus apb
							   JOIN sales s ON s.serial_sal=apb.serial_sal
							   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
							   JOIN dealer brc ON brc.serial_dea=cnt.serial_dea
							   WHERE brc.dea_serial_dea=$serial_dea
							   AND apb.bonus_to_apb='COUNTER'
							   AND apb.status_apb='ACTIVE'
							   AND brc.phone_sales_dea = 'NO'";
					break;
				case 'DEALER':
					$sql = "SELECT DISTINCT brc.serial_dea,brc.code_dea,brc.name_dea
							   FROM applied_bonus apb
							   JOIN sales s ON s.serial_sal=apb.serial_sal
							   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
							   JOIN dealer brc ON brc.serial_dea=cnt.serial_dea
							   WHERE brc.dea_serial_dea=$serial_dea
							   AND apb.bonus_to_apb='DEALER'
							   AND apb.status_apb='ACTIVE'
							   AND brc.phone_sales_dea = 'NO'";
					break;
			}
			if ($serial_dea_session != NULL) {
				$sql .= " AND brc.serial_dea = $serial_dea_session";
			}
		}
		
		$sql .= " ORDER BY brc.name_dea";
		//Debug::print_r($sql); die;
		$result = $this->db->getAll($sql);
		if ($result)
			return $result;
		else
			return false;
	}

	/**
	  @Name: getBranchesByDealerWithBonusToAuthorize
	  @Description: Gets all branches by dealer with out of date bonus to authorize
	  @Params: $serial_cit city's serial
	  $bonus_to: bonus type, COUNTER or DEALER
	  @Returns: Dealers array
	 * */
	function getBranchesByDealerWithBonusToAuthorize($serial_dea, $bonus_to, $dealer_restriction = NULL) {
		if (!$serial_dea || !$bonus_to) {
			return false;
		} else {
			switch ($bonus_to) {
				case 'COUNTER':
					$sql = "SELECT DISTINCT brc.serial_dea,brc.code_dea,brc.name_dea
								   FROM applied_bonus apb
								   JOIN sales s ON s.serial_sal=apb.serial_sal
								   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
								   JOIN dealer brc ON brc.serial_dea=cnt.serial_dea
								   WHERE brc.dea_serial_dea=$serial_dea
								   AND apb.bonus_to_apb='COUNTER'
								   AND apb.status_apb='ACTIVE'
								   AND apb.ondate_apb='NO'
								   AND apb.authorized_apb IS NULL
								   AND brc.phone_sales_dea = 'NO'";
					break;
				case 'DEALER':
					$sql = "SELECT DISTINCT brc.serial_dea,brc.code_dea,brc.name_dea
								   FROM applied_bonus apb
								   JOIN sales s ON s.serial_sal=apb.serial_sal
								   JOIN counter cnt ON cnt.serial_cnt=s.serial_cnt
								   JOIN dealer brc ON brc.serial_dea=cnt.serial_dea
								   WHERE brc.dea_serial_dea=$serial_dea
								   AND apb.bonus_to_apb='DEALER'
								   AND apb.status_apb='ACTIVE'
								   AND apb.ondate_apb='NO'
								   AND apb.authorized_apb IS NULL
								   AND brc.phone_sales_dea = 'NO'";
					break;
			}
			if ($dealer_restriction != NULL) {
				$sql .= " AND brc.serial_dea = $dealer_restriction";
			}
		}
		//echo '<pre>'.$sql.'</pre>';
		$result = $this->db->getAll($sql);
		if ($result)
			return $result;
		else
			return false;
	}

	/**
	  @Name: getBranchesByDealerWithComissions
	  @Description: Gets all branches by city with pending comissions
	  @Params: $serial_cit city's serial
	  $is_report: get all comissions or only the pending ones
	  @Returns: branches array
	 * */
	function getBranchesByDealerWithComissions($dea_serial_dea, $serial_dea = NULL, $is_report = false) {
		$sql = "SELECT DISTINCT brc.serial_dea,brc.code_dea,brc.name_dea
				FROM applied_comissions com
				JOIN dealer brc ON brc.serial_dea = com.serial_dea
				WHERE brc.dea_serial_dea = $dea_serial_dea
				AND com.value_dea_com > 0
				AND brc.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql.=" AND brc.serial_dea = $serial_dea";
		}

		if (!$is_report) {
			$sql.=" AND com.dea_payment_date_com IS NULL";
		}

		//echo $sql;
		$result = $this->db->getAll($sql);
		if ($result)
			return $result;
		else
			return false;
	}

	/**
	  @Name: getDealersForCalendar
	  @Description: Returns all Dealers ordered by sector
	  @Params: N/A
	  @Returns: TRUE if OK. / FALSE on error
	 * */
	function getDealersForCalendar($serial_usr) {
		$sql = "SELECT d.serial_dea, d.name_dea, d.visits_number_dea
				FROM dealer d
				JOIN dealer_type dt ON dt.serial_dlt=d.serial_dlt
				JOIN user_by_dealer ubd on ubd.serial_dea = d.serial_dea
				JOIN dealer_ty_by_language dtl ON dtl.serial_dlt=dt.serial_dlt AND dtl.serial_lang = '" . $_SESSION['serial_lang'] . "'
				WHERE d.status_dea ='ACTIVE'
				AND ubd.serial_usr =" . $serial_usr . "
				AND d.dea_serial_dea IS NULL
				AND d.phone_sales_dea = 'NO'
				ORDER BY d.name_dea";

		//echo $sql;
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arr = array();
			$cont = 0;

			do {
				$arr[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arr;
	}

	/**
	  @Name: getBranchesForCalendar
	  @Description: Returns all Dealers ordered by sector
	  @Params: N/A
	  @Returns: TRUE if OK. / FALSE on error
	 * */
	function getBranchesForCalendar($serial_usr) {
		$sql = "SELECT d.serial_dea, d.name_dea, d.visits_number_dea, d.code_dea
				 FROM dealer d
				 JOIN dealer_type dt ON dt.serial_dlt=d.serial_dlt
				 JOIN user_by_dealer ubd on ubd.serial_dea = d.serial_dea
				 JOIN dealer_ty_by_language dtl ON dtl.serial_dlt=dt.serial_dlt AND dtl.serial_lang = '" . $_SESSION['serial_lang'] . "'
				 WHERE d.status_dea ='ACTIVE'
				 AND ubd.serial_usr =" . $serial_usr . "
				 AND d.dea_serial_dea IS NOT NULL
				 ORDER BY d.serial_sec";

		//echo $sql;
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arr = array();
			$cont = 0;

			do {
				$arr[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arr;
	}

	/**
	  @Name: getBranchesForCalendar
	  @Description: Returns all Dealers ordered by sector
	  @Params: N/A
	  @Returns: TRUE if OK. / FALSE on error
	 * */
	function getTotalVisitsPerMonthByUser($serial_usr) {
		$sql = "SELECT SUM(d.visits_number_dea)
				 FROM dealer d
				 JOIN dealer_type dt ON dt.serial_dlt=d.serial_dlt
				 JOIN user_by_dealer ubd on ubd.serial_dea = d.serial_dea
				 JOIN dealer_ty_by_language dtl ON dtl.serial_dlt=dt.serial_dlt AND dtl.serial_lang = '" . $_SESSION['serial_lang'] . "'
				 WHERE d.status_dea ='ACTIVE'
				 AND ubd.serial_usr =" . $serial_usr . "
				 AND d.dea_serial_dea IS NOT NULL";

		$result = $this->db->getOne($sql);

		return $result;
	}

	/*	 * *********************************************
	 * funcion getPaymentDealers
	 * gets all the dealers that have pending invoice for payments
	 * ********************************************* */

	function getPaymentDealers($serial_cit, $serial_usr, $dealer_cat = NULL, $dea_serial_dea = NULL) {
		$comissionistSql = " ";
		if ($serial_usr) {
			$comissionistSql = "JOIN user_by_dealer ubd ON ubd.serial_dea=dea.serial_dea AND ubd.status_ubd='ACTIVE' AND ubd.serial_usr = $serial_usr";
		}

		$sql = "SELECT DISTINCT dea.serial_dea, dea.name_dea, dea.code_dea
					 FROM dealer dea
					 JOIN dealer d ON dea.serial_dea=d.dea_serial_dea
					 JOIN sector sec ON d.serial_sec=sec.serial_sec
					 JOIN city c ON sec.serial_cit=c.serial_cit AND c.serial_cit='" . $serial_cit . "'
					 $comissionistSql
					 JOIN counter cnt ON cnt.serial_dea=d.serial_dea
					 JOIN sales s ON cnt.serial_cnt=s.serial_cnt
					 JOIN invoice i ON i.serial_inv=s.serial_inv AND (i.status_inv='STAND-BY')";

		if ($dealer_cat != NULL) {
			$sql.="WHERE dea.category_dea='" . $dealer_cat . "'";
			if ($dea_serial_dea) {
				$sql.="AND dea.serial_dea='" . $dea_serial_dea . "'";
			}
		} else {
			if ($dea_serial_dea) {
				$sql.="WHERE dea.serial_dea='" . $dea_serial_dea . "'";
			}
		}

		$sql.= " ORDER BY dea.name_dea";
		//die($sql);
		$result = $this->db->Execute($sql);

		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/*	 * *********************************************
	 * funcion getPaymentDealers
	 * gets all the dealers that have pending invoice for payments
	 * ********************************************* */

	function getPaymentDealersByManager($serial_cit, $serial_usr, $serial_mbc) {
		$sql = "SELECT DISTINCT dea.serial_dea, dea.name_dea, dea.code_dea
                FROM dealer dea
                JOIN dealer d ON dea.serial_dea=d.dea_serial_dea
                JOIN sector sec ON d.serial_sec=sec.serial_sec
                JOIN city c ON sec.serial_cit=c.serial_cit AND c.serial_cit='$serial_cit'
                JOIN user_by_dealer ubd ON ubd.serial_dea=dea.serial_dea AND ubd.status_ubd='ACTIVE' AND ubd.serial_usr='$serial_usr'
                JOIN counter cnt ON cnt.serial_dea=d.serial_dea
                JOIN sales s ON cnt.serial_cnt=s.serial_cnt
                JOIN invoice i ON i.serial_inv=s.serial_inv AND (i.status_inv='STAND-BY' OR i.status_inv='VOID')
                WHERE dea.serial_mbc = $serial_mbc
                ORDER BY dea.name_dea";
		//die($sql);
		$result = $this->db->Execute($sql);

		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/*	 * *********************************************
	 * funcion getPaymentBranchesByManager
	 * gets all the dealers that have pending invoice for payments
	 * ********************************************* */

	function getPaymentBranchesByManager($serial_dea, $serial_usr) {
		$sql = "SELECT DISTINCT dea.serial_dea, dea.name_dea, dea.code_dea
                FROM dealer dea
                JOIN user_by_dealer ubd ON ubd.serial_dea=dea.serial_dea AND ubd.status_ubd='ACTIVE' AND ubd.serial_usr='$serial_usr'
                JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
                JOIN sales s ON cnt.serial_cnt=s.serial_cnt
                JOIN invoice i ON i.serial_inv=s.serial_inv AND (i.status_inv='STAND-BY' OR i.status_inv='VOID')
                WHERE dea.dea_serial_dea = $serial_dea
                ORDER BY dea.name_dea";
		//die($sql);
		$result = $this->db->Execute($sql);

		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/*	 * *********************************************
	 * funcion getCreditNoteDealersByCity
	 * gets all the dealers that have pending invoice for payments
	 * ********************************************* */

	function getCreditNoteDealersByCity($serial_cit, $serial_dea = NULL) {
		$sql = "SELECT dea.serial_dea, dea.name_dea
				FROM dealer dea
				JOIN invoice inv ON dea.serial_dea=inv.serial_dea
				JOIN sales sal ON inv.serial_inv=sal.serial_inv
				JOIN refund r ON r.serial_sal=sal.serial_sal
				JOIN credit_note cn ON r.serial_ref=cn.serial_ref AND cn.serial_ref IS NOT NULL AND cn.status_cn='ACTIVE' AND cn.payment_status_cn='PENDING'
				JOIN sector sec ON sec.serial_sec=dea.serial_sec
				WHERE sec.serial_cit = $serial_cit
				AND dea.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND dea.serial_dea = $serial_dea";
		}

		//die($sql);
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arr = array();
			$cont = 0;

			do {
				$arr[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arr;
	}

	/*	 * *********************************************
	 * funcion getInvoiceDealers
	 * gets all the dealers that have pending sales for invoices
	 * ********************************************* */

	function getInvoiceDealers($serial_mbc, $serial_usr = NULL, $dealer_cat = NULL, $serial_dlt = NULL, $serial_dea = NULL) {
		$sql = "SELECT DISTINCT dea.serial_dea, dea.name_dea, dea.code_dea
				FROM dealer dea
				JOIN dealer d ON dea.serial_dea=d.dea_serial_dea
				JOIN sector sec ON d.serial_sec=sec.serial_sec
				JOIN city c ON sec.serial_cit=c.serial_cit
				JOIN user_by_dealer ubd ON ubd.serial_dea=dea.serial_dea
				JOIN counter cnt ON cnt.serial_dea=d.serial_dea
				JOIN sales s ON cnt.serial_cnt=s.serial_cnt AND s.status_sal='REGISTERED' AND s.free_sal ='NO' AND s.serial_inv IS NULL
				JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro=pxc.serial_pro AND ((p.masive_pro='NO') OR (p.masive_pro='YES' AND s.sal_serial_sal IS NOT NULL))
				WHERE dea.serial_mbc='$serial_mbc'";

		if ($dealer_cat != NULL) {
			$sql.=" AND dea.category_dea='$dealer_cat'";
		}
		if ($serial_usr != NULL) {
			$sql.=" AND ubd.status_ubd='ACTIVE' AND ubd.serial_usr='$serial_usr'";
		}
		if ($serial_dlt != NULL) {
			$sql.=" AND dea.serial_dlt='$serial_dlt'";
		}
		if ($serial_dea != NULL) {
			$sql.=" AND dea.serial_dea='$serial_dea'";
		}
		$sql.= " ORDER BY dea.name_dea";
		//Debug::print_r($sql); die;
		$result = $this->db->Execute($sql);

		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/*	 * *********************************************
	 * funcion getPaymentBranches
	 * gets all the Branches that have pending invoice for payments
	 * ********************************************* */

	function getPaymentBranches($serial_dea) {
		$sql = "SELECT DISTINCT d.serial_dea, d.name_dea, d.code_dea
				 FROM dealer dea
				 JOIN dealer d ON dea.serial_dea=d.dea_serial_dea
				 JOIN counter cnt ON cnt.serial_dea=d.serial_dea
				 JOIN sales s ON cnt.serial_cnt=s.serial_cnt
				 JOIN invoice i ON i.serial_inv=s.serial_inv AND i.status_inv='STAND-BY'
				 WHERE d.dea_serial_dea='" . $serial_dea . "'
				 ORDER BY d.name_dea";	
//echo $sql;
		$result = $this->db->Execute($sql);

		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/*	 * *********************************************
	 * funcion getPaymentBranches
	 * gets all the Branches that have pending invoice for payments
	 * ********************************************* */

	function getInvoiceBranches($dea_serial_dea, $serial_dea = NULL) {
		$sql = "SELECT DISTINCT d.serial_dea, d.name_dea, d.code_dea
				FROM dealer dea
				JOIN dealer d ON dea.serial_dea=d.dea_serial_dea
				JOIN sector sec ON d.serial_sec=sec.serial_sec
				JOIN city c ON sec.serial_cit=c.serial_cit
				JOIN user_by_city ubc ON ubc.serial_cit=c.serial_cit
				JOIN counter cnt ON cnt.serial_dea=d.serial_dea
				JOIN sales s ON cnt.serial_cnt=s.serial_cnt AND s.status_sal='REGISTERED' AND s.free_sal ='NO' AND s.serial_inv IS NULL
				JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
				JOIN product p ON p.serial_pro=pxc.serial_pro AND ((p.masive_pro='NO') OR (p.masive_pro='YES' AND s.sal_serial_sal IS NOT NULL))
				WHERE d.dea_serial_dea='$dea_serial_dea'";
		if ($serial_dea != NULL) {
			$sql .= " AND d.serial_dea = $serial_dea";
		}
		$sql .= " ORDER BY d.name_dea";

		$result = $this->db->Execute($sql);

		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/**
	  @Name: getSalesForInvoice
	  @Description: Returns an array of sales availables for invoice.
	  @Params: class atribute serial_dea
	  @Returns: Sales array
	 * */
	function getSalesForInvoice() {
		$sql = "SELECT s.serial_pbd,s.serial_sal,s.card_number_sal,pbl.name_pbl, 
						cus.first_name_cus as legal_entity_name, 
						CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name,
						DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y')as emission_date,s.total_sal,
						s.status_sal,pxc.serial_pxc, IF(s.id_erp_sal IS NULL, 'NO', 'YES') AS 'on_erp',
						mbc.serial_man
				FROM sales s
				JOIN counter c ON s.serial_cnt=c.serial_cnt
				JOIN dealer d ON d.serial_dea=c.serial_dea AND c.serial_dea = {$this->serial_dea}
				JOIN manager_by_country mbc ON mbc.serial_mbc = d.serial_mbc
				JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
				JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
				JOIN product pr ON pr.serial_pro=pxc.serial_pro AND ((pr.masive_pro='NO') OR (pr.masive_pro='YES' AND s.sal_serial_sal IS NOT NULL))
				JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				JOIN customer cus ON cus.serial_cus=s.serial_cus
				WHERE s.status_sal='REGISTERED' AND s.free_sal ='NO'
				AND s.serial_inv IS NULL
				AND s.serial_sal NOT IN (SELECT DISTINCT sl.serial_sal
										 FROM sales_log sl
										 WHERE status_slg = 'PENDING')
				ORDER BY s.emission_date_sal DESC,s.card_number_sal";
		//die(Debug::print_r($sql));
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$list = array();
			$cont = 0;

			do {
				$list[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $list;
	}

	/**
	  @Name: getDealersAutocompleter
	  @Description: Returns an array of dealers.
	  @Params: Dealer name or pattern.
	  @Returns: Dealer array
	 * */
	function getDealersAutocompleter($name_dea, $serial_mbc, $serial_dea = NULL) {
		$sql = "SELECT d.serial_dea, d.name_dea, d.code_dea
				FROM dealer d
				WHERE ((d.name_dea) LIKE '%" . utf8_encode($name_dea) . "%'
				OR (d.code_dea) LIKE '%" . utf8_encode($name_dea) . "%')
				AND d.dea_serial_dea IS NULL
				AND d.phone_sales_dea = 'NO'";

		if ($serial_dea != NULL) {
			$sql .= " AND d.serial_dea = $serial_dea";
		}

		if ($serial_mbc != '1') {
			$sql .= " AND d.serial_mbc = $serial_mbc";
		}
		$sql .= " LIMIT 10";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/**
	 * @Name: getPromoDealers
	 * @Description: Returns an array of dealers for assigning them a new promotion.
	 * @Params: $serial_cit: specific city,
	 *          $products: a specific list of products sold in their home country.
	 * @Returns: Dealer array
	 * */
	function getPromoDealers($serial_cit, $products, $bannedDealers = NULL) {
		$sql = "SELECT d.serial_dea, d.name_dea
				FROM dealer d
				JOIN sector s ON s.serial_sec=d.serial_sec AND s.serial_cit = '" . $serial_cit . "'
				WHERE dea_serial_dea IS NULL
				AND d.serial_dea IN (SELECT DISTINCT pbd.serial_dea
									 FROM product_by_dealer pbd
									 WHERE serial_pxc IN (" . $products . "))";

		if ($bannedDealers != NULL && $bannedDealers != '') {
			$sql.=" AND d.serial_dea NOT IN (" . $bannedDealers . ")";
		}

		$sql.=" ORDER BY d.name_dea";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/**
	  @Name: existDealerCode
	  @Description: Verifies the existance of a dealer with the same code
	  in DB for the same country.
	  @Params: $code_dea (code), $serial_dea (ID)
	  @Returns: TRUE if exists / FALSE if not
	 * */
	function existDealerCode($code_dea, $serial_dea = NULL) {
		$sql = "SELECT serial_dea
				 FROM dealer
				 WHERE LOWER(code_dea)= _utf8'" . utf8_encode($code_dea) . "' collate utf8_bin";
		if ($serial_dea != NULL) {
			$sql.=" AND serial_dea <> " . $serial_dea;
		}

		$result = $this->db->Execute($sql);

		if ($result->fields[0]) {
			return true;
		} else {
			return false;
		}
	}

	/**
	  @Name: BranchNumber
	  @Description: Verifies the existance of a branch with the same code
	  in DB for the same dealer.
	  @Params: $branch_number_dea, $serial_dea, $dea_derial_dea
	  @Returns: TRUE if exists / FALSE if not
	 * */
	function existBranchNumber($branch_number_dea, $dea_serial_dea, $serial_dea = NULL) {
		$sql = "SELECT serial_dea
				 FROM dealer
				 WHERE LOWER(branch_number_dea)= _utf8'" . utf8_encode($branch_number_dea) . " collate utf8_bin'
				 AND dea_serial_dea='" . $dea_serial_dea . "'";
		if ($serial_dea != NULL) {
			$sql.=" AND serial_dea <> " . $serial_dea;
		}
		$result = $this->db->Execute($sql);

		if ($result->fields[0]) {
			return true;
		} else {
			return false;
		}
	}

	/**
	  @Name: existDealerEmail
	  @Description: Verifies the existance of a dealer with the same email
	  in DB for the same country.
	  @Params: $email_dea (code), $serial_dea (ID)
	  @Returns: TRUE if exists / FALSE if not
	 * */
	function existDealerEmail($email_dea, $serial_dea = NULL, $serial_father = NULL) {
		$sql = "SELECT d.serial_dea
                        FROM dealer d
                        WHERE ( LOWER(d.email1_dea)= _utf8'" . utf8_encode($email_dea) . "' collate utf8_bin
                                OR LOWER(d.email2_dea)= _utf8'" . utf8_encode($email_dea) . "' collate utf8_bin)
                        AND d.status_dea = 'ACTIVE'";

		if ($serial_father != NULL) {
			$sql.="  AND d.email1_dea NOT IN ( (SELECT email1_dea
                                                            FROM dealer p
                                                            WHERE p.dea_serial_dea ='" . $serial_padre . "')
                                                            UNION (
                                                            SELECT email2_dea
                                                            FROM dealer p
                                                            WHERE p.dea_serial_dea ='" . $serial_padre . "'
                                                            ))
				 AND d.email2_dea NOT IN ( (SELECT email1_dea
                                                            FROM dealer p
                                                            WHERE p.dea_serial_dea ='" . $serial_padre . "')
                                                            UNION (
                                                            SELECT email2_dea
                                                            FROM dealer p
                                                            WHERE p.dea_serial_dea ='" . $serial_padre . "'
                                                            ))";
		}

		if ($serial_dea != NULL) {
			$sql.=" AND d.serial_dea <> " . $serial_dea;
		}
		//echo $sql;

		$result = $this->db->Execute($sql);

		if ($result->fields[0]) {
			return true;
		} else {
			return false;
		}
	}

	/**
	  @Name: getStatusList
	  @Description: Gets all credit days status in DB.
	  @Params: N/A
	  @Returns: Status array
	 * */
	function getStatusList() {
		$sql = "SHOW COLUMNS FROM dealer LIKE 'status_dea'";
		$result = $this->db->Execute($sql);

		$type = $result->fields[1];
		$type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
		return $type;
	}

	/**
	  @Name: getStatusList
	  @Description: Gets all status in DB.
	  @Params: N/A
	  @Returns: Status array
	 * */
	function getCategories() {
		$sql = "SHOW COLUMNS FROM dealer LIKE 'category_dea'";
		$result = $this->db->Execute($sql);

		$type = $result->fields[1];
		$type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
		return $type;
	}

	/**
	  @Name: getStatusList
	  @Description: Gets all dedline types in DB.
	  @Params: N/A
	  @Returns: Status array
	 * */
	function getDedline() {
		$sql = "SHOW COLUMNS FROM dealer LIKE 'payment_deadline_dea'";
		$result = $this->db->Execute($sql);

		$type = $result->fields[1];
		$type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
		return $type;
	}

	/**
	  @Name: getPercentageType
	  @Description: Gets all percentage types in DB.
	  @Params: N/A
	  @Returns: Status array
	 * */
	function getPercentageType() {
		$sql = "SHOW COLUMNS FROM dealer LIKE 'type_percentage_dea'";
		$result = $this->db->Execute($sql);

		$type = $result->fields[1];
		$type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
		return $type;
	}

	/*	 * *********************************************
	 * funcion updateManager
	 * updates a register in DB
	 * ********************************************* */

	function updateManager($serial_mbc, $serial_mbc_new) {
		$sql = "
				UPDATE dealer SET serial_mbc		= '" . $serial_mbc_new . "'";
		$sql .=" WHERE serial_mbc = '" . $serial_mbc . "'";
		//echo $sql;
		$result = $this->db->Execute($sql);
		if ($result === false)
			return false;
		if ($result == true)
			return true;
		else
			return false;
	}

	/*	 * *********************************************
	 * funcion updateManager
	 * updates a register in DB
	 * ********************************************* */

	function updateManagerAssign($serial_mbc) {
		$sql = "
				UPDATE dealer SET serial_mbc		= '" . $serial_mbc . "'";
		$sql .=" WHERE serial_dea = '" . $this->serial_dea . "'";
		$sql .=" OR dea_serial_dea= '" . $this->serial_dea . "'";
		//echo $sql;
		$result = $this->db->Execute($sql);
		if ($result === false)
			return false;
		if ($result == true)
			return true;
		else
			return false;
	}

	/*	 * *********************************************
	 * funcion getNumeberOfDealers
	 * Returns how many dealers have the same manager
	 * ********************************************* */

	function getNumeberOfDealers($serial_mbc) {
		$sql = " SELECT COUNT(serial_dea)
								 FROM dealer";
		$sql .=" WHERE serial_mbc = '" . $serial_mbc . "'";
		//echo $sql;
		$result = $this->db->Execute($sql);
		if ($result === false)
			return false;

		if ($result == true)
			return $result->fields[0];
		else
			return false;
	}

	/**
	  @Name: getLastBranchNumber
	  @Description: Gets the last Branch Number inserted on DB.
	  @Params: N/A
	  @Returns: value
	 * */
	function getLastBranchNumber($serial_dea) {
		$sql = "SELECT max(d.branch_number_dea)
                            FROM dealer d
                            WHERE d.dea_serial_dea=" . $serial_dea;
		$result = $this->db->Execute($sql);

		$lastNumber = $result->fields[0];
		return $lastNumber;
	}
       
	/*	 * *********************************************
	 * funcion getDealerCity
	 * Description: Returns the city of a dealer, and part of the Seller Code
	 * Returns: array with cit.serial_cit
	 *                    cit.name_cit
	 *                    cit.code_cit
	 *                    cou.code_cou
	 *                    cou.name_cou
	 *                    cou.serial_cou
	 * ********************************************* */

	public static function getDealerCity($db, $serial_cnt) {
		/* $sql = " SELECT cit.serial_cit, cit.name_cit, cit.code_cit, cou.code_cou, cou.name_cou, cou.serial_cou
		  FROM city cit
		  JOIN sector sec ON sec.serial_sec = cit.serial_cit
		  JOIN country cou ON cou.serial_cou = cit.serial_cou
		  JOIN dealer dea ON dea.serial_sec = sec.serial_sec
		  JOIN counter cnt ON cnt.serial_dea = dea.serial_dea
		  JOIN user usr ON usr.serial_usr = cnt.serial_usr ";
		  $sql .=" WHERE usr.serial_usr= '".$serial_usr."'"; */

		$sql = " SELECT cit.serial_cit,
	                                       cit.name_cit,
	                                       cit.code_cit,
	                                       cou.code_cou,
	                                       cou.name_cou,
	                                       cou.serial_cou
	                            FROM counter cnt
	                            JOIN dealer dea ON cnt.serial_dea = dea.serial_dea
	                            JOIN sector sec ON sec.serial_sec=dea.serial_sec
	                            JOIN city cit ON cit.serial_cit=sec.serial_cit
	                            JOIN country cou ON cou.serial_cou=cit.serial_cou
	                            WHERE cnt.serial_cnt= '" . $serial_cnt . "'";

		$result = $db->getRow($sql);
		return $result;
	}

	/*	 * *********************************************
	 * funcion getManagersByCity
	 * Description: Returns the responsables by city
	 * Returns: array with serial_usr, name_usr
	 * ********************************************* */

	function getResponsablesByCity($serial_cit, $serial_mbc) {
		$sql = "SELECT DISTINCT ubd.serial_usr, CONCAT( u.first_name_usr, ' ', u.last_name_usr ) name_usr
            FROM dealer d
            JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea
            JOIN user u ON u.serial_usr = ubd.serial_usr
            JOIN sector s ON s.serial_sec = d.serial_sec
            AND s.serial_cit ='" . $serial_cit . "'";

		if ($serial_mbc != '1') {
			$sql.=" AND d.serial_mbc=" . $serial_mbc;
		}

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/*	 * *********************************************
	 * funcion getResponsablesByCityWithComissions
	 * Description: Returns the responsables who has pending comissions by city
	 * Parameters: serial_cit: serial of the city to search on
	 *
	 * Returns: array with serial_usr, name_usr
	 * ********************************************* */

	function getResponsablesByCityWithComissions($serial_cit, $serial_mbc, $is_report = false) {
		$sql = "SELECT DISTINCT ubd.serial_usr, CONCAT( u.first_name_usr, ' ', u.last_name_usr ) name_usr
            FROM user_by_dealer ubd
            JOIN dealer d ON d.serial_dea=ubd.serial_dea AND ubd.status_ubd = 'ACTIVE'";

		if ($serial_mbc != '1') {
			$sql.=" AND d.serial_mbc=" . $serial_mbc;
		}

		$sql.=" JOIN user u ON u.serial_usr = ubd.serial_usr AND u.status_usr = 'ACTIVE'
            JOIN sector s ON s.serial_sec = d.serial_sec
			JOIN applied_comissions com ON com.serial_ubd=ubd.serial_ubd AND com.value_ubd_com>0";
		if (!$is_report) {
			$sql.=" AND com.creation_date_com>ubd.last_commission_paid_ubd";
		}
		$sql.=" AND s.serial_cit ='" . $serial_cit . "'";
		$result = $this->db->Execute($sql);
		if ($result)
			return $result;
		else
			return false;
	}

	/*	 * *********************************************
	 * funcion getManagersByCity
	 * Description: Returns the responsables by city
	 * Returns: array with serial_usr, name_usr
	 * ********************************************* */

	function getPaymentResponsablesByCity($serial_cit, $serial_mbc, $serial_dea = NULL) {
		$sql = "SELECT DISTINCT ubd.serial_usr, CONCAT( u.first_name_usr, ' ', u.last_name_usr ) name_usr
            FROM dealer d
            JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea
            JOIN user u ON u.serial_usr = ubd.serial_usr
            JOIN sector s ON s.serial_sec = d.serial_sec
            JOIN counter cnt ON cnt.serial_dea=d.serial_dea
            JOIN sales sal ON cnt.serial_cnt=sal.serial_cnt
            JOIN invoice i ON i.serial_inv=sal.serial_inv AND (i.status_inv='STAND-BY')
            AND s.serial_cit ='" . $serial_cit . "'";

		if ($serial_mbc != '1') {
			$sql.=" AND d.serial_mbc=" . $serial_mbc;
		}

		if ($serial_dea) {
			$sql.=" AND ubd.serial_dea=" . $serial_dea;
		}

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/*	 * *********************************************
	 * funcion getInvoicesByBranch
	 * Description: Returns the responsables by city
	 * Returns: array with serial_usr, name_usr
	 * ********************************************* */

	function getInvoicesByBranch($payphone_id = null, $forAnnulment = null) {
		if ($this->serial_dea) {
			$sql = "(
                SELECT GROUP_CONCAT(DISTINCT i.serial_inv ORDER BY i.date_inv ASC SEPARATOR ',') serial_inv,
				rp.name AS 'nombre',
                rp.serial_sal AS 'status',
                           GROUP_CONCAT(DISTINCT DATE_FORMAT(i.date_inv, '%d/%m/%Y') ORDER BY i.date_inv ASC SEPARATOR '<br>') date_inv,
                           GROUP_CONCAT( DISTINCT i.number_inv ORDER BY i.date_inv ASC SEPARATOR '<br>' ) number_inv,
                           GROUP_CONCAT(DISTINCT IFNULL(CONCAT( cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'') ), d.name_dea) ORDER BY i.date_inv ASC SEPARATOR '<br>') invoice_to,
                           GROUP_CONCAT(DISTINCT DATEDIFF( NOW( ) , i.date_inv ) ORDER BY i.date_inv ASC SEPARATOR '<br>') days,
                           GROUP_CONCAT(DISTINCT i.serial_cus ORDER BY i.date_inv ASC SEPARATOR ',') serial_cus,
                           GROUP_CONCAT(DISTINCT i.serial_dea ORDER BY i.date_inv ASC SEPARATOR ',') serial_dea,
                           GROUP_CONCAT(DISTINCT i.status_inv ORDER BY i.date_inv ASC SEPARATOR ',') status_inv,
                           p.serial_pay,
                           IFNULL( p.total_to_pay_pay - p.total_payed_pay, i.total_inv ) total_to_pay,
                           ( p.total_to_pay_pay - p.total_payed_pay ) total_to_pay_fee,
                           IFNULL( p.total_payed_pay, 0.00 ) total_payed,
                           p.status_pay,
                           SUM(i.total_inv) total_inv ";
				$sql .= ($payphone_id? ", payphone_id " : ""); 
                $sql .= " FROM invoice i
                JOIN sales s ON s.serial_inv = i.serial_inv
                JOIN counter c ON c.serial_cnt = s.serial_cnt
                LEFT JOIN dealer d ON d.serial_dea = i.serial_dea
                LEFT JOIN customer cus ON cus.serial_cus = i.serial_cus
				LEFT JOIN record_payment rp ON rp.serial_inv = i.serial_inv
                LEFT JOIN payments p ON p.serial_pay = i.serial_pay ";
				$sql .= ($forAnnulment? " WHERE (i.status_inv = 'PAID') " : " WHERE (i.status_inv = 'STAND-BY') ");
                $sql .= " AND c.serial_dea = '" . $this->serial_dea . "' ";
				$sql .= ($forAnnulment? " AND p.status_pay = 'PAID' " : " AND (
                        p.status_pay = 'PARTIAL'
                        OR p.status_pay = 'VOID'
                        OR p.status_pay IS NULL
						OR p.status_pay = 'ONLINE_PENDING'
                ) ");	
                $sql.= " AND i.serial_pay IS NOT NULL ";
				$sql .= ($payphone_id? " AND payphone_id IS NOT NULL " : " AND payphone_id IS NULL ");	
				$sql .= " GROUP BY p.serial_pay HAVING p.serial_pay IS NOT NULL
        )
        UNION
        (
                SELECT i.serial_inv,
                           DATE_FORMAT(i.date_inv,'%d/%m/%Y') as date_inv,
						   rp.name AS 'nombre',
                rp.serial_sal AS 'status',
                           i.number_inv number_inv,
                           IFNULL(CONCAT( cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'') ), d.name_dea) invoice_to,
                           DATEDIFF( NOW( ) , i.date_inv ) days,
                           i.serial_cus,
                           i.serial_dea,
                           i.status_inv,
                           p.serial_pay,
                           IFNULL( p.total_to_pay_pay - p.total_payed_pay, i.total_inv ) total_to_pay,
                           ( p.total_to_pay_pay - p.total_payed_pay ) total_to_pay_fee,
                           IFNULL( p.total_payed_pay, 0.00 ) total_payed,
                           p.status_pay,
                           i.total_inv";
				$sql .= ($payphone_id? ", payphone_id " : "");
                $sql .= " FROM invoice i
                JOIN sales s ON s.serial_inv = i.serial_inv
                JOIN counter c ON c.serial_cnt = s.serial_cnt
                LEFT JOIN dealer d ON d.serial_dea = i.serial_dea
                LEFT JOIN customer cus ON cus.serial_cus = i.serial_cus
				LEFT JOIN record_payment rp ON rp.serial_inv = i.serial_inv
                LEFT JOIN payments p ON p.serial_pay = i.serial_pay ";
				$sql .= ($forAnnulment? " WHERE (i.status_inv = 'PAID') " : " WHERE (i.status_inv = 'STAND-BY') ");
                $sql .= " AND c.serial_dea = '" . $this->serial_dea . "' ";
				$sql .= ($forAnnulment? " AND p.status_pay = 'PAID' " : " AND (
                        p.status_pay = 'PARTIAL'
                        OR p.status_pay = 'VOID'
                        OR p.status_pay IS NULL
						OR p.status_pay = 'ONLINE_PENDING'
                ) ");	
                $sql.= " AND i.serial_pay IS NULL ";
				$sql .= ($payphone_id? " AND payphone_id IS NOT NULL " : " AND payphone_id IS NULL ");	
		$sql .= " )
        ORDER BY 1";
//			die('<pre>'.$sql.'</pre>');

			$result = $this->db->Execute($sql);
			$num = $result->RecordCount();
			if ($num > 0) {
				$arreglo = array();
				$cont = 0;

				do {
					$arreglo[$cont] = $result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				} while ($cont < $num);
			}

			return $arreglo;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * funcion getInvoicesByBranch
	 * Description: Returns the responsables by city
	 * Returns: array with serial_usr, name_usr
	 * ********************************************* */

	function getInvoicesBySerials($invoicesIds = NULL) {
		if ($this->serial_dea) {
			$sql = "SELECT DISTINCT i.serial_inv,
                                                       i.date_inv,
                                                       i.number_inv number_inv,
                                                       IFNULL(CONCAT( cus.first_name_cus, ' ', IFNULL(cus.last_name_cus,'') ), d.name_dea) invoice_to,
                                                       DATEDIFF( NOW( ) , i.date_inv ) days,
                                                       i.status_inv,
                                                       p.serial_pay,
                                                       cus.serial_cus,
                                                       IFNULL( p.total_to_pay_pay - p.total_payed_pay, i.total_inv ) total_to_pay,
                                                       ( p.total_to_pay_pay - p.total_payed_pay ) total_to_pay_fee,
                                                       IFNULL( p.total_payed_pay, 0.00 ) total_payed,
                                                       p.status_pay,
                                                       i.total_inv
                                            FROM invoice i
                                            JOIN sales s ON s.serial_inv = i.serial_inv
                                            JOIN counter c ON c.serial_cnt = s.serial_cnt
                                            JOIN dealer d ON d.serial_dea = c.serial_dea
                                            LEFT JOIN customer cus ON cus.serial_cus = i.serial_cus
                                            LEFT JOIN payments p ON p.serial_pay = i.serial_pay
                                            WHERE (i.status_inv = 'STAND-BY')
                                            AND c.serial_dea = '" . $this->serial_dea . "'
                                            AND (
                                                    p.status_pay = 'PARTIAL'
                                                    OR p.status_pay = 'VOID'
                                                    OR p.status_pay IS NULL
													OR p.status_pay = 'ONLINE_PENDING'
                                            )";
			if ($invoicesIds) {
				$sql .= " AND i.serial_inv IN (" . $invoicesIds . ")";
			}
			$sql .= " ORDER BY date_inv, total_to_pay DESC";
			//Debug::print_r($sql);
			$result = $this->db->Execute($sql);
			$num = $result->RecordCount();
			if ($num > 0) {
				$arreglo = array();
				$cont = 0;

				do {
					$arreglo[$cont] = $result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				} while ($cont < $num);
			}

			return $arreglo;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * funcion getDealerByPayment
	 * Description: Returns the dealer in charge of an specific payment
	 * Returns: dealer serial
	 * ********************************************* */

	function getDealerByPayment($serial_pay) {
		$sql = "SELECT dea.serial_dea
                    FROM dealer dea
                    JOIN counter cnt ON cnt.serial_dea=dea.serial_dea
                    JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
                    JOIN invoice inv ON sal.serial_inv=inv.serial_inv
                    JOIN payments pay ON pay.serial_pay=inv.serial_pay AND pay.serial_pay='" . $serial_pay . "'";
		//die($sql);
		$result = $this->db->Execute($sql);
		$arr = array();
		$arr = $result->GetRowAssoc(false);

		return $arr['serial_dea'];
	}

	/*	 * *********************************************
	 * funcion checkExcessPayment
	 * Description: Returns the responsables by city
	 * Returns: true if the customer or dealer has a payment with excess
	 *        false if not
	 * ********************************************* */

	function checkExcessPayment($serial_dea = null, $serial_cus = null) {
		$sql = "SELECT DISTINCT p.serial_pay, p.excess_amount_available_pay
                    FROM payments p
                    JOIN invoice i ON i.serial_pay = p.serial_pay ";

		if ($serial_cus) {
			$sql.="JOIN customer c ON c.serial_cus = i.serial_cus
                    WHERE c.serial_cus = " . $serial_cus;
		} else if ($serial_dea) {
			$sql.="JOIN dealer d ON d.serial_dea = i.serial_dea
                       WHERE d.serial_dea = " . $serial_dea;
		} else {
			return false;
		}
		$sql.=" AND p.status_pay='EXCESS' AND excess_amount_available_pay>0";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();

		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/*	 * *********************************************
	 * funcion getLastComissionPaymentDate
	 * 
	 * ********************************************* */

	public static function getLastComissionPaymentDate($db, $serial_dea) {
		$sql = "SELECT d.last_commission_paid_dea
                    FROM dealer d
                    WHERE d.serial_dea='$serial_dea'";

		$result = $db->getOne($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * funcion updateLastComissionPaid
	 * updates the last date a comission was paid.
	 * * Parameter: atribute serial_dea
	 * ********************************************* */

	function updateLastComissionPaid() {
		$sql = "
                UPDATE dealer SET last_commission_paid_dea = NOW() WHERE serial_dea = '" . $this->serial_dea . "'";
		//die($sql);
		$result = $this->db->Execute($sql);
		if ($result === false)
			return false;
		if ($result == true)
			return true;
		else
			return false;
	}

	/*	 * *********************************************
	 * @Name: getDealersWithExcessPayments
	 * @Description: Returns all the dealers with excess payments.
	 * @Returns: An array of dealers.
	 * ********************************************* */

	public static function getDealersWithExcessPayments($db, $serial_cit, $serial_usr, $cat_dealer = NULL, $serial_dea = NULL) {
		$sql = "SELECT DISTINCT d.serial_dea, d.name_dea, d.code_dea
				FROM dealer b
				JOIN dealer d ON d.serial_dea=b.dea_serial_dea
				JOIN sector s ON s.serial_sec=b.serial_sec AND s.serial_cit='$serial_cit'
				JOIN user_by_dealer ubd ON ubd.serial_dea=b.serial_dea AND ubd.serial_usr='$serial_usr'
				JOIN counter c ON c.serial_dea=b.serial_dea
				JOIN sales sal ON sal.serial_cnt=c.serial_cnt
				JOIN invoice i ON i.serial_inv=sal.serial_inv AND i.status_inv='PAID'
				JOIN payments p ON p.serial_pay=i.serial_pay AND p.status_pay='EXCESS' AND excess_amount_available_pay>0
				WHERE d.phone_sales_dea = 'NO'";

		if ($cat_dealer != 'false' and $cat_dealer != NULL) {
			$sql.=" AND b.category_dea='$cat_dealer'";
		}
		if ($serial_dea != NULL) {
			$sql.=" AND d.serial_dea='$serial_dea'";
		}
		
		$sql .= " ORDER BY name_dea";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	/**
	 * @name getDealersWithProducts
	 * @param DB_connection $db
	 * @param ManagerByCountry_ID $serial_mbc
	 * @return Array List_of_Dealers
	 * @static
	 */
	public static function getDealersWithProducts($db, $serial_mbc, $serial_dea = NULL) {
		$sql = "SELECT DISTINCT d.serial_dea, d.name_dea, d.code_dea
				FROM dealer d
				JOIN product_by_dealer pbd ON pbd.serial_dea=d.serial_dea AND d.serial_mbc=$serial_mbc";
		
		if($serial_dea):
			$sql .= " AND d.serial_dea = $serial_dea";
		endif;
				
		$sql .= " ORDER BY d.name_dea";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * @Name: getBranchesWithExcessPayments
	 * @Description: Returns all the branches with excess payments.
	 * @Returns: An array of dealers.
	 * ********************************************* */

	public static function getBranchesWithExcessPayments($db, $dea_serial_dea, $serial_dea = NULL) {
		$sql = "SELECT DISTINCT b.serial_dea, b.name_dea, b.code_dea
				FROM dealer b
				JOIN counter c ON c.serial_dea = b.serial_dea
				JOIN sales sal ON sal.serial_cnt = c.serial_cnt
				JOIN invoice i ON i.serial_inv = sal.serial_inv AND i.status_inv = 'PAID'
				JOIN payments p ON p.serial_pay = i.serial_pay AND p.status_pay = 'EXCESS' AND excess_amount_available_pay > 0
				WHERE b.dea_serial_dea = '$dea_serial_dea'
				AND b.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql.=" AND b.serial_dea='$serial_dea'";
		}

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * @Name: getDealersWithFreeSales
	 * @Description: Returns all the dealers with pending free sales.
	 * @Returns: An array of dealers.
	 * ********************************************* */

	public static function getDealersWithFreeSales($db, $serial_usr, $serial_cit, $serial_dea = NULL) {
		$sql = "SELECT DISTINCT d.serial_dea, d.name_dea, d.code_dea
				FROM dealer b
				JOIN dealer d ON d.serial_dea=b.dea_serial_dea
				JOIN sector s ON s.serial_sec=b.serial_sec AND s.serial_cit='$serial_cit'
				JOIN user_by_dealer ubd ON ubd.serial_dea=b.serial_dea AND ubd.serial_usr='$serial_usr'
				JOIN counter c ON c.serial_dea=b.serial_dea
				JOIN sales sal ON sal.serial_cnt=c.serial_cnt AND sal.status_sal='REQUESTED' AND sal.free_sal='YES'
				WHERE d.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND d.serial_dea = $serial_dea";
		}

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	/*	 * *********************************************
	 * @Name: getDealersWithFreeSales
	 * @Description: Returns all the dealers with pending free sales.
	 * @Returns: An array of dealers.
	 * ********************************************* */

	public static function getBranchesWithFreeSales($db, $dea_serial_dea, $serial_dea = NULL) {
		$sql = "SELECT DISTINCT b.serial_dea, b.name_dea, b.code_dea
				FROM dealer b
				JOIN counter c ON c.serial_dea=b.serial_dea
				JOIN sales sal ON sal.serial_cnt=c.serial_cnt AND sal.status_sal='REQUESTED' AND sal.free_sal='YES'
				WHERE b.dea_serial_dea='$dea_serial_dea'
				AND b.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND b.serial_dea = $serial_dea";
		}
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	/**
	  @Name: getDealersByInvoice
	  @Description: Retrieves the information of a specific dealer's list
	  @Params: Serial number of a country and serial number of an user
	  @Returns: Dealers array
	 * */
	function getDealersByInvoice($serial_cou, $serial_usr, $serial_dea = NULL) {
		$sql = "SELECT DISTINCT (dea.serial_dea), dea.name_dea, dea.code_dea
					FROM sales s
					JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
					JOIN dealer b ON cnt.serial_dea = b.serial_dea
					JOIN dealer dea ON dea.serial_dea = b.dea_serial_dea
					JOIN sector sec ON dea.serial_sec = sec.serial_sec
					JOIN city cit ON sec.serial_cit = cit.serial_cit
					JOIN user us ON cnt.serial_usr = us.serial_usr
					LEFT JOIN invoice i ON i.serial_inv = s.serial_inv
					WHERE us.serial_usr = $serial_usr
					AND cit.serial_cou = $serial_cou
					AND dea.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND dea.serial_dea = $serial_dea";
		}

		$sql .= " ORDER BY dea.name_dea";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$arreglo = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	/**
	  @Name: getBranchesByInvoice
	  @Description: Retrieves the information of a specific branch's list
	  @Params: Serial number of a dealer and serial number of a city
	  @Returns: Branches array
	 * */
	function getBranchesByInvoice($serial_dea, $serial_cit, $serial_dea = NULL) {
		$sql = "SELECT DISTINCT (dea.serial_dea), dea.name_dea, dea.code_dea
					FROM sales s
					JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
					JOIN dealer dea ON cnt.serial_dea = dea.serial_dea
					JOIN sector sec ON dea.serial_sec = sec.serial_sec
					JOIN city cit ON sec.serial_cit = cit.serial_cit
					JOIN user us ON cnt.serial_usr = us.serial_usr
					LEFT JOIN invoice i ON i.serial_inv = s.serial_inv
					WHERE cit.serial_cit = $serial_cit AND dea_serial_dea = $serial_dea
					AND dea.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND dea.serial_dea = $serial_dea";
		}
		$sql .= " ORDER BY dea.name_dea";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$arreglo = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	/**
	  @Name: getDealersByManagerWithInvoices
	  @Description: Retrieves the information of a specific dealer's list
	  @Params: Serial number of a manager
	  @Returns: Dealers array
	 * */
	function getDealersByManagerWithInvoices($serial_mbc, $cat_dealer, $serial_dea = NULL) {
		$f1 = '';
		if ($cat_dealer) {
			$f1 = "AND dea.category_dea='" . $cat_dealer . "'";
		}

		$sql = "SELECT distinct(dea.serial_dea),dea.name_dea, dea.code_dea
				 FROM sales s
				   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				   JOIN dealer br ON cnt.serial_dea=br.serial_dea
				   JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
                   JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc AND mbc.serial_mbc = $serial_mbc
				   WHERE  s.serial_inv IS NOT NULL $f1
				   AND dea.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND dea.serial_dea = $serial_dea";
		}
		$sql .= " ORDER BY dea.name_dea";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$arreglo = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	public static function getDealersWithPaymentsByComissionist($db, $serial_usr = NULL, $serial_cit = NULL) {
		if ($serial_cit || $serial_usr) {
			$serial_cit = ($serial_cit != '') ? " AND sec.serial_cit = $serial_cit" : "";
			$serial_usr = ($serial_usr != '') ? " AND ubd.serial_usr = $serial_usr" : "";


			$sql = "	SELECT distinct(dea.serial_dea),dea.name_dea, dea.code_dea
						FROM dealer dea
						JOIN dealer bra ON bra.dea_serial_dea = dea.serial_dea
						JOIN sector sec ON sec.serial_sec = bra.serial_sec $serial_cit
						JOIN user_by_dealer ubd ON ubd.serial_dea = bra.serial_dea $serial_usr
						JOIN counter cnt ON cnt.serial_dea = ubd.serial_dea
						JOIN sales sal ON sal.serial_cnt = cnt.serial_cnt
						JOIN invoice inv ON inv.serial_inv = sal.serial_inv AND inv.status_inv = 'PAID' 
						WHERE 1 
						ORDER BY dea.name_dea";
                        
			$result = $db->Execute($sql);
			$num = $result->RecordCount();
			$dealersList = array();
			if ($num > 0) {
				$cont = 0;
				do {
					$dealersList[$cont++] = $result->GetRowAssoc(false);
					$result->MoveNext();
				} while ($cont < $num);
			}
		}

		return $dealersList;
	}

	/**
	  @Name: getDealersByManagerAndCityAndComissionistAndCategory
	  @Description: Retrieves the information of a specific dealer's list
	  @Returns: Dealers array
	 * */
	public static function getDealersByManagerAndCityAndComissionistAndCategory($db, $serial_mbc, $serialComissionist, $category, $city, $serial_dea = NULL) {
		$categoryQuery = $category ? "AND dea.category_dea = '" . $category . "'" : '';
		$cityQuery = $city ? "JOIN sector sec ON dea.serial_sec = sec.serial_sec AND sec.serial_cit = $city" : '';
                
		$sql = "SELECT distinct(dea.serial_dea),dea.name_dea, dea.code_dea
				 FROM sales s
				   JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				   JOIN dealer br ON cnt.serial_dea=br.serial_dea
				   JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
                   JOIN manager_by_country mbc ON mbc.serial_mbc=br.serial_mbc AND mbc.serial_mbc = $serial_mbc
                   JOIN user_by_dealer ubd ON ubd.serial_usr = $serialComissionist AND ubd.status_ubd = 'ACTIVE' AND ubd.serial_dea = br.serial_dea
                   $cityQuery
				   WHERE  s.serial_inv IS NOT NULL 
                   $categoryQuery
				   AND dea.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND dea.serial_dea = $serial_dea";
		}
		$sql .= " ORDER BY dea.name_dea";

		//die($sql);
		$result = $db->Execute($sql);
		$num = $result->RecordCount();
		$resultArray = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$resultArray[$cont++] = $result->GetRowAssoc(false);
				$result->MoveNext();
			} while ($cont < $num);
		}
		return $resultArray;
	}

	/**
	  @Name: getBranchesByDealerWithInvoices
	  @Description: Retrieves the information of a specific dealer's list
	  @Params: Serial number of a dealer
	  @Returns: Dealers array
	 * */
	function getBranchesByDealerWithInvoices($dea_serial_dea, $serial_dea = NULL) {
		$sql = "SELECT distinct(dea.serial_dea),dea.name_dea, dea.code_dea
				 FROM sales s
				 JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				 JOIN dealer dea ON cnt.serial_dea=dea.serial_dea
                 JOIN manager_by_country mbc ON mbc.serial_mbc=dea.serial_mbc
                 JOIN manager man ON man.serial_man=mbc.serial_man
				 WHERE s.serial_inv IS NOT NULL AND dea.dea_serial_dea = $dea_serial_dea
				 AND dea.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND dea.serial_dea = $serial_dea";
		}
		$sql .= " ORDER BY dea.name_dea";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$arreglo = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	/**
	  @Name: getDealersExcessPayments
	  @Description: Retrieves the information of a specific dealer's list
	  @Params: Serial number of a manager
	  @Returns: Dealers array
	 * */
	function getDealersExcessPayments($serial_mbc, $serial_dea = NULL) {
		$sql = "SELECT distinct(dea.dea_serial_dea) as serial_dea, dea.name_dea, dea.code_dea
				 FROM sales s
				 JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				 JOIN dealer dea ON cnt.serial_dea=dea.serial_dea
                 JOIN manager_by_country mbc ON mbc.serial_mbc=dea.serial_mbc AND mbc.serial_mbc = $serial_mbc
				 LEFT JOIN invoice inv ON inv.serial_inv=s.serial_inv
                 JOIN payments pay ON pay.serial_pay=inv.serial_pay AND pay.status_pay='EXCESS'
				 WHERE dea.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND dea.serial_dea = $serial_dea";
		}
		$sql .= " ORDER BY dea.name_dea";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$arreglo = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	/**
	  @Name: getBranchesExcessPayments
	  @Description: Retrieves the information of a specific dealer's list
	  @Params: Serial number of a dealer
	  @Returns: Dealers array
	 * */
	function getBranchesExcessPayments($dea_serial_dea, $serial_dea = NULL) {
		$sql = "SELECT distinct(bra.serial_dea), bra.name_dea, bra.code_dea
				FROM sales s
				JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
				JOIN dealer bra ON cnt.serial_dea = bra.serial_dea AND bra.dea_serial_dea = $dea_serial_dea
				LEFT JOIN invoice inv ON inv.serial_inv = s.serial_inv
				JOIN payments pay ON pay.serial_pay = inv.serial_pay AND pay.status_pay='EXCESS'
				WHERE bra.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND bra.serial_dea = $serial_dea";
		}
		$sql .= " ORDER BY bra.name_dea";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$arreglo = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	/**
	  @Name: getDealersWithPayments
	  @Description: Retrieves the information of a specific dealer's list
	  @Params: Serial number of a manager
	  @Returns: Dealers array
	 * */
	function getDealersWithPayments($serial_mbc, $serial_dea = NULL) {
		$sql = "SELECT distinct(dea.dea_serial_dea) as serial_dea, dea.name_dea, dea.code_dea
				FROM sales s
				JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
				JOIN dealer dea ON cnt.serial_dea = dea.serial_dea
			    JOIN manager_by_country mbc ON mbc.serial_mbc=dea.serial_mbc AND mbc.serial_mbc = $serial_mbc
			    JOIN invoice inv ON inv.serial_inv = s.serial_inv AND inv.status_inv = 'PAID'
				WHERE dea.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND dea.dea_serial_dea = $serial_dea";
		}
		$sql .= " ORDER BY dea.name_dea";
//echo $sql;
		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$arreglo = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	/**
	  @Name: getBranchesWithPayments
	  @Description: Retrieves the information of a specific dealer's list
	  @Params: Serial number of a dealer
	  @Returns: Dealers array
	 * */
	function getBranchesWithPayments($dea_serial_dea, $serial_dea = NULL) {
		$sql = "SELECT distinct(bra.serial_dea), bra.name_dea, bra.code_dea
				FROM sales s
				JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
				JOIN dealer bra ON cnt.serial_dea=bra.serial_dea AND bra.dea_serial_dea=" . $dea_serial_dea . "
				LEFT JOIN invoice inv ON inv.serial_inv=s.serial_inv
				JOIN payments pay ON pay.serial_pay=inv.serial_pay
				WHERE bra.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND bra.serial_dea = $serial_dea";
		}
		$sql .= " ORDER BY bra.name_dea";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$arreglo = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	/**
	  @Name: getDealerByCardNumber
	  @Description: Retrieves the information a dealer by card number
	  @Params: card number
	  @Returns: Dealers array
	 * */
	public static function getDealerByCardNumber($db, $card_number) {
		$sql = "SELECT dea.*
                    FROM sales sal
                    JOIN counter cnt ON cnt.serial_cnt = sal.serial_cnt
                    JOIN dealer dea ON dea.serial_dea = cnt.serial_dea
                    LEFT JOIN traveler_log trl ON trl.serial_sal = sal.serial_sal
                    WHERE sal.card_number_sal ='" . $card_number . "'
                    OR trl.card_number_trl = '" . $card_number . "'";
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	/**
	  @Name: getDealersByManager
	  @Description: Retrieves the information of a specific dealer's list
	  @Params: Serial number of a manager
	  @Returns: Dealers array
	 * */
	function getDealersByManager($serial_mbc, $serial_dea = NULL) {
		$sql = "SELECT distinct(dea.serial_dea), dea.name_dea
				 FROM sales sal
				 JOIN counter cnt ON sal.serial_cnt = cnt.serial_cnt
				 JOIN dealer br ON cnt.serial_dea = br.serial_dea
				 JOIN dealer dea ON dea.serial_dea = br.dea_serial_dea
				 JOIN manager_by_country AS mbc ON mbc.serial_mbc = br.serial_mbc  AND mbc.serial_mbc = $serial_mbc
			     WHERE sal.free_sal = 'YES'
				 AND dea.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND dea.serial_dea = " . $serial_dea;
		}

		$sql .= " ORDER BY dea.name_dea";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$arreglo = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	/**
	  @Name: getBranchesByDealer
	  @Description: Retrieves the information of a specific dealer's list
	  @Params: Serial number of a dealer
	  @Returns: Dealers array
	 * */
	function getBranchesByDealer($dea_serial_dea, $serial_dea = NULL) {
		$sql = "SELECT distinct(bra.serial_dea),bra.name_dea, bra.code_dea
				 FROM sales sal
				 JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
				 JOIN dealer bra ON cnt.serial_dea=bra.serial_dea AND bra.dea_serial_dea = $dea_serial_dea
                 WHERE sal.free_sal = 'YES'
				 AND bra.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND bra.serial_dea = $serial_dea";
		}
		$sql .= " ORDER BY bra.name_dea";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$arreglo = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	/**
	  @Name: getDealerCode
	  @Description: Retrieves the code of a specific dealer
	  @Params: Serial number of a dealer
	  @Returns: Code
	 * */
	public static function getDealerCode($db, $serial_dea) {
		$sql = "SELECT dea.serial_dea, dea.dea_serial_dea, dea.code_dea, dea2.code_dea as parent_code_dea,
                        cit.code_cit, cou.code_cou
                 FROM dealer dea
                 LEFT JOIN dealer dea2 ON dea2.serial_dea = dea.dea_serial_dea
                 JOIN sector sec ON sec.serial_sec = dea.serial_sec
                 JOIN city cit ON cit.serial_cit = sec.serial_cit
                 JOIN country cou ON cou.serial_cou = cit.serial_cou
                 WHERE dea.serial_dea = $serial_dea";

		$result = $db->getAll($sql);

		if ($result[0]) {
			if ($result[0]['dea_serial_dea']) {
				$code = $result[0]['code_cou'] . "-" . $result[0]['code_cit'] . "-" . $result[0]['parent_code_dea'] . "-" . $result[0]['code_dea'];
			} else {
				$code = $result[0]['code_cou'] . "-" . $result[0]['code_cit'] . "-" . $result[0]['code_dea'];
			}
			return $code;
		} else {
			return false;
		}
	}

	/**
	  @Name: getDealerByManager
	  @Description: Retrieves the information of a specific dealer's list
	  @Params: Serial number of a manager
	  @Returns: Dealers array
	 * */
	function getDealerByManager($serial_mbc, $serial_cit, $phone_dealers = 'NO', $serial_dea = NULL) {
		if ($phone_dealers != 'ALL')
			$phone_restriction = " AND br.phone_sales_dea = 'NO'";

		$sql = "SELECT distinct(dea.serial_dea), dea.name_dea, dea.code_dea
				FROM dealer br
				JOIN dealer dea ON dea.serial_dea = br.dea_serial_dea $phone_restriction
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit=sec.serial_cit AND cit.serial_cit = $serial_cit
				JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc AND mbc.serial_mbc = $serial_mbc
				JOIN manager man ON man.serial_man=mbc.serial_man";

		if ($serial_dea != NULL) {
			$sql .= " WHERE dea.serial_dea = $serial_dea";
		}
		$sql .= " ORDER BY dea.name_dea";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		$arreglo = array();
		if ($num > 0) {
			$cont = 0;
			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $arreglo;
	}

	/**
	  @Name: getBranchByDealer
	  @Description: Retrieves the information of a specific branch's list
	  @Params: Serial number of a dealer
	  @Returns: Branches array
	 * */
	function getBranchByDealer($dea_serial_dea, $serial_dea = NULL) {
		if (is_numeric($dea_serial_dea)) {
			$sql = "SELECT distinct(dea.serial_dea),dea.name_dea, dea.code_dea
					 FROM dealer dea 
					 JOIN manager_by_country mbc ON mbc.serial_mbc=dea.serial_mbc
				     JOIN manager man ON man.serial_man=mbc.serial_man
				     WHERE dea.dea_serial_dea = $dea_serial_dea and dea.status_dea='ACTIVE'";
			if ($serial_dea != NULL) {
				$sql .= " AND serial_dea= $serial_dea";
			}
			$sql .= " ORDER BY dea.name_dea";

			$result = $this->db->Execute($sql);
			$num = $result->RecordCount();
			$arreglo = array();
			if ($num > 0) {
				$cont = 0;
				do {
					$arreglo[$cont] = $result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				} while ($cont < $num);
			}
			return $arreglo;
		}
		return false;
	}

	/*
	 * @name: getDealerByResponsible
	 * @param: $db - DB connection
	 * @param: $serial_usr - User ID. The user is responsible in a set of dealers.
	 * @param: $serial_mbc - Manager by country ID
	 */

	public static function getDealerByResponsible($db, $serial_usr, $serial_mbc, $serial_dea = NULL) {
		$sql = "SELECT DISTINCT d.serial_dea, d.name_dea, d.code_dea
			    FROM dealer d
			    JOIN dealer b ON b.dea_serial_dea = d.serial_dea AND b.serial_mbc = $serial_mbc
			    JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd='ACTIVE' AND ubd.serial_usr=$serial_usr
				WHERE d.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND d.serial_dea = $serial_dea";
		}
		$sql .= " ORDER BY d.name_dea";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: getDealerByCityByResponsible
	 */

	public static function getDealerByCityByResponsible($db, $serial_cit, $serial_usr, $serial_mbc, $serial_dea = NULL, $branch = NULL) {
		$sql = "SELECT DISTINCT d.serial_dea, d.name_dea
			    FROM dealer d
				JOIN sector sec ON sec.serial_sec = d.serial_sec AND sec.serial_cit = $serial_cit
			    JOIN dealer b ON b.dea_serial_dea = d.serial_dea AND b.serial_mbc = $serial_mbc
			    JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd='ACTIVE' AND ubd.serial_usr=$serial_usr
				WHERE d.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND d.dea_serial_dea = $serial_dea";
		}

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: getDealerByCityByResponsible
	 */

	public static function getBranchByDealerByCityByResponsible($db, $serial_cit, $serial_usr, $serial_mbc, $serial_dea) {
		$sql = "SELECT DISTINCT d.serial_dea, d.name_dea
			    FROM dealer d
				JOIN sector sec ON sec.serial_sec = d.serial_sec AND sec.serial_cit = $serial_cit
			    JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea AND ubd.status_ubd='ACTIVE' AND ubd.serial_usr=$serial_usr
				WHERE d.phone_sales_dea = 'NO'
				AND d.dea_serial_dea = $serial_dea";
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/*
	 * @name: getBranchesByDealerByResponsible
	 * @param: $db - DB connection
	 * @param: $serial_usr - User ID. The user is responsible in a set of dealers.
	 * @param: $serial_mbc - Manager by country ID
	 */

	public static function getBranchesByDealerByResponsible($db, $dea_serial_dea, $serial_usr, $serial_mbc, $serial_dea = NULL) {
		$sql = "SELECT DISTINCT b.serial_dea, b.name_dea, b.code_dea
			    FROM dealer b
			    JOIN user_by_dealer ubd ON ubd.serial_dea=b.serial_dea AND ubd.status_ubd='ACTIVE' AND ubd.serial_usr = $serial_usr
			    WHERE b.serial_mbc = $serial_mbc
			    AND b.dea_serial_dea = $dea_serial_dea
				AND b.phone_sales_dea = 'NO'";
		if ($serial_dea != NULL) {
			$sql .= " AND b.serial_dea = $serial_dea";
		}

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
	  @Name: getMasiveDealers
	  @Description: Returns an array of dealers with masive sales.
	  @Params: country ID
	  @Returns: ManagerID array
	 * */
	function getMasiveDealers($serial_mbc, $serial_usr = NULL, $dealer_cat = NULL, $serial_dlt = NULL) {
		$sql = "SELECT DISTINCT dea.serial_dea, dea.name_dea, dea.code_dea
					FROM dealer dea
					JOIN dealer d ON dea.serial_dea=d.dea_serial_dea
					JOIN sector sec ON d.serial_sec=sec.serial_sec
					JOIN city c ON sec.serial_cit=c.serial_cit
					JOIN user_by_dealer ubd ON ubd.serial_dea=dea.serial_dea
					JOIN counter cnt ON cnt.serial_dea=d.serial_dea
					JOIN sales s ON cnt.serial_cnt=s.serial_cnt AND s.status_sal='REGISTERED' 
					JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
					JOIN product p ON p.serial_pro=pxc.serial_pro 
						AND p.masive_pro='YES' 
						AND s.sal_serial_sal IS NULL
                    WHERE dea.serial_mbc='" . $serial_mbc . "'";

		if ($dealer_cat != NULL) {
			$sql.="AND dea.category_dea='" . $dealer_cat . "'";
		}
		if ($serial_usr != NULL) {
			$sql.="AND ubd.status_ubd='ACTIVE' AND ubd.serial_usr='" . $serial_usr . "'";
		}
		if ($serial_dlt != NULL) {
			$sql.="AND dea.serial_dlt='" . $serial_dlt . "'";
		}
		$sql.= " ORDER BY dea.name_dea";
		// die($sql);
		$result = $this->db->Execute($sql);

		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/**
	  @Name: getMasiveBranches
	  @Description: Returns an array of branches with masive sales.
	  @Params: country ID
	  @Returns: ManagerID array
	 * */
	function getMasiveBranches($serial_dea) {
		$sql = "SELECT DISTINCT d.serial_dea, d.name_dea, d.code_dea
					FROM dealer d
					JOIN counter cnt ON cnt.serial_dea=d.serial_dea AND cnt.status_cnt = 'ACTIVE'
					JOIN sales s ON cnt.serial_cnt=s.serial_cnt AND s.status_sal='REGISTERED' 
					JOIN product_by_dealer pbd ON pbd.serial_pbd=s.serial_pbd
					JOIN product_by_country pxc ON pxc.serial_pxc=pbd.serial_pxc
					JOIN product p ON p.serial_pro=pxc.serial_pro 
						AND p.masive_pro='YES' 
						AND s.sal_serial_sal IS NULL					
					WHERE d.dea_serial_dea='" . $serial_dea . "'
					AND NOW() < s.end_date_sal
					ORDER BY d.name_dea";
		//echo $sql;
		$result = $this->db->Execute($sql);

		$num = $result->RecordCount();
		if ($num > 0) {
			$arreglo = array();
			$cont = 0;

			do {
				$arreglo[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}

		return $arreglo;
	}

	/**
	  @Name: getMasiveSales
	  @Description: Returns an array of masive sales.
	  @Params: class atribute serial_dea
	  @Returns: Sales array
	 * */
	function getMasiveSales() {
		$sql = "SELECT s.serial_pbd,s.serial_sal,s.card_number_sal,pbl.name_pbl,
							cus.first_name_cus as legal_entity_name, CONCAT(cus.first_name_cus,' ',IFNULL(cus.last_name_cus,'')) as customer_name,
							DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y')as emission_date,s.total_sal,s.status_sal,
							pxc.serial_pxc
                    FROM sales s
                    JOIN counter c ON s.serial_cnt=c.serial_cnt
                    JOIN dealer d ON d.serial_dea=c.serial_dea AND c.serial_dea={$this->serial_dea}
                    JOIN product_by_dealer pbd ON s.serial_pbd=pbd.serial_pbd
                    JOIN product_by_country pxc ON pbd.serial_pxc=pxc.serial_pxc
                    JOIN product pr ON pr.serial_pro=pxc.serial_pro AND pr.masive_pro='YES' AND s.sal_serial_sal IS NULL
                    JOIN product_by_language pbl ON pr.serial_pro=pbl.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
                    JOIN customer cus ON cus.serial_cus=s.serial_cus
                    WHERE s.status_sal='REGISTERED'
					AND NOW() < s.end_date_sal
                    ORDER BY s.emission_date_sal DESC,s.card_number_sal";

		$result = $this->db->Execute($sql);
		$num = $result->RecordCount();
		if ($num > 0) {
			$list = array();
			$cont = 0;

			do {
				$list[$cont] = $result->GetRowAssoc(false);
				$result->MoveNext();
				$cont++;
			} while ($cont < $num);
		}
		return $list;
	}

	/**
	  @Name: getDealersWithRefunds
	  @Description: Retrieves the information of a specific dealer's list
	  @Params: Serial number of a responsible
	  @Returns: Dealers array
	 * */
	function getDealersWithRefunds($serial_usr, $serial_dea = NULL) {
		$arreglo = array();
		if ($serial_usr) {
			$sql = "SELECT distinct(dea.serial_dea),dea.name_dea
					 FROM sales s
					 JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
					 JOIN dealer br ON cnt.serial_dea=br.serial_dea
					 JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
					 JOIN user_by_dealer ubd ON ubd.serial_dea=br.serial_dea AND ubd.status_ubd='ACTIVE'
					 JOIN user usr ON usr.serial_usr=ubd.serial_usr AND usr.serial_usr= " . $serial_usr . "
					 JOIN refund ref ON ref.serial_sal=s.serial_sal
					 WHERE dea.phone_sales_dea = 'NO'";
			if ($serial_dea != NULL) {
				$sql .= " WHERE dea.serial_dea = " . $serial_dea;
			}
			$sql .= " ORDER BY dea.name_dea";

			$result = $this->db->Execute($sql);
			$num = $result->RecordCount();
			if ($num > 0) {
				$cont = 0;
				do {
					$arreglo[$cont] = $result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				} while ($cont < $num);
			}
		}
		return $arreglo;
	}

	/**
	  @Name: getBranchesWithRefunds
	  @Description: Retrieves the information of a specific branch
	  @Params: Serial of a dealer
	  @Returns: Branch array
	 * */
	function getBranchesWithRefunds($dea_serial_dea, $serial_dea = NULL) {
		$arreglo = array();
		if ($dea_serial_dea) {
			$sql = "SELECT distinct(bra.serial_dea), CONCAT(bra.code_dea,' - ',bra.name_dea) AS name_dea
					 FROM sales s
					 JOIN counter cnt ON s.serial_cnt=cnt.serial_cnt
					 JOIN dealer bra ON cnt.serial_dea=bra.serial_dea AND bra.dea_serial_dea=" . $dea_serial_dea . "
					 JOIN dealer dea ON dea.serial_dea=bra.dea_serial_dea
					 JOIN refund ref ON ref.serial_sal=s.serial_sal
					 WHERE bra.phone_sales_dea = 'NO'";
			if ($serial_dea != NULL) {
				$sql .= " WHERE bra.serial_dea = " . $serial_dea;
			}
			$sql .= " ORDER BY bra.name_dea";
			$result = $this->db->Execute($sql);
			$num = $result->RecordCount();
			if ($num > 0) {
				$cont = 0;
				do {
					$arreglo[$cont] = $result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				} while ($cont < $num);
			}
		}
		return $arreglo;
	}

	/**
	  @Name: getDealersSalesAnalysis
	  @Description: Retrieves the information of a specific dealer's list
	  @Params: Serial number of a responsible
	  @Returns: Dealers array
	 * */
	function getDealersSalesAnalysis($serial_usr, $serial_dea = NULL) {
		$arreglo = array();
		if ($serial_usr) {
			$sql = "SELECT distinct(dea.serial_dea), dea.name_dea
					 FROM sales sal
					 JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
					 JOIN dealer br ON cnt.serial_dea=br.serial_dea
					 JOIN dealer dea ON dea.serial_dea=br.dea_serial_dea
					 JOIN user_by_dealer ubd ON ubd.serial_dea=br.serial_dea AND ubd.status_ubd='ACTIVE'
					 JOIN user usr ON usr.serial_usr=ubd.serial_usr AND usr.serial_usr= " . $serial_usr . "
					 JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv
					 JOIN payments pay ON pay.serial_pay=inv.serial_pay
					 WHERE sal.free_sal = 'NO' AND sal.status_sal NOT IN ('VOID','DENIED','REFUNDED','BLOCKED')
					 AND sal.serial_inv IS NOT NULL AND inv.serial_pay IS NOT NULL AND pay.status_pay = 'PAID'
					 AND dea.phone_sales_dea = 'NO'";
			if ($serial_dea != NULL) {
				$sql .= " AND dea.serial_dea = " . $serial_dea;
			}
			$sql .= " ORDER BY dea.name_dea";

			$result = $this->db->Execute($sql);
			$num = $result->RecordCount();
			if ($num > 0) {
				$cont = 0;
				do {
					$arreglo[$cont] = $result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				} while ($cont < $num);
			}
		}
		return $arreglo;
	}

	/**
	  @Name: getBranchesSalesAnalysis
	  @Description: Retrieves the information of a specific branch's list
	  @Params: Serial number of a dealer
	  @Returns: Dealers array
	 * */
	function getBranchesSalesAnalysis($dea_serial_dea, $serial_dea = NULL) {
		$arreglo = array();
		if ($dea_serial_dea) {
			$sql = "SELECT distinct(bra.serial_dea), CONCAT(bra.code_dea,' - ',bra.name_dea) AS name_dea
					 FROM sales sal
					 JOIN counter cnt ON sal.serial_cnt=cnt.serial_cnt
					 JOIN dealer bra ON cnt.serial_dea=bra.serial_dea AND bra.dea_serial_dea=" . $dea_serial_dea . "
					 JOIN invoice AS inv ON inv.serial_inv = sal.serial_inv
					 JOIN payments pay ON pay.serial_pay=inv.serial_pay
					 WHERE sal.free_sal = 'NO' AND sal.status_sal NOT IN ('VOID','DENIED','REFUNDED','BLOCKED')
					 AND sal.serial_inv IS NOT NULL AND inv.serial_pay IS NOT NULL AND pay.status_pay = 'PAID'
					 AND bra.phone_sales_dea = 'NO'";
			if ($serial_dea != NULL) {
				$sql .= " AND bra.serial_dea = " . $serial_dea;
			}
			$sql .= " ORDER BY bra.name_dea";

			$result = $this->db->Execute($sql);
			$num = $result->RecordCount();
			if ($num > 0) {
				$cont = 0;
				do {
					$arreglo[$cont] = $result->GetRowAssoc(false);
					$result->MoveNext();
					$cont++;
				} while ($cont < $num);
			}
		}
		return $arreglo;
	}

	/*
	 * @name: getDealersWithBonus
	 * @param: $db - DB connection
	 * @return: Array with dealers
	 */

	public static function getDealersWithBonus($db, $serial_mbc, $serial_cit, $bonus_to = NULL, $at_least_one_paid = NULL, $authorized = NULL) {
		if ($at_least_one_paid)
			$one_paid = " JOIN applied_bonus_liquidation abl ON abl.serial_apb = apb.serial_apb";

		$sql = "SELECT DISTINCT d.serial_dea, d.name_dea, d.code_dea
				FROM dealer d
				JOIN dealer b ON b.dea_serial_dea=d.serial_dea AND b.serial_mbc=$serial_mbc
				JOIN sector s ON s.serial_sec=b.serial_sec
				JOIN city cit ON cit.serial_cit=s.serial_cit AND cit.serial_cit=$serial_cit
				JOIN counter cnt ON cnt.serial_dea=b.serial_dea
				JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
				JOIN applied_bonus apb ON apb.serial_sal=sal.serial_sal
				$one_paid";

		if ($bonus_to)
			$sql .= " AND apb.bonus_to_apb = '$bonus_to'";
		if ($authorized)
			$sql.=" AND apb.ondate_apb = 'NO'
								AND apb.authorized_apb = 'YES'";
		$sql .= " Order by d.name_dea";
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return NULL;
		}
	}

	/*
	 * @name: getDealersWithBonus
	 * @param: $db - DB connection
	 * @return: Array with dealers
	 */

	public static function getBranchesWithBonus($db, $serial_dea, $bonus_to = NULL, $at_least_one_paid = NULL, $authorized = NULL) {
		if ($at_least_one_paid)
			$one_paid = " JOIN applied_bonus_liquidation abl ON abl.serial_apb = apb.serial_apb";

		$sql = "SELECT DISTINCT d.serial_dea, d.name_dea, d.code_dea
				FROM dealer d
				JOIN counter cnt ON cnt.serial_dea=d.serial_dea
				JOIN sales sal ON sal.serial_cnt=cnt.serial_cnt
				JOIN applied_bonus apb ON apb.serial_sal=sal.serial_sal
				$one_paid
				WHERE d.dea_serial_dea=" . $serial_dea;

		if ($bonus_to)
			$sql .= " AND apb.bonus_to_apb = '$bonus_to'";
		if ($authorized)
			$sql.=" AND apb.ondate_apb = 'NO'
								AND apb.authorized_apb = 'YES'";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return NULL;
		}
	}

	public static function getDealersForMigration($db) {
		$sql = "SELECT *
				FROM dealer d
				WHERE d.serial_dea >= 13";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return NULL;
		}
	}

	public static function getDealerReport($db, $serial_dea) {
		$sql = "SELECT cou.name_cou, cit.name_cit, sec.name_sec, man.name_man, dea.code_dea, dea.official_seller_dea,
						CONCAT(usr.first_name_usr, ' ', usr.last_name_usr) as name_usr, dea.id_dea, dea.name_dea,
						dtl.description_dtl, dea.category_dea, dea.address_dea, dea.status_dea, dea.phone1_dea,
						dea.phone2_dea, dea.contact_dea, dea.email_contact_dea, dea.visits_number_dea,
						dea.manager_name_dea, dea.manager_phone_dea, dea.manager_email_dea, IFNULL(DATE_FORMAT(dea.manager_birthday_dea,'%d/%m/%Y'),'') AS manager_birthday_dea,
						dea.pay_contact_dea, dea.bill_to_dea, dea.payment_deadline_dea, cdd.days_cdd, dea.percentage_dea, dea.type_percentage_dea,
						dea.bonus_to_dea, dea.assistance_contact_dea
						FROM dealer dea
						JOIN sector sec ON sec.serial_sec = dea.serial_sec
						JOIN city cit ON cit.serial_cit = sec.serial_cit
						JOIN country cou ON cou.serial_cou = cit.serial_cou
						JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
						JOIN manager man ON man.serial_man = mbc.serial_man
						JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea
						JOIN user usr ON usr.serial_usr = ubd.serial_usr
						JOIN dealer_type dlt ON dlt.serial_dlt = dea.serial_dlt
						JOIN dealer_ty_by_language dtl ON dtl.serial_dlt = dlt.serial_dlt AND dtl.serial_lang = " . $_SESSION['serial_lang'] . "
						JOIN credit_day cdd ON cdd.serial_cdd = dea.serial_cdd
						WHERE dea.serial_dea = " . $serial_dea;
		//die($sql);
		$result = $db->getAll($sql);

		if ($result[0]) {
			return $result[0];
		} else {
			return NULL;
		}
	}

	public static function getBranchReport($db, $serial_cou, $serial_mbc = NULL, $serial_dea = NULL, $serial_usr = NULL, $status_dea = "ACTIVE", $dea_serial_dea = NULL) {
		$sql = "SELECT DISTINCT dea.name_dea, dea.category_dea, dea.visits_number_dea, dea.percentage_dea,
						cit.name_cit, sec.name_sec, dea.address_dea, dea.phone1_dea, dea.manager_name_dea,
						dea.contact_dea, dea.email_contact_dea, dea.id_dea, dea.type_percentage_dea,
						dea.manager_phone_dea, CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'responsible',
						cd.days_cdd,
						dea.code_dea,
						dea.id_dea,
						dea.manager_name_dea,
						dea.phone2_dea
				 FROM dealer dea
				 JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				 JOIN user u ON u.serial_usr = ubd.serial_usr AND u.status_usr = 'ACTIVE'
				 JOIN sector sec ON sec.serial_sec = dea.serial_sec
				 JOIN city cit ON cit.serial_cit = sec.serial_cit
				 JOIN manager_by_country mbc ON mbc.serial_mbc = dea.serial_mbc
				 JOIN credit_day cd ON cd.serial_cdd = dea.serial_cdd
				 WHERE dea.phone_sales_dea = 'NO'
				 AND dea.dea_serial_dea IS NOT NULL
				 AND dea.status_dea = '$status_dea'
				 AND cit.serial_cou = $serial_cou";

		if ($serial_mbc != NULL) {
			$sql .= " AND dea.serial_mbc = $serial_mbc";
		}
		
		if ($serial_dea != NULL) {
			$sql .= " AND dea.dea_serial_dea = $serial_dea";
		}
		if ($serial_usr != NULL) {
			$sql .= " AND ubd.serial_usr = $serial_usr";
		}
		if ($dea_serial_dea != NULL) {
			$sql .= " AND dea.dea_serial_dea = $dea_serial_dea";
		}
		
		$sql .= " ORDER BY dea.name_dea";
	//Debug::print_r($sql);die();
		$result = $db->getAll($sql);
		
		if ($result) {
			return $result;
		} else {
			return NULL;
		}
	}

	public static function getDealersCountrySerial($db, $serialDea) {
		$sql = "SELECT cit.serial_cou
                 FROM dealer dea
                 JOIN sector sec ON dea.serial_sec = sec.serial_sec
                 JOIN city cit ON sec.serial_cit = cit.serial_cit
                 WHERE dea.serial_dea = $serialDea";

		$result = $db->Execute($sql);
		if ($result) {
			return $result->fields[0];
		}
		return false;
	}

	public static function getDealerSinglePhoneBranch($db, $serialDea) {
		$sql = "SELECT bra.serial_dea
                 FROM dealer dea
                 JOIN dealer bra ON dea.serial_dea = bra.dea_serial_dea
                 WHERE dea.serial_dea = $serialDea";
		$result = $db->Execute($sql);
		if ($result) {
			return $result->fields[0];
		}
		return false;
	}

	public static function getDealerUniqueCounter($db, $serialDea) {
		$sql = "SELECT cou.serial_cnt
                 FROM dealer dea
                 JOIN dealer bra ON dea.serial_dea = bra.dea_serial_dea
                 JOIN counter cou ON cou.serial_dea = bra.serial_dea
                 WHERE dea.serial_dea = $serialDea";
		$result = $db->Execute($sql);
		if ($result) {
			if (count($result) > 1) { //CONSISTENCY CHECK
				ErrorLog::log($this->db, 'WARNING! MULTIPLE COUNTERS FOUND! DEALER: ' . $serialDea, $result->fields);
			}
			return $result->fields[0];
		}
		return false;
	}

	public static function getDealersByComissionistAndCity($db, $serial_cit, $serial_usr) {
		$sql = "SELECT DISTINCT d.*
				FROM dealer br
				JOIN dealer d ON d.serial_dea = br.dea_serial_dea AND d.status_dea = 'ACTIVE' AND br.status_dea = 'ACTIVE'
				JOIN user_by_dealer ubd ON ubd.serial_dea = br.serial_dea
					AND ubd.serial_usr = $serial_usr
					AND ubd.status_ubd = 'ACTIVE'
				JOIN sector s ON s.serial_sec = br.serial_sec AND s.serial_cit = $serial_cit
				ORDER BY d.name_dea";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}
	public static function getDealersByComissionist($db, $serial_mbc, $serial_usr, $serial_type = NULL) {
	if ($serial_type)
			$clause = " AND d.serial_dlt = $serial_type";	
            $sql = "SELECT DISTINCT d.*
				FROM dealer br
				JOIN dealer d ON d.serial_dea = br.dea_serial_dea AND d.status_dea = 'ACTIVE' AND br.status_dea = 'ACTIVE' and d.serial_mbc = $serial_mbc
				JOIN user_by_dealer ubd ON ubd.serial_dea = br.serial_dea
					AND ubd.serial_usr = $serial_usr
					AND ubd.status_ubd = 'ACTIVE'
                                $clause        
				ORDER BY d.name_dea";
                //Debug::print_r($sql);
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}
	public static function isBranch($db, $serialDea) {
		$sql = "SELECT 1
                 FROM dealer dea
                 WHERE dea.serial_dea = $serialDea
                 AND dea.dea_serial_dea IS NOT NULL";
		$result = $db->Execute($sql);
		if ($result->fields) {
			return true;
		}
		return false;
	}

	public static function changeOfManager($db, $serial_dea, $serial_mbc) {
		$sql = "UPDATE dealer SET
					serial_mbc = $serial_mbc
				WHERE serial_dea = $serial_dea
				OR dea_serial_dea = $serial_dea";

		$result = $db->Execute($sql);

		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * getAllBranchesByDealersDocument
	 * @param <ADODB> $db
	 * @param <string> $document
	 * @return Array of all branches with the same document as the sent in the parameter.
	 */
	public static function getAllBranchesByDealers($db, $serial_dea, $serial_bra = NULL) {
		$sql = "SELECT d.serial_dea, d.id_dea, d.name_dea, d.code_dea
				FROM dealer d
				WHERE dea_serial_dea = '$serial_dea'
				AND dea_serial_dea IS NOT NULL
				AND status_dea = 'ACTIVE'";
		
		if($serial_bra):
			$sql .= " AND serial_dea = $serial_bra";
		endif;
		
		$sql .= " ORDER BY d.name_dea";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public static function getGlobalSalesReport($db, $branches_serials_list, $begin_date, $end_date) {
		$sql = "SELECT cnt.serial_dea, s.card_number_sal, DATE_FORMAT(s.emission_date_sal, '%d/%m/%Y') AS 'emission_date_sal',
					CONCAT(cus.first_name_cus, ' ', cus.last_name_cus) AS 'name_cus',
					pbl.name_pbl, DATE_FORMAT(s.begin_date_sal, '%d/%m/%Y') AS 'begin_date_sal',
					DATE_FORMAT(s.end_date_sal, '%d/%m/%Y') AS 'end_date_sal', s.days_sal, s.status_sal,
					CONCAT(u.first_name_usr, ' ', u.last_name_usr) AS 'name_cnt', s.total_sal, d.name_dea, d.code_dea
				FROM sales s
				JOIN customer cus ON cus.serial_cus = s.serial_cus
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN dealer d ON d.serial_dea = cnt.serial_dea AND d.status_dea= 'ACTIVE'
				JOIN user u ON u.serial_usr = cnt.serial_usr
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = {$_SESSION['serial_lang']}
				WHERE cnt.serial_dea IN ($branches_serials_list)
				AND s.in_date_sal BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('$end_date', '%d/%m/%Y'), INTERVAL 1 DAY)
				ORDER BY cnt.serial_dea, s.in_date_sal";
		//die(Debug::print_r($sql));
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	public static function getGrossComissionsInfo($db, $serial_cou, $begin_date, $end_date, $serial_mbc = NULL, $serial_ubd = NULL) {
		if ($serial_mbc)
			$mbc_clause = " AND va.serial_mbc = $serial_mbc";
		if ($serial_ubd)
			$comissionist_clause = " AND u.serial_usr = $serial_ubd";

		$sql = "SELECT va.serial_usr, CONCAT(first_name_usr, ' ', last_name_usr) AS 'seller',
						ROUND(AVG(global_comission), 2) AS 'theoric_comission', ROUND((SUM(comission_value)/SUM(total_sal)) * 100, 2) AS 'effective_comission'
				FROM view_average_comission va
				JOIN user u ON u.serial_usr = va.serial_usr
				WHERE STR_TO_DATE(DATE_FORMAT(emission_date_sal, '%d/%m/%Y'), '%d/%m/%Y') 
					BETWEEN STR_TO_DATE('$begin_date', '%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
				AND serial_cou = $serial_cou
				$mbc_clause
				$comissionist_clause
				GROUP BY va.serial_usr
				ORDER BY effective_comission DESC";

		//Debug::print_r($sql);
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}
        
        /*
	 * @name: getRealGrossComissionsInfo
	 * @param: 
	 * @return:
	 */

        public static function getRealGrossComissionsInfo($db, $serial_cou, $begin_date, $end_date, $serial_mbc = NULL, $serial_ubd = NULL, $effective = false) {
		 
            if ($serial_mbc)
			$mbc_clause = " AND d.dea_serial_dea IS NOT NULL AND d.serial_mbc = $serial_mbc";
                        
		if ($serial_ubd)
			$comissionist_clause = "AND ubd.status_ubd = 'ACTIVE'  AND ubd.serial_usr = $serial_ubd";
                       
                $percentage = "";
		if ($effective) {
			$percentage = "AND d.percentage_dea > 0";
                    
                        }

		$sql = "(
			SELECT  CONCAT(u.first_name_usr,' ',u.last_name_usr) AS RESPONSABLE, 
                                d.name_dea, 
                                d.percentage_dea, 
                                s.card_number_sal, 
                                s.status_sal, 
                                s.total_sal, 
                                i.other_dscnt_inv,
                                d.percentage_dea+i.other_dscnt_inv AS TOTAL_COMISION
                                s.total_sal * ((d.percentage_dea+i.other_dscnt_inv) / 100) AS COMISION
                            FROM sales s
                            JOIN invoice i ON s.serial_inv = i.serial_inv
                            JOIN counter cnt ON s.serial_cnt = cnt.serial_cnt
                            JOIN dealer d ON cnt.serial_dea = d.serial_dea $mbc_clause
                            $percentage
                            JOIN manager_by_country mbc ON mbc.serial_mbc = d.serial_mbc AND mbc.serial_cou = $serial_cou
                            JOIN user_by_dealer ubd ON ubd.serial_dea = d.serial_dea
                            AND s.in_date_sal BETWEEN ubd.begin_date_ubd AND DATE_SUB(IFNULL(ubd.end_date_ubd, DATE_ADD(CURRENT_DATE(), INTERVAL 2 DAY)), INTERVAL 1 DAY)
                            $comissionist_clause 
                            JOIN user u ON ubd.serial_usr = u.serial_usr
                            WHERE 
                            s.in_date_sal BETWEEN STR_TO_DATE('$begin_date', '%d/%m/%Y') AND STR_TO_DATE('$end_date', '%d/%m/%Y')
                            AND s.status_sal NOT IN ('REFUENDED', 'VOID') 
                            ORDER BY RESPONSABLE
                         )";

		//Debug::print_r($sql);
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}
        
	/*
	 * @name: getAssignedDealers
	 * @param: $db, $serial_cou, $serial_cit, $serial_mbc, $serial_usr
	 * @return: Returns the number of dealers assigned to a country, also applies for manager, city and responsible
	 */

	public static function getAssignedDealers($db, $serial_cou, $begin_date, $end_date, $serial_cit = NULL, $serial_mbc = NULL, $serial_usr = NULL) {

		$sql = "SELECT COUNT(DISTINCT dea.serial_dea) as assigned
				FROM dealer dea
				JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user usr ON usr.serial_usr = ubd.serial_usr AND usr.status_usr = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit AND cit.serial_cou = $serial_cou
				JOIN counter cnt ON cnt.serial_dea = dea.serial_dea
				LEFT JOIN sales sal ON sal.serial_cnt = cnt.serial_cnt
					AND (sal.in_date_sal BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('$end_date', '%d/%m/%Y'), INTERVAL 1 DAY))
				WHERE dea.status_dea = 'ACTIVE' AND dea.dea_serial_dea IS NOT NULL
		";
		$sql .= $serial_usr ? " AND ubd.serial_usr = $serial_usr" : "";
		$sql .= $serial_mbc ? " AND dea.serial_mbc = $serial_mbc" : "";
		$sql .= $serial_cit ? " AND cit.serial_cit = $serial_cit" : "";
		//Debug::print_r($sql);
		$result = $db->getOne($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public static function getDealerCardReport($db, $serial_cou, $date_type, $begin_date, $end_date, $serial_cit = NULL, $serial_mbc = NULL, $dea_serial_dea = NULL, $serial_dea = NULL, $serial_pro = NULL, $type_sal = NULL, $status_sal = NULL, $operator = NULL, $total_sal = NULL, $stock_type = NULL, $order_by = NULL) {
		if ($order_by == NULL || $order_by == "card_number") {
			$order_by = "col1";
		} else {
			$order_by = "col2";
		}

		$sql = "SELECT  DISTINCT s.serial_sal,
						IFNULL(card_number_sal, 'N/A') as col1,
        				DATE_FORMAT(s.emission_date_sal,'%d/%m/%Y') as col2,
        				CONCAT(first_name_cus,' ',last_name_cus) as col3,
                     	pbl.name_pbl as col4,
                     	DATE_FORMAT(s.begin_date_sal,'%d/%m/%Y') as col5, 
                     	DATE_FORMAT(s.end_date_sal,'%d/%m/%Y') as col6,
                     	DATEDIFF(s.end_date_sal,s.begin_date_sal)+1 as col7,
                     	s.total_sal as col8,
                     	IF(s.free_sal = 'YES', 'FREE', s.status_sal) as col9,
                     	dea.name_dea as col10,
                     	CONCAT(usr.first_name_usr,' ',usr.last_name_usr) as col11,
                     	s.stock_type_sal as col12
				FROM sales s
				JOIN customer cus ON cus.serial_cus = s.serial_cus AND s.status_sal <> 'DENIED'
				JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
				JOIN user usr ON usr.serial_usr = cnt.serial_usr
				JOIN dealer dea ON dea.serial_dea = cnt.serial_dea AND dea.status_dea = 'ACTIVE'
				JOIN sector sec ON sec.serial_sec = dea.serial_sec
				JOIN city cit ON cit.serial_cit = sec.serial_cit
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
				JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang={$_SESSION['serial_lang']}
				$refund_join
				WHERE cit.serial_cou='$serial_cou'";

		if ($date_type == "insystem") {
			$sql .= " AND (s.in_date_sal BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND ADDDATE(STR_TO_DATE('$end_date','%d/%m/%Y'),1))";
		} else if ($date_type == "coverage") {
			$sql .= " AND (s.begin_date_sal BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND ADDDATE(STR_TO_DATE('$end_date','%d/%m/%Y'),1)
                      AND s.end_date_sal  BETWEEN STR_TO_DATE('$begin_date','%d/%m/%Y') AND ADDDATE(STR_TO_DATE('$end_date','%d/%m/%Y'),1))";
		}

		if ($serial_cit != NULL) {
			$sql.=" AND cit.serial_cit = '$serial_cit'";
		}
		if ($serial_mbc != NULL) {
			$sql.=" AND dea.serial_mbc = '$serial_mbc'";
		}
		if ($dea_serial_dea != NULL) {
			$sql.=" AND dea.dea_serial_dea = '$dea_serial_dea'";
		}
		if ($serial_dea != NULL) {
			$sql.=" AND dea.serial_dea = '$serial_dea'";
		}
		if ($serial_pro != NULL) {
			$sql.=" AND pxc.serial_pro = '$serial_pro'";
		}
		if ($type_sal != NULL) {
			$sql.=" AND s.type_sal = '$type_sal'";
		}
		if ($total_sal != NULL) {
			$sql.=" AND s.total_sal " . $operator . $total_sal;
		}
		if ($stock_type != NULL) {
			$sql.=" AND s.stock_type_sal = '$stock_type'";
		}
		if ($status_sal != NULL) {
			$sql .= " AND s.status_sal IN ($status_sal)";
		}
		$sql.=" ORDER BY $order_by";

		//die('<pre>'.$sql.'</pre>');
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return false;
		}
	}

	/**
	 * getBranchByManagerResponsible
	 * @param <ADODB> $db
	 * @param <int> $serial_mbc
	 * @param <int> $serial_usr
	 * @param <int> $serial_branches
	 * @return Array of all branches by manager by Country, and Responsible, removing branches on $serial_branches.
	 */
	public static function getBranchByManagerResponsible($db, $serial_mbc, $serial_usr, $serial_branches) {
		$sql = "SELECT dea.serial_dea, dea.name_dea , code_dea
				FROM dealer dea";
		$sql.=($serial_usr != '') ? " JOIN user_by_dealer ubd ON ubd.serial_dea = dea.serial_dea AND ubd.status_ubd = 'ACTIVE' AND ubd.serial_usr = $serial_usr
								  JOIN user u ON u.serial_usr = ubd.serial_usr AND u.status_usr = 'ACTIVE'" : ' ';
		$sql.="	WHERE dea.dea_serial_dea IS NOT NULL
				AND dea.serial_mbc = $serial_mbc
				AND dea.status_dea = 'ACTIVE'";
		$sql.=($serial_branches != '') ? " AND dea.serial_dea NOT IN($serial_branches)" : "";
		$sql.="ORDER BY dea.name_dea";
		//echo $sql;
		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}

	public static function getAllBranchesByDealer($db, $serial_dea) {
		$sql = "SELECT *
				FROM dealer
				WHERE dea_serial_dea = $serial_dea";

		$result = $db->getAll($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}
	
	public static function branchWithManagerRightsParent($db, $serial_bra){
		$sql = "SELECT d.manager_rights_dea, d.percentage_dea
				FROM dealer d
				JOIN dealer bra ON bra.dea_serial_dea = d.serial_dea
				WHERE bra.serial_dea = $serial_bra";
               
		$result = $db->getRow($sql);

		if ($result) {
			return $result;
		} else {
			return FALSE;
		}
	}
	
	public static function getBranchesAutocompleter($db, $pattern, $serial_cou = NULL) {
		if($serial_cou):
			$serial_cou = ' AND mbc.serial_cou = '.$serial_cou;
		endif;
		
		$sql = "SELECT d.serial_dea, d.name_dea, d.code_dea
				FROM dealer d
				JOIN manager_by_country mbc ON mbc.serial_mbc = d.serial_mbc $serial_cou
				WHERE (	d.name_dea LIKE '%" . utf8_encode($pattern) . "%'
					OR d.code_dea LIKE '%" . utf8_encode($pattern) . "%'
					OR d.id_dea LIKE '%" . utf8_encode($pattern) . "%')
				AND d.dea_serial_dea IS NOT NULL
				AND d.phone_sales_dea = 'NO'
				LIMIT 10";

		$branches = $db -> getAll($sql);
		
		if($branches){
			return $branches;
		}else{
			return FALSE;
		}
	}
	
	public static function getAccountTypes($db) {
		$sql = "SHOW COLUMNS FROM dealer LIKE 'account_type_dea'";
		$result = $db->Execute($sql);

		$type = $result->fields[1];
		$type = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $type));
		return $type;
	}
        
        /**
	 * @name getSubmanagerByCity
	 * @param <String> $db
	 * @param <String> $serial_cit
	 * @return <array>
	 */
	public static function getSubmanagerByCity($db, $serial_cit) {
		$sql = "SELECT d.serial_dea, d.name_dea
				FROM dealer d, sector s
				WHERE d.serial_sec = s.serial_sec
                                AND d.manager_rights_dea = 'YES'
                                AND d.status_dea = 'ACTIVE'
                                AND s.serial_cit = $serial_cit";

		$result=$db->getAll($sql);

		if($result){
			return $result;
		}else{
			return false;
		}
	}
     
	/**
	 * @name getMyManagersInfo
	 * @param type $db
	 * @param type $serial_dea
	 * @return boolean 
	 */
    public static function getMyManagerInfo($db, $serial_dea){
		$sql = "SELECT d.serial_dea, d.dea_serial_dea, d.serial_mbc, m.serial_man, m.name_man, 
						cou.name_cou, cou.code_cou, cou.serial_cou
				FROM dealer d
				JOIN manager_by_country mbc ON mbc.serial_mbc = d.serial_mbc AND d.serial_dea = $serial_dea
				JOIN country cou ON cou.serial_cou = mbc.serial_cou
				JOIN manager m ON m.serial_man = mbc.serial_man";
		
		//Debug::print_r($sql); die;
		$result = $db -> getRow($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function insertDealerLog($db,$serial_usr,$json_previous,$json_update,$date_update,$serial_dea){
        $sql="INSERT INTO `dealer_log` (`serial_user`,
            `json_previous`,
            `json_update`,
            `date_update`,
            `serial_dea`) VALUES (
            '" . $serial_usr . "',
            '" . $json_previous . "',
            '" . $json_update . "',
            '" . $date_update . "',
            '" . $serial_dea . "'
               )";

        $result = $db->Execute($sql);

        if ($result) {
            return $db->insert_ID();
        } else {
           // ErrorLog::log($this->db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
        }
    }

    public static function getLogByDealer($db,$serial_dea){
	    $sql="SELECT dl.serial_user as 'SERIAL_USER',
              concat(us.first_name_usr,' ', us.last_name_usr) AS 'USUARIO_ACTUALIZADOR',
              dl.json_previous AS 'DATOS_PREVIOS',
              dl.json_update AS 'DATOS_ACTUALIZADOS',
              dl.date_update AS 'FECHA_ACTUALIZACION' 
              FROM dealer_log dl 
              left join user us on us.serial_usr=dl.serial_user 
              where dl.serial_dea=$serial_dea order by date_update desc;";
        $result = $db->Execute($sql);

        $num=$result->RecordCount();
        if ($num > 0) {
            $arr = array();
            $cont = 0;

            do {
                $arr[$cont] = $result->GetRowAssoc(false);
                $result->MoveNext();
                $cont++;
            } while ($cont < $num);
        }

        return $arr;
    }

	//Getters
	public function getSerial_dea() {
		return $this->serial_dea;
	}

	public function getSerial_sec() {
		return $this->serial_sec;
	}

	public function getSerial_mbc() {
		return $this->serial_mbc;
	}

	public function getSerial_cdd() {
		return $this->serial_cdd;
	}

	public function getSerial_dlt() {
		return $this->serial_dlt;
	}

	public function getDea_serial_dea() {
		return $this->dea_serial_dea;
	}

	public function getSerial_usr() {
		return $this->serial_usr;
	}

	public function getId_dea() {
		return $this->id_dea;
	}

	public function getCode_dea() {
		return $this->code_dea;
	}

	public function getName_dea() {
		return $this->name_dea;
	}

	public function getCategory_dea() {
		return $this->category_dea;
	}

	public function getAddress_dea() {
		return $this->address_dea;
	}

	public function getPhone1_dea() {
		return $this->phone1_dea;
	}

	public function getPhone2_dea() {
		return $this->phone2_dea;
	}

	public function getContract_date_dea() {
		return $this->contract_date_dea;
	}

	public function getFax_dea() {
		return $this->fax_dea;
	}

	public function getEmail1_dea() {
		return $this->email1_dea;
	}

	public function getEmail2_dea() {
		return $this->email2_dea;
	}

	public function getContact_dea() {
		return $this->contact_dea;
	}

	public function getPhone_contact_dea() {
		return $this->phone_contact_dea;
	}

	public function getEmail_contact_dea() {
		return $this->email_contact_dea;
	}

	public function getPercentage_dea() {
		return $this->percentage_dea;
	}

	public function getType_percentage_dea() {
		return $this->type_percentage_dea;
	}

	public function getStatus_dea() {
		return $this->status_dea;
	}

	public function getPayment_deadline_dea() {
		return $this->payment_deadline_dea;
	}

	public function getBill_to_dea() {
		return $this->bill_to_dea;
	}

	public function getPay_day_dea() {
		return $this->pay_day_dea;
	}

	public function getPay_contact_dea() {
		return $this->pay_contact_dea;
	}

	public function getAssistance_contact_dea() {
		return $this->assistance_contact_dea;
	}

	public function getEmail_assistance_dea() {
		return $this->email_assistance_dea;
	}

	public function getBranch_number_dea() {
		return $this->branch_number_dea;
	}

	public function getVisits_number_dea() {
		return $this->visits_number_dea;
	}

	public function getOfficial_seller_dea() {
		return $this->official_seller_dea;
	}

	public function getLast_commission_paid_dea() {
		return $this->last_commission_paid_dea;
	}

	public function getBonus_to_dea() {
		return $this->bonus_to_dea;
	}

	public function getManager_name_dea() {
		return $this->manager_name_dea;
	}

	public function getManager_phone_dea() {
		return $this->manager_phone_dea;
	}

	public function getManager_email_dea() {
		return $this->manager_email_dea;
	}

	public function getManager_birthday_dea() {
		return $this->manager_birthday_dea;
	}

	public function getPhone_sales_dea() {
		return $this->phone_sales_dea;
	}

	public function getCreated_on_dea() {
		return $this->created_on_dea;
	}

	public function getManager_rights_dea() {
		return $this->manager_rights_dea;
	}

	public function getBank_dea() {
		return $this->bank_dea;
	}

	public function getAccount_type_dea() {
		return $this->account_type_dea;
	}

	public function getAccount_number_dea() {
		return $this->account_number_dea;
	}

	public function getAuxData_dea() {
		return $this->aux_data;
	}

	public function getMinimum_kits_dea() {
		return $this->minimum_kits_dea;
	}
	
	public function getNotificacions_dea() {
		return $this->notifications_dea;
	}

	public function getLogo_dea(){
		return $this->logo_dea;
	}

	//Setters
	public function setSerial_dea($serial_dea) {
		$this->serial_dea = $serial_dea;
	}

	public function setSerial_sec($serial_sec) {
		$this->serial_sec = $serial_sec;
	}

	public function setSerial_mbc($serial_mbc) {
		$this->serial_mbc = $serial_mbc;
	}

	public function setSerial_cdd($serial_cdd) {
		$this->serial_cdd = $serial_cdd;
	}

	public function setSerial_dlt($serial_dlt) {
		$this->serial_dlt = $serial_dlt;
	}

	public function setDea_serial_dea($dea_serial_dea) {
		$this->dea_serial_dea = $dea_serial_dea;
	}

	public function setSerial_usr($serial_usr) {
		$this->serial_usr = $serial_usr;
	}

	public function setId_dea($id_dea) {
		$this->id_dea = $id_dea;
	}

	public function setCode_dea($code_dea) {
		$this->code_dea = $code_dea;
	}

	public function setName_dea($name_dea) {
		$this->name_dea = utf8_decode($name_dea);
	}

	public function setCategory_dea($category_dea) {
		$this->category_dea = $category_dea;
	}

	public function setAddress_dea($address_dea) {
		$this->address_dea = utf8_decode($address_dea);
	}

	public function setPhone1_dea($phone1_dea) {
		$this->phone1_dea = $phone1_dea;
	}

	public function setPhone2_dea($phone2_dea) {
		$this->phone2_dea = $phone2_dea;
	}

	public function setContract_date_dea($contract_date_dea) {
		$this->contract_date_dea = $contract_date_dea;
	}

	public function setFax_dea($fax_dea) {
		$this->fax_dea = $fax_dea;
	}

	public function setEmail1_dea($email1_dea) {
		$this->email1_dea = $email1_dea;
	}

	public function setEmail2_dea($email2_dea) {
		$this->email2_dea = $email2_dea;
	}

	public function setContact_dea($contact_dea) {
		$this->contact_dea = utf8_decode($contact_dea);
	}

	public function setPhone_contact_dea($phone_contact_dea) {
		$this->phone_contact_dea = $phone_contact_dea;
	}

	public function setEmail_contact_dea($email_contact_dea) {
		$this->email_contact_dea = $email_contact_dea;
	}

	public function setPercentage_dea($percentage_dea) {
		$this->percentage_dea = $percentage_dea;
	}

	public function setType_percentage_dea($type_percentage_dea) {
		$this->type_percentage_dea = $type_percentage_dea;
	}

	public function setStatus_dea($status_dea) {
		$this->status_dea = $status_dea;
	}

	public function setPayment_deadline_dea($payment_deadline_dea) {
		$this->payment_deadline_dea = $payment_deadline_dea;
	}

	public function setBill_to_dea($bill_to_dea) {
		$this->bill_to_dea = $bill_to_dea;
	}

	public function setPay_day_dea($pay_day_dea) {
		$this->pay_day_dea = $pay_day_dea;
	}

	public function setPay_contact_dea($pay_contact_dea) {
		$this->pay_contact_dea = utf8_decode($pay_contact_dea);
	}

	public function setAssistance_contact_dea($assistance_contact_dea) {
		$this->assistance_contact_dea = utf8_decode($assistance_contact_dea);
	}

	public function setEmail_assistance_dea($email_assistance_dea) {
		$this->email_assistance_dea = $email_assistance_dea;
	}

	public function setBranch_number_dea($branch_number_dea) {
		$this->branch_number_dea = $branch_number_dea;
	}

	public function setVisits_number_dea($visits_number_dea) {
		$this->visits_number_dea = $visits_number_dea;
	}

	public function setOfficial_seller_dea($official_seller_dea) {
		$this->official_seller_dea = $official_seller_dea;
	}

	public function setLast_commission_paid_dea($last_commission_paid_dea) {
		$this->last_commission_paid_dea = $last_commission_paid_dea;
	}

	public function setBonus_to_dea($bonus_to_dea) {
		$this->bonus_to_dea = $bonus_to_dea;
	}

	public function setManager_name_dea($manager_name_dea) {
		$this->manager_name_dea = utf8_decode($manager_name_dea);
	}

	public function setManager_phone_dea($manager_phone_dea) {
		$this->manager_phone_dea = $manager_phone_dea;
	}

	public function setManager_email_dea($manager_email_dea) {
		$this->manager_email_dea = $manager_email_dea;
	}

	public function setManager_birthday_dea($manager_birthday_dea) {
		$this->manager_birthday_dea = $manager_birthday_dea;
	}

	public function setPhone_sales_dea($phone_sales_dea) {
		$this->phone_sales_dea = $phone_sales_dea;
	}

	public function setCreated_on_dea($created_on_dea) {
		$this->created_on_dea = $created_on_dea;
	}

	public function setManager_rights_dea($manager_rights_dea) {
		$this->manager_rights_dea = $manager_rights_dea;
	}

	public function setBank_dea($bank_dea) {
		$this->bank_dea = $bank_dea;
	}

	public function setAccount_type_dea($account_type_dea) {
		$this->account_type_dea = $account_type_dea;
	}

	public function setAccount_number_dea($account_number_dea) {
		$this->account_number_dea = $account_number_dea;
	}

	public function setAux_data($aux_data) {
		$this->aux_data = $aux_data;
	}

	public function setMinimum_kits_dea($minimum_kits_dea) {
		$this->minimum_kits_dea = $minimum_kits_dea;
	}

	public function setNotificacions_dea($notifications_dea) {
		$this->notifications_dea = $notifications_dea;
	}
	public function setLogo_dea($logo_dea){
		$this->logo_dea = $logo_dea;
}
}

?>