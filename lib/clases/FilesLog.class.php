<?php
/**
 * File: FilesLog
 * Author: Patricio Astudillo
 * Creation Date: 05/08/2015
 * Last Modified By: 05/08/2015
 */

class FilesLog {
    var $db;
    var $id;
    var $serial_fle;
    var $opened_on;
    var $opening_user;
	var $finished_on;
	var $finished_user;
	var $status;
	
	function __construct($db, $id = NULL){
            $this -> db = $db;
            $this -> id = $id;
			
			$this->load();
    }
	
	public function load(){
		$sql = "SELECT * FROM file_status_log WHERE id = $this->id";
		
		$result = $this->db->getRow($sql);
		
		if($result){
			$this -> serial_fle = $result['serial_fle'];
			$this -> opened_on = $result['opened_on'];
			$this -> opening_user = $result['opening_user'];
			$this -> finished_on = $result['finished_on'];
			$this -> finished_user = $result['finished_user'];
			$this -> status = $result['status'];
			
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	public function save(){
		if($this->id){
			$sql = "UPDATE file_status_log SET "
					. "finishing_user = $this->finished_user, "
					. "finished_on = CURRENT_TIMESTAMP(), "
					. "status = '$this->status'"
					. " WHERE id = $this->id";
		}else{
			$sql = "INSERT INTO file_status_log (
						id,
						serial_fle,
						opening_user,
						status
					) VALUES (
						NULL,
						{$this -> serial_fle},
						{$this -> opening_user},
						'{$this -> status}'
					)";
		}
		
		$result = $this->db->Execute($sql);
		
		if($result){
			if(!$this->id):
				$this->id = $this->db->insert_ID();
			endif;
			
			return $this->load();
		}else{
			ErrorLog::log($this -> db, 'SAVE FAILED - '.get_class($this), $sql);
			return FALSE;
		}
	}
	
	public static function getActiveLogForFileAndStatus($db, $serial_fle, $status){
		$sql = "SELECT id
				FROM file_status_log fl
				WHERE fl.serial_fle = $serial_fle
				AND status = '$status'
				AND finished_on IS NULL";
		
		$result = $db->getRow($sql);
		
		if($result){
			return $result;
		}else{
			ErrorLog::log($db, 'GET CTIVE LOG FAILED FOR STATUS '.$status.' AND FILE '.$serial_fle, $sql);
			return FALSE;
		}
	}
	
	public static function getFileHistory($db, $serial_fle){
		$sql = "SELECT fl.id, fl.serial_fle, fl.status, 
					CONCAT(uop.first_name_usr, ' ', uop.last_name_usr) AS 'opener',
					CONCAT(ufi.first_name_usr, ' ', ufi.last_name_usr) AS 'finisher', 
					f.diagnosis_fle, f.cause_fle,
					DATE_FORMAT(opened_on, '%d/%m/%Y %H:%m:%s') AS 'opened_on',
					IF(finished_on IS NULL, 'NA', DATE_FORMAT(finished_on, '%d/%m/%Y %H:%m:%s')) AS 'finished_on',
					IF(finished_on IS NULL, 'NA', TIMEDIFF(finished_on, opened_on)) AS 'timeElapsed'
				FROM file_status_log fl
				JOIN file f ON f.serial_fle = fl.serial_fle
				JOIN user uop ON uop.serial_usr = fl.opening_user
				JOIN user ufi ON ufi.serial_usr = fl.finishing_user
				WHERE fl.serial_fle = $serial_fle";
		
		//Debug::print_r($sql);
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
	
	public static function getFilesLogForAudit($db, $from_date, $to_date, $detailed = TRUE, $forFile = NULL){
		$sql = "SELECT fl.id, fl.serial_fle, fl.status, 
						CONCAT(uop.first_name_usr, ' ', uop.last_name_usr) AS 'opener',
						CONCAT(ufi.first_name_usr, ' ', ufi.last_name_usr) AS 'finisher', 
						f.diagnosis_fle, f.cause_fle,
						DATE_FORMAT(opened_on, '%d/%m/%Y %H:%m:%s') AS 'opened_on',
						IF(finished_on IS NULL, 'NA', DATE_FORMAT(finished_on, '%d/%m/%Y %H:%m:%s')) AS 'finished_on',
						IF(finished_on IS NULL, 'NA', TIMEDIFF(finished_on, opened_on)) AS 'timeElapsed'
				FROM file_status_log fl
				JOIN file f ON f.serial_fle = fl.serial_fle
					AND STR_TO_DATE(DATE_FORMAT(f.creation_date_fle, '%d/%m/%Y'), '%d/%m/%Y') BETWEEN STR_TO_DATE('$from_date', '%d/%m/%Y') AND STR_TO_DATE('$to_date', '%d/%m/%Y')
				JOIN user uop ON uop.serial_usr = fl.opening_user
				LEFT JOIN user ufi ON ufi.serial_usr = fl.finishing_user";
		
		//Debug::print_r($sql); die;
		$result = $db->getAll($sql);
		
		if($result){
			return $result;
		}else{
			return FALSE;
		}
	}
			
	
	function getId() {
		return $this->id;
	}

	function getSerial_fle() {
		return $this->serial_fle;
	}

	function getOpened_on() {
		return $this->opened_on;
	}

	function getOpening_user() {
		return $this->opening_user;
	}

	function getFinished_on() {
		return $this->finished_on;
	}

	function getFinished_user() {
		return $this->finished_user;
	}

	function getStatus() {
		return $this->status;
	}

	function setId($id) {
		$this->id = $id;
	}

	function setSerial_fle($serial_fle) {
		$this->serial_fle = $serial_fle;
	}

	function setOpened_on($opened_on) {
		$this->opened_on = $opened_on;
	}

	function setOpening_user($opening_user) {
		$this->opening_user = $opening_user;
	}

	function setFinished_on($finished_on) {
		$this->finished_on = $finished_on;
	}

	function setFinished_user($finished_user) {
		$this->finished_user = $finished_user;
	}

	function setStatus($status) {
		$this->status = $status;
	}
}