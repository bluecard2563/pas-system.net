<?php

class TandiConnectionFunctions {

    const EC_MBC_ARRAY = '2,3,4,5,6,31,32,33,34,35,36,38,40';
    const EC_URL_ERP = 'http://18.234.9.61:8087/';
    const EC_USR = 'ross';
    const EC_PSWRD = 'ross';

    public static function defineERPConnectionForManager($serial_mbc) {
        if (in_array($serial_mbc, split(',', self::EC_MBC_ARRAY))) {
            define('URL_ERP', self::EC_URL_ERP);
            define('URL_USR', self::EC_USRD);
            define('URL_PSWD', self::EC_PSWRD);

            return TRUE;
        }
    }

    /*
      public static function getSaleDataForOO($db, $serial_sal) {
      $sql = "SELECT s.card_number_sal, s.type_sal, s.total_cost_sal, s.total_sal,
      pbd.serial_pxc, d.serial_mbc, pbl.name_pbl,
      IF(b.bill_to_dea = 'CUSTOMER', CONCAT('C', s.serial_cus), CONCAT('D', b.serial_dea)) AS 'client_id',
      end_date_sal, begin_date_sal, s.emission_date_sal, s.observations_sal, id_erp_sal,
      CONCAT(IFNULL(cus.first_name_cus, ''), ' ', IFNULL(cus.last_name_cus, '')) AS 'full_name_cus',
      u.username_usr AS 'responsible',
      DATE_FORMAT(end_date_sal, '%Y-%m-%d') AS 'endDate',
      DATE_FORMAT(begin_date_sal, '%Y-%m-%d') AS 'beginDate',
      DATE_FORMAT(s.emission_date_sal, '%Y-%m-%d') AS 'emissionDate',
      b.id_dea, s.serial_cus, cus.document_cus, cus.first_name_cus, cus.last_name_cus,
      DATE_FORMAT(birthdate_cus, '%Y-%m-%d') AS 'birthdate_cus', IF(cus.gender_cus = 'H', 'M', cus.gender_cus) as 'gender_cus',
      cus.address_cus, cus.phone1_cus, cus.email_cus, sc.sise_id,
      sg.pais_id, sg.dpto_id, sg.municipio_id
      FROM sales s
      JOIN customer cus ON cus.serial_cus = s.serial_cus
      JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
      JOIN product_by_country pxc ON pxc.serial_pxc = pbd.serial_pxc
      JOIN product_by_language pbl ON pbl.serial_pro = pxc.serial_pro AND pbl.serial_lang = 2
      JOIN dealer d ON d.serial_dea = pbd.serial_dea
      JOIN city cit ON cit.serial_cit = cus.serial_cit
      JOIN country cou On cou.serial_cou = cit.serial_cou


      JOIN counter cnt ON cnt.serial_cnt = s.serial_cnt
      JOIN dealer b ON b.serial_dea = cnt.serial_dea
      JOIN user_by_dealer ubd ON ubd.serial_dea = b.serial_dea AND ubd.status_ubd = 'ACTIVE'
      JOIN user u ON u.serial_usr = ubd.serial_usr
      LEFT JOIN sise_country sc On sc.serial_cou = cou.serial_cou
      LEFT JOIN sise_geolocation_for_city sg On sg.serial_cit = cus.serial_cit
      WHERE s.serial_sal = $serial_sal";

      $result = $db->getRow($sql);

      if ($result) {
      return $result;
      } else {
      return FALSE;
      }
      }
     */
    /*
      public static function getInvoiceDataForOO($db, $serial, $type) {
      if ($type == 'D') {
      $sql = "SELECT serial_cdd AS 'payterm', CONCAT('D', serial_dea) AS 'client_id'
      FROM dealer d
      WHERE serial_dea = $serial";

      $result = $db->getRow($sql);
      } else {
      $result = array('client_id' => 'C' . $serial, 'payterm' => '10');
      }


      return $result;
      }
     */
    /* ++++++++++++++++OBTENER ID SUCURSAL DE TANDI A PARTIR DEL SERIAL MBC +++++++++++++++++++ */

    public static function getTandiSucursalID($serial_man) {
        switch ($serial_man) {
            case 2:
            case 19:
            case 20:
                $tandi_sucid = "88"; //Quito
                break;
            case 3:
                $tandi_sucid = "802684952"; //Guayaquil
                break;
            case 4:
                $tandi_sucid = "220377055233"; //Cuenca
                break;
            case 5:
                $tandi_sucid = "10724324278345"; //Quito Isla
                break;
            case 6:
                $tandi_sucid = "10724324278343"; //Guayaquil Isla
                break;
            case 21:
                $tandi_sucid = "802684955"; //Manta
                break;
            case 26:
                $tandi_sucid = "87"; //Ambato
                break;
            case 23:
                $tandi_sucid = "10724324278347"; //Loja
                break;
				case 27:
                $tandi_sucid = "10755949865177"; //Machala
                break;

            default:
                $tandi_sucid = FALSE;
        }

        return $tandi_sucid;
    }

    /* ++++++++++++++++OBTENER ID DEL PUNTO DE VENTA DE TANDI A PARTIR DEL SERIAL MBC +++++++++++++++++++ */

    public static function getTandiPtoID($serial_man) {
        switch ($serial_man) {
            case 2:
            case 19:
                $tandi_ptoid = "10724324278348"; //Quito
                break;
            case 3:
                $tandi_ptoid = "10724324278354"; //Guayaquil
                break;
            case 4:
                $tandi_ptoid = "10724324278362"; //Cuenca
                break;
            case 5:
                $tandi_ptoid = "10724324278352"; //Quito Isla
                break;
            case 6:
                $tandi_ptoid = "10724324278356"; //Guayaquil Isla
                break;
            case 20:
                $tandi_ptoid = "10724324278350"; //WEB
                break;
            case 21:
                $tandi_ptoid = "10724324278358"; //Manta
                break;
            case 26:
                $tandi_ptoid = "10724324278364"; //Ambato
                break;
            case 23:
                $tandi_ptoid = "10724324278360"; //Loja
                break;
				case 27:
                $tandi_ptoid = "10755949866707"; //Machala
                break;

            default:
                $tandi_ptoid = FALSE;
        }

        return $tandi_ptoid;
    }

    /* ++++++++++++++++OBTENER SERIAL MBC DEL BAS A PARTIR DEL ID DEL PUNTO DE VENTA TANDI +++++++++++++++++++ */

    public static function getBASMbc($tandi_id) {
        switch ($tandi_id) {
            case 10724324278348:

                $BASmbc = "2"; //Quito
                break;
            case 10724324278354:
                $BASmbc = "3"; //Guayaquil
                break;
            case 10724324278362:
                $BASmbc = "4"; //Cuenca
                break;
            case 10724324278352:
                $BASmbc = "5"; //Quito Isla
                break;
            case 10724324278356:
                $BASmbc = "6"; //Guayaquil Isla
                break;
            case 10724324278350:
                $BASmbc = "31"; //Web
                break;
            case 10724324278358:
                $BASmbc = "32"; //Manta
                break;
            case 10724324278364:
                $BASmbc = "38"; //Ambato
                break;
            case 10724324278360:
                $BASmbc = "34"; //Loja
                break;
				case 10755949866707:
                $BASmbc = "40"; //Machala
                break;

            default:
                $BASmbc = 2;
        }

        return $BASmbc;
    }

    /* +++++++++FUNCION PARA OBTENER EL TIPO DE DUCUMENTO +++++++++++++++++++ */

    public static function retrieveDocumentType($document) {
        /* IDType: [NE]. Indica el tipo de documento. Opciones v�lidas:
         * 	0: Ninguno
         * 	1: RUC.
         * 	2: C�dula.
         * 	7: Pasaporte.
         */

        $temp = (string) $document;

        if (is_numeric($document) || strlen($temp) < 10) {
            $document = str_split($document);

            if ($document[2] == 9) {
                if (GlobalFunctions::validate_ruc_9($document)) {
                    return '1'; //'RUC';
                } else {
                    return '0'; //'OTHER';
                }
            } elseif ($document[2] == 6) {
                if (GlobalFunctions::validate_ruc_6($document)) {
                    return '1'; //'RUC';
                } else {
                    return '0'; //'OTHER';
                }
            } elseif ($document[2] < 6) {
                $aux_ci = $document;
                unset($document[10]);
                unset($document[11]);
                unset($document[12]);

                if (GlobalFunctions::validate_ci(implode('', $document))) {
                    if (sizeof($aux_ci) == 10) {
                        return '2'; //'CI';
                    } else {
                        return '1'; //'RUC';
                    }
                } else {
                    return '0'; //'OTHER';
                }
            } else {
                return '0'; //'OTHER';
            }
        } else {
            return '7'; //'PASSPORT';
        }
    }

    /* +++++++++ Connect Tandi WS Emision +++++++++++++++++++ */

    public static function transformDate($date) {
//        Debug::print_r($date);
        $formated_date = str_replace('/', '-', $date);
        $newDate = date_create($formated_date);
        $goodDate = date_format($newDate, "Y-m-d");
//        Debug::print_r($goodDate);die();
        return $goodDate;
    }

    //Tipo de Documento//
    public static function documentType($document) {
//        $documentType = self::retrieveDocumentType($document);
        switch (strlen($document)) {
            case '10':
                $document = 'c';
                return $document;
//                $tipoEntidad = 'J';
                break;
            case '13':
                $document = 'r';
                return $document;
//                $tipoEntidad = 'N';
                break;
//            case '3':
//                $document = 'p';
//                return $document;
//                $tipoEntidad = 'N';
//                break;
            default:
                $document = 'p';
                return $document;
//                $tipoEntidad = 'N';
        }
    }

    //Tipo de Entidad//
    public static function tipoEntidad($document) {
//        $documentType = self::retrieveDocumentType($document);

        switch (strlen($document)) {
            case '10':
                $document = 'N';
                return $document;
//                $tipoEntidad = 'J';
                break;
            case '13':
                $document = 'J';
                return $document;
//                $tipoEntidad = 'N';
                break;
//            case '3':
//                $document = 'N';
//                return $document;
////                $tipoEntidad = 'N';
//                break;
            default:
                $document = 'N';
                return $document;
//                $tipoEntidad = 'N';
        }
    }

    public static function redondeo($numero) {
        $redondeo = number_format((float) $numero, 2, '.', '');
        return $redondeo;
    }

    //Calcular Capitados Con Descuento
    public static function capitados($serial_pro, $prima, $days, $discount) {
        global $db;
        $capitado_dental = 3.97;
        $capitado_vida = 0.59;
        $capitado_planet = 45.625 / 365;
        $capitado_planet_corportivo = 0;

        $dental = TandiGlobalFunctions::productName($db, $serial_pro, 'DENTAL');
//        Debug::print_r($prima);
        if ($dental) {

            $desc = (1 - ($discount / 100));
            $capitado_descontado = round(($capitado_dental * $desc), 2, PHP_ROUND_HALF_UP);
            $descuento_dental = round(($capitado_dental - $capitado_descontado), 2, PHP_ROUND_HALF_UP);
        }

        $vida = TandiGlobalFunctions::productName($db, $serial_pro, 'VIDA');
        if ($vida) {

            $desc = (1 - ($discount / 100));
            $capitado_vida_descontado = round(($capitado_vida * $desc), 2, PHP_ROUND_HALF_UP);
            $descuento_vida = round(($capitado_vida - $capitado_vida_descontado), 2, PHP_ROUND_HALF_UP);
        }

        //CAPITADO PLANET
        if($days > 365){
            $days = 365;
        }
        $capitado_tecnicos = round($days * $capitado_planet,2,PHP_ROUND_HALF_UP);
        
        $desc = (1 - ($discount / 100));
        $capitado_planet_descontado = round(($capitado_tecnicos * $desc), 2, PHP_ROUND_HALF_UP);
        $descuento_tecnico = round(($capitado_tecnicos - $capitado_planet_descontado), 2, PHP_ROUND_HALF_UP);
//        print_r($capitado_tecnicos * $desc);
//        print_r('dental');
//        print_r($descuento_dental);
//        print_r('vida');
//        print_r($descuento_vida);
//        print_r('tecnico');
//        print_r($capitado_planet_descontado);
//        print_r($descuento_tecnico);die();
        $suma_descunetos_capitados = $descuento_dental + $descuento_vida + $descuento_tecnico;
        $suma_descunetos_capitados = 0;
//        print_r('suma');
//        print_r($suma_descunetos_capitados);
//        Debug::print_r($capitado);die();
        return $suma_descunetos_capitados;
    }

    //Calcular PRIMA SIN CAPITADOS
    public static function prima_sin_capitados($serial_pro, $prima, $days, $discount) {
        global $db;
        $capitados = 0;
        $capitado_dental = 3.97;
        $capitado_vida = 0.59;
        $capitado_planet = 45.625 / 365;
        $capitado_planet_corporativo = 0;

        $dental = TandiGlobalFunctions::productName($db, $serial_pro, 'DENTAL');
        //Debug::print_r('dental');
       // Debug::print_r($dental);
        if ($dental) {
            $capitados += $capitado_dental;
          //  Debug::print_r('dental: '.$capitados);
        }

        $vida = TandiGlobalFunctions::productName($db, $serial_pro, 'VIDA');
        //Debug::print_r('vida');
        //Debug::print_r($vida);
        if ($vida) {
            $capitados += $capitado_vida;
            //Debug::print_r('vida: '.$capitados);
        }

        //CAPITADO PLANET
        if($days > 365){
            $days = 365;
        }
    //     if($serial_pro == 5){
    //         $capitado_tecnicos = 0;
    //     }else{
    //     $capitado_tecnicos = round(($days * $capitado_planet), 2, PHP_ROUND_HALF_UP);
    //     }
    //   //  Debug::print_r('tenico: '.$capitado_tecnicos);

    //     if ($serial_pro ==191) {
    //         $capitados = 0;
    //     } else {
    //         $capitados += $capitado_tecnicos;
    //     }
       $capitados = 0;
        //Debug::print_r("Prima",$prima);
        //Debug::print_r("Capitados",$capitados);
        $prima_sin_capitados = $prima - $capitados;
        //Debug::print_r($prima_sin_capitados);
        return $prima_sin_capitados;
    }

    //Calcular DESCUENTO A LA PRIMA SIN CAPITADOS
    public static function descuento_sin_capitados($serial_pro, $prima, $days, $discount, $prima_desc) {
//        Debug::print_r('prueba');die();
//        Debug::print_r($prima);
//        Debug::print_r($prima_desc);
//        Debug::print_r(self::capitados($serial_pro, $prima, $days, $discount));
        $descuento = (($prima - $prima_desc) - self::capitados($serial_pro, $prima, $days, $discount));
//        Debug::print_r($descuento);die();
        return $descuento;
    }
  /* +++++++++ Connect Tandi WS Emision WEB+++++++++++++++++++ */

    public static function tandiEmisionWeb($sending_data, $action = 'POST') {
        //Generate Sending Array
        $documentType = self::retrieveDocumentType($sending_data['txtCustomerDocument']);
        switch ($documentType) {
            case '1':
                $documentType = 'r';
                $tipoEntidad = 'J';
                break;
            case '2':
                $documentType = 'c';
                $tipoEntidad = 'N';
                break;
            case '3':
                $documentType = 'p';
                $tipoEntidad = 'N';
                break;
            default:
                $documentType = 'c';
                $tipoEntidad = 'N';
        }
        $arraySales = array();
        $polizasArray = array();
        foreach ($sending_data['hdnSaleId'] as $sale) {
            $serialSal = $sale;
            array_push($arraySales, $serialSal);

            global $db;
            //Obtain Sales Data
            $card = new Sales($db, $sale);
            $card->getData();

            //Obtain Contract Data
            $contract = new Contracts($db, $card->getSerial_con());
            $contract->getData();

            //Obtain Product Data
            $pbd = new ProductByDealer($db, $card->getSerial_pbd());
            $pbd->getData();

            $pro = new ProductByCountry($db, $pbd->getSerial_pxc());
            $pro->getData();

            //Obtain CardHolder
            $cardHolder = new Customer($db, $card->getSerial_cus());
            $cardHolder->getData();

            //Obtain Counter
            $counter = new Counter($db, $card->getSerial_cnt());
            $counter->getData();
            //Obtain Dealer
            $dealer = new Dealer($db, $counter->getSerial_dea());
            $dealer->getData();
                        
            //Obtain Extras (If Any)
            $extraArray = array();
            $extras = Extras::getExtrasBySale($db, $sale);

            //Obtain Discount
            $discount = $sending_data['txtDiscount'] + $sending_data['txtOtherDiscount'];

             //Calculate Variables
            $subTotal = ($card->getTotal_sal() - (($card->getTotal_sal() * $discount) /100));
            $subTotalSSC = round((($subTotal * 100)/100.50), 2, PHP_ROUND_HALF_UP);
            $totalSal = round((($subTotal * 100)/100.50), 2, PHP_ROUND_HALF_UP);
            $scExtra = $subTotal - $totalSal;
//          //Validate Discount
            $inicio=new DateTime(self::parseDate($card->getBeginDate_sal()));
            //Debug::print_r($inicio);
            $fin=new DateTime(self::parseDate($card->getEndDate_sal()));
            //Debug::print_r($fin);
            $interval = $inicio->diff($fin);
            //Debug::print_r($interval->days);
            $finalDays=$interval->days+1;
            if($discount > 0){
                 $subTotalSalWebD = round((($card->getTotal_sal() * 100)/100.50), 2, PHP_ROUND_HALF_UP);
                           
                            $subTotalSalDiscountD = round(($discount / 100) * $subTotalSalWebD, 2);
                           
                            $totalSSCD = $subTotalSalWebD - $subTotalSalDiscountD;
                            
                            $subtotalSCD = round($totalSSCD * 0.005, 2, PHP_ROUND_HALF_UP);
                            
                            $totalSalSCD = $totalSSCD + $subtotalSCD;
                            
                            $fixed_amount_with_couponD = $totalSalSCD;

                $prima = self::prima_sin_capitados($pro->getSerial_pro(), $fixed_amount_with_couponD, $finalDays, $discount);
                $descuento = round(self::descuento_sin_capitados($pro->getSerial_pro(), $card->getTotal_sal(), $finalDays, $discount, $subTotalSSC), 2, PHP_ROUND_HALF_UP);

            $subTotalSSC = round((($fixed_amount_with_couponD * 100)/100.50), 2, PHP_ROUND_HALF_UP);
            $totalDis= $fixed_amount_with_couponD - $subTotalSSC; 
            $sc = $totalDis;
            }else{
                $prima = self::prima_sin_capitados($pro->getSerial_pro(), $totalSal, $finalDays, $discount);
                $descuento = 0;
                $sc = 0;
            }
            if ($extras) {
                foreach ($extras as $extra) {

                    $cusExtra = new customer($db, $extra['serial_cus']);
                    $cusExtra->getData();
                    //Datos del Cliente/Asegurado
                    $tempArray['apellidosAsegurado'] = utf8_encode($cusExtra->getLastname_cus());
                    $tempArray['correoAsegurado'] = $cusExtra->getEmail_cus();
                    $city = self::getTandiCity($db, $cusExtra->getSerial_cit());
                    if ($city) {
                        $tempArray['idCiudad'] = $city;
                    } else {
                        $tempArray['idCiudad'] = '1311768578';
                    }
                    $tempArray['direccionDomicilio'] = utf8_encode($cusExtra->getAddress_cus());
                    $tempArray['identificacion'] = trim($cusExtra->getDocument_cus());
                    $tempArray['tipoIdentificacion'] = self::documentType(trim($cusExtra->getDocument_cus()));
                    $tempArray['nombresAsegurado'] = utf8_encode($cusExtra->getFirstname_cus());
                    $tempArray['idParentesco'] = '20';
                    $tempArray['prima'] = $extra['fee_ext'];
                    $ClienteBirthDate = self::transformDate($cusExtra->getBirthdate_cus());
                    $tempArray['fechaNacimiento'] = $ClienteBirthDate;
                    if ($cusExtra->getGender_cus() == 'ND' || $cusExtra->getGender_cus() == null) {
                        $tempArray['genero'] = 'm';
                    } else {
                        $tempArray['genero'] = $cusExtra->getGender_cus();
                    }
                    array_push($extraArray, $tempArray);
                }
            }
            if ($pro->getSerial_pro() == '111' || $pro->getSerial_pro() == '112' || $pro->getSerial_pro() == '136' || $pro->getSerial_pro() == '137' || $pro->getSerial_pro() == '138') {
                $asegurado = array();
                $aseguradoTemp['apellidosAsegurado'] = 'Final';
                $aseguradoTemp['correoAsegurado'] = 'consumidor@final.com';
                $aseguradoTemp['direccionDomicilio'] = 'Consumidor Final';
                $aseguradoTemp['fechaAlta'] = self::transformDate($card->getEmissionDate_sal());
                $aseguradoTemp['fechaIngreso'] = self::transformDate($card->getEmissionDate_sal());
                $aseguradoTemp['genero'] = 'm';
                $aseguradoTemp['idParentesco'] = '19';
                $aseguradoTemp['identificacion'] = '9999999999';
                $aseguradoTemp['nombresAsegurado'] = 'Consumidor';
                $aseguradoTemp['prima'] = $prima - $sc;
                $aseguradoTemp['telefonoAsegurado'] = '9999999999';
                $aseguradoTemp['tipoIdentificacion'] = 'c';
                $aseguradoTemp['tipoTitularId'] = '811335685';
                $city = self::getTandiCity($db, $cardHolder->getSerial_cit());
                if ($city) {
                    $aseguradoTemp['idCiudad'] = $city;
                } else {
                    $aseguradoTemp['idCiudad'] = '1311768578';
                }
                $aseguradoTemp['fechaNacimiento'] = '2000-01-01';
                if ($extraArray != null) {
                    $aseguradoTemp['dependientes'] = $extraArray;
                }
            } else {
                //Obtain Asegurado
                $asegurado = array();
                $aseguradoTemp['apellidosAsegurado'] = utf8_encode($cardHolder->getLastname_cus());
                $aseguradoTemp['correoAsegurado'] = utf8_encode($cardHolder->getEmail_cus());
                $aseguradoTemp['direccionDomicilio'] = utf8_encode($cardHolder->getAddress_cus());
                $aseguradoTemp['fechaAlta'] = self::transformDate($card->getEmissionDate_sal());
                $aseguradoTemp['fechaIngreso'] = self::transformDate($card->getEmissionDate_sal());
                if ($cardHolder->getGender_cus() == 'ND' || $cardHolder->getGender_cus() == null) {
                    $aseguradoTemp['genero'] = 'm';
                } else {
                    $aseguradoTemp['genero'] = $cardHolder->getGender_cus();
                }
                $aseguradoTemp['idParentesco'] = '19';
                $aseguradoTemp['identificacion'] = trim($cardHolder->getDocument_cus());
                $aseguradoTemp['nombresAsegurado'] = utf8_encode($cardHolder->getFirstname_cus());
                if ($extras) {
                    //$prima = self::prima_sin_capitados($pro->getSerial_pro(), $card->getFee_sal(), $finalDays, $discount);
                     if($discount > 0) {
                        $aseguradoTemp['prima'] = $prima - $sc;
                    }else {
                        $aseguradoTemp['prima'] = $prima;
                    }
                } else {
                    $aseguradoTemp['prima'] = $prima - $sc;
                }
                $aseguradoTemp['telefonoAsegurado'] = $cardHolder->getPhone1_cus();
                $aseguradoTemp['tipoIdentificacion'] = self::documentType(trim($cardHolder->getDocument_cus()));
                $aseguradoTemp['tipoTitularId'] = '811335685';
                $city = self::getTandiCity($db, $cardHolder->getSerial_cit());
                if ($city) {
                    $aseguradoTemp['idCiudad'] = $city;
                } else {
                    $aseguradoTemp['idCiudad'] = '1311768578';
                }
                $aseguradoTemp['fechaNacimiento'] = self::transformDate($cardHolder->getBirthdate_cus());
                if ($extraArray != null) {
                    $aseguradoTemp['dependientes'] = $extraArray;
                }
            }
            array_push($asegurado, $aseguradoTemp);

            //Polizas Array
            $polizasTempArray['asegurado'] = $asegurado;
            $polizasTempArray['comisionAgente'] = '0.00';
            if ($discount > 0) {
               // $polizasTempArray['descuento'] = $descuento;
                $polizasTempArray['descuento'] = '0';
            } else {
                $polizasTempArray['descuento'] = '0';
            }
            $polizasTempArray['fechaEmision'] = self::transformDate($card->getEmissionDate_sal());
            $polizasTempArray['fechaVigenciaDesde'] = self::transformDate($card->getBeginDate_sal());
            if($contract->getSerial_con() == null){
                $polizasTempArray['fechaVigenciaDesdeContenedor'] = self::transformDate($card->getBeginDate_sal());
            }else{
                $polizasTempArray['fechaVigenciaDesdeContenedor'] = self::transformDate($contract->getBegin_date_con());
            }
            //Validamos las fechas de vigencia
            if ($pro->getSerial_pro() == 7 || $pro->getSerial_pro() == 110 || $pro->getSerial_pro() == 100 || $pro->getSerial_pro() == 111 || $pro->getSerial_pro() == 112 || $pro->getSerial_pro() == 113 || $pro->getSerial_pro() == 115 || $pro->getSerial_pro() == 132) {
                if($card->getDays_sal() > 365){
                    $date = self::transformDate($card->getBeginDate_sal());
                    $polizasTempArray['fechaVigenciaHasta'] = date('Y-m-d', strtotime($date. ' + 364 days'));
                }else{
                    $date = self::transformDate($card->getBeginDate_sal());
                    $polizasTempArray['fechaVigenciaHasta'] = date('Y-m-d', strtotime($date . ' + ' . $card->getDays_sal() . ' days - 1 days'));
                }
            } elseif($card->getBeginDate_sal() == $card->getEndDate_sal()) {
                $date = self::transformDate($card->getBeginDate_sal());
                $polizasTempArray['fechaVigenciaHasta'] = date('Y-m-d', strtotime($date . ' + 1 days'));
            }else{
                $polizasTempArray['fechaVigenciaHasta'] = self::transformDate($card->getEndDate_sal());
            }
            //Si el contrato no existe
            if($contract->getSerial_con() == null){
                //Verificar que la fecha inicio y fin sean distintas
                if($card->getBeginDate_sal() == $card->getEndDate_sal()){
                    $date = self::transformDate($card->getBeginDate_sal());
                    $polizasTempArray['fechaVigenciaHastaContenedor'] = date('Y-m-d', strtotime($date . ' + 1 days'));
                }else{
                    $polizasTempArray['fechaVigenciaHastaContenedor'] = self::transformDate($card->getEndDate_sal());
                }
            }else{
                $polizasTempArray['fechaVigenciaHastaContenedor'] = self::transformDate($contract->getEnd_date_con());
            }
            $polizasTempArray['idAgente'] = '1';
            $polizasTempArray['idPuntoVenta'] = self::getTandiPtoID($sending_data['hdnSerialMan']);
            $polizasTempArray['idSucursal'] = self::getTandiSucursalID($sending_data['hdnSerialMan']);
            $polizasTempArray['idUnidadNegocio'] = '10602427325341';
            $unidadProduccion = self::getResponsibleDataForSale($db, $card->getSerial_sal());
            $polizasTempArray['idUnidadProduccion'] = $unidadProduccion['document_usr'];
            $polizasTempArray['idproductoHomologado'] = $pro->getSerial_pro();
             //Validación número de poliza madre
            $card_number_mother = TandiGlobalFunctions::getCardNumberMother($db,$dealer->getDea_serial_dea());

            $startDate = new DateTime(self::parseDate($card->getBeginDate_sal()));
            $endDate = new DateTime(self::parseDate($card->getEndDate_sal()));
            $intervalF = $startDate->diff($endDate);
            $finalDaysF = $intervalF->days+1;
            if ($finalDaysF >= 365) {
                if($contract->getSerial_con() == null){
                    $polizasTempArray['numeroContenedor'] = $card->getSerial_sal();
                }else{
                    $polizasTempArray['numeroContenedor'] = $contract->getSerial_con();
                }
            }else{
                //$polizasTempArray['numeroContenedor'] =  $card_number_mother['card_number_mother'];
                $polizasTempArray['numeroContenedor'] =  100112;
            }

            //End Validación número poliza madre
            if ($card->getCardNumber_sal() == null) {
                $polizasTempArray['numeroCertificado'] = $card->getSerial_sal();
                $polizasTempArray['numeroPoliza'] = $card->getSerial_sal();
            } else {
                $polizasTempArray['numeroCertificado'] = $card->getCardNumber_sal();
                $polizasTempArray['numeroPoliza'] = $card->getCardNumber_sal();
            }
            //$polizasTempArray['porcentajeDescuento'] = $discount;
            $polizasTempArray['porcentajeDescuento'] = '0';
            $polizasTempArray['tipoContenedor'] = 'A';
            $polizasTempArray['usuarioEmision'] = $counter->getSerial_usr();
            array_push($polizasArray, $polizasTempArray);
        }
//        die();
        //Obtain Payer
        if ($sending_data['bill_to_radio'] == 'CUSTOMER') {
            //Pagador es una persona natural
            $pagador = new Customer($db, $sending_data['hdnSerialCus']);
            $pagador->getData();

            $pagadorArray = array();
            $pagadorArray['apellidos'] = utf8_encode($pagador->getLastname_cus());
            $city = self::getTandiCity($db, $pagador->getSerial_cit());
            if ($city) {
                $pagadorArray['ciudad'] = $city;
            } else {
                $pagadorArray['ciudad'] = '1311768578';
            }
            if($pagador->getEmail_cus() == null){
                $pagadorArray['correoElectronico'] = 'operador1@bluecard.com.ec';
            }else{
            $pagadorArray['correoElectronico'] = $pagador->getEmail_cus();
            }
            $pagadorArray['direccion'] = utf8_encode($pagador->getAddress_cus());
            $pagadorArray['identificacion'] = trim($pagador->getDocument_cus());
            $pagadorArray['tipoIdentificacion'] = self::documentType(trim($pagador->getDocument_cus()));
            $pagadorArray['tipoEntidad'] = self::tipoEntidad($pagador->getDocument_cus());
            if ($pagadorArray['tipoEntidad'] == 'J') {
                $pagadorArray['sectorEmpresaId'] = '1';
            }
            $pagadorArray['nombres'] = utf8_encode($pagador->getFirstname_cus());
            $pagadorArray['nombresCompletos'] = utf8_encode($pagador->getLastname_cus() . ' ' . $pagador->getFirstname_cus());
            $birthDate = self::transformDate($pagador->getBirthdate_cus());
            $pagadorArray['fechaNacimiento'] = $birthDate;
            if ($pagador->getGender_cus() == 'ND' || $pagador->getGender_cus() == null) {
                $pagadorArray['genero'] = 'm';
            } else {
                $pagadorArray['genero'] = $pagador->getGender_cus();
            }

            $cliente = array();
            //Datos del Cliente
            $cliente['identificacion'] = trim($pagador->getDocument_cus());
            $cliente['tipoIdentificacion'] = self::documentType(trim($pagador->getDocument_cus()));
            $cliente['nombres'] = utf8_encode($pagador->getFirstname_cus());
            $cliente['apellidos'] = utf8_encode($pagador->getLastname_cus());
            $cliente['nombresCompletos'] = utf8_encode($pagador->getLastname_cus() . ' ' . $pagador->getFirstname_cus());
            $city = self::getTandiCity($db, $pagador->getSerial_cit());
            if ($city) {
                $cliente['ciudad'] = $city;
            } else {
                $cliente['ciudad'] = '1311768578';
            }
            if($pagador->getEmail_cus() == null){
                $cliente['correoElectronico'] = 'operador1@bluecard.com.ec';
            }else{
            $cliente['correoElectronico'] = $pagador->getEmail_cus();
            }
            $cliente['direccion'] = utf8_encode($pagador->getAddress_cus());
            $cliente['tipoEntidad'] = self::tipoEntidad($pagador->getDocument_cus());
            if ($cliente['tipoEntidad'] == 'J') {
                $cliente['sectorEmpresaId'] = '1';
            }
            $cliente['fechaNacimiento'] = self::transformDate($pagador->getBirthdate_cus());
            if ($pagador->getGender_cus() == 'ND' || $pagador->getGender_cus() == null) {
                $cliente['genero'] = 'm';
            } else {
                $cliente['genero'] = $pagador->getGender_cus();
            }
        }
        else {
            //El pagador es un comercializador
            $dealer = TandiGlobalFunctions::dealerIDExists($db, $sending_data['hdnDealerDocument']);
            $pagador = new Dealer($db, $dealer[0]['serial_dea']);
            $pagador->getData();
            $pagadorArray = array();
            //get Sector
            $sector = new Sector($db, $pagador->getSerial_sec());
            $sector->getData();
            //get City
            $city = self::getTandiCity($db, $sector->getSerial_cit());

            // The $city variable obtained is the current tandi city code saved in the tandi_cit.
            $pagadorArray['ciudad'] = $city;
            $pagadorArray['correoElectronico'] = $pagador->getEmail2_dea();
            $pagadorArray['direccion'] = utf8_encode($pagador->getAddress_dea());
            $pagadorArray['identificacion'] = trim($pagador->getId_dea());
            $pagadorArray['tipoIdentificacion'] = self::documentType(trim($pagador->getId_dea()));
            $pagadorArray['tipoEntidad'] = self::tipoEntidad($pagador->getId_dea());
            $pagadorArray['nombres'] = utf8_encode($pagador->getName_dea());
            $pagadorArray['nombresCompletos'] = utf8_encode($pagador->getName_dea());
            $pagadorArray['fechaNacimiento'] = date('Y-m-d');
            $pagadorArray['sectorEmpresaId'] = '1';

            $cliente = array();
            //Datos del Cliente
            $cliente['identificacion'] = utf8_encode($pagador->getId_dea());
            $cliente['tipoIdentificacion'] = self::documentType(trim($pagador->getId_dea()));
            $cliente['nombres'] = utf8_encode($pagador->getName_dea());
            $cliente['nombresCompletos'] = utf8_encode($pagador->getName_dea());
            if ($city) {
                $cliente['ciudad'] = $city;
            } else {
                $cliente['ciudad'] = '1311768578';
            }
            $cliente['correoElectronico'] = $pagador->getEmail2_dea();
            $cliente['direccion'] = utf8_encode($pagador->getAddress_dea());
            $cliente['tipoEntidad'] = self::tipoEntidad($pagador->getId_dea());
            $cliente['fechaNacimiento'] = date('Y-m-d');
            $cliente['sectorEmpresaId'] = '1';
        }
        $sending_array = array();

        //Build final Array
        $tempArray = array();
        $tempArray['cliente'] = $cliente;
        $tempArray['cajaId'] = '10729573843336';
        $tempArray['numeroPagos'] = '1';
        $tempArray['monedaId'] = '11141120';
        $tempArray['cobradorId'] = '1';
        $tempArray['concepto'] = 'Pago WEB BlueCard';
        $tempArray['conceptoTrans'] = 'Pago WEB BlueCard';
        $tempArray['importePagoTotal'] = $sending_data['pago'];
        $tempArray['numeroAutorizacion'] = $sending_data['idpago'];
        $tempArray['idComisionTajeta'] = '10729573779117';
        $tempArray['idTarjetaCredito'] = '10729573778932';
        $tempArray['numeroDocumentoTrans'] = $sending_data['idpago'];
        $tempArray['numeroTarjeta'] = "XXXX-XXXX-XXXX-1234";
        $tempArray['numeroVoucher'] = $sending_data['idpago'];
        $tempArray['valorTarjeta'] = $sending_data['pago'];
        $tempArray['fechaPrimerpago'] = Date('Y-m-d');
        $tempArray['pass'] = '';
        $tempArray['pagador'] = $pagadorArray;
        $tempArray['polizas'] = $polizasArray;
        $tempArray['puerto'] = '';
        $tempArray['esMigrado'] = '0';
        $tempArray['user'] = '';
        array_push($sending_array, $tempArray);
        $json = json_encode($tempArray, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        Sales::insertLogWSnewSale($db,$sending_data['hdnDealerDocument'],2,serialize($json));
//        Debug::print_r($json);die();
        //Tandi Log
        if(USE_TANDI){
        $tandiLog = TandiLog::insert($db, 'SALE', serialize($arraySales), $json);

        if (!function_exists('curl_init'))
            die('CURL No instalado. Por favor comun&iacute;quese con el administrador.');
        $curl = curl_init();



        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bluecard.com.ec/services/emision/put",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 3000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic cm9zczpyb3Nz",
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $jsonResponse = json_decode($response, true);
        $err = curl_error($curl);
        curl_close($curl);

        if ($jsonResponse['idFactura'] == null) {
                TandiLog::updateLog($db, $tandiLog, "ERROR", serialize(str_replace('\\', '', $jsonResponse)), 200);
            return false;
        } else {
            TandiLog::updateLog($db, $tandiLog, "SUCCESS", serialize($jsonResponse), 200);
            return $jsonResponse;
        }
        }else{
            return false;
        }
    }
	
	  /*Parse date for count days tandiEmision*/

    public static function parseDate($date){
        $date_array = explode("/",$date); // split the array
        $var_day = $date_array[0]; //day seqment
        $var_month = $date_array[1]; //month segment
        $var_year = $date_array[2]; //year segment
        $new_date_format = "$var_year-$var_month-$var_day"; // join them together
        return $new_date_format;
        // Debug::print_r($new_date_format);

    }
	
    /* +++++++++ Connect Tandi WS Emision +++++++++++++++++++ */

    public static function tandiEmision($sending_data, $action = 'POST') {
        //Generate Sending Array
        $documentType = self::retrieveDocumentType($sending_data['txtCustomerDocument']);
        switch ($documentType) {
            case '1':
                $documentType = 'r';
                $tipoEntidad = 'J';
                break;
            case '2':
                $documentType = 'c';
                $tipoEntidad = 'N';
                break;
            case '3':
                $documentType = 'p';
                $tipoEntidad = 'N';
                break;
            default:
                $documentType = 'c';
                $tipoEntidad = 'N';
        }
        $arraySales = array();
        $polizasArray = array();
        foreach ($sending_data['hdnSaleId'] as $sale) {
            $serialSal = $sale;
            array_push($arraySales, $serialSal);

            global $db;
            //Obtain Sales Data
            $card = new Sales($db, $sale);
            $card->getData();

            //Obtain Contract Data
            $contract = new Contracts($db, $card->getSerial_con());
            $contract->getData();

            //Obtain Product Data
            $pbd = new ProductByDealer($db, $card->getSerial_pbd());
            $pbd->getData();

            $pro = new ProductByCountry($db, $pbd->getSerial_pxc());
            $pro->getData();

            //Obtain CardHolder
            $cardHolder = new Customer($db, $card->getSerial_cus());
            $cardHolder->getData();

            //Obtain Counter
            $counter = new Counter($db, $card->getSerial_cnt());
            $counter->getData();
            //Obtain Dealer
            $dealer = new Dealer($db, $counter->getSerial_dea());
            $dealer->getData();
                        
            //Obtain Extras (If Any)
            $extraArray = array();
            $extras = Extras::getExtrasBySale($db, $sale);

            //Obtain Discount
            $discount = $sending_data['txtDiscount'] + $sending_data['txtOtherDiscount'];

            //Calculate Variables
            $subTotal = ($card->getTotal_sal() - (($card->getTotal_sal() * $discount) /100));
            $subTotalSSC = round((($subTotal * 100)/100.50), 2, PHP_ROUND_HALF_UP);
            $totalSal = round((($subTotal * 100)/100.50), 2, PHP_ROUND_HALF_UP);
            $scExtra = $subTotal - $totalSal;
//          //Validate Discount
            $inicio=new DateTime(self::parseDate($card->getBeginDate_sal()));
            //Debug::print_r($inicio);
            $fin=new DateTime(self::parseDate($card->getEndDate_sal()));
            //Debug::print_r($fin);
            $interval = $inicio->diff($fin);
            //Debug::print_r($interval->days);
            $finalDays=$interval->days+1;

            if($discount > 0){
                $prima = self::prima_sin_capitados($pro->getSerial_pro(), $card->getTotal_sal(), $finalDays, $discount);
                $descuento = round(self::descuento_sin_capitados($pro->getSerial_pro(), $card->getTotal_sal(), $finalDays, $discount, $subTotalSSC), 2, PHP_ROUND_HALF_UP);
                $sc = $subTotalSSC - $totalSal;
                $scEx=$subTotalSSC - $totalSal;
            }else{
                $prima = self::prima_sin_capitados($pro->getSerial_pro(), $totalSal, $finalDays, $discount);
                $descuento = 0;
                $sc = 0;
            }
            if ($extras) {
//                Debug::print_r($extras);
                foreach ($extras as $extra) {

                    $cusExtra = new customer($db, $extra['serial_cus']);
                    $cusExtra->getData();
                    //Datos del Cliente/Asegurado
                    $tempArray['apellidosAsegurado'] = utf8_encode($cusExtra->getLastname_cus());
                    $tempArray['correoAsegurado'] = $cusExtra->getEmail_cus();
                    $city = self::getTandiCity($db, $cusExtra->getSerial_cit());
                    if ($city) {
                        $tempArray['idCiudad'] = $city;
                    } else {
                        $tempArray['idCiudad'] = '1311768578';
                    }
                    $tempArray['direccionDomicilio'] = utf8_encode($cusExtra->getAddress_cus());
                    $tempArray['identificacion'] = trim($cusExtra->getDocument_cus());
                    $tempArray['tipoIdentificacion'] = self::documentType(trim($cusExtra->getDocument_cus()));
                    $tempArray['nombresAsegurado'] = utf8_encode($cusExtra->getFirstname_cus());
                    $tempArray['idParentesco'] = '20';
                    $tempArray['prima'] = $extra['fee_ext'];
                    $ClienteBirthDate = self::transformDate($cusExtra->getBirthdate_cus());
                    $tempArray['fechaNacimiento'] = $ClienteBirthDate;
                    if ($cusExtra->getGender_cus() == 'ND' || $cusExtra->getGender_cus() == null) {
                        $tempArray['genero'] = 'm';
                    } else {
                        $tempArray['genero'] = $cusExtra->getGender_cus();
                    }
                    array_push($extraArray, $tempArray);
                }
            }
            if ($pro->getSerial_pro() == '111' || $pro->getSerial_pro() == '112' || $pro->getSerial_pro() == '136' || $pro->getSerial_pro() == '137' || $pro->getSerial_pro() == '138' || $pro->getSerial_pro() == '139' || $pro->getSerial_pro() == '131' || $pro->getSerial_pro() == '88' || $pro->getSerial_pro() == '100' || $pro->getSerial_pro() == '133' || $cardHolder->getType_cus() == 'LEGAL_ENTITY')  {
                $asegurado = array();
                $aseguradoTemp['apellidosAsegurado'] = 'Final';
                $aseguradoTemp['correoAsegurado'] = 'consumidor@final.com';
                $aseguradoTemp['direccionDomicilio'] = 'Consumidor Final';
                $aseguradoTemp['fechaAlta'] = self::transformDate($card->getEmissionDate_sal());
                $aseguradoTemp['fechaIngreso'] = self::transformDate($card->getEmissionDate_sal());
                $aseguradoTemp['genero'] = 'm';
                $aseguradoTemp['idParentesco'] = '19';
                $aseguradoTemp['identificacion'] = '9999999999002';
                $aseguradoTemp['nombresAsegurado'] = 'Consumidor';
                $aseguradoTemp['prima'] = $prima - $sc;
                $aseguradoTemp['telefonoAsegurado'] = '9999999999';
                $aseguradoTemp['tipoIdentificacion'] = 'c';
                $aseguradoTemp['tipoTitularId'] = '811335685';
                $city = self::getTandiCity($db, $cardHolder->getSerial_cit());
                if ($city) {
                    $aseguradoTemp['idCiudad'] = $city;
                } else {
                    $aseguradoTemp['idCiudad'] = '1311768578';
                }
                $aseguradoTemp['fechaNacimiento'] = '2000-01-01';
               // if ($extraArray != null) {
                    $aseguradoTemp['dependientes'] = $extraArray;
               // }
            } else {
                //Obtain Asegurado
                $asegurado = array();
                $aseguradoTemp['apellidosAsegurado'] = utf8_encode($cardHolder->getLastname_cus());
                $aseguradoTemp['correoAsegurado'] = utf8_encode($cardHolder->getEmail_cus());
                $aseguradoTemp['direccionDomicilio'] = utf8_encode($cardHolder->getAddress_cus());
                $aseguradoTemp['fechaAlta'] = self::transformDate($card->getEmissionDate_sal());
                $aseguradoTemp['fechaIngreso'] = self::transformDate($card->getEmissionDate_sal());
                if ($cardHolder->getGender_cus() == 'ND' || $cardHolder->getGender_cus() == null) {
                    $aseguradoTemp['genero'] = 'm';
                } else {
                    $aseguradoTemp['genero'] = $cardHolder->getGender_cus();
                }
                $aseguradoTemp['idParentesco'] = '19';
                $aseguradoTemp['identificacion'] = trim($cardHolder->getDocument_cus());
                $aseguradoTemp['nombresAsegurado'] = utf8_encode($cardHolder->getFirstname_cus());
                if ($extras) {
                    $prima = self::prima_sin_capitados($pro->getSerial_pro(), $card->getFee_sal(), $finalDays, $discount);
                                        if($discount > 0) {
                        $aseguradoTemp['prima'] = $prima - $scEx;
                    }else {
                        $aseguradoTemp['prima'] = $prima - $scExtra;
                    }

                } else {
                    $aseguradoTemp['prima'] = $prima - $sc;
                }
                $aseguradoTemp['telefonoAsegurado'] = $cardHolder->getPhone1_cus();
                $aseguradoTemp['tipoIdentificacion'] = self::documentType(trim($cardHolder->getDocument_cus()));
                $aseguradoTemp['tipoTitularId'] = '811335685';
                $city = self::getTandiCity($db, $cardHolder->getSerial_cit());
                if ($city) {
                    $aseguradoTemp['idCiudad'] = $city;
                } else {
                    $aseguradoTemp['idCiudad'] = '1311768578';
                }
                $aseguradoTemp['fechaNacimiento'] = self::transformDate($cardHolder->getBirthdate_cus());
                //if ($extraArray != null) {
                    $aseguradoTemp['dependientes'] = $extraArray;
                //}
            }
            array_push($asegurado, $aseguradoTemp);

            //Polizas Array
            $polizasTempArray['asegurado'] = $asegurado;
            
            $typeDealer = $dealer->getSerial_dlt();
            if ($typeDealer == 1){
                $polizasTempArray['comisionAgente'] = $dealer->getPercentage_dea();
            } else {
                $polizasTempArray['comisionAgente'] = '0.00';
            }

            if ($discount > 0) {
                $polizasTempArray['descuento'] = $descuento;
            } else {
                $polizasTempArray['descuento'] = '0';
            }
            $polizasTempArray['fechaEmision'] = self::transformDate($card->getEmissionDate_sal());
            $polizasTempArray['fechaVigenciaDesde'] = self::transformDate($card->getBeginDate_sal());
            if($contract->getSerial_con() == null){
                $polizasTempArray['fechaVigenciaDesdeContenedor'] = self::transformDate($card->getBeginDate_sal());
            }else{
                $polizasTempArray['fechaVigenciaDesdeContenedor'] = self::transformDate($contract->getBegin_date_con());
            }
            //Validamos las fechas de vigencia
            //if ($pro->getSerial_pro() == 7 || $pro->getSerial_pro() == 110 || $pro->getSerial_pro() == 100 || $pro->getSerial_pro() == 111 || $pro->getSerial_pro() == 112 || $pro->getSerial_pro() == 113 || $pro->getSerial_pro() == 115 || $pro->getSerial_pro() == 132) {
              //  if($card->getDays_sal() > 365){
                //    $date = self::transformDate($card->getBeginDate_sal());
                  //  $polizasTempArray['fechaVigenciaHasta'] = date('Y-m-d', strtotime($date. ' + 364 days'));
                //}else{
                  //  $date = self::transformDate($card->getBeginDate_sal());
                    //$polizasTempArray['fechaVigenciaHasta'] = date('Y-m-d', strtotime($date . ' + ' . $card->getDays_sal() . ' days - 1 days'));
                //}
//            }
			if($card->getBeginDate_sal() == $card->getEndDate_sal()) {
                $date = self::transformDate($card->getBeginDate_sal());
                $polizasTempArray['fechaVigenciaHasta'] = date('Y-m-d', strtotime($date . ' + 1 days'));
            }else{
                $polizasTempArray['fechaVigenciaHasta'] = self::transformDate($card->getEndDate_sal());
            }
            //Si el contrato no existe
            if($contract->getSerial_con() == null){
                //Verificar que la fecha inicio y fin sean distintas
                if($card->getBeginDate_sal() == $card->getEndDate_sal()){
                    $date = self::transformDate($card->getBeginDate_sal());
                    $polizasTempArray['fechaVigenciaHastaContenedor'] = date('Y-m-d', strtotime($date . ' + 1 days'));
                }else{
                    $polizasTempArray['fechaVigenciaHastaContenedor'] = self::transformDate($card->getEndDate_sal());
                }
            }else{
                $polizasTempArray['fechaVigenciaHastaContenedor'] = self::transformDate($contract->getEnd_date_con());
            }
            $agente = TandiGlobalFunctions::getDealerTandiId($dealer->getId_dea());
            if($agente['idRespuesta'] == 0){
                $polizasTempArray['idAgente'] = $agente['idEntidad'];
            }else{
                $polizasTempArray['idAgente'] = '1';
            }

            $typePercentageDealer = $dealer->getType_percentage_dea();
            $typeDealer = $dealer->getSerial_dlt();

            if(($sending_data['bill_to_radio'] == 'CUSTOMER' || $sending_data['bill_to_radio'] == 'OTHER') && $typePercentageDealer == 'DISCOUNT' && $typeDealer == '1') {
                $polizasTempArray['porcentajeCanal'] = 0;
                $polizasTempArray['identificacionCanal'] ='-1';
            }elseif(($sending_data['bill_to_radio'] == 'DEALER') && $typePercentageDealer == 'DISCOUNT'){
                $polizasTempArray['idAgente'] = '1';
                     $polizasTempArray['porcentajeCanal'] = 0;
                $polizasTempArray['identificacionCanal'] = $dealer->getId_dea();
                    
            }elseif (($sending_data['bill_to_radio'] == 'CUSTOMER' || $sending_data['bill_to_radio'] == 'OTHER') && $typePercentageDealer == 'DISCOUNT') {
                $polizasTempArray['idAgente'] = '1';
                $polizasTempArray['porcentajeCanal'] = $dealer->getPercentage_dea();
                $polizasTempArray['identificacionCanal'] = $dealer->getId_dea();
            }elseif($typeDealer != '1' && $typePercentageDealer == 'COMISSION' && $polizasTempArray['idAgente'] != '1'){
                $polizasTempArray['idAgente'] = '1';
                $polizasTempArray['porcentajeCanal'] = $dealer->getPercentage_dea();
                $polizasTempArray['identificacionCanal'] = $dealer->getId_dea();
            }
             elseif ($polizasTempArray['idAgente'] != '1' || $typePercentageDealer == 'DISCOUNT') {
                //Debug::print_r($polizasTempArray['idAgente']);die();
                $polizasTempArray['porcentajeCanal'] = $dealer->getPercentage_dea();
                $polizasTempArray['identificacionCanal'] ='-1';
            } else {
                $polizasTempArray['porcentajeCanal'] = $dealer->getPercentage_dea();
                $polizasTempArray['identificacionCanal'] = $dealer->getId_dea();
            } 
            
            $polizasTempArray['idPuntoVenta'] = self::getTandiPtoID($sending_data['hdnSerialMan']);
            $polizasTempArray['idSucursal'] = self::getTandiSucursalID($sending_data['hdnSerialMan']);
            $polizasTempArray['idUnidadNegocio'] = '10602427325341';
            $unidadProduccion = self::getResponsibleDataForSale($db, $card->getSerial_sal());
            $polizasTempArray['idUnidadProduccion'] = $unidadProduccion['document_usr'];
            $polizasTempArray['idproductoHomologado'] = $pro->getSerial_pro();
           //Validación número de poliza madre
            $card_number_mother = TandiGlobalFunctions::getCardNumberMother($db,$dealer->getDea_serial_dea());

            $startDate = new DateTime(self::parseDate($card->getBeginDate_sal()));
            $endDate = new DateTime(self::parseDate($card->getEndDate_sal()));
            $intervalF = $startDate->diff($endDate);
            $finalDaysF = $intervalF->days+1;
            if ($finalDaysF >= 365) {
                if($contract->getSerial_con() == null){
                    $polizasTempArray['numeroContenedor'] = $card->getSerial_sal();
                }else{
                    $polizasTempArray['numeroContenedor'] = $contract->getSerial_con();
                }
            }else{
                //$polizasTempArray['numeroContenedor'] =  $card_number_mother['card_number_mother'];
                $polizasTempArray['numeroContenedor'] =  100112;
            }

            //End Validación número poliza madre
            if ($card->getCardNumber_sal() == null) {
                $polizasTempArray['numeroCertificado'] = $card->getSerial_sal();
                $polizasTempArray['numeroPoliza'] = $card->getSerial_sal();
            } else {
                $polizasTempArray['numeroCertificado'] = $card->getCardNumber_sal();
                $polizasTempArray['numeroPoliza'] = $card->getCardNumber_sal();
            }
            $polizasTempArray['porcentajeDescuento'] = $discount;
            $polizasTempArray['tipoContenedor'] = 'A';
            $polizasTempArray['usuarioEmision'] = $counter->getSerial_usr();
            array_push($polizasArray, $polizasTempArray);
        }
        //Obtain Payer
        if ($sending_data['bill_to_radio'] == 'CUSTOMER') {
            //Pagador es una persona natural
            $pagador = new Customer($db, $sending_data['hdnSerialCus']);
            $pagador->getData();

            $pagadorArray = array();
            $pagadorArray['apellidos'] = utf8_encode($pagador->getLastname_cus());
            $city = self::getTandiCity($db, $pagador->getSerial_cit());
            if ($city) {
                $pagadorArray['ciudad'] = $city;
            } else {
                $pagadorArray['ciudad'] = '1311768578';
            }
            if($pagador->getEmail_cus() == null){
                $pagadorArray['correoElectronico'] = 'operador1@bluecard.com.ec';
            }else{
                $pagadorArray['correoElectronico'] = $pagador->getEmail_cus();
            }
            $pagadorArray['direccion'] = utf8_encode($pagador->getAddress_cus());
            $pagadorArray['identificacion'] = trim($pagador->getDocument_cus());
            $pagadorArray['tipoIdentificacion'] = self::documentType(trim($pagador->getDocument_cus()));
            $pagadorArray['tipoEntidad'] = self::tipoEntidad($pagador->getDocument_cus());
            if ($pagadorArray['tipoEntidad'] == 'J') {
                $pagadorArray['sectorEmpresaId'] = '1';
            }
            $pagadorArray['nombres'] = utf8_encode($pagador->getFirstname_cus());
            $pagadorArray['nombresCompletos'] = utf8_encode($pagador->getLastname_cus() . ' ' . $pagador->getFirstname_cus());
            $birthDate = self::transformDate($pagador->getBirthdate_cus());
            $pagadorArray['fechaNacimiento'] = $birthDate;
            if ($pagador->getGender_cus() == 'ND' || $pagador->getGender_cus() == null) {
                $pagadorArray['genero'] = 'm';
            } else {
                $pagadorArray['genero'] = $pagador->getGender_cus();
            }

            $cliente = array();
            //Datos del Cliente
            $cliente['identificacion'] = trim($pagador->getDocument_cus());
            $cliente['tipoIdentificacion'] = self::documentType(trim($pagador->getDocument_cus()));
            $cliente['nombres'] = utf8_encode($pagador->getFirstname_cus());
            $cliente['apellidos'] = utf8_encode($pagador->getLastname_cus());
            $cliente['nombresCompletos'] = utf8_encode($pagador->getLastname_cus() . ' ' . $pagador->getFirstname_cus());
            $city = self::getTandiCity($db, $pagador->getSerial_cit());
            if ($city) {
                $cliente['ciudad'] = $city;
            } else {
                $cliente['ciudad'] = '1311768578';
            }
            if($pagador->getEmail_cus() == null){
                $cliente['correoElectronico'] = 'operador1@bluecard.com.ec';
            }else{
                $cliente['correoElectronico'] = $pagador->getEmail_cus();
            }
            $cliente['direccion'] = utf8_encode($pagador->getAddress_cus());
            $cliente['tipoEntidad'] = self::tipoEntidad($pagador->getDocument_cus());
            if ($cliente['tipoEntidad'] == 'J') {
                $cliente['sectorEmpresaId'] = '1';
            }
            $cliente['fechaNacimiento'] = self::transformDate($pagador->getBirthdate_cus());
            if ($pagador->getGender_cus() == 'ND' || $pagador->getGender_cus() == null) {
                $cliente['genero'] = 'm';
            } else {
                $cliente['genero'] = $pagador->getGender_cus();
            }
        }
        else {
            //El pagador es un comercializador
            $pagador = new Dealer($db, $dealer->getSerial_dea());
            $pagador->getData();
            $pagadorArray = array();
            //get Sector
            $sector = new Sector($db, $pagador->getSerial_sec());
            $sector->getData();
            //get City
            $city = self::getTandiCity($db, $sector->getSerial_cit());

            /// The $city variable obtained is the current tandi city code saved in the tandi_cit.
            $pagadorArray['ciudad'] = $city;
            if($pagador->getEmail2_dea() == null){
                $pagadorArray['correoElectronico'] = $pagador->getEmail1_dea();
            }else{
            $pagadorArray['correoElectronico'] = $pagador->getEmail2_dea();
            }
            $pagadorArray['direccion'] = utf8_encode($pagador->getAddress_dea());
            $pagadorArray['identificacion'] = trim($pagador->getId_dea());
            $pagadorArray['tipoIdentificacion'] = self::documentType(trim($pagador->getId_dea()));
            $pagadorArray['tipoEntidad'] = self::tipoEntidad($pagador->getId_dea());
            $pagadorArray['nombres'] = utf8_encode($pagador->getName_dea());
            $pagadorArray['nombresCompletos'] = utf8_encode($pagador->getName_dea());
            $pagadorArray['fechaNacimiento'] = date('Y-m-d');
            $pagadorArray['sectorEmpresaId'] = '1';

            $cliente = array();
            //Datos del Cliente
            $cliente['identificacion'] = utf8_encode($pagador->getId_dea());
            $cliente['tipoIdentificacion'] = self::documentType(trim($pagador->getId_dea()));
            $cliente['nombres'] = utf8_encode($pagador->getName_dea());
            $cliente['nombresCompletos'] = utf8_encode($pagador->getName_dea());
            if ($city) {
                $cliente['ciudad'] = $city;
            } else {
                $cliente['ciudad'] = '1311768578';
            }
            $cliente['correoElectronico'] = $pagador->getEmail2_dea();
            $cliente['direccion'] = utf8_encode($pagador->getAddress_dea());
            $cliente['tipoEntidad'] = self::tipoEntidad($pagador->getId_dea());
            $cliente['fechaNacimiento'] = date('Y-m-d');
            $cliente['sectorEmpresaId'] = '1';
        }
        $sending_array = array();

        //Build final Array
        $tempArray = array();
        $tempArray['cliente'] = $cliente;
        $tempArray['numeroPagos'] = '1';
        $tempArray['monedaId'] = '11141120';
        $tempArray['fechaPrimerpago'] = Date('Y-m-d', strtotime("+10 days"));
        $tempArray['pass'] = '';
        $tempArray['pagador'] = $pagadorArray;
        $tempArray['polizas'] = $polizasArray;
        $tempArray['puerto'] = '';
        $tempArray['esMigrado'] = '0';
        $tempArray['user'] = '';
        array_push($sending_array, $tempArray);
        $json = json_encode($tempArray, JSON_UNESCAPED_UNICODE);

//        Debug::print_r($json);die();
        if(USE_TANDI){
        //Tandi Log
//        $tandiLog = TandiLog::insert($db, 'SALE', serialize($arraySales), $json);
        $tandiLog = TandiLog::insert($db, 'SALE', serialize($arraySales), $tempArray);

        if (!function_exists('curl_init'))
            die('CURL No instalado. Por favor comun&iacute;quese con el administrador.');
        $curl = curl_init();



        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bluecard.com.ec/services/emision/put",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 300000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic cm9zczpyb3Nz",
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $jsonResponse = json_decode($response, true);
        $err = curl_error($curl);
        curl_close($curl);

        if ($jsonResponse['idFactura'] == null) {
                TandiLog::updateLog($db, $tandiLog, "ERROR", serialize(str_replace('\\', '', $jsonResponse)), 200);
            return false;
        } else {
            TandiLog::updateLog($db, $tandiLog, "SUCCESS", serialize($jsonResponse), 200);

            $phoneFull = $cardHolder->getCellphone_cus();
            $phone = substr($phoneFull, 1);

            $getInfoMessageCustomer = $cardHolder->getInfoMessageCustomer($db,$card->getCardNumber_sal());

            if ($getInfoMessageCustomer[0]['serial_pro'] == 197) {
                $body = '¡Hola *{{dato_01}}* te saluda *Mr.Blue*!, tu asistente virtual de *BLUECARD*, quiero agradecerte por la confianza depositada en nosotros, al contratar tu Plan *{{dato_02}}*, con cobertura *ILIMITADA* y vigencia desde *{{dato_04}}* hasta *{{dato_05}}*. En caso de requerir asistencia médica o técnica te puedes contactar al siguiente número *099-622-2100* Solo mensajes de WhatsApp (No aplica para llamadas). Te deseamos un feliz viaje, gracias por elegir BlueCard una marca orgullosamente ecuatoriana.';
            } else {
                $body = '¡Hola *{{dato_01}}* te saluda *Mr.Blue*!, tu asistente virtual de *BLUECARD*, quiero agradecerte por la confianza depositada en nosotros, al contratar tu Plan *{{dato_02}}*, con cobertura de *${{dato_03}}* y vigencia desde *{{dato_04}}* hasta *{{dato_05}}*. En caso de requerir asistencia médica o técnica te puedes contactar al siguiente número *099-622-2100* Solo mensajes de WhatsApp (No aplica para llamadas). Te deseamos un feliz viaje, gracias por elegir BlueCard una marca orgullosamente ecuatoriana.';
            }

            $params = [$getInfoMessageCustomer[0]['cliente'],$getInfoMessageCustomer[0]['producto'],$getInfoMessageCustomer[0]['cobertura'],$getInfoMessageCustomer[0]['inicio'],$getInfoMessageCustomer[0]['fin']];

            // $sendWP = self::sendMessageWhatsApp($phone, $body, $card->getCardNumber_sal(),$params );


            return $jsonResponse;
        }
        }else{
            return false;
    }
    }

    /* +++++++++OBTENER EL SERIAL MBC POR EL ID ERP +++++++++++++++++++ */

    public static function getMbcIdForSalesToERP($db, $id_erp_sal) {
        $sql = "SELECT DISTINCT d.serial_mbc
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN dealer d ON d.serial_dea = pbd.serial_dea
					AND s.id_erp_sal in ($id_erp_sal)";

        $result = $db->getOne($sql);

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    /* +++++++++OBTENER INFORMACIÓN DE LA FACTURA POR EL ERP ID +++++++++++++++++++ */

    public static function translateERPInvoiceIDToPASSerials($db, $erp_invoice_serials) {
        $sql = "SELECT serial_inv, number_inv, total_inv, serial_pay, erp_id
			FROM invoice 
			WHERE erp_id IN ($erp_invoice_serials)
			AND status_inv NOT IN ('PAID', 'UNCOLLECTABLE')";

        $result = $db->getAll($sql);

        if ($result) {
            return $result;
        } else {
            ErrorLog::log($db, 'NO PAS SERIALS FOR ERP INVOICE IDS', $erp_invoice_serials);
            return array();
        }
    }

    /* +++++++++NOTA CREDITO +++++++++++++++++++ */

    public static function logCreditNotePaymentLines($payment_detail) {
        $credit_notes_as_payments = array();

        if (is_array($payment_detail) && count($payment_detail) > 0) {
            foreach ($payment_detail as $pDetail) {
                if ($pDetail['type_pyf'] != 'CREDIT_NOTE'):
                    continue;
                endif;

                $credit_notes_as_payments[$pDetail['invoice']] += $pDetail['amount_pyf'];
            }
        }

        return $credit_notes_as_payments;
    }

    /* +++++++++OBTENER LOS DATOS DEL RESPONABLE A PARTIR DE LA VENTA +++++++++++++++++++ */

    public static function getResponsibleDataForSale($db, $serial_sal) {
        $sql = "SELECT u.document_usr, u.username_usr, u.serial_usr
				FROM sales s
				JOIN product_by_dealer pbd ON pbd.serial_pbd = s.serial_pbd
				JOIN user_by_dealer ubd ON ubd.serial_dea = pbd.serial_dea AND ubd.status_ubd = 'ACTIVE'
				JOIN user u ON u.serial_usr = ubd.serial_usr AND s.serial_sal = $serial_sal";
//        Debug::print_r($sql);die();
        $result = $db->getRow($sql);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    /* +++++++++OBTENER EL ID DE LA CIUDAD DE TANDI A PARTIR DEL SERIAL CIT +++++++++++++++++++ */

    public static function getTandiCity($db, $serial_cit) {
        $sql = "SELECT c.tandi_cit
				FROM city c
				WHERE c.serial_cit = $serial_cit";
        $result = $db->getAll($sql);
//        Debug::print_r($result[0][0]);die();

        if ($result[0][0]) {
            return $result[0][0];
        } else {
            return 1311768578; //Retorno Quito cuando no encuentra Ciudad
        }
    }

    public static function invoiceHasERPConnection($serial_mbc) {
        if (TANDI_ON) {
            $mbc_with_erp = array(2, 3, 4, 5, 6, 31, 32, 33, 34, 35, 36, 38,40);

            if (in_array($serial_mbc, $mbc_with_erp)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public static function sendMessageWhatsApp($phone,$body, $cardNumber, $params){
        $curl = curl_init();
        global $db;
curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://wapi.contactvox.com/instance438883/sendMessageCustom?token=739r7gc5jgh0kzvr',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => 'phone=593'.$phone.'&body='.$body. '&params=' . json_encode($params),
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/x-www-form-urlencoded'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
self::saveSendWhatsAppLog($db,$phone,$cardNumber,$body,$response);
    }

    public static function saveSendWhatsAppLog($db,$numberCustomer, $cardNumberSal, $request, $response) {
        $sql="INSERT INTO `whatsapp_send_log` (
            `number_customer`,
            `card_number_sal`,
            `request`,
            `response`,
            `date_whatsapp_send`
            ) VALUES (
            '" . $numberCustomer . "',
            '" . $cardNumberSal . "',
            '" . $request . "',
            '" . $response . "',
            NOW())";
            $result = $db->Execute($sql);
            
            if ($result) {
            return $db->insert_ID();
            } else {
            ErrorLog::log($db, 'INSERT FAILED - ' . get_class($this), $sql);
            return false;
            }

    }

}

?>
