<?php
function smarty_function_handle_merchant_account_errors($params, &$smarty){
    $error = $params['error'];
    switch ($error) {
        case "2001":
            echo "Test transaction Declined";
            break;
        case "02001":
            echo "Unable to Authorize at this time - Please call credit card issuer";
            break;
        case "02004":
            echo "PIC UP/Card Declined - please use another card";
            break;
        case "02005":
            echo "Declined - please use another card";
            break;
        case "02007":
            echo "Decline - CVV2";
            break;
        case "02013":
            echo "Invalid Amount/Unable to Authorize at this time";
            break;
        case "02014":
            echo "Invalid CID/Invalid Acct/Unable to Authorize at this time";
            break;
        case "02054":
            echo "Invalid Expiry Date - please correct and retry";
            break;
        case "02058":
            echo "Unauth Transaction/Possible set-up error/Unable to Authorize at thie time";
            break;
        case "02100":
            echo "Authorization Cancelled";
            break;
        case "02101":
            echo "Declined - Invalid Transaction Date";
            break;
        case "02102":
            echo "Declined - Processing Error";
            break;
        case "02103":
            echo "Declined - Transaction Previously Settled";
            break;
        case "02104":
            echo "Declined - Authorization Previously Cancelled";
            break;
        case "02105":
            echo "Declined - Authorization Previously Cancelled";
            break;
        case "02106":
            echo "Declined - Settlement Cancellation Time Exceeded";
            break;
        case "22000":
            echo "Cardholder Authenticated";
            break;
        case "22002":
            echo "Card Authentication Failed - Issuer Unavailable";
            break;
        case "22003":
            echo "Card Authentication Failed - Invalid Merchant ID";
            break;
        case "22004":
            echo "Card Authentication Failed - PIC UP/Card Declined";
            break;
        case "22005":
            echo "Card Authentication Failed - Card Declined";
            break;
        case "22007":
            echo "Card Authentication Failed - Decline CVV2";
            break;
        case "1001":
            echo "IP Addresses of fraudulent users are stored in a 'Hot List' maintained by InternetSecure . Transactions coming from any listed IP will be rejected.";
            break;
        case "1002":
            echo "Credit Card numbers reported as 'removed from circulation' (lost/stolen/cancelled) are stored in a 'Hot List' maintained by InternetSecure . Transactions attempted on any such cards will be rejected.";
            break;
        case "1004":
            echo "If a Card number is repeatedly used by a system and is declined, the system marks transaction as fraudulent and stores the CC details";
            break;
        case "1006":
            echo "The Reverse IP addresses of fraudulent users are stored in a 'Hot List' maintained by InternetSecure . Any transaction attempted from the listed IP is rejected.";
            break;
        case "1007":
            echo "IP addresses of fraudulent users are stored in a Hot list maintained by InternetSecure . Any transaction attempted from the listed IP is rejected.";
            break;
        case "1008":
            echo "IP addresses of fraudulent users as controlled by Merchants are stored in a Hot list maintained by InternetSecure . Any transaction attempted from the listed IP is rejected.";
            break;
        case "1009":
            echo "The email addresses of fraudulent users as controlled by Merchants are stored in the system. If this email address is used, the system declines the transaction.";
            break;
        case "1010":
            echo "The email addresses of fraudulent users are stored in the system. If this email address is used, the system declines the transaction.";
            break;
        case "1011":
            echo "The email addresses of fraudulent users are stored in the system. If this email address is used, the system declines the transaction.";
            break;
        case "1014":
            echo "IP Address of Customer (xxxCustomerIP) did not match the Customer Country (xxxCountry). IP Country is in the GeoIP Hot List";
            break;
        case "1095":
            echo "The number of times a Cardholder can use a card at the Merchant website within 24 hours. By default it is set to 1.";
            break;
        case "1096":
            echo "The email address entered by the customer does not have a valid mail server associated with it.";
            break;
        case "11001":
            echo "Transaction does not qualify for online refund.";
            break;
		case "11002":
            echo "Number of refunds has exceeded maximum. This feature is not currently enforced.";
            break;
		case "11003":
            echo "A refund request may not be for more than the total of the original transaction, even if submitted as partial refunds";
            break;
		case "11004":
            echo "Transaction details cannot be processed as submitted. Please verify transaction data.";
            break;
		case "11005":
            echo "The transaction cannot be refunded as it has already been chargedback by the Issuer.";
            break;
		case "11006":
            echo "A refund request for this transaction has already been submitted.";
            break;
		case "11000":
            echo "The refund has been processed successfuly and funds are on the way back to the cardholder.";
            break;
		case "40000":
            echo "Voided";
            break;
		case "40005":
            echo "No Match";
            break;
		case "40e08":
            echo "Declined - Unknown Error";
            break;
		case "40012":
            echo "Invalid Amount";
            break;
		case "40e16":
            echo '$AMT>Max';
            break;
		case "41000":
            echo "Online Void Not Allowed";
            break;
		case "42000":
            echo "Card Authentication Failed";
            break;
		case "70000":
            echo "Recurring Transaction Cancelled";
            break;
		case "70001":
            echo "Recurring Cancellation Failed";
            break;
		case "70002":
            echo "Recurring Previously Cancelled";
            break;
		case "70003":
            echo "Recurring Record Not Found";
            break;
		case "80005":
            echo "3D SecurePassword Not Completed";
            break;
		case "90002":
            echo "ISSUER UNAVAIL/Unable to Authorize at this time";
            break;
		case "90003":
            echo "INVLD MERCH ID/Unable to Authorize at this time";
            break;
		case "90004":
            echo "PIC UP/Card Declined - Please use another card";
            break;
		case "90005":
            echo "DECLINE/Card Declined - Please use another card";
            break;
		case "90006":
            echo "REVERSED/Transaction reversed - Thank you";
            break;
		case "90007":
            echo "Decline - CVV2";
            break;
		case "90008":
            echo "AP WITH ID/Unable to Authorize at this time - Please call your card issuer";
            break;
		case "90012":
            echo "INVLD SERV ID/Unable to Authorize at this time";
            break;
		case "90013":
            echo "INVLD AMOUNT/Unable to Authorize at this time";
            break;
		case "90014":
            echo "INVLD ACCT/Unable to Authorize at this time";
            break;
		case "90019":
            echo "PLEASE RETRY/Unable to Authorize at this time";
            break;
		case "90054":
            echo "INVLD EXP DATE/Invalid Expiry Date - Please correct";
            break;
		case "90055":
            echo "PIN INVLD/Invalid Cust Pin - Please correct";
            break;
		case "90058":
            echo "UNAUTH TRANS/Unable to Authorize at this time";
            break;
		case "90075":
            echo "MAX PIN RETRIES/Max PIN Attempts Exceeded - Please Contact your Issuer";
            break;
		case "90094":
            echo "AP DUPE/A Transaction for this Amount was previously processed - Thank you";
            break;
		case "900N1":
            echo "INV ACCT MATCH/Unable to Authorize at this time";
            break;
		case "900N2":
            echo "INV AMT MATCH/Unable to Authorize at this time";
            break;
		case "900N3":
            echo "INV ITEM NUM/Unable to Authorize at this time";
            break;
		case "900N4":
            echo "ITEM REV-VOIDED/Unable to Authorize at this time";
            break;
		case "900N5":
            echo "MUST BALANCE NOW/Unable to Authorize at this time";
            break;
		case "900N6":
            echo "USE DUP THEN BALANCE NOW/Unable to Authorize at this time";
            break;
		case "900N7":
            echo "NO DUPE FOUND/Unable to Authorize at this time";
            break;
		case "900N8":
            echo "INVALID DATA/Unable to Authorize at this time";
            break;
		case "900NA":
            echo "NO TRANS FOUND/Unable to Authorize at this time";
            break;
		case "900NC":
            echo "AP NOT CAPTURED/Unable to Authorize at this time";
            break;
		case "900NE":
            echo "AP AUTH ONLY/Unable to Authorize at this time";
            break;
		case "900NF":
            echo "INV BANK/Unable to Authorize at this time";
            break;
		case "900P0":
            echo "TRAN TYPE INVLD/Unable to Authorize at this time";
            break;
		case "900P1":
            echo "AP/DB Approved (Debit Card Purchase or Return)";
            break;
		case "900P2":
            echo "DB UNAVAIL 02/Unable to Authorize at this time";
            break;
		case "900P3":
            echo "DB UNAVAIL 03/Unable to Authorize at this time";
            break;
		case "900P4":
            echo "DB UNAVAIL 04/Unable to Authorize at this time";
            break;
		case "900P5":
            echo "UNAUTH USER/Unable to Authorize at this time";
            break;
		case "900P6":
            echo "INVALID CARD/Unable to Authorize at this time";
            break;
		case "900P7":
            echo "DB ISSUER UNAVAIL/Unable to Authorize at this time";
            break;
		case "900P8":
            echo "INVALID POS CARD/Unable to Authorize at this time";
            break;
		case "900P9":
            echo "ACCT TYPE INVLD/Unable to Authorize at this time";
            break;
		case "900PA":
            echo "INVALID PREFIX/Unable to Authorize at this time";
            break;
		case "900PB":
            echo "INVALID FIID/Unable to Authorize at this time";
            break;
		case "900S0":
            echo "VERIFY/Unable to Authorize at this time";
            break;
		case "900S1":
            echo "INVALID LIC/Unable to Authorize at this time";
            break;
		case "900S2":
            echo "INVALID STATE CD/Unable to Authorize at this time";
            break;
		case "900T1":
            echo "EDC UNAVAILABLE/Unable to Authorize at this time";
            break;
		case "900T2":
            echo "DB UNAVAIL 01/Unable to Authorize at this time";
            break;
		case "900T3":
            echo "SCAN UNAVAILABLE/Unable to Authorize at this time";
            break;
		case "90121":
            echo "EXCEEDS MAX AMT/Unable to Authorize at this time";
            break;
		case "98":
            echo "Validation Error";
            break;
		case "98000":
            echo "Card Not Supported";
            break;
		case "98001":
            echo "Bad Card Entry";
            break;
		case "98004":
            echo "Bad Expiry";
            break;
		case "98005":
            echo "Declined";
            break;
		case "98058":
            echo "System Error";
            break;
		case "98094":
            echo "Bad Card Entry";
            break;
		case "98e02":
            echo "Validation < Min";
            break;
		case "98e03":
            echo "Field Validation Error-Field Too long";
            break;
		case "98e04":
            echo "Validation- Bad Card Number";
            break;
		case "98e05":
            echo "Invalid Card (Mod 10)";
            break;
		case "98e06":
            echo "Invalid Card Number";
            break;
		case "98e07":
            echo "Invalid Expiry";
            break;
		case "98e08":
            echo "Timer Exceeded";
            break;
		case "98e09":
            echo "Bad Logical Merchant";
            break;
		case "98e10":
            echo "Security- LMN Not available";
            break;
		case "98e11":
            echo "Com Port Open Error";
            break;
		case "98e12":
            echo "Max Com Retries";
            break;
		case "98e13":
            echo "Open File Error";
            break;
		case "98e14":
            echo "Operator or Remote Abort";
            break;
		case "98e16":
            echo "Card velocity error";
            break;
		case "98e23":
            echo "Amount < Min";
            break;
		case "98e24":
            echo "Amount > Max";
            break;
		case "98e99":
            echo "Declined";
            break;
		case "98ZZZ":
            echo "Unexpected error";
            break;
		case "99005":
            echo "System Error";
            break;
		case "99821":
            echo "System Error";
            break;
		case "99i18":
            echo "System Error - RETRY/Please Retry";
            break;
		case "99s03":
            echo "System Error - Max Retry Count";
            break;
		case "99s05":
            echo "ERROR Max Com Retries";
            break;
		case "1083":
            echo "Invalid card number";
            break;
		default:
			echo "Error en el pago con tarjeta de cr&eacute;dito. Por favor comun&iacute;quese con el administrador y entr&eacute;guele este c&oacute;digo: $error";
    }
}
?>