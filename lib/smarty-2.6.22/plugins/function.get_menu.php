<?php
/*****************************************
* Menu
* carga el menu
*****************************************/

function smarty_function_get_menu($params, &$smarty)
{
	if (empty($params['menu'])) {
        $smarty->trigger_error("menu: missing 'menu' parameter");
        return;
    } else {
        $menu = $params['menu'];
    }
	
	
		$out .= "\n".'<div class="myMenu span-24" align="right" >
                        <div class="rootVoices span-24">' . "\n";
		
		for ( $i = 0; $i < count ( $menu ); $i++ )
		{
			if ( is_array ( $menu [ $i ] ) ) {
				if ( $menu [ $i ] [ 'show_condition_opt' ] && $menu [ $i ] [ 'opt_serial_opt' ] == 0 ) {//are we allowed to see this menu?
					$out .= ' <div class="rootVoice span-3button menuItem" ';
                                        
                                        $child = get_childs ( $menu, $menu[$i] ['serial_opt']  );
                                        $subout .=    $child;
                                        if($child != FALSE){
                                            $out .= ' menu="menu_'.$menu[$i] ['serial_opt'].'"';
                                        }
                                        $out .='>';
                                        $out .= utf8_encode($menu [ $i ] [ 'name_obl' ]);
					$out .= '</div>' . "\n";
				}
			}
			else {
				die ( sprintf ( 'menu nr %s must be an array', $i ) );
			}
		}
		
		$out .= '</div>
                        </div>'."\n";
		return $out . "\n\t" . $subout;
		
	
}
function get_childs ( $menu, $el_id )
	{
		$has_subcats = FALSE;
		$out = '';
		$out .= "\n".'<div id="menu_'.$el_id.'" class="mbmenu">' . "\n";
		for ( $i = 0; $i <= count ( $menu ); $i++ )
		{
			if ( $menu [ $i ] [ 'show_condition_opt' ] && $menu [ $i ] [ 'opt_serial_opt' ] == $el_id ) {//are we allowed to see this menu?
				$has_subcats = TRUE;

				$out .= '<a ';
				if($menu [ $i ] [ 'link_opt' ]!="#"){
					$out .= 'class="menu_reference" href="' .DOCUMENT_ROOT_LOGIC. $menu [ $i ] [ 'link_opt' ] . '" ';
				}

				$child = get_childs ( $menu, $menu[$i] ['serial_opt'] );
				$subout .= $child;
				if($child != FALSE){
					$out .= ' menu="menu_'.$menu[$i] ['serial_opt'].'" ';
				}

				$out .= '>'.utf8_encode($menu [ $i ] [ 'name_obl' ]).'</a>';
			}
		}
		$out .= '	</div>'."\n";
		return ( $has_subcats ) ? ($out.' '.$subout) : FALSE;
	}

        

?>
	
