<?php
/*
 * Document   : smarty_function_upload_file
 * Created on : Apr 19, 2010, 3:17:56 PM
 * Author     : H3Dur4k
 * Last Modified: May 25, 2010, 8:56:24 AM
 * Modified By: David Bergmann
 * Description:
 * Creates an interface to upload a file
 * works only for assistance
 * Parameters:
 *      serial_fle : name of the table where the remote_id is from.
*/
function smarty_function_upload_file($params, &$smarty){
	$serial_fle=$params['serial_fle'];
	if(!$serial_fle)
		die('Insufficient arguments');
 $smarty->assign('container','none');
 $smarty->assign('serial_fle',$serial_fle);
 $smarty->display('helpers/functions/upload_file.'.$_SESSION['language'].'.tpl');
}
?>
