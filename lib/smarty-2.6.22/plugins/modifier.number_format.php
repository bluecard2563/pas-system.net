<?php
/*
    Document   : modifier.number_format
    Created on : 08-jun-2010, 17:11:00
    Author     : Nicolas
    Description:
       gives number format .
*/
function smarty_modifier_number_format($string,$decimals)
{
    return number_format($string, $decimals, '.', '');
}
?>
