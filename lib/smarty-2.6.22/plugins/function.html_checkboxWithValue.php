<?php
/**
 * File: function
 * Author: Patricio Astudillo
 * Creation Date: 19-feb-2013
 * Last Modified: 19-feb-2013
 * Modified By: Patricio Astudillo
 */

function smarty_function_html_checkboxWithValue($params, &$smarty){
	$checkBoxString = '<input type="checkbox" name="'.$params['input_name'].'[]" id="'.$params['input_name'].''.$params['serial_cou'].'" value="'.$params['serial_cou'].'"';
	
	if($params['preLoadedDestinations']){
		if(in_array($params['serial_cou'], $params['preLoadedDestinations'])){
			$checkBoxString .= ' checked=""';
		}
	}
	
	echo $checkBoxString .' />';
}
?>
