<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

function smarty_modifier_trim($string)
{
    return trim($string);
}

/* vim: set expandtab: */

?>
