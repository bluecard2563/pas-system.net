<?
function smarty_function_serializar_arreglos($params, &$smarty){
	
	if (empty($params['titulos'])) {
        $smarty->trigger_error("menu: missing parameter");
        return;
    } else {
        $arregloTitulos = $params['titulos'];
    }
	
	if (empty($params['historialTratamientos'])) {
        $smarty->trigger_error("menu: missing parameter");
        return;
    } else {
        $arregloHistorial = $params['historialTratamientos'];
    }
	
         
	echo "<input type='hidden' name='serialTitulos' id='serialTitulos' value='".serialize($arregloTitulos)."'/><input type='hidden' name='serialHistorial' id='serialHistorial' value='".serialize($arregloHistorial)."'/>" ;

}
?>