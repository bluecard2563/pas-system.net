<?php
function smarty_function_generate_table_check($params, &$smarty){
    $data = $params['data'];
	$titles = $params['titles'];
	$fields = $params['fields'];
	$serial = $params['serial'];
	$text = $params['text'];
	//Debug::print_r($fields);

	if(is_array($data)){
		if(!$titles){
			$titles=array();
			array_push($titles,'#');	
			foreach($data[0] as $k2=>$i2){
				if($k2!='serial'){
					array_push($titles,$k2);	
				}
			}
		}
		
		$count=0;
		$maxj=sizeof($data[0]);
		$table='<center>';
		$table.='<table border="0" class="paginate-1 max-pages-7" id="theTable">';
		$table.='<THEAD>';
		$table.='<tr bgcolor="#284787">';

			$table.='<td align="center" style="padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center;">';
			$table.='<font color=#ffffff>';
			$table.='<input type="checkbox" id="checkAll"/>';
			$table.='</font>';
			$table.='</td>';

		for($j=0;$j<sizeof($titles);$j++){
			$table.='<td align="center" style="padding:0 5px 0 5px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center;">';
			$table.='<font color=#ffffff>';
			$table.=$titles[$j];	
			$table.='</font>';
			$table.='</td>';
		}
	
		$table.='</tr>';
		$table.='</THEAD><TBODY>';
		
		foreach($data as $key=>$info){
				$table.='<tr "';
				if($cont%2==0)
					$table.=' bgcolor="#E8F2FB">';
				else
					$table.=' bgcolor="#D7E8F9">';


				$table.='<td style="padding:5px 5px 5px 5px; text-align:left;';
				if($cont%2==0)
					$table.=' color:#f;">';
				else
					$table.=' color:#000000;">';
				$table.='<input type="checkbox" name="'.$serial.'[]" value="'.$info[$serial].'"/>';
				$table.='</td>';


				$table.='<td style="padding:5px 5px 5px 5px; text-align:left;';
				if($cont%2==0)
					$table.=' color:#000000;">';
				else
					$table.=' color:#000000;">';
				$table.=++$cont;
				$table.='</td>';



				foreach($info as $key2=>$info2){
					if(in_array($key2,$fields)){
						$table.='<td id="'.$key2.'_'.$serial.'" style="padding:5px 5px 5px 5px; text-align:center;';
						if($cont%2==0)
							$table.=' color:#000000;">';
						else
							$table.=' color:#000000;">';

						if($key2=='update'){
							$table.='<img src="'.DOCUMENT_ROOT_LOGIC.'img/pencil.png" border="0" id="'.$info2.'" class="image" width="25px" height="25px" style="cursor:pointer" />';
						}elseif($key2=='delete'){
							$table.='<img src="'.DOCUMENT_ROOT_LOGIC.'img/inactive.gif" border="0" id="'.$info2.'" class="delete" width="25px" height="25px" style="cursor:pointer" />';
						}elseif($key2=='status'){
												if($info2=='ACTIVE'){
													$table.=GLOBAL_STATUS_ACTIVE;
												}elseif($info2=='INACTIVE'){
													$table.=GLOBAL_STATUS_INACTIVE;
												}
						}elseif($key2=='language'){
							$table.=($info2);
						}
						else{
							$table.=(utf8_encode($info2));
						}
						$table.='</td>';
					}
				}

				$table.='</tr>';
			}
		
		$table.='</TBODY>';
		$table.='</table>';
		$table.='</center>';	


		$smarty->assign('table',$table);
		$smarty->assign('serial',$serial);
		$smarty->assign('text',$text);
	}
	$smarty->assign('container','none');
	$smarty->display('helpers/functions/generate_table_check.'.$_SESSION['language'].'.tpl');
}
?>