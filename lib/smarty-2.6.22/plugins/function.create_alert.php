<?php
/*
    Document   : smarty_function_create_alert
    Created on : Apr 19, 2010, 3:17:56 PM
    Author     : H3Dur4k
    Description:
    Creates an interface to insert an alert.
 * It works with a form, so it should not be used as part of another form.
 * Parameters:
 *	remote_id: id of the register wich will have an alert.
 * table_name : name of the table where the remote_id is from.
*/
function smarty_function_create_alert($params, &$smarty){
	$remote_id = $params['remote_id'];
	$table_name = $params['table_name'];
 	if($params['type']){
		$type = strtoupper($params['type']);
 		if(!Alert::isValidAlertType($db, $type)){
			die('insuficient arguments');
		}
 	}else{
		$type='FILE';
 	}

 	if($type=='FILE'){
		$serial_fle=$params['serial_fle'];
 	}
	if(!$params['serial_usr'])
		$serial_usr=$_SESSION['serial_usr'];
 	else
		$serial_usr=$params['serial_usr'];

 $today=date('d/m/Y');
 $array_hours=array('01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24');
 $array_minutes=array('00','05','10','15','20','25','30','35','40','45','50','55');
 $smarty->assign('container','none');
 $smarty->assign('array_hours',$array_hours);
 $smarty->assign('array_minutes',$array_minutes);
 $smarty->assign('remote_id',$remote_id);
 $smarty->assign('table_name',$table_name);
 $smarty->assign('serial_usr',$serial_usr);
 $smarty->assign('type',$type);
 $smarty->assign('today',$today);
  $smarty->assign('serial_fle',$serial_fle);
 $smarty->display('helpers/functions/create_alert.'.$_SESSION['language'].'.tpl');
}
?>