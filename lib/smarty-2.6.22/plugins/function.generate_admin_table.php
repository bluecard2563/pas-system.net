<?php
function smarty_function_generate_admin_table($params, &$smarty){
    $data = $params['data'];
	$titles = $params['titles'];
	$text=$params['text'];

	
	if(is_array($data)){
		if(!$titles){
			$titles=array();
			array_push($titles,'#');	
			foreach($data[0] as $k2=>$i2){
				if($k2!='serial'){
					array_push($titles,$k2);	
				}
			}
		}
		
		$count=0;
		$maxj=sizeof($data[0]);
		$table='<center>';
		$table.='<table border="0" class="paginate-1 max-pages-7 admin_table" id="theTable">';
		$table.='<THEAD>';
		$table.='<tr>';
	
		for($j=0;$j<sizeof($titles);$j++){
			$table.='<td align="center" class="tableTitles" >';
			$table.=$titles[$j];	
			$table.='</td>';
		}
	
		$table.='</tr>';
		$table.='</THEAD><TBODY>';
		
		foreach($data as $key=>$info){
			$table.='<tr class="';
			if($cont%2==0)
				$table.=' even-row">';
			else
				$table.=' even-odd">';
			
			$table.='<td class="';
			if($cont%2==0)
				$table.=' even-row">';
			else
				$table.=' odd-row">';
			$table.=++$cont;
			$table.='</td>';	

			foreach($info as $key2=>$info2){
				if($key2=='serial'){
					$serial=$info2;
				}else if($key2=='serial_lang' || $key2=='serial_cus' || $key2=='serial_cus_xls'  || $key2=='cus_serial_cus_xls'  || $key2=='start_trl' || $key2=='end_trl'  || $key2=='birthdate_cus' ||$key2=='relationship_ext' ||$key2=='extras' ){
                    $table.='<input type="hidden" id="'.$key2.'_'.$serial.'" value="'.$info2.'">';
                }else{
					$table.='<td id="'.$key2.'_'.$serial.'" ';

					if($cont%2==0)
						$table.=' >';
					else
						$table.=' >';
						
					if($key2=='update'){
						$table.='<img src="'.DOCUMENT_ROOT_LOGIC.'img/pencil.png" border="0" id="'.$info2.'" class="image" width="25px" height="25px" style="cursor:pointer" />';
					}elseif($key2=='weight_ben'){
                                                $table.=($info2);
                                        }elseif($key2=='delete'){
						$table.='<img src="'.DOCUMENT_ROOT_LOGIC.'img/inactive.gif" border="0" id="'.$info2.'" class="delete" width="25px" height="25px" style="cursor:pointer" />';
					}elseif($key2=='status'){
                                            if($info2=='ACTIVE'){
                                                $table.=GLOBAL_STATUS_ACTIVE;
                                            }elseif($info2=='INACTIVE'){
                                                $table.=GLOBAL_STATUS_INACTIVE;
                                            }
					}elseif($key2=='language'){
						$table.=($info2);
					}
					else{
						$table.=($info2);
					}
					$table.='</td>';
				}
			}
			
			$table.='</tr>';
		}
		$table.='</TBODY>';
		$table.='</table>';
		$table.='</center>';	
		
		$smarty->assign('table',$table);
		$smarty->assign('text',$text);
	}
	
	$smarty->display('helpers/functions/generate_admin_table.'.$_SESSION['language'].'.tpl');
}
?>