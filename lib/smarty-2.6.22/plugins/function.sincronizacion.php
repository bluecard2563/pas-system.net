<?php
/*****************************************
* Sincronizacion
* realiza el preoceso de sincronización.
* Extrae los datos de la base los escribe en arichivos
* sepearados por comas, los comprime y los sube al servidor.
*****************************************/

function smarty_function_sincronizacion($params, &$smarty)
{
	ini_set('max_execution_time',1000);
	ini_set('memory_limit','256M');
	include('adodb5/adodb.inc.php'); # load code common to ADOdb 
	$db = &ADONewConnection('mysql'); # create a connection
	$db -> PConnect('localhost','root','','dermacareprueba'); # connect to mysql, dermacareprueba DSN
	
	///DIRECTORIES VARS
	$base_local_dir="C:\\xampp\\htdocs\\DERMACARE\\modulos\\sincronizacion\\archivos_locales\\";
	$base_remote_dir='/public_html/modulos/sincronizacion/';
	
	include('lib.inc.php');
	
	///////////////////////////////////////////////////////////
	///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!///
	///IMPORTANTE, NO olvidar cambiar el valor del php.ini
	/// a por lo menos 256M
	///		memory_limit = 256M
	//////////////////////////////////////////////////////////
	//		GRANT FILE ON *.* TO 'atpucec_sync'@'localhost';
	//		GRANT FILE ON *.* TO `atpucec\_atpuce`.* TO 'atpucec_sync'@'localhost';
	
	
	//////////////////////////////////////////////////////////////////////////////////////
	/// En esta parte primera, se bajan los datos de los Socios cambiados
	/// a la base de ACCESS local!!!!!
	//////////////////////////////////////////////////////////////////////////////////////
	
	///Parámetros de la base LOCAL
	$gHost="localhost";
	$gUsuarioBD="root";
	$gPasswordBD="";
	$gBaseDatos="dermacareprueba";
	$dbRemote = NewADOConnection('mysql');
	if(!$dbRemote->pConnect($gHost,$gUsuarioBD,$gPasswordBD,$gBaseDatos)){
		die('Error en la conexión con la base de datos remota, Contacte a su Administrador');
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	/// TABLAS DE DERMACARE
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	$numTablas = sizeof($tablas);
	
	//Bucle que trabaja tabla por tabla
	for($k=0; $k<$numTablas; $k++){
		
		$fh = fopen($base_local_dir.$tablas[$k].'.sql','w');
		$recordSet = &$db->Execute('select * from '.$tablas[$k]);
		$numCol = $recordSet->FieldCount();
		
		$numRow = $recordSet->RecordCount();
		
		for($i=0; $i<$numRow; $i++){
			$sqlExp1.= '';
			
			for($j=0; $j<$numCol; $j++){
				
				$string = $recordSet->fields[$j];
				
				$string = str_replace("\n", "", $string);
				$string = str_replace("\r", "", $string);
				
				if($string==''){
					$string = 'NULL';
				}
	
				$sqlExp1.= "'".$string."'";
				
				if($j==$numCol-1){
					$sqlExp1.= ',
	';
				}
				else{
					$sqlExp1.= ',';
				}
			}
		
			if($i==$numRow-1){
	//			$sqlExp1.= '); ';
			}
			else{
	//			$sqlExp1.= '), ';
	//			$sqlExp1.= ';
	//';
			}
			
			$recordSet->MoveNext();
		}
		
		$finalSql.="
	
	";
		$finalSql = $sqlExp1;
		
		///Escribo los datos al archivo
		fwrite($fh, $finalSql);
		
		$finalSql="";
		$sqlExp1="";
	
		///Cierro el archivo
		fclose($fh);
	
	}//Fin bucle grande
	
	
	///Compresión de archivo a .GZ
	for($i=0; $i<$numTablas; $i++){
		gzip($base_local_dir.$tablas[$i].'.sql');
	}
	
	$getStr='?';
	
	///Saco los tamaños de archivos originales
	for($i=0; $i<$numTablas; $i++){
		if(filesize($base_local_dir.$tablas[$i].'.sql')>0)
			$getStr.='t'.$i.'='.filesize($base_local_dir.$tablas[$i].'.sql');
		else
			$getStr.='t'.$i.'=1';
		
		if($i==$numTablas-1){
			$getStr.= '';
		}
		else{
			$getStr.= '&';
		}
		
	}
	
	include_once('ftp.inc.php');
	
	///SUBO EL ARCHIVO AL SERVIDOR
	///Subo los archivos
	for($i=0; $i<$numTablas; $i++){
		SubirArchivo($base_local_dir.$tablas[$i].'.sql.gz',$base_remote_dir.$tablas[$i].'.sql.gz');
	}
	
	
	///Cerrar la conexión a la base
	$db->Close(); # optional
	
	echo '<script type="text/javascript">window.top.location="http://www.sistemadermacare.com/modulos/sincronizacion/script'.$getStr.'";</script>';	
	}
?>
	