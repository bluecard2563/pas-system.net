<?
function smarty_function_generar_tablas($params, &$smarty){
	
	if (empty($params['list'])) {
        $smarty->trigger_error("menu: missing 'list' parameter");
        return;
    } else {
        $list = $params['list'];
    }
	
	if (!empty($params['colSize'])) {
        $colSize = $params['colSize'];
    }
	
	if (!empty($params['titles'])) {
        $titles	 = $params['titles'];
    }

	
	if (empty($params['new'])) {   //creates a "new" button, this parameter must be the name of the "new" dialog 
        $smarty->trigger_error("menu: missing 'new' parameter");
        return;
    } else {
        $new = $params['new'];
    }
	
	
	

	$max=sizeof($list);

	
//	echo '<button id="create-user" class="ui-button ui-state-default ui-corner-all">Create new user</button>';
	echo '<ul id="create-user" class="ui-widget ui-helper-clearfix" style="margin:0; padding:0;">
		<li class="ui-state-default ui-corner-all" title=".ui-icon-plusthick" style="cursor:pointer;
float:left;
list-style-image:none;
list-style-position:outside;
list-style-type:none;
margin:4px;
padding:4px 4px;
position:relative;">
			<span class="ui-icon ui-icon-plusthick"/>
		</li>
	</ul>';
	$tabla.='<table border="0" class="paginate-10 max-pages-7" id="theTable" style=" width:0">';
	
	$tabla.='<THEAD>';
	$tabla.='<tr bgcolor="#0099CC">';
	
	$count=0;
	$countTitles=0;
	foreach($list[0] as $key => $element){
		$tabla.='<td align="center" style="padding:0 10px 0 10px; font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-style: normal; font-weight: bold; text-transform:uppercase; text-align:center;" ';
		if(isset($colSize)){
			$tabla.='width="'.$colSize[$count].'" ';	
			$count++;
		}
		$tabla.='>';
		$tabla.='<font color=#FFFFFF>';
       	if(isset($titles)){
			$tabla.=$titles[$countTitles];	
			$countTitles++;
		}
		else{	
			$tabla.=$key;
		}	
		$tabla.='</font>';
		$tabla.='</td>';
	}
	
	
	$tabla.='</tr>';
	$tabla.='</THEAD><TBODY>';

	foreach($list as $key => $element){
		$tabla.='<tr id="'.$serial.'"';
		if($i%2==0)
			$tabla.='>';
		else
			$tabla.=' bgcolor="#b0dbf0">';

		foreach($element as $elmKey => $item){
			$tabla.='<td style="padding:0 10px 0 10px;" class="textoContenido">';
			$tabla.=htmlentities($item);
			$tabla.='</td>';
		}
		$tabla.='</tr>';
	}
	$tabla.='</TBODY>';
	$tabla.='</table>';
	echo $tabla;
}
?>