<?php
class SmartyPlus extends Smarty{
	function display($name = NULL){
		/*************************ARRIBA************************/
		//if (!$name) $name = $_SERVER['SCRIPT_NAME'].'.'.$_SESSION['language'].'.tpl';
		//$css_file = $_SERVER['SCRIPT_NAME'].'.css';
		
		if (!$name){
			if(isset($_SESSION['language'])){
				$name = substr(str_replace("pas-system.net/","",$_SERVER['SCRIPT_NAME']), 1, -3).$_SESSION['language'].'.tpl';
			}else{
				$name = substr(str_replace("pas-system.net/","",$_SERVER['SCRIPT_NAME']), 1, -3).'tpl';					
			}
		}
		$css_file = substr(str_replace("pas-system.net/","",$_SERVER['SCRIPT_NAME']), 1, -3).'css';	
		$js_file = substr(str_replace("pas-system.net/","",$_SERVER['SCRIPT_NAME']), 1, -3).'js';	
		
		//CRIFA
		//if (!$name) $name = str_replace("planetassist/","",$_SERVER['SCRIPT_NAME']).'.tpl';
		//$css_file = str_replace("planetassist/","",$_SERVER['SCRIPT_NAME'])).'css';
						
		if (file_exists(DOCUMENT_ROOT.'css/'.$css_file)) {
			$this->assign('css_file', $css_file );
		}
		if (file_exists(DOCUMENT_ROOT.'js/'.$js_file)) {
			$this->assign('js_file', $js_file );
		}
								
		if($this->_tpl_vars['container']){
			$container=$this->_tpl_vars['container'];
		}else{
			$container='default';
		}

		/* MAKE SURE THE DB CONNECTIONS ARE WORKING AS SUPOSSED TO */
		// PAGE DATA CONNECTION
		global $db;
		global $db_database;
		$page_data = $db->Execute("SELECT * FROM $db_database.banner");

		if ($page_data === false){
			$this->assign('buffer', $this->fetch('templates/connection_error.tpl'));
			parent::display('containers/connection_error.tpl');
			return;
		}
		/* MAKE SURE THE DB CONNECTIONS ARE WORKING AS SUPOSSED TO */
		
		if($container=='none'){
			parent::display('templates/'.$name);
		}else{
			$this->assign('buffer', $this->fetch('templates/'.$name));
			parent::display('containers/'.$container.'.'.$_SESSION['language'].'.tpl');
		}
		//parent::display('templates/'.$name);
	}

	function register($vars){
		foreach (explode(',', $vars) as $var){
			if(isset($GLOBALS[$var])){
				$this->assign($var, $GLOBALS[$var]);
			}
		}
	}

	function fetch_direct($name, $cache_id = null, $compile_id = null, $display = false){
		return parent::fetch($name, $cache_id, $compile_id, $display);
	}
	
	function fetchHelper($filename){
		return parent::fetch("helpers/".$filename);
	}
}
?>
