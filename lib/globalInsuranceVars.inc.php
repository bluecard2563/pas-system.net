<?php
/**
 * Name: globalInsuranceVars
 * Created by: pastudillo
 * Created on: 28-mar-2017
 * Description: 
 * Modified by: pastudillo
 * Modified on: 28-mar-2017
 */

$emissionRightsFee = 10;
$managementFee = 4.50;
$farmersTax = 0.5/100;
$isdTaxPercentage = 5/100;
$vatPercentage = 14/100;

$creditCardComissionPercentage = 8/100;
$reinsurancePercentage = 6.14/100;
$paincPercentage = 12/100;
$accidentRatePercentage = 11/100;
$iessPercentage = 12.15/100;

$bonusPercentage = 5/100;
$comissionSalesPercentage = 45/100;
$ubdPercentge = 1.7 / 100;
$operationReserve = 10 / 100;