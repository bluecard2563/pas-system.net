<?php

	/**
	 * @name commitERPInvoicingCustomer
	 * @param type $db
	 * @param type $serial_client 
	 * @return boolean
	 * 
	 *		WS Structure
	 * ------------------------
	 * Code: [AN]. [UNICO]. C + serial_cus / D + serial_dea
	 * Name: [AN]. 
	 * TaxRegType: [NE]. Indica el tipo de inscripci?n del Cliente. Los valores admitidos son:
	 * 	0: Company
	 * 	1: Final Consumer
	 * 	3: Exempt
	 * 	6: International Exempt
	 * TaxRegNr: [AN]. [UNICO]. document number
	 * IDType: [NE]. Indica el tipo de documento. Opciones v?lidas:
	 * 	0: Ninguno
	 * 	1: RUC.
	 * 	2: C�dula.
	 * 	7: Pasaporte.
	 * Phone: [AN]. N?mero de tel�fono principal.
	 * Mobile: [AN]. [OPCIONAL]. N?mero de tel�fono m?vil. 
	 * City: [AN]. 
	 * Address: [AN]. 
	 * GroupCode: [AN]. customer/dealer category ["A", "B", "C", "D" o "9999"]
	 * CustomerClass: [AN]. Persona Natural ["N"], Persona Jurídica ["J"]
	 * Labels: [AN] [OOCIONAL]
	 * Country: [AN]
	 * InvoiceToCode: [AN] [OPCIONAL]
	 */
	function commitERPInvoicingCustomer($db, $serial_client, $serial_sal, $eInvoiceMail = NULL){
		$curl_instance = curl_init();
		curl_setopt($curl_instance, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
		curl_setopt($curl_instance, CURLOPT_USERPWD, URL_USRPSWD);
		curl_setopt($curl_instance, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_instance, CURLOPT_USERAGENT,'curl/7.24.0 (x86_64-apple-darwin12.0) libcurl/7.24.0 OpenSSL/0.9.8r zlib/1.2.5');
		curl_setopt($curl_instance, CURLOPT_POST, 1);
		//curl_setopt($curl_instance, CURLOPT_VERBOSE, TRUE);
		//$handle = fopen(DOCUMENT_ROOT."system_temp_files/web_service_logs.txt", "a+");
		//curl_setopt($curl_instance, CURLOPT_STDERR, $handle);
		
		$prefix = substr($serial_client, 0, 1);
		$entity_id = substr($serial_client, 1, strlen($serial_client));
		
		switch($prefix){
			case 'C':
				$customer = new Customer($db, $entity_id);
				$customer ->getData();
				
				$sending_array['Code'] = 'C'.$customer->getSerial_cus();
				$sending_array['Name'] = utf8_encode($customer->getFirstname_cus().' '.$customer->getLastname_cus());
				$sending_array['TaxRegType'] = ERPConnectionFunctions::retrievePersonType($customer->getDocument_cus());
				$sending_array['TaxRegNr'] = $customer->getDocument_cus();
				$sending_array['IDType'] = ERPConnectionFunctions::retrieveDocumentType($customer->getDocument_cus());
				$sending_array['Phone'] = $customer->getPhone1_cus();
				$sending_array['Mobile'] = $customer->getCellphone_cus();
				$sending_array['City'] = ERPConnectionFunctions::translateCity($db, $customer->getSerial_cit(), $serial_sal);
				$sending_array['Address'] = utf8_encode($customer->getAddress_cus());
				
				$email = ($customer->getEmail_cus() != '')?$customer->getEmail_cus():NULL;
				$sending_array['Email'] = ($eInvoiceMail)?utf8_encode($eInvoiceMail):utf8_encode($email);
				$sending_array['GroupCode'] = '9999';
				$sending_array['CustomerClass'] = ($customer->getType_cus()=='PERSON')?"N":"J";
				
				break;
			case 'D':
				$dealer = new Dealer($db, $entity_id);
				$dealer ->getData();
				$geo_data = $dealer->getAuxData_dea();
				
				$sending_array['Code'] = 'D'.$dealer->getSerial_dea();
				$sending_array['Name'] = utf8_encode($dealer->getName_dea());
				$sending_array['TaxRegType'] = ERPConnectionFunctions::retrievePersonType($dealer->getId_dea());
				$sending_array['TaxRegNr'] = $dealer->getId_dea();
				$sending_array['IDType'] = ERPConnectionFunctions::retrieveDocumentType($dealer->getId_dea());
				$sending_array['Phone'] = $dealer->getPhone1_dea();
				$sending_array['Mobile'] = $dealer->getPhone2_dea();
				$sending_array['City'] = ERPConnectionFunctions::translateCity($db, $geo_data['serial_cit'], $serial_sal);
				$sending_array['Address'] = utf8_encode($dealer->getAddress_dea());
				$sending_array['Email'] = ($eInvoiceMail)?utf8_encode($eInvoiceMail):utf8_encode($dealer->getEmail2_dea());
				$sending_array['GroupCode'] = $dealer->getCategory_dea();
				$sending_array['CustomerClass'] = 'J';
				
				break;
		}
		
		//echo $url; Debug::print_r($sending_array); echo json_encode($sending_array);
		$url = URL_ERP . "api/customer/create-or-update/?format=json&CustomerCode=".$serial_client;
		//echo '<h3>'.$url.'</h3>';
		curl_setopt($curl_instance, CURLOPT_POSTFIELDS, json_encode($sending_array));   //POST Vars
		curl_setopt($curl_instance, CURLOPT_URL, $url);

		$response = curl_exec($curl_instance);
		//Debug::print_r($response); die;
		$curl_response_result = curl_getinfo($curl_instance);
		$oo_ws_response = $curl_response_result['http_code']; //201 success code
		
		if($oo_ws_response != 200){ //ERRORS ON CREATING INVOICE
			ErrorLog::log($db, 'ERP_WS_CLIENT_COMMIT_FAILED', array(	'case' => $prefix, 
																		'id' => $entity_id,
																		'code' => $oo_ws_response,
'payload' => json_encode($sending_array),																		'message' => $response));
			
			return FALSE;
		}else{
			return TRUE;
		}
	}

	/**
	 * @name ERP_logActivity
	 * @global type $db
	 * @param type $replication_class
	 * @param type $sending_id
	 * @param type $action 
	 */
	function ERP_logActivity($replication_class, $sending_id, $action = 'POST', $eInvoiceMail = NULL) {
		global $db;

		if (!function_exists('curl_init'))
			die('CURL No instalado. Por favor comun&iacute;quese con el administrador.');

		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_USERPWD, URL_USRPSWD);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT,'curl/7.24.0 (x86_64-apple-darwin12.0) libcurl/7.24.0 OpenSSL/0.9.8r zlib/1.2.5');
		curl_setopt($ch, CURLOPT_TIMEOUT, 300);
		
		//curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
		//$handle = fopen(DOCUMENT_ROOT."system_temp_files/web_service_logs.txt", "a+");
		//curl_setopt($ch, CURLOPT_STDERR, $handle);
		if($action == 'POST'):
			curl_setopt($ch, CURLOPT_POST, 1);
		else:
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $action);
		endif;
	
		switch ($replication_class) {
			case 'SALES':
				/**** ARRAY FORMAT ****
				 * ['OperationType'] = 1, 2 o 3 (SYSTEM, WEB, PHONE)
				 * ['CustCode'] = Customer/Dealer ERP Code
				 * ['TransDate'] = "2013-01-01T12:00:00.000000-00:00"
				 * ['Office'] = MBC translation
				 * ['Items'] = array('ArtCode' => serial_pxc,
				 *					'ArtName' => name_pbl,
				 *					'SerialNr' => card_number_sal,
				 *					'FromDate' => 2013-01-01T12:00:00.000000-00:00,
				 *					'ToDate' => 2013-01-01T12:00:00.000000-00:00
				 *					)
				*/
				
				if(ERPConnectionFunctions::saleHasERPConnection($db, $sending_id)){
					$url = URL_ERP . "api/salesorder/?format=json";
					$base_data = ERPConnectionFunctions::getSaleDataForOO($db, $sending_id);
					
					if(commitERPInvoicingCustomer($db, $base_data['client_id'], $sending_id, $eInvoiceMail)){
						$sending_array['OperationType'] = ERPConnectionFunctions::translateSaleType($base_data['type_sal']);
						$sending_array['CustCode'] = $base_data['client_id'];
						$sending_array['TransDate'] = ERPConnectionFunctions::formatDateToISO8601($base_data['emission_date_sal']);
						$sending_array['Office'] = ERPConnectionFunctions::getStoreID($base_data['serial_mbc']);
						$sending_array['Computer'] = ERPConnectionFunctions::getDepartmentID($base_data['serial_mbc']);
						$sending_array['SalesMan'] = ERPConnectionFunctions::getResponsibleTranslationForVisitsInOO(utf8_encode($base_data['responsible']));
						$sending_array['Observations'] = $base_data['observations_sal'];
						//$sending_array['Labels'] = "001";
						$sending_array['Items'] = array(
														array(	'ArtCode' => $base_data['serial_pxc'],
																'ArtName' => utf8_encode($base_data['name_pbl']),
																'Name' => utf8_encode($base_data['full_name_cus']),
																'SerialNr' => $base_data['card_number_sal'],
																'FromDate' => ERPConnectionFunctions::formatDateToISO8601($base_data['begin_date_sal']),
																'ToDate' => ERPConnectionFunctions::formatDateToISO8601($base_data['end_date_sal']),
																"Price" => $base_data['total_sal'],
                                                                "Cost" => $base_data['total_cost_sal']
                                                            )
												);
						
						if(ERP_EMISSION_RIGHTS):
							array_push($sending_array['Items'], ERPConnectionFunctions::addEmissionRightsItem($base_data));
						endif;
                        
						curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($sending_array));   //POST Vars
						curl_setopt($ch, CURLOPT_URL, $url);
                        
						$response = curl_exec($ch);
						$curl_response_result = curl_getinfo($ch);
						$oo_ws_response = $curl_response_result['http_code']; //201 success code
						$response = json_decode($response);
						$saleId_on_oo = $response->id;
						curl_close($ch);

						if(!$saleId_on_oo || $oo_ws_response != 201){ //ERRORS ON CREATING INVOICE
							ErrorLog::log($db, 'ERP_WS_SALES_COMMIT_FAILED', array(	'case' => 'SALES', 
																					'id' => $sending_id,
																					'code' => $oo_ws_response,
																					'message' => $response));
							return FALSE;
						}else{
							$sale = new Sales($db, $sending_id);
							$sale->getData();
							$sale->setIdErpSal_sal($saleId_on_oo);
							if(!$sale->update()){
								ErrorLog::log($db, 'ERP_WS_SALES_SETID_FAILED', array(	'case' => 'SALES', 
																						'id' => $sending_id));
								
								return FALSE;
							}
							
							return TRUE;
						}
					}else{
						ErrorLog::log($db, 'ERP_WS_SALES_INIT_FAILED', array(	'case' =>	'SALES', 
																							'id' => $sending_id));
					}
				}
				
				break;
			
			case 'UPDATE_SALES':
				/**** ARRAY FORMAT ****
				 * ['OperationType'] = 1, 2 o 3 (SYSTEM, WEB, PHONE)
				 * ['CustCode'] = Customer/Dealer ERP Code
				 * ['TransDate'] = "2013-01-01T12:00:00.000000-00:00"
				 * ['Office'] = MBC translation
				 * ['Items'] = array('ArtCode' => serial_pxc,
				 *					'ArtName' => name_pbl,
				 *					'SerialNr' => card_number_sal,
				 *					'FromDate' => 2013-01-01T12:00:00.000000-00:00,
				 *					'ToDate' => 2013-01-01T12:00:00.000000-00:00
				 *					)
				*/
				
				if(ERPConnectionFunctions::saleHasERPConnection($db, $sending_id)){
					$base_data = ERPConnectionFunctions::getSaleDataForOO($db, $sending_id);
					$url = URL_ERP . "api/salesorder/{$base_data['id_erp_sal']}/?format=json";
					
					if(commitERPInvoicingCustomer($db, $base_data['client_id'], $sending_id, $eInvoiceMail) && $base_data['id_erp_sal']){
						$sending_array['OperationType'] = ERPConnectionFunctions::translateSaleType($base_data['type_sal']);
						$sending_array['CustCode'] = $base_data['client_id'];
						$sending_array['TransDate'] = ERPConnectionFunctions::formatDateToISO8601($base_data['emission_date_sal']);
						$sending_array['Office'] = ERPConnectionFunctions::getStoreID($base_data['serial_mbc']);
						$sending_array['Computer'] = ERPConnectionFunctions::getDepartmentID($base_data['serial_mbc']);
						$sending_array['SalesMan'] = ERPConnectionFunctions::getResponsibleTranslationForVisitsInOO(utf8_encode($base_data['responsible']));
						$sending_array['Observations'] = $base_data['observations_sal'];
						//$sending_array['Labels'] = "001";
						$sending_array['Items'] = array(
														array(	'ArtCode' => $base_data['serial_pxc'],
																'ArtName' => utf8_encode($base_data['name_pbl']),
																'Name' => utf8_encode($base_data['full_name_cus']),
																'SerialNr' => $base_data['card_number_sal'],
																'FromDate' => ERPConnectionFunctions::formatDateToISO8601($base_data['begin_date_sal']),
																'ToDate' => ERPConnectionFunctions::formatDateToISO8601($base_data['end_date_sal']),
																"Price" => $base_data['total_sal'],
                                                                "Cost" => $base_data['total_cost_sal']
                                                            )
												);
						
						if(ERP_EMISSION_RIGHTS):
							array_push($sending_array['Items'], ERPConnectionFunctions::addEmissionRightsItem($base_data));
						endif;
						
						//echo $url; Debug::print_r($sending_array); echo json_encode($sending_array); die;
						curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($sending_array));   //POST Vars
						curl_setopt($ch, CURLOPT_URL, $url);

						$response = curl_exec($ch);
						$curl_response_result = curl_getinfo($ch);
						$oo_ws_response = $curl_response_result['http_code']; //200 success code
						$response = json_decode($response);
						curl_close($ch);
						
						if(!$saleId_on_oo || $oo_ws_response != 200){ //ERRORS ON CREATING INVOICE
							ErrorLog::log($db, 'ERP_WS_UPDATE_SALES_COMMIT_FAILED', array(	'case' => 'UPDATE_SALES', 
																							'id' => $sending_id,
																							'code' => $oo_ws_response,
																							'message' => $response));
						}
					}else{
						ErrorLog::log($db, 'ERP_WS_UPDATE_SALES_INIT_FAILED', array(	'case' =>	'UPDATE_SALES', 
																						'id' => $base_data));
					}
				}
				
				break;
				
			case 'INVOICE':
				/******** SENDING_ID IS AN ARRAY FOR THIS CASE - ARRAY FORMAT ****
				 * discount -> percentage
				 * other_discount -> percentage
				 * holder_id -> serial_cus / serial_dea
				 * holder_type -> D/C
				 * sales_ids -> array
				 * 
				 ************ PAYLOAD FORMAT **********
				 * [PayTerm]: Los valores posibles son: 1, 4, 5, 6, 7, 8, 9, 10, 19
				 * [Discount1]: [N]. [OPCIONAL]
				 * [Discount2]: [N]. [OPCIONAL]
				*/

				$serialSalesString = implode(',', $sending_id['sales_ids']);			   
				$sales_order_url = implode('&salesOrder=', $sending_id['sales_ids']);
				$url = URL_ERP . "api/salesorder/generate-invoice?format=json&salesOrder=".$sales_order_url;
				//echo $url.'<br>';
				
				$custom_discount = $sending_id['discount'] + $sending_id['other_discount'];
				$total_sold = ERPConnectionFunctions::getFuturePayTotalAmountForSales($db, $serialSalesString, ERP_EMISSION_RIGHTS, ERP_FARMERS_TAX, FALSE, $custom_discount);
				$base_data = ERPConnectionFunctions::getInvoiceDataForOO($db, $sending_id['holder_id'], $sending_id['holder_type']);
				
				if(commitERPInvoicingCustomer($db, $base_data['client_id'], $sending_id['sales_ids'][0], $eInvoiceMail) && $total_sold){
					$sending_array['CustCode'] = $base_data['client_id'];
					$sending_array['PayTerm'] = $base_data['payterm'];
					$sending_array['Discount1'] = $sending_id['discount'];
					$sending_array['Discount2'] = $sending_id['other_discount'];
					$sending_array['OperationType'] = (string)$sending_id['operation_type'];
					$sending_array['Labels'] = ERPConnectionFunctions::getLabelTranslation(ERPConnectionFunctions::getMbcIdForSalesToERP($db, $serialSalesString));
					$sending_array['FuturePayments'] = array(
						array(	'PayModeCode' => ERPConnectionFunctions::getFuturePaymentCodeForSales($db, $serialSalesString),
								'Amount' => $total_sold,
								'Period' => '1',
								'TimeUnit' => 'dias'
							)
					);
					
					//echo json_encode($sending_array); die;
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($sending_array));   //POST Vars
					curl_setopt($ch, CURLOPT_URL, $url);
					
					$response = curl_exec($ch);
					$curl_response_result = curl_getinfo($ch);
					$oo_ws_response = $curl_response_result['http_code']; //200 success code
					$response = json_decode($response);
					curl_close($ch);
					
					$invoice_number = $response->SerNr;
					$invoice_number = (int)substr($invoice_number, 4, strlen($invoice_number-4));
					$erp_id = $response->id;
					
					if($oo_ws_response != 200){ //ERRORS ON CREATING INVOICE
						ErrorLog::log($db, 'ERP_WS_INVOICE_COMMIT_FAILED', array(	'case' => 'INVOICE', 
																						'id' => $sending_id,
																						'code' => $oo_ws_response,
																						'message' => $response));
						return FALSE;
					}
					
					return array('number_inv' => $invoice_number, 'erp_id' => $erp_id);
				}else{
					ErrorLog::log($db, 'ERP_WS_INVOICE_INIT_FAILED', array(	'case' =>	'INVOICE', 
																						'id' => $sending_id));
				}
				
				return FALSE;
				break;
				
				
			case 'CREDIT_NOTE':
				/******** PAYLOAD FORMAT **********
				 * [Penality]: decimal value
				 */
			   
				$url = URL_ERP . "api/invoice/{$sending_id['erp_invoice_id']}/generate-creditnote?format=json";
				$sending_array['Penality'] = $sending_id['penalty_fee'];
				$sending_array['IncludeItems'] = $sending_id['IncludeItems'];
				
				if($sending_id['salesToBeOpened']):
					$sending_array['FreeSalesOrdersSet'] = $sending_id['salesToBeOpened'];
				endif;
				
				//echo $url; Debug::print_r($sending_array); echo json_encode($sending_array); die;
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($sending_array));   //POST Vars
				curl_setopt($ch, CURLOPT_URL, $url);

				$response = curl_exec($ch);
				$curl_response_result = curl_getinfo($ch);
				$oo_ws_response = $curl_response_result['http_code']; //200 success code
				$response = json_decode($response);
				curl_close($ch);
				$cn_number = $response->SerNr;
				$cn_number = (int)substr($cn_number, 5, strlen($cn_number-5)); //5 CHARS DUE TO 9 DIGIT
				$erp_response['cnote_number'] = $cn_number;
				
				if($oo_ws_response != 200){ //ERRORS ON CREATING INVOICE
					ErrorLog::log($db, 'ERP_WS_CNOTE_COMMIT_FAILED', array(	'case' => 'CREDIT_NOTE', 
																			'information' => $sending_array,
																			'code' => $oo_ws_response,
																			'message' => $response));
					
					$_SESSION['erp_error_message'] = $response->ErrorResponse->errorMessage;
					
					return FALSE;
				}
				
				return $erp_response;
				break;
				
			case 'REFUND':
				/******** PAYLOAD FORMAT **********
				 * [Penality]: decimal value
				 */
			   
				$url = URL_ERP . "api/invoice/{$sending_id['erp_invoice_id']}/generate-creditnote?format=json";
				
				$sending_array['Penality'] = $sending_id['penalty_fee'];
				$sending_array['CreditNoteType'] = 1;	//ALWAYS SET AS MONEY REFUND
				$sending_array['InvoiceReturnPayMode'] = 'CASH';	//ALWAYS AS CASH
				$sending_array['IncludeItems'] = $sending_id['IncludeItems'];

				//echo $url; Debug::print_r($sending_array); echo json_encode($sending_array); die;
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($sending_array));   //POST Vars
				curl_setopt($ch, CURLOPT_URL, $url);

				$response = curl_exec($ch);
				$curl_response_result = curl_getinfo($ch);
				$oo_ws_response = $curl_response_result['http_code']; //200 success code
				$response = json_decode($response);
				curl_close($ch);
				$cn_number = $response->SerNr;
				$cn_number = (int)substr($cn_number, 5, strlen($cn_number-5)); //5 CHARS DUE TO 9 DIGIT
				$erp_response['cnote_number'] = $cn_number;
				
				if($oo_ws_response != 200){ //ERRORS ON CREATING INVOICE
					ErrorLog::log($db, 'ERP_WS_CNOTE_COMMIT_FAILED', array(	'case' => 'CREDIT_NOTE ON INVOICE OO_ID'.$sending_id['erp_invoice_id'], 
																			'information' => $sending_array,
																			'code' => $oo_ws_response,
																			'message' => $response));
					
					$_SESSION['erp_error_message'] = $response->ErrorResponse->errorMessage;
					
					return FALSE;
				}
				
				return $erp_response;
				break;
		}//end switch
	}
	
	function commitERPCustomer($db, $serial_cus, $eInvoiceMail = NULL){
		$curl_instance = curl_init();
		curl_setopt($curl_instance, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
		curl_setopt($curl_instance, CURLOPT_USERPWD, URL_USRPSWD);
		curl_setopt($curl_instance, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_instance, CURLOPT_USERAGENT,'curl/7.24.0 (x86_64-apple-darwin12.0) libcurl/7.24.0 OpenSSL/0.9.8r zlib/1.2.5');
		curl_setopt($curl_instance, CURLOPT_POST, 1);
		
		$customer = new Customer($db, $serial_cus);
		if($customer ->getData()){
			$sending_array['Code'] = 'C'.$customer->getSerial_cus();
			$sending_array['Name'] = utf8_encode($customer->getFirstname_cus().' '.$customer->getLastname_cus());
			$sending_array['TaxRegType'] = ERPConnectionFunctions::retrievePersonType($customer->getDocument_cus());
			$sending_array['TaxRegNr'] = $customer->getDocument_cus();
			$sending_array['IDType'] = ERPConnectionFunctions::retrieveDocumentType($customer->getDocument_cus());
			$sending_array['Phone'] = $customer->getPhone1_cus();
			$sending_array['Mobile'] = $customer->getCellphone_cus();
			$sending_array['City'] = ERPConnectionFunctions::translateCity($db, $customer->getSerial_cit());
			$sending_array['Address'] = utf8_encode($customer->getAddress_cus());
			$sending_array['Email'] = ($eInvoiceMail)?utf8_encode($eInvoiceMail):utf8_encode($customer->getEmail_cus());
			$sending_array['GroupCode'] = '9999';
			$sending_array['CustomerClass'] = ($customer->getType_cus()=='PERSON')?"N":"J";

			//echo $url; Debug::print_r($sending_array); echo json_encode($sending_array); die;
			$url = URL_ERP . "api/customer/create-or-update/?format=json&CustomerCode=".'C'.$customer->getSerial_cus();
			curl_setopt($curl_instance, CURLOPT_POSTFIELDS, json_encode($sending_array));   //POST Vars
			curl_setopt($curl_instance, CURLOPT_URL, $url);

			$response = curl_exec($curl_instance);
			$curl_response_result = curl_getinfo($curl_instance);
			$oo_ws_response = $curl_response_result['http_code']; //201 success code

			if($oo_ws_response != 200){ //ERRORS ON CREATING INVOICE
				ErrorLog::log($db, 'ERP_WS_CUSTOMER_COMMIT_FAILED', array(	'case' => 'CUSTOMER DIRECT REPLICATION', 
																			'id' => $serial_cus,
																			'code' => $oo_ws_response,
																			'message' => $response));

				
			}
		}
	}
?>
