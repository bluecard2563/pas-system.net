<?php

	session_start();

	require_once('db.inc.php');

	require_once('Request.class.php');

	require_once('utilidades.inc.php');

	Request::initialize();



	function __autoload($class) {

		//require_once('clases/'.str_replace("_", "/", $class).".class.php");

		require_once($class . ".class.php");

	}



	//*********************** GENERAL PARAMETERS ************************

	define('HOST', 'localhost');

	define('FROM_MAIL', 'website@bluecardassistance.info');

	define('USUARIO_MAIL', 'website@bluecardassistance.info');

    //define('PSWD_MAIL', 'BCpass2008!');
	define('PSWD_MAIL', 'BCecuador2022');

	define('PASWEB_URL', 'https://www.planet-assist.net');

	define('BLUE_URL', 'https://www.bluecard.com.ec');

	//*************CREDENTIALS PLACETOPAY**************
    //*****DEVELOP*****
	// define('TRANKEY','1KwOcMNbX0r7np17');
    // define('LOGIN','0d48f5702980f31c9fa830eaac53bcc6');
    // define('URL_SWITCH','http://54.159.232.149/');
    // define('URL_PTP','https://test.placetopay.ec/redirection/');

	//****PRODUCTION****
	define('TRANKEY','Ox3zo3Ls46LFVgpA');
	define('LOGIN','15b9673b8826987605fbd6e18947195b');
//  define('URL_SWITCH','http://52.87.250.190/');
//  define('URL_PTP','https://secure.placetopay.ec/redirection/');




	//********************* URL PARAMETERS DEVELOPING *********************

	//define('DOCUMENT_ROOT_LOGIC', '/PLANETASSIST/');

	//define('URL', 'http://localhost/PLANETASSIST/');

	//define('DOCUMENT_ROOT', '/Library/WebServer/Documents/PLANETASSIST/');

	//define('DOCUMENT_ROOT', '/opt/lampp/htdocs/PLANETASSIST/');

	//define('DOCUMENT_ROOT', 'C:/xampp/htdocs/PLANETASSIST/');

	

	//********************* URL PARAMETERS TESTING *********************

	//define('DOCUMENT_ROOT', '/home/blueorgo/public_html/');

	//define('DOCUMENT_ROOT_LOGIC', '/');

	//define('URL', 'http://www.bluecardassistance.org/');

	

	//********************* URL PARAMETERS PRODUCTION *********************

	define('DOCUMENT_ROOT', '/var/www/pas-system.net/');

	define('DOCUMENT_ROOT_LOGIC', '/');

	define('URL', 'https://www.pas-system.net/');

	require_once('PDF/tcpdf/config/tcpdf_config.php');
	require_once('PDF/tcpdf/tcpdf.php');

	/***************** WEB SERVICES ************************ */
	define('WSDL_ROOT', URL . 'wsdl/');

	define('ERP_ON', false);
	define('URL_ERP', 'https://bluecard.saas.la/');
	define('URL_USRPSWD', 'WEBSERVICE:web3589bluecard');
	
	define('PROCESS_CREDIT_CARDS', true);
    define('USE_CONVERGE', true);
    define('USE_PAYPHONE', false);

    define('ERP_FARMERS_TAX', false);
	define('ERP_EMISSION_RIGHTS', false);

	define('CUSTOM_PSWD_RESET', true);
	define('AUTO_INVOICE', false);

	define('USE_NEW_CONTRACT', false);
	define('CONTRACT_LOGIC', false);

//        define('TANDI_ON', true);
        define('USE_TANDI', true);
        
        define('FARMERS_TAX', true);
		
//USAR PLACETOPAY***************************************************************
define('USE_PTP', false);
define('RANDOM_NUMBER',  rand(100000, 999999));

	require_once('globalVars.inc.php');
	require_once('erpLibrary.inc.php');
	require_once('convergeVars.inc.php');
	require_once('payphoneVars.inc.php');

	define('NAME', 'BLUECARD' . ' - Tu Tranquilidad es Primero');



	if (!strstr($_SERVER['SCRIPT_NAME'], 'chrones')) {

		require_once('security.inc.php');

	}

	//PAYPHONE library
	require_once('payphone/library/Configuration.php');
	require_once('payphone/library/api/Transaction.php');
        
	//tcpdf library
	//require_once('tcpdf/config/tcpdf_config.php');
	//require_once('tcpdf/tcpdf.php');


	$smarty = new SmartyPlus();

	$smarty->template_dir = DOCUMENT_ROOT . 'smarty';

	$smarty->compile_dir = DOCUMENT_ROOT . 'smarty/compiled';

	$smarty->config_dir = DOCUMENT_ROOT . 'smarty/config';

	$smarty->plugins_dir = DOCUMENT_ROOT . 'lib/smarty-2.6.22/plugins';





	$language = $_SESSION['language'] = 'es';
	$smarty->assign('contract_logic', CONTRACT_LOGIC);
	
	if(isset($_SESSION['serial_pbc'])){

		$profileByCountry = new ProfileByCountry($db, $_SESSION['serial_pbc']);

		$menu = $profileByCountry->getParentsOptions($language);

		$smarty->register('menu');
	}
	

	if (isset($_SESSION['user_name'])) {

		$smarty->assign('nombreUsuario', $_SESSION['user_name']);

	}

	

	if (isset($_SESSION['serial_lang'])) {

		$smarty->assign('serial_lang', $_SESSION['serial_lang']);

	}



	//**************************** DEVELOPING ************************

	//$smarty->assign('document_root', '/PLANETASSIST/');



	//**************************** TESTING AND PRODUCTION ************************

	$smarty->assign('document_root','/');
	$smarty->assign('use_converge',USE_CONVERGE);
	$smarty->assign('use_payphone',USE_PAYPHONE);


	$smarty->assign('language', $_SESSION['language']);



	$date['weekday'] = date('N');

	$date['day'] = strftime("%d");

	$date['month'] = date('n');

	$date['year'] = strftime("%Y");
	
	//Santi 04-Jan-2019: Automatic site url detected.
	$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http");
	switch ($_SERVER['HTTP_HOST']){
		case 'localhost':
            define('SITE_URL', $protocol.'://localhost/PLANETASSIST/');
			break;
		case 'sandbox.pas-system.net':
            define('SITE_URL', $protocol.'://sandbox.pas-system.net/');
			break;
		case 'pas-system.net':
            define('SITE_URL', $protocol.'://www.pas-system.net/');
			break;
	}
	
		//Cabeceras CORS
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
	die();
}

    $smarty->assign('site_url', SITE_URL);
    $smarty->assign('random_number', RANDOM_NUMBER);
	//Debug::print_r(SITE_URL);

	$smarty->register('db,language,date,global_process_as');

	$smarty->register('global_weekDays,global_weekDaysNumb,global_weekMonths,global_typeManager,global_pType,global_status,global_prizeTypes,global_typeUser,global_extraRelationshipType');

	$smarty->register('global_saleTypes,global_promoTypes,global_periodTypes,global_repeatTypes,global_appliedToTypes,global_serviceProviderType,global_paymentForms,global_paymentStatus,global_paymentsStatus');

	$smarty->register('global_refundReasons,global_salesStatus,global_refundRetainedTypes,global_AlertTypes,global_sales_log_status,global_documentPurpose,global_yes_no');

	$smarty->register('global_country,global_cardSalestype,global_ApplianceStatus,global_billTo,global_paymentRequestStatus,global_invoice_status,global_bonus_coverage,global_bonusTo');

	$smarty->register('global_salesStockType,global_system_name,global_appliedBonusStatus,global_overAppliedTo,global_fileStatus,global_fileTypes,global_extraTypes,global_comments_by_service_provider_calification');

	$smarty->register('global_customerPromoTypes,global_customerPromoPaymentForms,global_customerPromoScopes,global_customerPromoStatus,global_customer_promo_types,global_weekMonthsTwoDigits,global_account_types');

?>
