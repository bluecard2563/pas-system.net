<?php

/*

 * File: merchanAccountConnection.php

 * Author: Patricio Astudillo M.

 * Creation Date: 04/08/2010 09:00

 * Last Modified: 04/08/2010

 * Modified By: Patricio Astudillo

 * Description: This script verifies that all the fields required to the merchant account transactions are set.

 * 				Then, it performs the payment and gets the response from Internet Secure. To use this script in

 * 				other servers, just change the "GatewayID" number and the name of the session_variables.

 */



if (isset($_POST['selCityCustomer']) && isset($_POST['selCountry']) && isset($_POST['txtNameCard']) &&
		isset($_POST['txtAddress']) && isset($_POST['txtState']) && isset($_POST['txtZip']) &&
		isset($_POST['txtPhone']) && isset($_POST['txtMail']) && isset($_POST['txtCardNum']) &&
		isset($_POST['selMonth']) && isset($_POST['selYear']) && isset($_POST['txtSecCode'])) {



	//SAVE THE CC NUMBER SAFELY, TO BE USED ON PAYMENT SAVE - pBuyShoppingCart file -

	$len = strlen($_POST['txtCardNum']);

	$ccnum = $_POST['txtCardNum'];

	$safeNum = '';

	for ($i = 0; $i < $len - 4; $i++) {
		$safeNum .= '*';
	}

	$_SESSION['ccnum'] = $safeNum . substr($ccnum, $len - 4, 4);



	$city = new City($db, $_POST['selCityCustomer']);

	$city->getData();

	$country = new Country($db, $_POST['selCountry']);

	$country->getData();

	$items_sold = explode('_', $_SESSION['current_sale_products']);



	/*	 * * FOR TESTING THE CONECTIVITY WITH THE MERCHANT ACCOUNT ** */

	//$test = 'TEST';

	/*	 * * END TESTING FLAG ** */



	//THE ITEM INFORMATION HAS THE FOLLOWING FORMAT:
	//Price::Qty::Product Code::Description::Flags

	$item_information = $_SESSION['current_sale_total'] . '::';

	//WE HAVE TO SENT ALWAYS 1 PRODUCT OTHERWISE THE MERCHANT WILL MULTIPLY THE TOTAL VALUE WITH THE QTY

	$item_information.='1::';

	if($test){
		$item_information.=$_SESSION['current_sale_products'].'::'.$description.'::{'.$test.'}';
	}else{
		$item_information.=$_SESSION['current_sale_products'].'::'.$description;
	}	



	//CREATING THE XML CONTENT

	$txn = ("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n");

	$txn.= ("<TranxRequest> \n");

	$txn.= ("<GatewayID>51815</GatewayID> \n");

	$txn.= ("<Products>$item_information</Products> \n");

	$txn.= ("<xxxName>" . utf8_encode($_POST['txtNameCard']) . "</xxxName> \n");

	$txn.= ("<xxxAddress>" . utf8_encode($_POST['txtAddress']) . "</xxxAddress> \n");

	$txn.= ("<xxxCity>" . utf8_encode($city->getName_cit()) . "</xxxCity> \n");

	$txn.= ("<xxxState>" . utf8_encode($_POST['txtState']) . "</xxxState> \n");

	$txn.= ("<xxxZipCode>" . $_POST['txtZip'] . "</xxxZipCode> \n");

	$txn.= ("<xxxCountry>" . $country->getCode_cou() . "</xxxCountry> \n");

	$txn.= ("<xxxPhone>" . $_POST['txtPhone'] . "</xxxPhone> \n");

	$txn.= ("<xxxEmail>" . $_POST['txtMail'] . "</xxxEmail> \n");

	$txn.= ("<xxxCard_Number>" . $_POST['txtCardNum'] . "</xxxCard_Number> \n");

	$txn.= ("<xxxCCMonth>" . $_POST['selMonth'] . "</xxxCCMonth> \n");

	$txn.= ("<xxxCCYear>" . $_POST['selYear'] . "</xxxCCYear> \n");

	$txn.= ("<CVV2>" . $_POST['txtSecCode'] . "</CVV2> \n");

	$txn.= ("<CVV2Indicator>1</CVV2Indicator> \n");

	$txn.= ("<xxxTransType>00</xxxTransType> \n");

	$txn.= ("</TranxRequest>\n");



	//ADING THE XML INFO, INTO THE REQUEST DATA

	$vars = "xxxRequestMode=X&xxxRequestData=" . $txn;



	/* //SEND THE APPLICATION WITH A CURL_ININT SESSION */

	  $ch = curl_init();

	  curl_setopt($ch, CURLOPT_URL, "https://secure.internetsecure.com/process.cgi");

	  curl_setopt($ch, CURLOPT_HEADER, 1);

	  curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

	  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);



	  //Set OFF THE HOST AND PEER WITH SSL

	  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

	  //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);



	  //SENDING PARAMETERS WITH POST

	  curl_setopt($ch, CURLOPT_POST, 1);

	  curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);



	  $data = curl_exec($ch);// execute form send

	  curl_close($ch);// close curl



	  // process $data, extract XML from $data var

	  $xml_data = strstr($data, '<');

	  $xml = simplexml_load_string($xml_data);

	  $transaction_response = $xml->Page;				// TRANSACTION RESPONSE

	  /*ADDITIONAL DATA THAT COULD BE STORED IN THE PAYMENT_DETAIL DESCRIPTION*/

	  $receipt_number = $xml->ReceiptNumber;

	  $sales_order_number = $xml->SalesOrderNumber;

	  $aproval_code = $xml->ApprovalCode;				// APROVAL CODE

	  $verbiage = $xml->Verbiage;

	  $credit_ccard_type = $xml->CardType;

	  /*END PAYMENT DETAILS */



	//$transaction_response = '2000';



	/* The TRANSACTION_RESPONSE can have many values. The success values are:

	 * 90000: LIVE Success

	 * 2000: TEST Success

	 */

	if ($transaction_response == '2000' or $transaction_response == '90000') {

		$payment_completed = TRUE;

		$custom_error_code = 40;



		/* UNSETTING THE SESSION VARIABLES OF THE SALE */

		if (isset($_SESSION['current_sale_total'])) {

			unset($_SESSION['current_sale_total']);
		}



		if (isset($_SESSION['current_sale_products'])) {

			unset($_SESSION['current_sale_products']);
		}

		/* END UNSET */
	} else {

		$payment_completed = FALSE;

		$custom_error_code = 41;

		//Debugging purposes
		//Debug::print_r($xml);
	}
} else {

	$payment_completed = FALSE;

	$custom_error_code = 42;

	$transaction_response = 'insuficient_data';
}
?>
