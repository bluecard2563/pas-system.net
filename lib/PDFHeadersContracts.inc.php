<?php
	$pdf =& new Cezpdf('A4','portrait');
	//WIDTH: 600
	//HEIGHT: 800
	set_time_limit(36000);
	ini_set('memory_limit','512M');
	
	$pdf->selectFont(DOCUMENT_ROOT.'lib/PDF/fonts/HELNLTSC.afm');
	$pdf -> ezSetCmMargins(1.19,1.5,1.15,1.15);
	$all = $pdf->openObject();
	$pdf->saveState();

	if($sale){
		$planetassist_sale = Sales::isPlanetAssistSale($db, $sale->getSerial_sal());

		if($planetassist_sale){
			$pdf->addJpegFromFile(DOCUMENT_ROOT."img/logopas_contracts.jpg",375,784,170);
		}else{
			$pdf->addJpegFromFile(DOCUMENT_ROOT."img/logoblue_contracts.jpg",410,784,140);
		}
	}else{
		$pdf->addJpegFromFile(DOCUMENT_ROOT."img/logopas_contracts.jpg",375,784,170);
	}
	
	$pdf->setColor(0, 0, 0);
	$pdf->ezStartPageNumbers(560,30,10,'','{PAGENUM} de {TOTALPAGENUM}',1);

	$pdf->restoreState();
	$pdf->closeObject();
	$pdf->addObject($all,'all');
?>
