<?php
/*
File: security.inc.php
Author: Patricio Astudillo
Creation Date: 08/02/2010
LastModified: 20/08/2010
Modified By: Patricio Astudillo
*/

/* OPTIMAL SECURITY FILTERS */
$request_uri = $_SERVER['REQUEST_URI'];
$request_uri_parts = explode('?', $request_uri);


if($request_uri_parts['1'] && $request_uri_parts['0'] == DOCUMENT_ROOT_LOGIC.'evaluateOptimalRequest'){
	$request_uri_parts = explode(',', $request_uri_parts['1']);

	$_SESSION['optimal_id'] = $request_uri_parts['0'];
	$_SESSION['optimal_user_id'] = $request_uri_parts['1'];
	
	echo '<script language="JavaScript" type="text/JavaScript">
	        location.href="'.DOCUMENT_ROOT_LOGIC.'evaluateOptimalRequest";
	    </script>';
}
/* OPTIMAL SECURITY FILTERS */

if(		!isset($_SESSION['serial_usr']) && 
		!isset($_SESSION['suitable_pages']) && 
		!GlobalFunctions::stringStartsWith($_SERVER['REQUEST_URI'], DOCUMENT_ROOT_LOGIC.'index') && 
		($_SERVER['REQUEST_URI'] != DOCUMENT_ROOT_LOGIC.'evaluateOptimalRequest') &&
		!strstr($_SERVER['REQUEST_URI'], 'wsdl') &&
		!strstr($_SERVER['REQUEST_URI'], 'migration_erp/OpenOrange')
){
			
			if($_SERVER['REQUEST_URI'] != DOCUMENT_ROOT_LOGIC . 'pForgotPasswordAjax.php'){ //Let the password retrieval ajax execute:
				echo '<script language="JavaScript" type="text/JavaScript">
						location.href="'.DOCUMENT_ROOT_LOGIC.'index";
					</script>';
			}
}

//*************** MAINTEINANCE SECURITY SORTING ***********************
$manteined_pages = array();
array_push($manteined_pages, 'modules/payments/fChooseInvoice');

$allowed_users = array(5, 3919, 20, 3983, 3220, 3365, 3852, 5183,5079,5233,5122,5578, 6474,3,4,6503,14,5292,6607,5996,6705,6836,6838,6860,6820,6527,6965,5885,7328,7355,7436);

if(count($manteined_pages) > 0){
	$auxiliar_url_m = substr($_SERVER['SCRIPT_NAME'], strlen(DOCUMENT_ROOT_LOGIC));
	$auxiliar_url_m = str_replace('.php', '', $auxiliar_url_m);
	
	if(in_array($auxiliar_url_m, $manteined_pages) && !in_array($_SESSION['serial_usr'], $allowed_users)){
		http_redirect('main/manteinance');
	}
}

//*************** DEFAULT SECURITIES ***********************
if(isset($_SESSION['suitable_pages'])){
	$auxiliar_url = substr($_SERVER['SCRIPT_NAME'], strlen(DOCUMENT_ROOT_LOGIC));
	$auxiliar_url = str_replace('.php', '', $auxiliar_url);
	$common_pages=array();

	if($_SESSION['firstAccess']==1){ //If the user entered the site before.
		array_push($common_pages, 'main');
		array_push($common_pages, 'index');
		array_push($common_pages, 'test');
		array_push($common_pages, '/index');
		array_push($common_pages, 'myAlerts');
		array_push($common_pages, 'modules/sales/modifications/fCheckSalesApplication');
		array_push($common_pages, '404');
		array_push($common_pages, 'myAccount');
		array_push($common_pages, 'pMyAccount');
		array_push($common_pages, 'rpc/remoteValidation/checkOldPassword.rpc');
		array_push($common_pages, 'rpc/alerts/checkUserAlerts.rpc');
		array_push($common_pages, 'test');
		array_push($common_pages, 'firstAccessUpdate');
		array_push($common_pages, 'rpc/remoteValidation/user/checkUserEmail.rpc');
		array_push($common_pages, 'rpc/loadDealers.rpc');
		
		//ALERT ATTENDING PAGES, COMMON TO EVERYBODY:
		array_push($common_pages, 'rpc/loadUsersByCity.rpc');
		array_push($common_pages, 'modules/sales/modifications/fDispatchStandByApplications');
		array_push($common_pages, 'modules/sales/modifications/pDispatchStandByApplications');
		array_push($common_pages, 'modules/sales/modifications/fDispatchBlockedApplications');
		array_push($common_pages, 'modules/sales/modifications/pDispatchBlockedApplications');
		array_push($common_pages, 'modules/sales/modifications/fDispatchReactivationApplications');
		array_push($common_pages, 'modules/sales/modifications/pDispatchReactivationApplications');
		array_push($common_pages, 'modules/sales/modifications/fCheckSalesVoidApplication');
		array_push($common_pages, 'modules/sales/modifications/fCheckSalesApplication');
		array_push($common_pages, 'modules/sales/modifications/pCheckSalesApplication');
		array_push($common_pages, 'modules/sales/modifications/fSearchPendingModifyApplications');
		array_push($common_pages, 'modules/sales/fSearchFreeSales');
		array_push($common_pages, 'modules/refund/fCheckRefundApplication');
		array_push($common_pages, 'modules/invoice/fAuthorizeInvoiceLog');
		array_push($common_pages, 'modules/invoice/pAuthorizeInvoiceLog');
		array_push($common_pages, 'modules/invoice/fSearchInvoiceLog');
		array_push($common_pages, 'modules/refund/fSearchPendingApplications');
		array_push($common_pages, 'modules/sales/modifications/fSearchPendingApplications');
		array_push($common_pages, 'getFreeSaleObsAjax');		
		
		if(!(in_array($auxiliar_url, $_SESSION['suitable_pages']) ||
			in_array($auxiliar_url, $common_pages))){
			ErrorLog::log($db, $_SESSION['serial_usr'] . ' BLOCKED! ' . $auxiliar_url, $_SERVER['HTTP_REFERER']);
			//http_redirect('main/pageError');
		}

 	}else{ //If it's the first time of the user in the PAS
		array_push($common_pages, 'firstAccessUpdate');
		array_push($common_pages, 'pFirstAccessUpdate');
		array_push($common_pages, 'rpc/remoteValidation/checkOldPassword.rpc');
		array_push($common_pages, 'rpc/remoteValidation/user/checkUserEmail.rpc');
		
		array_push($common_pages, 'index');
		if(!(in_array($auxiliar_url, $common_pages))){
			http_redirect('firstAccessUpdate/error');
		}
	}
}
?>
