<?php

################## utilities.inc.php

################## - functions used on pages



function http_redirect($url)
{
	//echo $url;
	header("Location: ".DOCUMENT_ROOT_LOGIC.$url);
	exit;
}
function specialchars($str)
    {
            # this is a very temporary patch... specialchars function was messing with serializing
            # what needs to be done is to find all of the places that serialize is being used
            # and convert the SQL::bind to an array function that will convert each element
            # but use quotes to encapsulate the array

            if(substr($str, 0, 2) == "a:")
            {
                    return addslashes($str);
            }
            else
            {
                    $str = str_replace("<", "&lt;", $str);
                    $str = str_replace(">", "&gt;", $str);
                    $str = str_replace("'", "&#039;", $str);
                    $str = str_replace("\"", "&quot;", $str);
                    $str = str_replace("\\\\", "\\", $str); # to catch any double encoding
                    $str = str_replace("&amp;", "&", $str); # this will reverse any double html encoding issues we have had

                    return addslashes($str); # this verifies that we have a safe query
            }
    }

?>