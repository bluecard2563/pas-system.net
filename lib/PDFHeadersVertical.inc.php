<?php
/*
 * File: PDFHeadersVertical.inc.php
 * Author: Edwin Salvador
 * Creation Date: 28/12/2010
 * Last Modified: 09/11/2010
 * Modified By: Patricio Astudillo
 */

$pdf =& new Cezpdf('A4','portrait');
//WIDTH: 600
//HEIGHT: 800
set_time_limit(36000);
ini_set('memory_limit','1024M');

$pdf->selectFont(DOCUMENT_ROOT.'lib/PDF/fonts/Helvetica.afm');
$pdf -> ezSetCmMargins(4,3,2,1);
$all = $pdf->openObject();
$pdf->saveState();

if(!$clean_pdf){
	$pdf->addJpegFromFile($global_system_logo,375,750,170);
	$pdf->setColor(0, 0.33, 0.64);
	$pdf->addText(60, 765, 16, '<b>SISTEMA '.$global_system_name.'</b>');
	$pdf->setColor(0.57, 0.58, 0.60);
}
$pdf->setColor(0, 0, 0);

$pdf->ezStartPageNumbers(560,30,10,'','{PAGENUM} de {TOTALPAGENUM}',1);
$pdf->restoreState();
$pdf->closeObject();
$pdf->addObject($all,'all');
?>