<?php
################## globalVars.inc.php
################## - General Variables for all the system.

//For more than one, separate with commas, goes inside a sql:
$globalBlueCardCountryCodes = '62';
$globalBlueCardQuitoMbc = 2;
$globalCEOCounter = 3; //JFPonce
$globalCEOCustomer = 93426; //JFPonce
$system_bck_email = 'logs@bluecard.com.ec';
$ageForDisplayQuestions = 75;
$global_ECVatPercentage = 14;
$global_contractDate = array(
	'weekday' => date('N'),
	'day' => strftime("%d"),
	'month' => date('n'),
	'year' => strftime("%Y")
);


$globalSTOCK_MAX_VALUE_PARAMETER = '37'; 
$globalCONTRACT_MAX_VALUE_PARAMETER = '51';

$globalFarmersTax = array (
    'ArtCode' => 'SC',
    'ArtName' => 'Seguro Campesino',
    'Name' => 'Seguro Campesino',
    'SerialNr' => 'N/A',
    'VAT' => 0,
    'tax' => 0.005
);

$global_EmissionRigths = array (
    'ArtCode' => '7777',
    'ArtName' => 'Derechos de Emision',
    'Name' => 'Derechos de Emision',
    'SerialNr' => 'N/A',
    'price' => 15,
	'VAT' => 14
);

$global_ManagementExpenses = array (
    'ArtCode' => '7777',
    'ArtName' => 'Gastos Administrativos',
    'Name' => 'Gastos Administrativos',
    'SerialNr' => 'N/A',
    'price' => 4.50,
    'VAT' => 14
);

$global_weekDays = array('MONDAY'=>'Lunes','TUESDAY'=>'Martes','WEDNESDAY'=>'Mi&eacute;rcoles','THURSDAY'=>'Jueves','FRIDAY'=>'Viernes','SATURDAY'=>'Sabado','SUNDAY'=>'Domingo','ANY'=>'Cualquiera');
$global_weekDaysNumb = array('1'=>'Lunes','2'=>'Martes','3'=>'Miércoles','4'=>'Jueves','5'=>'Viernes','6'=>'Sabado','7'=>'Domingo');
$global_weekDaysNumb_en = array('1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday');
$global_weekMonths = array(
	'1' => 'Enero',
	'2' => 'Febrero',
	'3' => 'Marzo',
	'4' => 'Abril',
	'5' => 'Mayo',
	'6' => 'Junio',
	'7' => 'Julio',
	'8' => 'Agosto',
	'9' => 'Septiembre',
	'10' => 'Octubre',
	'11' => 'Noviembre',
	'12' => 'Diciembre'
);

$global_weekMonths_en = array(
	'1' => 'January',
	'2' => 'February',
	'3' => 'March',
	'4' => 'April',
	'5' => 'May',
	'6' => 'June',
	'7' => 'July',
	'8' => 'August',
	'9' => 'September',
	'10' => 'October',
	'11' => 'November',
	'12' => 'December'
);

$global_weekOrdinalNum_en = array(
	'1' => 'st',
	'2' => 'nd',
	'3' => 'rd',
	'4' => 'th',
	'5' => 'th',
	'6' => 'th',
	'7' => 'th',
	'8' => 'th',
	'9' => 'th',
	'10' => 'th',
	'11' => 'th',
	'12' => 'th',
	'13' => 'th',
	'14' => 'th',
	'15' => 'th',
	'16' => 'th',
	'17' => 'th',
	'18' => 'th',
	'19' => 'th',
	'20' => 'th',
	'21' => 'st',
	'22' => 'nd',
	'23' => 'th',
	'24' => 'th',
	'25' => 'th',
	'26' => 'th',
	'27' => 'th',
	'28' => 'th',
	'29' => 'th',
	'30' => 'th',
	'31' => 'st',
);

$global_weekMonthsTwoDigits=array(   '01'=>'Enero',
                                     '02'=>'Febrero',
                                     '03'=>'Marzo',
                                     '04'=>'Abril',
                                     '05'=>'Mayo',
                                     '06'=>'Junio',
                                     '07'=>'Julio',
                                     '08'=>'Agosto',
                                     '09'=>'Septiembre',
                                     '10'=>'Octubre',
                                     '11'=>'Noviembre',
                                     '12'=>'Diciembre'
                                );
$globalCONTRACT_MAX_VALUE_PARAMETER = '51';
$global_typeManager=array('PERSON'=>'Persona Natural','LEGAL_ENTITY'=>'Persona Jur&iacute;dica','BOTH'=>'Persona Natural/Jur&iacute;dica');
$global_typeManagerPdf=array('PERSON'=>'Persona Natural','LEGAL_ENTITY'=>'Persona Juridica','BOTH'=>'Persona Natural/Juridica');
$global_pType=array('COMISSION'=>'Comisi&oacute;n','DISCOUNT'=>'Descuento');
$global_status=array('ACTIVE'=>'Activo','INACTIVE'=>'Inactivo','PENDING'=>'Pendiente');
$global_visit_type=array('PRODUCT'=>'Presentaci&oacute;n de producto','BONUS'=>'Promoci&oacute;n','PROMO'=>'Incentivo');
$global_visit_status=array('PENDING'=>'Pendiente','VISITED'=>'Visitado','SKIPPED'=>'Postergado');
$global_prizeTypes=array('PROMO'=>'Promoci&oacute;n','BONUS'=>'Incentivo','CUSTOMER'=>'Cliente');
$global_prizeCoverages=array('TOTAL'=>'Canje Total','AMOUNT'=>'Canje Unitario');
$global_typeUser=array('MANAGER'=>'Representante','COUNTER'=>'Counter','COMISSIONIST'=>'Responsable','USER'=>'Usuario');
$global_saleTypes=array('NET'=>'Neta','GROSS'=>'Bruta');
$global_promoTypes=array('PERCENTAGE'=>'Porcentaje de Ventas','AMOUNT'=>'Monto Fijo','RANGE'=>'Rango de Ventas','PRODUCT'=>'Producto','BOTH'=>'Ambas');
$global_periodTypes=array('ONE DAY'=>'Un d&iacute;a','RANDOM'=>'Aleatorio','SEVERAL DAYS'=>'Varios D&iacute;as');
$global_repeatTypes=array('WEEKLY'=>'Semanalmente','MONTHLY'=>'Mensualmente');
$global_appliedToTypes=array('COUNTRY'=>'Pa&iacute;s','CITY'=>'Ciudad','MANAGER'=>'Representante','DEALER'=>'Comercializador');
$global_serviceProviderType=array('MEDICAL'=>'M&eacute;dico','TECHNICAL'=>'T&eacute;cnico','BOTH'=>'M&eacute;dico y T&eacute;cnico');
$global_salesStatus=array('STANDBY'=>'Stand By','ACTIVE'=>'Activa','VOID'=>'Anulada','REGISTERED'=>'Registrada','EXPIRED'=>'Caducada','REQUESTED'=>'Solicitada','DENIED'=>'Negada','REFUNDED'=>'Reembolsada', 'BLOCKED'=>'Bloqueada','RESERVED'=>'Reservada');
$global_paymentForms=array( 'CASH'=>'Efectivo',
						    'CREDIT_CARD'=>'Tarjeta de Cr&eacute;dito',
							'CHECK'=>'Cheque',
							'CREDIT_NOTE'=>'Nota de Cr&eacute;dito',
							'TRANSFER'=>'Transferencia Bancaria',
							'EXCESS'=>'Pago en Exceso',
							'UNCOLLECTABLE'=>'Incobrable',
							'PAYPAL'=>'Pay Pal',
							'RETENTION'=>'Retenci&oacute;n',
							'COMMISSION'=>'Cruce con Comisi&oacute;n',
							'ELAVON' => 'Pago Elavon',
							'EXCESS_CHECK' => 'Pago en Exceso Liquidado');
$global_paymentStatus=array('ACTIVE'=>'Activo','VOID'=>'Anulado');
$global_extraRelationshipType=array('SPOUSE'=>'acompañante 1','RELATIVE'=>'acompañante 2','OTHER'=>'Otro');
$global_paymentsStatus=array('PAID' => 'Pagada','VOID' => 'Anulada','REFUNDED' => 'Reembolsada','PARTIAL' => 'Abonada','EXCESS' => 'Exceso', 'CxC' => 'Cuenta por Cobrar');
$global_refundReasons=array('VISA' => 'Negaci&oacute;n de Visa','MAJOR_ISSUE' => 'Fuerza Mayor', 'SPECIAL' => 'Especial');
$global_refundStatus=array('APROVED'=>'Aprobada','STAND-BY'=>'Stand-By','DENIED'=>'Negada');
$global_refundTypes=array('REGULAR' => 'Reembolsos','PENALTY' => 'Penalidad');
$global_refundRetainedTypes=array('PERCENTAGE' => 'Porcentajes','AMOUNT' => 'Valor Fijo');
$global_AlertTypes=array('FILE' => 'Asistencia',
						 'INVOICE' => 'Solicitud Facturaci&oacute;n',
						 'SALE' => 'Solicitud Venta',
						 'REFUND' => 'Solicitud Reembolso',
						 'CREDIT_NOTE' => 'Solicitud Nota de Cr&eacute;dito',
						 'FREE'=>'Solicitud Free',
						 'STANDBY'=>'Solicitud Stand-By',
						 'BLOCKED' => 'Bloqueado',
						 'REACTIVATED'=>'Solicitud de Reactivaci&oacute;n',
						 'SPECIAL_SALE_MODIFICATION'=>'Solicitud Venta',
						 'VOID_SALE' => 'Solicitud de anulacion de venta',
						 'SPECIAL_REFUND' => 'Solicitud Especial de Reembolso');
$global_yes_no=array('YES'=>'Si','NO'=>'No');
$global_invoice_number=array('AUTOMATIC'=>'Autom&aacute;tica','MANUAL'=>'Manual');
$global_sales_log_status=array('PENDING'=>'Pendiente','AUTHORIZED'=>'Autorizada','DENIED'=>'Negada');
$global_sales_log_type=array('ADD_DAYS' => 'Aumento de D&iacute;as',
							'STAND_BY' => 'Stand By',
							'MODIFICATION' => 'Modificaci&oacute;n Normal',
							'VOID_INVOICE' => 'Anulaci&oacute;n de Factura',
							'REFUNDED' => 'Reembolso',
							'BLOCKED' => 'Bloqueo de Tarjeta',
							'REACTIVATED' => 'Reactivaci&oacute;n de Tarjeta',
							'SPECIAL_MODIFICATION' => 'Modificati&oacute;n especial',
							'VOID_SALE' => 'Anulaci&oacute;n de Venta',
							'FREE' => 'Venta Free');
$global_documentPurpose=array('INVOICE'=>'Facturaci&oacute;n','CREDIT_NOTE'=>'Notas de Cr&eacute;dito','PAYMENT_NOTE'=>'Notas de Pago');
$global_medicalType=array('SIMPLE'=>'Simple','COMPLEX'=>'Compleja');
$global_contactsByProviderType=array('ASSISTANCE'=>'Asistencia','PAYMENT'=>'Pagos','MANAGER'=>'Representante');
$global_travelRegisterStatus=array('ACTIVE'=>'Activo','EXPIRED'=>'Caducado','DELETED'=>'Eliminado');
$global_cardSalestype=array('SYSTEM'=>'Sistema','WEB'=>'Web','PHONE'=>'Tel&eacute;fono');
$global_ApplianceStatus=array('REQUESTED'=>'Solicitada','DENIED'=>'Rechazada','AUTHORIZED'=>'Autorizada');
$global_typeDocumentFile=array('PAID'=>'Pagada','PENDING'=>'Pendiente','ALL'=>'Todos');
$global_billTo=array('CUSTOMER'=>'Cliente','CLIENT'=>'A Cliente','PROVIDER'=>'A Coordinador','DIRECT'=>'Directo','DEALER'=>'Comercializador','NONE'=>'No Aplica');
$global_bonusTo=array('COUNTER'=>'Counter','DEALER'=>'Comercializador');
$global_paymentRequestStatus=array('AUDIT_REQUEST'=>'Auditor&iacute solicitada','AUDITED'=>'Auditado','APROVED'=>'Aprobado','DENIED'=>'Negado','STAND-BY'=>'Pendiente');
$global_invoice_status=array('PAID'=>'Pagada','STAND-BY'=>'Pendiente','VOID'=>'Anulada','UNCOLLECTABLE'=>'Incobrables');
$global_bonus_coverage=array('AMOUNT'=>'Canje Unitario','TOTAL'=>'Canje Total');
$global_salesStockType=array('VIRTUAL'=>'Virtual','MANUAL'=>'Manual');
$global_appliedBonusStatus=array('ACTIVE'=>'Pendiente','PAID'=>'Pagado','REFUNDED'=>'Reembolsado');
$global_overAppliedTo=array('MANAGER'=>'Representante','DEALER'=>'Comercializador');
$global_extraTypes=array('ADULTS'=>'Solo adultos','CHILDREN'=>'Solo ni&ntilde;os','BOTH'=>'Adultos y ni&ntilde;os');
$global_fileStatus=array('open'=>'Abierto','closed'=>'Cerrado','in_liquidation'=>'En Liquidaci&oacute;n','void'=>'Anulado');
$global_fileTypes=array('AMBULATORY'=>'Ambulatoria','TECHNICAL'=>'T&eacute;cnica','HOSPITAL'=>'Hospitalaria','INFORMATIVE'=>'Informativa');
$global_assistanceEmailType=array('create'=>'Nuevo Expediente','update'=>'Actualizar Expediente','request'=>'Solicitud de Asistencia','medical_checkup'=>'Auditor&iacute;a M&eacute;dica','close'=>'Cierre de Expediente');
$global_alertTypes = array( 'VOID_INVOICE'=>'Anulaci&oacute;n de factura',
							'STAND_BY'=>'Autorizaci&oacute;n de stand by',
							'MODIFICATION'=>'Cambios de fecha normales',
							'SPECIAL_MODIFICATION'=>'Cambios de fecha especiales',
							'FREE_SALE'=>'Autorizaci&oacute;n de Free',
							'VOID_SALE'=>'Anulaci&oacute;n de tarjeta');
$global_alertStatus = array('PENDING'=>'Pendiente',
							'DONE'=>'Atendida');
$global_customerPromoTypes=array(   'CURRENT_SALE'=>'Compra Actual',
                                     'NEXT_SALE'=>'Siguiente Compra',
                                     'BOTH'=>'Compra Actual y Siguiente'
                                );
$global_customerPromoPaymentForms=array( 'CREDIT_CARD'=>'Tarjeta de Cr&eacute;dito',
										 'PAYPAL'=>'Pay Pal',
										 'CASH'=>'Efectivo y Otros'
                                );
$global_customerPromoScopes=array(  'WEB'=>'Ventas WEB',
                                    'SYSTEM'=>'Ventas PAS',
                                    'PHONE'=>'Ventas Telef&oacute;nicas'
                                );
$global_customerPromoStatus=array(  'REGISTERED'=>'Registrada',
                                    'ACTIVE'=>'Activa',
                                    'EXPIRED'=>'Expirada',
									'CANCELED'=>'Cancelada'
                                );
$global_customer_types=array(	'PERSON' => 'Persona Natural',
								'LEGAL_ENTITY' => 'Persona Jur&iacute;dica'
							);
$global_customer_promo_types=array(	'CURRENT_SALE' => 'Compra Actual',
									'NEXT_SALE' => 'Siguiente Compra',
									'BOTH' => 'Compra actual y siguiente compra.'
								);
$global_payment_request_type=array(	'FEE' => 'Fee',
									'MANAGEMENT' => 'Coordinaci&oacute;n',
									'AUDITORY' => 'Auditor&iacute;a',
									'REPRICING' => 'Repricing',
									'CREDIT_NOTE' => 'Nota de Cr&eacute;dito',
									'N/A' => 'N/A',
    								'FEE_MANAGEMENT' => 'Fee de Coordinaci&oacute;n',
    								'FEE_REPRICING' => 'Fee Repricing'
								);

$global_comments_by_service_provider_calification=array('EXCELENT' => 'Excelente',
														'VERY_GOOD' => 'Muy Bueno',
														'GOOD' => 'Bueno',
														'REGULAR' => 'Regular',
														'DEFICIENT' => 'Deficiente'
													);
$global_account_types = array ( 'SAVINGS' => 'Ahorros',
								'CURRENT' => 'Corriente');

$global_process_as = array(	'PAYABLE' => 'Pagado',
							'NOPAYABLE' => 'Por Pagar');


define('GLOBAL_STATUS_ACTIVE', 'Activo');
define('GLOBAL_STATUS_INACTIVE', 'Inactivo');

$generalConditionsDocPath = 'pdfs/generalConditions/';
$global_contracts_dir = 'modules/sales/contracts/';
$global_yearly_contracts = 'modules/sales/yearlycontracts/';
$phoneSalesDealer = 3; 
$webSalesDealer = 1;

/**** DEFINE THE TEXT FOR THE PAS LABEL ****/
if(isset($_SESSION['serial_usr'])){
	$global_country=GlobalFunctions::getUsersCountryOfWork($db, $_SESSION['serial_usr']);
	if($global_country=='62'){ //If the user is from ECUADOR
		$global_system_name='BLUECARD';
		$global_system_logo=DOCUMENT_ROOT."img/contract_logos/logoblue_contracts.jpg";
	}else{
            $global_system_name='BLUECARD';
            $global_system_logo=DOCUMENT_ROOT."img/contract_logos/logoblue_contracts.jpg";
//            $global_system_name='PLANET ASSIST';
//            $global_system_logo=DOCUMENT_ROOT."img/contract_logos/logopas_contracts.jpg";
	}
}else{
    $global_system_name='BLUECARD';
    $global_system_logo=DOCUMENT_ROOT."img/contract_logos/logoblue_contracts.jpg";
//    $global_system_name='PLANET ASSIST';
//    $global_system_logo=DOCUMENT_ROOT."img/contract_logos/logopas_contracts.jpg";
}
/**** END DEFINE ****/

/**************************************** CONTRACTS **************************************/
//Text Sizes:
$smallTextSize = 8;//8
$medTextSize = 10;//9
$stdTextSize = 10;//10
$largeTextSize = 10;//11
$titleTextSize = 14;

//Colors:
$blue = array(0, 0.33, 0.64);
$lightBlue = array(0.9, 0.93, 0.96);
$refText = array(0,0,0); //GRAY::array(0.57, 0.58, 0.60);
$red = array(0.92,0.11,0.14);
$black = array(0,0,0);
$white = array(1,1,1);

$refLines = array(0.57, 0.58, 0.60);
$rimacBckGnd = array(0.57, 0.58, 0.60);
$lightGray = array(0.89, 0.88, 0.87);

//Positioning:
$leftMostX = 40;
/**************************************** END CONTRACTS **********************************/

/**************************************** ESTIMATOR **************************************/
$estimatorSuccessPage = 'modules/estimator/registered/estimatorCustomer/';
$estimatorErrorPage = 'modules/estimator/registered/saleCustomerSummary/';
$paramBody = new Parameter($db,'7');
$paramBody -> getData();
$adultMinAge = $paramBody-> getValue_par(); //At this age people are considered to be adults
/**************************************** END ESTIMATOR **********************************/

$systemToken = md5('planet');
$systemBaseURL = 'http://www.pas-system.net/';
$webBaseURL = 'http://www.planet-assist.net/';

//$systemBaseURL = 'http://localhost/PLANETASSIST/';
//$webBaseURL = 'http://localhost/PLANETASSISTWEB/';

/***************** MIGRATION ITEMS FOR SALES *******************/
$global_salesRanges=array(
	array('from'=>65000,'to'=>85000)
);

$migration_date_for_stock='2010-12-22';
/***************** END MIGRATION ITEMS *************************/

/***************** RANGE AGES VA*************************/
$global_maxAgeAdults = 75;
$global_minAgeAdults = 12;
$global_maxAgeChildren = 11;
$global_minAgeChildren = 0;
$global_minAgeHolder = 0;
/***************** RANGE AGES VA*************************/

//******************* SALESMAN SPECIFIC COMISSIONS *************
$mbcAllowedToComissionate = "5,6";
$mbcCommissionTable = array('6'=> array( 
								array('base'=> 0, 'top'=>10000, 'comissionPercentage'=>0),
								array('base'=> 10001, 'top'=>18000, 'comissionPercentage'=>3),
								array('base'=> 18001, 'top'=>10000000000, 'comissionPercentage'=>5)
							), '5' => array(
								array('base'=> 0, 'top'=>8000, 'comissionPercentage'=>0),
								array('base'=> 8001, 'top'=>10000, 'comissionPercentage'=>2),
								array('base'=> 10001, 'top'=>20000, 'comissionPercentage'=>3),
								array('base'=> 20001, 'top'=>10000000000, 'comissionPercentage'=>5),
							));

//**************** CUSTOM PAYMENT FORMS
$custom_payments = array('CASH'=>'Efectivo', 'CREDIT_CARD'=>'Tarjeta de Cr&eacute;dito', 'CHECK'=>'Cheque', 'TRANSFER'=>'Transferencia Bancaria');

?>
