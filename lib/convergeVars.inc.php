<?php
    define('CONVERGE_MERCHANT_ID', '743418');
    define('CONVERGE_USER_ID', 'passystem');
    define('CONVERGE_PIN_ID', 'AE84UQLYBG90SN48QK54D0AG7EYUPT2744KE3LUC1KB0PX3QGU14X10IS6YJKN5F');
    
    define('CONVERGE_TEST_URL', 'https://demo.myvirtualmerchant.com/VirtualMerchantDemo/process.do');
    define('CONVERGE_PRODUCTION_URL', 'https://www.myvirtualmerchant.com/VirtualMerchant/process.do');
    define('CONVERGE_TRANSACTION_TYPE', 'ccsale');
    
    define('MERCHANT_SUCCESS_PAGE', URL.'modules/estimator/epayment/successTransaction');
    define('MERCHANT_ERROR_PAGE', URL.'modules/estimator/epayment/transactionDeclined');
    
    define('SITE_ERROR_PAGE', URL.'modules/estimator/epayment/declinedPage');
    define('FIXED_AMOUNT_PAYMENT', URL.'modules/estimator/epayment/fixedPaymentValidator');
    define('SELECT_INVOICE_PAYMENT', URL.'modules/estimator/epayment/selectedPaymentValidator');