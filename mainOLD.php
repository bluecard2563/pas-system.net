<?php 
/*
File: inicio.php
Author: Patricio Astudillo M.
Creation Date: 23/12/2009 15:21
Last Modified:19/04/2010
Modified By:Nicolas Flores
*/

	Request::setString('0:page');
	Request::setString('1:error');
	Request::setInteger('2:success');
	Request::setInteger('3:printingId');
	Request::setInteger('4:invoiceError');

	switch($page){
		case 'masive_sales':
		case 'sales':
				$sale=new Sales($db, $printingId);
				$sale->getData();
				$sale=get_object_vars($sale);
				unset($sale['db']);
				
				$invoice_data = Invoice::getBasicInvoiceInformation($db, $sale['serial_inv']);
				if($invoice_data){
					$serial_mbc = $invoice_data['serial_mbc'];                                          
					$erp_bodega = ERPConnectionFunctions::getStoreID($serial_mbc); 	    
					$erp_pde = ERPConnectionFunctions::getDepartmentID($serial_mbc); 	                                         
					
					if ((int)$erp_bodega > 10){
						$number_inv = substr ($invoice_data['number_inv'], 1);
						$cadena =(int)$erp_bodega.$erp_pde.str_pad($number_inv, 9, "0", STR_PAD_LEFT);                                                                      
					}else{
						$cadena =  (int)$erp_bodega.$erp_pde.str_pad($invoice_data['number_inv'], 9, "0", STR_PAD_LEFT)."";                                                                
					}   
					
					$printInvoiceData['total_number_inv'] = $invoice_data['number_inv'];
					$printInvoiceData['document'] = $invoice_data['holder_document'];
					$smarty->register('printInvoiceData');
				}else{
                                        $printInvoiceData['total_number_inv'] = $invoiceError;
                                        $smarty->register('printInvoiceData');
                                }

				/* CUPON INFORMATION */
				if(isset($_SESSION['serial_cup'])){
					$serial_cup = $_SESSION['serial_cup'];
					unset ($_SESSION['serial_cup']);
					$cupon_code = Cupon::getCouponCompleteCode($db, $serial_cup);
				}
				/* CUPON INFORMATION */
				$smarty->register('sale,cupon_code');
			break;
	}
	
	

	//******************* FAVORITES SECTION
	$myFavorites = Favorites::getMyFavorites($db, $_SESSION['serial_usr']);
	
	//****************** POP UP SECTION ********************
	$activePopUp = CustomAdds::getActivePopUpForManager($db, $_SESSION['serial_mbc']);
	if(!$activePopUp){
		$activeResponsible = UserByDealer::getActiveResponsibleForBranch($db, $_SESSION['serial_dea']);
        
		$activePopUp = CustomAdds::getActivePopUpForResponsible($db, $activeResponsible['serial_usr']);
	}
	
	$smarty->register('page,error,success,printingId,myFavorites,activePopUp');
	$smarty->display();
?>