<?php

$travelerLogs = TandiConnectionFunctionsMigration::getTravelerLogData($db, $card_number_sal);

foreach($travelerLogs as $log){
    $serial_trl = $log['serial_trl']; // Extrae el identificador único
    $travelerLog = [
        "facturaId" => $log['facturaId'],
        "fechaViajeInicio" => $log['fechaViajeInicio'],
        "fechaViajeFin" => $log['fechaViajeFin'],
        "listaAsegurados" => [
            [
                "genero" => $log['genero'],
                "fechaNacimiento" => $log['fechaNacimiento'],
                "tipoTitularId" => $log['tipoTitularId'],
                "identificacion" => $log['identificacion'],
                "tipoIdentificacion" => $log['tipoIdentificacion'],
                "nombresAsegurado" => $log['nombresAsegurado'],
                "apellidosAsegurado" => $log['apellidosAsegurado'],
                "direccionDomicilio" => $log['direccionDomicilio'],
            ],
        ],
    ];

    Debug:print_r($travelerLog);

     // Inicializamos cURL
     $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL =>  "https://insurance.bluecard.com.ec/insurance/rest/WSDeclaracionViajes/creaEndosoDDV",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($travelerLog, JSON_UNESCAPED_UNICODE),
        CURLOPT_HTTPHEADER => array(
            'usuario: UserBlueWS',
            'clave: $BluePass2023',
            'Content-Type: application/json',
        ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);
    
    $response = json_decode($response, true);
    Echo ('Respuesta:');
    Debug::print_r($response);

        // Si la respuesta es exitosa, actualizar el campo migrado_trl en la base de datos
    if (isset($response['success']) && $response['success'] == true) {
    $updateSql = "UPDATE traveler_log SET migrado_trl = 'SI' WHERE serial_trl = ?";
    $db->query($updateSql, [$serial_trl]);
    }
        else {
        $errorMessage = isset($response['error']) ? $response['error'] : 'Error desconocido';
        $updateSql = "UPDATE traveler_log SET error_migracion_trl = ? WHERE serial_trl = ?";
        $db->query($updateSql, [$errorMessage, $serial_trl]);
    }
}

?>
