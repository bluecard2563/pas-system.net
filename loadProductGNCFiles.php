<?php
/*
File: loadProductGNCFiles.php
Author: Santiago Borja Lopez
Creation Date: 26/02/2010 10:00
LastModified: 02/03/2010 10:00
Modified By: Santiago Borja Lopez
*/
	Request::setString('product_id');
	
	$cbp = new ConditionsByProduct($db, $product_id);
	if($country_of_sale != 62){
		$country_of_sale = 1;
	}
	
	$conds = $cbp -> getConditions($country_of_sale, $_SESSION['serial_lang']);
	
	$returnTable = '';
	if(is_array($conds)){
		foreach ($conds as $con){
			$returnTable .= '<a target="_blank" href="'.URL.'pdfs/generalConditions/'.$con['url_glc'].'">'.utf8_encode($con['name_glc']).'</a><br/>';
		}
	}
	
	if(count($conds) == 1){
		echo '1%%%'.URL.'pdfs/generalConditions/'.$conds[0]['url_glc'];return;
	}
	
	echo count($conds).'%%%'.$returnTable;
?>