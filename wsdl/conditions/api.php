<?php
/**
 * File: Confitions
 * Author: Santiago Albuja
 * Creation Date: 18-Agosto-2020
 */

require_once(DOCUMENT_ROOT . 'lib/config.inc.php');

class API extends Rest
{
    private $ProductTypes = array('PRODUCT', 'LEGAL_ENTITY');
    private $path;
    private $url;
    private $travelerPath;
    private $salesPath;
    private $contractPath;
    private $contractLanguageEn;
    private $contractLanguageEs;

    public function __construct()
    {
        parent::__construct();
        $this->path = 'pdfs/generalConditions/';
        $this->url = 'https://www.pas-system.net/';
        $this->travelerPath = 'modules/sales/travelRegister/pPrintRegister/';
        $this->salesPath = 'modules/sales/pPrintSalePDF/';
        $this->contractPath = 'pdfs/planet/app/contracts/';
        $this->contractLanguageEn = '1' . '/' . '1';
        $this->contractLanguageEs = '1';
    }

    public function processApi()
    {
        $func = strtolower(trim(str_replace("/", "", $_REQUEST['rquest'])));

        if ((int)method_exists($this, $func) > 0)
            $this->$func();
        else
            $this->response('', 404);                // If the method not exist with in this class, response would be "Page not found".
    }

    /**
     * Get the links of the general conditions files associated with a product.
     *
     * @param integer $productId
     * @return array
     */
    private function WSGeneralConditionsFiles()
    {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        if (!$this->validate_login_attempt()) {
            $this->response(array('status' => "Failed", "msg" => "Invalid Credentials"), 401);
        }

        if (!isset($this->_request['productId'])) {
            $this->response(array('status' => "Failed", "msg" => "Product id is required to process the request"), 422);
        }

        $serialProduct = $this->cleanField(utf8_decode($this->_request['productId']));
        $serialLanguage = $this->cleanField(utf8_decode($this->_request['languageId']));
        $generalConditions = ConditionsByProduct::getGeneralConditionsByProduct($this->_db, $serialProduct);

        if ($generalConditions) {
            foreach ($generalConditions as $key => $value) {
                $link = $this->url . $this->path . utf8_decode($value['url_glc']);
                $filename = DOCUMENT_ROOT . $this->path . utf8_decode($value['url_glc']);

                $data['condition'][$key]['name'] = $value['name_glc'];
                $data['condition'][$key]['file'] = $value['url_glc'];
                $data['condition'][$key]['link'] = $link;

                if (file_exists($filename)) {
                    $data['condition'][$key]['exists'] = true;
                } else {
                    $data['condition'][$key]['exists'] = false;
                }

            }
            $this->response($data, 200);
        } else {
            $this->response(array('status' => "Failed", "msg" => "The product has no associated general conditions"), 404);
        }
    }

    /**
     * Get the sales contract file link where the particular conditions are detailed.
     *
     * @param integer $cardNumber
     * @return array
     */
    private function WSParticularConditionsFiles()
    {

        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        if (!$this->validate_login_attempt()) {
            $this->response(array('status' => "Failed", "msg" => "Invalid Credentials"), 401);
        }

        if (!isset($this->_request['cardNumber'])) {
            $this->response(array('status' => "Failed", "msg" => "Card number is required to process the request"), 422);
        }

        $cardNumber = $this->cleanField(utf8_decode($this->_request['cardNumber']));
        $language = $this->cleanField(utf8_decode($this->_request['language']));
        $sale = ConditionsByProduct::getParticularConditionsByContract($this->_db, $cardNumber);

        if ($sale) {

            if (!empty(utf8_decode($sale['serial_trl']))) {
                // The serial_trl field is evaluated to obtain the sale contract generated in the traveler_log table
                $fileUrl = $this->url . $this->travelerPath . utf8_decode($sale['serial_trl']) . '/' . $this->contractLanguageEs;
                if ($language == 1) {
                    $fileUrl = $this->url . $this->travelerPath . utf8_decode($sale['serial_trl']) . '/' . $this->contractLanguageEn;
                }
                $fileName = utf8_decode($sale['card_number_trl']);
            } else {
                // Otherwise we obtain the normal sale contract generated in sales table
                $fileUrl = $this->url . $this->salesPath . utf8_decode($sale['serial_sal']) . '/' . $this->contractLanguageEs;
                if ($language == 1) {
                    $fileUrl = $this->url . $this->salesPath . utf8_decode($sale['serial_sal']) . '/' . $this->contractLanguageEn;
                }
                $fileName = utf8_decode($sale['card_number_sal']);
            }

            // Download a copy of the generated contract pdf file.
            $path = DOCUMENT_ROOT . $this->contractPath . $fileName . '.pdf';
            $this->downloadFile($path, $fileUrl);

            if (file_exists($path)) {
                // Build file link to attach to the response data
                $link = $this->url . $this->contractPath . $fileName;
            } else {
                $this->response(array('status' => "Failed", "msg" => "The file could not be downloaded to the server"), 404);
            }

            // Build response data-
            $data['contract']['saleId'] = $sale['serial_sal'];
            $data['contract']['link'] = $link;
            $data['contract']['status'] = $sale['status_sal'];

            // Return response data to the client.
            $this->response($data, 200);


        } else {
            $this->response(array('status' => "Failed", "msg" => "No contract found with the card number sent"), 404);
        }

    }

    /**
     * Save a copy in server directory of the generated pdf sales contract file.
     *
     * @param string $path
     * @param string $url
     * @return file
     */
    private function downloadFile($path, $url)
    {

        $newFileName = $path;
        $file = fopen($url, 'rb');
        if ($file) {
            $newFile = fopen($newFileName, 'wb');
            if ($newFile) {
                while(!feof($file)) {
                    fwrite($newFile, fread($file, 1024 * 8), 1024 * 8);
                }
            }
        }
        if ($file) {
            fclose($file);
        }
        if ($newFile) {
            fclose($newFile);
        }
    }

}

// Initiiate Library
$api = new API;
$api->processApi();
