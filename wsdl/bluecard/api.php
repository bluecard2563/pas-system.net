<?php
/**
 * File: api
 * Author: Valeria Andino
 * Creation Date: 21-07-2016
 * Last Modified: 21-07-2016
 */

	ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net/:/var/www/pas-system.net/lib/smarty-2.6.22:/var/www/pas-system.net/lib/Excel/Classes/:/var/www/pas-system.net/lib/PDF/');
	define('DOCUMENT_ROOT', '/var/www/pas-system.net/');
	require_once(DOCUMENT_ROOT.'lib/config.inc.php');
	
	//ini_set('include_path',ini_get('include_path').':/Applications/XAMPP/htdocs/RIMAC/lib/:/Applications/XAMPP/htdocs/RIMAC/lib/clases:/Applications/XAMPP/htdocs/RIMAC:/Applications/XAMPP/htdocs/RIMAC/lib/smarty-2.6.22');
	//define('DOCUMENT_ROOT', '/Applications/XAMPP/htdocs/RIMAC/');
	//require_once(DOCUMENT_ROOT.'lib/config.inc.php');
	
	class API extends Rest {
		
		public function __construct(){
			parent::__construct();				// Init parent contructor
		}
		
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);				// If the method not exist with in this class, response would be "Page not found".
		}
		
	    public function WSSalesByCustomerOrCardCompleteData(){
             
             if (!$this->validate_login_attempt()) {
              $this->response('', 418);
           }
           
            if ($this->get_request_method() != "POST") {
                $this->response('', 406);
            }           

            $customerId = $this->_request['idCustomer']; 
            $numberCardSale = $this->_request['numberCardSale'];
                       
            if (!isset($customerId)){
                if ( !isset($numberCardSale)){
                $this->response(array('status' => "Failed", "msg" => "INSERT DATA"), 409);}
            }   
           
            $sales = Sales::getSalesByCustomerOrCardCompleteData($this->_db,$customerId, $numberCardSale);
           
            if ($sales) {                
                
                $SalesSet = array();
                $temp_Array = array();

                foreach ($sales as $s) {
                                         $temp_Array['serialCus'] = $s['serial_cus'];
                                        $temp_Array['nameCus'] = utf8_encode($s['name']);
                                        $temp_Array['documentCus'] = $s['document_cus'];
                                        $temp_Array['addressCus'] = utf8_encode($s['address_cus']);
                                        $temp_Array['phone1Cus'] = $s['phone1_cus'];
                                        $temp_Array['phone2Cus'] = $s['phone2_cus'];
                                        $temp_Array['cellPhoneCus'] = $s['cellphone_cus'];
                                        $temp_Array['birthdateCus'] = $s['birthdate_cus'];
                                        $temp_Array['emailCus']= utf8_encode($s['email_cus']);
                                        $temp_Array['relativeCus'] = utf8_encode($s['relative_cus']);
                                        $temp_Array['relativePhoneCus']=$s['relative_phone_cus'];
                                        $temp_Array['serialSal']=$s['serial_sal'];
                                        $temp_Array['cardNumberSal']= $s['card_number_sal'];
                                        $temp_Array['inDateSal'] = utf8_encode($s['in_date_sal']);
                                        $temp_Array['beginDateSal'] = utf8_encode($s['begin_date_sal']);
                                        $temp_Array['endDateSal'] = utf8_encode($s['end_date_sal']);
                                        $temp_Array['statusSal'] = $s['status_sal'];
                                        $temp_Array['nameExt'] = utf8_encode($s['nameExt']);
                                        $temp_Array['documentExt']=$s['documentExt'];
                                        $temp_Array['relationshipExt'] = utf8_encode($s['relationship_ext']);                                        
                                        
                                        
                        array_push($SalesSet, $temp_Array);
                }

                $this->response($SalesSet, 200);
            }

            $this->response(array('status' => "Failed", "msg" => "NO SALES"), 200);            
                        
        }
       
        
        public function WSSalesByCardNumber(){
             
             if (!$this->validate_login_attempt()) {
                $this->response('', 418);
            }
            
            if ($this->get_request_method() != "POST") {
                $this->response('', 406);
            }          

            $numberCardSale= $this->_request['numberCardSale']; 
                       
            if (!isset($numberCardSale)){
                            $$this->response(array('status' => "Failed", "msg" => "INSERT ID SALE"), 409);
            }
            
            $sales = Sales::getSaleInformationbyCardNumber($this->_db,$numberCardSale);

            if ($sales) {
                
                $salesSet = array();
                $temp_Array = array();

                foreach ($sales as $s) {
                                         $temp_Array['emissionDateSal'] = utf8_encode($s['emission_date_sal']);
                                        $temp_Array['beginDateSal'] = utf8_encode($s['begin_date_sal']);                                        
                                        $temp_Array['endDateSal'] = utf8_encode($s['end_date_sal']);                                      
                                        $temp_Array['cardNumberSal'] = $s['card_number_sal'];
                                        $temp_Array['serialPxc']= $s['serial_pxc'];
                                        $temp_Array['namePro'] = utf8_encode($s['name_pbl']);
                                        $temp_Array['statusInvoice'] = $s['statusInvoice'];
                                        $temp_Array['numberInv'] = $s['number_inv'];
                                        $temp_Array['serialDea'] = $s['serial_dea'];
                                        $temp_Array['nameDea']=utf8_encode($s['name_dea']);
                                        $temp_Array['nameUser'] = utf8_encode( $s['nameUser']);
                                        $temp_Array['responsable'] = $s['nameResp'];
                                        $temp_Array['statusPayment'] = $s['statusPayment']; 
                                        
                    array_push($salesSet, $temp_Array);
                }
                $this->response($salesSet, 200);
            }

            $this->response(array('status' => "Failed", "msg" => "NO SALES"), 409);            
                        
        }    
        
	}
	
	// Initiiate Library
	$api = new API;
	$api->processApi();
?>