<?php
/**
 * File: MyBC
 * Author: Valeria Andino
 * Creation Date: 18-ene-2016
 * Last Modified:  

 */
//Produccion Pruebas
//ini_set('include_path',ini_get('include_path').':/home/blueorgo/public_html/lib/:/home/blueorgo/public_html/lib/clases:/home/blueorgo/public_html/:/home/blueorgo/public_html/lib/smarty-2.6.22:/home/blueorgo/public_html/lib/Excel/Classes/:/home/blueorgo/public_html/lib/PDF/');
//define('DOCUMENT_ROOT', '/home/blueorgo/public_html/lib/config.inc.php');
//require_once(DOCUMENT_ROOT.'lib/config.inc.php');
//XAMPP

ini_set('include_path', ini_get('include_path') . ':/Applications/XAMPP/htdocs/RIMAC/lib/:/Applications/XAMPP/htdocs/RIMAC/lib/clases:/Applications/XAMPP/htdocs/RIMAC:/Applications/XAMPP/htdocs/RIMAC/lib/smarty-2.6.22');
define('DOCUMENT_ROOT', '/Applications/XAMPP/htdocs/RIMAC/');
require_once(DOCUMENT_ROOT . 'lib/config.inc.php');

class API extends Rest {

	private $ProductTypes = array('PRODUCT', 'LEGAL_ENTITY');

	public function __construct() {
		parent::__construct();	// Init parent contructor
	}

	public function processApi() {
		$func = strtolower(trim(str_replace("/", "", $_REQUEST['rquest'])));
		if ((int) method_exists($this, $func) > 0)
			$this->$func();
		else
			$this->response('', 404);	// If the method not exist with in this class, response would be "Page not found".
	}

	/*	 * ****************************VA
	 * @name: WSGetFavorites
	 * This WS get favorites by customer used document. Used when customer select load my favorites

	 */

	public function WSGetFavorites() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$useSerial = $this->_request['useSerial'];
		$customer_Id = $this->_request['id_Customer'];
		$customerSerial = $this->_request['serialCustomer'];

		$customerHolder = new Customer($this->_db);

		if ($useSerial == 'TRUE') {
			$customerHolder->setserial_cus($customerSerial);
		} else {
			$customerHolder->setDocument_cus($customer_Id);
		}

		if (!$customerHolder->getData()) {
			$this->response(array('status' => "Failed", "msg" => "INSERT DATA CUSTOMER"), 409);
		}

		$serialHolder = $customerHolder->getSerial_cus();
		$favorites = FavoritesTravel::getFavoritesByCustomer($this->_db, $serialHolder);

		if ($favorites) {
			$favoritesSet = array();
			$temp_Array = array();

			foreach ($favorites as $c) {
				$temp_Array['serialHoler'] = $serialHolder;
				$temp_Array['serialFavorite'] = $c['serial_cus'];
				$temp_Array['document'] = $c['document_cus'];
				$temp_Array['firstName'] = utf8_encode($c['first_name_cus']);
				$temp_Array['lastName'] = utf8_encode($c['last_name_cus']);
				$temp_Array['birthdate_cus'] = ($c['birthdate_cus']);
				$temp_Array['address_cus'] = utf8_encode($c['address_cus']);
				$temp_Array['name_cou'] = utf8_encode($c['name_cou']);
				$temp_Array['name_cit'] = utf8_encode($c['name_cit']);
				$temp_Array['phone1_cus'] = ($c['phone1_cus']);
				$temp_Array['cellphone_cus'] = ($c['cellphone_cus']);
				$temp_Array['email_cus'] = utf8_encode($c['email_cus']);
				$temp_Array['relative_cus'] = utf8_encode($c['relative_cus']);
				$temp_Array['relative_phone_cus'] = $c['relative_phone_cus'];
				$temp_Array['relative_fav'] = utf8_encode($c['relative_fav']);

				array_push($favoritesSet, $temp_Array);
			}

			$this->response($favoritesSet, 200);
		}

		$this->response(array('status' => "Failed", "msg" => "NO FAVORITE"), 200);
	}

	/*	 * ****************************VA
	 * @name: WSSalesByCustomer
	 * This WS get sales by customer, used document. Used in my bluecard. Shopping history and generate link by pdf. 

	 */

	public function WSSalesByCustomer() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$useSerial = $this->_request['useSerial'];
		$customerId = $this->_request['id_Customer'];
		$customerSerial = $this->_request['serialCustomer'];
		$restrictList = $this->_request['restrictList'];

		$customerHolder = new Customer($this->_db);

		if ($useSerial == 'TRUE') {
			$customerHolder->setserial_cus($customerSerial);
		} else {
			$customerHolder->setDocument_cus($customerId);
		}

		if (!$customerHolder->getData()) {
			$this->response(array('status' => "Failed", "msg" => "INSERT DOCUMENT CUSTOMER"), 409);
		}

		$sales = Sales::getSalesByCustomer($this->_db, $customerHolder->getSerial_cus(), $restrictList);
		global $global_salesStatus;

		if ($sales) {
			$SalesSet = array();
			$temp_Array = array();

			foreach ($sales as $s) {
				$temp_Array['NoContrato'] = $s['card_number_sal'];
				$temp_Array['indate'] = utf8_encode($s['in_date_sal']);
				$temp_Array['nameproduct'] = utf8_encode($s['name_pbl']);
				$temp_Array['serialCou'] = $s['serial_cou'];
				$temp_Array['serialCit'] = $s['serial_cit'];
				$temp_Array['namecou'] = utf8_encode($s['name_cou']);
				$temp_Array['begindate'] = $s['begin_date_sal'];
				$temp_Array['enddate'] = $s['end_date_sal'];
				$temp_Array['status'] = utf8_encode($s['status_sal']);
				$temp_Array['document'] = $s['document'];
				$serial_mbc = $s['serial_mbc'];
				$temp_Array['serial_mbc'] = $serial_mbc;
				$erp_bodega = ERPConnectionFunctions::getStoreID($serial_mbc);
				$erp_ptoEmision = ERPConnectionFunctions::getDepartmentID($serial_mbc);
				$cadena = "$erp_bodega-$erp_ptoEmision-" . str_pad($s['number_inv'], 9, "0", STR_PAD_LEFT) . "";
				$cadena = str_replace('-', '', $cadena);
				$temp_Array['nInvoice'] = substr($cadena, 2, 13);
				$temp_Array['idSale'] = $s['serial_sal'];
				array_push($SalesSet, $temp_Array);
			}

			$this->response($SalesSet, 200);
		}

		$this->response(array('status' => "Failed", "msg" => "NO SALE"), 200);
	}

	/*	 * ****************************VA
	 * @name: WSDataPdfBySale
	 * This WS get data to create PDF, used serial sal and language. Used by create pdf. 

	 */

	public function WSDataPdfBySale() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$sale_Id = $this->_request['id_Sale'];
		$language = $this->_request['language'];

		if (!isset($sale_Id)) {
			$$this->response(array('status' => "Failed", "msg" => "INSERT ID SALE"), 409);
		}

		$dataExt = array();
		$sales = Sales::getDataPdfBySale($this->_db, $sale_Id);
		$sale = new Sales($this->_db);
		$sale->setSerial_sal($sale_Id);
		$dataBen = $sale->getSaleBenefitsWS($language);
		$dataServ = $sale->getSaleServicesWS($language);
		$dataExt = Extras::getExtrasBySale($this->_db, $sale_Id);
		$travelLog = new TravelerLog($this->_db);
		$travelLog->setSerial_sal($sale_Id);
		$travelBySale = $travelLog->getTravelLogBySale();
		
		if($dataBen){
			foreach($dataBen as &$benefit){
				$benefit['descriptionbbl'] = utf8_decode($benefit['descriptionbbl']);
			}
		}
		
		if($dataServ){
			foreach($dataServ as &$service){
				$service['namesbl'] = utf8_decode($service['namesbl']);
			}
		}else{
			$dataServ = NULL;
		}

		if ($sales) {
			$salesSet = array();
			$temp_Array = array();

			foreach ($sales as $s) {
				$temp_Array['cityDate'] = $s['citDea'];
				$temp_Array['indate'] = $s['in_date_sal'];
				$temp_Array['noContrato'] = utf8_encode($s['card_number_sal']);
				$temp_Array['codSuc'] = utf8_encode($s['code']);
				$temp_Array['firstName'] = utf8_encode($s['first_name_cus']);
				$temp_Array['lastName'] = utf8_encode($s['last_name_cus']);
				$temp_Array['serialCitCus'] = $s['serialCitCus'];
				$temp_Array['nameCitCus'] = utf8_encode($s['nameCitCus']);
				$temp_Array['serialCouCus'] = $s['serialCouCus'];
				$temp_Array['nameCouCus'] = utf8_encode($s['nameCouCus']);
				$temp_Array['address'] = utf8_encode($s['address_cus']);
				$temp_Array['document'] = utf8_encode($s['document_cus']);
				$temp_Array['birthdate'] = utf8_encode($s['birthdate_cus']);
				$temp_Array['phone'] = $s['phone1_cus'];
				$temp_Array['email'] = utf8_encode($s['email_cus']);
				$temp_Array['nameRelative'] = utf8_encode($s['relative_cus']);
				$temp_Array['phoneRelative'] = $s['relative_phone_cus'];
				$temp_Array['serialComercializador'] = $s['serialComercializador'];
				$temp_Array['nameComercializador'] = utf8_encode($s['nameComercializador']);
				$temp_Array['serialCounter'] = $s['serialCounter'];
				$temp_Array['nameCounter'] = utf8_encode($s['nameCounter']);
				$temp_Array['nameProduct'] = utf8_encode($s['name_pbl']);
				$temp_Array['serialCouPro'] = utf8_encode($s['serial_cou']);
				$temp_Array['nameCouPro'] = utf8_encode($s['name_cou']);
				$temp_Array['rate'] = $s['fee_sal'];
				$temp_Array['totalRate'] = $s['total_sal'];
				$temp_Array['nameProduct'] = utf8_encode($s['name_pbl']);
				$temp_Array['adendum'] = USE_NEW_CONTRACT;
				$temp_Array['instructionsUse'] = 'Para facilitar el uso del servicio de Asistencia de Viajes'
						. ', usted deberá comunicarse SIEMPRE con nuestra Central de Asistencia, a travéz de los n&úmeros telefónicos,'
						. 'correo electrónico o skype. Si la urgencia de la asistencia, le impide comunicarse de forma inmediata y previo el uso de cualquier'
						. 'tipo de servicio, usted deberá  aviso en nuestra Central e Asistencias dentro de las 24 horas siguientes a la ocurrencia'
						. 'del evento.';
				$temp_Array['generalConditions'] = 'El Cliente reconoce y acepta darse por enterado de todas las clausulas detalladas en las condiciones Generales BLUE CARD, las mismas que han sido entregadas físicamente, enviadas mediante correo electrónico o también puede ser descargada desde la página web www.bluecard.com.ec El presente Contrato, se regirá bajo las Condiciones Generales y Particulares de BLUE CARD vigentes al momento de su emisión, independientemente de que sean anuladas, modificadas o agregadas.'
						. 'Puede descargarse las condiciones generales en los siguientes Links:'
						. 'Condiciones Generales Planes Tradicionales: documents.bluecard.com.ec/conditions/es/cgtradicionales.pdf'
						. 'Condiciones Generales Planes Elite: documents.bluecard.com.ec/conditions/es/cgelite.pdf';
				$temp_Array['exclusions'] = 'Exclusiones según condiciones particulares';
				$temp_Array['productDescription'] = utf8_encode($s['description_pbl']);
				$temp_Array['particularConditions'] = utf8_encode('');
				$temp_Array['benefits'] = $dataBen;
				$temp_Array['benefits2'] = $dataServ;

				// get extras information by sale                
				if ($dataExt) {
					$extrasSet = array();
					$tempExtras = array();
					foreach ($dataExt as $e) {
						$tempExtras['serialExt'] = $e['serial_ext'];
						$tempExtras['serialCus'] = $e['serial_cus'];
						$tempExtras['documentCus'] = $e['document_cus'];
						$tempExtras['fee'] = utf8_encode($e['fee_ext']);
						$tempExtras['firstNameCus'] = utf8_encode($e['first_name_cus']);
						$tempExtras['lastNameCus'] = utf8_encode($e['last_name_cus']);
						$tempExtras['bithdate'] = utf8_encode($e['birthdate_cus']);
						array_push($extrasSet, $tempExtras);
						//print_r($tempExtras);   
					}
					// die();  
				}
				$temp_Array['extras'] = $extrasSet;
				//end extras                
				// get travel log by sale                
				if ($travelBySale) {
					$travelSet = array();
					$tempTravel = array();
					foreach ($travelBySale as $t) {
						$tempTravel['nameCus'] = utf8_encode($t['customer_name']);
						$tempTravel['country'] = utf8_encode($t ['name_cou']);
						$tempTravel['startTrl'] = $t ['start_trl'];
						$tempTravel['endTrl'] = $t ['end_trl'];
						$tempTravel['daysTrl'] = $t['days_trl'];
						$tempTravel['serialTrl'] = $t['serial_trl'];
						$tempTravel['serialUsr'] = $t['serial_usr'];
						$tempTravel['nameUsr'] = $t['user_name'];
						array_push($travelSet, $tempTravel);
					}
				}
				$temp_Array['travelLog'] = $travelSet;
				//end travel log
				array_push($salesSet, $temp_Array);
			}
			$this->response($salesSet, 200);
		}

		$this->response(array('status' => "Failed", "msg" => "NO DATA BY SALE"), 409);
	}

	/*	 * ****************************VA
	 * @name: WSRegisterTravelByCustomer
	 * This WS get travel by registered travel, used document . Used in My bluecard Travel log

	 */

	public function WSRegisterTravelByCustomer() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$useSerial = $this->_request['useSerial'];
		$customerId = $this->_request['id_Customer'];
		$customerSerial = $this->_request['serialCustomer'];

		$customerHolder = new Customer($this->_db);

		if ($useSerial == 'TRUE') {
			$customerHolder->setserial_cus($customerSerial);
		} else {
			$customerHolder->setDocument_cus($customerId);
		}

		if (!$customerHolder->getData()) {
			$this->response(array('status' => "Failed", "msg" => "INSERT  CUSTOMER"), 409);
		}
		$sales = Sales::getRegisterTravelByCustomer($this->_db, $customerHolder->getSerial_cus());

		if ($sales) {
			$sales_Set = array();
			$temp_Array = array();

			foreach ($sales as $s) {
				$temp_Array['serialSale'] = $s['serial_sal'];
				$temp_Array['noContrato'] = $s['card_number_sal'];
				$temp_Array['indate'] = $s['in_date_sal'];
				$temp_Array['nameproduct'] = utf8_encode($s['name_pbl']);
				$temp_Array['serialCountry'] = $s['serial_cou'];
				$temp_Array['namecou'] = utf8_encode($s['name_cou']);
				$temp_Array['begindate'] = $s['begin_date_sal'];
				$temp_Array['enddate'] = $s['end_date_sal'];
				$temp_Array['dayssale'] = $s['days_sal'];
				$temp_Array['daysfree'] = ($s['days']);
				array_push($sales_Set, $temp_Array);
			}
			$this->response($sales_Set, 200);
		}
		$this->response(array('status' => "Failed", "msg" => "NO EXIST DATA"), 200);
	}

	/*	 * ****************************VA
	 * @name: WSTravelsBySale
	 * This WS get travel registers by sale, used serial sal. Used in my bluecard Travel log

	 */

	public function WSTravelsBySale() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$sale_Id = $this->_request['id_Sale'];

		if (!isset($sale_Id)) {
			$this->response(array('status' => "Failed", "msg" => "INSERT ID SALE"), 409);
		}
		$sales = Sales::getTravelsBySale($this->_db, $sale_Id);

		if ($sales) {
			//print_r($sales); die();
			$salesSet = array();
			$temp_Array = array();

			foreach ($sales as $s) {
				$temp_Array['noContrato'] = utf8_encode($s['card_number_sal']);
				$temp_Array['firstName'] = utf8_encode($s['first_name_cus']);
				$temp_Array['lastName'] = utf8_encode($s['last_name_cus']);
				$temp_Array['country'] = utf8_encode($s['name_cou']);
				$temp_Array['startdate'] = $s['start_trl'];
				$temp_Array['enddate'] = $s['end_trl'];
				$temp_Array['days'] = $s['days_trl'];

				array_push($salesSet, $temp_Array);
			}
			$this->response($salesSet, 200);
		}

		$this->response(array('status' => "Failed", "msg" => "NO SALE"), 200);
	}

	/*	 * ****************************VA
	 * @name: WSNewFavorite
	 * This WS insert new favorite by customer, used document. Used when customer check new favorite in quotation
	 * and insert new favorite in my bluecard.    

	 */

	public function WSNewFavorite() {
		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$useSerial = $this->_request['useSerial'];
		$document_id = $this->_request['documentHolder_id'];
		$customerSerial = $this->_request['serialCustomer'];
		$documentF_id = $this->_request['documentFavorite_id'];
		$customerSerialFav = $this->_request['serialFavorite'];

		$customerHolder = new Customer($this->_db);
		$customerFavorite = new Customer($this->_db);

		if ($useSerial == 'TRUE') {
			$customerHolder->setserial_cus($customerSerial);
			$customerFavorite->setserial_cus($customerSerialFav);
		} else {
			$customerHolder->setDocument_cus($document_id);
			$customerFavorite->setDocument_cus($documentF_id);
		}

		if (!$customerHolder->getData()) {
			$this->response(array('status' => "Incorrect", "msg" => "CUSTOMER HOLDER NOT EXIST"), 409);
		}

		$minumDataCode = $this->minimumDataRequiredFavorite($this->_request);
		if ($minumDataCode != 200) {
			$this->response('', $minumDataCode);
		}
		$customerFavorite->getData();
		$favoriteSerial = $customerFavorite->getSerial_cus();
		$customer = New Customer($this->_db, $favoriteSerial);
		$customer->setSerial_cit($this->cleanField(utf8_encode($this->_request['city'])));
		$customer->setType_cus('PERSON');
		$customer->setDocument_cus($this->cleanField(utf8_encode($this->_request['idNumber'])));
		$customer->setFirstname_cus($this->cleanField(utf8_encode($this->_request['name'])));
		$customer->setLastname_cus($this->cleanField(utf8_encode($this->_request['lastname'])));
		$customer->setBirthdate_cus($this->cleanField(utf8_encode($this->_request['birthdate'])));
		$customer->setPhone1_cus($this->cleanField(utf8_encode($this->_request['phone1'])));
		$customer->setPhone2_cus($this->cleanField(utf8_encode($this->_request['phone2'])));
		$customer->setCellphone_cus($this->cleanField(utf8_encode($this->_request['cellphone'])));
		$customer->setEmail_cus($this->cleanField(utf8_encode($this->_request['email'])));
		$customer->setAddress_cus($this->cleanField(utf8_encode($this->_request['address'])));
		$customer->setRelative_cus($this->cleanField(utf8_decode($this->_request['relative_cus'])));
		$customer->setRelative_phone_cus($this->cleanField(utf8_encode($this->_request['phonerelative'])));
		$customer->setVip_cus('0');
		$customer->setFirst_access_cus('0');
		$customer->setStatus_cus('ACTIVE');
		$customer->setPassword_cus(md5(GlobalFunctions::generatePassword(6, 8, false)));

		if ($favoriteSerial) {
			if (!$customer->update()):
				$this->response(array('status' => "Incorrect", "msg" => "NO CUSTOMER UPDATE"), 409);
			endif;
		}else {
			if (!$customer->insert()):
				$this->response(array('status' => "Incorrect", "msg" => "NO CUSTOMER INSERT"), 409);
			endif;
		}

		$newfavorite = new FavoritesTravel($this->_db);
		$newfavorite->setSerial_cus_fav($customer->getSerial_cus());
		$newfavorite->getData();

		if (FavoritesTravel::getSerialCustomerFavoriteByHolder($this->_db, $customerHolder->getSerial_cus(), $customer->getSerial_cus())) {
			$this->response(array('status' => "WARNING", "msg" => "FAVORITE EXIST"), 200);
		}

		$newfavorite->setSerial_main_cus($customerHolder->getSerial_cus());
		$newfavorite->setStatus_fav('ACTIVE');
		$newfavorite->setRelative_fav($this->_request['relative']);
		$serialfav = $newfavorite->insert();

		if ($serialfav) {
			$this->response(array('status' => "Correct", "msg" => "INSERTED CORRECT", "idFavorite" => $serialfav, "idHolder" => $customerHolder->getSerial_cus()), 200);
		} else {
			$this->response(array('status' => "Incorrect", "msg" => "NO INSERT FAVORITE"), 409);
		}
	}

	/*	 * ****************************VA
	 * @name: WSNewResgisterTravelLog
	 * This WS insert new register travel, used document, serial sal and new information. Used in my bluecard option Travel Log   

	 */

	public function WSNewResgisterTravelLog() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$useSerial = $this->_request['useSerial'];
		$customerSerial = $this->_request['serialCustomer'];
		$document_id = $this->_request['document_id'];
		$serialSale = $this->_request['sale_id'];
		$begin_date = $this->_request['begin_date'];
		$end_date = $this->_request['end_date'];
		$days = $this->_request['days'];
		$counter = $this->_request['counter'];
		$serialCit = $this->_request['city_id'];

		$minumDataCode = $this->minimumDataRequired($this->_request);
		if ($minumDataCode != 200) {
			$this->response('', $minumDataCode);
		}

		$customerHolder = new Customer($this->_db);

		if ($useSerial == 'TRUE') {
			$customerHolder->setserial_cus($customerSerial);
		} else {
			$customerHolder->setDocument_cus($document_id);
		}

		if (!$customerHolder->getData()) {
			$this->response(array('status' => "Incorrect", "msg" => "CUSTOMER NOT EXIST"), 409);
		}

		$serialCustomer = $customerHolder->getSerial_cus();
		$serialUser = Counter::getUserByCounter($this->_db, $counter);

		if ($serialCustomer) {
			$travelerLog = new TravelerLog($this->_db);
			$travelerLog->setSerial_cus($serialCustomer);
			$travelerLog->setSerial_sal($serialSale);
			$travelerLog->setCard_number_trl(NULL);
			$travelerLog->setStart_trl($begin_date);
			$travelerLog->setEnd_trl($end_date);
			$travelerLog->setDays_trl($days);
			$travelerLog->setSerial_cnt($counter);
			$travelerLog->setSerial_cit($serialCit);
			$travelerLog->setSerial_usr($serialUser);
			$travelerID = $travelerLog->insert();
			if (!$travelerID)
				$this->response(array('status' => "Incorrect", "msg" => "NO INSERT"), 409);
			else {
				$this->response(array('status' => "Correct", "msg" => "INSERT CORRECT", "idTravel" => $travelerID), 200);
			}
		} else {
			$this->response(array('status' => "Incorrect", "msg" => "CUSTOMER NOT EXIST"), 409);
		}
	}

	/*	 * ****************************VA
	 * @name: WSInactiveFavorite
	 * This WS inactive favorite. Used when customer delete favorite in my bluecard.                
	 */

	public function WSInactiveFavorite() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$serial_fav = $this->_request['serialFav'];

		if (FavoritesTravel::inactiveFavorite($this->_db, $serial_fav)) {
			$this->response(array('status' => "Correct", "msg" => " DELETE FAVORITE "), 200);
		} else {
			$this->response(array('status' => "Incorrect", "msg" => "NO DELETE FAVORITE "), 200);
		}
	}

	/*	 * ****************************VA
	 * @name: WSReturnQuestions
	 * This WS return quoestions by product Senior 
	 */

	public function WSReturnQuestions() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$language = $this->_request['language'];

		$questionBL = new QuestionByLanguage($this->_db);
		$questionBL->setSerial_lang($language);
		$question = $questionBL->getQuestionsByLanguage();

		if ($question) {

			$questionSet = array();
			$temp_Array = array();

			foreach ($question as $q) {
				$temp_Array['Question'] = utf8_encode($q['text_qbl']);
				$temp_Array['seriaQue'] = $q['serial_qbl'];

				array_push($questionSet, $temp_Array);
			}

			$this->response($questionSet, 200);
		}

		$this->response(array('status' => "Failed", "msg" => "NO QUESTIONS"), 200);
	}

	/*	 * ******************************VA
	 * @name: WSChangeTravelDate
	 * This WS change travel date. Used in my bluecard, option 
	 */

	public function WSChangeTravel() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$sale_Card = $this->_request['noContrato'];
		$serialUsr = $this->_request['serialUsr'];
		$statusNew = $this->_request['status'];
		$beginDateNew = $this->_request['beginDate'];
		$endDateNew = $this->_request['endDate'];
		$serialCitNew = $this->_request['idCit'];
		$days = $this->_request['totalDays'];
		$typeChange = $this->_request['typeChange'];


		$sale_Id = Sales::getSerialSalByCardNumber($this->_db, $sale_Card);
		$sale = new Sales($this->_db);
		$sale->setSerial_sal($sale_Id);
		$sale->getData();
		$beginDateOld = $sale->getBeginDate_sal();
		$endDateOld = $sale->getEndDate_sal();
		$inDateOld = $sale->getInDate_sal();
		$serialCitOld = $sale->getSerial_cit();
		$statusOld = $sale->getStatus_sal();

		if ($statusOld == 'REGISTERED' || $statusOld == 'ACTIVE' || $statusOld == 'STANDBY') {

			$saleChange = Sales::updateDateByCardNumber($this->_db, $sale_Id, $beginDateNew, $endDateNew, $serialCitNew, $statusNew, $days);

			if ($saleChange = true) {

				$slg = new SalesLog($this->_db);
				$slg->setType_slg('AUTHORIZED');
				$slg->setSerial_usr($serialUsr);
				$slg->setSerial_sal($sale_Id);
				if ($typeChange == 1) {
					$misc = array(
						'begin_date_salOld' => $beginDateOld,
						'end_date_salOld' => $endDateOld,
						'in_date_salOld' => $inDateOld,
						'begin_date_salNew' => $beginDateNew,
						'end_date_salNew' => $endDateNew,
						'serial_citOld' => $serialCitOld,
						'serial_citNew' => $serialCitNew,
						'Description' => 'Change date of customer');
				} else {
					$misc = array('old_status_sal' => $statusOld,
						'new_status_sal' => $statusNew,
						'Description' => 'Change status of customer ');
				}
				$misc['global_info']['requester_usr'] = $serialUsr;
				$misc['global_info']['request_date'] = date('d/m/Y');

				$slg->setDescription_slg(serialize($misc));
				$slg->setStatus_slg('AUTHORIZED');

				if ($slg->insert()) {
					$this->response(array('status' => "Correct", "msg" => "CHANGE CORRECT"), 200);
				} else {
					$this->response(array('status' => "Failed", "msg" => "NO CHANGE"), 200);
				}
			}
		} else {
			$this->response(array('status' => "Incorrect", "msg" => "STATUS NO PERMIT CHANGE"), 200);
		}
	}

	/*	 * VA
	 * @name minimumDataRequired
	 * @param array $data
	 * @return boolean 
	 */

	private function minimumDataRequired($data) {
		$case_code = 200;

		if (!is_array($data)) {
			$case_code = 650;
		} elseif (!isset($data['document_id'])) {
			$case_code = 651;
		} elseif (!isset($data['sale_id'])) {
			$case_code = 652;
		} elseif (!isset($data['begin_date'])) {
			$case_code = 653;
		} elseif (!isset($data['end_date'])) {
			$case_code = 654;
		} elseif (!isset($data['days'])) {
			$case_code = 655;
		} elseif (!isset($data['counter'])) {
			$case_code = 656;
		} elseif (!isset($data['city_id'])) {
			$case_code = 657;
		}

		return $case_code;
	}

	private function minimumDataRequiredFavorite($data) {
		$case_code = 200;

		if (!is_array($data)) {
			$case_code = 650;
		} elseif (!isset($data['city'])) {
			$case_code = 651;
		} elseif (!isset($data['idNumber'])) {
			$case_code = 653;
		} elseif (!isset($data['name'])) {
			$case_code = 654;
		} elseif (!isset($data['lastname'])) {
			$case_code = 655;
		} elseif (!isset($data['birthdate'])) {
			$case_code = 656;
		} elseif (!isset($data['phone1'])) {
			$case_code = 657;
		} elseif (!isset($data['email'])) {
			$case_code = 659;
		} elseif (!isset($data['relative_cus'])) {
			$case_code = 660;
		} elseif (!isset($data['phonerelative'])) {
			$case_code = 661;
		} elseif (!isset($data['address'])) {
			$case_code = 662;
		}

		return $case_code;
	}

}

// Initiiate Library
$api = new API;
$api->processApi();
?>
