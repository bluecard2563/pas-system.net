<?php

/**
 * File: account
 * Author: Patricio Astudillo
 * Creation Date: 17-dic-2012
 * Last Modified: 01-jun-2016
 * Modified By: Valeria Andino
 */
//	ini_set('include_path',ini_get('include_path').':/home/blueorgo/public_html/lib/:/home/blueorgo/public_html/lib/clases:/home/blueorgo/public_html/:/home/blueorgo/public_html/lib/smarty-2.6.22:/home/blueorgo/public_html/lib/Excel/Classes/:/home/blueorgo/public_html/lib/PDF/');
//	define('DOCUMENT_ROOT', '/home/blueorgo/public_html/lib/config.inc.php');

ini_set('include_path', ini_get('include_path') . "C:/xampp/htdocs/PLANETASSIST/lib/;C:/xampp/htdocs/PLANETASSIST/lib/clases/;C:/xampp/htdocs/PLANETASSIST/;C:/xampp/htdocs/PLANETASSIST/lib/smarty-2.6.22/;C:/xampp/htdocs/PLANETASSIST/lib/PDF");
define('DOCUMENT_ROOT', "C:/xampp/htdocs/PLANETASSIST");
require_once(DOCUMENT_ROOT . 'lib/config.inc.php');
include_once DOCUMENT_ROOT . 'lib/erpLibrary.inc.php';

class API extends Rest {

    private $varBoolean = array('YES', 'NO');

    public function __construct() {
        parent::__construct();    // Init parent contructor
    }

    public function processApi() {
        $func = strtolower(trim(str_replace("/", "", $_REQUEST['rquest'])));
        if ((int) method_exists($this, $func) > 0)
            $this->$func();
        else
            $this->response('', 404);    // If the method not exist with in this class, response would be "Page not found".
    }

    //************************* PROCESS FUNCTIONS ********************
    /**
     * @access public
     * @author Patricio Astudillo <pastudillo@planet-assist.net>
     * @return Array Country List for Destinations/Origin
     */
    public function retrieveCountryList() {
       
       /* if(!$this->validate_login_attempt()){
        $this->response('', 418);
        }*/

        $countries = WSAccountsLib::getBaseCountries($this->_db);
        $countryData = array();
        $temp_array = array();

        foreach ($countries as $c) {
            $temp_array['ID'] = utf8_encode($c['serial_cou']);
            $temp_array['country'] = utf8_encode($c['name_cou']);
            $temp_array['zone'] = utf8_encode($c['name_zon']);

            array_push($countryData, $temp_array);
        }

        if ($countries) {
            $this->response($countryData, 200);
        }

        $this->response(array('status' => "Failed", "msg" => "NO PHONES"), 400);
    }

    /**
     * @access public
     * @author Patricio Astudillo <pastudillo@planet-assist.net>
     * @return Array City List for Destinations/Origin
     */
    public function retrieveCityListForCountry() {
        
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        /*if (!$this->validate_login_attempt()) {
            $this->response('Error when attempting to login', 418);
        }*/

        $cities = WSAccountsLib::getCitiesForCountry($this->_db, $this->_request['country']);

        if ($cities) {
            $citySet = array();
            $temp_array = array();

            foreach ($cities as $c) {
                $temp_array['ID'] = $c['serial_cit'];
                $temp_array['city'] = utf8_encode($c['name_cit']);

                array_push($citySet, $temp_array);
            }

            $this->response($citySet, 200);
        }

        $this->response(array('status' => "Failed", "msg" => "NO CITIES"), 409);
    }

    /**
     * @access public
     * @author Patricio Astudillo <pastudillo@planet-assist.net>
     * @param String $serial_pxc Product By Country ID
     * @param String $selDeparture Departing Country ID
     * @param String $selDestination Departing Country ID
     * @param String $checkPartner Departing Country ID
     * @param String $txtBeginDate Departing Country ID
     * @param String $txtEndDate Departing Country ID
     * @param String $targetDays Total travel days
     * @param String $txtOverAge Qty for adults in trip. Main passenger included.
     * @param String $txtUnderAge Qty for under age companions
     * @param String $selTripDays For Multi-Travel products, target days
     * @return Array Data set for Quote
     */
    public function get_travel_quote() {

        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        if (!$this->validate_login_attempt()) {
            $this->response('', 418);
        }

        $data_set = $this->_request;
        $singleProduct = array(
            'serial_pxc' => $data_set['serial_pxc'],
            'name_pbl' => $data_set['name_pbl'],
            'departure' => $data_set['selDeparture'],
            'destination' => $data_set['selDestination'],
            'spouseTraveling' => ($data_set['checkPartner'] == 'YES') ? true : false,
            'beginDate' => $data_set['txtBeginDate'],
            'endDate' => $data_set['txtEndDate'],
            'days' => $data_set['targetDays'],
            'adults' => $data_set['txtOverAge'],
            'children' => $data_set['txtUnderAge'],
        );

        if ($data_set['selTripDays'] == 'null') {
            $selTripDays = false;
        }

        if (GlobalFunctions::assignProductCalculatedPrices($this->_db, $singleProduct, $selTripDays, $data_set['extrasRestricted']) === 'NO_PRICES') {
            $this->response('', 800);
        }

        $response_data = array();
        $response_data['holder_fee'] = $singleProduct['price_pod'];
        $response_data['children_fee'] = $singleProduct['price_children_pod'];
        $response_data['spouse_fee'] = $singleProduct['price_spouse_pod'];
        $response_data['extra_fee'] = $singleProduct['price_extra_pod'];

        $response_data['cards_qty'] = $singleProduct['cards_pod'];
        $response_data['total_sale'] = $singleProduct['total_price_pod'];
        $response_data['days'] = $singleProduct['days'];

        $this->response($response_data, 200);
    }

    /**
     * @access public
     * @author Patricio Astudillo <pastudillo@planet-assist.net>
     * @param String $language Language ID
     * @return Array Product List in given Language
     */
    public function retrieve_product_list() {
        
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        if (!$this->validate_login_attempt()) {
            $this->response('', 418);
        }

        if (isset($this->_request['full_data'])) {
            $full_data = $this->_request['full_data'];
        }

        $counter = new Counter($this->_db);
        $serial_cnt = $counter->getCounters($this->_serial_usr);

        if (count($serial_cnt) == 1) {
            $counter = new Counter($this->_db, $serial_cnt['0']['serial_cnt']);

            if ($counter->getData()) {
                $branch = new Dealer($this->_db, $counter->getSerial_dea());

                if ($branch->getData()) {
                    $productListByDealer = WSAccountsLib::getProductForAccount($this->_db, $branch->getDea_serial_dea(), $full_data);

                    if ($productListByDealer) {
                        $this->response($productListByDealer, 200);
                    } else {
                        $this->response('', 850);
                    }
                } else {
                    $this->response('', 853);
                }
            } else {
                $this->response('', 851);
            }
        } else {
            $this->response('', 852);
        }
    }

    /*     * ****************************VA
     * @name: WSSaleReserved
     * dont use
     */

    public function WSSaleReserved() {

         if (!$this->validate_login_attempt()) {
            $this->response('', 418);
        }
        
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }       

        $minumDataCode = $this->minimunDataRequiredNewSale($this->_request);
        if ($minumDataCode != 200) {
            $this->response('', $minumDataCode);
        }

        $validateDataCode = $this->validateDataWSNewSale($this->_request);
        if ($validateDataCode != 200) {
            $this->response('', $validateDataCode);
        }

        $customerDocument = $this->_request['customerDocument'];
        $producbycountryId = $this->_request['prodxcountry_Id'];
        $countryId = $this->_request['country_Id'];
        $counterid = $this->_request['counter_Id'];
        $costWebSale = $this->_request['percentagecost'];
        $invId = NULL;
        $emissiondate = date('d/m/Y');
        $startdate = $this->_request['startDate'];
        $enddate = $this->_request['endDate'];
        $indate = date('d/m/Y');
        $days = $this->_request['days'];
        $observations = $this->_request['observations'];
        $total = $this->_request['price'];
        $cost = ($total * ($costWebSale / 100));
        $free = 'NO';
        $fee = $cost;
        $id_erp = null;
        $status = 'RESERVED';
        $type = 'WEB';
        $stock_type = 'VIRTUAL';
        $direct_sale = 'NO';
        $changeFeeSale = 1;
        $internationalfeestatus = 'TO_COLLECT';
        $internationalfeeused = 'NO';
        $internationalfeeamount = $cost; //costo                            
        $paypal_Id = $this->_request['paypal_Id'];


        $exist_customer = new Customer($this->_db);
        $producbydealId = Product::getProductByDealerByPBbC($this->_db, $producbycountryId, $counterid);
        // Get General city by country
        $cityId = City::getCityGeneralByCountry($this->_db, $countryId);
        //echo $producbydealId; die();
        if ($exist_customer->existsCustomer($customerDocument)) {
            $customerSerial = Customer::getSerialCustomerByDocument($this->_db, $customerDocument);
            $cardnumber = Stock::getNextStockNumber($this->_db, TRUE);
            
            // Insert new sale
            $newSale = New Sales($this->_db);
            $newSale->setSerial_cus($customerSerial);
            $newSale->setSerial_pbd($producbydealId);
            $newSale->setSerial_cnt($counterid);
            $newSale->setSerial_inv($invId);
            $newSale->setSerial_cit($cityId);
            $newSale->setCardNumber_sal($cardnumber);
            $newSale->setEmissionDate_sal($emissiondate);
            $newSale->setBeginDate_sal($startdate);
            $newSale->setEndDate_sal($enddate);
            $newSale->setInDate_sal($indate);
            $newSale->setDays_sal($days);
            $newSale->setFee_sal($fee);
            $newSale->setObservations_sal($observations);
            $newSale->setCost_sal($cost);
            $newSale->setFree_sal($free);
            $newSale->setIdErpSal_sal($id_erp);
            $newSale->setStatus_sal($status);
            $newSale->setType_sal($type);
            $newSale->setStockType_sal($stock_type);
            $newSale->setTotal_sal($total);
            $newSale->setTotalCost_sal($cost);
            $newSale->setInternationalFeeStatus_sal($internationalfeestatus);
            $newSale->setInternationalFeeUsed_sal($internationalfeeused);
            $newSale->setInternationalFeeAmount_sal($internationalfeeamount);
            $newSale->setDirectSale_sal($direct_sale);
            $newSale->setChange_fee_sal($changeFeeSale);
            $newSale->setTransaction_Id($paypal_Id);
            $serialSale = $newSale->insert();
            
        } else {
            $this->response(array('status' => "Failed", "msg" => "CUSTOMER NOT EXIST"), 409);
        }

        if ($serialSale) {
            $this->response(array('status' => "Correct", "msg" => "INSERTED SALE", "idSal" => $serialSale), 200);
        } else {
            $this->response(array('status' => "Failed", "msg" => "NO INSERT SALE"), 409);
        }
    }

    /*     * ****************************VA
     * @name: WSStatusPay
     * @return: Data status pay. received request id
     *
     */

   public function WSStatusPay() {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        $referencia=$this->_request['referencia'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://52.87.250.190/placeToPay/public/api/getPaymentRecords/".$referencia,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Postman-Token: ad44d741-7b34-486a-993a-4dbf458a7980"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);


        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            // $responseEdit = json_decode($response,true);
//        foreach ($responseEdit as $rs){
            //Create data for authentication in place to pay

            //@param $secretKey is trankey provided by place to pay
            $secretKey = 'pYvE8OGZNdDUMblg';
            //@param $login is provided by place to pay
            $login = '1f5ae3a4463468b5ad738f1f5ff38981';
            $seed = date('c');
            if (function_exists('random_bytes')) {
                $nonce = bin2hex(random_bytes(16));
            } elseif (function_exists('openssl_random_pseudo_bytes')) {
                $nonce = bin2hex(openssl_random_pseudo_bytes(16));
            } else {
                $nonce = mt_rand();
            }
            $nonceBase64 = base64_encode($nonce);
            $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

            $jsonAuth['auth'] = [
                "login" => $login,
                "seed" => $seed,
                "nonce" => $nonceBase64,
                "tranKey" => $tranKey
            ];

            $editedJson = json_encode($jsonAuth);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://checkout.placetopay.ec/api/session/" . $response,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $editedJson,
                CURLOPT_HTTPHEADER => array(
                    "Cache-Control: no-cache",
                    "Content-Type: application/json",
                    "Postman-Token: 16cd1e4b-c02b-4f23-a3a8-f907f2c3ef5f"
                ),
            ));
            $responses = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $this->response(json_decode($responses,true), 200);

            /*$responseEdit = json_decode($response,true);
            $reference= $responseEdit['request']['payment']['reference'];
            $status= $responseEdit['status']['status'];*/



        }

    }

      /*     * ****************************VA
    * @name: WSSavePreSale
    * @return: Data status pay. received request id
    *
    */

    public function WSSavePreSale() {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        $body_sale = json_decode($this->_request['body_sale']);
        $status_ptp= $this->_request['status_ptp'];
        $total_sal= $this->_request['total_sal'];
        $name_usr= $this->_request['name_usr'];
        $document_usr = $this->_request['document_usr'];
        $email_usr = $this->_request['email_usr'];
        $serial_cup = $this->_request['serial_cup'];

        $dataUser = Sales::getCustomerByDocumentPresale($this->_db,$document_usr);

       if ($dataUser){
           $serial_usr = $dataUser;
           $datausrPTP = Sales::getDataCustomerPTP($this->_db,$serial_usr);
           $misc['client'] = $datausrPTP[0]['name_usr'];
           $misc['user'] = $datausrPTP[0]['document_usr'];
           $misc['password'] = $datausrPTP[0]['password_usr'];
           $misc['email'] = $datausrPTP[0]['email_usr'];
           $misc['link'] = 'http://45.79.183.78/login';

           GlobalFunctions::sendMail($misc, 'newClient_ptp');
       } else {
          $serial_usr = Sales::insertWSUserPTP($this->_db,$document_usr, $name_usr, '1234',$email_usr);
           $misc['client'] =  $name_usr;
           $misc['user'] = $document_usr;
           $misc['password'] = '1234';
           $misc['email'] = $email_usr;
           $misc['link'] = 'http://45.79.183.78/login';

           GlobalFunctions::sendMail($misc, 'newClient_ptp');
       }

        if (is_array($body_sale) && count($body_sale) > 0) {
            foreach ($body_sale as $prod) {
                $prod = get_object_vars($prod);
            }
        }

        $reference = Sales::insertWSsatusSale($this->_db,serialize($prod),$status_ptp,$total_sal,$name_usr,$document_usr,$serial_usr,$serial_cup);
        $this->response(array($reference),200);
    }

    public function WSGetPreSale() {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }
        $reference = $this->_request['reference'];

        $dataPreSale= Sales::getDataPreSale($this->_db,$reference);
        $this->response(unserialize($dataPreSale[0][0]),200);
    }

     public function WSGetPreSaleSerialCup() {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }
        $reference = $this->_request['reference'];

        $dataPreSale1= Sales::getDataPreSaleSerialCup($this->_db,$reference);

        if ($dataPreSale1) {
            $cup_infoSet = array();
            $temp_Array = array();

            foreach ($dataPreSale1 as $c) {
                $temp_Array['serial_cup'] = $c['serial_cup'];
               
                array_push($cup_infoSet, $temp_Array);
            }
            $this->response($cup_infoSet, 200);
        } else {
            $this->response(array('status' => "Failed", "msg" => "NO EXIST CUPON"), 200);
        }

    }

    public function WSUpdatePreSale() {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }
        $reference = $this->_request['reference'];
        $status = $this->_request['status'];

        $dataUpdate = Sales::updatePresale($this->_db,$status,$reference);
        $this->response($dataUpdate);

    }

    public function wsLogin() {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        $document_usr = $this->_request['user'];
        $password_usr = $this->_request['password'];

        $responseLogin = Sales::getCustomerLogin($this->_db, $document_usr, $password_usr);

        if ($responseLogin) {
            $this->response(array($responseLogin),200);
        } else {
            $error = array('status' => "Failed", "msg" => "No Data");
            $this->response($error, 400);
        }
    }

    public function wsSerialUsr() {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        $document_usr = $this->_request['user'];

        $responseLogin = Sales::getCustomerSerialUsr($this->_db, $document_usr);

        if ($responseLogin) {
            $this->response(array($responseLogin),200);
        } else {
            $error = array('status' => "Failed", "msg" => "No Data");
            $this->response($error, 400);
        }
    }

    public function WSGetPreSaleUser() {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }
        $serialUsr = $this->_request['serialUsr'];

        $dataPreSaleUsr= Sales::getDataPreSalebyUser($this->_db,$serialUsr);
        $this->response($dataPreSaleUsr,200);

    }

     public function WSSerialCup(){
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }
        $db = $this->_db;
        $number_cup= $this->_request['number_cup'];
        $data = Cupon::getSerialCuponData($db, $number_cup);
        $promo_info = CustomerPromo::getPromoInfoForCupon($db, $data['serial_ccp']);
        if ($promo_info) {
            $promo_infoSet = array();
            $temp_Array = array();

            foreach ($promo_info as $c) {
                $temp_Array['begin_date_ccp'] = $c['begin_date_ccp'];
                $temp_Array['cupon_prefix_ccp'] = $c['cupon_prefix_ccp'];
                $temp_Array['discount_percentage_ccp'] = $c['discount_percentage_ccp'];
                $temp_Array['end_date_ccp'] = $c['end_date_ccp'];
                $temp_Array['name_ccp'] = $c['name_ccp'];
                $temp_Array['other_discount_ccp'] = $c['other_discount_ccp'];
                $temp_Array['payment_form_ccp'] = $c['payment_form_ccp'];
                $temp_Array['scope_ccp'] = $c['scope_ccp'];
                $temp_Array['serial_cup'] = $c['serial_cup'];
                array_push($promo_infoSet, $temp_Array);
            }
            $this->response($promo_infoSet, 200);
        } else {
            $this->response(array('status' => "Failed", "msg" => "NO EXIST CUPON"), 200);
        }
    }



    /*     * ****************************VA
     * @name: WSNewSale
     * @return: Data the new sale. Used in insert new sale. Recived json with information by sale
     * 
     */

    public function WSNewSale() {

      /* if (!$this->validate_login_attempt()) {
           $this->response('', 418);
        }*/
       
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }       
    
        $cartProds = json_decode($this->_request['cart_products_travelers_information']);
        $language = $this->_request['language'];
        $serial_cnt = $this->_request['idCounter'];

        $serial_cup= $this->_request['serial_cup'];
        $cupon_applied= 1;
        $rep= $this->_request['rep'];
        $bodyRep= json_decode($this->_request['body_rep']);
        $payment_form = 'DINERS MERCHANT';
        //Debug::print_r($cartProds);
        //Debug::print_r($rep);
        //Debug::print_r($bodyRep);
        //die();
//        $this->response(array('status' => "Failed", "msg" => $serial_cnt), 409);die();
        $db = $this->_db;
		
		$validityParam = new Parameter($db, '14');
		$validityParam->getData();
		$validityPeriod = $validityParam->getValue_par();
        if (is_array($cartProds) && count($cartProds) > 0) {
            foreach ($cartProds as $prod) {
                $prod = get_object_vars($prod);
                foreach ($prod['cards'] as $card) {
                    $card = get_object_vars($card);
                    $holder = get_object_vars($card['holder']);
                    $oldCard = TandiGlobalFunctions::getWebSalesData($db, $holder['serialCus']);
                }
            }
        }
		
//        Debug::print_r('help');die();
        //if (!$oldCard) {
        Sales::insertLogWSnewSale($db,$serial_cnt,$language,serialize($prod),'No card number');
            $response = WSSalesTandi::processCardInformationFromWebSale($db, $cartProds, $serial_cnt, $validityPeriod, $language,$rep,$bodyRep,$payment_form ,$serial_cup,$cupon_applied);

            if ($response['0'] == 50) {
			$this->response(array('status' => "Correct", "msg" => "INSERTED CORRECT", "transactions" => json_encode($response)), 200);
            } else {
                $this->response(array('status' => "Failed", "msg" => $response['1']), 409);
            }
//        } else {
//            $this->response(array('status' => "Failed", "msg" => 'ACTIVE CARD'), 409);
//    }
    }
    //Ws for kiosks
    //Ws update register table kiosk_paymeny
    public function WSupdateKioskPayment(){
        if (!$this->validate_login_attempt()) {
            $this->response('', 418);
        }

        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        //Obtain WS Data
        $json = file_get_contents('php://input');
        $kioskPay = json_decode($json, true);

        if($kioskPay['idPago']){
            $kpayment = new KioskPayment($this->_db, $kioskPay['idPago']);

            if($kpayment ->getData(false)){if($kpayment->getCodigoRespuesta() != $kioskPay['codigoRespuesta']) {
                $kpayment->setCodigoRespuesta($kioskPay['codigoRespuesta']);
            }
                if($kpayment->getLote() != $kioskPay['lote']) {
                    $kpayment->setLote($kioskPay['lote']);
                }
                if($kpayment->getReferencia() != $kioskPay['referencia']) {
                    $kpayment->setReferencia($kioskPay['referencia']);
                }
                if($kpayment->getAutorizacion() != $kioskPay['autorizacion']) {
                    $kpayment->setAutorizacion($kioskPay['autorizacion']);
                }
                if($kpayment->getCodigoAdquirente() != $kioskPay['codigoAdquirente']) {
                    $kpayment->setCodigoAdquirente($kioskPay['codigoAdquirente']);
                }
                if($kpayment->getNombreAdquiriente() != $kioskPay['nombreAdquiriente']) {
                    $kpayment->setNombreAdquiriente($kioskPay['nombreAdquiriente']);
                }
                if($kpayment->getTarjetaHabiente() != $kioskPay['tarjetaHabiente']) {
                    $kpayment->setTarjetaHabiente($kioskPay['tarjetaHabiente']);
                }
                if($kpayment->getNumeroTarjeta() != $kioskPay['numeroTarjeta']) {
                    $kpayment->setNumeroTarjeta($kioskPay['numeroTarjeta']);
                }
                if($kpayment->getMontoFijo() != $kioskPay['montoFijo']) {
                    $kpayment->setMontoFijo($kioskPay['montoFijo']);
                }
                if($kpayment->getInteres() != $kioskPay['interes']) {
                    $kpayment->setInteres($kioskPay['interes']);
                }
                if($kpayment->getAID() != $kioskPay['AID']) {
                    $kpayment->setAID($kioskPay['AID']);
                }
                if($kpayment->getARQC() != $kioskPay['ARQC']) {
                    $kpayment->setARQC($kioskPay['ARQC']);
                }
                if($kpayment->getAplicacionEMV() != $kioskPay['aplicacionEMV']) {
                    $kpayment->setAplicacionEMV($kioskPay['aplicacionEMV']);
                }
                if($kpayment->getCodigoRespuestaAut() != $kioskPay['codigoRespuestaAut']) {
                    $kpayment->setCodigoRespuestaAut($kioskPay['codigoRespuestaAut']);
                }
                if($kpayment->getCriptograma() != $kioskPay['criptograma']) {
                    $kpayment->setCriptograma($kioskPay['criptograma']);
                }
                if($kpayment->getFecha() != $kioskPay['fecha']) {
                    $kpayment->setFecha($kioskPay['fecha']);
                }
                if($kpayment->getFechaVencimiento() != $kioskPay['fechaVencimiento']) {
                    $kpayment->setFechaVencimiento($kioskPay['fechaVencimiento']);
                }
                if($kpayment->getHora() != $kioskPay['hora']) {
                    $kpayment->setHora($kioskPay['hora']);
                }
                if($kpayment->getMID() != $kioskPay['MID']) {
                    $kpayment->setMID($kioskPay['MID']);
                }
                if($kpayment->getModoLectura() != $kioskPay['modoLectura']) {
                    $kpayment->setModoLectura($kioskPay['modoLectura']);
                }
                if($kpayment->getNumerTarjetaEncriptado() != $kioskPay['numerTarjetaEncriptado']) {
                    $kpayment->setNumerTarjetaEncriptado($kioskPay['numerTarjetaEncriptado']);
                }
                if($kpayment->getPIN() != $kioskPay['PIN']) {
                    $kpayment->setPIN($kioskPay['PIN']);
                }
                if($kpayment->getPublicidad() != $kioskPay['publicidad']) {
                    $kpayment->setPublicidad($kioskPay['publicidad']);
                }
                if($kpayment->getTID() != $kioskPay['TID']) {
                    $kpayment->setTID($kioskPay['TID']);
                }
                if($kpayment->getTSI() != $kioskPay['TSI']) {
                    $kpayment->setTSI($kioskPay['TSI']);
                }
                if($kpayment->getTVR() != $kioskPay['TVR']) {
                    $kpayment->setTVR($kioskPay['TVR']);
                }
                if($kpayment->getTipoMensaje() != $kioskPay['tipoMensaje']) {
                    $kpayment->setTipoMensaje($kioskPay['tipoMensaje']);
                }
                if($kpayment->getMensajeRespuestaAut() != $kioskPay['mensajeRespuestaAut']) {
                    $kpayment->setMensajeRespuestaAut($kioskPay['mensajeRespuestaAut']);
                }
                if($kpayment->getMonto() != $kioskPay['monto']) {
                    $kpayment->setMonto($kioskPay['monto']);
                }
                if($kpayment->getIVA() != $kioskPay['IVA']) {
                    $kpayment->setIVA($kioskPay['IVA']);
                }
                if($kpayment->getPropina() != $kioskPay['propina']) {
                    $kpayment->setPropina($kioskPay['propina']);
                }
                if($kpayment->getServicio() != $kioskPay['servicio']) {
                    $kpayment->setServicio($kioskPay['servicio']);
                }
                if($kpayment->getICE() != $kioskPay['ICE']) {
                    $kpayment->setICE($kioskPay['ICE']);
                }
                if($kpayment->getOtrosImp() != $kioskPay['otrosImp']) {
                    $kpayment->setOtrosImp($kioskPay['otrosImp']);
                }
                if($kpayment->getBase0() != $kioskPay['base0']) {
                    $kpayment->setBase0($kioskPay['base0']);
                }
                if($kpayment->getBase12() != $kioskPay['base12']) {
                    $kpayment->setBase12($kioskPay['base12']);
                }
                if($kpayment->getCashOver() != $kioskPay['cashOver']) {
                    $kpayment->setCashOver($kioskPay['cashOver']);
                }
                if($kpayment->getTrama() != $kioskPay['trama']) {
                    $kpayment->setTrama($kioskPay['trama']);
                }

                if($kpayment->update()){
                    $this->response(array('success'), 200);
                }
            }
        }
        $error = array('status' => "Failed", "msg" => "No Data");
        $this->response($error, 400);

    }
    //WS get specific register
    public function WSKioskPayment(){
        if (!$this->validate_login_attempt()) {
            $this->response('', 418);
        }

        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        //Obtain WS Data
        $json = file_get_contents('php://input');
        $kioskPay = json_decode($json, true);


        $idPago = $kioskPay['idPago'];
        $kiosk = new KioskPayment($this->_db,$idPago);
        $result=$kiosk->getData();


        if ($result) {

            $kioskPaymentSet = array();
            $temp_Array = array();

            $temp_Array['idPago'] = $kiosk->getIdPago();
            $temp_Array['codigoRespuesta'] = $kiosk->getCodigoRespuesta();
            $temp_Array['lote'] = $kiosk->getLote();
            $temp_Array['referencia'] = $kiosk->getReferencia();
            $temp_Array['autorizacion'] = $kiosk->getAutorizacion();
            $temp_Array['codigoAdquirente'] = $kiosk->getCodigoAdquirente();
            $temp_Array['nombreAdquiriente'] = $kiosk->getNombreAdquiriente();
            $temp_Array['tarjetaHabiente'] = $kiosk->getTarjetaHabiente();
            $temp_Array['numeroTarjeta'] = $kiosk->getNumeroTarjeta();
            $temp_Array['montoFijo'] = $kiosk->getMontoFijo();
            $temp_Array['interes'] = $kiosk->getInteres();
            $temp_Array['AID'] = $kiosk->getAID();
            $temp_Array['ARQC'] = $kiosk->getARQC();
            $temp_Array['aplicacionEMV'] = $kiosk->getAplicacionEMV();
            $temp_Array['codigoRespuestaAut'] = $kiosk->getCodigoRespuestaAut();
            $temp_Array['criptograma'] = $kiosk->getCriptograma();
            $temp_Array['fecha'] = $kiosk->getFecha();
            $temp_Array['fechaVencimiento'] = $kiosk->getFechaVencimiento();
            $temp_Array['hora'] = $kiosk->getHora();
            $temp_Array['MID'] = $kiosk->getMID();
            $temp_Array['modoLectura'] = $kiosk->getModoLectura();
            $temp_Array['numerTarjetaEncriptado'] = $kiosk->getNumerTarjetaEncriptado();
            $temp_Array['PIN'] = $kiosk->getPIN();
            $temp_Array['publicidad'] = $kiosk->getPublicidad();
            $temp_Array['TID'] = $kiosk->getTID();
            $temp_Array['TSI'] = $kiosk->getTSI();
            $temp_Array['TVR'] = $kiosk->getTVR();
            $temp_Array['tipoMensaje'] = $kiosk->getTipoMensaje();
            $temp_Array['mensajeRespuestaAut'] = $kiosk->getMensajeRespuestaAut();
            $temp_Array['monto'] = $kiosk->getMonto();
            $temp_Array['IVA'] = $kiosk->getIVA();
            $temp_Array['propina'] = $kiosk->getPropina();
            $temp_Array['servicio'] = $kiosk->getServicio();
            $temp_Array['ICE'] = $kiosk->getICE();
            $temp_Array['otrosImp'] = $kiosk->getOtrosImp();
            $temp_Array['base0'] = $kiosk->getBase0();
            $temp_Array['base12'] = $kiosk->getBase12();
            $temp_Array['cashOver'] = $kiosk->getCashOver();
            $temp_Array['trama'] = $kiosk->getTrama();
            array_push($kioskPaymentSet, $temp_Array);

            $this->response($kioskPaymentSet, 200);
        } else {
            $this->response(array('status' => "Failed", "msg" => "NO PAYMENTS KIOSK"), 409);
        }

    }
    //WS get all data
    public function WSGetKioskPayments()
    {

        if (!$this->validate_login_attempt()) {
            $this->response('', 418);
        }

        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        $kioskPayment = New KioskPayment($this->_db);
        $kioskPayments=$kioskPayment->getAllData();

        if ($kioskPayments) {

            $kioskPaymentSet = array();
            $temp_Array = array();

            foreach ($kioskPayments as $c) {

                $temp_Array['idPago'] = $c['idPago'];
                $temp_Array['codigoRespuesta'] = $c['codigoRespuesta'];
                $temp_Array['lote'] = $c['lote'];
                $temp_Array['referencia'] = $c['referencia'];
                $temp_Array['autorizacion'] = $c['autorizacion'];
                $temp_Array['codigoAdquirente'] = $c['codigoAdquirente'];
                $temp_Array['nombreAdquiriente'] = $c['nombreAdquiriente'];
                $temp_Array['tarjetaHabiente'] = $c['tarjetaHabiente'];
                $temp_Array['numeroTarjeta'] = $c['numeroTarjeta'];
                $temp_Array['montoFijo'] = $c['montoFijo'];
                $temp_Array['interes'] = $c['interes'];
                $temp_Array['AID'] = $c['AID'];
                $temp_Array['ARQC'] = $c['ARQC'];
                $temp_Array['aplicacionEMV'] = $c['aplicacionEMV'];
                $temp_Array['codigoRespuestaAut'] = $c['codigoRespuestaAut'];
                $temp_Array['criptograma'] = $c['criptograma'];
                $temp_Array['fecha'] = $c['fecha'];
                $temp_Array['fechaVencimiento'] = $c['fechaVencimiento'];
                $temp_Array['hora'] = $c['hora'];
                $temp_Array['MID'] = $c['MID'];
                $temp_Array['modoLectura'] = $c['modoLectura'];
                $temp_Array['numerTarjetaEncriptado'] = $c['numerTarjetaEncriptado'];
                $temp_Array['PIN'] = $c['PIN'];
                $temp_Array['publicidad'] = $c['publicidad'];
                $temp_Array['TID'] = $c['TID'];
                $temp_Array['TSI'] = $c['TSI'];
                $temp_Array['TVR'] = $c['TVR'];
                $temp_Array['tipoMensaje'] = $c['tipoMensaje'];
                $temp_Array['mensajeRespuestaAut'] = $c['mensajeRespuestaAut'];
                $temp_Array['monto'] = $c['monto'];
                $temp_Array['IVA'] = $c['IVA'];
                $temp_Array['propina'] = $c['propina'];
                $temp_Array['servicio'] = $c['servicio'];
                $temp_Array['ICE'] = $c['ICE'];
                $temp_Array['otrosImp'] = $c['otrosImp'];
                $temp_Array['base0'] = $c['base0'];
                $temp_Array['base12'] = $c['base12'];
                $temp_Array['cashOver'] = $c['cashOver'];
                $temp_Array['trama'] = $c['trama'];


                array_push($kioskPaymentSet, $temp_Array);
            }
            $this->response($kioskPaymentSet, 200);
        } else {
            $this->response(array('status' => "Failed", "msg" => "NO PAYMENTS KIOSK"), 409);
        }


    }

    /*     * ****************************VA
     * @name:WSGetCardByCustomer
     * @return: List of sales web by customer
     * @params: Document customer
     */

    public function WSGetCardByCustomer() {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }
        $customer = new Customer($this->_db);
        $serial_cus = $customer -> getCustomerSerialbyDocument($this->_request['document_cus']);

        $list = Sales::getCardsByCustomerReportWeb($this->_db, $serial_cus, TRUE, TRUE);

        $this->response($list, 200);
    }




    //WS insert data
    public function WSNewKioskPayment()
    {

        if (!$this->validate_login_attempt()) {
            $this->response('', 418);
        }

        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }
        //Obtain WS Data
        $json = file_get_contents('php://input');
        $kioskPay = json_decode($json, true);
        //Debug::print_r($kioskPay['codigoRespuesta']);die();

        $codigoRespuesta = $kioskPay['codigoRespuesta'];
        $lote = $kioskPay['lote'];
        $referencia = $kioskPay['referencia'];
        $autorizacion = $kioskPay['autorizacion'];
        $codigoAdquirente = $kioskPay['codigoAdquirente'];
        $nombreAdquiriente = $kioskPay['nombreAdquiriente'];
        $tarjetaHabiente = $kioskPay['tarjetaHabiente'];
        $numeroTarjeta = $kioskPay['numeroTarjeta'];
        $montoFijo = $kioskPay['montoFijo'];
        $interes = $kioskPay['interes'];
        $AID = $kioskPay['AID'];
        $ARQC = $kioskPay['ARQC'];
        $aplicacionEMV = $kioskPay['aplicacionEMV'];
        $codigoRespuestaAut = $kioskPay['codigoRespuestaAut'];
        $criptograma = $kioskPay['criptograma'];
        $fecha = $kioskPay['fecha'];
        $fechaVencimiento = $kioskPay['fechaVencimiento'];
        $hora = $kioskPay['hora'];
        $MID = $kioskPay['MID'];
        $modoLectura = $kioskPay['modoLectura'];
        $numerTarjetaEncriptado = $kioskPay['numerTarjetaEncriptado'];
        $PIN = $kioskPay['PIN'];
        $publicidad = $kioskPay['publicidad'];
        $TID = $kioskPay['TID'];
        $TSI = $kioskPay['TSI'];
        $TVR = $kioskPay['TVR'];
        $tipoMensaje = $kioskPay['tipoMensaje'];
        $mensajeRespuestaAut = $kioskPay['mensajeRespuestaAut'];
        $monto = $kioskPay['monto'];
        $IVA = $kioskPay['IVA'];
        $propina = $kioskPay['propina'];
        $servicio = $kioskPay['servicio'];
        $ICE = $kioskPay['ICE'];
        $otrosImp = $kioskPay['otrosImp'];
        $base0 = $kioskPay['base0'];
        $base12 = $kioskPay['base12'];
        $cashOver = $kioskPay['cashOver'];
        $trama = $kioskPay['trama'];

        $kioskPayment = New KioskPayment($this->_db);
        $kioskPayment->setCodigoRespuesta($codigoRespuesta);
        $kioskPayment->setLote($lote);
        $kioskPayment->setReferencia($referencia);
        $kioskPayment->setAutorizacion($autorizacion);
        $kioskPayment->setCodigoAdquirente($codigoAdquirente);
        $kioskPayment->setNombreAdquiriente($nombreAdquiriente);
        $kioskPayment->setTarjetaHabiente($tarjetaHabiente);
        $kioskPayment->setNumeroTarjeta($numeroTarjeta);
        $kioskPayment->setMontoFijo($montoFijo);
        $kioskPayment->setInteres($interes);
        $kioskPayment->setAID($AID);
        $kioskPayment->setARQC($ARQC);
        $kioskPayment->setAplicacionEMV($aplicacionEMV);
        $kioskPayment->setCodigoRespuestaAut($codigoRespuestaAut);
        $kioskPayment->setCriptograma($criptograma);
        $kioskPayment->setFecha($fecha);
        $kioskPayment->setFechaVencimiento($fechaVencimiento);
        $kioskPayment->setHora($hora);
        $kioskPayment->setMID($MID);
        $kioskPayment->setModoLectura($modoLectura);
        $kioskPayment->setNumerTarjetaEncriptado($numerTarjetaEncriptado);
        $kioskPayment->setPIN($PIN);
        $kioskPayment->setPublicidad($publicidad);
        $kioskPayment->setTID($TID);
        $kioskPayment->setTSI($TSI);
        $kioskPayment->setTVR($TVR);
        $kioskPayment->setTipoMensaje($tipoMensaje);
        $kioskPayment->setMensajeRespuestaAut($mensajeRespuestaAut);
        $kioskPayment->setMonto($monto);
        $kioskPayment->setIVA($IVA);
        $kioskPayment->setPropina($propina);
        $kioskPayment->setServicio($servicio);
        $kioskPayment->setICE($ICE);
        $kioskPayment->setOtrosImp($otrosImp);
        $kioskPayment->setBase0($base0);
        $kioskPayment->setBase12($base12);
        $kioskPayment->setCashOver($cashOver);
        $kioskPayment->setTrama($trama);


        if ($kioskPayment->insert()) {
            $this->response(array('status' => "Correct", "msg" => "INSERTED KIOSK PAYMENT"), 200);
        } else {
            $this->response(array('status' => "Failed", "msg" => "NO INSERT KIOSK PAYMENT"), 409);
        }



    }



    //WS TABLE TRAMA

    public function WSNewTrama() {

        if (!$this->validate_login_attempt()) {
            $this->response('', 418);
        }

        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }
        //Obtain WS Data
        $json = file_get_contents('php://input');
        $trama = json_decode($json, true);


        $contenido = $trama['contenido'];
        $hora = $trama['hora'];
        $fecha = $trama['fecha'];
        $idKiosko = $trama['idKiosko'];
        $estado = $trama['estado'];


        $newTrama = New Trama($this->_db);
        $newTrama->setContenido($contenido);
        $newTrama->setHora($hora);
        $newTrama->setFecha($fecha);
        $newTrama->setIdKiosko($idKiosko);
        $newTrama->setEstado($estado);


        if ($newTrama->insert()) {
            $this->response(array('status' => "Correct", "msg" => "INSERTED TRAMA"), 200);
        } else {
            $this->response(array('status' => "Failed", "msg" => "NO INSERT TRAMA"), 409);
        }
    }

    public function WSGetTramas() {
        if (!$this->validate_login_attempt()) {
            $this->response('', 418);
        }

        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        $trama = New Trama($this->_db);
        $tramas=$trama->getAllData();

        if ($tramas) {

            $tramaSet = array();
            $temp_Array = array();

            foreach ($tramas as $c) {

                $temp_Array['idTrama'] = $c['idTrama'];
                $temp_Array['contenido'] = $c['contenido'];
                $temp_Array['hora'] = $c['hora'];
                $temp_Array['fecha'] = $c['fecha'];
                $temp_Array['idKiosko'] = $c['idKiosko'];
                $temp_Array['estado'] = $c['estado'];

                array_push($tramaSet, $temp_Array);
            }
            $this->response($tramaSet, 200);
        } else {
            $this->response(array('status' => "Failed", "msg" => "NO TRAMA"), 409);
        }

    }

    public function WSTrama() {

        if (!$this->validate_login_attempt()) {
            $this->response('', 418);
        }

        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        //Obtain WS Data
        $json = file_get_contents('php://input');
        $trama = json_decode($json, true);


        $idTrama = $trama['idTrama'];
        $tramaById = new Trama($this->_db,$idTrama);
        $result=$tramaById->getData();


        if ($result) {

            $tramaSet = array();
            $temp_Array = array();

            $temp_Array['idTrama'] = $tramaById->getIdTrama();
            $temp_Array['contenido'] = $tramaById->getContenido();
            $temp_Array['hora'] = $tramaById->getHora();
            $temp_Array['fecha'] = $tramaById->getFecha();
            $temp_Array['idKiosko'] = $tramaById->getIdKiosko();
            $temp_Array['estado'] = $tramaById->getEstado();

            array_push($tramaSet, $temp_Array);

            $this->response($tramaSet, 200);
        } else {
            $this->response(array('status' => "Failed", "msg" => "NO TRAMA"), 409);
        }
    }

    public function WSupdateTrama() {
        if (!$this->validate_login_attempt()) {
            $this->response('', 418);
        }

        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        //Obtain WS Data
        $json = file_get_contents('php://input');
        $trama = json_decode($json, true);

        if($trama['idTrama']){
            $uTrama = new Trama($this->_db, $trama['idTrama']);

            if($uTrama ->getData(false)){

                if($uTrama->getContenido() != $trama['contenido']) {
                    $uTrama->setContenido($trama['contenido']);
                }
                if($uTrama->getHora() != $trama['hora']) {
                    $uTrama->setHora($trama['hora']);
                }
                if($uTrama->getFecha() != $trama['fecha']) {
                    $uTrama->setFecha($trama['fecha']);
                }
                if($uTrama->getIdKiosko() != $trama['idKiosko']) {
                    $uTrama->setIdKiosko($trama['idKiosko']);
                }
                if($uTrama->getEstado() != $trama['estado']) {
                    $uTrama->setEstado($trama['estado']);
                }

                if($uTrama->update()){
                    $this->response(array('success'), 200);
                }
            }
        }
        $error = array('status' => "Failed", "msg" => "No Data");
        $this->response($error, 400);

    }


    /*     * ****************************VA
     * @name: WSTransactionByCustomer 
     * @return: Array with transactions by customer. No use
     * POST: document 
     */

    public function WSTransactionByCustomer() {

        if (!$this->validate_login_attempt()) {
            $this->response('', 418);
        }
        
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

		$useSerial= $this->_request['useSerial']; 
        $documentCus = $this->_request['document_Cus'];
		$customerSerial= $this->_request['serialCustomer'];
		
		$customerData = new Customer($this->_db);  
		
		if ($useSerial == 'TRUE' ){                  
			$customerData->setserial_cus($this->cleanField(utf8_encode($customerSerial)));                  
        }else{
			$customerData->setDocument_cus($this->cleanField(utf8_encode($documentCus)));                   
        }
		
        if (!$customerData->getData()){
			$this->response(array('status' => "Failed", "msg" => "INSERT DOCUMENT"), 409);
		}     
                       
        $customer = Sales::getTransactionByCustomer($this->_db, $customerData->getSerial_cus());

        if ($customer) {

            $customerSet = array();
            $temp_Array = array();

            foreach ($customer as $c) {

                $temp_Array['idTransaction'] = $c['transaction_id'];
                array_push($customerSet, $temp_Array);
            }
            $this->response($customerSet, 200);
        } else {
            $this->response(array('status' => "Failed", "msg" => "NO TRANSACTION"), 409);
        }
    }
    
    /*     * ****************************VA
     * @name: WSBenefitsByProduct 
     * @return: Array with benefits by product
     * POST: serial product by country
     */
    
    public function WSBenefitsByProduct(){
            
             if(!$this->validate_login_attempt()){
                $this->response('', 418);
            } 
            
            if($this->get_request_method() != "POST"){
		$this->response('',406);
			}           
            
            $productxc_Id = $this->_request['product_Id'];
            
            if (!$productxc_Id){
                $this->response(array('status' => "Failed", "msg" => "INSERT PRODUCT"), 409);
            }
            
            $product = new ProductByCountry($this->_db);
            $product->setSerial_pxc($productxc_Id);
            $product->getData();
            $product_Id = $product->getSerial_pro();            
            
            $benefits = Product::getBenefitsByProduct($this->_db, $product_Id);
            
            if ($benefits) {

                    $benefitsSet = array();
                    $temp_Array = array();

                    foreach ($benefits as $c) {

                        $temp_Array['category'] = utf8_encode($c['description_bcat']);
                        $temp_Array['idBenefit'] = $c['serial_ben'];
                        $temp_Array['benefitDescription'] = utf8_encode($c['description_bbl']);
                        $temp_Array['coverage'] = $c['price_bxp'];
                        $temp_Array['restrictionCoverage'] = $c['restriction_price_bxp'];
                        $temp_Array['coverageType'] = utf8_encode($c['description_rbl']);
                        $temp_Array['coverageText'] = utf8_encode($c['description_cbl']);
                        
                        array_push($benefitsSet, $temp_Array);
                    }
                    $this->response($benefitsSet, 200);
            } else {
            $this->response(array('status' => "Failed", "msg" => "NO BENEFITS"), 409);
        }
            
            
    }
    
     /*     * ****************************VA
     * @name: WSListSalesByTransaction 
     * @return: Array with list sales by transaction. No used
     * 
     */
    
    public function WSListSalesByTransaction(){
            
             if(!$this->validate_login_attempt()){
                $this->response('', 418);
            }
            
            if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
                     
            $transaction_id = $this->_request['transaction_Id'];
            
            if (!$transaction_id){
                $this->response(array('status' => "Failed", "msg" => "INSERT TRANSACTION"), 204);
            }        
 
            $transaction = Sales::getSalesByTransaction($this->_db, $transaction_id);
            
            if ($transaction) {

                    $transationSet = array();
                    $temp_Array = array();

                    foreach ($transaction as $c) {

                        $temp_Array['idTransaction'] = $c['transaction_id'];
                        $temp_Array['idSale'] = $c['serial_sal'];
                        $temp_Array['idCustomer'] = ($c['serial_cus']);
                        
                        
                        array_push($transationSet, $temp_Array);
                    }
                    $this->response($transationSet, 200);
            } else {
            $this->response(array('status' => "Failed", "msg" => "NO TRANSACTION"), 200);
        }
            
            
    }
    
    /*     * ****************************VA
     * @name: WSListSalesByDate 
     * WS para retornar las ventas a Centinel: 
     * Autor: Luis Salvador
     * 
     */
    
    public function WSListSalesByDate(){
            
             if(!$this->validate_login_attempt()){
                $this->response('', 418);
            }
            
            if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
                     
            $date = $this->_request['date'];
            $valores = explode('/', $date);
            
            if (!checkdate($valores[1], $valores[0], $valores[2])){
                $this->response(array('status' => "Failed", "msg" => "WRONG DATE"), 204);
            } 
            
            $newDate = $valores[2].'-'.$valores[1].'-'.$valores[0];
            $list_sales = Sales::getSalesByDate($this->_db, $newDate);
            if ($list_sales) {

                    $salesSet = array();
                    $temp_Array = array();

                    foreach ($list_sales as $c) {

                        $temp_Array['CEDULA'] = $c['CEDULA'];
                        $temp_Array['NOMBRES'] = utf8_encode($c['NOMBRE']);
                        $temp_Array['FEC_NACIMIENTO'] = ($c['FEC_NACIMIENTO']);
                        $temp_Array['PHONE'] = $c['PHONE'];
                        $temp_Array['CONTRACT'] = $c['CONTRACT'];
                        $temp_Array['PRODUCTO'] = $c['PRODUCTO'];
                        $temp_Array['ESTADO'] = $c['ESTADO'];
                        //$temp_Array['DATE'] = ($c['DATE']);
                        array_push($salesSet, $temp_Array);
                    }
                    $this->response($salesSet, 200);
            } else {
            $this->response(array('status' => "Failed", "msg" => "NO CUSTOMER"), 200);
        }
            
            
    }

    public function WSGetSaleCreditNote(){
        ini_set('memory_limit', '512M');
        $list_sales = Invoice::getSaleCreditNote($this->_db);
        
        if ($list_sales) {
            // Array para almacenar los datos formateados
            $data = [];
            $taxAppliedValue = 0.5;
            // Procesar cada fila de resultado
            foreach ($list_sales as $row) {
                $valorPrimaSC = $row['ValorPrimaSC'] ;
                $descuento = $row['DescuentoPorcentaje'] ;
                $otroDescuento = $row['OtroDescuentoPorcentaje'];
                $valorFactura = isset($row['ValorFactura']) ? $row['ValorFactura'] : 0;
                
                // Calcular la prima neta
    if (isset($row['TipoDocumento']) && $row['TipoDocumento'] === 'NOTA DE CREDITO') {
        // Para notas de crédito, calcular el valor sin impuesto
    $valorImpuesto = number_format(($valorFactura * $taxAppliedValue) / 100, 2, ".", "");
    $valueWithOutTax = number_format($valorFactura - $valorImpuesto, 2, ".", "");
    } else {
        // Para otros documentos, calcular considerando descuentos
        if ($descuento > 0 || $otroDescuento > 0) {
            $valorImpuestoPrev = $valorPrimaSC - number_format(($valorPrimaSC * $taxAppliedValue) / 100, 2, ".", "");
            $valueDiscount = number_format($valorImpuestoPrev * (($descuento + $otroDescuento) / 100), 2, ".", "");
            $valueWithOutTax = number_format($valorImpuestoPrev - $valueDiscount, 2, ".", "");
            $valorImpuesto = number_format(($valueWithOutTax * $taxAppliedValue) / 100, 2, ".", "");
        } else {
            $valorImpuesto = number_format(($valorPrimaSC * $taxAppliedValue) / 100, 2, ".", "");
            $valueWithOutTax = number_format($valorPrimaSC - $valorImpuesto, 2, ".", "");
            $valueDiscount = $valorPrimaSC * (($descuento + $otroDescuento) / 100);
        }
        $valueDiscount = number_format($valueDiscount, 2, ".", "");
    }
                
                $data[] = [
                   "SUCURSAL" => isset($row['Sucursal']) ? $row['Sucursal'] : '',
                   "RESPONSABLE" => isset($row['Responsable']) ? $row['Responsable'] : '',
                   "CORREO_RESPONSABLE" => isset($row['EmailResponsable']) ? $row['EmailResponsable'] : '',
                "POLIZA" => isset($row['Poliza']) ? $row['Poliza'] : '',
                "IDENTIFICACION" => isset($row['Documento']) ? $row['Documento'] : '',
                "NOMBRES" => isset($row['Cliente']) ? $row['Cliente'] : '',
                "FECHA_VENTA" => isset($row['FechaContabilidad']) ? $row['FechaContabilidad'] : '',
                "INICIO_VIGENCIA" => isset($row['FechaVigDesde']) ? $row['FechaVigDesde'] : '',
                "FIN_VIGENCIA" => isset($row['FechaVigHasta']) ? $row['FechaVigHasta'] : '',
                "DIAS_COMPRADOS" => isset($row['DiasComprados']) ? $row['DiasComprados'] : '',
                "ID_PRODUCTO" => isset($row['ID_PRODUCTO']) ? $row['ID_PRODUCTO'] : '',
                "PRODUCTO" => isset($row['Producto']) ? $row['Producto'] : '',
                "FECHA_FACTURA" => (isset($row['TipoDocumento']) && $row['TipoDocumento'] === 'NOTA DE CREDITO') 
                ? (isset($row['FechaCN']) ? $row['FechaCN'] : '') 
                : (isset($row['FechaContabilidad']) ? $row['FechaContabilidad'] : ''),
                "FACTURA" => isset($row['SerieFactura']) ? $row['SerieFactura'] : '',
                "TipoDocumento" => isset($row['TipoDocumento']) ? $row['TipoDocumento'] : '',
                "NotaCredito" => isset($row['NotaCredito']) ? $row['NotaCredito'] : '',
                "DESTINO" => isset($row['Destino']) ? $row['Destino'] : '',
                "IDENTIFICACION_COMERCIALIZADOR" => isset($row['RucIntermediario']) ? $row['RucIntermediario'] : '',
                "COMERCIALIZADOR" => isset($row['Agente']) ? $row['Agente'] : '',
                "PRIMA_NETA" => isset($row['TipoDocumento']) && $row['TipoDocumento'] === 'NOTA DE CREDITO'
                ? (strpos($valueWithOutTax, '-') === 0
                    ? number_format($valueWithOutTax, 2, '.', '')
                    : '-' . number_format($valueWithOutTax, 2, '.', ''))
                : number_format($valueWithOutTax, 2, '.', ''),
                "PRIMA_TOTAL" => isset($row['ValorPrimaSC']) ? $row['ValorPrimaSC'] : '',
                "VALOR_FACTURA" => isset($row['ValorFactura']) ? $row['ValorFactura'] : '',
                "DESCUENTOS" => isset($row['Descuento']) ? $row['Descuento'] : '0.00',
                "OTROS_DESCUENTOS" => isset($row['OtroDescuento']) ? $row['OtroDescuento'] : '0.00',
                "CUPON_DESCUENTO" => isset($row['CuponDescuento']) ? $row['CuponDescuento'] : '0.00',
                "ESTADO" => isset($row['Estado']) ? $row['Estado'] : 'PAID',
                "TIPO_VENTA" => ($row['RucIntermediario'] === '1391722907001') ? 'Venta Directa' : 'Comercializador',
                "TIPO_COMERCIALIZADOR" => isset($row['TipoComercializador']) ? $row['TipoComercializador'] : '',
                "LINEA_NEGOCIO" => 'BlueAssist'
                ];
            }
        
            function utf8ize($data) {
                if (is_array($data)) {
                    foreach ($data as $key => $value) {
                        $data[$key] = utf8ize($value);
                    }
                } elseif (is_string($data)) {
                    return utf8_encode($data);
                }
                return $data;
            }
            $data = utf8ize($data);
            // Crear respuesta
            $response = [
                "success" => true,
                "data" => $data,
                "message" => "Se encontraron registros",
                "status" => 200
            ];
          
            
            ini_set('output_buffering', 'On');
            ob_start();
            $this->response($data, 200);
                //return json_encode($response);
            ob_end_flush();
            // Enviar respuesta en formato JSON
            //return json_encode($response);
        } else {
            // Crear respuesta de error
            $response = [
                "success" => false,
                "message" => "No se encontraron registros",
                "status" => 404
            ];
        
            // Enviar respuesta en formato JSON
            return json_encode($response);
        }
        
    }

    public function WSGetSaleCreditNoteHistorical(){
            // Aumentar el tiempo máximo de ejecución a 5 minutos
    ini_set('max_execution_time', 300); // 300 segundos = 5 minutos
        ini_set('memory_limit', '512M');
        $list_sales = Invoice::getSaleCreditNoteHistorical($this->_db);
        
        if ($list_sales) {
            // Array para almacenar los datos formateados
            $data = [];
            $taxAppliedValue = 0.5;
            // Procesar cada fila de resultado
            foreach ($list_sales as $row) {
                $valorPrimaSC = $row['ValorPrimaSC'] ;
                $descuento = $row['DescuentoPorcentaje'] ;
                $otroDescuento = $row['OtroDescuentoPorcentaje'];
                
                if (!empty($taxAppliedValue)) {
                    if ($descuento > 0 || $otroDescuento > 0) {
                        $valorImpuestoPrev = $valorPrimaSC - number_format(($valorPrimaSC * $taxAppliedValue) / 100, 2, ".", "");
                        $valueDiscount = number_format($valorImpuestoPrev * (($descuento + $otroDescuento) / 100), 2, ".", "");
                        $valueWithOutTax = number_format($valorImpuestoPrev - $valueDiscount, 2, ".", "");
                        $valorImpuesto = number_format(($valueWithOutTax * $taxAppliedValue) / 100, 2, ".", "");
                    } else {
                        $valorImpuesto = number_format(($valorPrimaSC * $taxAppliedValue) / 100, 2, ".", "");
                        $valueWithOutTax = number_format($valorPrimaSC - $valorImpuesto, 2, ".", "");
                        $valueDiscount = $valorPrimaSC * (($descuento + $otroDescuento) / 100);
                    }
                    $valueDiscount = number_format($valueDiscount, 2, ".", "");
                }
                
                $data[] = [
                   "SUCURSAL" => isset($row['Sucursal']) ? $row['Sucursal'] : '',
                   "RESPONSABLE" => isset($row['Responsable']) ? $row['Responsable'] : '',
                   "CORREO_RESPONSABLE" => isset($row['EmailResponsable']) ? $row['EmailResponsable'] : '',
                "POLIZA" => isset($row['Poliza']) ? $row['Poliza'] : '',
                "IDENTIFICACION" => isset($row['Documento']) ? $row['Documento'] : '',
                "NOMBRES" => isset($row['Cliente']) ? $row['Cliente'] : '',
                "FECHA_VENTA" => isset($row['FechaContabilidad']) ? $row['FechaContabilidad'] : '',
                "INICIO_VIGENCIA" => isset($row['FechaVigDesde']) ? $row['FechaVigDesde'] : '',
                "FIN_VIGENCIA" => isset($row['FechaVigHasta']) ? $row['FechaVigHasta'] : '',
                "DIAS_COMPRADOS" => isset($row['DiasComprados']) ? $row['DiasComprados'] : '',
                "ID_PRODUCTO" => isset($row['ID_PRODUCTO']) ? $row['ID_PRODUCTO'] : '',
                "PRODUCTO" => isset($row['Producto']) ? $row['Producto'] : '',
                "FECHA_FACTURA" => (isset($row['TipoDocumento']) && $row['TipoDocumento'] === 'NOTA DE CREDITO') 
                ? (isset($row['FechaCN']) ? $row['FechaCN'] : '') 
                : (isset($row['FechaContabilidad']) ? $row['FechaContabilidad'] : ''),
                "FACTURA" => isset($row['SerieFactura']) ? $row['SerieFactura'] : '',
                "TipoDocumento" => isset($row['TipoDocumento']) ? $row['TipoDocumento'] : '',
                "NotaCredito" => isset($row['NotaCredito']) ? $row['NotaCredito'] : '',
                "DESTINO" => isset($row['Destino']) ? $row['Destino'] : '',
                "IDENTIFICACION_COMERCIALIZADOR" => isset($row['RucIntermediario']) ? $row['RucIntermediario'] : '',
                "COMERCIALIZADOR" => isset($row['Agente']) ? $row['Agente'] : '',
                "PRIMA_NETA" =>  isset($row['TipoDocumento']) && $row['TipoDocumento'] === 'NOTA DE CREDITO' 
                ? (strpos($valueWithOutTax, '-') === 0 ? number_format($valueWithOutTax, 2, '.', '') 
                : '-' . number_format($valueWithOutTax, 2, '.', ''))
                : number_format($valueWithOutTax, 2, '.', ''),
                "PRIMA_TOTAL" => isset($row['ValorPrimaSC']) ? $row['ValorPrimaSC'] : '',
                "VALOR_FACTURA" => isset($row['ValorFactura']) ? $row['ValorFactura'] : '',
                "DESCUENTOS" => isset($row['Descuento']) ? $row['Descuento'] : '0.00',
                "OTROS_DESCUENTOS" => isset($row['OtroDescuento']) ? $row['OtroDescuento'] : '0.00',
                "CUPON_DESCUENTO" => isset($row['CuponDescuento']) ? $row['CuponDescuento'] : '0.00',
                "ESTADO" => isset($row['Estado']) ? $row['Estado'] : 'PAID',
                "TIPO_VENTA" => ($row['RucIntermediario'] === '1391722907001') ? 'Venta Directa' : 'Comercializador',
                "TIPO_COMERCIALIZADOR" => isset($row['TipoComercializador']) ? $row['TipoComercializador'] : '',
                "LINEA_NEGOCIO" => 'BlueAssist'
                ];
            }
        
            function utf8ize($data) {
                if (is_array($data)) {
                    foreach ($data as $key => $value) {
                        $data[$key] = utf8ize($value);
                    }
                } elseif (is_string($data)) {
                    return utf8_encode($data);
                }
                return $data;
            }
            $data = utf8ize($data);
            // Crear respuesta
            $response = [
                "success" => true,
                "data" => $data,
                "message" => "Se encontraron registros",
                "status" => 200
            ];
          
            
            ini_set('output_buffering', 'On');
            ob_start();
            $this->response($data, 200);
                //return json_encode($response);
            ob_end_flush();
            // Enviar respuesta en formato JSON
            //return json_encode($response);
        } else {
            // Crear respuesta de error
            $response = [
                "success" => false,
                "message" => "No se encontraron registros",
                "status" => 404
            ];
        
            // Enviar respuesta en formato JSON
            return json_encode($response);
        }
        
    }

    public function WSGetActiveDealers() {

    
        ini_set('memory_limit', '512M');
        $dealers = Invoice::getActiveDealers($this->_db);
    
        if ($dealers) {
            $data = [];
    
            foreach ($dealers as $dealer) {
                $data[] = [
                    "dealer_id" => $dealer['dealer_id'],
                    "dealer_type_id" => $dealer['dealer_type_id'],
                    "dealer_type" => $dealer['dealer_type'],
                    "document_number" => $dealer['document_number'],
                    "name" => $dealer['name'],
                    "short_name" => $dealer['short_name'],
                    "email" => $dealer['email'],
                    "phone" => $dealer['phone'],
                    "address" => $dealer['address'],
                    "contract_date" => $dealer['contract_date'],
                    "bank" => $dealer['bank'],
                    "account_type" => $dealer['account_type'],
                    "account_number" => $dealer['account_number'],
                    "status" => $dealer['status'],
                    "manager" => $dealer['manager'],
                    "manager_email" => $dealer['manager_email'],
                    "manager_phone" => $dealer['manager_phone'],
                    "manager_mobile" => $dealer['manager_mobile'],
                    "manager_birth_date" => $dealer['manager_birth_date'],
                    "payment_contact" => $dealer['payment_contact'],
                    "billing_email" => $dealer['billing_email'],
                    "percentage" => $dealer['percentage'],
                    "bill_to" => $dealer['bill_to'],
                    "percentage_type" => $dealer['percentage_type'],
                    "document_number_responsible" => $dealer['document_number_responsible'],
                    "first_name" => $dealer['first_name'],
                    "last_name" => $dealer['last_name'],
                    "branch_name" => $dealer['branch_name']
                ];
            }

            function utf8ize($data) {
                if (is_array($data)) {
                    foreach ($data as $key => $value) {
                        $data[$key] = utf8ize($value);
                    }
                } elseif (is_string($data)) {
                    return utf8_encode($data);
                }
                return $data;
            }
            $data = utf8ize($data);
    
            $response = [
                "success" => true,
                "data" => $data,
                "message" => "Se encontraron registros",
                "status" => 200
            ];
        } else {
            $response = [
                "success" => false,
                "message" => "No se encontraron registros",
                "status" => 404
            ];
        }
    
        ini_set('output_buffering', 'On');
        ob_start();
        $this->response($data, 200);
        ob_end_flush();
    }
    
    public function getSaleDetailRefundZoho(){
        ini_set('max_execution_time', 300); // 300 segundos = 5 minutos
        ini_set('memory_limit', '512M');
       if($this->get_request_method() != "POST"){
           $this->response('',406);
       }
       $json = file_get_contents('php://input');
       $cardnumberJson = json_decode($json, true);
  $cardNumber =  $cardnumberJson['cardNumber'];

       $list_sales = WSSales::getSaleDetailRefundZoho($this->_db, $cardNumber);
       if ($list_sales) {

               $salesSet = array();
               $temp_Array = array();

               foreach ($list_sales as $c) {

                   $temp_Array['sale_id'] = $c['sale_id'];
                   $temp_Array['card_number'] =$c['card_number'];
                   $temp_Array['dealer_id'] = $c['dealer_id'];
                   $temp_Array['document_number'] = $c['document_cus'];
                   $temp_Array['customer_name'] =  utf8_encode($c['customer_name']);
                   $temp_Array['dealer_name'] = $c['dealer_name'];
                   $temp_Array['name'] = $c['name'];
                   $temp_Array['created_at'] = $c['created_at'];
                   $temp_Array['departure_date'] = $c['departure_date'];
                   $temp_Array['return_date'] = $c['return_date'];
                   $temp_Array['total'] = $c['total'];
                   $temp_Array['status_sal'] = $c['status_sal'];
                   $temp_Array['invoice_number'] = $c['invoice_number'];
                   $temp_Array['status_inv'] = $c['status_inv'];
                   $temp_Array['branch_name'] = $c['branch_name'];
                   $temp_Array['responsible'] = $c['responsible'];
                   $temp_Array['document_number_dea'] = $c['document_number_dea'];
                   $temp_Array['email_cus'] = $c['email_cus'];
                   array_push($salesSet, $temp_Array);
               }
               ini_set('output_buffering', 'On');
               ob_start();
               $this->response($salesSet, 200);
               ob_end_flush();
       } else {
       $this->response(array('status' => "Failed", "msg" => "NO INFORMATION"), 200);
   }
       
       
}
    
    // This process indicates if minium data is correctly
    private function minimunDataRequiredNewSale($data_array) {
        $case_code = 200;

        if (!isset($data_array['facEmail'])) {
            $case_code = 651;
        } elseif (!isset($data_array['facPhone'])) {
            $case_code = 652;
        } elseif (!isset($data_array['facAddress'])) {
            $case_code = 653;
        } elseif (!isset($data_array['facCI'])) {
            $case_code = 654;
        } elseif (!isset($data_array['facName'])) {
            $case_code = 655;
        } elseif (!isset($data_array['facCity'])) {
            $case_code = 656;
        } elseif (!isset($data_array['daysPod'])) {
            $case_code = 657;
        } elseif (!isset($data_array['statusSale'])) {
            $case_code = 658;
        }elseif (!isset($data_array['serialCou'])) {
            $case_code = 659;
        }elseif (!isset($data_array['serialPxc'])) {
            $case_code = 660;
        }elseif (!isset($data_array['beginDatePod'])) {
            $case_code = 661;
        }elseif (!isset($data_array['endDatePod'])) {
            $case_code = 662;
        }elseif (!isset($data_array['daysPod'])) {
            $case_code = 663;
        }elseif (!isset($data_array['adultsPod'])) {
            $case_code = 664;
        }elseif (!isset($data_array['childrenPod'])) {
            $case_code = 665;
        }elseif (!isset($data_array['spousePod'])) {
            $case_code = 666;
        }elseif (!isset($data_array['costPod'])) {
            $case_code = 667;
        }elseif (!isset($data_array['costChildrenPod'])) {
            $case_code = 668;
        }elseif (!isset($data_array['costSpousePod'])) {
            $case_code = 669;
        }elseif (!isset($data_array['costExtraPod'])) {
            $case_code = 670;
        }elseif (!isset($data_array['priceExtraPod'])) {
            $case_code = 671;
        }elseif (!isset($data_array['totalPricePod'])) {
            $case_code = 672;
        }elseif (!isset($data_array['quantity'])) {
            $case_code = 673;
        }elseif (!isset($data_array['proBenefitName'])) {
            $case_code = 674;
        }elseif (!isset($data_array['proBenefitCost'])) {
            $case_code = 675;
        }elseif (!isset($data_array['informationComplete'])) {
            $case_code = 676;
        }elseif (!isset($data_array['seniorRestriction'])) {
            $case_code = 677;
        }elseif (!isset($data_array['severalFlights'])) {
            $case_code = 678;
        }

        return $case_code;
    }

    // This process indicates if a data is formatted correctly  
    private function validateDataWSNewSale($data_array) {

        $code_validation = 200;

        if (!is_numeric($data_array['price'])) {
            $code_validation = 763;
        } elseif (!strstr($data_array['startDate'], '/')) {
            $code_validation = 765;
        } elseif (!strstr($data_array['endDate'], '/')) {
            $code_validation = 766;
        }
        return $code_validation;
    }

}

//************************ API INITIALIZE **************
$api = new API;
$api->processApi();
?>
