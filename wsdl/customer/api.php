<?php

/**
 * File: api
 * Author: Patricio Astudillo
 * Creation Date: 19-jun-2013
 * Last Modified: 3-jul-2017
 * Modified By: David Rosales
 */
//	ini_set('include_path',ini_get('include_path').':/home/passyste/public_html/RIMAC/lib/:/home/passyste/public_html/RIMAC/lib/clases:/home/passyste/public_html/RIMAC/:/home/passyste/public_html/RIMAC/lib/smarty-2.6.22:/home/passyste/public_html/RIMAC/lib/Excel/Classes/:/home/passyste/public_html/RIMAC/lib/PDF/');
//	define('DOCUMENT_ROOT', '/home/passyste/public_html/RIMAC/');
//	require_once(DOCUMENT_ROOT.'lib/config.inc.php');
//ini_set('include_path', ini_get('include_path') . ':/Applications/XAMPP/htdocs/RIMAC/lib/:/Applications/XAMPP/htdocs/RIMAC/lib/clases:/Applications/XAMPP/htdocs/RIMAC:/Applications/XAMPP/htdocs/RIMAC/lib/smarty-2.6.22');
//define('DOCUMENT_ROOT', '/Applications/XAMPP/htdocs/RIMAC/');
require_once(DOCUMENT_ROOT . 'lib/config.inc.php');

class API extends Rest {

	private $customerTypes = array('PERSON', 'LEGAL_ENTITY');

	public function __construct() {
		parent::__construct(); // Init parent contructor
	}

	public function processApi() {
		$func = strtolower(trim(str_replace("/", "", $_REQUEST['rquest'])));
		if ((int) method_exists($this, $func) > 0)
			$this->$func();
		else
			$this->response('', 404); // If the method not exist with in this class, response would be "Page not found".
	}

	/**
	 * Retrieves database connection
	 * 
	 * @param int $queue_opt Identifier which describes db selection 
	 *
	 * @return string $db
	 */
	private function chooseConnection($queue_opt) {
		if (is_numeric($queue_opt)) {
			switch ($queue_opt) {
				//contactvox 1: Blue Card
				case '1': $host = 'localhost';
					$db_name = 'planetassist';
					$user = 'root';
					$pass = 'planet';
					break;
				//contactvox 2: Rocafuerte
				case '2': $host = 'localhost';
					$db_name = 'rocafuerte';
					$user = 'root';
					$pass = 'planet';
					break;
				//contactvox 3: Sucre
				case '3': $host = 'localhost';
					$db_name = 'sucre';
					$user = 'root';
					$pass = 'planet';
					break;
				//contactvox 5: Planet Axis
				case '5': $host = 'localhost';
					$db_name = 'planetassist';
					$user = 'root';
					$pass = 'planet';
					break;
				//contactvox 6: Buen Viaje
				case '6': $host = 'localhost';
					$db_name = 'equivida';
					$user = 'root';
					$pass = 'planet';
					break;
				case '7':
					$host = 'localhost';
					$db_name = 'passyste_PAS';
					$user = 'passyste_pas';
					$pass = 'cf0b73355f0c60e9ebe6';
					break;
				default: $host = NULL;
					$db_name = NULL;
					$user = NULL;
					$pass = NULL;
					break;
			}
			if ($host && $db_name && $user) {
				$db_connection = &ADONewConnection('mysql');
				$db_connection->PConnect($host, $user, $pass, $db_name);

				return $db_connection;
			}
		}
		return false;
	}

	private function createUpdateCustomer() { //NO SE USA ESTO EN WEBRATIO
		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		if (!$this->loginAccount()) {
			$this->response('', 418);
		}

		// This process indicates if a minium data is correctly
		$minumDataCode = $this->minimumDataRequired($this->_request);
		if ($minumDataCode != 200) {
			$this->response('', $minumDataCode);
		}

		// This process indicates if a data is formatted correctly
		$control_code = $this->validateDataSet($this->_request);
		if ($control_code != 200) {
			$this->response('', $control_code);
		}

		$customer = new Customer($this->db);
		$customer->setEmail_cus($this->_request['email']);
		$existing_customer = $customer->getData(false);
		$customer->setSerial_cit($this->cleanField(utf8_decode($this->_request['city'])));
		$customer->setType_cus($this->cleanField(utf8_decode($this->_request['customerType'])));
		$customer->setDocument_cus($this->cleanField(utf8_decode($this->_request['idNumber'])));
		$customer->setFirstname_cus($this->cleanField(utf8_decode($this->_request['name'])));
		$customer->setLastname_cus($this->cleanField(utf8_decode($this->_request['lastname'])));
		$customer->setBirthdate_cus($this->cleanField(utf8_decode($this->_request['birthdate'])));
		$customer->setPhone1_cus($this->cleanField(utf8_decode($this->_request['phone1'])));
		$customer->setPhone2_cus($this->cleanField(utf8_decode($this->_request['phone2'])));
		$customer->setCellphone_cus($this->cleanField(utf8_decode($this->_request['cellphone'])));
		$customer->setEmail_cus($this->cleanField(utf8_decode($this->_request['email'])));
		$customer->setAddress_cus($this->cleanField(utf8_decode($this->_request['address'])));
		$customer->setRelative_cus($this->cleanField(utf8_decode($this->_request['relative'])));
		$customer->setRelative_phone_cus($this->cleanField(utf8_decode($this->_request['phonerelative'])));
		$customer->setVip_cus('0');
		$customer->setStatus_cus('ACTIVE');
		$customer->setPassword_cus(md5(GlobalFunctions::generatePassword(6, 8, false)));

		$error_code = $control_code;

		if (!$existing_customer) {
			$serial_cus = $customer->insert();
			if (!$serial_cus):
				$error_code = 409;
			endif;
		}else {
			$serial_cus = $customer->getSerial_cus();

			if (!$customer->update()):
				$error_code = 304;
			endif;
		}

		if ($error_code == 200) {
			$customer->setserial_cus($serial_cus);
		}

		$cus_data = array();
		$cus_data['serial'] = $customer->getSerial_cus();
		$cus_data['firstname'] = utf8_encode($customer->getFirstname_cus());
		$cus_data['lastname'] = utf8_encode($customer->getLastname_cus());
		$cus_data['birthdate'] = $customer->getBirthdate_cus();
		$cus_data['email'] = $customer->getEmail_cus();
		$cus_data['password'] = $customer->getPassword_cus();
		$cus_data['phone1'] = $customer->getPhone1_cus();
		$cus_data['phone2'] = $customer->getPhone2_cus();
		$cus_data['address'] = utf8_encode($customer->getAddress_cus());
		$cus_data['relative'] = utf8_encode($customer->getRelative_cus());
		$cus_data['relative_phone'] = $customer->getRelative_phone_cus();

		$this->response(array('data' => $cus_data), $error_code);
	}

	/*	 * ****************************VA
	 * @name: WSInsertCustomerUser
	 * This WS insert or update Customer as user. Used in Register web page.

	 */

	private function WSInsertCustomerUser() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$minumDataCode = $this->minimumDataRequiredWSInsertLogin($this->_request);
		if ($minumDataCode != 200) {
			$this->response('', $minumDataCode);
		}

		$customer = new Customer($this->_db);
		$customer->setEmail_cus($this->cleanField(utf8_encode($this->_request['email'])));
		$existing_customer = Customer::getExistCustomerByUser($this->_db, $this->_request['email']);
		$customer->setSerial_cit($this->cleanField(utf8_encode($this->_request['city'])));
		$customer->setType_cus('PERSON');
		$customer->setFirstname_cus($this->cleanField(utf8_encode($this->_request['name'])));
		$customer->setLastname_cus($this->cleanField(utf8_encode($this->_request['lastname'])));
		$customer->setDocument_cus($this->cleanField(utf8_encode($this->_request['idNumber'])));
                $customer->setBirthdate_cus($this->cleanField($this->_request['birthdate']));
		$customer->setVip_cus('0');
		$customer->setStatus_cus('ACTIVE');
		if ($this->_request['password']) {
			$customer->setPassword_cus(md5(utf8_encode($this->_request['password'])));
		}

		if (!$existing_customer) {
			$serial_cus = $customer->insert();
			if (!$serial_cus) {
				$this->response(array('status' => "Failed", "msg" => "NO INSERT CUSTOMER"), 200);
			} else {
                                $customer->setserial_cus($serial_cus);
                                $customer->getData();

                                $cus_data = array();
                                $cus_data['id'] = $customer->getSerial_cus();
                                $cus_data['document'] = $customer->getDocument_cus();
                                $cus_data['firstname'] = utf8_encode($customer->getFirstname_cus());
                                $cus_data['lastname'] = utf8_encode($customer->getLastname_cus());
                                $cus_data['birthdate'] = $customer->getBirthdate_cus();
                                $cus_data['email'] = $customer->getEmail_cus();
                                $cus_data['password'] = $customer->getPassword_cus();
                                $cus_data['phone1'] = $customer->getPhone1_cus();
                                $cus_data['phone2'] = $customer->getPhone2_cus();
                                $cus_data['address'] = utf8_encode($customer->getAddress_cus());
                                $cus_data['relative'] = utf8_encode($customer->getRelative_cus());
                                $cus_data['relative_phone'] = $customer->getRelative_phone_cus();

                                $this->response(array('status' => "Correct", "msg" => "INSERT CUSTOMER", "data" => $cus_data), 200);
				
			}
		} else {
			$customer->setserial_cus($existing_customer);
			$customer->getData();

			$cus_data = array();
			$cus_data['id'] = $customer->getSerial_cus();
			$cus_data['firstname'] = utf8_encode($customer->getFirstname_cus());
			$cus_data['lastname'] = utf8_encode($customer->getLastname_cus());
			$cus_data['birthdate'] = $customer->getBirthdate_cus();
			$cus_data['email'] = $customer->getEmail_cus();
			$cus_data['password'] = $customer->getPassword_cus();
			$cus_data['phone1'] = $customer->getPhone1_cus();
			$cus_data['phone2'] = $customer->getPhone2_cus();
			$cus_data['address'] = utf8_encode($customer->getAddress_cus());
			$cus_data['relative'] = utf8_encode($customer->getRelative_cus());
			$cus_data['relative_phone'] = $customer->getRelative_phone_cus();

			$this->response(array('status' => "Failed", "msg" => "CUSTOMER EXIST", "data" => $cus_data), 200);
		}
	}

	/*	 * ****************************VA
	 * @name: WSInsertUpdateCustomer
	 * This WS insert or update Customer. Used when customer insert o update more informacion in my bluecard,
	 *  or/and insert or update favorites
	 */

	private function WSInsertUpdateCustomer() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		// This process indicates if a minium data is correctly
		$minumDataCode = $this->minimumDataRequired($this->_request);
		if ($minumDataCode != 200) {
			$this->response(array('status' => "Failed", "msg" => "DATA MINIMUM"), $minumDataCode);
		}

		// This process indicates if a data is formatted correctly
		$control_code = $this->validateDataSet($this->_request);
		if ($control_code != 200) {
			$this->response(array('status' => "Failed", "msg" => "DATA VALIDATE"), $control_code);
		}
		//Validate receive id customer
		$useSerial = $this->_request['useSerial'];
		$customerId = $this->_request['idNumber'];
		$customerSerial = $this->_request['serialCustomer'];

		$customer = new Customer($this->_db);
		if ($useSerial == 'TRUE') {
			$customer->setserial_cus($this->cleanField(utf8_encode($customerSerial)));
		} else {
			$customer->setDocument_cus($this->cleanField(utf8_encode($customerId)));
		}

		$existing_customer = $customer->getData(false);

		$customer->setSerial_cit($this->cleanField(utf8_encode($this->_request['city'])));
		$customer->setDocument_cus($this->cleanField(utf8_encode($this->_request['idNumber'])));
		$customer->setType_cus($this->cleanField(utf8_encode($this->_request['customerType'])));
		$customer->setFirstname_cus($this->cleanField(utf8_encode($this->_request['name'])));
		$customer->setEmail_cus($this->cleanField(utf8_encode($this->_request['email'])));
		$customer->setLastname_cus($this->cleanField(utf8_encode($this->_request['lastname'])));
		$customer->setBirthdate_cus($this->cleanField(utf8_encode($this->_request['birthdate'])));
		$customer->setPhone1_cus($this->cleanField(utf8_encode($this->_request['phone1'])));
		$customer->setPhone2_cus($this->cleanField(utf8_encode($this->_request['phone2'])));
		$customer->setCellphone_cus($this->cleanField(utf8_encode($this->_request['cellphone'])));
		$customer->setAddress_cus($this->cleanField(utf8_encode($this->_request['address'])));
		$customer->setRelative_cus($this->cleanField(utf8_encode($this->_request['relative'])));
		$customer->setRelative_phone_cus($this->cleanField(utf8_encode($this->_request['phonerelative'])));
		$customer->setVip_cus('0');
		$customer->setStatus_cus('ACTIVE');

		$error_code = $control_code;

		if (!$existing_customer) {
			$customer->setPassword_cus(md5(GlobalFunctions::generatePassword(6, 8, false)));

			$serial_cus = $customer->insert();
			if (!$serial_cus):
				$this->response(array('status' => "Failed", "msg" => "NO INSERT CUSTOMER"), 409);
			endif;
		}else {
			$serial_cus = $customer->getSerial_cus();
			if (!$customer->update()):
				$this->response(array('status' => "Failed", "msg" => "NO UPDATE CUSTOMER"), 409);
			endif;
		}

		if ($error_code == 200) {
			$customer->setserial_cus($serial_cus);
		}
		if ($serial_cus) {
			$cus_data = array();
			$cus_data['id'] = $customer->getSerial_cus();
			$cus_data['document'] = $customer->getDocument_cus();
			$cus_data['firstname'] = utf8_encode($customer->getFirstname_cus());
			$cus_data['lastname'] = utf8_encode($customer->getLastname_cus());
			$cus_data['birthdate'] = $customer->getBirthdate_cus();
			$cus_data['email'] = $customer->getEmail_cus();
			$cus_data['password'] = $customer->getPassword_cus();
			$cus_data['phone1'] = $customer->getPhone1_cus();
			$cus_data['phone2'] = $customer->getPhone2_cus();
			$cus_data['address'] = utf8_encode($customer->getAddress_cus());
			$cus_data['relative'] = utf8_encode($customer->getRelative_cus());
			$cus_data['relative_phone'] = $customer->getRelative_phone_cus();
			$this->response(array('data' => $cus_data, 'status' => "Correct"), 200);
		} else {
			$this->response(array('data' => $cus_data, 'status' => "Incorrect"), 409);
		}
	}

	/*	 * ****************************VA
	 * @name: LisCustomerByDocument
	 * @return: Array whit all user information bu document.
	 * Used when customer insert your document and the information reload in data by card.

	 */

	private function WSListCustomerByDocument() {

//		if (!$this->validate_login_attempt()) {
//			$this->response('', 418);
//		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$useSerial = $this->_request['useSerial'];
		$customerId = $this->_request['id_Customer'];
		$customerSerial = $this->_request['serialCustomer'];

		$customerData = new Customer($this->_db);

		if ($useSerial == 'TRUE') {
			$customerData->setserial_cus($this->cleanField(utf8_encode($customerSerial)));
		} else {
			$customerData->setDocument_cus($this->cleanField(utf8_encode($customerId)));
		}
		$customerData->getData(false);
		$customer = Customer::getCustomerBySerial($this->_db, $customerData->getSerial_cus());

		if ($customer) {
			$customerSet = array();
			$temp_Array = array();

			foreach ($customer as $c) {
				$temp_Array['id'] = $c['serial_cus'];
				$temp_Array['document'] = $c['document_cus'];
				$temp_Array['firstName'] = utf8_encode($c['first_name_cus']);
				$temp_Array['lastName'] = utf8_encode($c['last_name_cus']);
				$temp_Array['birthdate_cus'] = ($c['birthdate_cus']);
				$temp_Array['address_cus'] = utf8_encode($c['address_cus']);
				$temp_Array['serial_cou'] = utf8_encode($c['serial_cou']);
				$temp_Array['serial_cit'] = utf8_encode($c['serial_cit']);
				$temp_Array['name_cou'] = utf8_encode($c['name_cou']);
				$temp_Array['name_cit'] = utf8_encode($c['name_cit']);
				$temp_Array['phone1_cus'] = ($c['phone1_cus']);
				$temp_Array['cellphone_cus'] = ($c['cellphone_cus']);
				$temp_Array['email_cus'] = utf8_encode($c['email_cus']);
				$temp_Array['relative_cus'] = utf8_encode($c['relative_cus']);
				$temp_Array['relative_phone_cus'] = $c['relative_phone_cus'];
				array_push($customerSet, $temp_Array);
			}
			$this->response($customerSet, 200);
		} else {
			$this->response(array('status' => "Failed", "msg" => "NO EXIST CUSTOMER"), 200);
		}
	}

	/*	 * ****************************VA
	 * @name: WSLoginCustomer
	 * @return: Login customer. Used when customer login in web page

	 */

	private function WSLoginCustomer() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$email = $this->_request['username'];
		$password = $this->_request['password'];

		$minumDataCode = $this->minimumDataRequiredWSSearchCustomer($this->_request);
		if ($minumDataCode != 200) {
			$this->response('', $minumDataCode);
		}

		$customerTemp = new Customer($this->_db);
		$customerTemp->setEmail_cus($email);
		$customerTemp->getData(false);
		if ($customerTemp->existsCustomerEmail($email, NULL)) {
			if (md5($password) == $customerTemp->getPassword_cus()) {
				if ($customerTemp->getStatus_cus() == 'ACTIVE') {
					$customer = Customer::getCustomerByUserAndPass($this->_db, $email, md5($password));

					if ($customer) {

						$customerSet = array();
						$temp_Array = array();

						foreach ($customer as $c) {
							$temp_Array['id'] = $c['serial_cus'];
							$temp_Array['document'] = $c['document_cus'];
							//                                        $temp_Array['firstName'] = utf8_encode($c['first_name_cus']);
							//                                        $temp_Array['lastName'] = utf8_encode($c['last_name_cus']);
							//                                        $temp_Array['birthdate_cus'] = ($c['birthdate_cus']);
							//                                        $temp_Array['address_cus'] = utf8_encode($c['address_cus']);
							//                                        $temp_Array['name_cou'] = utf8_encode($c['name_cou']);
							//                                        $temp_Array['name_cit'] = utf8_encode($c['name_cit']);
							//                                        $temp_Array['phone1_cus'] = ($c['phone1_cus']);
							//                                        $temp_Array['cellphone_cus'] = ($c['cellphone_cus']);
							//                                        $temp_Array['email_cus'] = utf8_encode($c['email_cus']);
							//                                        $temp_Array['relative_cus'] = utf8_encode($c['relative_cus']);
							//                                        $temp_Array['relative_phone_cus'] = $c['relative_phone_cus'];
							array_push($customerSet, $temp_Array);
						}
						$this->response($customerSet, 200);
					}
				} else {
					$this->response(array('status' => "Failed", "msg" => "Usuario inactivo, por favor comuniquese con nuestras oficinas para mayor informaci�n."), 409);
				}
			} else {
				$this->response(array('status' => "Failed", "msg" => "Clave incorrrecta, por favor intente nuevamente."), 409);
			}
		} else {
			$this->response(array('status' => "Failed", "msg" => "Usuario/mail incorrecto, por favor intente nuevo."), 409);
		}
	}

	/*	 * ****************************VA
	 * @name: WSCompareEmail
	 * @return:Used to compare email.

	 */

	private function WSCompareEmail() {

		//if (!$this->validate_login_attempt()) {
		//	$this->response('', 418);
		//}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$email = $this->_request['username'];

		if (!$email) {
			$this->response(array('status' => "Failed", "msg" => "Insert Email"));
		}

		$customerTemp = new Customer($this->_db);
		$customerTemp->setEmail_cus($email);
		$customerTemp->getData(false);
		if ($customerTemp->existsCustomerEmail($email, NULL)) {
			if ($customerTemp->getStatus_cus() == 'ACTIVE') {
				$temp_Array = array();
				$temp_Array['id'] = $customerTemp->getSerial_cus();
				$temp_Array['document'] = $customerTemp->getDocument_cus();
				$temp_Array['exist'] = true;
				$this->response($temp_Array, 200);
			} else {
				$this->response(array('status' => "Failed", "msg" => "Usuario inactivo, por favor comuniquese con nuestras oficinas para mayor informaci�n."), 200);
			}
		} else {
			$this->response(array('status' => "Failed", "msg" => "Usuario/mail incorrecto, por favor intente nuevo."), 200);
		}
	}

	/*	 * ****************************VA
	 * @name: WSNewPassword
	 * @return: update password. Used when customer need update password.

	 */

	private function WSNewPassword() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}
//
		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$password = $this->_request['newpassword'];
		$useSerial = $this->_request['useSerial'];
		$customerId = $this->_request['id_Customer'];
		$customerSerial = $this->_request['serialCustomer'];

		$customerData = new Customer($this->_db);

		if ($useSerial == 'TRUE') {
			$customerData->setserial_cus($this->cleanField(utf8_encode($customerSerial)));
		} else {
			$customerData->setDocument_cus($this->cleanField(utf8_encode($customerId)));
		}

		if ($customerData->getData()) {
			$update = Customer::updatePassword($this->_db, $customerData->getSerial_cus(), $password);

			if ($update == true) {
				$this->response(array('status' => "Correct", "msg" => "Password Updated"), 409);
			} else {
				$this->response(array('status' => "Failed", "msg" => "Password No Updated"), 409);
			}
		} else {
			$this->response(array('status' => "Failed", "msg" => "Incorrect Data"), 409);
		}
	}

	private function WSSearchCustomerDataCardsRelated() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}
      
		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		//Phone queue option
		$queue_opt = $this->_request['caller'];
		//Customer
		$customerId = $this->_request['customerDNI'];
		$customerSerial = $this->_request['customerSerial'];
		//If cardNumber search for specific card and customer
		$cardNumber = $this->_request['cardNumber'];
		//If false search customer coincidences
		$customerSearchPattern = $this->_request['searchPattern'];
            
		if (is_numeric($queue_opt)) {
			$db =  $this->chooseConnection($queue_opt);
                
			if ($db){
				$customerData = new WSCustomer($db);

				if (trim($customerId) != '' || trim($customerSerial) != '') {
					//Search for customer data
					$customerData->setserialCustomer($this->cleanField(utf8_encode($customerSerial)));
					$customerData->setDocumentCustomer($this->cleanField(utf8_encode($customerId)));
					$customer = $customerData->getCustomerBySerialorDocument($db);
					//Must retrieve just a customer
					if (count($customer) == 1) {
						//CUSTOMER Cards
						$cards_by_customer = WSSales::getCardsByCustomer($db, $customerData->getSerial_cus());
					}
				} elseif (trim($cardNumber) != '') {
					$cards_by_customer = WSSales::getCardInfoByNumber($db, $this->cleanField(utf8_encode($cardNumber)));
					if (is_array($cards_by_customer) && isset($cards_by_customer[0]['serial_cus'])) {
						//CUSTOMER Cards
						$customerData->setserialCustomer($this->cleanField(utf8_encode($cards_by_customer[0]['serial_cus'])));
						$customer = $customerData->getCustomerBySerialorDocument($db);
					}
				} elseif (trim($customerSearchPattern) != '') {
					$customer_opt = $customerData->getCustomers($this->cleanField(utf8_encode($customerSearchPattern)));
					if (count($customer_opt) == 1) {
						$customer = $customer_opt;
						foreach ($customer as $custom) {
							$serial_cus = $custom['serial_cus'];
						}
						//CUSTOMER Cards
						$cards_by_customer = WSSales::getCardsByCustomer($db, $serial_cus);
					}
				}
				
				$customerSet = array();
				//POPULATE CUSTOMER DATA
				if ($customer) {
					$customerSet['CUSTOMER'] = array();
					$temp_Array = array();

					foreach ($customer as $key => $c) {
						$temp_Array['id'] = $c['serial_cus'];
						$temp_Array['document'] = $c['document_cus'];
						$temp_Array['firstName'] = utf8_encode($c['first_name_cus']);
						$temp_Array['lastName'] = utf8_encode($c['last_name_cus']);
						$temp_Array['birthdate_cus'] = $c['birthdate_cus'];
						$temp_Array['address_cus'] = utf8_encode($c['address_cus']);
						$temp_Array['serial_cou'] = utf8_encode($c['serial_cou']);
						$temp_Array['serial_cit'] = utf8_encode($c['serial_cit']);
						$temp_Array['name_cou'] = utf8_encode($c['name_cou']);
						$temp_Array['name_cit'] = utf8_encode($c['name_cit']);
						$temp_Array['phone1_cus'] = $c['phone1_cus'];
						$temp_Array['cellphone_cus'] = $c['cellphone_cus'];
						$temp_Array['email_cus'] = utf8_encode($c['email_cus']);
						$temp_Array['emergency_contact'] = utf8_encode($c['relative_cus']);
						$temp_Array['emergency_phone'] = utf8_encode($c['relative_phone_cus']);
						array_push($customerSet['CUSTOMER'], $temp_Array);
					}
				} else {
					$this->response(array('status' => "Failed", "msg" => "NO EXIST CUSTOMER"), 200);
				}

				//POPULATE CUSTOMER CARDS
				if ($cards_by_customer) {
					$customerSet['CARDS'] = array();

					foreach ($cards_by_customer as $c) {
						$temp_Array = array();
						$temp_Array['serial_sal'] = $c['serial_sal'];
						$temp_Array['card_number_sal'] = $c['card_number_sal'];
						$temp_Array['serial_pro'] = $c['serial_pro'];
						$temp_Array['name_pbl'] = utf8_encode($c['name_pbl']);
						$temp_Array['serial_cou'] = $c['serial_cou'];
						if($c['serial_trl']){
							$temp_Array['begin_date_sal'] = $c['begin_date_trl'];
						$temp_Array['end_date_sal'] = $c['end_date_trl'];
						}else{
							$temp_Array['begin_date_sal'] = $c['begin_date_sal'];
						$temp_Array['end_date_sal'] = $c['end_date_sal'];
						}
						$temp_Array['name_cou'] = utf8_encode($c['name_cou']);
						
						$temp_Array['serial_cnt'] = $c['serial_cnt'];
						$temp_Array['free_sal'] = $c['free_sal'];
						$temp_Array['serial_trl'] = $c['serial_trl'];
						$temp_Array['card_number_trl'] = $c['card_number_trl'];
						$temp_Array['serial_con'] = $c['serial_con'];
						$temp_Array['status_sal'] = $c['status_sal'];
						$temp_Array['serial_inv'] = $c['serial_inv'];
						$temp_Array['number_inv'] = $c['number_inv'];
						$temp_Array['status_inv'] = $c['status_inv'];
						$comments = '';
						if($c['status_inv'] == "STAND-BY") {
							if($c['days_to_dueDate']<0) {
								$comments = 'La factura de la venta no ha sido pagada y está fuera de los días de crédito.';
							} else {
								$comments = 'La factura de la venta no ha sido pagada pero está dentro de los días de crédito.';
							}
						}
						if($c['days_since_emition'] < 48) {
							$comments = 'La tarjeta fue emitida hace menos de 48 horas.';
						}
						if($c['status_sal'] == "EXPIRED") {
							$comments = 'Tenga en cuenta que esta tarjeta ya ha caducado.';
						}
						$temp_Array['comments'] = $comments;
						
						$insured = array();
						if ($customerSet['CUSTOMER'][0]['id'] != $c['serial_cus']){
							$customer_aux = new WSCustomer($db);
							$customer_aux->setserialCustomer($this->cleanField(utf8_encode($c['serial_cus'])));
							$beneficiary = $customer_aux->getCustomerBySerialorDocument($db);
							foreach ($beneficiary as $cust) {
								$insured['id'] = $cust['serial_cus'];
								$insured['document'] = $cust['document_cus'];
								$insured['firstName'] = utf8_encode($cust['first_name_cus']);
								$insured['lastName'] = utf8_encode($cust['last_name_cus']);
								$insured['birthdate_cus'] = ($cust['birthdate_cus']);
								$insured['address_cus'] = utf8_encode($cust['address_cus']);
								$insured['serial_cou'] = utf8_encode($cust['serial_cou']);
								$insured['serial_cit'] = utf8_encode($cust['serial_cit']);
								$insured['name_cou'] = utf8_encode($cust['name_cou']);
								$insured['name_cit'] = utf8_encode($cust['name_cit']);
								$insured['phone1_cus'] = $cust['phone1_cus'];
								$insured['cellphone_cus'] = $cust['cellphone_cus'];
								$insured['email_cus'] = utf8_encode($cust['email_cus']);
							}
						} else{
								$insured['id'] = $customerSet['CUSTOMER'][0]['serial_cus'];
								$insured['document'] = $customerSet['CUSTOMER'][0]['document'];
								$insured['firstName'] = $customerSet['CUSTOMER'][0]['firstName'];
								$insured['lastName'] = $customerSet['CUSTOMER'][0]['lastName'];
								$insured['birthdate_cus'] = $customerSet['CUSTOMER'][0]['birthdate_cus'];
								$insured['address_cus'] = $customerSet['CUSTOMER'][0]['address_cus'];
								$insured['serial_cou'] = $customerSet['CUSTOMER'][0]['serial_cou'];
								$insured['serial_cit'] = $customerSet['CUSTOMER'][0]['serial_cit'];
								$insured['name_cou'] = $customerSet['CUSTOMER'][0]['name_cou'];
								$insured['name_cit'] = $customerSet['CUSTOMER'][0]['name_cit'];
								$insured['phone1_cus'] = $customerSet['CUSTOMER'][0]['phone1_cus'];
								$insured['cellphone_cus'] = $customerSet['CUSTOMER'][0]['cellphone_cus'];
								$insured['email_cus'] = $customerSet['CUSTOMER'][0]['email_cus'];
						}
						$temp_Array['beneficiary'] = $insured;
						/*Dependents*/
						$dependents = array();
						$cus_temp_arr = array();
						$cards_beneficiaries = WSSales::getCardBeneficiaryDependents($db, $c['card_number_sal']);
						if (is_array($cards_beneficiaries) && count($cards_beneficiaries) > 0) {
							foreach ($cards_beneficiaries as $ky => $cus) {
								if ($c['serial_cus'] != $cus['serial_cus']){
									$cus_temp_arr['id'] = $cus['serial_cus'];
									$cus_temp_arr['document'] = $cus['document_cus'];
									$cus_temp_arr['firstName'] = utf8_encode($cus['first_name_cus']);
									$cus_temp_arr['lastName'] = utf8_encode($cus['last_name_cus']);
									$cus_temp_arr['birthdate_cus'] = $cus['birthdate_cus'];
									$cus_temp_arr['address_cus'] = utf8_encode($cus['address_cus']);
									$cus_temp_arr['serial_cou'] = utf8_encode($cus['serial_cou']);
									$cus_temp_arr['serial_cit'] = utf8_encode($cus['serial_cit']);
									$cus_temp_arr['name_cou'] = utf8_encode($cus['name_cou']);
									$cus_temp_arr['name_cit'] = utf8_encode($cus['name_cit']);
									$cus_temp_arr['phone1_cus'] = $cus['phone1_cus'];
									$cus_temp_arr['cellphone_cus'] = $cus['cellphone_cus'];
									$cus_temp_arr['email_cus'] = utf8_encode($cus['email_cus']);
									array_push($dependents, $cus_temp_arr);
								}
							}
						}
						$temp_Array['dependents'] = $dependents;
						/*Open files*/
						$files = array();
						$files_temp_arr = array();
						$fileCards = WSSales::getCardFileByCardNumber($db,utf8_encode($c['card_number_sal']));
						if (is_array($fileCards) && count($fileCards) > 0) {
							foreach ($fileCards as $ite => $fle) {
								$files_temp_arr['file_number'] = $fle['serial_fle'];
								$files_temp_arr['country_id'] = $fle['serial_cou'];
								$files_temp_arr['country'] = utf8_encode($fle['name_cou']);
								$files_temp_arr['insured'] = utf8_encode($fle['name_cus']);
								$files_temp_arr['reason'] = utf8_encode($fle['cause_fle']);
								$files_temp_arr['begin_date'] = $fle['creation_date_fle'];
								$files_temp_arr['status'] = $fle['status_fle'];
								array_push($files, $files_temp_arr);
							}
						}
						$temp_Array['files'] = $files;
						array_push($customerSet['CARDS'], $temp_Array);
					}
				}

				$this->response($customerSet, 200);
		
			} else {
				$this->response(array('data' => 'Cannot connnect to database'), 404);
			}
		} else {
			$this->response(array('data' => 'Incomplete data'), 404);
		}
	}

	/*	 * VA
	 * @name minimumDataRequired
	 * @param array $data
	 * @return boolean
	 */

	private function minimumDataRequired($data) {
		$case_code = 200;

		if (!is_array($data)) {
			$case_code = 650;
		} elseif (!isset($data['city'])) {
			$case_code = 651;
		} elseif (!isset($data['customerType'])) {
			$case_code = 652;
		} elseif (!isset($data['idNumber'])) {
			$case_code = 653;
		} elseif (!isset($data['name'])) {
			$case_code = 654;
		} elseif (!isset($data['lastname'])) {
			$case_code = 655;
		} elseif (!isset($data['birthdate'])) {
			$case_code = 656;
		} elseif (!isset($data['phone1'])) {
			$case_code = 657;
		} elseif (!isset($data['email'])) {
			$case_code = 659;
		} elseif (!isset($data['relative'])) {
			$case_code = 660;
		} elseif (!isset($data['phonerelative'])) {
			$case_code = 661;
		} elseif (!isset($data['address'])) {
			$case_code = 662;
		}

		return $case_code;
	}

	private function minimumDataRequiredWSSearchCustomer($data) {
		$case_code = 200;

		if (!is_array($data)) {
			$case_code = 650;
		} elseif (!isset($data['username'])) {
			$case_code = 651;
		} elseif (!isset($data['password'])) {
			$case_code = 652;
		}
		return $case_code;
	}

	private function minimumDataRequiredWSInsertLogin($data) {
		$case_code = 200;

		if (!is_array($data)) {
			$case_code = 650;
		} elseif (!isset($data['email'])) {
			$case_code = 651;
		} elseif (!isset($data['city'])) {
			$case_code = 652;
		} elseif (!isset($data['name'])) {
			$case_code = 653;
		} elseif (!isset($data['lastname'])) {
			$case_code = 654;
		}
		return $case_code;
	}

	/*	 * VA
	 * @name validateDataSet
	 * @param array $data
	 * @return boolean
	 */

	private function validateDataSet($data) {
		$step_code_validation = 200;

		if (!in_array($data['customerType'], $this->customerTypes)) {
			$step_code_validation = 611;
		} elseif (!is_string($data['city'])) {
			$step_code_validation = 600;
		} elseif (!is_string($data['idNumber'])) {
			$step_code_validation = 601;
		} elseif (!is_string($data['name'])) {
			$step_code_validation = 602;
		} elseif (!is_string($data['lastname'])) {
			$step_code_validation = 603;
		} elseif (!strstr($data['birthdate'], '/')) {
			$step_code_validation = 604;
		} elseif (!is_string($data['phone1'])) {
			$step_code_validation = 605;
		} elseif (!is_string($data['cellphone'])) {
			$step_code_validation = 606;
		} elseif (!is_string($data['email'])) {
			$step_code_validation = 607;
		} elseif (!is_string($data['relative'])) {
			$step_code_validation = 608;
		} elseif (!is_string($data['phonerelative'])) {
			$step_code_validation = 609;
		} elseif (!is_string($data['address'])) {
			$step_code_validation = 610;
		}

		return $step_code_validation;
	}

	// Get all customers and companions associated with sales.
	private function WSGetSaleCustomersZoho()
	{
		$queue_opt = 7;
		$customers = array();

		// if (!$this->validate_login_attempt()) {
		// 	$this->response('', 418);
		// }

		if ($this->get_request_method() != "GET") {
			$this->response('', 406);
		}

		if (is_numeric($queue_opt)) {
			$db =  $this->chooseConnection($queue_opt);
			if ($db) {
				$sales = WSSales::getSaleCustomersZoho($db);
				if (is_array($sales) && !empty($sales)) {
					foreach ($sales as $sale) {
						$companions = [];
						$saleCompanions = WSSales::getSaleCompanionsZoho($db, $sale['serial_sal']);
						if (is_array($saleCompanions) && !empty($saleCompanions)) {
							foreach ($saleCompanions as $saleCompanion) {
								$companions[] = [
									'saleId' => $saleCompanion['serial_sal'],
									'customerId' => $saleCompanion['serial_cus'],
									'documentNumber' => $saleCompanion['document_cus'],
									'name' => $saleCompanion['name'],
									'birthDate' => $saleCompanion['birth_date'],
									'gender' => $saleCompanion['gender_cus'],
								];
							}
						}

						$customers[] = [
							'saleId' => $sale['serial_sal'],
							'customerId' => $sale['serial_cus'],
							'policyNumber' => $sale['card_number_sal'],
							'documentNumber' => $sale['document_cus'],
							'name' => $sale['name'],
							'birthDate' => $sale['birth_date'],
							'gender' => $sale['gender_cus'],
							'status' => $sale['status_sal'],
							'companions' => count($companions) > 0 ? $companions : null,
						];
					}

					function utf8ize($data) 
					{
               			if (is_array($data)) {
                    		foreach ($data as $key => $value) {
                        		$data[$key] = utf8ize($value);
                    		}
                		} elseif (is_string($data)) {
                    		return utf8_encode($data);
                		}
               			 return $data;
            		}

					$data = utf8ize($customers);
            		
            		// Crear respuesta
            		$response = [
                		"success" => true,
                		"data" => $data,
                		"message" => "Se encontraron registros",
                		"status" => 200
            		];
          
            
            		ini_set('output_buffering', 'On');
            		ob_start();
            		$this->response($data, 200);
           			ob_end_flush();

					return json_encode($response);
				} else {
					$this->response(array('data' => 'No se encontraron ventas'), 404);
				}
			} else {
				$this->response(array('data' => 'Cannot connnect to database'), 404);
			}
		} else {
			$this->response(array('data' => 'Incomplete data'), 404);
		}
	}


}

// Initiiate Library
$api = new API;
$api->processApi();
?>
