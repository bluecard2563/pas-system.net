<?php

/**
 * File: api
 * Author: Patricio Astudillo
 * Creation Date: 19-jun-2013
 * Last Modified: 19-jun-2016
 * Modified By: Valeria Andino
 */
ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net/:/var/www/pas-system.net/lib/smarty-2.6.22:/var/www/pas-system.net/lib/Excel/Classes/:/var/www/pas-system.net/lib/PDF/');
define('DOCUMENT_ROOT', '/var/www/pas-system.net/');
require_once(DOCUMENT_ROOT.'lib/config.inc.php');

//ini_set('include_path', ini_get('include_path') . ':/Applications/XAMPP/htdocs/RIMAC/lib/:/Applications/XAMPP/htdocs/RIMAC/lib/clases:/Applications/XAMPP/htdocs/RIMAC:/Applications/XAMPP/htdocs/RIMAC/lib/smarty-2.6.22');
//require_once(DOCUMENT_ROOT . 'lib/config.inc.php');

class API extends Rest {

	private $customerTypes = array('PERSON', 'LEGAL_ENTITY');

	public function __construct() {
		parent::__construct();	// Init parent contructor
	}

	public function processApi() {
		$func = strtolower(trim(str_replace("/", "", $_REQUEST['rquest'])));
		if ((int) method_exists($this, $func) > 0)
			$this->$func();
		else
			$this->response('', 404);	// If the method not exist with in this class, response would be "Page not found".
	}

	private function createUpdateCustomer() { //NO SE USA ESTO EN WEBRATIO
		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		if (!$this->loginAccount()) {
			$this->response('', 418);
		}

		// This process indicates if a minium data is correctly
		$minumDataCode = $this->minimumDataRequired($this->_request);
		if ($minumDataCode != 200) {
			$this->response('', $minumDataCode);
		}

		// This process indicates if a data is formatted correctly
		$control_code = $this->validateDataSet($this->_request);
		if ($control_code != 200) {
			$this->response('', $control_code);
		}

		$customer = new Customer($this->db);
		$customer->setEmail_cus($this->_request['email']);
		$existing_customer = $customer->getData(false);
		$customer->setSerial_cit($this->cleanField(utf8_decode($this->_request['city'])));
		$customer->setType_cus($this->cleanField(utf8_decode($this->_request['customerType'])));
		$customer->setDocument_cus($this->cleanField(utf8_decode($this->_request['idNumber'])));
		$customer->setFirstname_cus($this->cleanField(utf8_decode($this->_request['name'])));
		$customer->setLastname_cus($this->cleanField(utf8_decode($this->_request['lastname'])));
		$customer->setBirthdate_cus($this->cleanField(utf8_decode($this->_request['birthdate'])));
		$customer->setPhone1_cus($this->cleanField(utf8_decode($this->_request['phone1'])));
		$customer->setPhone2_cus($this->cleanField(utf8_decode($this->_request['phone2'])));
		$customer->setCellphone_cus($this->cleanField(utf8_decode($this->_request['cellphone'])));
		$customer->setEmail_cus($this->cleanField(utf8_decode($this->_request['email'])));
		$customer->setAddress_cus($this->cleanField(utf8_decode($this->_request['address'])));
		$customer->setRelative_cus($this->cleanField(utf8_decode($this->_request['relative'])));
		$customer->setRelative_phone_cus($this->cleanField(utf8_decode($this->_request['phonerelative'])));
		$customer->setVip_cus('0');
		$customer->setStatus_cus('ACTIVE');
		$customer->setPassword_cus(md5(GlobalFunctions::generatePassword(6, 8, false)));

		$error_code = $control_code;

		if (!$existing_customer) {
			$serial_cus = $customer->insert();
			if (!$serial_cus):
				$error_code = 409;
			endif;
		}else {
			$serial_cus = $customer->getSerial_cus();

			if (!$customer->update()):
				$error_code = 304;
			endif;
		}

		if ($error_code == 200) {
			$customer->setserial_cus($serial_cus);
		}

		$cus_data = array();
		$cus_data['serial'] = $customer->getSerial_cus();
		$cus_data['firstname'] = utf8_encode($customer->getFirstname_cus());
		$cus_data['lastname'] = utf8_encode($customer->getLastname_cus());
		$cus_data['birthdate'] = $customer->getBirthdate_cus();
		$cus_data['email'] = $customer->getEmail_cus();
		$cus_data['password'] = $customer->getPassword_cus();
		$cus_data['phone1'] = $customer->getPhone1_cus();
		$cus_data['phone2'] = $customer->getPhone2_cus();
		$cus_data['address'] = utf8_encode($customer->getAddress_cus());
		$cus_data['relative'] = utf8_encode($customer->getRelative_cus());
		$cus_data['relative_phone'] = $customer->getRelative_phone_cus();

		$this->response(array('data' => $cus_data), $error_code);
	}

	/*	 * ****************************VA
	 * @name: WSInsertCustomerUser
	 * This WS insert or update Customer as user. Used in Register web page.

	 */

	private function WSInsertCustomerUser() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$minumDataCode = $this->minimumDataRequiredWSInsertLogin($this->_request);
		if ($minumDataCode != 200) {
			$this->response('', $minumDataCode);
		}

		$customer = new Customer($this->_db);
		$customer->setEmail_cus($this->cleanField(utf8_encode($this->_request['email'])));
		$existing_customer = Customer::getExistCustomerByUser($this->_db, $this->_request['email']);
		$customer->setSerial_cit($this->cleanField(utf8_encode($this->_request['city'])));
		$customer->setType_cus('PERSON');
		$customer->setFirstname_cus($this->cleanField(utf8_encode($this->_request['name'])));
		$customer->setLastname_cus($this->cleanField(utf8_encode($this->_request['lastname'])));
		$customer->setDocument_cus($this->cleanField(utf8_encode($this->_request['idNumber'])));
		$customer->setBirthdate_cus($this->cleanField($this->_request['birthdate']));
		$customer->setVip_cus('0');
		$customer->setStatus_cus('ACTIVE');
		if($this->_request['password']){
			$customer->setPassword_cus(md5(utf8_encode($this->_request['password'])));
		}

		if (!$existing_customer) {
			$serial_cus = $customer->insert();
			if (!$serial_cus) {
				$this->response(array('status' => "Failed", "msg" => "NO INSERT CUSTOMER"), 200);
			} else {
				$customer->setserial_cus($serial_cus);
                                $customer->getData();

                                $cus_data = array();
                                $cus_data['id'] = $customer->getSerial_cus();
                                $cus_data['firstname'] = utf8_encode($customer->getFirstname_cus());
                                $cus_data['lastname'] = utf8_encode($customer->getLastname_cus());
                                $cus_data['birthdate'] = $customer->getBirthdate_cus();
                                $cus_data['email'] = $customer->getEmail_cus();
                                $cus_data['password'] = $customer->getPassword_cus();
                                $cus_data['phone1'] = $customer->getPhone1_cus();
                                $cus_data['phone2'] = $customer->getPhone2_cus();
                                $cus_data['address'] = utf8_encode($customer->getAddress_cus());
                                $cus_data['relative'] = utf8_encode($customer->getRelative_cus());
                                $cus_data['relative_phone'] = $customer->getRelative_phone_cus();

                                $this->response(array('status' => "Correct", "msg" => "INSERT CUSTOMER", "data" => $cus_data), 200);
			}
		} else {
			$customer->setserial_cus($existing_customer);
			$customer->getData();
			
			$cus_data = array();
			$cus_data['id'] = $customer->getSerial_cus();
			$cus_data['firstname'] = utf8_encode($customer->getFirstname_cus());
			$cus_data['lastname'] = utf8_encode($customer->getLastname_cus());
			$cus_data['birthdate'] = $customer->getBirthdate_cus();
			$cus_data['email'] = $customer->getEmail_cus();
			$cus_data['password'] = $customer->getPassword_cus();
			$cus_data['phone1'] = $customer->getPhone1_cus();
			$cus_data['phone2'] = $customer->getPhone2_cus();
			$cus_data['address'] = utf8_encode($customer->getAddress_cus());
			$cus_data['relative'] = utf8_encode($customer->getRelative_cus());
			$cus_data['relative_phone'] = $customer->getRelative_phone_cus();
			
			$this->response(array('status' => "Failed", "msg" => "CUSTOMER EXIST", "data"=>$cus_data), 200);
		}
	}

	/*	 * ****************************VA
	 * @name: WSInsertUpdateCustomer
	 * This WS insert or update Customer. Used when customer insert o update more informacion in my bluecard,
	 *  or/and insert or update favorites
	 */

	private function WSInsertUpdateCustomer() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		// This process indicates if a minium data is correctly
		$minumDataCode = $this->minimumDataRequired($this->_request);
		if ($minumDataCode != 200) {
			$this->response(array('status' => "Failed", "msg" => "DATA MINIMUM"), $minumDataCode);
		}

		// This process indicates if a data is formatted correctly
		$control_code = $this->validateDataSet($this->_request);
		if ($control_code != 200) {
			$this->response(array('status' => "Failed", "msg" => "DATA VALIDATE"), $control_code);
		}
		//Validate receive id customer
		$useSerial = $this->_request['useSerial'];
		$customerId = $this->_request['id_Customer'];
		$customerSerial = $this->_request['serialCustomer'];

		$customer = new Customer($this->_db);
		if ($useSerial == 'TRUE') {
			$customer->setserial_cus($this->cleanField(utf8_encode($customerSerial)));
		} else {
			$customer->setDocument_cus($this->cleanField(utf8_encode($customerId)));
		}

		$existing_customer = $customer->getData(false);

		$customer->setSerial_cit($this->cleanField(utf8_encode($this->_request['city'])));
		$customer->setDocument_cus($this->cleanField(utf8_encode($this->_request['idNumber'])));
		$customer->setType_cus($this->cleanField(utf8_encode($this->_request['customerType'])));
		$customer->setFirstname_cus($this->cleanField(utf8_encode($this->_request['name'])));
		$customer->setEmail_cus($this->cleanField(utf8_encode($this->_request['email'])));
		$customer->setLastname_cus($this->cleanField(utf8_encode($this->_request['lastname'])));
		$customer->setBirthdate_cus($this->cleanField(utf8_encode($this->_request['birthdate'])));
		$customer->setPhone1_cus($this->cleanField(utf8_encode($this->_request['phone1'])));
		$customer->setPhone2_cus($this->cleanField(utf8_encode($this->_request['phone2'])));
		$customer->setCellphone_cus($this->cleanField(utf8_encode($this->_request['cellphone'])));
		$customer->setAddress_cus($this->cleanField(utf8_encode($this->_request['address'])));
		$customer->setRelative_cus($this->cleanField(utf8_encode($this->_request['relative'])));
		$customer->setRelative_phone_cus($this->cleanField(utf8_encode($this->_request['phonerelative'])));
		$customer->setVip_cus('0');
		$customer->setStatus_cus('ACTIVE');

		$error_code = $control_code;

		if (!$existing_customer) {
			$customer->setPassword_cus(md5(GlobalFunctions::generatePassword(6, 8, false)));
			
			$serial_cus = $customer->insert();
			if (!$serial_cus):
				$this->response(array('status' => "Failed", "msg" => "NO INSERT CUSTOMER"), 409);
			endif;
		}else {
			$serial_cus = $customer->getSerial_cus();
			if (!$customer->update()):
				$this->response(array('status' => "Failed", "msg" => "NO UPDATE CUSTOMER"), 409);
			endif;
		}

		if ($error_code == 200) {
			$customer->setserial_cus($serial_cus);
		}
		if ($serial_cus) {
			$cus_data = array();
			$cus_data['id'] = $customer->getSerial_cus();
			$cus_data['document'] = $customer->getDocument_cus();
			$cus_data['firstname'] = utf8_encode($customer->getFirstname_cus());
			$cus_data['lastname'] = utf8_encode($customer->getLastname_cus());
			$cus_data['birthdate'] = $customer->getBirthdate_cus();
			$cus_data['email'] = $customer->getEmail_cus();
			$cus_data['password'] = $customer->getPassword_cus();
			$cus_data['phone1'] = $customer->getPhone1_cus();
			$cus_data['phone2'] = $customer->getPhone2_cus();
			$cus_data['address'] = utf8_encode($customer->getAddress_cus());
			$cus_data['relative'] = utf8_encode($customer->getRelative_cus());
			$cus_data['relative_phone'] = $customer->getRelative_phone_cus();
			$this->response(array('data' => $cus_data, 'status' => "Correct"), 200);
		} else {
			$this->response(array('data' => $cus_data, 'status' => "Incorrect"), 409);
		}
	}

	/*	 * ****************************VA
	 * @name: LisCustomerByDocument
	 * @return: Array whit all user information bu document.
	 * Used when customer insert your document and the information reload in data by card.

	 */

	private function WSListCustomerByDocument() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$useSerial = $this->_request['useSerial'];
		$customerId = $this->_request['id_Customer'];
		$customerSerial = $this->_request['serialCustomer'];

		$customerData = new Customer($this->_db);

		if ($useSerial == 'TRUE') {
			$customerData->setserial_cus($this->cleanField(utf8_encode($customerSerial)));
		} else {
			$customerData->setDocument_cus($this->cleanField(utf8_encode($customerId)));
		}
		$customerData->getData(false);
		$customer = Customer::getCustomerBySerial($this->_db, $customerData->getSerial_cus());

		if ($customer) {
			$customerSet = array();
			$temp_Array = array();

			foreach ($customer as $c) {
				$temp_Array['id'] = $c['serial_cus'];
				$temp_Array['document'] = $c['document_cus'];
				$temp_Array['firstName'] = utf8_encode($c['first_name_cus']);
				$temp_Array['lastName'] = utf8_encode($c['last_name_cus']);
				$temp_Array['birthdate_cus'] = ($c['birthdate_cus']);
				$temp_Array['address_cus'] = utf8_encode($c['address_cus']);
				$temp_Array['serial_cou'] = utf8_encode($c['serial_cou']);
				$temp_Array['serial_cit'] = utf8_encode($c['serial_cit']);
				$temp_Array['name_cou'] = utf8_encode($c['name_cou']);
				$temp_Array['name_cit'] = utf8_encode($c['name_cit']);
				$temp_Array['phone1_cus'] = ($c['phone1_cus']);
				$temp_Array['cellphone_cus'] = ($c['cellphone_cus']);
				$temp_Array['email_cus'] = utf8_encode($c['email_cus']);
				$temp_Array['relative_cus'] = utf8_encode($c['relative_cus']);
				$temp_Array['relative_phone_cus'] = $c['relative_phone_cus'];
				array_push($customerSet, $temp_Array);
			}
			$this->response($customerSet, 200);
		} else {
			$this->response(array('status' => "Failed", "msg" => "NO EXIST CUSTOMER"), 409);
		}
	}

	/*	 * ****************************VA
	 * @name: WSLoginCustomer
	 * @return: Login customer. Used when customer login in web page

	 */

	private function WSLoginCustomer() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$email = $this->_request['username'];
		$password = $this->_request['password'];

		$minumDataCode = $this->minimumDataRequiredWSSearchCustomer($this->_request);
		if ($minumDataCode != 200) {
			$this->response('', $minumDataCode);
		}

		$customerTemp = new Customer($this->_db);
		$customerTemp->setEmail_cus($email);
		$customerTemp->getData(false);
		if ($customerTemp->existsCustomerEmail($email, NULL)) {
			if (md5($password) == $customerTemp->getPassword_cus()) {
				if ($customerTemp->getStatus_cus() == 'ACTIVE') {
					$customer = Customer::getCustomerByUserAndPass($this->_db, $email, md5($password));

					if ($customer) {

						$customerSet = array();
						$temp_Array = array();

						foreach ($customer as $c) {
							$temp_Array['id'] = $c['serial_cus'];
							$temp_Array['document'] = $c['document_cus'];
							//                                        $temp_Array['firstName'] = utf8_encode($c['first_name_cus']);
							//                                        $temp_Array['lastName'] = utf8_encode($c['last_name_cus']);
							//                                        $temp_Array['birthdate_cus'] = ($c['birthdate_cus']);
							//                                        $temp_Array['address_cus'] = utf8_encode($c['address_cus']);
							//                                        $temp_Array['name_cou'] = utf8_encode($c['name_cou']);
							//                                        $temp_Array['name_cit'] = utf8_encode($c['name_cit']);
							//                                        $temp_Array['phone1_cus'] = ($c['phone1_cus']);
							//                                        $temp_Array['cellphone_cus'] = ($c['cellphone_cus']);
							//                                        $temp_Array['email_cus'] = utf8_encode($c['email_cus']);
							//                                        $temp_Array['relative_cus'] = utf8_encode($c['relative_cus']);
							//                                        $temp_Array['relative_phone_cus'] = $c['relative_phone_cus'];
							array_push($customerSet, $temp_Array);
						}
						$this->response($customerSet, 200);
					}
				} else {
					$this->response(array('status' => "Failed", "msg" => "Usuario inactivo, por favor comuniquese con nuestras oficinas para mayor informaci�n."), 409);
				}
			} else {
				$this->response(array('status' => "Failed", "msg" => "Clave incorrrecta, por favor intente nuevamente."), 409);
			}
		} else {
			$this->response(array('status' => "Failed", "msg" => "Usuario/mail incorrecto, por favor intente nuevo."), 409);
		}
	}

	/*	 * ****************************VA
	 * @name: WSCompareEmail
	 * @return:Used to compare email.

	 */

	private function WSCompareEmail() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$email = $this->_request['username'];

		if (!$email) {
			$this->response(array('status' => "Failed", "msg" => "Insert Email"));
		}

		$customerTemp = new Customer($this->_db);
		$customerTemp->setEmail_cus($email);
		$customerTemp->getData(false);
		if ($customerTemp->existsCustomerEmail($email, NULL)) {
			if ($customerTemp->getStatus_cus() == 'ACTIVE') {
				$temp_Array = array();
				$temp_Array['id'] = $customerTemp->getSerial_cus();
				$temp_Array['document'] = $customerTemp->getDocument_cus();
				$temp_Array['exist'] = true;
				$this->response($temp_Array, 200);
			} else {
				$this->response(array('status' => "Failed", "msg" => "Usuario inactivo, por favor comuniquese con nuestras oficinas para mayor informacion."), 200);
			}
		} else {
			$this->response(array('status' => "Failed", "msg" => "Usuario/mail incorrecto, por favor intente nuevo."), 200);
		}
	}

	/*	 * ****************************VA
	 * @name: WSNewPassword
	 * @return: update password. Used when customer need update password.

	 */

	private function WSNewPassword() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}
//
		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$password = $this->_request['newpassword'];
		$useSerial = $this->_request['useSerial'];
		$customerId = $this->_request['id_Customer'];
		$customerSerial = $this->_request['serialCustomer'];

		$customerData = new Customer($this->_db);

		if ($useSerial == 'TRUE') {
			$customerData->setserial_cus($this->cleanField(utf8_encode($customerSerial)));
		} else {
			$customerData->setDocument_cus($this->cleanField(utf8_encode($customerId)));
		}

		if ($customerData->getData()) {
			$update = Customer::updatePassword($this->_db, $customerData->getSerial_cus(), $password);

			if ($update == true) {
				$this->response(array('status' => "Correct", "msg" => "Password Updated"), 409);
			} else {
				$this->response(array('status' => "Failed", "msg" => "Password No Updated"), 409);
			}
		} else {
			$this->response(array('status' => "Failed", "msg" => "Incorrect Data"), 409);
		}
	}

	/*	 * VA
	 * @name minimumDataRequired
	 * @param array $data
	 * @return boolean
	 */

	private function minimumDataRequired($data) {
		$case_code = 200;

		if (!is_array($data)) {
			$case_code = 650;
		} elseif (!isset($data['city'])) {
			$case_code = 651;
		} elseif (!isset($data['customerType'])) {
			$case_code = 652;
		} elseif (!isset($data['idNumber'])) {
			$case_code = 653;
		} elseif (!isset($data['name'])) {
			$case_code = 654;
		} elseif (!isset($data['lastname'])) {
			$case_code = 655;
		} elseif (!isset($data['birthdate'])) {
			$case_code = 656;
		} elseif (!isset($data['phone1'])) {
			$case_code = 657;
		} elseif (!isset($data['email'])) {
			$case_code = 659;
		} elseif (!isset($data['relative'])) {
			$case_code = 660;
		} elseif (!isset($data['phonerelative'])) {
			$case_code = 661;
		} elseif (!isset($data['address'])) {
			$case_code = 662;
		}

		return $case_code;
	}

	private function minimumDataRequiredWSSearchCustomer($data) {
		$case_code = 200;

		if (!is_array($data)) {
			$case_code = 650;
		} elseif (!isset($data['username'])) {
			$case_code = 651;
		} elseif (!isset($data['password'])) {
			$case_code = 652;
		}
		return $case_code;
	}

	private function minimumDataRequiredWSInsertLogin($data) {
		$case_code = 200;

		if (!is_array($data)) {
			$case_code = 650;
		} elseif (!isset($data['email'])) {
			$case_code = 651;
		} elseif (!isset($data['city'])) {
			$case_code = 652;
		} elseif (!isset($data['name'])) {
			$case_code = 653;
		} elseif (!isset($data['lastname'])) {
			$case_code = 654;
		}
		return $case_code;
	}

	/*	 * VA
	 * @name validateDataSet
	 * @param array $data
	 * @return boolean
	 */

	private function validateDataSet($data) {
		$step_code_validation = 200;

		if (!in_array($data['customerType'], $this->customerTypes)) {
			$step_code_validation = 611;
		} elseif (!is_string($data['city'])) {
			$step_code_validation = 600;
		} elseif (!is_string($data['idNumber'])) {
			$step_code_validation = 601;
		} elseif (!is_string($data['name'])) {
			$step_code_validation = 602;
		} elseif (!is_string($data['lastname'])) {
			$step_code_validation = 603;
		} elseif (!strstr($data['birthdate'], '/')) {
			$step_code_validation = 604;
		} elseif (!is_string($data['phone1'])) {
			$step_code_validation = 605;
		} elseif (!is_string($data['cellphone'])) {
			$step_code_validation = 606;
		} elseif (!is_string($data['email'])) {
			$step_code_validation = 607;
		} elseif (!is_string($data['relative'])) {
			$step_code_validation = 608;
		} elseif (!is_string($data['phonerelative'])) {
			$step_code_validation = 609;
		} elseif (!is_string($data['address'])) {
			$step_code_validation = 610;
		}

		return $step_code_validation;
	}

}

// Initiiate Library
$api = new API;
$api->processApi();
?>