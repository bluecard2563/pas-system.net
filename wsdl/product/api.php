<?php

/**
 * File: Product
 * Author: Luis Salvador
 * Creation Date: 18-ene-2016
 * Last Modified:  18-ene-2016
 * Modified by: Valeria Andino C�lleri
 */
        //Produccion Pruebas
	//ini_set('include_path',ini_get('include_path').':/home/blueorgo/public_html/lib/:/home/blueorgo/public_html/lib/clases:/home/blueorgo/public_html/:/home/blueorgo/public_html/lib/smarty-2.6.22:/home/blueorgo/public_html/lib/Excel/Classes/:/home/blueorgo/public_html/lib/PDF/');
	//define('DOCUMENT_ROOT', '/home/blueorgo/public_html/lib/config.inc.php');
	//require_once(DOCUMENT_ROOT.'lib/config.inc.php');
	
	//XAMPP
	ini_set('include_path',ini_get('include_path').':/Applications/XAMPP/htdocs/RIMAC/lib/:/Applications/XAMPP/htdocs/RIMAC/lib/clases:/Applications/XAMPP/htdocs/RIMAC:/Applications/XAMPP/htdocs/RIMAC/lib/smarty-2.6.22');
	define('DOCUMENT_ROOT', '/Applications/XAMPP/htdocs/RIMAC/');
	require_once(DOCUMENT_ROOT.'lib/config.inc.php');
	
	class API extends Rest {
		private $ProductTypes = array('PRODUCT','LEGAL_ENTITY');
		
		public function __construct(){
			parent::__construct();				// Init parent contructor
		}
        
				public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
		
                        if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);				// If the method not exist with in this class, response would be "Page not found".
		}
		
        public function retrieveCityListForCountry(){
			
            if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			if(!$this->validate_login_attempt()){
				$this->response('Error when attempting to login', 418);
			}
			
			$cities = WSAccountsLib::getCitiesForCountry($this->_db, $this->_request['country']);
			
			if($cities){
				$citySet = array();
				$temp_array = array();

				foreach($cities as $c){
					$temp_array['ID'] = $c['serial_cit'];
					$temp_array['city'] = utf8_encode($c['name_cit']);

					array_push($citySet, $temp_array);
				}
			
				$this->response($citySet, 200);
			}
			
			$this->response(array('status' => "Failed", "msg" => "NO CITIES"), 204);
		}	
		
        /******************************VA
             * @name: WSListProductByCountry
             * @return: array product by country. Used in area Quotation
             */
	
        private function WSListProductByCountry (){   
                        
                if ($this->get_request_method() != "POST") {
                    $this->response('', 406);
                }

                if (!$this->validate_login_attempt()) {
                    $this->response('', 418);
                }

                $origin_country = $this->_request['origin'];
                $destination_country = $this->_request['destination'];
                $language = $this->_request['language'];

                $product = Product::getProductListByCountry($this->_db, $origin_country, $destination_country, $language);


                if ($product) {
                    $productSet = array();
                    $tempArray = array();

                    foreach ($product as $p) {
                        $tempArray['id'] = $p['serial_pxc'];
                        $tempArray['Name'] = utf8_encode($p['name_pbl']);
                        $tempArray['Description'] = utf8_encode($p['description_pbl']);
                        $tempArray['FreeKids'] = $p['children_pro'];
                        $tempArray['Web'] = $p['in_web_pro'];
                        $tempArray['Senior'] = $p['senior_pro'];
                        $tempArray['Flights'] = $p['flights_pro'];
                        $tempArray['EmissionCountry'] = $p['emission_country_pro'];
                        $tempArray['ExtrasMaxByProduct'] = $p['max_extras_pro'];
                        $tempArray['Spouse'] = $p['spouse_pro'];
                        $tempArray['Relative'] = $p['relative_pro'];
						$tempArray['AllowsServices'] = $p['has_services_pro'];
                        array_push($productSet, $tempArray);
                    }

                    $this->response($productSet, 200);
                }

                $this->response(array('status' => "Failed", "msg" => "NO PRODUCT"), 602);
            }

            /******************************VA
		 * @name: WSListProductByCountryAndTravelMotive 
		 * @return: array product by serial country and serial travel motive. Used in area Quotation
                 
		 */
	
        private function WSListProductByCountryAndTravelMotive() {

            if ($this->get_request_method() != "POST") {
                $this->response('', 406);
            }

            if (!$this->validate_login_attempt()) {
                $this->response('', 418);
            }

            $travelMotive_Id = $this->_request['travelMotive'];
            $destination_Id = $this->_request['destination'];
            $language = $this->_request['language'];
            $counter_Id = $this->_request['counter'];
            $origin_country = $this->_request['origin'];

            $product = Product::getListProductsByTravelMotiveAndDestination($this->_db, $travelMotive_Id, $counter_Id, $language, $destination_Id, $origin_country);


            if ($product) {
                $productSet = array();
                $tempArray = array();

                foreach ($product as $p) {
                    $tempArray['id'] = $p['serial_pxc'];
                    $tempArray['Name'] = utf8_encode($p['name_pbl']);
                    $tempArray['Description'] = utf8_encode($p['description_pbl']);
                    $tempArray['FreeKids'] = $p['children_pro'];
                    $tempArray['Web'] = $p['in_web_pro'];
                    $tempArray['Senior'] = $p['senior_pro'];
                    $tempArray['Flights'] = $p['flights_pro'];
                    $tempArray['EmissionCountry'] = $p['emission_country_pro'];
                    $tempArray['ExtrasMaxByProduct'] = $p['max_extras_pro'];
                    $tempArray['Spouse'] = $p['spouse_pro'];
                    $tempArray['Relative'] = $p['relative_pro'];
					$tempArray['AllowsServices'] = $p['has_services_pro'];
                    array_push($productSet, $tempArray);
                }

                $this->response($productSet, 200);
            }

            $this->response(array('status' => "Failed", "msg" => "NO EXIST PRODUCT"), 602);
    }

        /******************************VA
		 * @name: WSListProductByTravelMotive
		 * @return: array product for travel motive, used in area quotation
                 * POST: Country
		 */
	
        private function WSListProductByTravelMotive() {
            
            if ($this->get_request_method() != "POST") {
                $this->response('', 406);
            }

            if (!$this->validate_login_attempt()) {
                $this->response('', 418);
            }

            $travelMotive_Id = $this->_request['travelMotive'];
            $language = $this->_request['language'];
            $counter_Id = $this->_request['counter'];
                          

            $product = Product::getListProductsByTravelMotive($this->_db, $travelMotive_Id, $counter_Id, $language);


            if ($product) {
                $productSet = array();
                $tempArray = array();

                foreach ($product as $p) {
                    $tempArray['id'] = $p['serial_pxc'];
                    $tempArray['Name'] = utf8_encode($p['name_pbl']);
                    $tempArray['Description'] = utf8_encode($p['description_pbl']);
                    $tempArray['FreeKids'] = $p['children_pro'];
                    $tempArray['Web'] = $p['in_web_pro'];
                    $tempArray['Senior'] = $p['senior_pro'];
                    $tempArray['Flights'] = $p['flights_pro'];
                    $tempArray['EmissionCountry'] = $p['emission_country_pro'];
                    $tempArray['ExtrasMaxByProduct'] = $p['max_extras_pro'];
                    $tempArray['Spouse'] = $p['spouse_pro'];
                    $tempArray['Relative'] = $p['relative_pro'];
					$tempArray['AllowsServices'] = $p['has_services_pro'];
                    array_push($productSet, $tempArray);
                }

                $this->response($productSet, 200);
            }

            $this->response(array('status' => "Failed", "msg" => "NO PRODUCT"), 602);
        }

        /******************************VA
		 * @name: WSListProduct
		 * @return: array products, used counter and language. Used in quotation by products
                 
		 */
	
        private function WSListProduct (){   
                        
           /* if (!$this->validate_login_attempt()) {
                $this->response('', 418);
            }*/


            if ($this->get_request_method() != "POST") {
                $this->response('', 406);
            }

            // This process indicates if exist minimun data 
            $minumDataCode = $this->minimunDataRequiredWSlp($this->_request);
            if ($minumDataCode != 200) {
                $this->response('', $minumDataCode);
            }

            // This process indicates if a data is formatted correctly                  
            $control_code = $this->validateDataWSlp($this->_request);
            if ($control_code != 200) {
                $this->response('', $control_code);
            }

            $counter_Id = $this->_request['id_Counter'];
            $language = $this->_request['language'];

            $product = Product::getListProducts($this->_db, $counter_Id, $language);

            if ($product) {
                $productSet = array();
                $tempArray = array();

                foreach ($product as $p) {
                    $tempArray['id'] = $p['serial_pxc'];
                    $tempArray['Name'] = $p['name_pbl'];
                    $tempArray['Description'] = utf8_encode( $p['description_pbl']);
                    $tempArray['FreeKids'] = $p['children_pro'];
                    $tempArray['Web'] = $p['in_web_pro'];
                    $tempArray['Senior'] = $p['senior_pro'];
                    $tempArray['Flights'] = $p['flights_pro'];
                    $tempArray['EmissionCountry'] = $p['emission_country_pro'];
                    $tempArray['ExtrasMaxByProduct'] = $p['max_extras_pro'];
                    $tempArray['Spouse'] = $p['spouse_pro'];
                    $tempArray['Relative'] = $p['relative_pro'];
					$tempArray['AllowsServices'] = $p['has_services_pro'];
                    array_push($productSet, $tempArray);
                }

                $this->response($productSet, 200);
            }

            $this->response(array('status' => "Failed", "msg" => "NO EXIST PRODUCT"), 602);
        }  
               
         /******************************VA
		 * @name: WSPriceForDays
		 * @return: Price by days. No used 
                 
		 */
	
        private function WSPriceForDays (){   
            
            if (!$this->validate_login_attempt()) {
                $this->response('', 418);
            }

            if ($this->get_request_method() != "POST") {
                $this->response('', 406);
            }

            $startDate = $this->_request['startdate'];
            $endDate = $this->_request['enddate'];
            $SerialPbD = $this->_request['serialPbD'];
            $TotalDays = $this->_request['totaldays'];
            $serialpxc = $this->_request['serialPbC'];

            if (!isset($startDate, $endDay, $TotalDays)) {
                $this->response('', 418);
            }

            $days = GlobalFunctions::getDaysDiffDdMmYyyy($startDate, $endDate);

            if ($TotalDays == $days) {

                $product = PriceByPOS::getPriceForDays($this->_db, $SerialPbD, $days, $serialpxc);

                if ($product) {

                    $productSet = array();
                    $productSet ['price'] = $product ['price_ppc'];
                    $this->response($productSet, 200);
                }

                $this->response(array('status' => "Failed", "msg" => "NO PRICE"), 601);
            } Else {
                $this->response(array('status' => "Failed", "msg" => "NO DAYS"), 600);
            }
        }  
                
        /******************************VA
		 * @name: WSRelationShip
		 * @return: Return relation ship exist. Used in insert information by travel
         * 
                 
		 */
        private function WSRelationShip(){
                    
                        if (!$this->validate_login_attempt()) {
                                 $this->response('', 418);
                                }

                    
                        if($this->get_request_method() != "POST"){
                                    $this->response('',406);
                        }
                        
                                $id = array('SPOUSE', 'RELATIVE','OTHER');
                                $name = array('CONYUGUE', 'FAMILIAR','OTRO');
                                $productSet = array();
                                $tempArray = array();
                                $i =0;
                                        foreach($id as $p){
                                        $tempArray['id'] = $id[$i];
                                        $tempArray['Name'] = $name[$i];
                                        $i++;
                                        array_push($productSet, $tempArray);
                                          }  
                                         
                            $this->response($productSet, 200);
                        
                }
        
        /**VA
		 * @name minimumDataRequired
		 * @param array $data
		 * @return boolean 
		 */
                
        private function minimunDataRequiredWSlp($data_array) {
                    $case_code = 200;

                    if (!is_array($data_array)) {
                        $case_code = 650;
                    } elseif (!isset($data_array['id_Counter'])) {
                        $case_code = 651;
                    }elseif (!isset($data_array['language'])) {
                        $case_code = 652;
                    }
                                       
                    return $case_code;
                }
                
        private function validateDataWSlp($data_array){
                    
                    $code_validation = 200;
                    
                    if (!is_numeric($data_array['id_Counter'])){
                        $code_validation = 662;
                    }elseif (!is_numeric($data_array['language'])) {
                        $code_validation = 663;
                    }
                    return $code_validation;
                }
        //url para imagen:
                public function returnurl(){
                   //Debug::print_r($this->_request['serial_pro']);die;
                    // if($this->get_request_method() != "POST"){
                      //              $this->response('',406);
                        //}
						
				   if (!$this->validate_login_attempt()) {
                                 $this->response('', 418);
                                }                 
                                           
                        $serial_pro = $this->_request['serial_pro'];
                        $namepro = ProductbyLanguage::getNamepbl($this->_db,$serial_pro);
						//print_r($namepro);
                              
                        //$url = "https://www.pas-system.net/images/productos/".$namepro.".jpg";
                                 
                                         
                            $this->response(array("https://www.pas-system.net/images/productos/".$namepro.".jpg"), 200);
                }        
               
        }
       
	// Initiiate Library
	$api = new API;
	$api->processApi();
?>