<?php

/**
 * File: Tandi API
 * Author: Carlos Oberto
 * Creation Date: 29-05-2018
 * Last Modified: 
 * Modified By: 
 */
//	ini_set('include_path',ini_get('include_path').':/home/blueorgo/public_html/lib/:/home/blueorgo/public_html/lib/clases:/home/blueorgo/public_html/:/home/blueorgo/public_html/lib/smarty-2.6.22:/home/blueorgo/public_html/lib/Excel/Classes/:/home/blueorgo/public_html/lib/PDF/');
//	define('DOCUMENT_ROOT', '/home/blueorgo/public_html/lib/config.inc.php');

ini_set('include_path', ini_get('include_path') . ':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net/:/var/www/pas-system.net/lib/smarty-2.6.22:/var/www/pas-system.net/lib/Excel/Classes/:/var/www/pas-system.net/lib/PDF/');
define('DOCUMENT_ROOT', '/var/www/pas-system.net/');
require_once(DOCUMENT_ROOT . 'lib/config.inc.php');

//ini_set('include_path', ini_get('include_path') . "C:/xampp/htdocs/PLANETASSIST/lib/;C:/xampp/htdocs/PLANETASSIST/lib/clases/;C:/xampp/htdocs/PLANETASSIST/;C:/xampp/htdocs/PLANETASSIST/lib/smarty-2.6.22/;C:/xampp/htdocs/PLANETASSIST/lib/PDF");
//define('DOCUMENT_ROOT', "C:/xampp/htdocs/PLANETASSIST");
//require_once(DOCUMENT_ROOT . 'lib/config.inc.php');
//include_once DOCUMENT_ROOT . 'lib/erpLibrary.inc.php';

class API extends Rest {

    private $varBoolean = array('YES', 'NO');

    public function __construct() {
        parent::__construct();    // Init parent contructor
    }

    public function processApi() {
        $func = strtolower(trim(str_replace("/", "", $_REQUEST['rquest'])));
        if ((int) method_exists($this, $func) > 0)
            $this->$func();
        else
            $this->response('', 404);    // If the method not exist with in this class, response would be "Page not found".
    }

    //************************* PROCESS FUNCTIONS ********************
    /**
     * @access public
     * @author Carlos Oberto <coberto@planet-assist.net>
     * @process VoidInvoice
     */
    public function voidSale() {
        if (USE_TANDI) {

            if (!$this->validate_login_attempt()) {
                $this->response('', 418);
            }
            if ($this->get_request_method() != "POST") {
                $this->response('', 406);
            }
            //Obtain Data From WS
            $json = file_get_contents('php://input');
            $array = json_decode($json, true);

            if ($array) {
//                Debug::print_r($array);
//                Debug::var_dump($invTandi);die();
                $tandiLog = TandiLog::insert($this->_db, 'VOID_INVOICE', $array['id_factura'], $array);
                $id_factura = $array['id_factura'];
                $numero_factura = $array['numero_factura'];
                $tandi_id = $array['tandi_id'];
                //Get SerialMBC
                $serialMbc = TandiConnectionFunctions::getBASMbc($tandi_id);
                //Get SerialMan
                $serialMan = new ManagerbyCountry($this->_db, $serialMbc);
                $serialMan->getData();
                //Get Numero DBM
                $numero_dbm = DocumentByManager::retrieveSelfDBMSerial($this->_db, $serialMan->getSerial_man(), 'BOTH', 'INVOICE');

                $number_cn = $array['number_cn'];
                $id_cn = $array['id_cn'];
                $penalty_cn = $array['penalty_cn'];
                $amount_cn = $array['amount_cn'];
                $numero_tarjeta = $array['sales'];
                $user_request = $array['cedu_usuario_request'];
                $user_auth = $array['cedu_usuario_auth'];
                $obs_auth = $array['obs_sale'];
                $responseArray = array();
                $description_sales_log = array();

                if (!isset($id_factura) || (!isset($numero_factura)) || (!isset($numero_dbm)) || (!isset($number_cn)) || (!isset($id_cn) || (!isset($penalty_cn)) || (!isset($amount_cn)) || (!isset($numero_tarjeta)) || (!isset($user_request) || (!isset($user_auth)) || (!isset($obs_auth))))) {
                    //Update Log
                    TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing Data", 403);
                    //Response
                    $array = array('status' => "Failed", "msg" => "Missing Data", "code" => "403");
                    $this->response($array, 403);
                } else {

                    //Obatin Invoice
                    $invoice = TandiGlobalFunctions::getInvoiceByErp($this->_db, $id_factura);

                    if (!$invoice) {
                        if($numero_dbm == 116 || $numero_dbm == 139){
                            $numero_factura = substr($numero_factura, 1);
                            $numero_factura = '1'.$numero_factura;
                        }
                        if (!$invoice = TandiGlobalFunctions::getInvoices($this->_db, $numero_factura, $numero_dbm)) {
                            //Update Log
                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing Invoice", 405);
                            //Response
                            $array = array('status' => "Failed", "msg" => "Missing Invoice", "code" => "405");
                            $this->response($array, 405);
                        }
                    }



                    //                Debug::print_r($invoice);die();
                    //Check Invoice Payment
                    $invoicePayment = TandiGlobalFunctions::getInvoicePayment($this->_db, $invoice[0]['serial_inv']);

                    if ($invoicePayment) {
                        //Update Log
                        TandiLog::updateLog($this->_db, $tandiLog, "Error", "Invoice Payment", 412);
                        //Response
                        $array = array('status' => "Failed", "msg" => "Invoice Payment", "code" => "412");
                        $this->response($array, 412);
                    }

                    //Obtain Sales
                    foreach ($numero_tarjeta as $tarjetas) {
//                        Debug::print_r($sales);die();
                        foreach ($tarjetas as $sales) {

                            $sale = TandiGlobalFunctions::getSalesByInvoiceTandi($this->_db, $sales, $invoice[0]['serial_inv']);


                            if (!$sale) {
                                //Update Log
                                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing Sales | card_number_sal = " . $sales, 406);
                                //Response
                                $array = array('status' => "Failed", "msg" => "Missing Sales ! card_number_sal = " . $sales, "code" => "406");
                                $this->response($array, 406);
                            }
                            //Obtain users for Logs
                            $requestUser = TandiGlobalFunctions::getUserByDocument($this->_db, $user_request);
                            if (!$requestUser) {
                                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing Request User", 413);
                                //Response
                                $array = array("status" => "Warning", "msg" => "Missing Request User", "code" => "413");
                                array_push($responseArray, $array);
                                //Default User
                                $requestUser['serial_usr'] = 6888;
                            }
                            $authUser = TandiGlobalFunctions::getUserByDocument($this->_db, $user_auth);
                            if (!$authUser) {
//                                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing Auth User", 414);
                                //Response
                                $array = array("status" => "Warning", "msg" => "Missing Auth User", "code" => "413");
                                array_push($responseArray, $array);
                                //Default User
                                $authUser['serial_usr'] = 6888;
                            }

                            //Invoice Log
                            $invLog = new InvoiceLog($this->_db);
                            $invLog->setSerial_inv($invoice[0]['serial_inv']);
                            $invLog->setSerial_usr($requestUser['serial_usr']);
                            $invLog->setUsr_serial_usr($authUser['serial_usr']);
                            $invLog->setSerial_sal($sale[0]['serial_sal']);
                            $invLog->setIn_date_inl(date('Y-m-d H:i:s'));
                            $invLog->setIn_date_inl(date('Y-m-d H:i:s'));
                            $invLog->setstatus_inl('AUTHORIZED');
                            $invLog->setPenalty_fee_inl($penalty_cn);
                            $invLog->setObservation_inl($obs_auth);
                            $invLog->setVoid_sales_inl('YES');
                            //                    Debug::print_r($invLog);die();
                            if (!$invLog->insert()) {
                                //Update Log
//                                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Insert Invoice Log Error", 407);
                                $array = array("status" => "Warning", "msg" => "Insert Invoice Log Error", "code" => "407");
                                array_push($responseArray, $array);
                                //Response
//                                $this->response(array('status' => "Failed", "msg" => "Insert Invoice Log Error"), 407);
                            }

                            $authInvLog = new InvoiceLog($this->_db);
                            $authInvLog->setSerial_inv($invoice[0]['serial_inv']);
                            $authInvLog->setUsr_serial_usr($authUser['serial_usr']);
                            $authInvLog->setAuthorizing_obs_inl($obs_auth);
                            $authInvLog->changestatus('AUTHORIZED');

                            $inv = new Invoice($this->_db, $invoice[0]['serial_inv']);
                            $inv->getData();

                            $serial_mbc = $inv->getSerialMbc();
                            $mbc = new ManagerbyCountry($this->_db, $serial_mbc['serial_mbc']);
                            $mbc->getData();

                            $misc['db'] = $this->_db;
                            $misc['type'] = 'INVOICE';
                            $misc['serial'] = $invoice[0]['serial_inv'];
                            $misc['serial_man'] = $mbc->getSerial_man();
                            $misc['amount'] = $amount_cn;
                            $misc['paymentstatus'] = 'PAID';
                            $misc['cnote_number'] = $cnote_number;

                            $invoiceNew = new Invoice($this->_db, $invoice[0]['serial_inv']);
                            if($invoice[0]['total_inv'] == abs($amount_cn)){
                                if (!$invoiceNew->changestatus('VOID')) {
                                    //Update Log
                                    $array = array("status" => "Warning", "msg" => "Change Invoice status Error", "code" => "408");
                                    array_push($responseArray, $array);
//                                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Change Invoice status Error", 408);
                                    //Response
//                                $this->response(array('status' => "Failed", "msg" => "Change Invoice status Error"), 408);
                                }
                            }

                            //Credit Note
                            $serial_dbm = DocumentByManager::retrieveSelfDBMSerial($this->_db, $mbc->serial_man, 'BOTH', 'CREDIT_NOTE');

                            $cNote = new CreditNote($this->_db);
                            $cNote->setSerial_dbm($serial_dbm);
                            $cNote->setSerial_inv($invoice[0]['serial_inv']);
                            $cNote->setSerial_sal($sale[0]['serial_sal']);
                            $cNote->setstatus_cn('ACTIVE');
                            $cNote->setAmount_cn(abs($amount_cn));
                            $cNote->setNumber_cn($number_cn);
                            $cNote->setPayment_status_cn('PAID');
                            $cNote->setPenalty_fee_cn($penalty_cn);
                            if (!$cNote->insert()) {
                                //Update Log
                                $array = array("status" => "Warning", "msg" => "Change Credit Note Error", "code" => "409");
                                array_push($responseArray, $array);
//                                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Insert Credit Note Error", 409);
                                //Response
//                                $this->response(array('status' => "Failed", "msg" => "Insert Credit Note Error"), 409);
                            }

                            $saleVoid = new Sales($this->_db);
                            $sales = Array();

                            $saleVoid->setSerial_sal($sale[0]['serial_sal']);
                            if (!$saleVoid->changestatus('VOID')) {
                                //Update Log
                                $array = array("status" => "Warning", "msg" => "Change Sales status Error", "code" => "410");
                                array_push($responseArray, $array);
//                                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Change Sales status Error", 410);
                                //Response
//                                $this->response(array('status' => "Failed", "msg" => "Change Sales status Error"), 410);
                            } else {
                                $slg = new SalesLog($this->_db);
                                $slg->setType_slg('VOID_INVOICE');
                                $slg->setSerial_usr($requestUser['serial_usr']);
                                $slg->setUsr_serial_usr($authUser['serial_usr']);
                                $slg->setSerial_sal($sale[0]['serial_sal']);
                                $slg->setstatus_slg('AUTHORIZED');
                                $misc = array('old_status_sal' => 'REGISTERED',
                                    'new_status_sal' => 'VOID',
                                    'Description' => $obs_auth,
                                    'old_serial_inv' => $invoice[0]['serial_inv'],
                                    'new_serial_inv' => 'NULL',
                                    'Description_invoice' => $obs_auth);

                                $misc['global_info']['requester_usr'] = $requestUser['serial_usr'];
                                $misc['global_info']['request_date'] = date('d/m/Y');
                                $misc['global_info']['request_obs'] = $obs_auth;
                                $misc['global_info']['authorizing_usr'] = $authUser['serial_usr'];
                                $misc['global_info']['authorizing_date'] = date('d/m/Y');
                                $misc['global_info']['authorizing_obs'] = $obs_auth;

                                $slg->setDescription_slg(serialize($misc));
                                if (!$slg->insert()) {
                                    //Update Log
                                    $array = array("status" => "Warning", "msg" => "Insert Sales Log Error", "code" => "411");
                                    array_push($responseArray, $array);
//                                    TandiLog::updateLog($this->_db, $tandiLog, "Error", "Insert Sales Log Error", 411);
                                    //Response
//                                    $this->response(array('status' => "Failed", "msg" => "Insert Sales Log Error"), 411);
                                }
                                GlobalFunctions::refundInternationalFee($this->_db, $sale[0]['serial_sal']);
                                //*********************** DELETE ALL TRAVELS REGISTERED FOR VOID SALES
                                TravelerLog::voidAllTravelsRegistered($this->_db, $sale[0]['serial_sal']);
                            }
                        }
                        $array = array('status' => 'Success', 'msg' => 'Void Sale Sucess', 'code' => '200');
                        array_push($responseArray, $array);
                        //Update Log
                        TandiLog::updateLog($this->_db, $tandiLog, "SUCCESS", serialize($responseArray), 200);
                        //Response
                        $this->response($array, 200);
                    }
                }
            } else {
                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Array Error", 200);
                $this->response(array('status' => 'Error', 'msg' => 'Array Error', 'code' => '420'), 420);
            }
        } else {
            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Tandi Coneection OFF", 200);
            $this->response(array('status' => 'Error', 'msg' => 'Tandi Connection OFF', 'code' => '420'), 420);
        }
    }

    public function refundSale() {
        if (USE_TANDI) {
            if (!$this->validate_login_attempt()) {
                $this->response('', 418);
            }
            if ($this->get_request_method() != "POST") {
                $this->response('', 406);
            }

            //Obtain Data From WS
            $json = file_get_contents('php://input');
            $invTandi = json_decode($json, true);

            $tandiLog = TandiLog::insert($this->_db, 'REFUND', $invTandi['id_factura'], $invTandi);
            if ($invTandi) {
                //        Debug::print_r($invTandi);die();
                $id_factura = $invTandi['id_factura'];
                $numero_factura = $invTandi['numero_factura'];
                $tandi_id = $invTandi['tandi_id'];
                //Get SerialMBC
                $serialMbc = TandiConnectionFunctions::getBASMbc($tandi_id);
                //Get SerialMan
                $serialMan = new ManagerbyCountry($this->_db, $serialMbc);
                $serialMan->getData();
                //Get Numero DBM
                $numero_dbm = DocumentByManager::retrieveSelfDBMSerial($this->_db, $serialMan->getSerial_man(), 'BOTH', 'INVOICE');

                $number_cn = $invTandi['number_cn'];
                $id_cn = $invTandi['id_cn'];
                $penalty_cn = $invTandi['penalty_cn'];
                $amount_cn = $invTandi['amount_cn'];
                $numero_tarjeta = $invTandi['sales'];
                $user_request = $invTandi['cedu_usuario_request'];
                $user_auth = $invTandi['cedu_usuario_auth'];
                $obs_auth = $invTandi['obs_sale'];
                $responseArray = array();
                $description_sales_log = array();

                if (!isset($id_factura) || (!isset($numero_factura)) || (!isset($numero_dbm)) || (!isset($number_cn)) || (!isset($id_cn) || (!isset($penalty_cn)) || (!isset($amount_cn)) || (!isset($numero_tarjeta)) || (!isset($user_request) || (!isset($user_auth)) || (!isset($obs_auth))))) {
                    //Update Log
                    TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing Data", 403);
                    //Response
                    $this->response(array('status' => "Failed", "msg" => "Missing Data", "code" => "403"), 403);
                } else {
                    //Obatin Invoice
                    $invoice = TandiGlobalFunctions::getInvoiceByErp($this->_db, $id_factura);

//                                    Debug::print_r($invoice);die();
                    if (!$invoice) {
                        if($numero_dbm == 116 || $numero_dbm == 139){
                            $numero_factura = substr($numero_factura, 1);
                            $numero_factura = '1'.$numero_factura;
                        }
                        if (!$invoice = TandiGlobalFunctions::getInvoices($this->_db, $numero_factura, $numero_dbm)){
                            //Update Log
                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing Invoice", 405);
                            //Response
                            $this->response(array('status' => "Failed", "msg" => "Missing Invoice", "code" => "405"), 405);
                        }    
                    }
                    
                    //Invoice Payment
                    if($invoice[0]['status_inv'] == 'VOID'){
                        //Update Log
                        TandiLog::updateLog($this->_db, $tandiLog, "Error", "Invoice Not Void", 405);
                        //Response
                        $this->response(array('status' => "Failed", "msg" => "Invoice Not Void1", "code" => "405"), 405);
                        
                    }
                    //Obtain Sales
                    foreach ($numero_tarjeta as $sales) {
                        $sale = TandiGlobalFunctions::getSalesByInvoiceTandi($this->_db, $sales[0], $invoice[0]['serial_inv']);


                        if (!$sale) {
                            //Update Log
                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing Sales | card_number_sal = " . $sales[0], 406);
                            //Response
                            $this->response(array('status' => "Failed", "msg" => "Missing Sales ! card_number_sal = " . $sales[0], "code" => "406"), 406);
                        }
                        //                Debug::print_r($sale);die();
                        //Obtain users for Logs
                        $requestUser = TandiGlobalFunctions::getUserByDocument($this->_db, $user_request);
                        if (!$requestUser) {
//                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing Request User", 413);
                            //Response
                            $array = array("status" => "Warning", "msg" => "Missing Request User", "code" => "413");
                            array_push($responseArray, $array);
                            //Default User
                            $requestUser['serial_usr'] = 6888;
                        }
                        $authUser = TandiGlobalFunctions::getUserByDocument($this->_db, $user_auth);
                        if (!$authUser) {
//                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing Auth User", 414);
                            //Response
                            $array = array("status" => "Warning", "msg" => "Missing Auth User", "code" => "413");
                            array_push($responseArray, $array);
                            //Default User
                            $requestUser['serial_usr'] = 6888;
                        }

                        //Invoice Log
                        $invLog = new InvoiceLog($this->_db);
                        $invLog->setSerial_inv($invoice[0]['serial_inv']);
                        $invLog->setSerial_usr($requestUser['serial_usr']);
                        $invLog->setUsr_serial_usr($authUser['serial_usr']);
                        $invLog->setSerial_sal($sale[0]['serial_sal']);
                        $invLog->setIn_date_inl(date('Y-m-d H:i:s'));
                        $invLog->setIn_date_inl(date('Y-m-d H:i:s'));
                        $invLog->setstatus_inl('AUTHORIZED');
                        $invLog->setPenalty_fee_inl($penalty_cn);
                        $invLog->setObservation_inl($obs_auth);
                        $invLog->setVoid_sales_inl('NO');
                        if (!$invLog->insert()) {
                            $array = array("status" => "Warning", "msg" => "Insert Invoice Log Error", "code" => "407");
                            array_push($responseArray, $array);
                            //Update Log
//                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Insert Invoice Log Error", 407);
                            //Response
//                            $this->response(array('status' => "Failed", "msg" => "Insert Invoice Log Error"), 407);
                        }
                        //                    Debug::print_r($invLog);die();
                        //Refund
                        $refund = new Refund($this->_db);
                        $refund->setSerial_sal($sale[0]['serial_sal']);
                        $refund->setSerial_usr($requestUser['serial_usr']);
                        $refund->setstatus_ref('APROVED');
                        $refund->setTotalToPay_ref($invoice[0]['total_inv']);
                        $refund->setComments_ref(specialchars($obs_auth));
                        $refund->setType_ref('REGULAR');
                        $refund->setDiscount_ref($invoice[0]['discount_prcg_inv ']);
                        $refund->setOtherDiscount_ref($invoice[0]['other_dscnt_inv']);
                        $refund->setDocument_ref(serialize($obs_auth));
                        $refund->setReason_ref('SPECIAL');
                        $refund->setRetainType_ref('AMOUNT');
                        $refund->setRetainValue_ref($penalty_cn);
                        $refundID = $refund->insert();

                        $authInvLog = new InvoiceLog($this->_db);
                        $authInvLog->setSerial_inv($invoice[0]['serial_inv']);
                        $authInvLog->setUsr_serial_usr($authUser['serial_usr']);
                        $authInvLog->setAuthorizing_obs_inl($obs_auth);
                        $authInvLog->changestatus('AUTHORIZED');

                        $inv = new Invoice($this->_db, $invoice[0]['serial_inv']);
                        $inv->getData();

                        $serial_mbc = $inv->getSerialMbc();
                        $mbc = new ManagerbyCountry($this->_db, $serial_mbc['serial_mbc']);
                        $mbc->getData();

                        $misc['db'] = $this->_db;
                        $misc['type'] = 'INVOICE';
                        $misc['serial'] = $invoice[0]['serial_inv'];
                        $misc['serial_man'] = $mbc->getSerial_man();
                        $misc['amount'] = $amount_cn;
                        $misc['paymentstatus'] = 'PAID';
                        $misc['cnote_number'] = $cnote_number;

                        $invoiceNew = new Invoice($this->_db, $invoice[0]['serial_inv']);
                        if (!$invoiceNew->changestatus('PAID')) {
                            $array = array("status" => "Warning", "msg" => "Change Invoice Status Error", "code" => "408");
                            array_push($responseArray, $array);
                            //Update Log
//                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Change Invoice status Error", 408);
                            //Response
//                            $this->response(array('status' => "Failed", "msg" => "Change Invoice status Error"), 408);
                        }

                        //Credit Note
                        $serial_dbm = DocumentByManager::retrieveSelfDBMSerial($this->_db, $mbc->serial_man, 'BOTH', 'CREDIT_NOTE');

                        $cNote = new CreditNote($this->_db);
                        $cNote->setSerial_dbm($serial_dbm);
                        $cNote->setSerial_inv($invoice[0]['serial_inv']);
                        $cNote->setstatus_cn('ACTIVE');
                        $cNote->setAmount_cn(abs($amount_cn));
                        $cNote->setNumber_cn($number_cn);
                        $cNote->setPayment_status_cn('PAID');
                        $cNote->setPenalty_fee_cn($penalty_cn);
                        $cNote->setSerial_ref($refundID);
                        if (!$cNote->insert()) {
                            $array = array("status" => "Warning", "msg" => "Insert Credit Note Error", "code" => "409");
                            array_push($responseArray, $array);
                            //Update Log
//                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Insert Credit Note Error", 409);
                            //Response
//                            $this->response(array('status' => "Failed", "msg" => "Insert Credit Note Error"), 409);
                        }

                        $saleVoid = new Sales($this->_db);
                        $sales = Array();

                        $saleVoid->setSerial_sal($sale[0]['serial_sal']);
                        if (!$saleVoid->changestatus('REFUNDED')) {
                            $array = array("status" => "Warning", "msg" => "Change Sales status Error", "code" => "410");
                            array_push($responseArray, $array);
                            //Update Log
//                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Change Sales status Error", 410);
                            //Response
//                            $this->response(array('status' => "Failed", "msg" => "Change Sales status Error"), 410);
                        } else {
                            //Expire Traveler log registers for card
                            if (Sales::hasTravelsRegistered($this->_db, $sale[0]['serial_sal'])) {
                                TravelerLog::expireAllTravels($this->_db, $sale[0]['serial_sal']);
                            }

                            /* Put the Sale in REFUND status */
                            $sLog = new SalesLog($this->_db);
                            $sLog->setSerial_sal($sale[0]['serial_sal']);
                            $sLog->setSerial_usr($requestUser['serial_usr']);
                            $sLog->setUsr_serial_usr($authUser['serial_usr']);
                            $sLog->setType_slg('REFUNDED');
                            $sLog->setstatus_slg('AUTHORIZED');
                            $miscLog = array('old_status_sal' => $sale[0]['serial_sal'],
                                'new_status_sal' => 'REFUNDED',
                                'Description' => 'Refund of a sale.',
                                'Observations' => $obs_auth);

                            $miscLog['global_info']['requester_usr'] = 6888;
                            $miscLog['global_info']['request_date'] = date('d/m/Y');
                            $miscLog['global_info']['request_obs'] = $obs_auth;
                            $miscLog['global_info']['authorizing_usr'] = 6888;
                            $miscLog['global_info']['authorizing_date'] = date('d/m/Y');
                            $miscLog['global_info']['authorizing_obs'] = $obs_auth;

                            $sLog->setDescription_slg(serialize($miscLog));
                            if (!$sLog->insert()) {
                                $array = array("status" => "Warning", "msg" => "Insert Sales Log Error", "code" => "411");
                                array_push($responseArray, $array);
                                //Update Log
//                                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Insert Sales Log Error", 411);
                                //Response
//                                $this->response(array('status' => "Failed", "msg" => "Insert Sales Log Error"), 411);
                            }

                            GlobalFunctions::refundInternationalFee($this->_db, $sale[0]['serial_sal']);
                            /* End Sale Modification */

                            //Applied Comissions
                            $serial_sal = new AppliedComission($this->_db);
                            $serial_sal->setSerial_sal($sale[0]['serial_sal']);
                            if ($serial_sal->getPercentagesSale()) {
                                $misc['db'] = $this->_db;
                                GlobalFunctions::refundCommissions($misc, $sale[0]['serial_sal']);
                            }

                            //*********************** DELETE ALL TRAVELS REGISTERED FOR VOID SALES
                            TravelerLog::voidAllTravelsRegistered($this->_db, $sale[0]['serial_sal']);
                        }
                    }
                    $array = array('status' => 'Success', 'msg' => 'Refund Sale Sucess', 'code' => '200');
                    array_push($responseArray, $array);
                    //Update Log
                    TandiLog::updateLog($this->_db, $tandiLog, "SUCCESS", serialize($responseArray), 200);
                    //Response
                    $this->response($array, 200);
                }
            } else {
                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Array Error", 420);
                $this->response(array('status' => 'Error', 'msg' => 'Array Error', 'code' => '420'), 420);
            }
        } else {
            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Tandi Connection OFF", 420);
            $this->response(array('status' => 'Error', 'msg' => 'Tandi Connection OFF', 'code' => '420'), 420);
        }
    }

    public function paymentTandi() {
        //Check Tandi Usage
        if (USE_TANDI) {
            if(!$this->validate_login_attempt()){
                $this->response('', 418);
            }
            if ($this->get_request_method() != "POST") {
                $this->response('', 406);
            }    
            //Obtain WS Data
            $json = file_get_contents('php://input');
            $pay = json_decode($json, true);
//            Debug::print_r($pay);die();
            if ($pay) {
                $facturas = $pay['facturas'];
                foreach ($facturas as $factura) {
                    $_SESSION['serial_lang'] = 2;
                    $id_factura = $factura['id_factura'];
                    $numero_factura = $factura['numero_factura'];
                    $tandi_id = $factura['tandi_id'];
                    
                    //Get SerialMBC
                    $serialMbc = TandiConnectionFunctions::getBASMbc($tandi_id);

                    //Get SerialMan
                    $serialMan = new ManagerbyCountry($this->_db, $serialMbc);
                    $serialMan->getData();
                    
                    //Get Numero DBM
                    $numero_dbm = DocumentByManager::retrieveSelfDBMSerial($this->_db, $serialMan->getSerial_man(), 'BOTH', 'INVOICE');
                    
                    //Tandi Log
                    $tandiLog = TandiLog::insert($this->_db, 'PAYMENT', $id_factura, $pay);

                    $pagos = $factura['pagos'];
                    $paymentsList = array();
                    foreach ($pagos as $pago) {
//                            Debug::print_r($pago);die();

                        $type_payment = $pago['type_payment'];
                        $payment_card = $pago['card_payment'];
                        $payment_excess = $pago['payment_excess'];
                        $credit_note = $pago['credit_note'];
                        $payment_bank = $pago['payment_bank'];
                        $payment_other = $pago['payment_other'];
                        $payment_number = $pago['payment_number'];
                        $payment_date = $pay['payment_date'];
                        $payment_value = $pago['payment_value'];
                        //                $payment_pending = $pago['payment_pending'];
                        $obs_payment = $pay['obs_payment'];
                        $cedu_user = $pay['cedu_user'];
                        $status_pay = $factura['estado_payment'];
                        $payment_obs = $pago['obs_payment'];


                        if (!isset($id_factura) || !isset($numero_factura) || !isset($tandi_id) || !isset($type_payment) || !isset($payment_number) || !isset($payment_date) || !isset($payment_value) || !isset($obs_payment) || !isset($cedu_user) || !isset($status_pay)) {
                            //Update Log
                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing Data", 403);
                            //Response
                            $array = array('status' => "Failed", "msg" => "Missing Data", "code" => "403");
                            $this->response($array, 403);
                        }

                        //Payment Array
                        $paymentsListTemp['paymentType'] = $type_payment;
                        $paymentsListTemp['paymentDesc'] = $type_payment;
                        $paymentsListTemp['paymentCard'] = $payment_card;
                        $paymentsListTemp['paymentExcess'] = $payment_excess;
                        $paymentsListTemp['creditNotes'] = $credit_note;
                        $paymentsListTemp['paymentBank'] = $payment_bank;
                        $paymentsListTemp['paymentOther'] = $payment_other;
                        $paymentsListTemp['paymentDocument'] = $payment_number;
                        $paymentsListTemp['paymentVal'] = $payment_value;
                        $paymentsListTemp['paymentObs'] = $payment_obs;
                        array_push($paymentsList, $paymentsListTemp);
                    }
//                        Debug::print_r($paymentsList);die();
                    //Obatin Invoice
                    $invoice = TandiGlobalFunctions::getInvoiceByErp($this->_db, $id_factura);

                    if (!$invoice) {
                        if($numero_dbm == 116 || $numero_dbm == 139){
                            $numero_factura = substr($numero_factura, 1);
                            $numero_factura = '1'.$numero_factura;
                        }
                        if (!$invoice = TandiGlobalFunctions::getInvoices($this->_db, $numero_factura, $numero_dbm)) {
                            //Update Log
                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing Invoice", 405);
                            //Response
                            $this->response(array('status' => "Failed", "msg" => "Missing Invoice", "code" => "405"), 405);
                        }
                    }

                    //Check Invoice Status
                    $invoicePayment = TandiGlobalFunctions::getInvoicePayment($this->_db, $invoice[0]['serial_inv']);
//                    Debug::print_r($invoice);DIE();
                    if ($invoice[0]['status_inv'] != 'STAND-BY') {
                        //Update Log
                        TandiLog::updateLog($this->_db, $tandiLog, "Error", "Invoice Status Error", 412);
                        //Response
                        $this->response(array('status' => "Failed", "msg" => "Invoice Status Error" .$invoice[0]['erp_id'], "code" => "412"), 412);
                    }

                    $responseArray = array();
                    $user = TandiGlobalFunctions::getUserByDocument($this->_db, $cedu_user);
                    if (!$user) {
//                        TandiLog::updateLog($this->_db, $tandiLog, "Error", "Missing User", 413);
                        //Response
                        $array = array("status" => "Warning", "msg" => "Missing User", "code" => "413");
                        array_push($responseArray, $array);
                        //Default User
                        $user['serial_usr'] = 6888;
                    }

//                Debug::print_r($invoice);die();
                    $inv = new Invoice($this->_db, $invoice[0]['serial_inv']);
                    $inv->getData();
                    $total_to_pay = $inv->getTotal_inv();
                    $serial_inv = $inv->getSerial_inv();
                    $serial_pay = $inv->getSerial_pay();
                    $serial_dea = $inv->getSerial_dea();
                    $dealer = new Dealer($this->_db, $serial_dea);
                    $dealer->getData();

                    //Check Invoice Payment
                    if ($inv->getstatus_inv() == 'PAID') {
                        $array = array('status' => "Failed", "msg" => "Invoice Paid - ".$inv->getErp_id(), "code" => "412");
                        array_push($responseArray, $array);
                        //Update Log
                        TandiLog::updateLog($this->_db, $tandiLog, "Error", serialize($responseArray), 412);
                        //Response
//                        array_push($responseArray, $array);
                        $this->response($array, 412);
                    }
                    $last_payments = 0;
                    if ($paymentsList) {
                        foreach ($paymentsList as $pl) {
                            $last_payments += $pl['paymentVal']; //get the sum of the current payment and the partial payments
                        }

                        $total_payed += $last_payments;
                        $payment = new Payment($this->_db, $serial_pay);
                        $sale = new Sales($this->_db);
                        $payment->getData();
                        $payment->setTotal_to_pay_pay($total_to_pay);
                        $total_payed = $payment->getTotal_payed_pay() + $last_payments;
                        $payment->setTotal_payed_pay($total_payed);
                        $payment->setObservation_pay($obs_payment);
                        if ($serial_pay) {
                            $erp_id = $payment->getErp_id();
                            $payment_number = ($payment_number . '|' . $erp_id);
                            $payment->setErp_id($payment_number);
                        } else {
                            $payment->setErp_id($payment_number);
                        }
                        if ($last_payments > $total_to_pay) {
                            $payment->setExcess_amount_available_pay($last_payments - $total_to_pay);
                        } else {
                            $payment->setExcess_amount_available_pay(0);
                        }
                        $payment->setstatus_pay($status_pay);
                        if (!$serial_pay) {
                            if (!$serial_pay = $payment->insert()) {
                                //Update Log
//                                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Payment Error", 414);
                                //Response
                                $array = array("status" => "Warning", "msg" => "Payment Error", "code" => "414");
                                array_push($responseArray, $array);
//                                $this->response(array('status' => "Failed", "msg" => "Payment Error", "code" => "414"), 414);
                            }
                        } else {
                            if (!$payment->update()) {
                                $serial_pay = NULL;
                                //Update Log
//                                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Payment Error", 414);
                                //Response
                                $array = array("status" => "Warning", "msg" => "Payment Error", "code" => "414");
                                array_push($responseArray, $array);
//                                $this->response(array('status' => "Failed", "msg" => "Payment Error", "code" => "414"), 414);
                            }
                        }

                        if ($serial_pay) {
                            if (is_array($paymentsList)) {
                                foreach ($paymentsList as $pl) {
//                                Debug::print_r($pl);die();
                                    $paymentDetail = new PaymentDetail($this->_db);
                                    $paymentDetail->setSerial_pay($serial_pay);
                                    $paymentDetail->setType_pyf($pl['paymentType']);
                                    $paymentDetail->setAmount_pyf($pl['paymentVal']);
//                                Debug::print_r($payment_date);die();
                                    $date = date_format(date_create($payment_date), 'd/m/Y');
//                                Debug::print_r($date);die();
                                    $paymentDetail->setDate_pyf($date);
                                    $paymentDetail->setDocumentNumber_pyf($pl['paymentDocument']);
                                    $paymentDetail->setNumber_pyf($payment_number);

                                    if ($pl['paymentDesc']) {
                                        $paymentDetail->setComments_pyf($pl['paymentObs']);
                                    } elseif ($pl['paymentCard']) {
                                        if ($pl['paymentCard'] != 'OTHER') {
                                            $paymentDetail->setComments_pyf($pl['paymentCard']);
                                        } else {
                                            $paymentDetail->setComments_pyf($pl['paymentOther']);
                                        }
                                    } elseif ($pl['paymentBank']) {
                                        if ($pl['paymentBank'] != 'OTHER') {
                                            $paymentDetail->setComments_pyf($pl['paymentBank']);
                                        } else {
                                            $paymentDetail->setComments_pyf($pl['paymentOther']);
                                        }
                                    } elseif ($pl['paymentExcess']) {
                                        $paymentDetail->setComments_pyf('Pago en exceso');
                                    } elseif ($pl['creditNotes']) {
                                        $paymentDetail->setComments_pyf(utf8_decode('Nota de crédito'));
                                        $paymentDetail->setSerial_cn($pl['creditNotes']);
                                    }
                                    $paymentDetail->setSerial_usr($user['serial_usr']);
                                    if ($paymentDetail->insert()) {
                                        $error = 1;
                                    } else {
                                        $error = 3; //ERROR TO INSERT PAYMENT DETAIL
                                    }
                                    if ($pl['paymentType'] == 'EXCESS') {
                                        $pay = new Payment($this->_db, $pl['paymentExcess']);
                                        if ($pay->updateExcess_amount_available_pay($pl['paymentVal'])) {
                                            $error = 1;
                                        } else {
                                            $error = 3; //ERROR TO INSERT PAYMENT DETAIL
//                                            Update Log
//                                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Payment Deetail Error", 414);
                                            //Response
                                            $array = array("status" => "Warning", "msg" => "Missing Detail Error", "code" => "414");
                                            array_push($responseArray, $array);
//                                            $this->response(array('status' => "Failed", "msg" => "Payment Detail Error", "code" => "414"), 414);
                                        }
                                    } elseif ($pl['paymentType'] == 'CREDIT_NOTE') {
                                        $cn = new CreditNote($this->_db, $pl['creditNotes']);
                                        $cn->setPayment_status_cn('PAID');
                                        if ($cn->updatePayment_status_cn()) {
                                            $error = 1;
                                        } else {
                                            $error = 3; //ERROR TO INSERT PAYMENT DETAIL
                                            //Update Log
//                                            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Payment Detail Error", 414);
                                            //Response
                                            $array = array("status" => "Warning", "msg" => "Missing Detail Error", "code" => "414");
                                            array_push($responseArray, $array);
//                                            $this->response(array('status' => "Failed", "msg" => "Payment Detail Error", "code" => "414"), 414);
                                        }
                                    }
                                }

                                /* TODO change the invoice status */
//                                if ($error == 1) {
//                                    Debug::print_r($serial_invs);die();
//                                    $serial_invs = explode(',', $serial_invs);
                                    $invoice = new Invoice($this->_db, $serial_inv);
                                    $invoice->getData();
                                    if (!$invoice->getSerial_pay()) {//if the invoice doesn't have a payment assigned then assign the current payment
                                        $invoice->setSerial_pay($serial_pay); //update the invoice serial_pay
                                    }
                                    if (($status_pay == 'PAID' || $status_pay == 'EXCESS') && $invoice->getstatus_inv() != 'VOID') {
                                        $invoice->setstatus_inv('PAID'); //update the invoice status
                                        Payment::registerEfectivePaymentDate($this->_db, $serial_pay);

                                        //CHANGE ALL REGISTERED SALES TO 'ACTIVE' STATUS
                                        $saleList = $sale->getSalesByInvoice($serial_inv);

                                        if (sizeof($saleList) > 0) {
                                            foreach ($saleList as $s) {
                                                if ($s['status_sal'] == 'REGISTERED') {
                                                    $sale->setSerial_sal($s['serial_sal']);
                                                    if (!$sale->changestatus('ACTIVE')) { //THE SALES WEREN'T 
                                                        $error = 5;
//                                                        Update Log
//                                                        TandiLog::updateLog($this->_db, $tandiLog, "Error", "Change Sales Estatus Error", 406);
                                                        //Response
                                                        $array = array("status" => "Warning", "msg" => "Change Sales Estatus Error", "code" => "406");
                                                        array_push($responseArray, $array);
//                                                        $this->response(array('status' => "Failed", "msg" => "Change Sales Estatus Error", "code" => "406"), 406);
                                                        break 2;
                                                    }
                                                }
                                            }
                                        }

                                    }

                                    if (!$invoice->update()) {//update the serial_pay and  the status_inv
                                        $error = 4; //ERROR TO ASSIGN THE PAYMENT TO THE INVOICE
                                        //Update Log
//                                        TandiLog::updateLog($this->_db, $tandiLog, "Error", "Invoice Update Error", 405);
                                        //Response
                                         $array = array("status" => "Warning", "msg" => "Invoice Update Error", "code" => "405");
                                        array_push($responseArray, $array);
//                                        $this->response(array('status' => "Failed", "msg" => "Invoice Update Error", "code" => "408"), 408);
                                    }
                                    if($invoice->status_inv == 'PAID'){
                                    //Generate Commission
                                    $misc['db'] = $this->_db;
                                    $misc['type_com'] = 'IN';
                                    $misc['serial_fre'] = NULL;
                                    $misc['percentage_fre'] = NULL;
                                    //GlobalFunctions::calculateCommissions($misc, $serial_inv);
                                    GlobalFunctions::calculateBonus($misc, $serial_inv);
                                    }
//                                }
                            } else {
                                $error = 3; //ERROR TO INSERT PAYMENT DETAIL
                            }
                        } else {
                            $error = 2; //ERROR TO INSERT THE PAYMENT
                        }
                    }

//                    $responseArray = array();
//                    }
                    $array = array('status' => 'Success', 'msg' => 'Payment Sucess', 'code' => '200');
                    array_push($responseArray, $array);
                    TandiLog::updateLog($this->_db, $tandiLog, "SUCCESS", serialize($responseArray), 200);
                }
                    $array = array('status' => 'Success', 'msg' => 'Payment Sucess', 'code' => '200');
                    array_push($responseArray, $array);
                    TandiLog::updateLog($this->_db, $tandiLog, "SUCCESS", serialize($responseArray), 200);
                    $this->response($array, 200);
            } else {
                //Tandi Log
                $tandiLog = TandiLog::insert($this->_db, 'PAYMENT', $id_factura, $pay);
                TandiLog::updateLog($this->_db, $tandiLog, "Error", "Array Error", 420);
                $this->response(array('status' => 'Error', 'msg' => 'Tandi Connection OFF', 'code' => '420'), 420);
            }
        } else {
            //Tandi Log
            $tandiLog = TandiLog::insert($this->_db, 'PAYMENT', $id_factura, $pay);
            TandiLog::updateLog($this->_db, $tandiLog, "Error", "Tandi Connection OFF", 420);
            $this->response(array('status' => 'Error', 'msg' => 'Tandi Connection OFF', 'code' => '420'), 420);
        }
    }

}

//************************ API INITIALIZE **************
$api = new API;
$api->processApi();
?>
