<?php
/**
 * Description of api
 *
 * @author Valeria Andino Celleri
 */

//Produccion Pruebas
//ini_set('include_path',ini_get('include_path').':/home/blueorgo/public_html/lib/:/home/blueorgo/public_html/lib/clases:/home/blueorgo/public_html/:/home/blueorgo/public_html/lib/smarty-2.6.22:/home/blueorgo/public_html/lib/Excel/Classes/:/home/blueorgo/public_html/lib/PDF/');
//define('DOCUMENT_ROOT', '/home/blueorgo/public_html/lib/config.inc.php');
//require_once(DOCUMENT_ROOT.'lib/config.inc.php');

//XAMPP
ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net/:/var/www/pas-system.net/lib/smarty-2.6.22:/var/www/pas-system.net/lib/Excel/Classes/:/var/www/pas-system.net/lib/PDF/');
define('DOCUMENT_ROOT', '/var/www/pas-system.net/');
require_once(DOCUMENT_ROOT . 'lib/config.inc.php');

class API extends Rest {

	private $manyTravelsAllowedValues = array('TRUE', 'FALSE');

	public function __construct() {
		parent::__construct();	// Init parent contructor
	}

	public function processApi() {
		$func = strtolower(trim(str_replace("/", "", $_REQUEST['rquest'])));
		if ((int) method_exists($this, $func) > 0)
			$this->$func();
		else
			$this->response('', 404);	// If the method not exist with in this class, response would be "Page not found".
	}

	//************************* PROCESS FUNCTIONS ********************
	/**
	 * @access public
	 * @author Patricio Astudillo <pastudillo@planet-assist.net>
	 * @return Array Country List for Destinations/Origin
	 */
	public function WSretrieveCountryList() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		$countries = WSAccountsLib::getBaseCountries($this->_db);
		$countryData = array();
		$temp_array = array();

		foreach ($countries as $c) {
			$temp_array['ID'] = utf8_encode($c['serial_cou']);
			$temp_array['country'] = utf8_encode($c['name_cou']);
			$temp_array['zone'] = utf8_encode($c['name_zon']);

			array_push($countryData, $temp_array);
		}

		if ($countries) {
			$this->response($countryData, 200);
		}

		$this->response(array('status' => "Failed", "msg" => "NO COUNTRY"), 409);
	}

	/*	 * ****************************VA
	 * @name: ListListTravelMotives
	 * @return: array product by travel motives
	 * POST: travel motives
	 */

	private function WSListTravelMotives() {


		//if (!$this->validate_login_attempt()) {
		//	$this->response('', 418);
		//}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$product = WSTravelMotives::getAllTravelMotives($this->_db, true);

		if ($product) {

			$productData = array();
			$temp_array = array();

			foreach ($product as &$p) {
				$temp_array['ID'] = $p['serial_trm'];
				$temp_array['name'] = utf8_encode($p['name_trm']);
				array_push($productData, $temp_array);
			}

			$this->response($productData, 200);
		} Else {
			$this->response(array('status' => "Failed", "msg" => "NO MOTIVE"), 409);
		}
	}

	/*	 * ****************************VA
	 * @name: WSListProductByTravelMotive
	 * @return: array product by travel motive
	 * POST: travel motive
	 */

	private function WSListProductByTravelMotive() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		// This process indicates if exist minimun data 
		$minumDataCode = $this->minimunDataRequiredWSlpbm($this->_request);
		if ($minumDataCode != 200) {
			$this->response('', $minumDataCode);
		}
                
		// This process indicates if a data is formatted correctly                  
		$control_code = $this->validateDataWSlpbm($this->_request);
		if ($control_code != 200) {
			$this->response('', $control_code);
		}

		$counter_Id = $this->_request['id_Counter'];
		$language = $this->_request['language'];
		$motive_Id = $this->_request['id_Motive'];

		$product = Product::getListProductsByTravelMotive($this->_db, $motive_Id, $counter_Id, $language);

		if ($product) {
			$productSet = array();
			$tempArray = array();

			foreach ($product as $p) {
				$tempArray['id_pxc'] = $p['serial_pxc'];
				$tempArray['Name'] = utf8_encode($p['name_pbl']);
				$tempArray['Description'] = utf8_encode($p['description_pbl']);
				$tempArray['FreeKids'] = $p['children_pro'];
				$tempArray['Web'] = $p['in_web_pro'];
				$tempArray['Senior'] = $p['senior_pro'];
				$tempArray['Flights'] = $p['flights_pro'];
				$tempArray['EmissionCountry'] = $p['emission_country_pro'];
				$tempArray['ExtrasMaxByProduct'] = $p['max_extras_pro'];
				$tempArray['Spouse'] = $p['spouse_pro'];
				$tempArray['Relative'] = $p['relative_pro'];
				array_push($productSet, $tempArray);
			}

			$this->response($productSet, 200);
		}

		$this->response(array('status' => "Failed", "msg" => "NO PRODUCT"), 409);
	}

	/*	 * ****************************VA
	 * @name: WSQuotation
	 * @return: array of quotation

	 */

	public function WSQuotation() {

//		if (!$this->validate_login_attempt()) {
//			$this->response('', 418);
//                }

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
                }

		// This process indicates if exist minimun data
		$minumDataCode = WSQuotation::minimunDataRequiredWSQuotation($this->_request);
		if ($minumDataCode != 200) {
			$this->response('', $minumDataCode);
                }

		// This process indicates if a data is formatted correctly                  
		$control_code = WSQuotation::validateDataWSQuotation($this->_request);
		if ($control_code != 200) {
			$this->response('', $control_code);
               }


		$countryDestination_Id = $this->_request['id_Destination'];
		$travelMotive_Id = $this->_request['id_TravelMotive'];
		$startDate = $this->_request['startDate'];
		$endDate = $this->_request['endDate'];
		$totalKids = $this->_request['totalKids'];
		$totalAdults = $this->_request['totalAdults'];
		$totalSeniorAllowed = $this->_request['totalSeniorAllowed'];
		$language = $this->_request['language'];
		$manyTravel = $this->_request['manyTravel'];
		$countryOrigin_Id = $this->_request['id_Origin'];
		$counter_Id = $this->_request['id_Counter']; //Global Var
		$max_Days = $this->_request['max_days']; //Global Var


		if ($totalSeniorAllowed > 0) {
			$seniorAllowed = 'YES';
		} else {
			$seniorAllowed = 'NO';
		}

//		if ($manyTravel == 'TRUE') {
//			$totalAdults = $totalAdults + $totalKids;
//			$totalKids = 0;
//		}
//		if ($travelMotive_Id == 3 || $travelMotive_Id == 4) {
//			$totalAdults = $totalAdults + $totalKids;
//			$totalKids = 0;
//		}

		if ($countryDestination_Id == $countryOrigin_Id) {
			$productNational = 'YES';
			$totalAdults = $totalAdults + $totalKids;
			//$totalKids = 0;
		} else {
			$productNational = 'NO';
		}

		if ($totalKids > 0) {
			$kids = 'YES';
		} else {
			$kids = 'NO';
		}
                $totalTravellers = $totalAdults + $totalKids + $totalSeniorAllowed;
//                Debug::print_r($totalKids);die();
		$days = GlobalFunctions::getDaysDiffDdMmYyyy($startDate, $endDate);
		$zone = Zone::getZonesByCountry($this->_db, $countryDestination_Id);
//		$product = WSTravelMotivesByProduct::getBestProduct($this->_db, $language, $countryDestination_Id, $travelMotive_Id, $kids, $manyTravel, $seniorAllowed, $days, $productNational, $totalAdults, $zone, $counter_Id);
                $product = WSTravelMotivesByProduct::getBestProductWS($this->_db, $travelMotive_Id, $seniorAllowed, $kids, $countryDestination_Id, $days);
//                Debug::print_r($product);die();
		global $global_maxAgeAdults;
		global $global_minAgeHolder;
		if ($days > 0 && $days <= $max_Days) {
			if ($product) {

				$productSet = array();
				$temp_Array = array();

				foreach ($product as $key => $p) {
					$temp_Array['order'] = $key;
					$temp_Array['idProByCou'] = $p['serial_pxc'];
					$temp_Array['idPro'] = $p['serial_pro'];
					$namepro = ProductbyLanguage::getNamepbl($this->_db,$p['serial_pro']);
					$url="https://www.pas-system.net/images/productos/".str_replace(" ","",$namepro).".jpg";
					$temp_Array['urlImage'] = $url;
					$temp_Array['productName'] = utf8_encode($p['name_pbl']);
					$temp_Array['description'] = utf8_encode($p['description_pbl']);
					$percentageExtra = $p['percentage_extras_pxc'];
					$totalkidsbyproduct = $p['children_pro'];
					$proSenior = $p['senior_pro'];
					$maxExtras = $p['max_extras_pro'];
					//$spouse = $p['spouse_pro'];
					$relative = $p['relative_pro'];
					//$probydealer = $p['serial_pbd'];
					$temp_Array['maxBeneficiaryExtrasByCard'] = $maxExtras;
					$temp_Array['maxfreeKidsByCard'] = $totalkidsbyproduct;

					if ($proSenior == 'YES') {
//						$totalAdults = $totalSeniorAllowed;
//						$totalKids = 0;
						$global_maxAgeAdults = 100;
						$global_minAgeHolder = 75; // Var use by minium age for holder
					}
					if ($totalKids > 0){
					   $extrasRestricted = 'BOTH';
					}    

					$cardsWR = GlobalFunctions::getCardsTravelersDistribution2($maxExtras, $totalAdults, $totalKids, 0, $extrasRestricted, 0, $totalkidsbyproduct, $print = true,$totalSeniorAllowed);
					$cards = GlobalFunctions::getCardsTravelersDistribution2($maxExtras, $totalAdults, $totalKids, 0, $extrasRestricted, 0, $totalkidsbyproduct, $print = false,$totalSeniorAllowed);
					$totalCards = count($cards);
					$totalSpouse = 0;
					$totalChildren = 0;
					$totalFree = 0;
					$totalAdultsB = 0;
					foreach ($cards as $key => $value) {
						foreach ($value as $item) {
							switch ($item) {

								case 'S': $totalSpouse++;
									break;
								case 'C': $totalChildren++;
									break;
								case 'F': $totalFree++;
									break;
								case 'A': $totalAdultsB++;
									break;
							}
						}
					}

					$temp_Array['totalCardsByProduct'] = $totalCards;
					$temp_Array['totalFreeByProduct'] = $totalFree;
					$temp_Array['totalExtrasbByProduct'] = $totalAdultsB + $totalChildren + $totalSeniorAllowed;
					;
//                                        if($totalAdults < 1 && $totalSeniorAllowed > 0){
//                                                $priceA = WSTravelMotivesByProduct::getPriceByProduct($this->_db, $p['serial_pxc'], $countryOrigin_Id, $countryDestination_Id, '', true, $startDate, $endDate, $totalAdults, $totalKids, false, 0, $extrasRestricted, $language, $days, $wService = TRUE, $totalSeniorAllowed);
//                                        } else {

                                            $priceA = WSTravelMotivesByProduct::getPriceByProduct($this->_db, $p['serial_pxc'], $countryOrigin_Id, $countryDestination_Id, '', true, $startDate, $endDate, 1, 0, false, 0, $extrasRestricted, $language, $days, $wService = TRUE,0);
//
//                                        }
					if($totalTravellers > $maxExtras + 1){
                                            $temp_Array['priceByExtrasB'] = $priceA - (($priceA) * ($percentageExtra / 100));
                                        }else{
                                            $temp_Array['priceByExtrasB'] = 0;
                                        }
					$totalprice = WSTravelMotivesByProduct::getPriceByProduct($this->_db, $p['serial_pxc'], $countryOrigin_Id, $countryDestination_Id, '', true, $startDate, $endDate, $totalAdults, $totalKids, false, 0, $extrasRestricted, $language, $days, $wService = TRUE, $totalSeniorAllowed);
//
                                        if($totalTravellers > $maxExtras + 1){
                                            $temp_Array['priceHolderByCard'] = $priceA;
                                        }else{
                                            $temp_Array['priceHolderByCard'] = $totalprice;
                                        }
					$temp_Array['totalPriceByProduct'] = $totalprice;
					$temp_Array['extraDataArrayByCard'] = $cardsWR;

					array_push($productSet, $temp_Array);
				}
				$this->response($productSet, 200);
			} else {
				$this->response(array('status' => "Failed", "msg" => "NO EXIST PRODUCT"), 409);
			}
		} else {
			$this->response(array('status' => "Failed", "msg" => "NO EXIST PRODUCT FOR THIS DAYS"), 409);
		}
	}

	/*	 * ****************************VA
	 * @name: WSMailQuotation
	 * @return: email client
	 */

	public function WSMailQuotation() {

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}
		$misc['name'] = $this->_request['name'];
		$misc['email'] = $this->_request['email'];
		$misc['startDate'] = $this->_request['startDate'];
		$misc['endDate'] = $this->_request['endDate'];
		$misc['today'] = $this->_request['today'];
		$misc['data'] = json_decode($this->_request['data'],true);
		$misc['days'] = $this->_request['days'];
		$misc['destiny'] = $this->_request['destiny'];
		$misc['totalKids'] = $this->_request['totalKids'];
		$misc['totalAdults'] = $this->_request['totalAdults'];
		$misc['totalSeniorAllowed'] = $this->_request['totalSeniorAllowed'] + $this->_request['totalSeniorAllowedTwo'] +$this->_request['totalSeniorAllowedThree'];
		$misc['emailUsr'] = $this->_request['emailUsr'];
		$mailSend = GlobalFunctions::sendMail($misc, 'mailQuotation');
		$this->response($mailSend, 200);
	}

	public function  WSMailQuotationStep() {

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}
		$misc['step'] = $this->_request['step'];
		$misc['phoneCustomer'] = $this->_request['phoneCustomer'];
		$misc['emailCustomer'] = $this->_request['emailCustomer'];
		$misc['startDate'] = $this->_request['startDate'];
		$misc['endDate'] = $this->_request['endDate'];
		$misc['today'] = $this->_request['today'];
		$misc['data'] = $this->_request['data'];
		$misc['days'] = $this->_request['days'];
		$misc['destiny'] = $this->_request['destiny'];
		$misc['totalKids'] = $this->_request['totalKids'];
		$misc['totalAdults'] = $this->_request['totalAdults'];
		$misc['totalSeniorAllowed'] = $this->_request['totalSeniorAllowed'];
		$misc['totalPrice'] = $this->_request['totalPrice'];
		$mailSend = GlobalFunctions::sendMail($misc, 'mailQuotationStep');
		Sales::insertLogStep($this->_db, $this->_request['step'], $this->_request['phoneCustomer'],$this->_request['emailCustomer'],$this->_request['startDate'],
			$this->_request['endDate'],$this->_request['today'],$this->_request['data'],$this->_request['days'],$this->_request['destiny'],
			$this->_request['totalKids'],$this->_request['totalAdults'],$this->_request['totalSeniorAllowed'],$this->_request['totalPrice']);
		$this->response($mailSend, 200);
	}

	public function WSgetStatusUser() {
		if ($this->get_request_method() != "POST") {
			$this->response('',406);
		}
		$serial_usr = $this->_request['serial_usr'];
		$status_usr = User::getStatusUser($this->_db, $serial_usr);
		if ($status_usr){

			$userData = array();
			$temp_array = array();

			foreach ($status_usr as &$p) {
				$temp_array['serial_usr'] = $p['serial_usr'];
				$temp_array['serial_dea'] = $p['serial_dea'];
				$temp_array['username_usr'] = $p['username_usr'];
				$temp_array['document_usr'] = $p['document_usr'];
				$temp_array['first_name_usr'] = $p['first_name_usr'];
				$temp_array['last_name_usr'] = $p['last_name_usr'];
				$temp_array['phone_usr'] = $p['phone_usr'];
				$temp_array['email_usr'] = $p['email_usr'];
				$temp_array['status_usr'] = $p['status_usr'];
				array_push($userData, $temp_array);
			}
			$this->response($userData,200);
		}else{
			$this->response(array('status' => "Failed", "msg" => "INACTIVE USER"), 409);
		}
	}

	/*	 * ****************************VA
         * @name: WSQuotationLog
         *
         */
	public function WSQuotationLog() {
		if ($this->get_request_method() != "POST") {
			$this->response('',406);
		}
		$serial_dea = $this->_request['serial_dea'];
		$serial_usr = $this->_request['serial_usr'];
		$serial_cus = $this->_request['serial_cus'];
		$document_cus = $this->_request['document_cus'];
		$first_name_cus = $this->_request['first_name_cus'];
		$last_name_cus = $this->_request['last_name_cus'];
		$phone_cus =  $this->_request['phone_cus'];
		$email_cus = $this->_request['email_cus'];
		Sales::insertLogQuotation($this->_db,$serial_dea,$serial_usr,$serial_cus,$document_cus,$first_name_cus,$last_name_cus,$phone_cus,$email_cus);

	}

	/*	 * ****************************VA
	 * @name: WSQuotationByProduct
	 * @return: array of quotation

	 */

	private function WSQuotationByProduct() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		// This process indicates if exist minimun data 
		$minumDataCode = WSQuotation::minimunDataRequiredWSQuotationByProduct($this->_request);
		if ($minumDataCode != 200) {
			$this->response('', $minumDataCode);
		}

		// This process indicates if a data is formatted correctly                  
		$control_code = WSQuotation::validateDataWSQuotationByProduct($this->_request);
		if ($control_code != 200) {
			$this->response('', $control_code);
		}


		$countryDestination_Id = $this->_request['id_Destination'];
		$startDate = $this->_request['startDate'];
		$endDate = $this->_request['endDate'];
		$totalKids = $this->_request['totalKids'];
		$totalAdults = $this->_request['totalAdults'];
		$totalSeniorAllowed = $this->_request['totalSeniorAllowed'];
		$language = $this->_request['language'];
		$counter_Id = $this->_request['id_Counter']; //Global Var
		$max_Days = $this->_request['max_days']; //Global Var
		$countryOrigin_Id = $this->_request['id_Origin'];
		$productByCountry_Id = $this->_request['id_Product'];
                $days = GlobalFunctions::getDaysDiffDdMmYyyy($startDate, $endDate);
		$product = Product::getProduct($this->_db, $productByCountry_Id, $countryOrigin_Id, $countryDestination_Id, $language, $counter_Id, $totalKids, $totalAdults, $many_travel, $national, $totalSeniorAllowed, $days);
		global $global_maxAgeAdults;
		global $global_minAgeHolder;

	                  
                            if ($days <= $max_Days){
                                if ($product){
                                         
                                         $productSet = array();
                                         $temp_Array = array();
                                                
                                                 foreach ($product as $key => $p) {
                                                        $temp_Array['order'] = $key;
                                                        $temp_Array['idProByCou'] = $p['serial_pxc'];
                                                        $temp_Array['idPro'] = $p['serial_pro'];
                                                        $temp_Array['productName'] = utf8_encode($p['name_pbl']);
                                                        $temp_Array['description'] = utf8_encode($p['description_pbl']);
                                                        $percentageExtra = $p['percentage_extras_pxc'];
                                                        $totalkidsbyproduct = $p['children_pro'];
                                                        $proSenior = $p['senior_pro'];
                                                        $maxExtras = $p['max_extras_pro'];
                                                        //$spouse = $p['spouse_pro'];
                                                        //$relative = $p['relative_pro'];                   
                                                        //$probydealer = $p['serial_pbd'];
                                                        $temp_Array['maxBeneficiaryExtrasByCard'] = $maxExtras; 
                                                        $temp_Array['maxfreeKidsByCard']  = $totalkidsbyproduct;
                                                        
                                                        if ($proSenior == 'YES'){
                                                            $totalAdults = $totalSeniorAllowed;
                                                            $totalKids = 0;
                                                            $global_maxAgeAdults = 100;
                                                            $global_minAgeHolder = 72;
                                                        } 
                                                        //if ($totalKids > 0){
                                                        //   $extrasRestricted = 'CHILDREN';
                                                        //}    
                                                        $cardsWR = GlobalFunctions::getCardsTravelersDistribution2($maxExtras,$totalAdults,$totalKids,0,$extrasRestricted,0,$totalkidsbyproduct, $print = true);
                                                        $cards = GlobalFunctions::getCardsTravelersDistribution2($maxExtras,$totalAdults,$totalKids,0,$extrasRestricted,0,$totalkidsbyproduct, $print = false);
                                                        $totalCards = count($cards);
                                                        $totalSpouse =0;
                                                        $totalChildren =0;
                                                        $totalFree =0;
                                                        $totalAdultsB=0;
                                                        foreach ($cards as $key=>$value) {
                                                           foreach ($value as $item){
                                                            switch ($item){
                                                                
                                                                case 'S':  $totalSpouse++;
                                                                    break;
                                                                case 'C': $totalChildren++;
                                                                    break;
                                                                case 'F': $totalFree++;
                                                                    break;
                                                                case 'A': $totalAdultsB++;
                                                                    break;
                                                           }}
                                                                
                                                         }
                                                                                                         
                                                        $temp_Array['totalCardsByProduct']  = $totalCards;
                                                        $temp_Array['totalFreeByProduct'] = $totalFree;
                                                        $temp_Array['totalExtrasbByProduct'] = $totalAdultsB+$totalChildren;
                                                        $priceA = WSTravelMotivesByProduct::getPriceByProduct($this->_db,$p['serial_pxc'],$countryOrigin_Id, $countryDestination_Id,'',true, $startDate, $endDate,1, 0, false, 0,$extrasRestricted, $language, $days);
                                                        $temp_Array['priceHolderByCard']  = $priceA; 
                                                        if ($percentageExtra >0 ){
                                                        $temp_Array['priceByExtrasB']  = $priceA-(($priceA)*($percentageExtra/100));}
                                                        else 
                                                        {$temp_Array['priceByExtrasB']  = 0;}                                                           
                                                        $totalprice = WSTravelMotivesByProduct::getPriceByProduct($this->_db,$p['serial_pxc'],$countryOrigin_Id, $countryDestination_Id,'',true, $startDate, $endDate,$totalAdults, $totalKids, false, 0,$extrasRestricted, $language, $days);
                                                        $temp_Array['totalPriceByProduct']  = $totalprice; 
                                                        $temp_Array['extraDataArrayByCard']  = $cardsWR;
                                                        
                                                  array_push($productSet, $temp_Array);
    
                                                    }
                                                    $this->response($productSet,200);
                                }
                                else{ 
                                    $this->response(array('status' => "Failed", "msg" => "NO EXIST PRODUCT"), 409);
                                }
                             }
                             else{
                                 $this->response(array('status' => "Failed", "msg" => "NO EXIST PRODUCT FOR THIS DAYS"), 409);
                             } 
                                 
                 }
                 
      	
    /*	 * ****************************VA
	 * @name: WSFullQuotation
	 * @return: array of quotation. Used in edition

	 */

	private function WSFullQuotation() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$dataQuotation = $this->_request['dataQuotation'];
		$arrayDataQuotation = json_decode($dataQuotation);
		//print_r($jsonDataQuotation); die();       

		$response_array = array();

		if (is_array($arrayDataQuotation)) {
			foreach ($arrayDataQuotation as $key => $data) {
				$objData = get_object_vars($data);
				switch ($objData['logic']) {
					case 'TRAVEL_MOTIVE':
						$response_array[$key] = WSQuotation::getQuotationForTravelMotive($this->_db, $objData);

						break;
					case 'PRODUCT':
						$response_array[$key] = WSQuotation::getQuotationForProduct($this->_db, $objData);
						break;
				}
			}
		}
		$this->response($response_array, 200);
	}

	/*	 * ****************************VA
	 * @name: WSAdditionalBenefits
	 * @return: array of additional benefits
	 * 
	 */

	private function WSAdditionalBenefits() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}
		// This process indicates if exist minimun data 
		$minumDataCode = $this->minimunDataRequiredWSAddBen($this->_request);
		if ($minumDataCode != 200) {
			$this->response('', $minumDataCode);
		}

		$countryOrigin_Id = $this->_request['id_Origin'];
		$counter_Id = $this->_request['id_Counter'];
		$language = $this->_request['language'];

		$services = ServicesByDealer::getServicesByDealerWS($this->_db, $counter_Id, $language, $countryOrigin_Id);

		if ($services) {
			$productSet = array();
			$temp_Array = array();

			foreach ($services as $p) {
				$temp_Array['ID_serBycoun'] = $p['serial_sbc'];
				$temp_Array['ID_serByDea'] = $p['serial_sbd'];
				$temp_Array['name'] = utf8_encode($p['name_sbl']);
				$temp_Array['description'] = utf8_encode($p['description_sbl']);
				$temp_Array['type_fee'] = utf8_encode($p['fee_type_ser']);
				$temp_Array['price'] = $p['price_sbc'];
				$temp_Array['coverage'] = $p['coverage_sbc'];
				//$temp_Array['cost'] = $p['price_sbc']/2;
				array_push($productSet, $temp_Array);
			}
			$this->response($productSet, 200);
		} else {
			$this->response(array('status' => "Failed", "msg" => "NO EXIST DATA"), 409);
		}
	}

	/*	 * ****************************VA
	 * @name: WSCardsTravelersDistribution
	 * @return: array cards. No used

	 */

	private function WSCardsTravelersDistribution() {

		if (!$this->validate_login_attempt()) {
			$this->response('', 418);
		}

		if ($this->get_request_method() != "POST") {
			$this->response('', 406);
		}

		$maxExtras = $this->_request['maxBeneficiaryExtrasByProduct'];
		$adultsNum = $this->_request['totalAdults'];
		$childrenNum = $this->_request ['totalKids'];
		$spouseTraveling = $this->_request ['spouseTraveling'];
		$extrasRestricted = $this->_request ['extrasRestricted'];
		$adultsFree = $this->_request ['adultsFree'];
		$childrenFree = $this->_request ['childrenFree'];

		$cards = GlobalFunctions::getCardsTravelersDistribution($maxExtras, $adultsNum, $childrenNum, $spouseTraveling, $extrasRestricted, $adultsFree, $childrenFree);
		if ($cards) {
			$productSet = array();
			$temp_Array = array();
			foreach ($cards as $c) {
				$temp_Array['ID_proBycou'] = $c['serial_pxc'];
			}
		}
	}

	/*	 * VA
	 * @name minimumDataRequired
	 * @param array $data
	 * @return boolean 
	 */

	private function minimunDataRequiredWSAddBen($data_array) {
		$case_code = 200;

		if (!is_array($data_array)) {
			$case_code = 650;
		} elseif (!isset($data_array['language'])) {
			$case_code = 658;
		} elseif (!isset($data_array['id_Origin'])) {
			$case_code = 660;
		} elseif (!isset($data_array['id_Counter'])) {
			$case_code = 659;
		}

		return $case_code;
	}

	private function minimunDataRequiredWSlpbm($data_array) {
		$case_code = 200;

		if (!is_array($data_array)) {
			$case_code = 650;
		} elseif (!isset($data_array['id_Motive'])) {
			$case_code = 651;
		} elseif (!isset($data_array['language'])) {
			$case_code = 652;
		} elseif (!isset($data_array['id_Counter'])) {
			$case_code = 653;
		}

		return $case_code;
	}

	/*	 * VA
	 * @name validateDataSet
	 * @param array $data
	 * @return boolean 
	 */

	private function validateDataWSlpbm($data_array) {

		$code_validation = 200;

		if (!is_numeric($data_array['id_Motive'])) {
			$code_validation = 662;
		} elseif (!is_numeric($data_array['language'])) {
			$code_validation = 663;
		} elseif (!is_numeric($data_array['id_Counter'])) {
			$code_validation = 664;
		}
		return $code_validation;
	}

}

$api = new API;
$api->processApi();
?>
