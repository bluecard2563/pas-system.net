<?php
/**
 * File: ws_products
 * Author: Patricio Astudillo
 * Creation Date: 24-sep-2012
 * Last Modified: 24-sep-2012
 * Modified By: Patricio Astudillo
 */

	ini_set('include_path',ini_get('include_path').':/Applications/XAMPP/htdocs/PLANETASSIST/lib/:/Applications/XAMPP/htdocs/PLANETASSIST/lib/clases:/Applications/XAMPP/htdocs/PLANETASSIST:/Applications/XAMPP/htdocs/PLANETASSIST/lib/smarty-2.6.22');

	define('DOCUMENT_ROOT', '/Applications/XAMPP/htdocs/PLANETASSIST/');
	require_once(DOCUMENT_ROOT.'lib/config.inc.php');
	require_once (DOCUMENT_ROOT.'lib/nusoap/nusoap.php');
	
	/**************************** HEADER INFORMATION *******************************/
	$server = new soap_server;
	$ns = WSDL_ROOT;
	$server ->configureWSDL('ws_products_library', $ns);
	$server ->wsdl->schematargetnamespace = $ns;
	
	function doAuthenticate() {
		global $db;
		
		if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
			$serial_pos = WebPOS::verifyWSDLLogin($db, $_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'], $_SERVER['REMOTE_ADDR']);

			if ($serial_pos)
				return $serial_pos;
			else
				return false;
		}else{
			return false;
		}
	}	
	
	/**************************** ALL PRODUCTS *******************************/	
	$server->wsdl->addComplexType(
				'ProductResponse',
				'complexType',
				'struct',
				'All',
				'',
				array(
					'operation_result' => array(	'name' => 'operation_result',
													'type' => 'xsd:string'),
					'result_code' => array(	'name' => 'result_code',
											'type' => 'xsd:string'),
					'custom_payment_code' => array(	'name' => 'custom_payment_code',
													'type' => 'xsd:int'),
					'data_struct' => array(	'name' => 'data_struct',
														'type' => 'tns:ProductArray')
				)
			);
	
	$server->wsdl->addComplexType(
				'ProductArray',										//Name
				'complexType',										//Class name
				'array',											//PHP Type
				'',													//Type Definition [all/sequence/choice]
				'SOAP-ENC:Array',
				array(),
				array(												//Attributes
					array(	'ref' => 'SOAP-ENC:arrayType',
							'wsdl:arrayType' => 'tns:Products')
				),
				'tns:Products'
			);
	
	$server->wsdl->addComplexType(
				'Products',
				'complexType',
				'struct',
				'All',
				'',
				array(
					'serial_pxc' => array(	'name' => 'serial_pxc',
											'type' => 'xsd:int'),
					'name_pbl' => array(	'name' => 'name_pbl',
											'type' => 'xsd:string'),
					'serial_pro' => array(	'name' => 'serial_pro',
											'type' => 'xsd:int'),
					'show_price_pro' => array(	'name' => 'show_price_pro',
												'type' => 'xsd:string')
				)
			);
	
	function wsGetAllProducts($language){
		$serial_pos = doAuthenticate();
		$response = array();
		
		if($serial_pos){
			global $db;
			$products = WebPOS::getProductListByPOS($db, $serial_pos, $language);
			
			if(!$products){
				$response = array(	'operation_result'=> 'error',
									'result_code' => 'LP0001',
									'custom_payment_code' => '0',
									'data_struct' => NULL
							);
			}else{
				$response = array(	'operation_result'=> 'success',
									'result_code' => 'LP0000',
									'custom_payment_code' => '0',
									'data_struct' => $products
							);
			}
			
		}else{
			$response = array(	'operation_result'=> 'error',
									'result_code' => 'ATH0000',
									'custom_payment_code' => '0',
									'data_struct' => NULL
							);
		}
		
		return $response;
	}
	
	$server->register(
				'wsGetAllProducts',								//function name
				array(	'language' => 'xsd:int'),					//parametros
				array('return' => 'tns:ProductResponse'),
				$ns
			);
	
	
	/**************************** GET BENEFITS INFORMATION *******************************/
	$server->wsdl->addComplexType(
				'BenefitsResponse',
				'complexType',
				'struct',
				'All',
				'',
				array(
					'operation_result' => array(	'name' => 'operation_result',
													'type' => 'xsd:string'),
					'result_code' => array(	'name' => 'result_code',
											'type' => 'xsd:string'),
					'custom_payment_code' => array(	'name' => 'custom_payment_code',
													'type' => 'xsd:int'),
					'data_struct' => array(	'name' =>	'data_struct',
														'type' => 'tns:BenefitArray')
				)
			);
	
	$server->wsdl->addComplexType(
				'BenefitArray',										//Name
				'complexType',										//Class name
				'array',											//PHP Type
				'',													//Type Definition [all/sequence/choice]
				'SOAP-ENC:Array',
				array(),
				array(												//Attributes
					array(	'ref' => 'SOAP-ENC:arrayType',
							'wsdl:arrayType' => 'tns:Benefits')
				),
				'tns:Benefits'
			);
	
	$server->wsdl->addComplexType(
				'Benefits',
				'complexType',
				'struct',
				'All',
				'',
				array(
					'serial_ben' => array(				'name' => 'serial_ben',				'type' => 'xsd:int'),
					'description_bbl' => array(			'name' => 'description_bbl',		'type' => 'xsd:string'),
					'price_bxp' => array(				'name' => 'price_bxp',				'type' => 'xsd:float'),
					'symbol_cur' => array(				'name' => 'symbol_cur',				'type' => 'xsd:string'),
					'restriction_price_bxp' => array(	'name' => 'restriction_price_bxp',	'type' => 'xsd:float'),
					'description_rbl' => array(			'name' => 'description_rbl',		'type' => 'xsd:string'),
					'description_cbl' => array(			'name' => 'description_cbl',		'type' => 'xsd:string')
				)
			);
	
	function wsGetBenefitsForProduct($product_id, $language){
		$serial_pos = doAuthenticate();
		$response = array();
		
		if($serial_pos){
			global $db;
			$benfits = WebPOS::getBenefitsByProductPOS($db, $serial_pos, $product_id, $language);

			if($benfits){
				foreach($benfits as &$b){
					if($b['restriction_price_bxp'] == ''){
						$b['restriction_price_bxp'] = 0;
					}

					if($b['description_rbl'] == ''){
						$b['description_rbl'] = 'N/A';
					}
				}
				
				$response = array(	'operation_result'=> 'success',
									'result_code' => 'LB0000',
									'custom_payment_code' => '0',
									'data_struct' => $benfits
							);

			}else{
				$response = array(	'operation_result'=> 'error',
									'result_code' => 'LB0001',
									'custom_payment_code' => '0',
									'data_struct' => NULL
							);
			}
		}else{
			$response = array(	'operation_result'=> 'error',
									'result_code' => 'ATH0000',
									'custom_payment_code' => '0',
									'data_struct' => NULL
							);
		}
		
		return $response;
		
	}
	
	$server->register(
				'wsGetBenefitsForProduct',
				array(	'product_id' => 'xsd:int',
						'language' => 'xsd:int'),
				array('return' => 'tns:BenefitsResponse'),
				$ns
			);
	
	
	/**************************** PRICE QUOTATION INFORMATION *******************************/
	$server->wsdl->addComplexType(
				'QuoteResponse',
				'complexType',
				'struct',
				'All',
				'',
				array(
					'operation_result' => array(	'name' => 'operation_result',
													'type' => 'xsd:string'),
					'result_code' => array(	'name' => 'result_code',
											'type' => 'xsd:string'),
					'custom_payment_code' => array(	'name' => 'custom_payment_code',
													'type' => 'xsd:int'),
					'data_struct' => array(	'name' =>	'data_struct',
														'type' => 'tns:QuotesArray')
				)
			);
	
	$server->wsdl->addComplexType(
				'QuotesArray',										//Name
				'complexType',										//Class name
				'array',											//PHP Type
				'',													//Type Definition [all/sequence/choice]
				'SOAP-ENC:Array',
				array(),
				array(												//Attributes
					array(	'ref' => 'SOAP-ENC:arrayType',
							'wsdl:arrayType' => 'tns:Benefits')
				),
				'tns:Quotations'
			);
	
	$server->wsdl->addComplexType(
				'Quotations',
				'complexType',
				'struct',
				'All',
				'',
				array(
					'serial_pxc' => array(		'name' => 'serial_pxc',				'type' => 'xsd:int'),
					'name_pbl' => array(		'name' => 'name_pbl',		'type' => 'xsd:string'),
					'spouseTraveling' => array(	'name' => 'spouseTraveling',				'type' => 'xsd:string'),
					'beginDate' => array(		'name' => 'beginDate',				'type' => 'xsd:string'),
					'endDate' => array(			'name' => 'endDate',	'type' => 'xsd:string'),
					'days' => array(			'name' => 'days',		'type' => 'xsd:int'),
					'adults' => array(			'name' => 'adults',		'type' => 'xsd:int'),
					'children' => array(		'name' => 'children',				'type' => 'xsd:int'),
					'max_extras' => array(		'name' => 'max_extras',		'type' => 'xsd:int'),
					'price_pod' => array(		'name' => 'price_pod',				'type' => 'xsd:float'),
					'price_children_pod' => array('name' => 'price_children_pod',				'type' => 'xsd:float'),
					'price_spouse_pod' => array(  'name' => 'price_spouse_pod',	'type' => 'xsd:float'),
					'price_extra_pod' => array(	'name' => 'price_extra_pod',		'type' => 'xsd:float'),
					'cards_pod' => array(		'name' => 'cards_pod',		'type' => 'xsd:int'),
					'total_price_pod' => array(	'name' => 'total_price_pod',		'type' => 'xsd:float')
				)
			);
	
	/**
	 * @name wsGetQuote
	 * @global type $db
	 * @param int $product_id Spected value of serial_pxc
	 * @param int $language
	 * @param int $travel_target_days
	 * @param string $spouse_travels true/false
	 * @param string $date_from dd/mm/YYY
	 * @param string $date_to dd/mm/YYY
	 * @param int $older_travelers
	 * @param int $children_travelers
	 * @return int|string 
	 */
	function wsGetQuote($serial_pxc, $language, $travel_target_days, $spouse_travels, $date_from,
						$date_to, $older_travelers, $children_travelers){
		global $db;
		$serial_pos = doAuthenticate();
		$flight_days = GlobalFunctions::getDaysDiffDdMmYyyy($date_from, $date_to);
		$quotation_list = array();
		$no_prices = false;
		
		if($serial_pos){
			$code_lang = Language::getCodeLangBySerial($db, $language);			//GET LANGUAGE CODE FOR SERIAL

			if($serial_pxc == "-1"){	//IF WE NEED A GENERAL QUOTATION.
				$allEstimatorProducts = WebPOS::getProductListByPOS($db, $serial_pos, $language);
			}elseif($serial_pxc){		//IF WE NEED A SINGLE PRODUCT QUOTE
				$prodByCou = new ProductByCountry($db, $serial_pxc);
				$prodByCou -> getData();
				$prodP = new Product($db, $prodByCou -> getSerial_pro());
				$prod = $prodP -> getInfoProduct($code_lang);

				$prod[0]['serial_pxc'] = $serial_pxc;
				$prod[0]['serial_pro'] = $prodByCou -> getSerial_pro();
				$allEstimatorProducts = $prod;
			}

			foreach ($allEstimatorProducts as $prod){
				switch($extrasRestricted){
					case 'ADULTS':
						$children_travelers = 0;
						break;
					case 'CHILDREN':
						//LEAVE FORM INTACT
						break;
					default: //BOTH
						break;
				}

				$singleProduct = array(
					'serial_pxc' => $prod['serial_pxc'],
					'name_pbl' => $prod['name_pbl'],
					'spouseTraveling'=> $spouse_travels,
					'beginDate'=> $date_from,
					'endDate'=> $date_to,
					'days' => $flight_days,
					'adults' => $older_travelers,
					'children' => $children_travelers,
				);
				
				$serial_pbd = WebPOS::getSerialPBDForPOSProduct($db, $serial_pos, $prod['serial_pxc']);

				//GLOBAL FUNCTION WHICH GIVES A PRICE SET IN TH SAME $singleProduct ARRAY
				if(WebPOS::calculatePriceForPOSList($db, $singleProduct, $selTripDays, $extrasRestricted, $serial_pbd) === 'NO_PRICES'){
					$no_prices = true;
					break;
				}

				/* UNSET COSTS REFERENCE FOR AVOID INFORMATION LEAK */
				unset($singleProduct['cost_pod']);
				unset($singleProduct['cost_children_pod']);
				unset($singleProduct['cost_spouse_pod']);
				unset($singleProduct['cost_extra_pod']);

				//FILLING FINAL ARRAY
				array_push($quotation_list, $singleProduct);
			}
			
		}else{	//ERROR CONNECTION FAILURE
			$no_prices = true;
			$response = array(	'operation_result'=> 'error',
									'result_code' => 'ATH0000',
									'custom_payment_code' => '0',
									'data_struct' => NULL
							);
		}
		
		
		if(!$no_prices){	//SUCCESS AND RETURN
			$response = array(	'operation_result'=> 'success',
								'result_code' => 'GQ0000',
								'custom_payment_code' => '0',
								'data_struct' => $quotation_list
						);
		}else{	//NO QUOTATION
			$response = array(	'operation_result'=> 'error',
								'result_code' => 'GQ0001',
								'custom_payment_code' => '0',
								'data_struct' => NULL
						);
			
		}
		
		return $response;
	}
	
	$server->register(
				'wsGetQuote',
				array(	'serial_pxc' => 'xsd:int',
						'language' => 'xsd:int',
						'travel_target_days' => 'xsd:int',
						'spouse_travels' => 'xsd:string',
						'date_from' => 'xsd:string',
						'date_to' => 'xsd:string',
						'older_travelers' => 'xsd:int',
						'children_travelers' => 'xsd:int',
					),
				array('return' => 'tns:QuoteResponse'),
				$ns
			);
	
	
	/**************************** ENDING WS INFORMATION *******************************/
	
	$input = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : implode('\r\n', file('php://input'));
	$server->service($input);
	exit;
			
?>
