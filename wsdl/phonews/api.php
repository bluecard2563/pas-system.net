<?php

/**
 * File: api
 * Author: Patricio Astudillo
 * Creation Date: 19-jun-2013
 * Last Modified: 19-jun-2013
 * Modified By: Patricio Astudillo
 */
ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net/:/var/www/pas-system.net/lib/smarty-2.6.22:/var/www/pas-system.net/lib/Excel/Classes/:/var/www/pas-system.net/lib/PDF/');
define('DOCUMENT_ROOT', '/var/www/pas-system.net/');
require_once(DOCUMENT_ROOT.'lib/config.inc.php');


//ini_set('include_path', ini_get('include_path') . ':/var/www/html/PLANETASSIST/lib/:/var/www/html/PLANETASSIST/lib/clases:/var/www/html/PLANETASSIST/:/var/www/html/PLANETASSIST/lib/smarty-2.6.22:/var/www/html/PLANETASSIST/lib/Excel/Classes/:/var/www/html/PLANETASSIST/lib/PDF/');
//define('DOCUMENT_ROOT', '/var/www/html/PLANETASSIST/');
//require_once(DOCUMENT_ROOT . 'lib/config.inc.php');
//require_once(DOCUMENT_ROOT . 'lib/adodb5/adodb.inc.php');

    class API extends Rest {
        
        public function __construct(){
            parent::__construct();	// Init parent contructor
        }

        public function processApi(){
            $func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
            if((int)method_exists($this,$func) > 0)
                $this->$func();
            else
                $this->response('',404);	// If the method not exist with in this class, response would be "Page not found".
        }
        
        /**
         * Retrieves database connection
         * 
         * @param int $queue_opt Identifier which describes db selection 
         *
         * @return string $db
        */
        private function chooseConnection($queue_opt){
            if (is_numeric($queue_opt)){
                switch ($queue_opt){
                    case '1':   $host = 'localhost';
                                $db_name = 'planetassist';
                                $user = 'root';
                                $pass = '';
                                break;
                    
                    case '2':   $host = 'localhost';
                                $db_name = 'rocafuerte';
                                $user = 'root';
                                $pass = '';
                                break;
                    
                    case '3':   $host = 'localhost';
                                $db_name = 'sucre';
                                $user = 'root';
                                $pass = '';
                                break;

                    default:    $host = NULL;
                                $db_name = NULL;
                                $user = NULL;
                                $pass = NULL;
                                break;
                }
                if ($host && $db_name && $user){
                    $db_connection = &ADONewConnection('mysql');
                    $db_connection->PConnect($host, $user, $pass, $db_name);
                    
                    return $db_connection;
                }
            }
            return false;
        }


        /**
         * Retrieves customer if matches criteria
         * File, customer or card number
         *
         * @return array $customer
        */
        function searchContact() {
            if ($this->get_request_method() != "POST") {
               $this->response('', 406);
            }

//            if (!$this->validate_login_attempt()) {
//                $this->response(array('data' => 'Error when attempting to login'), 418);
//            }
            
            //Phone queue option
            $queue_opt = $this->_request['caller'];
            
            if (is_numeric($queue_opt)) {
                $db =  $this->chooseConnection($queue_opt);
                
                if ($db){
                    $response = array();
                    if ($this->_request['file'] != '') {
                        $search_param = $this->_request['file'];
                        $function = 'WSPhonesLib::getCustomerByFile';
                        $type = 'file';
                    } elseif ($this->_request['document'] != ''){
                        $search_param = $this->_request['document'];
                        $function = 'WSPhonesLib::getCustomerByDocument';
                        $type = 'document';
                    } elseif ($this->_request['card'] != ''){
                        $search_param = $this->_request['card'];
                        $function = 'WSPhonesLib::getCustomerByCardNumber';
                        $type = 'card';
                    } else {
                        $search_param = NULL;
                        $function = NULL;
                    }
                    
                    if ($function && $search_param){
                        $search_results = call_user_func_array($function, array($db, $search_param));
                        if (count($search_results)>0){
                            if ($type == 'card') {
                                if ($search_results[0]['type_con'] == 'LEGAL_ENTITY' && $search_results[0]['check_con'] == 'MULTI'){
                                    $response['legal_entity']['id'] = $search_results[0]['id'];
                                    $response['legal_entity']['document_con'] = $search_results[0]['document_con'];
                                    $response['legal_entity']['firstname_con'] = $search_results[0]['firstname_con'];
                                    $response['legal_entity']['lastname_con'] = $search_results[0]['lastname_con'];

                                    $response['search']['id'] = $search_results[0]['id_ext'];
                                    $response['search']['document_con'] = $search_results[0]['document_ext'];
                                    $response['search']['firstname_con'] = $search_results[0]['firstname_ext'];
                                    $response['search']['lastname_con'] = $search_results[0]['lastname_ext'];
                                    $response['search']['begin_date'] = $search_results[0]['begin_date'];
                                    $response['search']['end_date'] = $search_results[0]['end_date'];
                                } else {
                                    $response['extras'] = WSPhonesLib::getExtrasCustomersBySale($db, $search_results[0]['serial_sal']);

                                    $response['search']['id'] = $search_results[0]['id'];
                                    $response['search']['document_con'] = $search_results[0]['document_con'];
                                    $response['search']['firstname_con'] = $search_results[0]['firstname_con'];
                                    $response['search']['lastname_con'] = $search_results[0]['lastname_con'];
                                    $response['search']['begin_date'] = $search_results[0]['begin_date'];
                                    $response['search']['end_date'] = $search_results[0]['end_date'];
                                }
                            } else {
                                if ($search_results[0]['check_per'] == 'EXTRA'){
                                    $response['main'] = WSPhonesLib::getMainCustomerBySale($db, $search_results[0]['serial_sal']);
                                }else
                                    $response['extras'] = WSPhonesLib::getExtrasCustomersBySale($db, $search_results[0]['serial_sal']);
                                unset($search_results[0]['check_per']);
                                unset($search_results[0]['serial_sal']);
                                unset($search_results[0]['type_con']);
                                $response['search'] = $search_results[0];
                            }
                        }
                    }
                    
                    $this->response(array('data' => $response), $this->_code);
                    
                } else {
                    $this->response(array('data' => 'Cannot connnect to database'), 404);
                }
            } else {
                $this->response(array('data' => 'Incomplete data'), 404);
            }
       }
    }
    
    // Initiiate Library
	$api = new API;
	$api->processApi();