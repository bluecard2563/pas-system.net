<?php

/**
 * File: country
 * Author: Carlos Oberto
 * Creation Date: 04-09-2017
 * 
 */
//	ini_set('include_path',ini_get('include_path').':/home/blueorgo/public_html/lib/:/home/blueorgo/public_html/lib/clases:/home/blueorgo/public_html/:/home/blueorgo/public_html/lib/smarty-2.6.22:/home/blueorgo/public_html/lib/Excel/Classes/:/home/blueorgo/public_html/lib/PDF/');
//	define('DOCUMENT_ROOT', '/home/blueorgo/public_html/lib/config.inc.php');

ini_set('include_path', ini_get('include_path') . "C:/xampp/htdocs/PLANETASSIST/lib/;C:/xampp/htdocs/PLANETASSIST/lib/clases/;C:/xampp/htdocs/PLANETASSIST/;C:/xampp/htdocs/PLANETASSIST/lib/smarty-2.6.22/;C:/xampp/htdocs/PLANETASSIST/lib/PDF");
define('DOCUMENT_ROOT', "C:/xampp/htdocs/PLANETASSIST");
//require_once('C:/xampp/htdocs/PLANETASSIST/lib/config.inc.php');
require_once(DOCUMENT_ROOT . 'lib/config.inc.php');
//include_once 'C:/xampp/htdocs/PLANETASSIST/lib/erpLibrary.inc.php';
include_once DOCUMENT_ROOT . 'lib/erpLibrary.inc.php';

class API extends Rest {

    private $varBoolean = array('YES', 'NO');

    public function __construct() {
        parent::__construct();    // Init parent contructor
    }

    public function processApi() {
        $func = strtolower(trim(str_replace("/", "", $_REQUEST['rquest'])));
        if ((int) method_exists($this, $func) > 0)
            $this->$func();
        else
            $this->response('', 404);    // If the method not exist with in this class, response would be "Page not found".
    }
    
    /* **************************
     * FUNCTIONS
     */
    
     /* ********************************
     * $Name  WSListCountries
     * Autor: Carlos Oberto
     * Date: 04/09/2017
     */
    public function WSListCountries() {               
            $listCou = SiseCountry::LoadCountries($this->_db);
            if($listCou){
                
                $arrCou = array();
                $arrFinal = array();
                $temp_Array = array();
                $arrFinal['estadoTransaccion'] = 'true';
                foreach ($listCou as $c) {
                        
                        $temp_Array['Id'] = $c['serial_sise_country'];
                        $temp_Array['Pais'] = utf8_decode($c['desc_sise_country']);
                        $temp_Array['codUaf'] = $c['cod_uaf_sise_country'];
                        $temp_Array['codUif'] = ($c['cod_uif_sise_country']);
                        $temp_Array['codAts'] = $c['cod_ats_sise_country'];
                        $temp_Array['codSuperNac'] = $c['cod_super_nac_sise_country'];
                        $temp_Array['codTipoRegimen'] = ($c['cod_tipo_regimen_sise_country']);
                        $temp_Array['codParFiscal'] = ($c['cod_par_fiscal_sise_country']);
                        $temp_Array['codPASCountry'] = ($c['cod_pas_sise_country']);
                        
                        array_push($arrCou, $temp_Array);
                    }
                    $arrFinal['paises'] = $arrCou;
                    $arrFinal['errorCodigo'] = '0';
                    $arrFinal['errorTipo'] = '';
                    $arrFinal['errorDescripcion'] = '';
//                    Debug::print_r($temp_Cou); die();
                    $this->response($arrFinal, 200);
                    } else {
                    $this->response(array('status' => "Failed", "msg" => "NO COUNTRIES"), 409);
                }
            }
            /* ********************************
             * $Name  WSListProvincias
             * Autor: Carlos Oberto
             * Date: 05/09/2017
             */
            public function WSListProvinces(){
//                if (!$this->validate_login_attempt()) {
//                   $this->response('', 418);
//                }

//                if ($this->get_request_method() != "POST") {
//                    $this->response('', 406);
//                }       

                $serialCou = $this->_request['serial_country'];
//                Debug::print_r($serialDepar); die();
                $listPro = SiseCountry::LoadProvinces($this->_db, $serialCou);
                if($listPro){
                    
                    $arrPro = array();
                    $temp_Array = array();
                    $arrFinal = array();
                    $arrFinal['estadoTransaccion'] = 'true';
                    
                foreach ($listPro as $c) {
                    
                        $temp_Array['IdDepar'] = $c['serial_sise_departamento'];
                        $temp_Array['descDepar'] = utf8_decode($c['desc_sise_departamento']);
                        $temp_Array['IdCountry'] = $c['serial_sise_country'];
                        $temp_Array['descCou'] = utf8_decode($c['desc_sise_country']);
                        $temp_Array['codUaf'] = $c['cod_uaf_sise_country'];
                        $temp_Array['codUif'] = ($c['cod_uif_sise_country']);
                        $temp_Array['codAts'] = $c['cod_ats_sise_country'];
                        $temp_Array['codSuperNac'] = $c['cod_super_nac_sise_country'];
                        $temp_Array['codTipoRegimen'] = ($c['cod_tipo_regimen_sise_country']);
                        $temp_Array['codParFiscal'] = ($c['cod_par_fiscal_sise_country']);
                        $temp_Array['codZonaSismica'] = ($c['cod_zona_sismica']);
                        $temp_Array['codSubZona'] = ($c['cod_subzona']);
                        $temp_Array['codUif'] = ($c['cod_uif']);
                        $temp_Array['codPASCountry'] = ($c['cod_pas_sise_country']);
                        
                        array_push($arrPro, $temp_Array);
                    }
                    $arrFinal['provincias'] = $arrPro;
                    $arrFinal['errorCodigo'] = '0';
                    $arrFinal['errorTipo'] = '';
                    $arrFinal['errorDescripcion'] = '';
//                    Debug::print_r($temp_Cou); die();
                    $this->response($arrFinal, 200);
//                    $this->response($arrPro, 200);
                    } else {
                    $this->response(array('status' => "Failed", "msg" => "NO PROVINCIAS"), 409);
                }
            }
            /* ********************************
             * $Name  WSListMuni
             * Autor: Carlos Oberto
             * Date: 05/09/2017
             */
            public function WSListMuni(){
//                if (!$this->validate_login_attempt()) {
//                   $this->response('', 418);
//                }

//                if ($this->get_request_method() != "POST") {
//                    $this->response('', 406);
//                }       

                $serialCou = $this->_request['serial_country'];
//                Debug::print_r($serialMuni); die();
                $listPro = SiseCountry::LoadMuni($this->_db, $serialCou);
                if($listPro){
                    
                    $arrPro = array();
                    $temp_Array = array();
                    $arrFinal = array();
                    $arrFinal['estadoTransaccion'] = 'true';
                    
                foreach ($listPro as $c) {
                        $temp_Array['idMuni'] = utf8_decode($c['cod_sise_municipio']);
                        $temp_Array['descMuni'] = utf8_decode($c['desc_sise_municipio']);
                        $temp_Array['IdDepar'] = $c['serial_sise_departamento'];
                        $temp_Array['descDepar'] = utf8_decode($c['desc_sise_departamento']);
                        $temp_Array['IdCountry'] = $c['serial_sise_country'];
                        $temp_Array['descCou'] = utf8_decode($c['desc_sise_country']);
                        $temp_Array['codUaf'] = $c['cod_uaf_sise_country'];
                        $temp_Array['codUif'] = ($c['cod_uif_sise_country']);
                        $temp_Array['codAts'] = $c['cod_ats_sise_country'];
                        $temp_Array['codSuperNac'] = $c['cod_super_nac_sise_country'];
                        $temp_Array['codTipoRegimen'] = ($c['cod_tipo_regimen_sise_country']);
                        $temp_Array['codParFiscal'] = ($c['cod_par_fiscal_sise_country']);
                        $temp_Array['codZonaSismica'] = ($c['cod_zona_sismica']);
                        $temp_Array['codSubZona'] = ($c['cod_subzona']);
                        $temp_Array['codUif'] = ($c['cod_uif']);
                        $temp_Array['codPASCountry'] = ($c['cod_pas_sise_country']);
                        
                        array_push($arrPro, $temp_Array);
                    }
                    $arrFinal['Municipios'] = $arrPro;
                    $arrFinal['errorCodigo'] = '0';
                    $arrFinal['errorTipo'] = '';
                    $arrFinal['errorDescripcion'] = '';
//                    Debug::print_r($temp_Cou); die();
                    $this->response($arrFinal, 200);
//                    $this->response($arrPro, 200);
                    } else {
                    $this->response(array('status' => "Failed", "msg" => "NO MUNICIPIOS"), 409);
                }
            }
}




//************************ API INITIALIZE **************
$api = new API;
$api->processApi();
?>
