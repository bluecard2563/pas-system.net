<?php
/**
 * File: account
 * Author: Patricio Astudillo
 * Creation Date: 17-dic-2012
 * Last Modified: 17-dic-2012
 * Modified By: Patricio Astudillo
 */

	ini_set('include_path',ini_get('include_path').':/var/www/pas-system.net/lib/:/var/www/pas-system.net/lib/clases:/var/www/pas-system.net:/var/www/pas-system.net/lib/smarty-2.6.22');
	define('DOCUMENT_ROOT', '/var/www/pas-system.net/');
	require_once(DOCUMENT_ROOT.'lib/config.inc.php');
	
	class API extends Rest {
		public function __construct(){
			parent::__construct();				// Init parent contructor
		}
		
		public function processApi(){
			$func = strtolower(trim(str_replace("/","",$_REQUEST['rquest'])));
			if((int)method_exists($this,$func) > 0)
				$this->$func();
			else
				$this->response('',404);				// If the method not exist with in this class, response would be "Page not found".
		}
		
		/* 
		 *	Simple login API
		 *  Login must be POST method
		 *  email : <USER EMAIL>
		 *  pwd : <USER PASSWORD>
		 */
		private function login(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			$email = $this->_request['email'];		
			$password = $this->_request['pwd'];
			
			if(!empty($email) and !empty($password)){
				if(filter_var($email, FILTER_VALIDATE_EMAIL)){
					$customer = new Customer($this->_db);
					$customer -> setEmail_cus($email);
					$customer -> getData(false);
					if($customer->existsCustomerEmail($email,NULL)){
						if(md5($password) == $customer -> getPassword_cus()){
							$cus_data = array();
							$cus_data['serial'] = $customer->getSerial_cus();
							$cus_data['firstname'] = utf8_encode($customer->getFirstname_cus());
							$cus_data['lastname'] = utf8_encode($customer->getLastname_cus());
							$cus_data['birthdate'] = $customer->getBirthdate_cus();
							$cus_data['email'] = $customer->getEmail_cus();
							$cus_data['password'] = $customer->getPassword_cus();
							$cus_data['phone1'] = $customer->getPhone1_cus();
							$cus_data['address'] = utf8_encode($customer->getAddress_cus());
							$cus_data['relative'] = utf8_encode($customer->getRelative_cus());
							$cus_data['relative_phone'] = $customer->getRelative_phone_cus();
							
							$this->response($cus_data, 200);
						}
					}
				}
			}
			
			$error = array('status' => "Failed", "msg" => "Invalid Email address or Password");
			$this->response($error, 400);
		}
		
		private function updateMyData(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			$serial = $this->_request['serial'];		
			$firstname = $this->_request['firstname'];
			$lastname = $this->_request['lastname'];		
			$phone1 = $this->_request['phone1'];
			$address = $this->_request['address'];		
			$relative = $this->_request['relative'];
			$relative_phone = $this->_request['relative_phone'];
			
			if(!empty($serial)){
					$customer = new Customer($this->_db, $serial);
					if($customer ->getData(false)){
						$customer ->setFirstname_cus($firstname);
						$customer ->setLastname_cus($lastname);
						$customer ->setPhone1_cus($phone1);
						$customer ->setAddress_cus($address);
						$customer ->setRelative_cus($relative);
						$customer ->setRelative_phone_cus($relative_phone);
						
						if($customer->update()){
							$this->response(array('success'), 200);
						}
					}
			}
			
			$error = array('status' => "Failed", "msg" => "No Data");
			$this->response($error, 400);
		}
		
		private function retriveContactPhones(){
			global $globalPhone;

			$xCountry = new Country($this->_db);
			$phones = $xCountry->getPhoneCountries();
			
			if($phones[0]['serial_cou'] == '1'){
				$globalPhone = $phones[0]['phone_cou'];
				unset($phones[0]);
			}
			
			if($phones){
				$this->response($phones, 200);
			}
			
			$error = array('status' => "Failed", "msg" => "NO PHONES");
			$this->response($error, 400);
		}
		
		private function myTrips(){
			if($this->get_request_method() != "POST"){
				$this->response('',406);
			}
			
			$serial_cus = $this->_request['serial'];
			
			$my_trips = WSAccountsLib::getMyAllTravels($this->_db, $serial_cus);
			
			if($my_trips){
				foreach($my_trips as &$trip){
					$trip['name_cou'] = utf8_encode($trip['name_cou']);
					$trip['name_pbl'] = utf8_encode($trip['name_pbl']);
					$trip['name_cit'] = utf8_encode($trip['name_cit']);
				}
				
				$this->response($my_trips, 200);
			}
			
			$error = array('status' => "Failed", "msg" => "NO TRIPS");
			$this->response($error, 400);
		}
		
		private function primeDestination(){
			$destination = WSAccountsLib::destinationMonth();
			
			$this->response($destination, 200);
		}
		
		private function retrieveCountryList(){
			$countries = WSAccountsLib::getBaseCountries($this->_db);
			
			foreach($countries as &$c){
				$c['name_cou'] = utf8_encode($c['name_cou']);
				$c['name_zon'] = utf8_encode($c['name_zon']);
			}
			
			if($countries){
				$this->response($countries, 200);
			}
			
			$error = array('status' => "Failed", "msg" => "NO PHONES");
			$this->response($error, 400);
		}
	}
	
	// Initiiate Library
	$api = new API;
	$api->processApi();
?>
